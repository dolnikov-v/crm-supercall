<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=yii2advanced',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
        'genesys' => [
            'token' => '',
            'secretKey' => '',
            'db' => [
                'dsn' => 'oci:host=;dbname=',
                'username' => '',
                'password' => '',
                'charset' => 'utf8',
            ],
            'db2' => [
                'dsn' => 'oci:dbname=',
                'username' => '',
                'password' => '',
            ],
        ],
    ],
];

<?php

$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => '',
        ],
        'genesys' => [
            'db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'oci:dbname=//localhost:1521/dbname',
                'username' => '',
                'password' => '',
                'charset' => 'utf8',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];

return $config;

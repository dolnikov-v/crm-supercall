<?php
namespace common\widgets;

use common\components\base\FormModel;
use common\widgets\base\Checkbox;
use common\widgets\base\Datepicker;
use common\widgets\base\Email;
use common\widgets\base\Phone;
use common\widgets\base\Select2;
use common\widgets\base\StringGenerator;
use common\widgets\base\TextInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class ActiveField
 * @package common\widgets
 */
class ActiveField extends \yii\widgets\ActiveField
{
    const TYPE_INTEGER = 'integer';
    const TYPE_STRING = 'string';
    const TYPE_TEXT = 'text';
    const TYPE_EMAIL = 'email';
    const TYPE_PHONE = 'phone';
    const TYPE_LIST = 'list';

    /**
     * @var array
     */
    public $options = [];

    /**
     * @var string
     */
    public $template = '<div class="form-group"><div class="control-label">{label}</div>{input}</div>';

    /**
     * @var string
     */
    public $templatePasswordInput = '{label}<div class="input-group box-password-showing">{input}<span class="input-group-btn"><button class="btn btn-default" type="button" data-widget="password-showing"><i class="fa fa-eye"></i></button></span></div>';

    /**
     * @var array
     */
    public $labelOptions = [];

    /**
     * @var string
     */
    public $type;

    /**
     * @param array $options
     * @return $this
     */
    public function textInput($options = [])
    {
        $custom = [
            'id' => Html::getInputId($this->model, $this->attribute),
            'name' => Html::getInputName($this->model, $this->attribute),
            'placeholder' => $this->model->getAttributeLabel(Html::getAttributeName($this->attribute)),
            'value' => Html::getAttributeValue($this->model, $this->attribute),
        ];

        $options = ArrayHelper::merge($custom, $options);

        $this->parts['{input}'] = TextInput::widget($options);

        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function textInputGenerate($options = [])
    {
        $custom = [
            'id' => Html::getInputId($this->model, $this->attribute),
            'name' => Html::getInputName($this->model, $this->attribute),
            'placeholder' => $this->model->getAttributeLabel($this->attribute),
            'value' => $this->model->{$this->attribute},
        ];

        $options = ArrayHelper::merge($custom, $options);
        $options['password'] = false;

        $this->parts['{input}'] = StringGenerator::widget($options);

        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function passwordInput($options = [])
    {
        $this->template = $this->templatePasswordInput;
        $placeholder = $this->model->getAttributeLabel($this->attribute);

        $custom = [
            'placeholder' => $placeholder,
        ];

        $options = ArrayHelper::merge($custom, $options);

        return parent::passwordInput($options);
    }

    /**
     * @param array $options
     * @return $this
     */
    public function passwordInputGenerate($options = [])
    {
        $custom = [
            'id' => Html::getInputId($this->model, $this->attribute),
            'name' => Html::getInputName($this->model, $this->attribute),
            'placeholder' => $this->model->getAttributeLabel($this->attribute),
            'value' => $this->model->{$this->attribute},
        ];

        $options = ArrayHelper::merge($custom, $options);
        $options['password'] = true;

        $this->parts['{input}'] = StringGenerator::widget($options);

        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function emailInput($options = [])
    {
        $custom = [
            'id' => Html::getInputId($this->model, $this->attribute),
            'name' => Html::getInputName($this->model, $this->attribute),
            'placeholder' => $this->model->getAttributeLabel($this->attribute),
            'value' => $this->model->{$this->attribute},
        ];

        $options = ArrayHelper::merge($custom, $options);

        $this->parts['{input}'] = Email::widget($options);

        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function phoneInput($options = [])
    {
        $custom = [
            'id' => Html::getInputId($this->model, $this->attribute),
            'name' => Html::getInputName($this->model, $this->attribute),
            'placeholder' => $this->model->getAttributeLabel($this->attribute),
            'value' => $this->model->{$this->attribute},
        ];

        $options = ArrayHelper::merge($custom, $options);

        $this->parts['{input}'] = Phone::widget($options);

        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function textarea($options = [])
    {
        $placeholder = $this->model->getAttributeLabel($this->attribute);

        $custom = [
            'placeholder' => $placeholder,
        ];

        $options = ArrayHelper::merge($custom, $options);

        return parent::textarea($options);
    }

    /**
     * @param array $config
     * @param boolean $enclosedByLabel
     * @return string
     */
    public function checkbox($config = [], $enclosedByLabel = true)
    {
        $options['id'] = isset($config['id']) ? $config['id'] : Html::getInputId($this->model, $this->attribute);
        $options['name'] = isset($config['name']) ? $config['name'] : Html::getInputName($this->model, $this->attribute);
        $options['value'] = isset($config['value']) ? $config['value'] : Html::getAttributeValue($this->model, $this->attribute);
        $options['label'] = isset($config['label']) ? $config['label'] : $this->model->getAttributeLabel(Html::getAttributeName($this->attribute));

        $config = ArrayHelper::merge($config, $options);

        return Checkbox::widget($config);
    }

    /**
     * @param array $items
     * @param array $options
     * @return $this
     */
    public function select2List($items, $options = [])
    {
        $config = [
            'items' => $items,
        ];

        $options['id'] = isset($options['id']) ? $options['id'] : Html::getInputId($this->model, $this->attribute);
        $options['value'] = isset($options['value']) ? $options['value'] : $this->model{$this->attribute};

        if (isset($options['multiple']) && $options['multiple']) {
            $options['name'] = isset($options['name']) ? $options['name'] . '[]' : Html::getInputName($this->model, $this->attribute) . '[]';
            $options['multiple'] = 'multiple';
        } else {
            $options['name'] = isset($options['name']) ? $options['name'] : Html::getInputName($this->model, $this->attribute);
            $options['length'] = isset($options['length']) ? $options['length'] : -1;
        }

        $config = ArrayHelper::merge($config, $options);

        $this->parts['{input}'] = Select2::widget($config);

        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function datepicker($options = [])
    {
        $options['id'] = isset($options['id']) ? $options['id'] : Html::getInputId($this->model, $this->attribute);
        $options['value'] = isset($options['value']) ? $options['value'] : $this->model{$this->attribute};
        $options['name'] = isset($options['name']) ? $options['name'] : Html::getInputName($this->model, $this->attribute);
        $options['label'] = isset($options['label']) ? $options['label'] : $this->model->getAttributeLabel(Html::getAttributeName($this->attribute));

        $this->parts['{input}'] = Datepicker::widget($options);

        return $this;
    }

    public function render($content = null)
    {
        if ($content === null) {
            if (!isset($this->parts['{input}']) && $this->model instanceof FormModel) {
                $options = [];
                if (isset($this->model->gmapMapping[$this->attribute])){
                    $options['attributes'] = [
                        'data-gmap-address-component' => $this->model->gmapMapping[$this->attribute]
                    ];
                }
                switch ($this->model->getAttributeType($this->attribute)) {
                    case self::TYPE_INTEGER:
                        $this->textInput($options);
                        break;
                    case self::TYPE_STRING:
                        $this->textInput($options);
                        break;
                    case self::TYPE_TEXT:
                        $this->textarea($options);
                        break;
                    case self::TYPE_EMAIL:
                        $this->emailInput($options);
                        break;
                    case self::TYPE_PHONE:
                        $this->phoneInput($options);
                        break;
                }
            }
        }
        return parent::render($content); // TODO: Change the autogenerated stub
    }
}

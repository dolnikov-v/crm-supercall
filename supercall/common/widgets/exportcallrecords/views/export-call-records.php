<?php
use common\helpers\fonts\FontAwesome;
use common\widgets\base\Button;
use backend\modules\order\assets\ExportCallRecordsAsset;
use common\widgets\exportcallrecords\ExportCallRecords;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/** @var ExportCallRecords $widget */
/** @var string $place */
/** @var boolean $checkPlace */
ExportCallRecordsAsset::register($this);
?>

<?php if($checkPlace):?>

    <?php if($place == ExportCallRecords::PLACE_ORDER_LIST):?>

    <?=Button::widget([
        'classes' => yii::$app->user->can('order.index.exportcallrecords') ? ' ' : 'hidden ' . 'text-right export-call-record-btn',
        'style' => Button::STYLE_INFO,
        'label' => '<small>' . yii::t('common', 'Экспорт записей разговоров') . '</small>',
        'size' => Button::SIZE_SMALL,
        'icon' => FontAwesome::FILE_AUDIO,
        'id' => 'export-call-records-button'
    ]);?>

   <?php endif;?>

   <?php if($place == ExportCallRecords::PLACE_ORDER):?>

        <?=Button::widget([
            'classes' => yii::$app->user->can('order.index.exportcallrecords') ? ' ' : 'hidden ' . 'text-right export-call-record-btn',
            'style' => Button::STYLE_INFO,
            'label' => '<small>' . yii::t('common', 'Экспорт записей разговоров') . '</small>',
            'size' => Button::SIZE_SMALL,
            'icon' => FontAwesome::FILE_AUDIO,
            'id' => 'export-call-records-button'
        ]);?>

   <?php endif;?>

<?php else:?>

    <span class="text-danger"><?=yii::t('common','Некорректный параметр $place: {place}', ['place' => $place]);?></span>

<?php endif;?>


<?php Modal::begin([
    'id' => 'modal-confirm-export-call-records',
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4>' . yii::t('common', 'Подтверждение') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-success', 'id' => 'export-ok'])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'id' => 'export-cancel'])
]); ?>

<div class="row">
    <div class="col-lg-12"><?=yii::t('common', 'Вы действительно собираетесь выполнить данную операцию по экспорту записей разговоров?')?></div>
</div>

<?php Modal::end(); ?>

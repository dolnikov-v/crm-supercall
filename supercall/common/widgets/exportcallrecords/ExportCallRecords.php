<?php

namespace common\widgets\exportcallrecords;

use common\widgets\base\Widget;

/**
 * Class ExportCallRecords
 * @package common\widgets\exportcallrecords
 */
class ExportCallRecords extends Widget
{
    const PLACE_ORDER_LIST = 'orderList';
    const PLACE_ORDER = 'order';

    public $place;

    /**
     * @return array
     */
    public function getPlaceCollection()
    {
        return [
            self::PLACE_ORDER_LIST,
            self::PLACE_ORDER
        ];
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('export-call-records',
            [
                'widget' => $this,
                'place' => $this->place,
                'checkPlace' => in_array($this->place, $this->getPlaceCollection())
            ]
        );
    }
}
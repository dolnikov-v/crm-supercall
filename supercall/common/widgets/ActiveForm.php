<?php
namespace common\widgets;

use common\widgets\base\Button;
use common\widgets\base\ButtonLink;
use yii\helpers\ArrayHelper;

/**
 * Class ActiveForm
 * @package common\widgets
 */
class ActiveForm extends \yii\widgets\ActiveForm
{
    public $fieldClass = 'common\widgets\ActiveField';

    /**
     * @param array $config
     * @return static
     */
    public static function begin($config = [])
    {
        $form = parent::begin($config);

        if ($form->enableClientValidation) {
            $form->view->registerJs("
            jQuery('#" . $form->id . "').on('afterValidateAttribute', function (event, attribute, message) {
                if ($(attribute.input).length == 0){
                    return;
                }
                $.each(message, function(index, value) {
                    toastr.error(value);
                });
            });
            ");
        }
        if ($form->enableAjaxValidation){
            $form->view->registerJs("
            jQuery('#" . $form->id . "').on('ajaxComplete', function (event, xhr, status) {
                var json = JSON.parse(xhr.responseText);
                for (var i in json){
                    for (var j=0; j<json[i].length; j++){
                        if (jQuery('#'+i).length == 1){
                            toastr.error(json[i][j]);
                        }
                    }
                }
            });
            ");
        }

        return $form;
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     * @param array $options
     * @return ActiveField
     */
    public function field($model, $attribute, $options = [])
    {
        return parent::field($model, $attribute, $options);
    }

    /**
     * @param string $name
     * @return string
     */
    public function button($name)
    {
        return Button::widget([
            'label' => $name,
            'size' => Button::SIZE_SMALL,
        ]);
    }

    /**
     * @param string $name
     * @param array $options
     * @return string
     */
    public function submit($name, $options = [])
    {
        $config = [
            'type' => 'submit',
            'label' => $name,
        ];

        $config['size'] = isset($config['size']) ? $config['size'] : Button::SIZE_SMALL;
        $config['style'] = isset($config['style']) ? $config['style'] : Button::STYLE_SUCCESS;

        $config = ArrayHelper::merge($config, $options);

        return Button::widget($config);
    }

    /**
     * @param $name
     * @param $url
     * @return mixed
     */
    public function link($name, $url)
    {
        return ButtonLink::widget([
            'label' => $name,
            'url' => $url,
            'size' => ButtonLink::SIZE_SMALL
        ]);
    }
}

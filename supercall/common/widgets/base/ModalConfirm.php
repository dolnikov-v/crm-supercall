<?php
namespace common\widgets\base;

use Yii;

/**
 * Class ModalConfirm
 * @package common\widgets\base
 */
class ModalConfirm extends Modal
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal', [
            'id' => 'modal_confirm',
            'color' => self::COLOR_INFO,
            'title' => Yii::t('common', 'Подтверждение'),
            'description' => Yii::t('common', 'Вы действительно собираетесь выполнить данную операцию ?'),
            'body' => $this->body,
            'bodyStyle' => $this->bodyStyle,
            'footer' => $this->footer,
            'footerStyle' => $this->footerStyle,
            'labelOk' => $this->labelOk,
            'labelCancel' => $this->labelCancel,
        ]);
    }
}

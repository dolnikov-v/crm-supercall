<?php
namespace common\widgets\base;

/**
 * Class Select2
 * @package common\widgets\base
 */
class Select2 extends Widget
{
    const STYLE_PRIMARY = 'select2-primary';
    const STYLE_SUCCESS = 'select2-success';
    const STYLE_INFO = 'select2-info';
    const STYLE_WARNING = 'select2-waring';
    const STYLE_DANGER = 'select2-danger';

    /**
     * @var string Имя для select
     */
    public $name;

    /**
     * @var string Выбранное значение
     */
    public $value;

    /**
     * @var string
     */
    public $prompt;

    /**
     * @var array
     */
    public $items = [];

    /**
     * @var bool
     */
    public $multiple = false;

    /**
     * @var bool
     */
    public $disabled = false;

    /**
     * @var bool|string
     */
    public $placeholder = false;

    /**
     * @var bool|string
     */
    public $style = false;

    /**
     * @var bool|integer
     */
    public $length = false;

    /**
     * @var boolean
     */
    public $autoEllipsis = false;

    /**
     * @var bool
     */
    public $tags = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('select2', [
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'prompt' => $this->prompt,
            'items' => $this->items,
            'multiple' => $this->multiple,
            'disabled' => $this->disabled,
            'placeholder' => $this->placeholder,
            'style' => $this->style,
            'length' => $this->length,
            'autoEllipsis' => $this->autoEllipsis,
            'tags' => $this->tags,
        ]);
    }
}

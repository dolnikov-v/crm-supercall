<?php
namespace common\widgets\base;

/**
 * Class StringGenerator
 * @package common\widgets\base
 */
class StringGenerator extends Widget
{
    /** @var  string */
    public $value = '';

    /** @var string */
    public $label;

    /** @var string */
    public $name;

     /** @var string */
    public $placeholder;

    /** @var integer */
    public $length = 8;

    /** @var string */
    public $chars = '23456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz';

    /** @var bool */
    public $password = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('string-generator', [
            'id' => $this->id,
            'value' => $this->value,
            'name' => $this->name,
            'label' => $this->label,
            'length' => $this->length,
            'chars' => $this->chars,
            'password' => $this->password
        ]);
    }
}

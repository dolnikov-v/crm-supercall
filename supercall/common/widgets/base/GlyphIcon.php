<?php

namespace common\widgets\base;

use yii\base\Object;

/**
 * Class GlyphIcon
 * @package app\helpers
 */
class GlyphIcon extends Object
{
    const GIFT = 'glyphicon-gift';
    const EARPHONE = 'glyphicon-earphone';
    const PHONE_ALT = 'glyphicon-phone-alt';
    const GLOBE = 'glyphicon-globe';
    const CLOUD = 'glyphicon-cloud';
    const TASKS = 'glyphicon-tasks';
    const PICTURE = 'glyphicon-picture';
    const PHONE = 'glyphicon-phone';
    const PLUS = 'glyphicon-plus';
    const MINUS = 'glyphicon-minus';
    const REFRESH = 'glyphicon-refresh';
    const CLOSE = 'glyphicon-remove-circle';
    const EXPORT = 'glyphicon-export';
    const CLOCK = 'glyphicon-dashboard';
    const USER = 'glyphicon-user';
    const MESSAGE = 'glyphicon-envelope';
    const REPLY = 'glyphicon-share-alt';
}

<?php

namespace common\widgets\base;

use common\widgets\base\Dropdown;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\widgets\LinkPager as YiiLinkPager;

/**
 * Class LinkPager
 * @package app\widgets
 */
class LinkPager extends YiiLinkPager
{
    const COUNTER_POSITION_TOP = 'top';
    const COUNTER_POSITION_BOTTOM = 'bottom';
    const COUNTER_POSITION_LEFT = 'left';
    const COUNTER_POSITION_RIGHT = 'right';
    const COUNTER_POSITION_NONE = null;

    /**
     * @var string
     */
    public $counterSessionKey;

    /**
     * @var string|null
     */
    public $counterPosition = self::COUNTER_POSITION_NONE;

    /**
     * @var array
     */
    public static $counterVariants = [
        5,
        20,
        50,
        100,
        250,
        500
    ];

    protected static $defaultPageSize = 20;

    /**
     * @return string
     */
    public static function generateCounterSessionKey()
    {
        $key = str_replace('/', '.', preg_replace("/(\w+)\?.*/", "$1", Url::to()));
        $key = strtolower($key);
        $key = trim($key, '.');

        return 'LinkPager[' . $key . '][counter]';
    }

    /**
     * @param Pagination $pagination
     * @param string|null $sessionKey
     * @return integer
     */
    public static function setPageSize($pagination, $sessionKey = null)
    {
        if (is_null($sessionKey)) {
            $sessionKey = LinkPager::generateCounterSessionKey();
        }

        $pageSize = (int)Yii::$app->request->get($pagination->pageSizeParam);

        if ($pageSize) {
            Yii::$app->session->set($sessionKey, $pageSize);
        }

        $pageSize = (int)Yii::$app->session->get($sessionKey);

        $pagination->pageSize = $pageSize ? $pageSize : self::$defaultPageSize;
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (empty($this->counterSessionKey)) {
            $this->counterSessionKey = self::generateCounterSessionKey();
        }

        $pageSize = (int)Yii::$app->request->get($this->pagination->pageSizeParam);

        if ($pageSize) {
            Yii::$app->session->set($this->counterSessionKey, $pageSize);
        }

        if ($pageSize = Yii::$app->session->get($this->counterSessionKey)) {
            $this->pagination->pageSize = $pageSize;
        }
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected function renderPageButtons()
    {
        $buttons = parent::renderPageButtons();

        return $this->renderCounter($buttons);
    }

    /**
     * @param string $buttons
     * @return null|string
     * @throws \Exception
     */
    protected function renderCounter($buttons)
    {
        if ($this->pagination->totalCount == 0) {
            return '';
        }

        $result = $buttons;

        $links = [];
        foreach (LinkPager::$counterVariants as $variant) {

            $queryParams = Yii::$app->request->getQueryParams();
            $queryParams[$this->pagination->pageSizeParam] = $variant;
            $url = array_merge([''], $queryParams);

            $links[] = [
                'label' => $variant,
                'url' => Url::to($url),
            ];
        }

        switch ($this->counterPosition) {
            case self::COUNTER_POSITION_TOP:

                break;
            case self::COUNTER_POSITION_BOTTOM:

                break;
            case self::COUNTER_POSITION_LEFT:
                $dropdown = Dropdown::widget([
                    'label' => $this->pagination->pageSize,
                    'links' => $links,
                    'style' => Dropdown::STYLE_DEFAULT . ' btn-outline',
                    'styleDropdown' => 'link-pager-counter position-left',
                    'right' => true,
                    'dropup' => true,
                    'bullet' => true,
                ]);

                $result = $result . $dropdown;

                break;
            case self::COUNTER_POSITION_RIGHT:
                $dropdown = Dropdown::widget([
                    'label' => $this->pagination->pageSize,
                    'links' => $links,
                    'style' => Dropdown::STYLE_DEFAULT . ' btn-outline ' . Dropdown::SIZE_SMALL,
                    'styleDropdown' => 'link-pager-counter pull-right',
                    'right' => true,
                    'dropup' => true,
                    'bullet' => true,
                ]);

                $result = $dropdown . $result;

                break;
            case self::COUNTER_POSITION_NONE:

                break;
        }

        return $result;
    }

    public static function getPageSize()
    {
        if(!empty(Yii::$app->request->get('per-page')) && in_array(Yii::$app->request->get('per-page'), self::$counterVariants)){
            return Yii::$app->request->get('per-page');
        } else {
            if(in_array(Yii::$app->session->get(self::generateCounterSessionKey()), self::$counterVariants)){
                return Yii::$app->session->get(self::generateCounterSessionKey());
            }

            return self::$defaultPageSize;
        }

    }

    public static function getPaginationSizer()
    {
        return ['pageSize' => self::getPageSize()];
    }
}

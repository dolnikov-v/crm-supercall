<?php
namespace common\widgets\base;

/**
 * Class TextInput
 * @package common\widgets\base
 */
class TextInput extends Widget
{
    const SIZE_SMALL = 'input-sm';
    const SIZE_LARGE = 'input-lg';

    /**
     * @var string
     */
    public $type = 'text';

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $placeholder;

    /**
     * @var string
     */
    public $autocomplete;

    /**
     * @var string
     */
    public $size;

    /**
     * @var boolean
     */
    public $disabled = false;

    /**
     * @var bool
     */
    public $readonly = false;

    /**
     * @var boolean
     */
    public $focus = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('text-input', [
            'type' => $this->type,
            'name' => $this->name,
            'value' => $this->value,
            'placeholder' => $this->placeholder,
            'autocomplete' => $this->autocomplete,
            'size' => $this->size,
            'disabled' => $this->disabled,
            'readonly' => $this->readonly,
            'focus' => $this->focus,
            'id' => $this->id,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}

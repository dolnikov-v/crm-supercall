<?php
namespace common\widgets\base;

/**
 * Class InputText
 * @package common\widgets\base
 */
class InputText extends Widget
{
    const SIZE_SMALL = 'input-sm';
    const SIZE_LARGE = 'input-lg';

    /**
     * @var string
     */
    public $type = 'text';

    /**
     * @var
     */
    public $name;

    /**
     * @var
     */
    public $value;

    /**
     * @var string
     */
    public $placeholder;

    /**
     * @var
     */
    public $size;

    /**
     * @var boolean
     */
    public $disabled = false;

    /**
     * @var boolean
     */
    public $focus = false;

    /**
     * @var boolean
     */
    public $readonly = false;

    /**
     * @var string
     */
    public $hook = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('input-text', [
            'type' => $this->type,
            'name' => $this->name,
            'value' => $this->value,
            'placeholder' => $this->placeholder,
            'size' => $this->size,
            'disabled' => $this->disabled,
            'focus' => $this->focus,
            'readonly' => $this->readonly,
            'id' => $this->id,
            'hook' => $this->hook,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}

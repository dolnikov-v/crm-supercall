<?php
namespace common\widgets\base;

/**
 * Class Checkbox
 * @package common\widgets\base
 */
class Checkbox extends Widget
{
    const STYLE_DEFAULT = '';
    const STYLE_PRIMARY = 'checkbox-primary';
    const STYLE_SUCCESS = 'checkbox-success';
    const STYLE_INFO = 'checkbox-info';
    const STYLE_WARNING = 'checkbox-warning';
    const STYLE_DANGER = 'checkbox-danger';

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $value;

    /** @var string */
    public $unchecked_value = null;

    /**
     * @var string
     */
    public $style = self::STYLE_SUCCESS;

    /**
     * @var string
     */
    public $label;

    /**
     * @var boolean
     */
    public $checked = false;

    /**
     * @var boolean
     */
    public $disabled = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('checkbox', [
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'unchecked_value' => $this->unchecked_value,
            'style' => $this->style,
            'label' => $this->label,
            'checked' => $this->checked,
            'disabled' => $this->disabled,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}

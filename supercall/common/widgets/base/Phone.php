<?php
namespace common\widgets\base;

/**
 * Class Email
 * @package common\widgets\base
 */
class Phone extends \yii\base\Widget
{
    /** @var string */
    public $id;

    /** @var  string */
    public $value = '';

    /** @var string */
    public $label;

    /** @var string */
    public $name;

     /** @var string */
    public $placeholder;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('phone', [
            'id' => $this->id,
            'value' => $this->value,
            'name' => $this->name,
            'label' => $this->label,
            'placeholder' => $this->placeholder
        ]);
    }
}

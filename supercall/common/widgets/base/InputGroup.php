<?php
namespace common\widgets\base;

/**
 * Class InputGroup
 * @package common\widgets\base
 */
class InputGroup extends Widget
{
    /**
     * @var string
     * @var string
     */
    public $label;

    /**
     * @var InputText
     */
    public $input;

    /**
     * @var array
     */
    public $left = [];

    /**
     * @var array
     */
    public $right = [];

    /**
     * @return string
     * @throws \InvalidArgumentException
     */
    public function run()
    {
        return $this->render('input-group', [
            'label' => $this->label,
            'input' => $this->input,
            'left' => $this->left,
            'right' => $this->right,
        ]);
    }
}

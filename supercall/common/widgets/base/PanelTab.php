<?php
namespace common\widgets\base;

/**
 * Class PanelTab
 * @package common\widgets\base
 */
class PanelTab extends Widget
{
    /** @var string */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $content;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('panel-tab', [
            'id' => $this->id,
            'content' => $this->content,
            'title' => $this->title
        ]);
    }
}

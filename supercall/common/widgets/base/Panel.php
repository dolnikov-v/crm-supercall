<?php
namespace common\widgets\base;

/**
 * Class Panel
 * @package common\widgets\base
 */
class Panel extends Widget
{
    const BORDER_COLOR_GREEN = 'hgreen';
    const BORDER_COLOR_ORANGE = 'horange';
    const BORDER_COLOR_RED = 'hred';
    const BORDER_COLOR_YELLOW = 'hyellow';
    const BORDER_COLOR_VIOLET = 'hviolet';

    const ALERT_SUCCESS = 'alert-success';
    const ALERT_DANGER = 'alert-danger';
    const ALERT_WARNING = 'alert-warning';
    const ALERT_INFO = 'alert-info';

    /** @var string */
    public $id;

    /** @var string */
    public $title;

    /** @var  string */
    public $actions;

    /** @var string */
    public $borderColor = self::BORDER_COLOR_GREEN;

    /** @var boolean */
    public $showButtons = false;

    /** @var boolean */
    public $header = true;

    /** @var string */
    public $content;

    /** @var boolean */
    public $contentPadding = true;

    /** @var boolean Использовать ли блок panel-body */
    public $withBody = true;

    public $close;

    public $refresh;
    
    public $export;
    
    /**
     * Отображать или скрывать panel
     * @var bool */
    public $contentVisible = true;

    /** @var string */
    public $footer;

    /** @var string */
    public $alert;

    /** @var string */
    public $alertStyle = self::ALERT_SUCCESS;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('panel', [
            'id' => $this->id,
            'title' => $this->title,
            'actions' => $this->actions,
            'header' => $this->header,
            'contentPadding' => $this->contentPadding,
            'contentVisible' => $this->contentVisible,
            'content' => $this->content,
            'showButtons' => $this->showButtons,
            'borderColor' => $this->borderColor,
            'footer' => $this->footer,
            'alert' => $this->alert,
            'alertStyle' => $this->alertStyle,
            'withBody' => $this->withBody,
        ]);
    }
}

<?php
namespace common\widgets\base;

/**
 * Class Nav
 * @package common\widgets\base
 */
class Nav extends Widget
{
    /** @var array */
    public $tabs;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('nav', [
            'tabs' => $this->tabs
        ]);
    }
}

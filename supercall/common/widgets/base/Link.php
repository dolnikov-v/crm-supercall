<?php
namespace common\widgets\base;

/**
 * Class Link
 * @package common\widgets\base
 */
class Link extends Widget
{
    /**
     * @var string Ссылка
     */
    public $url;

    /**
     * @var string Основной класс
     */
    public $style;

    /**
     * @var string Текст
     */
    public $label;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('link', [
            'id' => $this->id,
            'url' => $this->url,
            'style' => $this->style,
            'label' => $this->label,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}

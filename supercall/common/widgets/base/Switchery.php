<?php
namespace common\widgets\base;

use Yii;

/**
 * Class Switchery
 * @package common\widgets\base
 */
class Switchery extends Widget
{
    const SIZE_MINI = 'mini';
    const SIZE_SMALL = 'small';
    const SIZE_NORMAL = 'normal';
    const SIZE_LARGE = 'large';

    const COLOR_PRIMARY = 'primary';
    const COLOR_INFO = 'info';
    const COLOR_SUCCESS = 'success';
    const COLOR_WARNING = 'warning';
    const COLOR_DEFAULT = 'default';

    /** @var string */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $style;

    /** @var boolean */
    public $checked = false;

    /** @var string */
    public $onText;

    /** @var string */
    public $offText;

    /** @var boolean */
    public $disabled = false;

    /** @var string */
    public $color = self::COLOR_SUCCESS;

    /** @var string */
    public $size = self::SIZE_SMALL;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->onText = $this->onText ? $this->onText : Yii::t('common', 'Вкл');
        $this->offText = $this->offText ? $this->offText : Yii::t('common', 'Откл');

        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('switchery', [
            'id' => $this->id,
            'name' => $this->name,
            'offText' => $this->offText,
            'onText' => $this->onText,
            'style' => $this->style,
            'color' => $this->color,
            'size' => $this->size,
            'checked' => $this->checked,
            'disabled' => $this->disabled,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}

<?php
namespace common\widgets\base;

/**
 * Class Dropdown
 * @package common\widgets\base
 */
class Dropdown extends Button
{
    /**
     * @var string
     */
    public $styleDropdown;

    /**
     * @var bool
     */
    public $withButton = true;

    /**
     * @var array Ссылки
     */
    public $links = [];

    /**
     * @var boolean
     */
    public $right = true;

    /**
     * @var string
     */
    public $style = 'btn-default';

    /**
     * @var boolean
     */
    public $dropup = false;

    /**
     * @var boolean
     */
    public $bullet = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('dropdown', [
            'type' => $this->type,
            'withButton' => $this->withButton,
            'styleDropdown' => $this->styleDropdown,
            'label' => $this->label,
            'style' => $this->style,
            'links' => $this->links,
            'right' => $this->right,
            'dropup' => $this->dropup,
            'bullet' => $this->bullet,
        ]);
    }
}

<?php
namespace common\widgets\base;

use Yii;

/**
 * Class ModalConfirmDelete
 * @package common\widgets\base
 */
class ModalConfirmDelete extends Modal
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal', [
            'id' => 'modal_confirm_delete',
            'color' => self::COLOR_DANGER,
            'title' => Yii::t('common', 'Подтверждение удаления'),
            'description' => Yii::t('common', 'Вы действительно хотите удалить данную запись?'),
            'body' => $this->body,
            'bodyStyle' => $this->bodyStyle,
            'footer' => $this->footer,
            'footerStyle' => $this->footerStyle,
            'labelOk' => Yii::t('common', 'Удалить'),
            'labelCancel' => $this->labelCancel,
        ]);
    }
}

<?php
namespace common\widgets\base;

/**
 * Class DropdownActions
 * @package common\widgets\base
 */
class DropdownActions extends Dropdown
{
    /**
     * @var array Ссылки
     */
    public $links = [];

    /**
     * @var string
     */
    public $style = 'btn-default';

    /**
     * @var string
     */
    public $size = Dropdown::SIZE_SMALL;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('dropdown-actions', [
            'style' => $this->style,
            'size' => $this->size,
            'links' => $this->links,
        ]);
    }
}

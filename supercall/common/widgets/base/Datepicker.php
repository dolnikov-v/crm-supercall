<?php
namespace common\widgets\base;

use common\helpers\i18n\DateTimePicker;

/**
 * Class Datepicker
 * @package common\widgets\base
 */
class Datepicker extends Widget
{
    /**
     * @var string Имя
     */
    public $name;

    /**
     * @var string Выбранное значение
     */
    public $value;

    /** @var string */
    public $label;

    /**
     * @var bool
     */
    public $disabled = false;

    /**
     * @var bool|string
     */
    public $placeholder = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('datepicker', [
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value,
            'disabled' => $this->disabled,
            'placeholder' => $this->placeholder,
            'language' => DateTimePicker::getLocale()
        ]);
    }
}

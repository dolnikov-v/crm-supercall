<?php
/** @var string $id */
/** @var string $type Тип кнопки */
/** @var string $name Имя */
/** @var string $value Значение кнопки */
/** @var string $icon Иконка */
/** @var string $label Текст кнопки */
/** @var string $style Основной класс */
/** @var string $size Размер */
/** @var string $classes Дополнительные классы */
/** @var string $attributes Атрибуты */
?>

<button <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
        class="btn <?= $style ?> <?= $size ?> <?= $classes ?>"
        type="<?= $type ?>"
        <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
        <?php if ($value): ?>value="<?= $value ?>"<?php endif; ?>
        <?= $attributes ?>>
    <?php if ($icon): ?>
        <i class="icon <?= $icon ?>"></i>
    <?php endif; ?>
    <?= $label ?>
</button>

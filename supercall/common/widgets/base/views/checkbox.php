<?php
/** @var string $id */
/** @var string $name */
/** @var string $value */
/** @var string $unchecked_value */
/** @var string $style */
/** @var string $label */
/** @var string $checked */
/** @var string $disabled */
/** @var array $attributes */
?>

<div class="checkbox <?= $style ?>">
    <?php if ($unchecked_value !== null){
        echo \yii\helpers\Html::hiddenInput($name, $unchecked_value);
    } ?>
    <input type="checkbox"
           <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
           <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
           <?php if ($value): ?>value="<?= $value ?>"<?php endif; ?>
           <?php if ($checked): ?>checked="checked"<?php endif; ?>
           <?php if ($disabled): ?>disabled="disabled"<?php endif; ?>
        <?= $attributes ?>/>

    <?php if ($label): ?>
        <label <?php if ($id): ?>for="<?= $id ?>"<?php endif; ?>><?= $label ?></label>
    <?php endif; ?>
</div>

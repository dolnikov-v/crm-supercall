<?php
/** @var string $title */
/** @var string $breadcrumbs */
?>

<div class="normalheader small-header">
    <div class="hpanel">
        <div class="panel-body">

            <?= $breadcrumbs ?>

            <h2 class="font-light m-b-xs">
                <?= $title ?>
            </h2>
        </div>
    </div>
</div>

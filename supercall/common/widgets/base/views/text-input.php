<?php
/** @var string $type */
/** @var string $name */
/** @var string $value */
/** @var boolean $disabled */
/** @var boolean $readonly */
/** @var string $placeholder */
/** @var string $autocomplete */
/** @var string $size */
/** @var boolean $focus */
/** @var string $id */
/** @var string $attributes */
?>

<input class="form-control <?= $size ?> <?php if ($focus): ?>focus<?php endif; ?>"
       <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
       <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
       <?php if ($placeholder): ?>placeholder="<?= $placeholder ?>"<?php endif; ?>
       <?php if ($autocomplete): ?>autocomplete="<?= $autocomplete ?>"<?php endif; ?>
       <?php if (isset($value)): ?>value="<?= $value ?>"<?php endif; ?>
       <?php if ($disabled): ?>disabled="disabled"<?php endif; ?>
       <?php if ($readonly): ?>readonly="readonly"<?php endif; ?>
       type="<?= $type ?>"
    <?= $attributes ?>>

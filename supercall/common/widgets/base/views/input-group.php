<?php
/** @var string $label */
/** @var string $input */
/** @var array $left */
/** @var array $right */
?>

<div class="input-group">
    <?php foreach ($left as $item): ?>
        <span class="input-group-btn">
            <?= $item ?>
        </span>
    <?php endforeach; ?>

    <div class="form-control-wrap">
        <?= $input ?>
    </div>

    <?php foreach ($right as $item): ?>
        <span class="input-group-btn">
            <?= $item ?>
        </span>
    <?php endforeach; ?>
</div>

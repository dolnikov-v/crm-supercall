<?php
/** @var string $content */
/** @var string $id */
?>
<div class="panel">
    <div class="panel-heading" role="tab">
        <a class="panel-title collapsed" data-toggle="collapse" href="#<?= $id ?>"
           data-parent="#panelGroup" aria-expanded="false" aria-controls="<?= $id ?>">
            <?= $title ?>
        </a>
    </div>
    <div class="panel-collapse collapse" id="<?= $id ?>" aria-labelledby="exampleHeadingDefaultOne"
         role="tabpanel" aria-expanded="false" style="height: 0px;">
        <div class="panel-body">
            <?= $content; ?>
        </div>
    </div>
</div>

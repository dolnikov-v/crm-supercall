<?php
/** @var string $label */
/** @var string $style */
/** @var string $size */
/** @var boolean $round */
/** @var boolean $outline */
?>

<span
    class="label <?= $style ?> <?= $size ?> <?php if ($round): ?>label-round<?php endif; ?> <?php if ($outline): ?>label-outline<?php endif; ?>">
    <?= $label ?>
</span>

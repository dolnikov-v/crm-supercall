<?php
use common\assets\vendor\SwitcheryAsset;

/** @var string $id */
/** @var string $name */
/** @var string $onText */
/** @var string $offText */
/** @var string $checked */
/** @var string $color */
/** @var string $size */
/** @var string $style */
/** @var string $disabled */
/** @var array $attributes */

SwitcheryAsset::register($this);
?>

<input type="checkbox"
       data-type-checkbox="switchery"
       data-size="<?= $size ?>"
       data-on-color="<?= $color ?>"
       data-off-text="<?= $offText ?>"
       data-on-text="<?= $onText ?>"
    <?php if ($style): ?> class="<?= $style ?>"<?php endif; ?>
    <?php if ($name): ?> name="<?= $name ?>"<?php endif; ?>
    <?php if ($checked): ?> checked="checked"<?php endif; ?>
    <?php if ($disabled): ?> disabled="disabled"<?php endif; ?>
    <?= $attributes ?>/>

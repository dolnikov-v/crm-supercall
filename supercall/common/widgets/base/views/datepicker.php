<?php
use common\assets\vendor\DatepickerAsset;

/** @var string $id */
/** @var string $name */
/** @var string $value */
/** @var boolean $disabled */
/** @var string $language */

DatepickerAsset::register($this);
?>
<input type="text"
       class="form-control"
       <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
       <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
       <?php if ($value): ?>value="<?= $value ?>"<?php endif; ?>
       <?php if ($disabled): ?>disabled<?php endif; ?>
        <?php if ($language): ?>data-date-language="<?= $language ?>"<?php endif; ?>
       data-provide="datepicker" />
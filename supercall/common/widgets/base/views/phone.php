<?php
use common\helpers\fonts\FontAwesome;
use common\widgets\base\Button;
use common\widgets\base\InputGroup;
use common\widgets\base\InputText;

/** @var string $id */
/** @var string $name */
/** @var string $value */
/** @var string $label */
/** @var string $placeholder */

?>

<?= InputGroup::widget([
    'input' => InputText::widget([
        'id' => $id,
        'name' => $name,
        'value' => $value,
        'placeholder' => $placeholder
    ]),
    'right' => [
        Button::widget([
            'classes' => 'btn-phone',
            'icon' => FontAwesome::PHONE,
        ]),
    ],
])

?>

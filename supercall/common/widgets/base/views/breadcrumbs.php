<?php
/** @var array $links */
?>

<?php if (!empty($links)): ?>
    <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
            <?php foreach ($links as $key => $link): ?>
                <?php if (isset($link['url'])): ?>
                    <li>
                        <a href="<?= $link['url'] ?>">
                            <?= $link['label'] ?>
                        </a>
                    </li>
                <?php else: ?>
                    <li class="active">
                        <span><?= $link['label'] ?></span>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ol>
    </div>
<?php endif; ?>


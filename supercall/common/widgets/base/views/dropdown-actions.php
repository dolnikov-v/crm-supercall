<?php
use common\widgets\base\Link;

/** @var string $style Основной класс */
/** @var string $size Размер */
/** @var array $links Ссылки */
?>

<?php if (!empty($links)): ?>
    <div class="btn-group dropdown-actions">
        <button class="btn btn-default btn-flat btn-sm" type="button" data-toggle="dropdown">
            <?= Yii::t('common', 'Действия') ?>
        </button>

        <button class="btn btn-default btn-flat btn-sm dropdown-toggle" aria-expanded="false" type="button"
                data-toggle="dropdown">
            <span class="caret"></span>
            <span class="sr-only"><?= Yii::t('common', 'Действия') ?></span>
        </button>

        <ul class="dropdown-menu dropdown-menu-right">
            <?php foreach ($links as $link): ?>
                <?php if (is_array($link)): ?>
                    <?php if (isset($link['url'])): ?>
                        <?php $class = (isset($link['active'])) ? 'active' : '' ?>
                        <li class="<?= $class ?>">
                            <?= Link::widget([
                                'url' => $link['url'],
                                'label' => $link['label'],
                                'style' => isset($link['style']) ? $link['style'] : '',
                                'attributes' => isset($link['attributes']) ? $link['attributes'] : [],
                            ]) ?>
                        </li>
                    <?php else: ?>
                        <li class="dropdown-header"><?= $link['label'] ?></li>
                    <?php endif; ?>
                <?php else: ?>
                    <li class="divider"></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<?php
/** @var string $id */
/** @var string $actions */
/** @var boolean $showButtons */
/** @var string $borderColor */
/** @var boolean $header */
/** @var string $content */
/** @var boolean $contentPadding */
/** @var array $footer */
/** @var string $alert */
/** @var string $alertStyle */
?>

<div <?php if ($id): ?> id="<?= $id ?>" <?php endif; ?> class="hpanel <?= $borderColor ?> <?= $contentVisible ? '' : ' panel-collapse' ?>">
    <?php if ($header): ?>
        <div class="panel-heading hbuilt">
            <?php if ($showButtons): ?>
                <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-<?= $contentVisible ? 'up' : 'down' ?>"></i></a>
                </div>
            <?php endif; ?>
            <?= $title ?>
            <?php if ($actions) : ?>
                <div class="panel-actions">
                    <?= $actions ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php if (trim($alert)): ?>
        <div class="alert <?= $alertStyle ?>">
            <?= $alert ?>
        </div>
    <?php endif; ?>

    <div class="panel-body <?= $contentPadding ? '' : 'no-padding' ?>" <?= $contentVisible ? '' : 'style="display:none"' ?> >
        <?= $content ?>
    </div>

    <?php if (trim($footer)): ?>
        <div class="panel-footer">
            <?= $footer ?>
        </div>
    <?php endif; ?>
</div>

<?php if ($showButtons): ?>
    <div class="filter-string"></div>
<?php endif; ?>

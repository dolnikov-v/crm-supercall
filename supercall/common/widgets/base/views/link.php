<?php
/** @var string $id ID */
/** @var string $url Сылка */
/** @var string $label Текст кнопки */
/** @var string $style Основной класс */
/** @var string $attributes Атрибуты */
?>
<a <?= $id ? 'id="' . $id . '"' : '' ?> href="<?= $url ?>" class="<?= $style ?>" <?= $attributes ?>><?= $label ?></a>

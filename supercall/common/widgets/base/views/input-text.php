<?php
/** @var string $type */
/** @var string $name */
/** @var string $value */
/** @var bool $disabled */
/** @var string $placeholder */
/** @var string $size */
/** @var bool $focus */
/** @var bool $readonly */
/** @var bool $hook */
/** @var string $id */
/** @var string $attributes */
?>

<input class="form-control <?= $size ?> <?php if ($focus): ?>focus<?php endif; ?>"
       <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
       <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
       <?php if ($placeholder): ?>placeholder="<?= $placeholder ?>"<?php endif; ?>
       <?php if ($value): ?>value="<?= $value ?>"<?php endif; ?>
       <?php if ($disabled): ?>disabled="disabled"<?php endif; ?>
       <?php if ($readonly): ?>readonly="readonly"<?php endif; ?>
       <?php if ($hook): ?>autocomplete="off" readonly onfocus="this.removeAttribute('readonly')" <?php endif; ?>
    <?= $attributes ?>
       type="<?= $type ?>">

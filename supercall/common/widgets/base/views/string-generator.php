<?php
use common\assets\widgets\base\StringGeneratorAsset;
use common\helpers\fonts\FontAwesome;
use common\widgets\base\Button;
use common\widgets\base\InputGroup;
use common\widgets\base\InputText;

/** @var string $id */
/** @var string $name */
/** @var string $value */
/** @var string $label */
/** @var integer $length */
/** @var integer $chars */
/** @var boolean $password */

StringGeneratorAsset::register($this);
?>

<?= InputGroup::widget([
    'input' => InputText::widget([
        'id' => $id,
        'name' => $name,
        'value' => $value,
        'hook' => $password ? true : false,
        'attributes' => [
            'type' => $password ? 'password' : 'text'
        ]
    ]),
    'left' => [
        Button::widget([
            'icon' => FontAwesome::RANDOM,
            'attributes' => [
                'data-widget' => 'string-generator',
                'data-generator-length' => $length,
                'data-generator-chars' => $chars
            ],
        ]),
    ],
    'right' => [
        $password ? Button::widget([
            'icon' => FontAwesome::EYE,
            'attributes' => [
                'data-widget' => 'password-showing'
            ],
        ]) : '',
    ],
])

?>

<?php
use common\assets\widgets\ModalConfirmDeleteAsset;
use common\widgets\base\Button;
use common\widgets\base\ButtonLink;

/** @var string $id */
/** @var string $title */
/** @var string $description */
/** @var string $color */
/** @var string $body */
/** @var string $bodyStyle */
/** @var string $footer */
/** @var string $footerStyle */
/** @var string $labelOk */
/** @var string $labelCancel */

ModalConfirmDeleteAsset::register($this);

?>

<div class="modal fade <?= $color ?>" id="<?= $id ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <?php if ($title || $description) : ?>
                <div class="modal-header text-center">
                    <h4 class="modal-title"><?= $title ?></h4>
                    <small class="font-bold"><?= $description ?></small>
                </div>
            <?php endif; ?>
            <?php if ($body): ?>
                <div class="modal-body <?= $bodyStyle ?>">
                    <?= $body ?>
                </div>
            <?php endif; ?>
            <?php if ($footer): ?>
                <?= $footer ?>
            <?php else: ?>
                <div class="modal-footer <?= $footerStyle ?>">
                    <?= ButtonLink::widget([
                        'id' => 'modal_confirm_delete_link',
                        'label' => Yii::t('common', $labelOk),
                        'url' => '#',
                        'style' => Button::STYLE_PRIMARY,
                        'size' => Button::SIZE_SMALL,
                    ]) ?>

                    <?= Button::widget([
                        'label' => Yii::t('common', $labelCancel),
                        'size' => Button::SIZE_SMALL,
                        'attributes' => [
                            'data-dismiss' => 'modal',
                        ],
                    ]) ?>

                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

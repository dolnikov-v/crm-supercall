<div class="hpanel">
    <ul class="nav nav-tabs">
        <?php foreach ($tabs as $key => $tab): ?>
            <li class="<?php if ($key == 0) : ?>active<?php endif; ?>">
                <a data-toggle="tab" href="#tab-<?= $key + 1 ?>"><?= $tab['label'] ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
    <div class="tab-content">
        <?php foreach ($tabs as $key => $tab): ?>
            <div id="tab-<?= $key + 1 ?>" class="tab-pane <?php if ($key == 0) : ?>active<?php endif; ?>">
                <div class="panel-body">
                    <?= $tab['content'] ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

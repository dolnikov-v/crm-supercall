<?php
use common\widgets\base\Link;

/** @var string $type Тип кнопки */
/** @var string $withButton С кнопкой */
/** @var string $label Текст кнопки */
/** @var string $style Основной класс */
/** @var array $links Ссылки */
/** @var array $right C права */
/** @var array $dropup Открытие вверх */
/** @var array $bullet */
/** @var string $styleDropdown */
?>

<div class="dropdown <?= $styleDropdown ?> <?php if ($right): ?>btn-group<?php endif; ?> <?php if ($dropup): ?>dropup<?php endif; ?>">

    <?php if ($withButton): ?>
        <button type="<?= $type ?>"
                class="btn <?= $style ?> <?php if ($right): ?>pull-right<?php endif; ?> dropdown-toggle"
                data-toggle="dropdown">
            <?= $label ?>
            <span class="caret"></span>
        </button>
    <?php else: ?>
        <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
            <?= $label ?>
            <span class="caret"></span>
        </a>
    <?php endif; ?>

    <?php if (!empty($links)): ?>
        <ul class="dropdown-menu <?php if ($right): ?>dropdown-menu-right<?php endif; ?> <?php if ($bullet): ?>bullet<?php endif; ?>">
            <?php foreach ($links as $link): ?>

                <?php if (is_array($link)): ?>

                    <?php if (isset($link['url'])): ?>
                        <?php $class = (isset($link['active'])) ? 'active' : '' ?>
                        <li class="<?= $class ?>">
                            <?= Link::widget([
                                'url' => $link['url'],
                                'label' => $link['label'],
                                'style' => isset($link['style']) ? $link['style'] : '',
                                'attributes' => isset($link['attributes']) ? $link['attributes'] : [],
                            ]) ?>
                        </li>
                    <?php else: ?>
                        <li class="dropdown-header"><?= $link['label'] ?></li>
                    <?php endif; ?>

                <?php else: ?>
                    <li class="divider"></li>
                <?php endif; ?>

            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>

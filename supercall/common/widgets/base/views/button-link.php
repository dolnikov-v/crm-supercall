<?php
/** @var string $id ID */
/** @var string $icon Иконка */
/** @var string $label Текст кнопки */
/** @var string $url Сылка */
/** @var string $style Основной класс */
/** @var string $size Размер */
/** @var boolean $isBlock btn-block */
/** @var string $attributes Атрибуты */
?>
<a <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
   href="<?= $url ?>"
   class="btn <?= $style ?> <?= $size ?> <?php if ($isBlock): ?>btn-block<?php endif; ?>" <?= $attributes ?>>
    <?= $icon ?> <?= $label ?>
</a>

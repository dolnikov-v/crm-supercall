<?php
use common\assets\vendor\Select2Asset;

/** @var string $id */
/** @var string $name */
/** @var string|array $value */
/** @var string $prompt */
/** @var array $items */
/** @var boolean $multiple */
/** @var boolean $disabled */
/** @var string $placeholder */
/** @var string $style */
/** @var integer $length */
/** @var boolean $autoEllipsis */

Select2Asset::register($this);

$prompt_selected = empty($value) && !is_numeric($value);

if (!is_array($value)) {
    $value = (array)$value;
}

?>
<div class="<?= $style ?>">
    <select class="form-control <?php if ($autoEllipsis): ?>select2-auto-ellipsis<?php endif; ?>"
            <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?>
            <?php if ($name): ?>name="<?= $name ?>"<?php endif; ?>
            <?php if ($multiple): ?>multiple<?php endif; ?>
            <?php if ($disabled): ?>disabled<?php endif; ?>
            <?php if ($placeholder): ?>data-placeholder="<?= $placeholder ?>"<?php endif; ?>
            <?php if ($length): ?>data-minimum-results-for-search="<?= $length ?>"<?php endif; ?>
            <?php if ($tags): ?>data-tags="true"  data-token-separators=","<?php endif; ?>
            data-plugin="select2">

        <?php if ($prompt): ?>
            <option value="" <?= $prompt_selected ? 'selected' : '' ?>><?= $prompt ?></option>
        <?php endif; ?>

        <?php foreach ($items as $key => $item): ?>
            <?php if (is_array($item)): ?>
            <optgroup label="<?= $key ?>">
                <?php foreach ($item as $sub_key => $sub_item): ?>
                    <option value="<?= $sub_key ?>"
                            <?php if (in_array($sub_key, $value)): ?>selected="selected"<?php endif; ?>><?= $sub_item ?></option>
                <?php endforeach; ?>
            <?php else: ?>
                <option value="<?= $key ?>"<?php if (in_array($key, $value) && !$prompt_selected): ?>selected="selected"<?php endif; ?>><?= $item ?></option>
            <?php endif; ?>

        <?php endforeach; ?>
    </select>
</div>

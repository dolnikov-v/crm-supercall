<?php
namespace common\widgets\base;

/**
 * Class PanelHeader
 * @package common\widgets\base
 */
class PanelHeader extends Widget
{
    /** @var string */
    public $title = '';

    /** @var string Хребные крошки */
    public $breadcrumbs = '';

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('panel-header', [
            'title' => $this->title,
            'breadcrumbs' => $this->breadcrumbs,
        ]);
    }

}

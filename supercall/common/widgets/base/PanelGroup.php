<?php
namespace common\widgets\base;

/**
 * Class PanelGroup
 * @package common\widgets\base
 */
class PanelGroup extends Widget
{
    /** @var string */
    public $id;

    /** @var PanelTab[] $panels */
    public $panels = [];

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('@common/widgets/base/views/panel-group', [
            'panels' => $this->panels,
            'id' => $this->id,
        ]);
    }
}

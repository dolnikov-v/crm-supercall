<?php
namespace common\widgets\base;

/**
 * Class Widget
 * @package common\widgets\base
 */
class Widget extends \yii\base\Widget
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var array Атрибуты
     */
    public $attributes = [];

    /**
     * @return string
     */
    protected function prepareAttributes()
    {
        $attributes = array_map(function ($value, $name) {
            return '' . $name . '="' . $value . '"';
        }, $this->attributes, array_keys($this->attributes));

        return implode(' ', $attributes);
    }
}

<?php
namespace common\widgets\base;

/**
 * Class ButtonLink
 * @package common\widgets\base
 */
class ButtonLink extends Button
{
    /**
     * @var string Ссылка
     */
    public $url;

    /**
     * @var string Основной класс
     */
    public $style = self::STYLE_DEFAULT;

    /**
     * @var boolean btn-block
     */
    public $isBlock = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('button-link', [
            'id' => $this->id,
            'url' => $this->url,
            'style' => $this->style,
            'size' => $this->size,
            'isBlock' => $this->isBlock,
            'icon' => $this->icon,
            'label' => $this->label,
            'attributes' => $this->prepareAttributes(),
        ]);
    }
}

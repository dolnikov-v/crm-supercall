<?php
namespace common\widgets\base;

/**
 * Class Breadcrumbs
 * @package common\widgets\base
 */
class Breadcrumbs extends Widget
{
    /**
     * @var array Набор ссылок
     */
    public $links = [];

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('breadcrumbs', [
            'links' => $this->links,
        ]);
    }
}

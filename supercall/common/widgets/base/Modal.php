<?php
namespace common\widgets\base;

use Yii;

/**
 * Class Modal
 * @package common\widgets\base
 */
class Modal extends Widget
{
    const COLOR_PRIMARY = 'hmodal-default';
    const COLOR_SUCCESS = 'hmodal-success';
    const COLOR_DANGER = 'hmodal-danger';
    const COLOR_WARNING = 'hmodal-warning';
    const COLOR_INFO = 'hmodal-info';

    /** @var string */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var  string */
    public $color;

    /** @var string */
    public $body = '';

    /** @var string */
    public $bodyStyle;

    /** @var string */
    public $footer = '';

    /** @var string */
    public $footerStyle;

    /** @var  string */
    public $labelOk;

    /** @var  string */
    public $labelCancel;
                    

    public function init()
    {
        $this->title = $this->title ? $this->title : Yii::t('common', 'Подтвердить действие?');
        $this->labelOk = $this->labelOk ? $this->labelOk : Yii::t('common', 'Принять');
        $this->labelCancel = $this->labelCancel ? $this->labelCancel : Yii::t('common', 'Отменить');

        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal', [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'color' => $this->color,
            'body' => $this->body,
            'bodyStyle' => $this->bodyStyle,
            'footer' => $this->footer,
            'footerStyle' => $this->footerStyle,
            'labelOk' => $this->labelOk,  
            'labelCancel' => $this->labelCancel,
        ]);
    }
}

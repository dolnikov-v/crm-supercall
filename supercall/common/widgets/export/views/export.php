<?php

/* @var $widget phpnt\exportFile\ExportFile */

use common\helpers\fonts\FontAwesome;
use common\widgets\base\Dropdown;
use common\widgets\base\GlyphIcon;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

?>

    <div class="export_btn <?= $widget->blockClass ?>" style="<?= $widget->blockStyle ?>">

        <?= Html::beginForm(['/'], 'post', [
            'target' => '_blank'
        ]); ?>

        <?= Dropdown::widget([
            'label' => Html::tag('span', '', ['class' => GlyphIcon::EXPORT]) . ' ' . Html::tag('small', Yii::t('common', 'Export')),
            'withButton' => true,
            'links' => [
                [
                    'label' => Html::tag('span', '', ['class' => FontAwesome::FILE_EXCEL_O]) . ' ' . Yii::t('common', 'Excel'),
                    'url' => Url::to('#'),
                    'style' => $widget->xls ? 'export_to' : 'hidden',
                    'attributes' => [
                        'data-url' => $widget->controllerPathWithAction.'/excel',
                        'data-type' => 'excel'
                    ]
                ],
                [
                    'label' => Html::tag('span', '', ['class' => FontAwesome::FILE_PDF_O]) . ' ' . Yii::t('common', 'Pdf'),
                    'url' => Url::to('#'),
                    'style' => $widget->pdf ? 'export_to' : 'hidden',
                    'attributes' => [
                        'data-url' =>  $widget->controllerPathWithAction.'/pdf',
                        'data-type' => 'pdf'
                    ]
                ],
                [
                    'label' => Html::tag('span', '', ['class' => FontAwesome::FILE_TEXT]) . ' ' . Yii::t('common', 'Csv [comma]'),
                    'url' => Url::to('#'),
                    'style' => $widget->csv ? 'export_to' : 'hidden',
                    'attributes' => [
                        'data-url' => $widget->controllerPathWithAction.'/csv-comma',
                        'data-type' => 'csv comma'
                    ]
                ],
                [
                    'label' => Html::tag('span', '', ['class' => FontAwesome::FILE_TEXT]) . ' ' . Yii::t('common', 'Csv [semicolon]'),
                    'url' => Url::to('#'),
                    'style' => $widget->csv ? 'export_to' : 'hidden',
                    'attributes' => [
                        'data-url' =>  $widget->controllerPathWithAction.'/csv-semicolon',
                        'data-type' => 'csv semicolon'
                    ]
                ],
                [
                    'label' => Html::tag('span', '', ['class' => FontAwesome::FILE_WORD_O]) . ' ' . Yii::t('common', 'Word'),
                    'url' => Url::to('#'),
                    'style' => $widget->word ? 'export_to' : 'hidden',
                    'attributes' => [
                        'data-url' =>  $widget->controllerPathWithAction.'/word',
                        'data-type' => 'word'
                    ]
                ],
                [
                    'label' => Html::tag('span', '', ['class' => FontAwesome::FILE_TEXT]) . ' ' . Yii::t('common', 'Html'),
                    'url' => Url::to('#'),
                    'style' => $widget->html ? 'export_to' : 'hidden',
                    'attributes' => [
                        'data-url' =>  $widget->controllerPathWithAction.'/html',
                        'data-type' => 'html'
                    ]
                ]
            ]
        ]);
        ?>

        <?= Html::hiddenInput('title', $widget->title); ?>
        <?= Html::hiddenInput('model', $widget->model); ?>
        <?= Html::hiddenInput('getAll', $widget->getAll); ?>
        <?= Html::hiddenInput('queryParams', $widget->queryParams); ?>
        <?= Html::endForm(); ?>
    </div>

<?php Modal::begin([
    'id' => 'orders_export',
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4></h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Export'), [
        'class' => 'btn btn-default',
        'id' => 'orders_export_btn',
        'disabled' => true
    ])
]); ?>

    <div class="loader"></div>
    <div id="order_export_modals" class="center"></div>

<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'orders_export_error',
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4></h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' =>  Html::button(yii::t('common', 'Закрыть'), ['class' => 'btn btn-info', 'data-dismiss' => 'modal', 'id' => 'orders_export_error_close_btn',])
]); ?>

<div class="error"><?=yii::t('common', 'Установлен лимит в 64 000 строки при экспорте в Excel. Для экспорта большего кол-ва строк используйте формат CSV')?></div>

<?php Modal::end(); ?>

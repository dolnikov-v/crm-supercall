<?php

namespace common\widgets\export;

use phpnt\exportFile\ExportFile;

/**
 * Class OrderExport
 * @package common\widgets\export
 */
class Export extends ExportFile
{
    /** @var  string */
    public $controllerPathWithAction;

    public function run()
    {
        return $this->render('export',
            [
                'widget' => $this
            ]
        );
    }
}
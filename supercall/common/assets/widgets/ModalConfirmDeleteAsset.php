<?php
namespace common\assets\widgets;

use yii\web\AssetBundle;

/**
 * Class ModalConfirmDeleteAsset
 */
class ModalConfirmDeleteAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/web/resources/widgets/modal-confirm-delete';

    /**
     * @var array
     */
    public $js = [
        'modal-confirm-delete.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        //'backend\assets\base\ThemeAsset',
    ];
}

<?php
namespace common\assets\widgets;

use yii\web\AssetBundle;

/**
 * Class PhoneIcon
 */
class PhoneIcon extends AssetBundle
{
    public $sourcePath = '@common/web/resources/widgets/phone-icon';

    public $js = [
        'phone-icon.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}

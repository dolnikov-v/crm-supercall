<?php
namespace common\assets\widgets\base;

use yii\web\AssetBundle;

/**
 * Class StringGeneratorAsset
 * @package common\assets\widgets\base
 */
class StringGeneratorAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/widgets/base/string-generator';

    public $js = [
        'string-generator.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

<?php
namespace common\assets\vendor;

use common\helpers\i18n\DateTimePicker;
use Yii;
use yii\web\AssetBundle;

/**
 * Class DatepickerAsset
 * @package common\assets\vendor
 */
class DatepickerAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/datepicker';

    public $css = [
        'css/bootstrap-datepicker3.min.css',
    ];

    public $js = [
        'js/bootstrap-datepicker.min.js',
    ];

    public $depends = [
        'common\assets\vendor\BootstrapAsset',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($locale = DateTimePicker::getLocale()) {
            $path = 'locales/bootstrap-datepicker.' . $locale . '.min.js';
            $file = Yii::getAlias($this->sourcePath . '/' . $path);

            if (file_exists($file)) {
                $this->js[] = $path;
            }
        }

        parent::init();
    }
}

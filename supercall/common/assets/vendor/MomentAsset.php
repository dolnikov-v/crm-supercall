<?php
namespace common\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class MomentAsset
 * @package common\assets\vendor
 */
class MomentAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/moment';

    public $js = [
        'moment-with-locales.js',
    ];
}

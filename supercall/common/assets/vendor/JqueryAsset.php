<?php
namespace common\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class JqueryAsset
 * @package common\assets\vendor
 */
class JqueryAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/jquery';

    public $js = [
        'jquery.min.js',
    ];

    public $css = [

    ];
}

<?php
namespace common\assets\vendor;

use common\helpers\i18n\DateTimePicker;
use Yii;
use yii\web\AssetBundle;

/**
 * Class Select2Asset
 * @package common\assets\vendor
 */
class Select2Asset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/select2';

    public $css = [
        'select2.min.css',
        'select2.custom.css',
    ];

    public $js = [
        'select2.full.min.js',
        'select2.custom.js',
        'components/select2.min.js',
    ];

    public $depends = [
        'common\assets\base\ComponentsAsset',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($locale = DateTimePicker::getLocale()) {
            $path = 'i18n/' . $locale . '.js';
            $file = Yii::getAlias($this->sourcePath . '/' . $path);

            if (file_exists($file)) {
                $this->js[] = $path;
            }
        }

        parent::init();
    }
}

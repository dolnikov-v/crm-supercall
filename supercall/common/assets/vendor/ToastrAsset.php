<?php
namespace common\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class ToastrAsset
 * @package common\assets\vendor
 */
class ToastrAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/toastr';

    public $js = [
        'toastr.min.js',
        'toastr.custom.js',
    ];

    public $css = [
        'toastr.min.css',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

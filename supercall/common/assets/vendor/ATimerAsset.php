<?php
namespace common\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class ATimerAsset
 * @package common\assets\vendor
 */
class ATimerAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/atimer';

    public $js = [
        'jquery.atimer.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\MomentAsset',
    ];
}

<?php
namespace common\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class SwitcheryAsset
 * @package common\assets\vendor
 */
class SwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/switchery';

    public $css = [
        'bootstrap-switch.min.css'
    ];

    public $js = [
        'bootstrap-switch.min.js',
        'switchery.js',
    ];

    public $depends = [
    ];
}

<?php
namespace common\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class AsScrollableAsset
 * @package common\assets\vendor
 */
class AsScrollableAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/asscrollable';

    public $css = [
        'asScrollable.min.css'
    ];

    public $js = [
        'asScrollable.all.min.js',
        'components/asscrollable.min.js',
    ];

    public $depends = [
        'common\assets\base\ComponentsAsset',
    ];
}

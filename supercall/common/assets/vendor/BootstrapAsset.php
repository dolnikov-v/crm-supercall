<?php
namespace common\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class BootstrapAsset
 * @package common\assets\vendor
 */
class BootstrapAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/bootstrap';

    public $js = [
        'bootstrap.min.js',
        'notify.min.js',
    ];

    public $css = [
        'bootstrap.min.css',
        'bootstrap.custom.css',
        'awesome-checkbox.css',
        'awesome-checkbox.custom.css',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

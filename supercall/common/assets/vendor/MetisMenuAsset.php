<?php
namespace common\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class MetisMenuAsset
 * @package common\assets\vendor
 */
class MetisMenuAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/metisMenu';

    public $js = [
        'metisMenu.min.js',
    ];

    public $css = [
        'metisMenu.min.css',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

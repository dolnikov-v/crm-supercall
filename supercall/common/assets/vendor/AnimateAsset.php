<?php
namespace common\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class AnimateAsset
 * @package common\assets\vendor
 */
class AnimateAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/vendor/animate';

    public $js = [

    ];

    public $css = [
        'animate.min.css',
    ];
}

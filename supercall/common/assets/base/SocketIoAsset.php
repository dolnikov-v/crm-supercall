<?php

namespace common\assets\base;

use yii\web\AssetBundle;

/**
 * Class SocketIoAsset
 * @package common\assets\base
 */
class SocketIoAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/base/components/socket-io';

    public $js = [
        'socket.io.js',
    ];

}
<?php

namespace common\assets\base;

use yii\web\AssetBundle;

/**
 * Class KVSAsset
 * @package common\assets\base
 */
class KVSAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/base/kvs';

    public $js = [
        'kvs.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
<?php
namespace common\assets\base;

use yii\web\AssetBundle;

/**
 * Class ComponentsAsset
 * @package common\assets\base
 */
class ComponentsAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/base/components/';

    public $js = [
        'components.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

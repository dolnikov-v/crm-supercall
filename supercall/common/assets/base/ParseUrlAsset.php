<?php

namespace common\assets\base;

use yii\web\AssetBundle;

/**
 * Class ParseUrlAsset
 * @package common\assets\base
 */
class ParseUrlAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/base/';

    public $js = [
        'parse-url.js',
    ];
}
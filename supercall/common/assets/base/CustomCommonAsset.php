<?php

namespace common\assets\base;

use yii\web\AssetBundle;

/**
 * Class CustomCommonAsset
 * @package common\assets\base
 */
class CustomCommonAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/base/custom-common';

    public $js = [
        'custom-common.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
<?php
namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class ProfileCommonAsset
 * @package common\assets
 */
class ProfileCommonAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/profile/';

    public $js = [
        'profile-common.js',
    ];
}
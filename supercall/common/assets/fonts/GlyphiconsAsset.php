<?php
namespace common\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class GlyphiconsAsset
 * @package common\assets\fonts
 */
class GlyphiconsAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/fonts/glyphicons';

    public $css = [
        'glyphicons.css',
        'glyphicons.custom.css',
    ];
}

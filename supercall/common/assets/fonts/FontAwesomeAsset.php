<?php
namespace common\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class FontAwesomeAsset
 * @package common\assets\fonts
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/fonts/font-awesome';

    public $css = [
        'font-awesome.css',
        'font-awesome.custom.css',
    ];
}

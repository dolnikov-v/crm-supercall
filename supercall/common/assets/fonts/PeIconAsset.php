<?php
namespace common\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class PeIconAsset
 * @package common\assets\fonts
 */
class PeIconAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/fonts/pe-icon';

    public $css = [
        'pe-icon-7-stroke.css',
        'pe-icon-7-stroke.custom.css',
        'helper.css',
    ];
}

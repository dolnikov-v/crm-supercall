<?php
namespace common\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class OpenSansAsset
 * @package common\assets\fonts
 */
class OpenSansAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/fonts/open-sans';

    public $css = [
        'open-sans.css',
    ];
}

<?php
namespace common\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class FlagIconAsset
 * @package common\assets\fonts
 */
class FlagIconAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/fonts/flag-icon';

    public $css = [
        'flag-icon.min.css',
    ];
}

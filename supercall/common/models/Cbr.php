<?php
    
namespace common\models;

/**
 * Класс для сбора кура валют с сайта ЦБ РФ
 *
 * Class Cbr
 * @package common\models
 */
class Cbr
{
    protected $list = [];
    
    /**
     * Определяет корректность соединения с ЦБ РФ и получения значений
     *
     * @return bool
     */
    public function load()
    {
        $xml = new \DOMDocument();
        $url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=' . date('d.m.Y');
        
        if (@$xml->load($url))
        {
            $root = $xml->documentElement;
            $items = $root->getElementsByTagName('Valute');
            
            foreach ($items as $item)
            {
                $code = $item->getElementsByTagName('CharCode')->item(0)->nodeValue;
                $nominal = $item->getElementsByTagName('Nominal')->item(0)->nodeValue;
                $curs = $item->getElementsByTagName('Value')->item(0)->nodeValue;
                $this->list[ $code ] = [
                    'value'   => floatval(str_replace(',', '.', $curs)),
                    'nominal' => intval($nominal),
                ];
            }
            
            return true;
        }
        else
            return false;
    }
    
    /**
     * Возвращает значение курса валют
     *
     * @param $cur
     * @return int|mixed
     */
    public function get($cur)
    {
        return $this->list[$cur] ?? 0;
    }
    
}
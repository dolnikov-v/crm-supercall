<?php
namespace common\models;

use backend\modules\access\models\AuthAssignment;
use Yii;
use yii\helpers\Json;
use yii\httpclient\Client;

class Asterisk
{

    /**
     * @param $data
     */
    public static function addUser($data)
    {
        $user = User::findOne(['username' => $data['username']]);

        $client = new Client();

        if (yii::$app->params['is_dev']) {
            $login = yii::$app->params['AsteriskSp_dev_prefix'] . $user->id;
        } else {
            $login = yii::$app->params['AsteriskSp_prefix'] . $user->id;
        }

        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(yii::$app->params['AsteriskSp_adduser'])
            ->setData([
                'login' => $login,
                'pass' => $data['password']
            ])
            ->send();

        if ($response->isOk) {
            $answer = json_decode($response->content, 1);

            if (isset($answer['status']) && $answer['status'] === true) {

                $parseUrl = parse_url(yii::$app->params['AsteriskSp_adduser']);

                $sip = [
                    'sip_host' => $parseUrl['host'],
                    'sip_login' => $login,
                    'sip_pass' => $data['password']
                ];

                $user->sipArray = $sip;

                if (!$user->save()) {
                    $log = new ApiLog();
                    $log->request = 'insert sip data to User (id = ' . $user->id . ') table . ' . json_encode($sip, JSON_UNESCAPED_UNICODE);
                    $log->response = json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE);
                    $log->save();
                }

            } else {
                $log = new ApiLog();
                $log->request = 'insert sip data to Asterisk2 (id = ' . $user->id . ') . ' . json_encode([
                        'login' => $login,
                        'pass' => $data['password']
                    ], JSON_UNESCAPED_UNICODE);
                $log->response = $response->content;
                $log->save();
            }
        } else {
            $log = new ApiLog();
            $log->request = 'insert sip data to Asterisk2 (id = ' . $user->id . ') . ' . json_encode([
                    'login' => $login,
                    'pass' => $data['password']
                ], JSON_UNESCAPED_UNICODE);
            $log->response = $response->content;
            $log->save();
        }
    }

    /**
     * Новый метод управления паузами на астере
     * @param int $paused
     * @param null $user_sip
     * @return string|\yii\httpclient\Response
     */
    public static function setUserPaused($paused = 1, $user_sip = null)
    {
        $sentRequest = true;

        if (is_null($user_sip)) {
            $sip_data = Yii::$app->user->identity->sipArray;
            $user_sip = $sip_data['sip_login'];

            if(!Yii::$app->user->identity->auto_call){
                $sentRequest = false;
            }
        }

        if($sentRequest){
            $client = new Client();

            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(AutoCall::URL_SET_USER_PAUSED)
                ->setHeaders(['Authorization' => 'Basic ' .  base64_encode(\common\components\web\User::ACCESS_TYPE_CC)])
                ->setData([
                    'paused' => $paused,
                    'user_sip' => $user_sip
                ])
                ->send();

            $content = $response->content;
        }else{
            $content = Json::encode(['success' => true, 'message' => 0]);
        }

        return $content;
    }

    public static function sendRequest($data = [], $method = 'post')
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod($method)
            ->setUrl(yii::$app->params['AsteriskSp_actions'])
            ->setData($data)
            ->send();

        if ($response->isOk) {
            return json_decode($response->content, 1);
        }

        return false;
    }

    public static function getSystemUser()
    {
        $user = AuthAssignment::findOne(['item_name' => 'asterisk']);
        if (!$user)
            return 1;
        return $user->user_id;
    }
}

<?php
namespace common\models;

use common\components\db\ActiveRecord;
use yii;
use common\models\Teams;
use common\models\Country;

class TeamsCountry extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%teams_country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'country_id'], 'required'],
            [['team_id', 'country_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasOne(Teams::className(), ['id' => 'team_id']);
    }

    /**
     * Добавить запись в TeamsCountry
     * @param $team_id
     * @param integer|array $country_id
     * @return bool
     */
    public static function addToTeamsCountry($team_id, $country_id)
    {
        $country_id = is_array($country_id) ? $country_id : [];

        $transaction = yii::$app->db->beginTransaction();

        foreach($country_id as $id) {
            $model = new TeamsCountry();
            $model->team_id = $team_id;
            $model->country_id = $id;
            $model->created_at = time();

            if (!$model->save()) {
                $transaction->rollBack();
                yii::$app->notifier->addNotifierErrorByModel($model);
                return false;
            }
        }

        $transaction->commit();

        return true;
    }

    /**
     * Очистить TeamsCountry по конкретной Team
     * @param $team_id
     * @return bool
     */
    public static function clearTeamsCountryByTeam($team_id)
    {
        $teamsCountryQuery = TeamsCountry::find()->where(['team_id' => $team_id]);

        if($teamsCountryQuery->exists()){
            $teamsCountry = $teamsCountryQuery->all();

            foreach($teamsCountry as $teamCountry){
                if(!$teamCountry->delete()){
                    yii::$app->notifier->addNotifierErrorByModel($teamCountry);
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Очистить TeamsCountry по конкретной Country
     * @param $country_id
     * @return bool
     */
    public static function clearTeamsCountryByCountry($country_id)
    {
        $teamsCountryQuery = TeamsCountry::find()->where(['country_id' => $country_id]);

        if($teamsCountryQuery->exists()){
            $teamsCountry = $teamsCountryQuery->all();

            foreach($teamsCountry as $teamCountry){
                if(!$teamCountry->delete()){
                    yii::$app->notifier->addNotifierErrorByModel($teamCountry);
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Список команд по стране / странам
     * @param integer|array $country_id
     * @return array
     */
    public static function getTeamsByCountry($country_id)
    {
        $country_id = is_array($country_id) ? $country_id : [$country_id];

        return TeamsCountry::find()
            ->joinWith(['teams', 'country'])
            ->where(['country_id' => $country_id])
            ->all();
    }

    /**
     * Все страны по команде
     * @param $team_id
     * @return array|yii\db\ActiveRecord[]
     */
    public static function getCountryByTeam($team_id)
    {
        return TeamsCountry::find()
            ->joinWith('country')
            ->where(['team_id' => $team_id])
            ->all();
    }
}
<?php

namespace common\models;

/**
 * This is the model class for table "queue_order".
 *
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $blocked_to
 *
 * @property User $blockedUser
 */

/**
 * Class QueueOrder
 * @package common\models
 */
class QueueOrder extends \yii\db\ActiveRecord
{
    public $user;

    public static function tableName()
    {
        return '{{%queue_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id', 'blocked_to'], 'required'],
            [['user_id', 'order_id', 'blocked_to'], 'integer']
        ];
    }

    /**
     * @param $user_id
     * @param $order_id
     * @return array
     */
    public static function setBlockOrder($user_id, $order_id)
    {
        $current_time = (new \DateTime())->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))->getTimestamp();

        $model = new QueueOrder();
        $model->order_id = $order_id;
        $model->user_id = $user_id;
        $model->blocked_to = $current_time + 3600;

        if (!$model->save()) {
            $result = [
                'success' => false,
                'message' => $model->getErrors()
            ];
        } else {
            $result = [
                'success' => true,
                'message' => ''
            ];
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlockedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function clearBlocks($user_id)
    {
        QueueOrder::deleteAll(['user_id' => $user_id]);
    }

    public static function clearBlocksById($order_id)
    {
        QueueOrder::deleteAll(['order_id' => $order_id]);
    }
}
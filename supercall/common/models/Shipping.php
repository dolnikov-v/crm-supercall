<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\models\query\ShippingQuery;
use Yii;

/**
 * Class Shipping
 * @package common\models
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 */
class Shipping extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shipping}}';
    }

    /**
     * @return ShippingQuery
     */
    public static function find()
    {
        return new ShippingQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'filter', 'filter' => 'trim'],
            ['name', 'string', 'max' => 128, 'tooLong' => Yii::t('common', 'Слишком длинное название.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }
}

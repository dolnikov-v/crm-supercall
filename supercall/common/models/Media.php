<?php
namespace common\models;

use common\components\db\ActiveRecord;
use Yii;

class Media extends ActiveRecord
{
    //@common/upload/ - корень всех загружаемых файлов, далее по типу
    const TYPE_DOCUMENT_SCAN = 'document_scan';
    const TYPE_PROFILE_IMAGE= 'profile_image';

    const ACTIVE = true;
    const NOT_ACTIVE = false;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%media}}';
    }

    public static function getPathDocumentScan()
    {
        return yii::getAlias('@backend/web/upload/'.self::TYPE_DOCUMENT_SCAN);
    }

    public static function getAbsolutePathDocumentScun()
    {
        return yii::$app->params['urlBackend']. '/upload/' .self::TYPE_DOCUMENT_SCAN;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['filename', 'filepath', 'user_id', 'type', 'created_at'], 'required'],
            [['filename', 'filepath', 'type'], 'string'],
            [['active'], 'boolean'],
            [['user_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => yii::t('common', 'Номер'),
            'filename' => yii::t('common', 'Файл'),
            'filepath' => yii::t('common', 'Директория'),
            'user_id' => yii::t('common', 'Пользователь'),
            'type' => yii::t('common', 'Тип'),
            'active' => yii::t('common', 'Активность'),
            'created_at' => yii::t('common', 'Дата создания'),
            'updated_at' => yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return array
     */
    public static function typesCollection()
    {
        return [
          self::TYPE_DOCUMENT_SCAN => yii::t('common', 'Сканы документов')
        ];
    }

    /**
     * @param integer $type
     * @return bool|string
     */
    public static function getType($type)
    {
        $typesCollection = self::typesCollection();

        return isset($typesCollection[$type]) ? $typesCollection[$type] : false;
    }

    /**
     * @return array
     */
    public static function pathsCollection()
    {
        return [
            self::TYPE_DOCUMENT_SCAN => self::getPathDocumentScan(),
            self::TYPE_PROFILE_IMAGE => self::getPathDocumentScan(),
        ];
    }

    public static function absolutePathCollection()
    {
        return [
            self::TYPE_DOCUMENT_SCAN => self::getAbsolutePathDocumentScun(),
            self::TYPE_PROFILE_IMAGE => self::getAbsolutePathDocumentScun(),
        ];
    }

    /**
     * @param integer $type
     * @return bool|string
     */
    public static function getPath($type)
    {
        $pathsCollection = self::pathsCollection();

        return isset($pathsCollection[$type]) ? $pathsCollection[$type] : false;
    }

    public static function getAbsolutePath($type)
    {
        $pathsCollection = self::absolutePathCollection();

        return isset($pathsCollection[$type]) ? $pathsCollection[$type] : false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param $id
     * @return bool|false|int
     */
    public static function deleteMedia($id)
    {
        $model = Media::findOne(['id' => $id]);
        $model->active = Media::NOT_ACTIVE;

        if($model){
            return $model->save();
        }

        return true;
    }

}
<?php
namespace common\models;

use common\components\db\ActiveRecord;
use Yii;

/**
 * Class CountryWorkTime
 * @package common\models
 * @property integer country_id
 * @property integer partner_id
 * @property integer begin_time_hour
 * @property integer begin_time_minute
 * @property integer end_time_hour
 * @property integer end_time_minute
 */
class CountryWorkTime extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country_work_time}}';
    }

    /**
     * @return array
     */
    public static function getHourList()
    {
        return range(0, 23);
    }

    /**
     * @return array
     */
    public static function getMinuteList()
    {
        return range(0, 59);
    }

    /**
     * @param integer $country_id
     * @param array $time
     * @return bool
     */
    public static function updateWorkTime($country_id, $time = [])
    {
        $model = new CountryWorkTime();
        $model->load($time, '');
        $model->country_id = $country_id;

        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param integer $country_id
     * @return bool
     */
    public static function clearWorkTime($country_id)
    {
        CountryWorkTime::deleteAll(['country_id' => $country_id]);
        return true;
    }

    /**
     * @param integer $country_id
     * @return $this
     */
    public static function getCountryWorkTime($country_id){
        return CountryWorkTime::find()->where(['country_id' => $country_id]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[['country_id', 'begin_time_hour', 'begin_time_minute', 'end_time_hour', 'end_time_minute'], 'required'],
            [['country_id', 'partner_id', 'begin_time_hour', 'begin_time_minute', 'end_time_hour', 'end_time_minute'], 'integer']];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('common', 'ID страны'),
            'partner_id' => Yii::t('common', 'ID партнера'),
            'begin_time_hour' => Yii::t('common', 'Час'),
            'begin_time_minute' => Yii::t('common', 'Минута'),
            'end_time_hour' => Yii::t('common', 'Час'),
            'end_time_minute' => Yii::t('common', 'Минута'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

}

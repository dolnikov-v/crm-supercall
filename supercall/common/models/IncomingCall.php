<?php

namespace common\models;


use common\components\db\ModelTrait;
use common\models\query\IncomingCallQuery;
use yii\db\ActiveRecord;

/**
 * Class IncomingCall
 * @package common\models
 * @property integer $id [integer]
 * @property string $phone
 * @property string $user_sip
 * @property double $cdr_id
 * @property integer $status
 * @property integer $created_at
 */
class IncomingCall extends ActiveRecord
{
    use ModelTrait;

    const STATUS_NEW = 1;
    const STATUS_IN_PROGRESS = 2;

    /**
     * @return string
     */
    public static function tableName()
    {
        return "{{%incoming_call}}";
    }


    /**
     * @return IncomingCallQuery
     */
    public static function find()
    {
        return new IncomingCallQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['phone', 'user_sip'], 'string', 'max' => 15],
            ['cdr_id', 'double'],
            ['status', 'integer'],
            ['status', 'default', 'value' => self::STATUS_NEW],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'phone' => \Yii::t('common', 'Номер телефона'),
            'user_sip' => \Yii::t('common', 'SIP оператора'),
            'cdr_id' => \Yii::t('common', 'CDR'),
        ];
    }

    /**
     * @param $userSip
     * @return bool
     */
    public static function isThereForUserSip($userSip)
    {
        return IncomingCall::find()->byUserSip($userSip)->exists();
    }

    /**
     * @param $userSip
     * @return int
     */
    public static function removeByUserSip($userSip)
    {
        return self::deleteAll(['user_sip' => $userSip]);
    }
}
<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\UserCountry;

/**
 * Class UserCountryQuery
 * @package common\models\query
 * @method UserCountry one() one($db = null)
 * @method UserCountry[] all() all($db = null)
 */
class UserCountryQuery extends ActiveQuery
{
    /**
     * @param integer $id
     * @return $this
     */
    public function byUserId($id)
    {
        return $this->andWhere(['user_id' => $id]);
    }
}

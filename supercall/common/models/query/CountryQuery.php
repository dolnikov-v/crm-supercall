<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Country;

/**
 * Class CountryQuery
 * @package common\models\query
 * @method Country one($db = null)
 * @method Country[] all($db = null)
 */
class CountryQuery extends ActiveQuery
{
    /**
     * @param string $code
     * @return $this
     */
    public function byCharCode($code)
    {
        return $this->andWhere(['char_code' => $code]);
    }

    /**
     * @param string $slug
     * @return $this
     */
    public function bySlug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([Country::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere([Country::tableName() . '.active' => 0]);
    }
}

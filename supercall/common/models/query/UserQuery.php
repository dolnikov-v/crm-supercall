<?php
namespace common\models\query;

use backend\modules\access\models\AuthAssignment;
use common\components\db\ActiveQuery;
use common\models\User;
use common\models\UserCountry;
use common\models\UserPartner;
use common\modules\partner\models\Partner;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class UserQuery
 * @package common\models\query
 * @method UserCountry one() one($db = null)
 * @method UserCountry[] all() all($db = null)
 */
class UserQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';
    protected $collectionValue = 'username';

    public function collection($translateCategory = null)
    {
        $items = $this->select(['id', 'username', 'status', 'created_at'])->orderBy([
                'username' => SORT_ASC,
            ])->all();

        return ArrayHelper::map($items, $this->collectionKey, $this->collectionValue);
    }

    /**
     * @param integer|array $partner_id
     * @return $this
     */
    public function byPartner($partner_id)
    {
        if(Yii::$app->user->isSuperadmin){
            $partner_id = ArrayHelper::getColumn(Partner::findAll(['active' => Partner::ACTIVE]), 'id');
        }else {
            $partner_id = $this->toArray($partner_id);
        }

        return $this
            ->leftJoin(UserPartner::tableName(), UserPartner::tableName().'.user_id=' . User::tableName().'.id')
            ->andWhere(['partner_id' => $partner_id]);
    }

    /**
     * @param User $user
     * @return UserQuery
     */
    public function byUserCountry(User $user)
    {
        return $this->byCountry(ArrayHelper::getColumn($user->getCountries()->all(), 'id'));
    }

    /**
     * @param User $user
     * @return UserQuery
     */
    public function byUserPartner(User $user)
    {
        return $this->byPartner(ArrayHelper::getColumn($user->partners, 'id'));
    }

    /**
     * @param integer|array $country_id
     * @return $this
     */
    public function byCountry($country_id)
    {
        $country_id =$this->toArray($country_id);

        return $this
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName().'.user_id=' . User::tableName().'.id')
            ->andWhere(['country_id' => $country_id]);
    }

    /**
     * @param integer|array $status
     * @return $this
     */
    public function byStatus($status)
    {
        $status = is_array($status) ? $status : [$status];

        return $this->andWhere(['status' => $status]);
    }

    /**
     * @param integer|array $role
     * @return $this
     */
    public function byRole($role)
    {
        $role = $this->toArray($role);

        return $this
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName().'.user_id=' . User::tableName().'.id')
            ->andWhere(['item_name' => $role]);
    }

    /**
     * @return $this
     */
    public function byIdentityUser()
    {
        if(yii::$app->user->isSuperadmin){
            return $this;
        }

        return $this
            ->byUserPartner(yii::$app->user->identity)
            ->byUserCountry(yii::$app->user->identity);
    }
}

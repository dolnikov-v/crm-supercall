<?php

namespace common\models\query;


use yii\db\ActiveQuery;
/**
 * This is the ActiveQuery class for [[UserPartner]].
 *
 * @see UserPartner
 */
class UserPartnerQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    /**
     * @inheritdoc
     * @return UserPartner[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserPartner|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * @param integer $id
     * @return $this
     */
    public function byUserId($id)
    {
        return $this->andWhere(['user_id' => $id]);
    }
}

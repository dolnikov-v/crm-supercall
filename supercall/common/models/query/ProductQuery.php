<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Product;

/**
 * Class ProductQuery
 * @package common\models\query
 * @method Product one($db = null)
 * @method Product[] all($db = null)
 */
class ProductQuery extends ActiveQuery
{

}

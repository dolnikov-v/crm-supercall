<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Shipping;

/**
 * Class ShippingQuery
 * @package common\models\query
 * @method Shipping one($db = null)
 * @method Shipping[] all($db = null)
 */
class ShippingQuery extends ActiveQuery
{

}

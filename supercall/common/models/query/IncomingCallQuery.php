<?php

namespace common\models\query;

use common\components\db\ActiveQuery;


/**
 * Class IncomingCallQuery
 * @package common\models\query
 */
class IncomingCallQuery extends ActiveQuery
{
    /**
     * @param string $userSip
     * @return $this
     */
    public function byUserSip($userSip)
    {
        return $this->andWhere(['user_sip' => $userSip]);
    }
}
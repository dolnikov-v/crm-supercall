<?php

namespace common\models\query;


use common\components\db\ActiveQuery;
use common\models\Teams;

/**
 * This is the ActiveQuery class for [[\common\models\Teams]].
 *
 * @see \common\models\Teams
 */
class TeamsQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere([Teams::tableName() .'.active' => true]);
    }

    /**
     * @inheritdoc
     * @return \common\models\Teams[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Teams|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

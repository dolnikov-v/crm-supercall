<?php

namespace common\models\query;

use common\models\LiveMessage;
use yii\db\ActiveQuery;

/**
 * Class LiveMessageQuery
 * @package common\models\query
 */
class LiveMessageQuery extends ActiveQuery
{
    public function findNew()
    {
        return $this->andWhere(['is_new' => LiveMessage::IS_NEW]);
    }

    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|\yii\db\ActiveRecord
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
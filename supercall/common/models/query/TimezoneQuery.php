<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Timezone;

/**
 * Class TimezoneQuery
 * @package common\models\query
 * @method Timezone one() one($db = null)
 * @method Timezone[] all() all($db = null)
 */
class TimezoneQuery extends ActiveQuery
{
    /**
     * @param string $timezoneId
     * @return $this
     */
    public function byTimezoneId($timezoneId)
    {
        return $this->andWhere(['timezone_id' => $timezoneId]);
    }
    
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }
}

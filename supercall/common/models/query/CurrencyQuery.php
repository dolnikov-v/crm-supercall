<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Currency;

/**
 * Class TimezoneQuery
 * @package common\models\query
 *
 * @method Currency one($db = null)
 * @method Currency[] all($db = null)
 */
class CurrencyQuery extends ActiveQuery
{
    /**
     * @param string $charCode
     * @return $this
     */
    public function byCharCode($charCode)
    {
        return $this->andWhere(['char_code' => $charCode]);
    }

    /**
     * @param integer $numCode
     * @return $this
     */
    public function byNumCode($numCode)
    {
        return $this->andWhere(['num_code' => $numCode]);
    }
}

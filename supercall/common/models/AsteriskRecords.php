<?php
namespace common\models;

use Yii;
use yii\httpclient\Client;

class AsteriskRecords
{
    public function getRecords($user_sip = '', $phone, $type = false)
    {
        if (empty($user_sip) && !is_string($user_sip) && (empty($phone) || !is_string($phone))) {
            return false;
        }

        return $this->Send([
            'user_sip' => $user_sip,
            'phone' => $phone
        ], $type ? $type : 'origin');
    }

    private function Send($params, $type = false, $vb = false)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(yii::$app->params['AsteriskRecordUrl'])
            ->setData($params)
            ->send();

        if ($response->isOk) {
            if ($type == 'array') {
                return json_decode($response->content, 1);
            } else {
                return $response->content;
            }

        } else {
            if ($vb) {
                $vb = ['status' => 'fail', 'params' => $params, 'response' => print_r($response, 1)];
                if ($type == 'origin') {
                    return $vb;
                } else if ('array') {
                    return json_decode($vb, 1);
                }

            } else {
                return false;
            }
        }
    }
}
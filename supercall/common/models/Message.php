<?php
namespace common\models;

use common\components\db\ActiveRecord;
use Yii;

/**
 * Class Message
 * @package common\models
 * @property integer $id
 * @property string $language
 * @property string $translation
 * @property integer $created_at
 * @property integer $updated_at
 * @property SourceMessage $sourceMessage
 */
class Message extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%i18n_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'translation'], 'required'],
            ['id', 'exist', 'targetClass' => '\common\models\SourceMessage', 'targetAttribute' => 'id', 'message' => Yii::t('common', 'Неверное значение исходного сообщения.')],
            ['id', 'validateDuplicate'],
            ['language', 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'language' => Yii::t('common', 'Язык'),
            'translation' => Yii::t('common', 'Перевод'),
            'phrase' => Yii::t('common', 'Фраза'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceMessage()
    {
        return $this->hasOne(SourceMessage::className(), ['id' => 'id']);
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateDuplicate($attribute, $params)
    {
        if ($this->language && $this->translation) {
            $query = Message::find()
                ->where(['id' => $this->id])
                ->andWhere(['language' => $this->language]);

            if (!$this->isNewRecord) {
                $query->andWhere('id != ' . $this->id);
            }

            $exists = $query->exists();

            if ($exists) {
                $this->addError($attribute, Yii::t('common', 'Перевод уже существует.'));
            }
        }
    }
}

<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\models\query\CurrencyQuery;
use Yii;

/**
 * Class Currency
 * @package common\models
 * @property integer $id
 * @property string $name
 * @property string $num_code
 * @property string $char_code
 * @property integer $created_at
 * @property integer $updated_at
 */
class Currency extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @return CurrencyQuery
     */
    public static function find()
    {
        return new CurrencyQuery(get_called_class());
    }

    /**
     * @param $numCode
     * @return Currency
     */
    public static function findByNumCode($numCode)
    {
        return Currency::find()
            ->where(['num_code' => $numCode])
            ->one();
    }

    /**
     * @param $charCode
     * @return Currency
     */
    public static function findByCharCode($charCode)
    {
        return Currency::find()
            ->where(['char_code' => $charCode])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'num_code', 'char_code'], 'required'],

            [['name', 'num_code', 'char_code'], 'filter', 'filter' => 'trim'],

            ['name', 'string', 'max' => 128, 'tooLong' => Yii::t('common', 'Слишком длинное название.')],
            ['num_code', 'string', 'max' => 3, 'tooLong' => Yii::t('common', 'Слишком длинный цифровой код.')],
            ['char_code', 'string', 'max' => 3, 'tooLong' => Yii::t('common', 'Слишком длинный буквенный код.')],

            [['num_code', 'char_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'num_code' => Yii::t('common', 'Цифровой код'),
            'char_code' => Yii::t('common', 'Буквенный код'),
        ];
    }
}

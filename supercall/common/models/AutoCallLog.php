<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\modules\order\models\Order;
use yii;

/**
 * Class AutoCallLog
 * @package common\models
 *
 * @property Order $order
 */
class AutoCallLog extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%auto_call_log}}';
    }

    public function rules()
    {
        return [
            [['id','order_id','created_at','country_id'], 'integer'],
            [['order_phone', 'queue_name', 'cdr_id', 'user_sip', 'call_status', 'group_queue'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'          => yii::t('common', 'ID'),
            'order_id'    => yii::t('common', 'ID заказа'),
            'order_phone' => yii::t('common', 'Телефон'),
            'created_at'  => yii::t('common', 'Время создания'),
            'country_id'  => yii::t('common', 'Страна'),
            'queue_name'  => yii::t('common', 'Код очереди'),
            'group_queue' => Yii::t('common', 'Группа'),
            'cdr_id'      => yii::t('common', 'Код звонка'),
            'user_sip'    => yii::t('common', 'Сип логин'),
            'call_status' => yii::t('common', 'Статус звонка'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
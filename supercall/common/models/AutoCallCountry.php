<?php

namespace common\models;


use common\components\db\ActiveRecord;
use Yii;

/**
 * Class AutoCallCountry
 * @package common\models
 * @property integer country_id
 * @property integer count
 * @property boolean is_active
 * @property Country country
 */
class AutoCallCountry extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{auto_call_country}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id'], 'required'],
            ['count', 'default', 'value' => 1],
            [['country_id', 'count'], 'integer'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('common', 'Страна'),
            'count' => Yii::t('common', 'Количество заказов на оператора'),
            'is_active' => Yii::t('common', 'Доступность'),
        ];
    }

    /**
     * @param $country_id
     * @return bool
     */
    public static function clearAutoCallCount($country_id)
    {
        AutoCallCountry::deleteAll(['country_id' => $country_id]);
        return true;
    }


    /**
     * @param $country_id
     * @param $params
     * @return bool
     */
    public static function updateAutoCallCount($country_id, $params)
    {
        $model = new AutoCallCountry();
        $model->load($params, '');
        $model->country_id = $country_id;
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $country_id
     * @return int
     */
    public static function getCount($country_id)
    {
        $count = self::findOne(['country_id' => $country_id]);

        if (!$count || is_null($count)) {
            return 1;
        }

        return $count->count;
    }


    /**
     * @return array
     */
    public static function getCountryId()
    {

        $data = self::find()->all();

        $country_ids = [];
        foreach ($data as $value) {
            $country_ids[] = $value->country_id;
        }

        if (!$country_ids || is_null($country_ids) || empty($country_ids)) {
            return [];
        }

        return $country_ids;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }


}
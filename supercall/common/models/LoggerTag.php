<?php
namespace common\models;

use common\components\db\ActiveRecord;
use yii;

class LoggerTag extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%base_tag_log}}';
    }

    public function rules()
    {
        return [
            [['log_id'], 'required'],
            [['log_id'], 'integer'],
            [['tag', 'value'], 'string'],
            [['created_at', 'updated_at'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'tag' => yii::t('common', 'Свойство'),
            'value' => yii::t('common', 'Значение'),
            'log_id' => yii::t('common', 'ссылка на лог'),
            'created_at' => yii::t('common', 'Создано'),
            'updated_at' => yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogger()
    {
        return $this->hasOne(Logger::className(), ['id' => 'log_id']);
    }
}
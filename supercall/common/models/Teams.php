<?php

namespace common\models;

use Yii;

/**
 * Модель отвечает за команды для пользователей
 * В команды можно объеденить несколько пользователей/операторов
 *
 * This is the model class for table "{{%teams}}".
 *
 * @property integer $id
 * @property string $name
 * @property boolean $active
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User[] $users
 */
class Teams extends \common\components\db\ActiveRecord
{
    const STATUS_ACTIVE = 1; // Активен
    const STATUS_NOT_ACTIVE = 0; // Не активен

    public $country_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%team}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['active'], 'boolean'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique', 'targetClass' => 'common\models\Teams', 'message' => yii::t('common', 'Имя команды уже используется')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Название команды'),
            'country_id' => Yii::t('common', 'Страна'),
            'active' => Yii::t('common', 'Активна'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTeams()
    {
        return $this->hasMany(UserTeam::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamsCountry()
    {
        return $this->hasMany(TeamsCountry::className(), ['team_id' => 'id']);
    }

    /**
     * @param integer|array $team_id
     * @return array
     */
    public static function getUsers($team_id)
    {
        $team_id = is_array($team_id) ? $team_id : [$team_id];

        $userTeamsQuery = UserTeam::find()->where(['team_id' => $team_id]);

        $users = [];

        if($userTeamsQuery->exists()){
            $userTeams = $userTeamsQuery->all();

            foreach ($userTeams as $user_team) {
                $listUsers = $user_team->getUsers()->all();

                foreach($listUsers as $user){
                    $users[] = $user;
                }
            }
        }

        return $users;
    }

    /**
     * @inheritdoc
     * @return \common\models\query\TeamsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\TeamsQuery(get_called_class());
    }


    /**
     * Список статусов Активности, либо
     * если передан $key, то конкретный статус
     * @param type $key
     * @return string
     */
    public static function getStatus($key = null)
    {
        $status = [
            self::STATUS_ACTIVE => 'Да',
            self::STATUS_NOT_ACTIVE => 'Нет',
        ];

        if (!is_null($key) && isset($status[$key])) {
            return $status[$key];
        }

        return $status;
    }
}

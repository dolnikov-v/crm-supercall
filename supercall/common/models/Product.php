<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\models\query\ProductQuery;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
/**
 * Class Product
 * @package common\models
 * @property integer $id
 * @property string $name
 * @property boolean $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class Product extends ActiveRecord
{
    const ACTIVE = true;
    const NOT_ACTIVE = false;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @return ProductQuery
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'filter', 'filter' => 'trim'],
            [['active'], 'boolean'],
            ['name', 'string', 'max' => 128, 'tooLong' => Yii::t('common', 'Слишком длинное название.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Активен'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    public static function activate($id)
    {
        $model = Product::findOne(['id' => $id]);

        if(is_null($model)){
            throw new HttpException(404, Yii::t('common', 'Товар не найден.'));
        }else{
            $model->active = true;

            if(!$model->save(false)){
                yii::$app->notifier->addNotifier(yii::t('common', 'Не удалось активировать товар'), 'error');
            }else{
                yii::$app->notifier->addNotifier(yii::t('common', 'Товар активирован'), 'success');
            }
        }
    }

    public static function deactivate($id)
    {
        $model = Product::findOne(['id' => $id]);

        if(is_null($model)){
            throw new HttpException(404, Yii::t('common', 'Товар не найден.'));
        }else{
            $model->active = false;

            if(!$model->save(false)){
                yii::$app->notifier->addNotifier(yii::t('common', 'Не удалось деактивировать товар'), 'error');
            }else{
                yii::$app->notifier->addNotifier(yii::t('common', 'Товар деактивирован'), 'success');
            }
        }
    }

    public static function getActiveListProducts($active = true, $translatePath = 'common')
    {
        $products = ArrayHelper::map(Product::findAll(['active' => $active]), 'id', 'name');

        foreach ($products as $key => $item) {
            $products[$key] = Yii::t($translatePath, $item);
        }

        return $products;
    }
}

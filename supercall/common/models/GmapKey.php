<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\modules\order\models\Order;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Expression;
use yii\web\View;

/**
 * Class GmapKey
 * @package common\models
 * @property integer $id
 * @property string $email
 * @property string $key
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class GmapKey extends ActiveRecord
{
    const DEFAULT_ACCOUNT = 'technical2wtrade@gmail.com';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gmap_key}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'key'], 'required'],
            [['active'], 'integer'],
            [['active'], 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'email' => Yii::t('common', 'Email'),
            'key' => Yii::t('common', 'Ключ'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * Регистрирует js файл карт с активным ключом
     * @param View $view
     * @throws InvalidConfigException
     */
    public static function register(View $view, Order $order)
    {
        $key = self::find()->where(['active' => 1])->orderBy(['id' => SORT_DESC])->one();
        if (!$key){
            throw new InvalidConfigException(Yii::t('common', 'Не найден активный ключ'));
        }
        // язык поиска
        $language = self::getGmapLanguage($order);
        
        $view->registerJsFile('https://maps.googleapis.com/maps/api/js?key=' . $key->key . $language .'&libraries=places&callback=');
    }
    
    /**
     * Определить язык по соответствующей этому заказу стране
     * добавит в запрос параметр "language"
     * https://developers.google.com/maps/documentation/geocoding/intro?hl=ru
     *
     * поддержка языков https://developers.google.com/maps/faq?hl=ru#languagesupport
     *
     * @param Order $order
     * @return string
     */
    private static function getGmapLanguage(Order $order)
    {
        return $order->country->slug
            ? '&language=' .  $order->country->slug
            : ''; // либо не запрашивать
    }
}

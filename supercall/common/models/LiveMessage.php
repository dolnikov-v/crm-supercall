<?php

namespace common\models;

use common\components\db\ActiveRecord;
use common\models\query\LiveMessageQuery;
use common\modules\order\models\Order;
use Yii;

/**
 * This is the model class for table "live_message".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $user_id
 * @property boolean $is_new
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $phone
 * @property integer $queue_number
 * @property integer $chat_id
 *
 * @property Order $order
 * @property User $user
 */

/**
 * Class LiveMessage
 * @package common\models
 */
class LiveMessage extends ActiveRecord
{
    const IS_NEW = true;
    const IS_NOT_NEW = false;

    const LOGGER_TYPE = 'live message';

    const TYPE_WHATSAPP = 'whatsapp';

    public $sender;

    public static function tableName()
    {
        return '{{%live_message}}';
    }

    public function rules()
    {
        return [
            [['order_id', 'type', 'content'], 'required'],
            [['order_id', 'user_id', 'created_at', 'updated_at', 'queue_number'], 'integer'],
            [['is_new'], 'boolean'],
            [['type', 'content', 'phone', 'chat_id'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Заказ'),
            'type' => Yii::t('common', 'Тип'),
            'content' => Yii::t('common', 'Сообщение'),
            'user_id' => Yii::t('common', 'Оператор'),
            'is_new' => Yii::t('common', 'Новое'),
            'phone' => Yii::t('common', 'Телефон'),
            'queue_number' => Yii::t('common', 'Номер'),
            'chat_id' => Yii::t('common', 'Номер чата'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления')
        ];
    }

    /**
     * @return array
     */
    public static function getTypesCollection()
    {
        return [
            self::TYPE_WHATSAPP
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return LiveMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LiveMessageQuery(get_called_class());
    }

    public function afterFind()
    {
        $this->sender = is_null($this->user_id) ? $this->order->customer_full_name : $this->user->username;
        $this->created_at = yii::$app->formatter->asDate($this->created_at, 'php:d/m/Y H:i:s');

        parent::afterFind();
    }

}
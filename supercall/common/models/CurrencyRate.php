<?php
namespace common\models;

use common\components\db\ActiveRecord;
use Yii;

/**
 * Class CurrencyRate
 * @package common\models
 * @property integer $id
 * @property integer $currency_id
 * @property float $rate
 * @property float $ask
 * @property float $bid
 * @property integer $created_at
 * @property integer $updated_at
 * @property Currency $currency
 */
class CurrencyRate extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency_rate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['currency_id', 'required'],
            [['currency_id', 'created_at', 'updated_at'], 'integer'],

            ['currency_id', 'exist', 'targetClass' => '\app\models\Currency', 'targetAttribute' => 'id', 'message' => Yii::t('common', 'Неверное значение валюты.')],

            [['rate', 'ask', 'bid'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'currency_id' => Yii::t('common', 'Валюта'),
            'rate' => Yii::t('common', 'Курс'),
            'ask' => Yii::t('common', 'Курс продажи'),
            'bid' => Yii::t('common', 'Курс покупки'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
}

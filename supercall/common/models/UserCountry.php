<?php
namespace common\models;

use backend\modules\access\models\AuthAssignment;
use common\components\db\ActiveRecord;
use common\models\query\UserCountryQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * Class UserCountry
 * @package common\models
 * @property integer $id
 * @property integer $user_id
 * @property integer $country_id
 * @property Country $country
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property User $user
 */
class UserCountry extends ActiveRecord
{
    const ACTIVE = 0;
    const NOT_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_country}}';
    }
    
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * @return UserCountryQuery
     */
    public static function find()
    {
        return new UserCountryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'country_id'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [
                'user_id',
                'exist',
                'targetClass' => '\common\models\User',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение пользователя.')
            ],
            [
                'country_id',
                'exist',
                'targetClass' => '\common\models\Country',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение связи со страной.')
            ],

            ['active', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('common', 'Номер'),
            'user_id'    => Yii::t('common', 'Пользователь'),
            'country_id' => Yii::t('common', 'Страна'),
            'created_at' => Yii::t('common', 'Время создания'),
            'updated_at' => Yii::t('common', 'Время изменения'),
        ];
    }

    /**
     * @param array $country_ids
     * @param bool $active
     * @param string|bool $role - false - all roles
     * @return UserCountry[]
     */
    public static function getUsersByCountry($country_ids, $active, $role)
    {
        $country_ids = is_array($country_ids) ? $country_ids : [$country_ids];

        $queryOperators = User::find()
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id=' . User::tableName() . '.id')
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName() . '.user_id=' . User::tableName() . '.id')
            ->where(['is not', User::tableName() . '.id', null])
            ->andWhere([UserCountry::tableName().'.country_id' => $country_ids]);

        if($active){
           $queryOperators ->andFilterWhere(['not in', User::tableName() . '.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]]);
        }else{
            $queryOperators ->andFilterWhere(['in', User::tableName() . '.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]]);
        }

        if($role){
            $queryOperators->andFilterWhere([AuthAssignment::tableName() . '.item_name' => $role]);
        }

        $queryOperators->groupBy(User::tableName() . '.id');

        return $queryOperators->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

}

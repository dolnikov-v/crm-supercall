<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\models\query\TimezoneQuery;
use Yii;

/**
 * Class Timezone
 * @package common\models
 * @property integer $id
 * @property integer $time_offset
 * @property string $name
 * @property string $timezone_id
 *
 * @method static Timezone findOne($condition)
 */
class Timezone extends ActiveRecord
{
    const DEFAULT_TIME_OFFSET = 0;
    const DEFAULT_TIMEZONE = 'Europe/London';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%timezone}}';
    }

    /**
     * @return TimezoneQuery
     */
    public static function find()
    {
        return new TimezoneQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'time_offset' => Yii::t('common', 'Смещение'),
            'name' => Yii::t('common', 'Название'),
            'timezone_id' => Yii::t('common', 'Временная зона'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @return Country
     */
    public static function getDefault()
    {
        return self::find()->byTimezoneId(self::DEFAULT_TIMEZONE)->one();
    }
}

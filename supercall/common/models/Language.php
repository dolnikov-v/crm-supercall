<?php
namespace common\models;

use common\models\query\LanguageQuery;
use common\components\db\ActiveRecord;
use Yii;

/**
 * Class Language
 * @package common\models
 * @property integer $id
 * @property string $name
 * @property string $icon
 * @property string $locale
 * @property integer $source_language
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class Language extends ActiveRecord
{
    const DEFAULT_LOCALE = 'en-US';
    const DEFAULT_LANGUAGE_NAME = 'English';
    
    const LANGUAGE_RUSSIAN    = 'Russian';
    const LANGUAGE_ENGLISH    = 'English';
    const LANGUAGE_SPANISH    = 'Spanish';
    const LANGUAGE_FRENCH     = 'French';
    const LANGUAGE_GERMAN     = 'German';
    const LANGUAGE_SLOVAK     = 'Slovak';
    const LANGUAGE_ROMANIAN   = 'Romanian';
    const LANGUAGE_CZECH      = 'Czech';
    const LANGUAGE_PORTUGUESE = 'Portuguese';
    const LANGUAGE_GREEK      = 'Greek';
    const LANGUAGE_BULGARIAN  = 'Bulgarian';
    const LANGUAGE_ITALIAN    = 'Italian';
    const LANGUAGE_CHINESE    = 'Chinese';
    const LANGUAGE_POLISH     = 'Polish';
    const LANGUAGE_KAZAKH     = 'Kazakh';
    const LANGUAGE_HUNGARIAN  = 'Hungarian';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%i18n_language}}';
    }

    /**
     * @return LanguageQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new LanguageQuery(get_called_class());
    }

    /**
     * @return Language
     */
    public static function getDefault()
    {
        return self::find()
            ->where(['locale' => self::DEFAULT_LOCALE])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'icon', 'locale'], 'required'],

            ['name', 'string', 'max' => 32],
            ['locale', 'string', 'max' => 8],
            [['genesys_name'], 'in', 'range' => array_keys( self::getLanguageList() ) ],
            [['source_language', 'active'], 'integer'],
            [['source_language', 'active'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'locale' => Yii::t('common', 'Локаль'),
            'genesys_name' => Yii::t('common', 'Genesys name'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }
    
    /**
     * Выбор языка для поля "genesys_name"
     * @return array
     */
    public static function getLanguageList()
    {
        return [
            self::LANGUAGE_RUSSIAN    => Yii::t('common','Русский'),
            self::LANGUAGE_ENGLISH    => Yii::t('common','Английский'),
            self::LANGUAGE_SPANISH    => Yii::t('common','Испанский'),
            self::LANGUAGE_FRENCH     => Yii::t('common','Французский'),
            self::LANGUAGE_GERMAN     => Yii::t('common','Немецкий'),
            self::LANGUAGE_SLOVAK     => Yii::t('common','Словацкий'),
            self::LANGUAGE_ROMANIAN   => Yii::t('common','Румынский'),
            self::LANGUAGE_CZECH      => Yii::t('common','Чешский'),
            self::LANGUAGE_PORTUGUESE => Yii::t('common','Португальский'),
            self::LANGUAGE_GREEK      => Yii::t('common','Греческий'),
            self::LANGUAGE_BULGARIAN  => Yii::t('common','Болгарский'),
            self::LANGUAGE_ITALIAN    => Yii::t('common','Итальянский'),
            self::LANGUAGE_CHINESE    => Yii::t('common','Китайский'),
            self::LANGUAGE_POLISH     => Yii::t('common','Польский'),
            self::LANGUAGE_KAZAKH     => Yii::t('common','Казахский'),
            self::LANGUAGE_HUNGARIAN  => Yii::t('common','Венгерский'),
        ];
    }
}

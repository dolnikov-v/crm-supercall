<?php
namespace common\models;

use backend\modules\audit\models\AggregateWord;
use backend\modules\audit\models\TranscribeSetting;
use backend\modules\catalog\models\PriceShipping;
use backend\modules\catalog\models\ScriptsOperators;
use backend\modules\operator\models\QuestionnaireBase;
use backend\modules\widget\models\WidgetCache;
use common\components\db\ActiveRecord;
use common\models\query\CountryQuery;
use common\modules\order\models\Order;
use common\modules\partner\models\Package;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerProductPrice;
use common\modules\partner\models\PartnerSettings;
use common\modules\partner\models\PartnerShipping;
use common\modules\partner\models\PartnerVat;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $char_code
 * @property integer $active
 * @property integer $timezone_id
 * @property integer $currency_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $language_id
 * @property string $helpline_phone
 * @property boolean $order_hold_verification
 * @property boolean $klader_in_all_string
 *
 * @property AggregateWord[] $aggregateWords
 * @property AggregateWordStat[] $aggregateWordStats
 * @property AutoCallCounty[] $autoCallCounties
 * @property Currency $currency
 * @property I18nLanguage $language
 * @property Timezone $timezone
 * @property CountryPhoneCode[] $countryPhoneCodes
 * @property CountryWorkTime[] $countryWorkTimes
 * @property Order[] $orders
 * @property OrderLogOlapApproved[] $orderLogOlapApproveds
 * @property OrderLogOlapGeneral[] $orderLogOlapGenerals
 * @property OrderLogOlapReject[] $orderLogOlapRejects
 * @property OrderLogOlapTrash[] $orderLogOlapTrashes
 * @property OrderOlapApproved[] $orderOlapApproveds
 * @property OrderOlapAttempts[] $orderOlapAttempts
 * @property OrderOlapGeneral[] $orderOlapGenerals
 * @property OrderOlapReject[] $orderOlapRejects
 * @property OrderOlapTrash[] $orderOlapTrashes
 * @property Package[] $packages
 * @property PartnerForm[] $partnerForms
 * @property PartnerProductPrice[] $partnerProductPrices
 * @property PartnerSettings[] $partnerSettings
 * @property PartnerShipping[] $partnerShippings
 * @property PartnerVat[] $partnerVats
 * @property PriceShipping[] $priceShippings
 * @property PromotionBuilder[] $promotionBuilders
 * @property QuestionnaireBase[] $questionnaireBases
 * @property ScriptsOperators[] $scriptsOperators
 * @property TeamsCountry[] $teamsCountries
 * @property TranscribeSetting[] $transcribeSettings
 * @property UserCountry[] $userCountries
 * @property User[] $users
 * @property WidgetCache[] $widgetCaches
 * @property ZingTreeCountry[] $zingTreeCountries
 * @property string $whatsapp_number
 * @property string $whatsapp_url
 * @property string $whatsapp_token
 * @property string $whatsapp_phone
 * @property string $whatsapp_description
 * @property string $whatsapp_message_template
 * @property jsonb $strategy
 */
class Country extends ActiveRecord
{
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;
    const KLADER_IN_ALL_STRING_ON = 1;
    const KLADER_IN_ALL_STRING_OFF = 0;
    const DEFAULT_STRATEGY = [];

    const STRATEGY_TYPE_STATUS = 'status';
    const STRATEGY_TYPE_QUEUE = 'queue';

    public $country_id;
    public $status;
    public $queue;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * @return CountryQuery
     */
    public static function find()
    {
        return new CountryQuery(get_called_class());
    }

    /**
     * @return Country
     */
    public static function getDefault()
    {
        $country = null;

        if (Yii::$app->user->isSuperadmin) {
            $country = self::find()
                ->active()
                ->one();
        } else {
            if (!Yii::$app->user->isGuest) {
                $country = self::find()
                    ->joinWith(['userCountry'])
                    ->where([UserCountry::tableName() . '.user_id' => Yii::$app->user->id])
                    ->active()
                    ->one();
            }
        }

        if (is_null($country)) {
            $country = new Country();
        }

        return $country;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'char_code', 'timezone_id'], 'required'],
            ['name', 'string', 'max' => 32],
            ['slug', 'string', 'max' => 3],
            ['slug', 'string', 'max' => 3],
            [['helpline_phone', 'whatsapp_number', 'whatsapp_url', 'whatsapp_token', 'whatsapp_phone', 'whatsapp_description', 'whatsapp_message_template'], 'string'],
            ['strategy', 'string'],
            [['order_hold_verification', 'klader_in_all_string'], 'boolean'],
            ['slug', 'unique'],
            ['slug', 'validateSlug'],
            ['char_code', 'string', 'max' => 3],
            [
                'timezone_id',
                'exist',
                'targetClass' => '\common\models\Timezone',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение временной зоны.'),
            ],
            [
                'currency_id',
                'exist',
                'targetClass' => '\common\models\Currency',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение валюты.'),
            ],
            ['language_id', 'validateLanguage'],
            [['status', 'queue'], 'integer']
            //['free_delivery', 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Доступность'),
            'slug' => Yii::t('common', 'URL сегмент'),
            'char_code' => Yii::t('common', 'Код страны'),
            'timezone_id' => Yii::t('common', 'Временная зона'),
            'currency_id' => Yii::t('common', 'Валюта'),
            'helpline_phone' => Yii::t('common', 'Телефон доверия'),
            'order_hold_verification' => Yii::t('common', 'Задержка заказа при проверке адреса'),
            'klader_in_all_string' => Yii::t('common', 'При работе с КЛАДР искать совпадения по всей строке'),
            'strategy' => Yii::t('common', 'Действия с заказами с невалидными номерами')
            //'free_delivery' => Yii::t('common', 'Бесплатная доставка')
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->strategy = self::DEFAULT_STRATEGY;

        if ($this->status) {
            $this->strategy = [
                'type' => self::STRATEGY_TYPE_STATUS,
                'value' => $this->status
            ];
        } elseif ($this->queue) {
            $this->strategy = [
                'type' => self::STRATEGY_TYPE_QUEUE,
                'value' => $this->queue
            ];
        }

        $this->strategy = Json::encode($this->strategy);

        return parent::beforeValidate();
    }

    /**
     *
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->strategy = Json::decode($this->strategy);

        if(isset($this->strategy['type'])){
            if($this->strategy['type'] == self::STRATEGY_TYPE_STATUS){
                $this->status = $this->strategy['value'];
            }
            elseif($this->strategy['type'] == self::STRATEGY_TYPE_QUEUE){
                $this->queue = $this->strategy['value'];
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateSlug($attribute, $params)
    {
        if (preg_match("/[^a-z0-9]/", $this->slug)) {
            $this->addError($attribute, Yii::t('common', 'URL сегмент может содержать только буквы английского алфавита в нижнем регистре и цифры от 0 до 9.'));
        }
    }

    public function validateLanguage()
    {
        if (!$this->language) {
            $this->addError('language_id', Yii::t('common', 'Неверное значение языка'));
            return;
        }
        if (!$this->language->locale) {
            $this->addError('language_id', Yii::t('common', 'У языка не задана локаль'));
        }
        if (!$this->language->genesys_name) {
            $this->addError('language_id', Yii::t('common', 'У язык не задан genesys_name'));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimezone()
    {
        return $this->hasOne(Timezone::className(), ['id' => 'timezone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCountry()
    {
        return $this->hasMany(UserCountry::className(), ['country_id' => 'id']);
    }

    /**
     * Функция возвратит массив стран доступных текущему пользователю
     *
     * @return array
     */
    public static function getAllowCountries()
    {
        $query = Country::find()->select(['id', 'name'])->asArray(true)->active();
        if (!\Yii::$app->user->isSuperadmin) {
            $countries = $query
                ->joinWith(['userCountry'])
                ->where([UserCountry::tableName() . '.user_id' => \Yii::$app->user->id]);
        }

        return ArrayHelper::map($query->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranscribeSetting()
    {
        return $this->hasOne(TranscribeSetting::className(), ['country_id' => 'id']);
    }


}

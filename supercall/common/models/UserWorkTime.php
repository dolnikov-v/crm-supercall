<?php
namespace common\models;


use common\components\db\ActiveRecord;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\UserReady;
use yii\helpers\Url;

/**
 * Class UserWorkTime
 * @package common\models
 * @property integer id
 * @property integer user_id
 * @property integer time
 * @property integer date
 * @property integer created_at
 * @property integer updated_at
 *
 * @property User user
 */
class UserWorkTime extends ActiveRecord
{

    const LOGGER_TYPE = 'aggregate work time';


    /**
     * @var UserWorkTime workTime
     */
    public $workTime;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%user_work_time%}}';
    }

    /**
     *
     */
    public function getUser()
    {
        $this->hasOne(User::className(), ['user_id' => 'id']);
    }

    public function rules()
    {
        return [
            [
                ['user_id', 'time', 'date'], 'required'
            ],
            [
                ['user_id', 'time', 'date'], 'integer'
            ],
        ];
    }

    /**
     * @param bool $allTime
     */
    public static function aggregate($allTime = false, $debug = false)
    {
        $times = UserReady::find()
            ->select([
                    'user_id' => UserReady::tableName() . '.user_id',
                    'date' => new Expression('extract(epoch from date_trunc(\'day\', to_timestamp(' . UserReady::tableName() . '.start_time) at time zone \'UTC\'))'),
                    'time' => 'SUM(diff)'
                ]
            );

        if (!$allTime) {
            $yesterday = strtotime("-1 day");

            $times = $times->where(
                ['between',
                    UserReady::tableName() . '.start_time',
                    strtotime(date('Y-m-d 00:00:00', $yesterday)), strtotime(date('Y-m-d 23:59:59', $yesterday))
                ]
            );
        }

        $times = $times->groupBy(['date', 'user_id'])
            ->asArray()
            ->all();

        if (empty($times)) {
            return false;
        }

        $logger = yii::$app->get('logger');
        $logger->action = 'common\models\UserWorkTime::aggregate()';
        $logger->route = 'common\models\UserWorkTime';
        $logger->type = UserWorkTime::LOGGER_TYPE;

        foreach ($times as $time) {
            if (!empty($time['user_id']) && !empty($time['date'])) {
                if ($debug) {
                    echo "Date: " . $time['date'] . " user_id: " . $time['user_id'] . " time: " . $time['time'] . PHP_EOL;
                }

                $workTime = new UserWorkTime();
                $workTime->load($time, '');

                UserWorkTime::deleteAll(['user_id' => $time['user_id'], 'date' => $time['date']]);

                if (!$workTime->save()) {
                    if ($debug) {
                        echo "Error save" . PHP_EOL;
                    }
                    $logger->tags = ArrayHelper::merge($time, ['action' => '$workTime->save()', 'errors' => print_r($workTime->getErrors(),1)]);
                    $logger->save();
                }

            } else {
                $logger->tags = ArrayHelper::merge($time, ['action' => 'empty_user_id_or_date']);
                $logger->save();
            }
        }
    }
}
<?php
namespace common\models;

use common\components\db\ActiveRecord;
use yii;

/**
 * Class Logger
 * @package common\models
 */
class Logger extends ActiveRecord
{
    public $tag;
    public $value;

    public static function tableName()
    {
        return '{{%base_log}}';
    }

    public function rules()
    {
        return [
            ['created_at', 'required'],
            [['route', 'action', 'tags', 'type', 'old_tags', 'model'], 'string'],
            [['created_at', 'updated_at', 'user_id'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'route' => yii::t('common', 'Путь'),
            'action' => yii::t('common', 'Действие'),
            'tags' => yii::t('common', 'Поля'),
            'type' => yii::t('common', 'Тип'),
            'tag' => yii::t('common', 'Поле'),
            'value' => yii::t('common', 'Значение'),
            'created_at' => yii::t('common', 'Создано'),
            'updated_at' => yii::t('common', 'Обновлено'),
            'old_tags' => yii::t('common', 'Старые поля'),
            'model' => yii::t('common', 'Модель'),
            'user_id' => yii::t('common', 'ID пользователя'),
        ];
    }

    public function getTypes()
    {
        return Logger::find()->select(['type'])->groupBy('type')->all();
    }

    public function getActions()
    {
        return Logger::find()->select(['action'])->groupBy('action')->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBaseTagLogs()
    {
        return $this->hasMany(LoggerTag::className(), ['log_id' => 'id']);
    }
}
<?php

namespace common\models;

use common\models\query\UserPartnerQuery;
use common\modules\partner\models\Partner;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_partner}}".
 *
 * @property integer $user_id
 * @property integer $partner_id
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Partner $partner
 * @property User $user
 */
class UserPartner extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_partner}}';
    }
    
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'partner_id'], 'required'],
            [['user_id', 'partner_id', 'active', 'created_at', 'updated_at'], 'integer'],
            [
                ['partner_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Partner::className(),
                'targetAttribute' => ['partner_id' => 'id']
            ],
            [
                ['user_id'],
                 'exist',
                 'skipOnError' => true,
                 'targetClass' => User::className(),
                 'targetAttribute' => ['user_id' => 'id']
            ],
            ['active', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id'    => Yii::t('common', 'Пользователь'),
            'partner_id' => Yii::t('common', 'Партнер'),
            'active'     => Yii::t('common', 'Active'),
            'created_at' => Yii::t('common', 'Время создания'),
            'updated_at' => Yii::t('common', 'Время изменения'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return UserPartnerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserPartnerQuery(get_called_class());
    }
}

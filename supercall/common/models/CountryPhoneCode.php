<?php

namespace common\models;

use common\components\db\ActiveRecord;

//use common\models\query\TimezoneQuery;
use common\components\ZorraHLR;
use Yii;
use yii\helpers\VarDumper;

/**
 * Class CountryPhoneCode
 * @package common\models
 * @property integer $country_id
 * @property integer $phone_code
 * @property integer $phone_length_min
 * @property integer $phone_length_max
 * @property integer $check_phone
 * @property integer $use_zorra_validate
 *
 */
class CountryPhoneCode extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country_phone_code}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[['country_id', 'phone_code', 'phone_length_min', 'phone_length_max', 'use_zorra_validate'], 'required'],
            [['country_id', 'phone_code', 'phone_length_min', 'phone_length_max', 'check_phone', 'use_zorra_validate'], 'integer']];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('common', 'ID страны'),
            'phone_code' => Yii::t('common', 'Телефонный код'),
            'phone_length_min' => Yii::t('common', 'Минимальная длина номера'),
            'phone_length_max' => Yii::t('common', 'Максимальная длина номера'),
            'check_phone' => Yii::t('common', 'Проверять номер?'),
            'use_zorra_validate' => Yii::t('common', 'Zorra валидация'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    static public function checkPhone($phone = false, $country = false, $debug = false)
    {
        if (!$country || !$phone || empty($phone)) {
            return $phone;
        }

        $codeData = self::findOne(['country_id' => (int)$country]);
        if (!$codeData || !$codeData->check_phone) {
            return $phone;
        }

        $phone = ltrim($phone, '0');

        $checkRes = mb_strpos($phone, $codeData->phone_code);

        if ($debug) {
            echo 'check phone ' . var_dump($checkRes) . PHP_EOL;
        }

        if ($checkRes == false) { // || $checkRes > 0
            $newPhone = $codeData->phone_code . $phone;
            if ($debug) {
                echo 'new Phone ' . $newPhone . PHP_EOL;
            }

            if ($debug) {
                echo "check phone " . PHP_EOL;
                echo "strlen new phone " . strlen($newPhone) . PHP_EOL;
                echo "min " . $codeData->phone_length_min . PHP_EOL;
                echo "max " . $codeData->phone_length_max . PHP_EOL;
            }

            if (strlen($newPhone) < $codeData->phone_length_min || strlen($newPhone) > $codeData->phone_length_max) {
                return $phone;
            }

            return $newPhone;
        }

        return $phone;
    }

    /**
     * @param string $phone
     * @param int $country
     * @return bool
     */
    public function validatePhone($phone, $country)
    {
        $codeData = self::findOne(['country_id' => (int)$country]);

        if (is_null($codeData)) {
            return false;
        }

        $phone = ltrim($phone, '0');

        if (strlen($phone) >= $codeData->phone_length_min && strlen($phone) <= $codeData->phone_length_max) {
            $phoneCode = substr($phone, 0, strlen((string)$codeData->phone_code));

            if ($phoneCode == $codeData->phone_code) {
                return true;
            }
        }

        $newPhone = $codeData->phone_code . $phone;

        if (strlen($newPhone) >= $codeData->phone_length_min && strlen($newPhone) <= $codeData->phone_length_max) {
            return true;
        }

        return false;
    }
}

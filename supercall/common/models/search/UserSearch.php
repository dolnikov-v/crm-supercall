<?php
namespace common\models\search;

use backend\modules\access\models\AuthAssignment;
use common\models\Teams;
use common\models\User;
use common\models\UserCountry;
use common\models\UserPartner;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class UserSearch
 * @package common\models\search
 */
class UserSearch extends User
{
    public $username;
    public $user_id;
    public $team_id;
    public $role;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'user_id', 'role', 'team_id', 'country_id'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('common', 'Имя пользователя'),
            'user_id' => Yii::t('common', 'ID пользователя'),
            'role' => Yii::t('common', 'Роль'),
            'team_id' => Yii::t('common', 'Команда'),
        ];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort[User::tableName() . '.id'] = SORT_DESC;
        }

        $query = User::find()
            ->where(['!=', '(status & ' . User::STATUS_DELETED . ')', User::STATUS_DELETED])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];
        
        $config['pagination'] = [
            'pageSize' => $withPagination == true ? 20 : 0,
        ];
        
        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', User::tableName().'.username', $this->username]);
        $query->andFilterWhere([User::tableName().'.id' => $this->user_id]);

        if($this->team_id) {
            $users = Teams::getUsers($this->team_id);

            if(is_array($users)){
                $user_ids = ArrayHelper::getColumn($users, 'id');
                $query->andFilterWhere(['user.id' => $user_ids]);
            }
        }else{
            $userModel = yii::$app->user->getUserModel();
            if(!$userModel->isSuperadmin) {
                $users = $userModel->getCoTeamsUsers();
                if(!empty($users)){
                    $user_ids = ArrayHelper::getColumn($users, 'id');
                    $query->andFilterWhere(['user.id' => $user_ids]);
                }
            }
        }


        if ($this->role) {
            $query->joinWith('authAssignment')
                ->andWhere(['like', '"auth_assignment".item_name', $this->role]);
        }

        if($this->country_id){
            $query->leftJoin(UserCountry::tableName(), UserCountry::tableName().'.user_id='.User::tableName().'.id');
            $query->andWhere([UserCountry::tableName().'.country_id' => $this->country_id]);
        }

        if(!Yii::$app->user->isSuperadmin){
            $partner = UserPartner::findOne(['user_id' => Yii::$app->user->id]);
            if($partner){
                $query->leftJoin(UserPartner::tableName(),UserPartner::tableName().'.user_id = '.User::tableName().'.id');
                $query->andWhere([UserPartner::tableName().'.partner_id' => $partner->partner_id]);
            }
        }

        return $dataProvider;
    }
}

<?php
namespace common\models\search;

use common\models\CurrencyRate;
use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class CurrencyRateSearch
 * @package common\models\search
 */
class CurrencyRateSearch extends CurrencyRate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = CurrencyRate::find()->orderBy($sort);

        if (!Yii::$app->user->can(User::ROLE_SUPERADMIN)) {

            $countries = Yii::$app->user->getAllowCountries();

            $currencyId = [];

            foreach ($countries as $country) {
                if (isset($country->currency)) {
                    $currencyId[] = $country->currency->id;
                }
            }

            $query->andWhere(['currency_id' => $currencyId]);
        }

        $config = [
            'query' => $query,
        ];

        $config['pagination'] = [
            'pageSize' => $withPagination == true ? 20 : 0,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

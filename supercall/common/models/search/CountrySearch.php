<?php
namespace common\models\search;

use common\models\Country;
use yii\data\ActiveDataProvider;
/**
 * Class CountrySearch
 * @package common\models\search
 */
class CountrySearch extends Country
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string', 'max' => 32],
            ['char_code', 'string', 'max' => 3],
            ];
    }


    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = Country::find()
            ->with(['timezone', 'currency'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->name]);
        $query->andFilterWhere(['id' => $this->char_code]);

        return $dataProvider;
    }
}

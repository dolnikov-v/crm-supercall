<?php
namespace common\models\search;

use common\models\Language;
use common\models\Message;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class MessageSearch
 * @package common\models\search
 */
class MessageSearch extends Message
{
    public $phrase;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'translation', 'phrase'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $languagesKeys = array_keys(Language::find()->collection());

        $query = Message::find();

        $query->andFilterWhere(['in', 'language', $languagesKeys]);

        $query->joinWith('sourceMessage')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['language' => $this->language]);
        $query->andFilterWhere(['like', 'translation', $this->translation]);
        $query->andFilterWhere(['like', 'i18n_source_message.message', $this->phrase]);

        return $dataProvider;
    }
}

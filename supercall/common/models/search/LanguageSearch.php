<?php
namespace common\models\search;

use common\models\Language;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class LanguageSearch
 * @package common\models\search
 */
class LanguageSearch extends Language
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [
            'active' => SORT_DESC,
        ];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = Language::find()->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

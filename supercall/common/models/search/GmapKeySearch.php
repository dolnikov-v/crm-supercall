<?php
namespace common\models\search;

use common\models\GmapKey;
use yii\data\ActiveDataProvider;

/**
 * Class GmapKeySearch
 * @package common\models\search
 */
class GmapKeySearch extends GmapKey
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [
            'active' => SORT_DESC,
        ];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = GmapKeySearch::find()->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

<?php
namespace common\models\search;

use common\models\Teams;
use common\models\TeamsCountry;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class TeamSearch
 * @package common\models\search
 */
class TeamSearch extends Teams
{
    public $name;
    public $team_id;
    public $active;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'active', 'team_id', 'country_id'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Название команды'),
            'team_id' => Yii::t('common', 'Название команды'),
            'country_id' => Yii::t('common', 'Страна'),
            'active' => Yii::t('common', 'Статус команды'),
            'created_at' => Yii::t('common', 'Дата создания'),
        ];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort[Teams::tableName() . '.name'] = SORT_ASC;
        }

        $query = Teams::find()
            ->joinWith('teamsCountry')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $config['pagination'] = [
            'pageSize' => $withPagination == true ? 20 : 0,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', Teams::tableName().'.name', $this->name]);
        $query->andFilterWhere([Teams::tableName().'.id' => $this->team_id]);

        if($this->country_id){
            $query->andFilterWhere([TeamsCountry::tableName().'.country_id' => $this->country_id]);
        }else {
            $query->andFilterWhere([TeamsCountry::tableName() . '.country_id' => ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id')]);
        }

        return $dataProvider;
    }
}

<?php
namespace common\models\search;

use common\models\Language;
use common\models\Message;
use common\models\SourceMessage;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class SourceMessageSearch
 * @package common\models\search
 */
class SourceMessageSearch extends SourceMessage
{
    public $isFullyTranslation = 'no';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'message'], 'safe'],
            ['isFullyTranslation', 'in', 'range' => ['', 'no', 'yes']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(['isFullyTranslation' => Yii::t('common', 'Полный перевод')], parent::attributeLabels());
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $activeLanguages = array_keys(Language::find()->collection());
        $countActiveTranslations = count($activeLanguages);
        $str = implode("', '", $activeLanguages);

        $subSelect = "SELECT COUNT(*) FROM " . Message::tableName() . " WHERE language IN ('" . $str . "') AND id = " . SourceMessage::tableName() . ".id";

        $query = SourceMessage::find()
            ->select(['*', "CASE ($subSelect) WHEN $countActiveTranslations THEN 1 ELSE 0 END AS is_fully_translation"])
            ->with('messages')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $config['pagination'] = [
            'pageSize' => $withPagination == true ? 20 : 0,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if ($params) {
            if (!$this->load($params) || !$this->validate()) {
                return $dataProvider;
            }
        }

        $query->andFilterWhere(['like', 'category', $this->category]);
        $query->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}

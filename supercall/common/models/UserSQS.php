<?php

namespace common\models;
use yii;

/**
 * Class UserSQS
 * @package common\models
 */
class UserSQS extends AmazonSQS
{
    const LOGGER_TYPE = 'usersqs';

    //получаем
    const AMAZON_ACTIONS_USER_FROM_ERP = 'https://sqs.eu-west-1.amazonaws.com/636470419474/ActionWithUserFromERP';

    //отправляем
    const AMAZON_ACTIONS_USER_FROM_CRM = 'https://sqs.eu-west-1.amazonaws.com/636470419474/ActionWithUserFromCRM';

    const ACTION_DISMISSAL = 'dismissal';

    const ACTION_EMPLOY = 'employ';

    public $handlers;

    /**
     * @param int $number
     * @param array $attributes
     * @return array|bool
     */
    public function getActions($number = 10, $attributes = ['All'])
    {
        return $this->getMessages($number, $attributes, self::AMAZON_ACTIONS_USER_FROM_ERP);

    }

    /**
     * @param array $params
     * @return \Aws\Result|bool
     */
    public function sendMessage($params = [])
    {
        return parent::sendMessage($params);
    }

    /**
     * @param $handler
     * @param $queue_url
     * @return boolean
     */
    public function deleteMessage($handler, $queue_url)
    {
        if (yii::$app->params['is_dev']) {
            return true;
        }

        return parent::deleteMessage($handler, $queue_url);
    }
}
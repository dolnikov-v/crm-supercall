<?php

namespace common\models;

use backend\modules\access\models\AuthAssignment;
use common\components\db\ActiveRecord;
use common\models\User as UserModel;
use common\models\User;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use backend\modules\queue\models\Queue;
use backend\modules\queue\models\QueueUser;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use backend\modules\operator\models\ProductOperator;
use backend\modules\order\models\OrderBackup;
use common\models\CountryPhoneCode;

/**
 * This is the model class for table "auto_call".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $order_phone
 * @property string $cdr_id
 * @property string $call_status
 * @property string $queue_name
 * @property string $group_queue
 * @property integer $status
 * @property string $user_sip
 * @property integer $call_time
 * @property integer $country_id
 *
 * @property Order $order
 */
class AutoCall extends ActiveRecord
{
    const MAX_COUNT_CALL_OPERATORS = 3;

    const DEBUG = true;
    const NODEBUG = false;
    const PAUSEON = 1;
    const PAUSEOFF = 0;
    const URL_ASTER_AUTO_CALL = 'http://asterweb.2wcall.com/index.php?r=v1/auto-call/call';
    const URL_REFRESH_OPERS_ACTIVITY = 'http://asterweb.2wcall.com/index.php?r=v1/asterisk/refresh-opers-activity';
    const URL_PAUSED_MEMBER = 'http://asterweb.2wcall.com/index.php?r=v1/asterisk/pause-member';
    const URL_MONITOR = 'http://asterweb.2wcall.com/index.php?r=v1/asterisk/monitor';
    const UNPAUSE_OPERATORS = 'http://asterweb.2wcall.com/index.php?r=v1/asterisk/unpause-operators';
    const URL_SET_USER_PAUSED = 'http://asterweb.2wcall.com/index.php?r=v1/auto-call/set-user-paused';
    const URL_SET_OPERATORS_IN_QUEUES = 'http://asterweb.2wcall.com/index.php?r=v1/auto-call/set-operators-in-queues';
    const URL_CREATE_QUEUE = 'http://asterweb.2wcall.com/index.php?r=v1/auto-call/create-queue';

    const ACTION_SEARCH_FORM = 'search-form';
    const ACTION_OPEN_FORM = 'open-form';
    const ACTION_HANGUP = 'hangup';
    const ACTION_LOST = 'lost';

    const STATUS_ANSWERED = 'Answered';

    CONST DISPOSITION_ANSWERED = 'ANSWERED';
    CONST DISPOSITION_FAILED = 'FAILED';
    CONST DISPOSITION_BUSY = 'BUSY';

    public $countries = [];
    public $usersOnline = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auto_call}}';
    }

    public function behaviors()
    {
        return [];
    }

    /**
     * @return array
     */
    public static function CollectionActions()
    {
        return [
            self::ACTION_SEARCH_FORM,
            self::ACTION_OPEN_FORM,
            self::ACTION_HANGUP,
            self::ACTION_LOST,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'order_id' => Yii::t('common', 'Номер заказа'),
            'cdr_id' => Yii::t('common', 'CDR'),
            'call_status' => Yii::t('common', 'Статус звонка'),
            'queue_name' => Yii::t('common', 'Очередь'),
            'group_queue' => Yii::t('common', 'Группа'),
            'status' => Yii::t('common', 'Статус'),
            'user_sip' => Yii::t('common', 'SIP поператора'),
            'call_time' => Yii::t('common', 'Время звонка'),
        ];
    }

    /**
     * @return array
     */
    public function getUsersOnline()
    {
        $this->usersOnline = UserReady::getFullUserOnline(['auto_call' => User::AUTO_CALL_ON]);
        return $this->usersOnline;
    }

    public function clearAutoCallTable()
    {
        $time = (new \DateTime('now', new \DateTimeZone(Timezone::DEFAULT_TIMEZONE)))->getTimestamp();

        self::deleteAll('"call_status" in (\'LOST\', \'BUSY\', \'NO ANSWER\', \'FAILED\', \'\')');
        self::deleteAll('("call_status" = \'ANSWERED\' AND "status" = \'0\' AND "call_time" < :time and user_sip = \'\') ', ['time' => $time - 600]);
        self::deleteAll('("call_status" = \'\' AND "call_time" < :time and user_sip = \'\') ', ['time' => $time - 240]);


        //self::deleteAll('"call_status" in (\'ANSWERED\') and cdr_id = \'\'');
    }

    /**
     * @param $country
     * @return false|null|string
     */
    public function getCountOrdersInCalling($country)
    {
        return AutoCall::find()
            ->select('count(order_id)')
            ->where(['=', 'status', 0])
            ->andWhere([
                'OR',
                [
                    'AND',
                    ['=', 'call_status', ''],
                    ['=', 'country_id', $country]
                ],
                [
                    'AND',
                    ['=', 'call_status', 'ANSWERED'],
                    ['=', 'user_sip', ''],
                    ['=', 'country_id', $country]
                ]
            ])
            ->scalar();
    }


    /**
     * @return UserCountry[]
     */
    public function getUsersFullInfo()
    {
        return UserModel::find()
            ->select(['user.id', 'user_country.country_id', 'user.sip'])
            ->leftJoin('user_country', "user_country.user_id = " . UserModel::tableName() . ".id")
            ->where(['in', 'id', $this->usersOnline])
            ->andWhere(['=', 'auto_call', 1])
            ->andWhere(['=', 'is_ready', 1])
            ->asArray()
            ->all();
    }

    /**
     * подбор заказов для операторов с учётом стран и настройек автодозвонов по странам
     * @param $debug
     * @return array
     */
    public function generateOrders($debug)
    {
        $this->usersOnline = $this->getUsersOnline();

        if(empty($this->usersOnline)){
            return [];
        }

        Console::output(Yii::$app->formatter->asDatetime(time()) . ' Start GENERATE orders');

        Console::output('Count operators: ' . count($this->usersOnline));

        $orders = [];

        if (!empty($this->usersOnline )) {
            $this->countries = ArrayHelper::map(Country::find()->active()->all(), 'id', 'slug');

            $usersByCountry = [];


            $autoCallCountries = ArrayHelper::map(AutoCallCountry::find()->orderBy(['id' => SORT_ASC])->all(), 'country_id', function ($item) {
                return !is_numeric($item->count) ? 1 : $item->count;
            });

            $autoCallCountries_id = array_keys($autoCallCountries);

            /** @var User $user */
            foreach ($this->usersOnline as $user) {
                $sipData = Json::decode($user->sip);

                /** @var UserCountry $userCountry */
                //берём только те страны - для которых включен автодозвон
                foreach ($user->getUserCountry()->where(['country_id' => $autoCallCountries_id])->all() as $userCountry) {
                    if (in_array($userCountry->country_id, array_keys($this->countries))) {
                        $usersByCountry[$userCountry->country_id][] = [
                            'sip' => $sipData['sip_login'],
                            'id' => $user->id,
                            'country_id' => ArrayHelper::getColumn($user->userCountry, 'country_id')
                        ];
                    }
                }
            }

            //$this->clearAutoCallTable();

            //сортировать согласно списку стран автодозвона
            uksort($usersByCountry, function ($k1, $k2) use ($autoCallCountries_id) {
                return array_search($k1, $autoCallCountries_id) > array_search($k2, $autoCallCountries_id);
            });

            foreach ($usersByCountry as $country => $users) {
                if (in_array($country, array_keys($autoCallCountries))) {
                    $ordersNeeded = $autoCallCountries[$country] - $this->getCountOrdersInCalling($country);

                    foreach ($users as $user) {
                        $foundedOrders = $this->getOrders($user['id'], $country, $ordersNeeded, true, AutoCall::NODEBUG);

                        foreach ($foundedOrders as $order) {
                            $preData = ['order_id' => $order['id'],
                                'order_phone' => ltrim($order['phone'], '0'),/*79133869812,*/
                                'queue_name' => $order['asterisk_queue_name'],
                                'country_id' => $order['country_id'],
                                'country_char_code' => $order['country_char_code'],
                                'user_id' => $order['user_id'],
                                'user_sip' => $user['sip']
                            ];

                            $orders[$user['id']][] = $preData;

                            if ($debug) {
                                Console::output(Json::encode($preData));
                            }
                        }
                    }
                }
            }
        }

        return $orders;
    }

    /**
     * @param $user_id
     * @param $country_id
     * @param $count
     * @param bool $blockByAsterisk
     * @param bool $debug
     * @return array
     */
    public function getOrders($user_id, $country_id, $count, $blockByAsterisk = false, $debug = true)
    {
        // Только из очередей, доступных данному Оператору
        $accessQueues = QueueUser::find()
            ->select(['queue_id'])
            ->where(['user_id' => $user_id])
            ->column();

        if ($debug) {
            Console::output("accessQueues for user_id " . $user_id . " : " . print_r($accessQueues, 1));
        }

        $activeQueues = Queue::find()
            ->byCountry($country_id)
            ->priority()
            ->active()
            ->andWhere(['in', Queue::tableName() . '.id', $accessQueues])
            ->all();

        if ($debug) {
            Console::output("activeQueues: " . count($activeQueues));
        }

        $orders = [];

        $product = ArrayHelper::getColumn(ProductOperator::findAll(['user_id' => $user_id]), 'product_id');

        while ($count > 0) {
            --$count;

            foreach ($activeQueues as $queue) {
                $queue_id = $queue->id;

                if ($debug) {
                    Console::output("Queue id: " . $queue_id);
                    Console::output("Queue name: " . $queue->name);
                }

                $message = Yii::$app->amqp->basicGet($queue_id);

                if (isset($message)) {
                    Yii::$app->amqp->basicAck($message->delivery_info['delivery_tag']);
                    $orderId = (int)$message->body;

                    if ($debug) {
                        Console::output("found order ID " . $orderId);
                    }

                    $query = Order::find()
                        ->leftJoin(OrderProduct::tableName(),
                            OrderProduct::tableName() . '.order_id = ' . Order::tableName() . '.id')
                        ->where([Order::tableName() . '.id' => $orderId]);


                    if (!empty($product)) {
                        $query->andWhere(['in', OrderProduct::tableName() . '.product_id', $product]);
                    }

                    if ($query->exists()) {
                        $order = $query->one();

                        $checkedPhone = CountryPhoneCode::checkPhone($order->customer_phone, $order->country_id);

                        $order->customer_phone = $checkedPhone;

                        $current_time = (new \DateTime())
                            ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
                            ->getTimestamp();

                        $blocked_to = $current_time + 1800; // в секундах
                        $orderId = $order->id;

                        Order::updateByParams(['blocked_to' => $blocked_to], ['id' => $orderId]);

                        $asteriskQueueName = 'cc_' . $queue->id;

                        $orders[] = [
                            'id' => $order->id,
                            'blocked_to' => $blocked_to,
                            'status' => $order->status,
                            'phone' => $order->customer_phone,
                            'queue_id' => $order->last_queue_id,
                            'country_id' => $order->country_id,
                            'country_char_code' => $order->country->char_code,
                            'asterisk_queue_name' => $asteriskQueueName,
                            'user_id' => $user_id
                        ];;
                    }
                } else {
                    if ($debug) {
                        Console::output("Not find message in queue " . $queue->name);
                    }
                }
            }
        }

        if (!empty($orders)) {
            if ($blockByAsterisk) {
                $user_id = Asterisk::getSystemUser();
            }

            foreach($orders as $order) {
                Yii::$app->db->createCommand()->insert('{{%queue_order}}', [
                    'user_id' => $user_id,
                    'order_id' => $order['id'],
                    'blocked_to' => $order['blocked_to'],
                ])->execute();
            }

            return $orders;
        }

        return [];
    }

    public function trashOrder($id)
    {
        $order = Order::findOne($id);
        $order->status = $order::STATUS_TRASH;
        $order->sub_status = $order::SUB_STATUS_TRASH_REASON_WRONG_NUMBER;
        $params = $order->dirtyAttributes;
        if (key_exists('status', $params)) {
            $order->saveLogsByOrder();
        }
        Order::updateByParams($params, ['id' => $order->id]);

        Yii::$app->db->createCommand()->delete('{{%queue_order}}', [
            'order_id' => $order->id
        ])->execute();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}

<?php

namespace common\models;

use backend\modules\access\models\AuthAssignment;
use backend\modules\access\models\AuthItem;
use backend\modules\audit\models\AggregateWord;
use common\components\db\ActiveRecord;
use common\models\query\UserQuery;
use common\modules\partner\models\Partner;
use Yii;
use yii\base\NotSupportedException;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 * Class User
 * @package common\models
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $timezone_id
 * @property string $email
 * @property string $phone
 * @property string $first_name
 * @property string $last_name
 * @property string $auth_key
 * @property integer $status
 * @property string $registration_token
 * @property integer $created_at
 * @property integer $updated_at
 * @property boolean $isSuperadmin
 * @property boolean $isReady
 * @property boolean $check_unready
 * @property json $sip
 * @property Country[] $countries
 * @property AuthAssignment[] $roles
 * @property integer $auto_call
 * @property boolean $incoming_call
 * @property integer $last_activity
 * @property string $ip
 * @property string $outer_ip
 * @property AggregateWord[] $aggregateWords
 * @property Partner[] $partners
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DEFAULT = 0x00;
    const STATUS_BLOCKED = 0x01;
    const STATUS_DELETED = 0x02;
    const STATUS_NEW = 0x03;
    const STATUS_APPLICANT = 0x04;

    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';

    const ROLE_SUPERADMIN = 'superadmin';
    const ROLE_OPERATOR = 'operator';
    const ROLE_CONTROLLER = 'controller';
    const ROLE_CURATOR = 'curator';
    const ROLE_SUPERVISOR = 'supervisor';
    const ROLE_APPLICANT = 'applicant';
    const ROLE_AUDITOR = 'auditor';
    const ROLE_SENIOR_OPERATOR = 'senior_operator';
    const ROLE_TEAM_LEAD = 'team_lead';
    const ROLE_SALES_DIRECTOR = 'sales_director';
    const ROLE_QUALITY_MANAGER = 'quality_manager';
    const ROLE_LOGISTICIAN = 'logistician';
    const ROLE_ADMINISTRATOR = 'administrator';
    const ROLE_SALES_TRAINER = 'saletrainer';

    const AUTO_CALL_ON = 1;
    const AUTO_CALL_OFF = 0;

    const USE_MESSENGER_ON = true;
    const USE_MESSENGER_OFF = false;

    const WEBRTC_ON = true;
    const WEBRTC_OFF = false;

    const USER_READY = true;
    const USER_NOT_READY = false;

    public $country;
    public $timezone;
    public $partner;
    public $sipArray;
    public $check_unready;
    public $role;
    public $document;
    public $country_id;

    /** @var  [] */
    public $countriesList;
    /** @var  [] */
    public $queuesList;
    /** @var  [] */
    public $productsList;

    public $_sip_login;

    public $profile_image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_DEFAULT => yii::t('common', 'По умолчанию'),
            self::STATUS_BLOCKED => yii::t('common', 'Заблокирован'),
            self::STATUS_DELETED => yii::t('common', 'Удален'),
            self::STATUS_NEW => yii::t('common', 'Новый'),
            self::STATUS_APPLICANT => yii::t('common', 'Заявитель'),
        ];
    }

    /**
     * @return array
     */
    public static function blockedStatusCollection()
    {
        return [
            self::STATUS_DELETED,
            self::STATUS_BLOCKED
        ];
    }


    public static function getTextStatus($status_id)
    {
        $statuses = self::getStatuses();

        return isset($statuses[$status_id]) ? $statuses[$status_id] : 'undefined';
    }


    public static function getApplicantStatusCollection()
    {
        return [
            self::STATUS_BLOCKED => yii::t('common', 'Заблокированный / Отклонённый'),
            self::STATUS_APPLICANT => yii::t('common', 'Ожидает активации')
        ];
    }

    public static function getApplicantRolesCollection()
    {
        return [
            self::ROLE_APPLICANT => yii::t('common', 'Заявитель'),
            self::ROLE_OPERATOR => yii::t('common', 'Оператор')
        ];
    }


    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($username)
    {
        return static::findOne(['email' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'required', 'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]],
            ['password', 'required', 'on' => self::SCENARIO_INSERT],
            [['is_ready', 'check_unready'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
            [['username', 'password'], 'string', 'max' => 64],
            [['username', 'password'], 'filter', 'filter' => 'trim'],
            [['sip', 'ip'], 'string'],
            [['sipArray'], 'safe'],
            [['first_name', 'last_name'], 'safe'],
            [['email'], 'email'],
            [['document'], 'safe'],
            [['document'], 'file', 'extensions' => 'jpg, gif, png'],
            [['profile_image'], 'file', 'extensions' => 'jpg, gif, png'],
            [
                'timezone_id',
                'exist',
                'targetClass' => '\common\models\Currency',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неверное значение временной зоны.'),
            ],
            [
                'username',
                'unique',
                'targetClass' => '\common\models\User',
                'message' => Yii::t('common', 'Это имя пользователя уже используется.'),
            ],
            ['password', 'validateChangePassword', 'skipOnEmpty' => false],
            ['status', 'default', 'value' => self::STATUS_DEFAULT],
            [['auto_call', 'last_activity'], 'integer'],
            [['webrtc', 'incoming_call', 'use_messenger'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'username' => Yii::t('common', 'Имя пользователя'),
            'password' => Yii::t('common', 'Пароль'),
            'timezone_id' => Yii::t('common', 'Временная зона'),
            'email' => Yii::t('common', 'Электронная почта'),
            'status' => Yii::t('common', 'Статус'),
            'created_at' => Yii::t('common', 'Дата добавления'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'rolesNames' => Yii::t('common', 'Роли'),
            'partner' => Yii::t('common', 'Партнеры'),
            'country' => Yii::t('common', 'Страны'),
            'timezone' => Yii::t('common', 'Часовой пояс'),
            'sip' => Yii::t('common', 'SIP'),
            'team_id' => Yii::t('common', 'Команда'),
            'countries' => Yii::t('common', 'Страны'),
            'products' => Yii::t('common', 'Закреплённые товары'),
            'queues' => Yii::t('common', 'Доступ к очередям'),
            'document' => Yii::t('common', 'Документ'),
            'auto_call' => Yii::t('common', 'Автодозвон'),
            'webrtc' => Yii::t('common', 'WebRTC'),
            'incoming_call' => Yii::t('common', 'Входящие звонки'),
            'last_activity' => Yii::t('common', 'Последняя активность'),
            'use_messenger' => Yii::t('common', 'Использовать Messenger'),
            'ip' => Yii::t('common', 'Локальный IP адрес'),
            'outer_ip' => Yii::t('common', 'Внешний IP адрес'),
            'profile_image' => Yii::t('common', 'Фото профиля'),
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateChangePassword($attribute, $params)
    {
        if ($this->isNewRecord) {
            $this->setPassword($this->$attribute);
        } else {
            if (array_key_exists($attribute, $this->getDirtyAttributes())) {
                $newValue = trim($this->$attribute);
                $hash = $this->getOldAttribute($attribute);

                if (!empty($newValue) && !Yii::$app->security->validatePassword($newValue, $hash)) {
                    $this->setPassword($this->$attribute);
                } else {
                    $this->$attribute = $this->getOldAttribute($attribute);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @return boolean
     */
    public function getIsSuperadmin()
    {
        return Yii::$app->authManager->getAssignment(User::ROLE_SUPERADMIN, $this->id);
    }

    /**
     * @return boolean
     */
    public function getIsReady()
    {
        return $this->is_ready;
    }

    /**
     * @return boolean
     */
    public function getCheckUnready()
    {
        return $this->check_unready;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public static function generatePassword($length = 9)
    {
        $password = '';
        $chars = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        for ($i = 0; $i < $length; $i++) {
            $password .= $chars[array_rand($chars)];
        }
        return $password;
    }

    /**
     * @param string $password
     * @return boolean
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param integer $status
     * @return integer
     */
    public function hasStatus($status)
    {
        return $this->status & $status;
    }

    /**
     * Устанавливаем статус
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = ($this->status | $status);

        return $this;
    }

    /**
     * Сбрасываем статус
     * @param integer $status
     * @return $this
     */
    public function unsetStatus($status)
    {
        $this->status = ($this->status & ~$status);

        return $this;
    }

    /**
     * @return string
     */
    public function getRolesNames($asArray = false)
    {
        $roles = [];

        foreach (Yii::$app->authManager->getRolesByUser($this->id) as $role) {
            $roles[] = $role->name;
        }

        return !$asArray ? implode(', ', $roles) : $roles;
    }

    public static function getRoleCollection()
    {
        return yii::$app->db->createCommand("select name, description from auth_item where type = 1")->queryAll();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCountry()
    {
        return $this->hasMany(UserCountry::className(), ['user_id' => 'id']);
    }

    public function getAuthAssignment()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    public function getUserPartner()
    {
        return $this->hasMany(UserPartner::className(), ['user_id' => 'id']);
    }

    public function getUserTeams()
    {
        return $this->hasMany(UserTeam::className(), ['user_id' => 'id']);
    }

    public function getTeams()
    {
        $teams = [];

        if (is_array($this->getUserTeams()->all())) {
            foreach ($this->getUserTeams()->all() as $user_team) {
                $teams[] = $user_team->getTeam()->one();
            }
        }

        return $teams;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['id' => 'country_id'])
            ->where(['active' => 1])
            ->orderBy(['name' => SORT_ASC])
            ->via('userCountry');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAllowCountries()
    {
        //superadmin is the god
        if (yii::$app->user->getIsSuperadmin()) {
            return Country::find()
                ->select([
                    'country_id' => 'id'
                ])
                ->where(['active' => Country::ACTIVE])
                ->orderBy(['name' => SORT_ASC]);
        }

        return UserCountry::find()
            ->where(['user_id' => Yii::$app->user->id]);
    }

    /**
     * @return $this|Partner[]|\yii\db\ActiveQuery
     */
    public function getPartners()
    {
        if($this->getIsSuperadmin()){
            return Partner::findAll(['active' => Partner::ACTIVE]);
        }else {
            return $this->hasMany(Partner::className(), ['id' => 'partner_id'])
                ->where(['active' => 1])
                ->via('userPartner');
        }
    }

    /**
     * Получить все модели команд пользователя
     * @param bool $active
     * @return static[]
     */
    public function getTeamsByUser($active = true)
    {
        if (yii::$app->user->getIsSuperadmin()) {
            return Teams::findAll(['active' => Teams::STATUS_ACTIVE]);
        } else {
            $userTeamQuery = UserTeam::find()->where(['user_id' => Yii::$app->user->id]);

            if ($userTeamQuery->exists()) {
                $team_ids = ArrayHelper::getColumn($userTeamQuery->all(), 'team_id');

                return Teams::findAll([
                    'active' => $active ? Teams::STATUS_ACTIVE : [Teams::STATUS_ACTIVE, Teams::STATUS_NOT_ACTIVE],
                    'id' => $team_ids
                ]);
            }
        }

        return [];
    }

    /**
     * Вернёт всех пользователей - участников команд - в которые включён данный пользователь
     * @return array
     */
    public function getCoTeamsUsers()
    {
        $teams = $this->getTeamsByUser();

        if (empty($teams)) {
            $users = [];
        } else {
            $team_ids = ArrayHelper::getColumn($teams, 'id');
            $users = Teams::getUsers($team_ids);
        }

        return $users;
    }

    /**
     * @return $this
     */
    public function getActiveCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])
            ->via('userCountry', function ($query) {
                /** @var \yii\db\ActiveQuery $query */

                return $query->andWhere([
                    'active' => 1
                ]);
            });
    }

    public function beforeValidate()
    {
        if (!empty($this->sipArray))
            $this->sip = Json::encode($this->sipArray);

        return parent::beforeValidate();
    }

    public function afterFind()
    {
        if (!empty($this->sip)) {
            $this->sipArray = Json::decode($this->sip, true);
        } else {
            $this->sipArray = [
                'sip_host' => '',
                'sip_pass' => '',
                'sip_login' => '',
            ];
        }

        parent::afterFind();
    }

    /**
     * @param string $permission
     * @return bool
     */
    public function can($permission)
    {
        $query = Yii::$app->db->createCommand("
            select *
            from auth_assignment
            left join auth_item_child on parent = item_name
            where child = '" . $permission . "'
            and user_id::integer = " . $this->id)->queryAll();

        return !empty($query);
    }

    /**
     * @param $country_id
     * @param $roles
     * @param bool $active
     * @return UserCountry[]
     */
    public static function getActiveByRole($country_id, $roles, $active = true)
    {
        $roles = is_array($roles) ? $roles : [$roles];
        $country_id = empty($country_id) ? [] : $country_id;
        $country_id = is_array($country_id) ? $country_id : [$country_id];

        $query = User::find()
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id::integer=' . User::tableName() . '.id')
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName() . '.user_id=' . User::tableName() . '.id')
            ->where([AuthAssignment::tableName() . '.item_name' => $roles]);

        if (!empty($country_id)) {
            $query->andWhere(['in', UserCountry::tableName() . '.country_id', ArrayHelper::getColumn($country_id, 'id')]);
        }

        if ($active) {
            $query->andWhere(['not in', User::tableName() . '.status', [User::STATUS_BLOCKED, User::STATUS_DELETED]]);
        }

        return $query->all();
    }

    public function getMedia()
    {
        return $this->hasMany(Media::className(), ['user_id' => 'id']);
    }

    /**
     * @param $user_id
     * @return array
     */
    public static function blockUser($user_id)
    {
        $model = User::findOne(['id' => $user_id]);

        if ($model) {
            $model->status = self::STATUS_BLOCKED;

            if (!$model->save()) {
                return [
                    'success' => false,
                    'message' => 'Ошибка смены статуса (' . self::STATUS_BLOCKED . ') для пользователя ' . $user_id . ' : ' . json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE)
                ];
            }
        } else {
            return [
                'success' => false,
                'message' => 'Пользователь ' . $user_id . ' не найден в CRM'
            ];
        }

        return [
            'success' => true
        ];
    }

    /**
     * @param $id
     * @return static
     */
    public static function getById($id)
    {
        return User::findOne(['id' => $id]);
    }

    /**
     * @return array
     */
    public static function setLastActivity()
    {
        if (yii::$app->user->isGuest) {
            if (isset($_GET['user_id']) && !empty($_GET['user_id'])) {
                $user_id = $_GET['user_id'];
            } else {
                return true;
            }
        } else {
            if (isset(Yii::$app->user->identity)) {
                $user_id = (int)yii::$app->user->identity->id;
            } else {
                return true;
            }
        }


        $sql = 'update "user" set last_activity = ' . time() . ' where id = :user_id';
        $query = yii::$app->db->createCommand($sql)->bindParam('user_id', $user_id)->execute();

        if (!$query) {
            return [
                'success' => false,
                'message' => 'last activity not fixed : ' . $sql
            ];
        } else {
            return [
                'success' => true
            ];
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAggregateWords()
    {
        return $this->hasMany(AggregateWord::className(), ['user_id' => 'id']);
    }
}

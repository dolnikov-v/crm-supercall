<?php

namespace common\models;


use api\models\ApiLog;
use Aws\Exception\AwsException;
use Aws\Sqs\SqsClient;
use backend\modules\order\models\AddressVerification;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderType;
use common\modules\partner\helpers\PartnerProduct;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use common\modules\partner\models\PartnerProduct as PartnerProductModel;
use common\modules\partner\models\PartnerShipping;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


class LeadSQS extends AmazonSQS
{

    /**
     * API KEY
     */
    const AMAZON_API_KEY = 'AKIAJ7LAO7MJMRVM3GSA';

    /**
     * SECRET KEY
     */
    const AMAZON_SECRET_KEY = 'Ho9HW7+elOafUKY1JqCAzwmIrNyqC4wpmqUOguT7';

    /**
     * REGION
     */
    const AMAZON_REGION = 'eu-west-1';

    /**
     * URL очереди на новые лиды(заказы)
     */
    const AMAZON_LEADS_QUEUE = 'https://sqs.eu-west-1.amazonaws.com/636470419474/newLeads';

    /**
     * URL очереди для невалидных лидов
     */
    const AMAZON_ERROR_LEADS_QUEUE = 'https://sqs.eu-west-1.amazonaws.com/636470419474/LeadSendError';
    const AMAZON_ERROR_LEADS_QUEUE_DEV = 'https://sqs.eu-west-1.amazonaws.com/636470419474/dev_LeadSendError';
    /**
     * URL очереди для отправки созданных заказов на КЦ в Пэй
     */
    const AMAZON_NEW_LEAD_TO_PAY = 'https://sqs.eu-west-1.amazonaws.com/636470419474/newLeadFromCC';

    /**
     * URL очереди для подхвата foreign id из пея. Если пей не смог по апи нам обновить у заказа foreign id
     */
    const AMAZON_GET_FOREIGN_ID = 'https://sqs.eu-west-1.amazonaws.com/636470419474/newLeadFromCCResponse';

    /**
     * URL очереди для подхвата перзвонов с пея
     */
    const AMAZON_GET_RECALL_LEAD_FROM_PAY = 'https://sqs.eu-west-1.amazonaws.com/636470419474/LeadRecallFromPay';

    /**
     *  идентификатор группировки в очереди для FIFO
     */
    const MESSAGE_GROUP_ID = 'get-statuses';

    /**
     *  обязательно на пей с этим source
     */
    const SOURCE = '2wcall';

    const ERROR_TYPE_UNKNOWN_PARTNER = 1;
    const ERROR_TYPE_UNKNOWN_QUEUE = 2;
    const ERROR_TYPE_ORDER_NOT_FOUND = 3;

    /**
     * объект
     * @object SqsClient
     */
    public $amazonClient;

    /**
     * Храним текущие необработанные ИДшники итемов
     * @array
     */
    public $handlers;

    public $log;

    /**
     * @var
     */
    public $error;

    /**
     * Берем из очереди item
     * @param int $number
     * @param array $attributes
     * @return array|bool
     */
    public function getLeads($number = 10, $attributes = ['All'])
    {
        return $this->getMessages($number, $attributes, LeadSQS::AMAZON_LEADS_QUEUE);
    }

    /**
     * Добавляем ошибки в очередь
     * @param string $message
     * @return \Aws\Result|bool
     */
    public function addItemErrorQueue($message = '')
    {
        if (Yii::$app->params['is_dev']) {
            return true;
        }

        $params = [
            'MessageBody' => $message,
            'QueueUrl' => LeadSQS::AMAZON_ERROR_LEADS_QUEUE
        ];

        return $this->sendMessage($params);
    }

    /**
     * Добавляем ошибки в очередь для DEV
     * @param string $message
     * @return \Aws\Result|bool
     */
    public function addItemDevErrorQueue($message = '')
    {
        $params = [
            'MessageBody' => $message,
            'QueueUrl' => LeadSQS::AMAZON_ERROR_LEADS_QUEUE_DEV
        ];

        return $this->sendMessageToDev($params);
    }

    /**
     * Отправлем измененный ордер в очередь
     * @param $order
     * @return \Aws\Result|bool|string
     */
    public function sendLeadStatus($order)
    {
        //Устанавливаем статус в false перед отправкой
        Yii::$app->db->createCommand()
            ->update('{{%order}}', ['sent_to_sqs' => false], ['id' => $order->id])
            ->execute();

        $order->partner_token = $order->partner->token;

        $prepareMessage = $this->createOrderMessageFifo($order, $order->partner->url_queue);

        ob_start();
        var_dump($prepareMessage);
        $bodyMessage = ob_get_contents();
        ob_end_clean();

        if (empty($prepareMessage)) {
            echo 'Empty $prepareMessage : ' . $bodyMessage;
            return false;
        }

        $prepareMessage = json_decode($prepareMessage['MessageBody'], true);

        //Подтягиваем историю
        $history = OrderLog::find()
            ->select(['field', 'old', 'new', 'old', 'user_id', 'operator'])
            ->where(['order_id' => $order->id])
            ->orderBy(['id' => 'ASC'])
            ->asArray()->all();

        //Пока не перевели все коллы, подстраивается под старый КЦ
        $callData = CallHistory::find()
            ->select([
                'record_file' => new Expression("case when disposition = '".CallHistory::DISPOSITION_ANSWERED."' then concat('".CallHistory::ASTERISK_SERVER_URL."', '".CallHistory::FULL_PATH_PATTERN."', ".CallHistory::tableName() . ".recordingfile) else null end "),
                'record_date' => new Expression("CONCAT(to_timestamp(" . CallHistory::tableName() . ".created_at)::date, ' ', to_timestamp(" . CallHistory::tableName() . ".created_at)::time)"),
                'record_phone' => CallHistory::tableName() . '.phone',
                'user_sip' => CallHistory::tableName() . '.user_id',
            ])
            ->where(['order_id' => $order->id])
            ->asArray()
            ->all();

        $approveOperator = null;
        //Ищем в ней первую смену
        foreach ($history as $value) {
            if ($value['field'] == 'status' && !empty($value['user_id'])) {
                $approveOperator = $value['user_id'];
            }
        }

        $form = PartnerForm::find()->where(['id' => $prepareMessage['form_id']])->one();
        $extraPrice = 0;
        if (isset($form) && isset($form->extra_price)) {
            $extraPrice = $form->extra_price;
        }


        //Время блокировки
        $hold_to = 0;
        if ($order->status == Order::STATUS_APPROVED && $order->confirm_address == AddressVerification::ADDRESS_NEED_CONFIRM && $order->form->check_address) {
            $hold_to = $prepareMessage['updated_at'] + ($order->country->order_hold_verification) ? AddressVerification::HOLD_TO : 0;
        }


        $message = [
            'id' => $prepareMessage['id'],
            'status' => $prepareMessage['status'],
            'sub_status' => (string)$prepareMessage['sub_status'],
            'foreign_id' => $prepareMessage['foreign_id'],
            'init_price' => $prepareMessage['init_price'],
            'final_price' => $prepareMessage['final_price'],
            'shipping_price' => $prepareMessage['shipping_price'],
            'extra_price' => $extraPrice,
            'shipping_id' => $prepareMessage['shipping_id'],
            'disable_broker' => $prepareMessage['disable_broker'],
            'order_comment' => $order->getLastComment(),//$prepareMessage['order_comment'],
            'country_char_code' => $prepareMessage['country_char_code'],
            'source' => $prepareMessage['source'],
            'customer_components' => $prepareMessage['customer_components'],
            'updated_at' => $prepareMessage['updated_at'],
            'last_operator_id' => $approveOperator,
            'delivery_from' => $prepareMessage['delivery_from'],
            'delivery_to' => $prepareMessage['delivery_to'],
            'comment' => $order->getLastComment(),//Last comment operator
            'history' => $history,
            'queue_type' => isset($prepareMessage['type_id']) ? OrderType::getReMappedType($prepareMessage['type_id']) : '',
            'confirm_address' => $prepareMessage['confirm_address'],
            'hold_to' => $hold_to,
            'call_data' => $callData ?? []
        ];

        $orderProduct = [];
        $productCount = 0;
        foreach ($prepareMessage['orderProducts'] as $product) {
            if (!empty($product['partner_product'])) {
                $orderProduct[] = [
                    'product_id' => $product['partner_product']['partner_product_id'],
                    'price' => $product['price'],
                    'quantity' => $product['quantity'],
                    'cost' => $product['cost'],
                    'gift' => $product['gift'],
                ];

                if (!$product['gift']) {
                    $productCount += $product['quantity'];
                }
            }
        }

        if (isset($prepareMessage['customer_components']) && is_string($prepareMessage['customer_components'])) {
            $prepareMessage['customer_components'] = json_decode($prepareMessage['customer_components'], true);
            if (isset($prepareMessage['customer_components']['address']) && !empty($prepareMessage['customer_components']['address'])) {
                $attributes = PartnerFormAttribute::getFormAttributes($prepareMessage['form_id']);
                foreach (ArrayHelper::toArray($attributes) as $attr) {
                    //customer_zip мы клеим последним в строке адреса
                   if ($attr['name'] != 'customer_zip') {
                        if (isset($prepareMessage['customer_components']['address'][$attr['name']])) {
                            $component = trim($prepareMessage['customer_components']['address'][$attr['name']]);
                            if (!empty($component)) {
                                if (empty($prepareMessage['customer_components']['address']['customer_address'])) {
                                    $prepareMessage['customer_components']['address']['customer_address'] = $component;
                                } else {
                                    $prepareMessage['customer_components']['address']['customer_address'] .= ', ' . $component;
                                }
                            }
                        }
                    }
                }
                if (!empty($prepareMessage['customer_components']['address']['customer_zip'])) {
                    $prepareMessage['customer_components']['address']['customer_address'] .= ', ' . trim($prepareMessage['customer_components']['address']['customer_zip']);
                }
            }
        }

        $message['customer_components'] = \GuzzleHttp\json_encode($prepareMessage['customer_components']);
        $message['orderProducts'] = $orderProduct;
        $message = \GuzzleHttp\json_encode($message);

        $message = [
            'MessageBody' => $message,
            'MessageGroupId' => self::createGroupID($order),
            'QueueUrl' => $order->partner->url_queue,
        ];

        $response = '';

        if (!is_null($order->partner->url_queue)) {
            try {
                $response = $this->sendMessage($message);
                // Устанавливаем флаг успешной отправки
                Yii::$app->db->createCommand()
                    ->update('{{%order}}', ['sent_to_sqs' => true], ['id' => $order->id])
                    ->execute();
            } catch (AwsException $e) {
                $this->error = $e->getMessage();
            }
        } else {
            $this->error = 'Partner dosen`t have URL queue';
        }

        if (!empty($this->error)) {
            $this->log = new ApiLog();
            $this->log->url = 'console/workers/lead';//Yii::$app->request->url;
            $this->log->order_id = $order->id;
            $this->log->operator = isset(Yii::$app->user->id) ? Yii::$app->user->id : null;
            $this->log->request = json_encode([
                'url' => $order->partner->url_queue,
                'response' => $message
            ], JSON_UNESCAPED_UNICODE);
            $this->log->response = json_encode(['error' => $this->error], JSON_UNESCAPED_UNICODE);
            $this->log->save();
            echo 'Error: ' . $this->error;
            echo ' ApiLog is created';
            return false;
        } else {
            $this->log = new ApiLog();
            $this->log->url = 'console/workers/lead';//Yii::$app->request->url;
            $this->log->order_id = $order->id;
            $this->log->operator = isset(Yii::$app->user->id) ? Yii::$app->user->id : null;
            $this->log->request = json_encode([
                'url' => $order->partner->url_queue,
                'response' => $message
            ], JSON_UNESCAPED_UNICODE);
            $this->log->save();
        }

        return $response;
    }

    /**
     * Перепроверить
     * Метод генерации сообщений для очереди амазон
     * @param Order $order
     * @return array|string
     */
    public function generateMessageBodyJson($order)
    {
        $order->partner_token = $order->partner->token;


        $prepareMessage = $this->createOrderMessageFifo($order, $order->partner->url_queue);

        if (empty($prepareMessage)) {
            ob_start();
            var_dump($prepareMessage);
            $bodyMessage = ob_get_contents();
            ob_end_clean();

            echo 'Empty $prepareMessage : ' . $bodyMessage;
            return false;
        }

        $prepareMessage = json_decode($prepareMessage['MessageBody'], true);

        //Подтягиваем историю
        $history = OrderLog::find()
            ->select(['field', 'old', 'new', 'old', 'user_id', 'operator'])
            ->where(['order_id' => $order->id])
            ->orderBy(['id' => 'ASC'])
            ->asArray()->all();

        //Пока не перевели все коллы, подстраивается под старый КЦ
        $callData = CallHistory::find()
            ->select([
                'record_date' => new Expression("CONCAT(to_timestamp(" . CallHistory::tableName() . ".created_at)::date, ' ', to_timestamp(" . CallHistory::tableName() . ".created_at)::time)"),
                'record_phone' => CallHistory::tableName() . '.phone',
                'user_sip' => CallHistory::tableName() . '.user_id',
            ])
            ->where(['order_id' => $order->id])
            ->asArray()
            ->all();

        $approveOperator = null;
        //Ищем в ней первую смену
        foreach ($history as $value) {
            if ($value['field'] == 'status' && !empty($value['user_id'])) {
                $approveOperator = $value['user_id'];
            }
        }

        $form = PartnerForm::find()->where(['id' => $prepareMessage['form_id']])->one();
        $extraPrice = 0;
        if (isset($form) && isset($form->extra_price)) {
            $extraPrice = $form->extra_price;
        }

        //Время блокировки
        $hold_to = 0;
        if ($order->status == Order::STATUS_APPROVED && $order->confirm_address == AddressVerification::ADDRESS_NEED_CONFIRM && $order->form->check_address) {
            $hold_to = $prepareMessage['updated_at'] + ($order->country->order_hold_verification) ? AddressVerification::HOLD_TO : 0;
        }

        $message = [
            'id' => $prepareMessage['id'],
            'status' => $prepareMessage['status'],
            'sub_status' => (string)$prepareMessage['sub_status'],
            'foreign_id' => $prepareMessage['foreign_id'],
            'init_price' => $prepareMessage['init_price'],
            'final_price' => $prepareMessage['final_price'],
            'shipping_price' => $prepareMessage['shipping_price'],
            'extra_price' => $extraPrice,
            'shipping_id' => $prepareMessage['shipping_id'],
            'disable_broker' => $prepareMessage['disable_broker'],
            'order_comment' => $order->getLastComment(),//$prepareMessage['order_comment'],
            'country_char_code' => $prepareMessage['country_char_code'],
            'source' => $prepareMessage['source'],
            'customer_components' => $prepareMessage['customer_components'],
            'updated_at' => $prepareMessage['updated_at'],
            'last_operator_id' => $approveOperator,
            'delivery_from' => $prepareMessage['delivery_from'],
            'delivery_to' => $prepareMessage['delivery_to'],
            'comment' => $order->getLastComment(),//Last comment operator
            'history' => $history,
            'queue_type' => isset($prepareMessage['type_id']) ? OrderType::getReMappedType($prepareMessage['type_id']) : '',
            'confirm_address' => $prepareMessage['confirm_address'],
            'hold_to' => $hold_to,
            'call_data' => $callData ?? []
        ];

        $orderProduct = [];
        foreach ($prepareMessage['orderProducts'] as $product) {
            if (!empty($product['partner_product'])) {
                $orderProduct[] = [
                    'product_id' => $product['partner_product']['partner_product_id'],
                    'price' => $product['price'],
                    'quantity' => $product['quantity'],
                    'cost' => $product['cost'],
                    'gift' => $product['gift'],
                ];
            }
        }

        if (isset($prepareMessage['customer_components']) && is_string($prepareMessage['customer_components'])) {
            $prepareMessage['customer_components'] = json_decode($prepareMessage['customer_components'], true);
            if (isset($prepareMessage['customer_components']['address']) && !empty($prepareMessage['customer_components']['address'])) {
                $attributes = PartnerFormAttribute::getFormAttributes($prepareMessage['form_id']);
                foreach (ArrayHelper::toArray($attributes) as $attr) {
                    if (isset($prepareMessage['customer_components']['address'][$attr['name']])) {
                        $component = trim($prepareMessage['customer_components']['address'][$attr['name']]);
                        if (!empty($component)) {
                            if (empty($prepareMessage['customer_components']['address']['customer_address'])) {
                                $prepareMessage['customer_components']['address']['customer_address'] = $component;
                            } else {
                                $prepareMessage['customer_components']['address']['customer_address'] .= ', ' . $component;
                            }
                        }
                    }
                }
                if (!empty($prepareMessage['customer_components']['address']['customer_zip'])) {
                    $prepareMessage['customer_components']['address']['customer_address'] .= ', ' . trim($prepareMessage['customer_components']['address']['customer_zip']);
                }
            }
        }

        $message['customer_components'] = \GuzzleHttp\json_encode($prepareMessage['customer_components']);
        $message['orderProducts'] = $orderProduct;
        $message = \GuzzleHttp\json_encode($message);

        return $message;
    }

    /**
     * Подготавливаем сообщение в очередь
     * @param Order $order
     * @param $queueUrl
     * @return array
     */
    protected function createOrderMessageFifo($order, $queueUrl)
    {
        $orderArray = ArrayHelper::toArray($order, []);
        if (isset($orderArray['customer_components'])) {
            $customerComponents = json_decode($orderArray['customer_components'], 1);
            $customerComponents['general']['customer_phone'] = $orderArray['customer_phone'];
            $orderArray['customer_components'] = json_encode($customerComponents);
        }
        $orderArray['shipping_id'] = PartnerShipping::find()->where([
            'partner_id' => $order->partner_id,
            'shipping_id' => $order->shipping_id,
            'country_id' => $order->country_id
        ])->select(['partner_shipping_id'])->scalar();

        if ($order->form->use_foreign_delivery_broker && !empty(str_replace(' ', '', $order->form->partner->url_foreign_delivery_broker)) && !empty($orderArray['shipping_id'])) {
            $orderArray['disable_broker'] = true;
        } else {
            $orderArray['disable_broker'] = false;
        }
        $orderArray['partner_token'] = $order->partner->token;
        $orderArray['source'] = self::SOURCE;
        $orderArray['country_char_code'] = $order->country->char_code;

        $orderArray['orderProducts'] = ArrayHelper::toArray($order->orderProducts, []);

        if (!empty($order->orderPromoProducts)) {
            $orderArray['orderProducts'] = ArrayHelper::merge($orderArray['orderProducts'], ArrayHelper::toArray($order->orderPromoProducts, []));
        }

        //$orderArray['orderProducts'] = ArrayHelper::toArray($order->orderProducts, []);
        //партнёрские данные товара нужны самому партнёру
        if (!empty($orderArray['orderProducts'])) {
            foreach ($orderArray['orderProducts'] as $k => $product) {

                $products = ArrayHelper::merge($order->orderProducts, $order->orderPromoProducts);

                if (isset($products[$k]->partner_product)) {
                    $orderArray['orderProducts'][$k]['partner_product'] = ArrayHelper::toArray($products[$k]->partner_product);
                } else {
                    $orderArray['orderProducts'][$k]['partner_product'] = PartnerProductModel::find()
                        ->where([
                            'partner_id' => $order->partner_id,
                            'product_id' => $products[$k]->product_id,
                            'country_id' => $order->country_id,
                            'active' => PartnerProductModel::ACTIVE
                        ])
                        ->asArray()
                        ->one();
                    if (empty($orderArray['orderProducts'][$k]['partner_product'])) {
                        echo 'Error: not isset: $products[$k]->partner_product';
                        return false;
                    }
                }
            }
        } else {
            $orderArray['orderProducts'] = [];
        }

        return [
            'MessageDeduplicationId' => time(),
            'MessageAttributes' => [
                'order_id' => [
                    'DataType' => 'Number',
                    'StringValue' => $order->foreign_id
                ]
                ,
                'timestamp' => [
                    'DataType' => 'Number',
                    'StringValue' => time()
                ]
            ],
            'MessageBody' => json_encode($orderArray, JSON_UNESCAPED_UNICODE),
            'MessageGroupId' => self::createGroupID($order),
            'QueueUrl' => $queueUrl
        ];
    }


    /**
     * @param Order $order
     * @return \Aws\Result|bool
     */
    protected function createGroupID($order)
    {
        if (!$order instanceof Order) {
            return self::MESSAGE_GROUP_ID;
        }

        return $order->partner_id . '_' . $order->country_id;
    }

    /**
     * @param Order $order
     * @return \Aws\Result|bool|string
     */
    public function sendNewLead($order)
    {
        Yii::$app->db->createCommand()
            ->update('{{%order}}', ['sent_to_sqs' => false], ['id' => $order->id])
            ->execute();

        $order->partner_token = $order->partner->token;
        $message = $this->createOrderMessageFifo($order, self::AMAZON_NEW_LEAD_TO_PAY);
        $message['MessageBody'] = json_decode($message['MessageBody'], 1);

        $orderProduct = [];
        foreach ($message['MessageBody']['orderProducts'] as $product) {
            if (!empty($product['partner_product'])) {
                $orderProduct[] = [
                    'product_id' => $product['partner_product']['partner_product_id'],
                    'price' => $product['price'],
                    'quantity' => $product['quantity'],
                    'cost' => $product['cost'],
                    'gift' => $product['gift'],
                ];
            }
        }

        $message['MessageBody']['orderProducts'] = $orderProduct;

        //Забираем только нужные поля для отправки
        $message = [
            'MessageBody' => json_encode($message['MessageBody'], JSON_UNESCAPED_UNICODE),
            'QueueUrl' => $message['QueueUrl'],
        ];

        $response = '';
        try {
            $response = $this->sendMessage($message);
            Yii::$app->db->createCommand()
                ->update('{{%order}}', ['sent_to_sqs' => true], ['id' => $order->id])
                ->execute();
        } catch (AwsException $e) {
            $this->error = $e->getMessage();
        }

        if (!empty($this->error)) {
            $this->log = new ApiLog();
            $this->log->url = Yii::$app->request->url;
            $this->log->order_id = $order->id;
            $this->log->operator = Yii::$app->user->id;
            $this->log->request = json_encode([
                'url' => $order->partner->url_queue,
                'response' => $message
            ], JSON_UNESCAPED_UNICODE);
            $this->log->response = json_encode(['error' => $this->error], JSON_UNESCAPED_UNICODE);
            $this->log->save();

            return false;
        }

        return $response;
    }

    /**
     * @param $handler
     * @param $queue_url
     * @return boolean
     */
    public function deleteMessage($handler, $queue_url)
    {
        if (yii::$app->params['is_dev']) {
            return true;
        }

        return parent::deleteMessage($handler, $queue_url);
    }

    public function getForeignIDFromPay($number = 10, $attributes = ['All'])
    {
        return $this->getMessages($number, $attributes, LeadSQS::AMAZON_GET_FOREIGN_ID);
    }

    public function getRecallLead($number = 10, $attributes = ['All'])
    {
        return $this->getMessages($number, $attributes, LeadSQS::AMAZON_GET_FOREIGN_ID);
    }

    /**
     * @param $queue
     * @param int $number
     * @param array $attributes
     * @return array|bool
     */
    public function getMessagesFromQueue($queue, $number = 10, $attributes = ['All'])
    {
        return $this->getMessages($number, $attributes, $queue);
    }
}
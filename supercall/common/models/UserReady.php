<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_ready}}".
 *
 * @property integer $user_id
 * @property integer $start_time
 * @property integer $stop_time
 * @property integer $diff
 *
 * @property User $user
 */
class UserReady extends ActiveRecord
{
    /** @var  string */
    public $group;
    /** @var  float */
    public $online;
    /** @var  string */
    public $user_name;
    /** @var  integer */
    public $midcheck;
    public $total;
    public $status_quantity_1;
    public $status_quantity_2;
    public $status_quantity_3;
    public $status_quantity_4;
    public $status_quantity_5;
    public $status_quantity_6;
    public $status_quantity_7;
    public $status_quantity_8;
    public $username;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_ready}}';
    }

    /**
     * Функция для пользователя меняет статус isReady
     *
     * @param $user_id
     * @param array $post
     * @return bool
     */
    public static function change($user_id, array $post)
    {
        $params = [
            'is_ready',
            'check_unready',
        ];

        if (!in_array($post['param'], $params))
            return false;

        //для auto_call операторов - управление паузой на астериске
        if ($user = Yii::$app->user->identity) {
            if ($user->auto_call) {
                $userSip = $user->sipArray;
                Asterisk::setUserPaused(!$post['value'], $userSip['sip_login']);
            }
        }

        if ($post['param'] == 'is_ready') {
            return self::_changeIsReady($user_id, $post['value']);
        } else if ($post['param'] == 'check_unready') {
            return self::_changeCheckUnready($user_id, $post['value']);
        }
    }

    /**
     * Обновить статус для параметра is_ready
     *
     * @param $user_id
     * @param $value
     * @return bool
     */
    private static function _changeIsReady($user_id, $value)
    {
        $timezone = Timezone::DEFAULT_TIMEZONE;
        $current_time = (new \DateTime('now', new \DateTimeZone($timezone)))->getTimestamp();

        if ($value) {

            //уже ведётся учёт времени на учётку
            $isStarted = UserReady::find()->where(['user_id' => $user_id])->orderBy(['id' => SORT_DESC])->limit(1)->one();
            if (!is_null($isStarted)) {
                if (is_null($isStarted->stop_time)) {
                    //мы всё равно чекаем каждую минуту активность пользователя
                    if ($current_time - $isStarted->start_time > 60) {
                        $isStarted->stop_time = $isStarted->start_time + 60;
                    } else {
                        $isStarted->stop_time = $current_time;
                    }
                    $isStarted->diff = $isStarted->stop_time - $isStarted->start_time;
                    $isStarted->save(false);
                }
            }

            $userReady = new self;
            $userReady->user_id = $user_id;
            $userReady->start_time = $current_time;
            if ($userReady->save()) {
                Yii::$app->db->createCommand()
                    ->update('{{%user}}', ['is_ready' => true], ['id' => $user_id])->execute();
                return true;
            } else {
                return false;
            }
        } // Отключаем "Готов"
        else {

            //уже ведётся учёт времени на учётку
            $isStarted = UserReady::find()->where(['user_id' => $user_id])->orderBy(['id' => SORT_DESC])->limit(1)->one();

            if (!is_null($isStarted)) {
                if (is_null($isStarted->stop_time)) {
                    //мы всё равно чекаем каждую минуту активность пользователя
                    if (($current_time - $isStarted->start_time) > 60) {
                        $isStarted->stop_time = $isStarted->start_time + 60;
                    } else {
                        $isStarted->stop_time = $current_time;
                    }
                    $isStarted->diff = $isStarted->stop_time - $isStarted->start_time;
                    $isStarted->save(false);
                }
            }

            Yii::$app->db->createCommand('UPDATE "user" SET is_ready = FALSE WHERE id = ' . $user_id)->execute();
            sleep(2);
            //User::updateAll(['is_ready' => false], ['id' => yii::$app->user->identity->id]);
            //Yii::$app->db->createCommand()->update('{{%user}}', ['is_ready' => false], ['id' => $user_id])->execute();
            return true;
        }
    }

    /**
     * @param $params
     * @return $this
     */
    public static function getFullUserOnline($params)
    {
//        $time = (new \DateTime('now', new \DateTimeZone(Timezone::DEFAULT_TIMEZONE)))->getTimestamp();
//
//        $query = UserReady::find()
//            ->leftJoin(User::tableName(), User::tableName() . '.id = ' . UserReady::tableName() . '.user_id')
//            ->leftJoin(QueueOrder::tableName(), QueueOrder::tableName() . '.user_id = ' . UserReady::tableName() . '.user_id')
//            //->where(['>=', 'start_time', $time - 60])
//            ->andWhere(['is_ready' => true])
//            ->andWhere(['is', QueueOrder::tableName() . '.user_id', NULL]);
//
//        echo $query->createCommand()->getRawSql();
//
//
//
//        foreach ($params as $name => $value) {
//            $query->andWhere([$name => $value]);
//        }
//
//        $users = [];
//
//        /** @var UserReady $userReady */
//        foreach ($query->all() as $userReady) {
//            $users[$userReady->user->id] = $userReady->user;
//        }

        $query = User::find()
            ->select(User::tableName() . '.*')
            ->leftJoin(QueueOrder::tableName(), QueueOrder::tableName() . '.user_id=' . User::tableName() . '.id')
            ->where([
                'is_ready' => User::USER_READY,
                'auto_call' => User::AUTO_CALL_ON
            ])
            ->andWhere(['is', QueueOrder::tableName() . '.user_id', null]);

        $users = $query->all();

        $list = [];

        foreach ($users as $user){
            $list[$user->id] = $user;
        }

        return $list;
    }

    public static function getUsersOnline($params = [])
    {
        $time = (new \DateTime('now', new \DateTimeZone(Timezone::DEFAULT_TIMEZONE)))->getTimestamp();

        $users = UserReady::find()
            ->select([UserReady::tableName() . '.user_id'])
            ->leftJoin(User::tableName(), User::tableName() . '.id = ' . UserReady::tableName() . '.user_id')
            ->leftJoin(QueueOrder::tableName(), QueueOrder::tableName() . '.user_id = ' . UserReady::tableName() . '.user_id')
            ->where(['>=', 'start_time', $time - 60])
            ->andWhere(['is_ready' => true])
            ->andWhere(['is', QueueOrder::tableName() . '.user_id', NULL])
            ->groupBy(UserReady::tableName() . '.user_id');

        foreach ($params as $name => $value) {
            $users->andWhere([$name => $value]);
        }

        $returnUser = $users->asArray()->column();

//        foreach ($returnUser as $user) {
//            if (!empty($user->sip)) {
//                $userSip = json_decode($user->sip, 1);
//                Asterisk::setUserPaused(0, $userSip['sip_login']);
//            }
//        }

        return $returnUser;
    }


    /**
     * Функция обновит статус для параметра check_unready
     * @param $user_id
     * @param $value
     * @return bool
     */
    private static function _changeCheckUnready($user_id, $value)
    {
        if ($value) {
            Yii::$app->db->createCommand()
                ->update('{{%user}}', ['check_unready' => true], ['id' => $user_id])->execute();

            return true;
        } // Отключаем
        else {
            Yii::$app->db->createCommand()
                ->update('{{%user}}', ['check_unready' => false], ['id' => $user_id])->execute();

            return true;
        }
    }

    /**
     * Публичная обёртка для приватного _changeIsReady()
     * @param $user_id
     * @param $value
     * @return bool
     */
    public static function changeIsReady($user_id, $value)
    {
        return self::_changeIsReady($user_id, $value);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'start_time', 'diff'], 'integer'],
            [['stop_time'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => false, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('common', 'User ID'),
            'start_time' => Yii::t('common', 'Start Time'),
            'stop_time' => Yii::t('common', 'Stop Time'),
            'diff' => Yii::t('common', 'Diff'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}

<?php

namespace common\models;

use api\models\ApiLog;
use Aws\Exception\AwsException;
use Aws\Sqs\SqsClient;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class AmazonSQS
 * @package common\models
 */
class AmazonSQS
{
    /**
     * API KEY
     */
    const AMAZON_API_KEY = 'AKIAJ7LAO7MJMRVM3GSA';

    /**
     * SECRET KEY
     */
    const AMAZON_SECRET_KEY = 'Ho9HW7+elOafUKY1JqCAzwmIrNyqC4wpmqUOguT7';

    /**
     * REGION
     */
    const AMAZON_REGION = 'eu-west-1';

    /**
     * @var SqsClient
     */
    public $amazonClient = null;

    public $handlers = [];

    public $error;

    /**
     * AmazonSQS constructor.
     */
    public function __construct()
    {
        $this->amazonClient = new SqsClient([
            'credentials' => [
                'key' => self::AMAZON_API_KEY,
                'secret' => self::AMAZON_SECRET_KEY,
            ],
            'region' => self::AMAZON_REGION,
            'version' => 'latest',
        ]);
    }

    /**
     * @param $number
     * @param $attributes
     * @param $url
     * @return array|bool
     */
    public function getMessages($number, $attributes, $url)
    {
        try {
            $response = $this->amazonClient->receiveMessage([
                'MaxNumberOfMessages' => $number,
                'MessageAttributeNames' => $attributes,
                'QueueUrl' => $url,
            ]);

            if (empty($response))
                return false;

            $messages = $response->get('Messages');
            $messages = is_array($messages) ? $messages : [];
            $this->handlers = ArrayHelper::map($messages, 'MessageId', 'ReceiptHandle');
            $result = ArrayHelper::map($messages, 'MessageId', 'Body');

            return $result;
        } catch (\Throwable $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     * @param $handler
     * @param $queue_url
     * @return boolean
     */
    public function deleteMessage($handler, $queue_url)
    {
        $this->amazonClient->deleteMessage([
            'QueueUrl' => $queue_url,
            'ReceiptHandle' => $handler
        ]);

        return true;
    }

    /**
     * @param string $queueUrl
     * @param array $handlers
     */
    public function deleteMessages($queueUrl, $handlers)
    {
        foreach (array_chunk($handlers, 10, true) as $batch) {
            $entries = [];
            foreach ($batch as $messageId => $receiptHandler) {
                $entries[] = [
                    'Id' => $messageId,
                    'ReceiptHandle' => $receiptHandler
                ];
            }
            $response = $this->amazonClient->deleteMessageBatch([
                'QueueUrl' => $queueUrl,
                'Entries' => $entries
            ]);
        }
    }


    /**
     * Отправка сообщений в очередь
     * [
     *   'DelaySeconds' => 10,
     *   'MessageAttributes' => [
     *       "Title" => [
     *          'DataType' => "String",
     *          'StringValue' => "The Hitchhiker's Guide to the Galaxy"
     *      ],
     *      "Author" => [
     *          'DataType' => "String",
     *          'StringValue' => "Douglas Adams."
     *      ],
     *      "WeeksOn" => [
     *          'DataType' => "Number",
     *          'StringValue' => "6"
     *      ]
     *  ],
     *  'MessageBody' => "Information about current NY Times fiction bestseller for week of 12/11/2016.",
     *  'QueueUrl' => 'QUEUE_URL'
     * ]
     *
     * @param array $params
     * @return \Aws\Result|bool
     */
    public function sendMessage($params = [])
    {
        if (!yii::$app->params['is_dev']) {
            try {
                return $this->amazonClient->sendMessage($params);
            } catch (AwsException $e) {
                echo 'Error: ' . $this->error = $e->getMessage();
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * @param array $params
     * @return \Aws\Result|bool
     */
    protected function sendMessageToDev($params = [])
    {
        try {
            return $this->amazonClient->sendMessage($params);
        } catch (AwsException $e) {
            echo 'Error: ' . $this->error = $e->getMessage();
            return false;
        }
    }
}
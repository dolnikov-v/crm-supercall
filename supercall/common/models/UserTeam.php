<?php
namespace common\models;


use yii\db\ActiveRecord;
use yii;

/**
 * Class UserTeam
 * @package common\models
 */
class UserTeam extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%user_team}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'team_id', 'user_id', 'created_at'], 'required'],
            [['user_id', 'team_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'team_id_id' => yii::t('common', 'Команда'),
            'user_id' => yii::t('common', 'Пользователь'),
            'created_at' => yii::t('common', 'Дата создания'),
            'updated_at' => yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Teams::className(), ['id' => 'team_id']);
    }

    /**
     * @param $user_id
     * @param $team_id
     * @return bool
     */
    public static function addInTeam($user_id, $team_id)
    {
        $model = new UserTeam();
        $model->user_id = $user_id;
        $model->team_id = $team_id;
        $model->created_at = time();

        if (!$model->save()) {
            yii::$app->notifier->addNotifierErrorByModel($model);
            return false;
        }

        return true;
    }

    /**
     * @param $user_id
     * @param $team_id
     * @return bool
     */
    public static function removeFromTeam($user_id, $team_id)
    {
        $modelsQuery = UserTeam::find()->where(['user_id' => $user_id, 'team_id' => $team_id]);

        if ($modelsQuery->exists()) {
            $models = $modelsQuery->all();

            foreach ($models as $model) {
                if (!$model->delete()) {
                    yii::$app->notifier->addNotifierErrorByModel($model);
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $user_id
     * @return bool
     */
    public static function removeFromTeamByUser($user_id)
    {
        $modelsQuery = UserTeam::find()->where(['user_id' => $user_id]);

        if ($modelsQuery->exists()) {
            $models = $modelsQuery->all();

            foreach ($models as $model) {
                if (!$model->delete()) {
                    yii::$app->notifier->addNotifierErrorByModel($model);
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $team_id
     * @return bool
     */
    public static function clearTeam($team_id)
    {
        $modelsQuery = UserTeam::find()->where(['team_id' => $team_id]);

        if ($modelsQuery->exists()) {
            $models = $modelsQuery->all();

            foreach ($models as $model) {
                if (!$model->delete()) {
                    yii::$app->notifier->addNotifierErrorByModel($model);
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $user_id
     * @param $team_ids
     * @return bool
     */
    public static function updateUserTeams($user_id, $team_ids)
    {
        $team_ids = is_array($team_ids) ? $team_ids : [$team_ids];

        $transaction = yii::$app->db->beginTransaction();

        //удалить из всех команд пользователя
        if(!self::removeFromTeamByUser($user_id)){
            $transaction->rollBack();
            return false;
        }else{
            //записать пользователя в указанные команды
            foreach($team_ids as $team_id){
                if(!self::addInTeam($user_id, $team_id)){
                    $transaction->rollBack();
                    return false;
                }
            }
        }

        $transaction->commit();
        return true;
    }
}
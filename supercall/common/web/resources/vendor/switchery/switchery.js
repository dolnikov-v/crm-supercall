$(function () {
    Switchery.init();
});

var Switchery = {
    init: function () {
        $("[data-type-checkbox='switchery']").bootstrapSwitch();
    }
}

$(function () {
    const LOCAL = false;
    const DEBUG = false;

    var serverNodeJS = LOCAL ? 'http://socket2wcall.local' : 'http://socket.2wcall.com:44366';

    var KVS = {
        TOKEN: 'YzFhYjk5MjRlY2RhMWJlYWY4YmJhYTFlYjgyMzhiODNlMGVkOGM2Mw==',
        SOCKET:  io.connect(serverNodeJS),
        TYPE_SEND_SOCKET: 'socket',
        TYPE_SEND_AJAX: 'ajax',
        TYPE_DATA: 'type_data',
        TYPE_LOGOUT: 'type_logout',
        TYPE_CLOSE: 'type_close',
        ACTION_USER: 'user',
        ACTION_SIP: 'sip',
        ACTION_USER_READY: 'userReady',
        ACTION_STATUS_ZOIPER: 'statusZoiper',
        ACTION_STATUS_CALL: 'statusCall',
        ACTION_TYPE_CALL: 'typeCall',
        ACTION_ORDER: 'order',
        ACTION_LAST_ACTIVITY: 'lastActivity',
        ACTION_IP: 'ip',
        TYPE_CALL_DEFAULT: 'default',
        TYPE_CALL_INCOMING: 'incoming',
        TYPE_CALL_AUTO_CALL: 'auto call',
        ACTION_USER_COUNTRY: 'userCountry',
        ACTION_QUEUE_PAUSED: 'queuePaused',
        ACTION_UNIQUEID: 'uniqueid',
        AUTO_CALL_ON: 1,
        STATUS_CALL_DEFAULT: null,
        STATUS_ZOIPER_DEFAULT: null,
        QUEUE_PAUSED_DEFAULT: null,
        db: {},
        db_prev: {},
        uniqueueId: null,
        country_collection: {},
        user_ready: '#ready-button',
        logout: '.logout',
        init: function () {
            var data = [];

            KVS.SOCKET.on('connect', function(){
                if(DEBUG) console.log('EVENT CONNECT ', KVS.SOCKET.connected);
                KVS.sendData(KVS.TYPE_DATA);
            });

            if (typeof window.user === 'object') {
                if (!KVS.uniqueueId) {
                    KVS.uniqueueId = window.user.id;// + '-' + KVS.generateUniqueId();
                }

                data[KVS.ACTION_USER] = window.user;
                data[KVS.ACTION_USER_READY] = true;

                var typeCall = KVS.TYPE_CALL_DEFAULT;

                if (user.auto_call === KVS.AUTO_CALL_ON) {
                    typeCall = KVS.TYPE_CALL_AUTO_CALL;
                } else if (user.incoming_call) {
                    typeCall = KVS.TYPE_CALL_INCOMING;
                }

                data[KVS.ACTION_TYPE_CALL] = typeCall;
                data[KVS.ACTION_SIP] = JSON.parse(window.user.sip);
                data[KVS.ACTION_USER_COUNTRY] = window.user_country;
                data[KVS.ACTION_UNIQUEID] = KVS.uniqueueId;
                data[KVS.ACTION_QUEUE_PAUSED] = KVS.QUEUE_PAUSED_DEFAULT;

                KVS.setBatch(data);
            }

            KVS.checkUserReadyEvent();
        },
        generateUniqueId: function () {
            return window.btoa((window.user.id + '-' + (new Date()).getTime()).toString());
        },
        checkUserReadyEvent: function () {
            $(KVS.user_ready).on('change', function () {
                KVS.set(KVS.ACTION_USER_READY, $(this).prop('checked'));
            });
        },
        getTypeCall: function () {
            var user = KVS.getData(KVS.ACTION_USER);

            if (user) {
                if (user.auto_call === KVS.AUTO_CALL_ON) {
                    return KVS.TYPE_CALL_AUTO_CALL;
                } else if (user.incoming_call) {
                    return KVS.TYPE_CALL_INCOMING;
                } else {
                    return KVS.TYPE_CALL_DEFAULT;
                }
            }
        },
        getTime: function () {
            return (new Date).getTime();
        },
        create: function (key) {
            if (typeof KVS.db[key] === 'undefined') {
                KVS.db[key] = {};
            }
        },
        set: function (key, value) {
            if (DEBUG) {
                console.warn('set: ' + key);
            }

            KVS.create(key);
            KVS.db[key] = {
                data: value,
                time: KVS.getTime()
            };

            KVS.db[KVS.ACTION_LAST_ACTIVITY] = KVS.getTime();

            KVS.sendData(KVS.TYPE_DATA);
        },
        setBatch: function (data) {
            for (var key in data) {
                KVS.create(key);
                KVS.db[key] = {
                    data: data[key],
                    time: KVS.getTime()
                };
            }

            KVS.sendData(KVS.TYPE_DATA);
        },
        get: function (key) {
            return typeof KVS.db[key] === 'undefined' ? null : KVS.db[key];
        },
        getData: function (key) {
            return typeof KVS.db[key] === 'undefined' ? null : KVS.db[key].data;
        },
        getAllData: function () {
            return KVS.db;
        },
        unloadData: function () {
            KVS.db = {};
        },
        checkEqualData: function(data){
            var last_activity = data.last_activity;

            KVS.db_prev.last_activity = null;
            data.last_activity = null;

            var equal =  JSON.stringify(KVS.db_prev) === JSON.stringify(data);

            data.last_activity = last_activity;

            return equal;
        },
        sendData: function (type) {
            var user_data = {};

            if (DEBUG)  console.warn(KVS.getData(KVS.ACTION_USER));

            if (window.user) {
                KVS.db[KVS.ACTION_LAST_ACTIVITY] = KVS.getTime();

                var data = {};

                user_data = {
                    user_id: KVS.getData(KVS.ACTION_USER).id,
                    last_activity: KVS.getTime(),
                    username: KVS.getData(KVS.ACTION_USER).username,
                    user_sip: JSON.parse(KVS.getData(KVS.ACTION_USER).sip),
                    type_call: KVS.getData(KVS.ACTION_TYPE_CALL),
                    use_messenger: KVS.getData(KVS.ACTION_USER).use_messenger,
                    status_zoiper: KVS.getData(KVS.ACTION_STATUS_ZOIPER),
                    status_call: KVS.getData(KVS.ACTION_STATUS_CALL),
                    user_ready: KVS.getData(KVS.ACTION_USER_READY),
                    order: KVS.getData(KVS.ACTION_ORDER),
                    user_country: KVS.getData(KVS.ACTION_USER_COUNTRY),
                    queue_paused: KVS.getData(KVS.ACTION_QUEUE_PAUSED),
                    uniqueid: KVS.getData(KVS.ACTION_UNIQUEID),
                    ip: KVS.getData(KVS.ACTION_USER).ip,
                    type: type
                };

                switch (type) {
                    case KVS.TYPE_DATA:
                        data = {
                            token: KVS.TOKEN,
                            data: user_data
                        };
                        break;
                }

                if (DEBUG) {
                    console.log('data', data);
                }

                //отправлять если только данные разные
                if(!KVS.checkEqualData(user_data)) {
                    KVS.db_prev = user_data;
                    KVS.SOCKET.emit(type, data);
                }
            }

        }
    };

    try {
        window.KVS = KVS;

        KVS.init();

        KVS = Object.assign({}, window.KVS);
    }catch(e){
        console.log(e);
    }
});
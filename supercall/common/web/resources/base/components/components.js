(function(window, document, $) {
    'use strict';

    $.components = $.components || {};

    $.extend($.components, {
        _components: {},

        register: function(name, obj) {
            this._components[name] = obj;
        },

        init: function(name, context, args) {
            var self = this;

            if (typeof name === 'undefined') {
                $.each(this._components, function(name) {
                    self.init(name);
                });
            } else {
                context = context || document;
                args = args || [];

                var obj = this.get(name);

                if (obj) {
                    switch (obj.mode) {
                        case 'default':
                            return this._initDefault(name, context);
                        case 'init':
                            return this._initComponent(name, obj, context, args);
                        case 'api':
                            return this._initApi(name, obj, args);
                        default:
                            this._initApi(name, obj, context, args);
                            this._initComponent(name, obj, context, args);
                            return;
                    }
                }
            }
        },

        call: function(name, context) {
            var args = Array.prototype.slice.call(arguments, 2);
            var obj = this.get(name);

            context = context || document;

            return this._initComponent(name, obj, context, args);
        },

        _initDefault: function(name, context) {
            if (!$.fn[name]) return;

            var defaults = this.getDefaults(name);

            $('[data-plugin=' + name + ']', context).each(function() {
                var $this = $(this),
                    options = $.extend(true, {}, defaults, $this.data());

                $this[name](options);
            });
        },


        _initComponent: function(name, obj, context, args) {
            if ($.isFunction(obj.init)) {
                obj.init.apply(obj, [context].concat(args));
            }
        },

        _initApi: function(name, obj, args) {
            if (typeof obj.apiCalled === 'undefined' && $.isFunction(obj.api)) {
                obj.api.apply(obj, args);

                obj.apiCalled = true;
            }
        },


        getDefaults: function(name) {
            var component = this.get(name);

            if (component && typeof component.defaults !== "undefined") {
                return component.defaults;
            } else {
                return {};
            }
        },

        get: function(name, property) {
            if (typeof this._components[name] !== "undefined") {
                if (typeof property !== "undefined") {
                    return this._components[name][property];
                } else {
                    return this._components[name];
                }
            } else {
                console.warn('component:' + component + ' script is not loaded.');

                return undefined;
            }
        }
    });

})(window, document, jQuery);

$(document).on('shown.bs.dropdown', '.table-responsive', function (e) {
    // The .dropdown container
    var $container = $(e.target);

    // Find the actual .dropdown-menu
    var $dropdown = $container.find('.dropdown-menu');
    if ($dropdown.length) {
        // Save a reference to it, so we can find it after we've attached it to the body
        $container.data('dropdown-menu', $dropdown);
    } else {
        $dropdown = $container.data('dropdown-menu');
    }

    $dropdown.css('top', ($container.offset().top + $container.outerHeight()) + 'px');
    $dropdown.css('left', $container.offset().left + 'px');
    $dropdown.css('position', 'absolute');
    $dropdown.css('display', 'block');
    $dropdown.appendTo('body');
});

$(document).on('hide.bs.dropdown', '.table-responsive', function (e) {
    // Hide the dropdown menu bound to this button
    $(e.target).data('dropdown-menu').css('display', 'none');
});

$(function(){
    $('.panel-footer').addClass('table-responsive');
});
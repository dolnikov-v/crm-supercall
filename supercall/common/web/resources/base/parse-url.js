//поиск значения параметра get
function parseUrl(name, url) {
    if (!url)
        url = window.location.href;

    url = decodeURIComponent(url);
    name = name.replace(/[\[\]]/g, "\\$&");

    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    var results = regex.exec(url);

    if (!results)
        return null;
    if (!results[2])
        return '';

    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function parseUrlMultiple(name, url) {
    if (!url) {
        url = window.location.href;
    }

    url = decodeURIComponent(url);
    nameReplaced = name.replace(/[\[\]]/g, "\\$&");

    var regex = new RegExp("[?&]" + nameReplaced + "(=([^&#]*)|&|#|$)");
    var results = regex.exec(url);

    if (!results) {
        return null;
    }

    if (!results[2]) {
        return '';
    }

    var result = [];

    while (results && results[2]) {
        result.push(results[2].replace(/\+/g, " "));
        url = url.replace(name + '=' + results[2], '');
        results = regex.exec(url);
    }

    return result;
}

function getSearchModelName() {
    url = decodeURIComponent(window.location.href);
    searchModelName = url.match('(\\?([^[]+)\\[)');

    if (!searchModelName) {
        return null;
    }

    return searchModelName[2];
}
getCustomFunctions = function(){
    setGroupCustomFunctionInfo(
        'isFunction',
        'bool',
        'проверка typeof data === function'
    );

    setGroupCustomFunctionInfo(
        'isUndefined',
        'bool',
        'проверка typeof data === undefined'
    );

    setGroupCustomFunctionInfo(
        'isNull',
        'bool',
        'проверка typeof data === null'
    );

    setGroupCustomFunctionInfo(
        'isEmpty',
        'bool',
        'проверка data === ""'
    );

    setGroupCustomFunctionInfo(
        'count',
        'вернёт кол-во элементов массива, ключей объекта, символов в строке или числе',
        'number'
    );

    setGroupCustomFunctionInfo(
        'parseUrl',
        'string',
        'возвращает значение GET параметра. Пример: parseUrl("OrderSearch[foreign_id]", url); По дефолту url = location.href '
    );

    setGroupCustomFunctionInfo(
        'getIp',
        'string',
        'возвращает приватный IP\nиспользование: \nvar getUserIP(function(ip){\nalert(ip);\n})'
    );

    setGroupCustomFunctionInfo(
        'hashCode',
        'number',
        'возвращает hash code строки'
    );

    setGroupCustomFunctionInfo(
        'in_array',
        'bool',
        'аналог PHP in_array(handle, array)'
    );

    setGroupCustomFunctionInfo(
        'uniqueid',
        'string',
        'генерит уникальную строку вида _omrwpcgf6'
    );


    return true;
};

function setGroupCustomFunctionInfo(nameFunction, returnData, description){
    console.log('');
    console.info('>>> function: ' + nameFunction);
    console.log('return: ' + returnData);
    console.log('description: ' + description);
}

if(typeof isFunction !== 'function'){
    function isFunction(value){
        return typeof value === 'function';
    }
}


if(!isFunction('isUndefined')){
    function isUndefined(value){
        return typeof value === 'undefined';
    }
}

if(!isFunction('isNull')) {
    function isNull(value) {
        return value === null;
    }
}

if(!isFunction('isEmpty')) {
    function isEmpty(value) {
        return value === '';
    }
}

if(!isFunction('count')) {
    function count(data) {
        if (typeof data === 'object') {
            return Object.keys(data).length;
        }else if (data === null) {
            return 0;
        } else {
            return data.toString().length;
        }
    }
}

if(!isFunction('getIp')){
    function getIp(){

        $.getJSON('http://ipinfo.io', function (data) {
            console.log(data);
        });

        $.getJSON("https://jsonip.com?callback=?", function(data) {
            console.log(data);
        });


    }
}

if(!isFunction('parseUrl')){
    function parseUrl(name, url) {
        if (!url)
            url = window.location.href;

        url = decodeURIComponent(url);
        name = name.replace(/[\[\]]/g, "\\$&");

        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
        var results = regex.exec(url);

        if (!results)
            return null;
        if (!results[2])
            return '';

        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
}

if(!isFunction('getUserIP')) {
    function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
        //compatibility for firefox and chrome
        var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
        var pc = new myPeerConnection({
                iceServers: []
            }),
            noop = function () {
            },
            localIPs = {},
            ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
            key;

        function iterateIP(ip) {
            if (!localIPs[ip]) onNewIP(ip);
            localIPs[ip] = true;
        }

        //create a bogus data channel
        pc.createDataChannel("");

        // create offer and set local description
        pc.createOffer().then(function (sdp) {
            sdp.sdp.split('\n').forEach(function (line) {
                if (line.indexOf('candidate') < 0) return;
                line.match(ipRegex).forEach(iterateIP);
            });

            pc.setLocalDescription(sdp, noop, noop);
        }).catch(function (reason) {
            // An error occurred, so handle the failure to connect
        });

        //listen for candidate events
        pc.onicecandidate = function (ice) {
            if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
            ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
        };
    }

   //usage
    /*
    var ip = getUserIP(function(ip){
        return ip;
    });
    */
}

if(!isFunction('hashCode')){
    function hashCode(string){
        var h = 0;
        if (string.length == 0) return h;
        for (i = 0; i < string.length; i++) {
            char = string.charCodeAt(i);
            h = ((h<<5)-h)+char;
            h = h & h;
        }
        return h;
    }
}

if(!isFunction('in_array')){
    function in_array(handle, array){
        return array.indexOf(handle) !== -1;
    }
}

$(function(){
     getUserIP(function(ip){
         $('#local_ip').val(ip);
    });
});

if(!isFunction('uniqueid')){
    function uniqueid(){
        return '_' + Math.random().toString(36).substr(2, 9);
    }
}





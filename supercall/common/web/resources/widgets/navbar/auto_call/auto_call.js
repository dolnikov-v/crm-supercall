$(function () {
    console.log('init autoCall modal window')
    var AutoCall = {
        modal: '.modal_window',
        button_accept: '.accept_auto_call',
        init: function () {
            var modal = '<div class="modal_window success hidden">\n' +
                '<div class="modal_head"><span class="glyphicon glyphicon-earphone"></span> Incomming call</div>\n' +
                '<div class="modal_body">\n' +
                '<div class="row">\n' +
                ' <div class="col-lg-4"></div>\n' +
                ' <div class="col-lg-4"><button type="button" class="btn btn-lg btn-default accept_auto_call">Accept</button></div>\n' +
                ' <div class="col-lg-4"></div>\n' +
                '</div>\n' +
                '</div>\n' +
                '<div class="modal_footer"></div>\n' +
                '</div>';

            $(modal).appendTo('body');
            $(AutoCall.button_accept).css('cursor', 'pointer');
            $(AutoCall.button_accept).on('click', function () {
                AutoCall.hide();
            });
        },
        show: function () {
            $(AutoCall.modal).removeClass('hidden');
            $(AutoCall.modal).stop( true, true ).fadeIn("slow");
        },
        hide: function () {
            $(AutoCall.modal).stop( true, true ).fadeOut("slow");
        }
    };

    AutoCall.init();

    window.AutoCall = AutoCall;


    //Принятия входящего звонка нажатием энтера или пробела
    $('body').on('keydown', function (event) {
        if (event.keyCode == 13) {
            $(AutoCall.button_accept).trigger('click');
        }
    });
});

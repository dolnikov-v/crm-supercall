$(document).ready(function () {
    var phoneFileds = new Array('customer_phone', 'customer_mobile')
    $.each(phoneFileds, function( index, value ) {
        var customer_phone_id = '#order-general-' + value;
        var customer_phone = $(customer_phone_id)
        if (customer_phone.length > 0) {
            var formGroup = customer_phone.parent();
            var customer_phone_val = customer_phone.val();
            var object = '<div class="input-group"><span class="input-group-addon"><a href="javascript:void(0)" onclick="zoiperDial(\'' + customer_phone_id + '\')">&#128222;</a></span>' + customer_phone.get(0).outerHTML + '</div>';
            $(formGroup).html(object);
        }
    });

    // динам.телефоны
    var phones = $('.order-phone--item')
    if (phones.length > 0) {
        $.each(phones, function( index, value ) {
            var phoneItem = value.value
            var phoneItemId = $(value).attr('id')

            var object = '<span class="input-group-addon"><a href="javascript:void(0)" onclick="zoiperDial(\'' + phoneItemId + '\')">&#128222;</a></span>';
            $(object).insertBefore('#' + phoneItemId);

        });
    }
})
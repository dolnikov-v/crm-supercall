$(function () {
    $(document).on('click', '[data-widget="string-generator"]', StringGenerator.generate);
    $(document).on('click', '[data-widget="password-showing"]', StringGenerator.toggle);
});

var StringGenerator = {
    generate: function () {
        var chars = $(this).data('generator-chars').split('');
        var $input = $(this).closest('.input-group').find('input');

        var stringLength = $(this).data('generator-length');

        var string = '';
        var rand = 0;
        var timeout = Math.floor(1000 / stringLength);

        var timeoutGenerator = function () {
            rand = Math.floor(Math.random() * (chars.length));
            string = string + chars[rand];

            $input.val(string);

            if (string.length < stringLength) {
                setTimeout(timeoutGenerator, timeout);
            }
        };

        timeoutGenerator();
    },

    toggle: function () {
        var $input = $(this).closest('.input-group').find('input');
        var $icon = $(this).find('i.icon');

        if ($input.attr('type') == 'text') {
            $input.attr('type', 'password');
            $icon.removeClass('fa-eye-slash').addClass('fa-eye');
        } else {
            $input.attr('type', 'text');
            $icon.removeClass('fa-eye').addClass('fa-eye-slash');
        }
    }
};

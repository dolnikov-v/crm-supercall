$(function () {
    $(document).on('click', '.confirm-delete-link', ModalConfirmDelete.confirmDelete);
});

var ModalConfirmDelete = {
    /**
     * Подтвержение удаления
     */
    confirmDelete: function () {
        $('#modal_confirm_delete_link').attr('href', $(this).data('href'));
        $('#modal_confirm_delete').modal();

        return false;
    }
};

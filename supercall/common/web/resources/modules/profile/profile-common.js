$(function(){
    var ProfileCommon = {
        modal: '#modal-common-profile',
        imageProfileBlock: '.profile-picture',
        previewBlock: '.file-preview',
        btnChange: '.btn-change-profile-image',
        defaultAvatar: '.default-avatar',
        btnSetDefault: '.set-default',
        uploadedFieldAvatar: '[name*=profile_image]',
        placeAvatarForm: '.place-avatar',
        urlBackend: '/profile/control/delete-profile-image',
        urlFrontend: '/profile/control/delete-profile-image',
        btnSubmit: '[type="submit"]',
        init: function(){
            ProfileCommon.setPointer();
            $(ProfileCommon.imageProfileBlock).find('img').on('click', ProfileCommon.eventChangeImageProfile);
            $(ProfileCommon.modal).on('show.bs.modal', ProfileCommon.afterShow);
            $(ProfileCommon.btnChange).on('click', ProfileCommon.eventChange);
            $(ProfileCommon.btnSetDefault).on('click', ProfileCommon.eventBtnDefault);
        },
        eventBtnDefault: function(){
            ProfileCommon.eventDelete();

            $(ProfileCommon.imageProfileBlock).find('img').attr('src', $(ProfileCommon.defaultAvatar).attr('data-img')).addClass('img-circle').removeClass('img-thumbnail');

            $(ProfileCommon.placeAvatarForm).find('div').remove();

            ProfileCommon.modalHide();
        },
        eventChangeImageProfile: function(){
            ProfileCommon.modalShow();
        },
        eventChange: function(){
            $(ProfileCommon.imageProfileBlock).find('img').attr('src', $(ProfileCommon.previewBlock).find('img').attr('src')).removeClass('img-circle').addClass('img-thumbnail');

            $(ProfileCommon.placeAvatarForm).append($(ProfileCommon.uploadedFieldAvatar));

            ProfileCommon.modalHide();

            $(ProfileCommon.btnSubmit).trigger('click');
        },
        eventDelete: function(){
            var url  = $('meta[name="csrf-param"]').attr('content').replace(/_csrf\-/, '') == 'backend' ? ProfileCommon.urlBackend : ProfileCommon.urlFrontend;

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    user_id: $('#user_id').text()
                },
                success: function(data){

                },
                error: function(error){

                }
            });
        },
        setPointer: function(){
            if(typeof $(ProfileCommon.uploadedFieldAvatar).prop('type') != 'undefined') {
                $(ProfileCommon.imageProfileBlock).find('img').attr('style', 'cursor:pointer;');
            }

            $(ProfileCommon.imageProfileBlock).find('img').attr('title', 'Go to Profile and click to picture for change');
        },
        modalShow: function(){
            $(ProfileCommon.modal).modal('show');
        },
        modalHide: function(){
            $(ProfileCommon.modal).modal('hide');
        },
        afterShow: function(){
            $(ProfileCommon.modal).find(ProfileCommon.previewBlock).attr('style', 'width:300px');
        }
    };

    ProfileCommon.init();
});
$(function () {
    const LOCAL = false;

    var serverNodeJS = LOCAL ? 'http://socket2wcall.local:44377' : 'http://socket.2wcall.com:44377';

    var LiveMessage = {
        socketServer: serverNodeJS,
        countSubmit: 0,
        tplTypeOperator: 'operator',
        tplTypeClient: 'client',
        tplTypeSystem: 'system',
        message_container: $('.messages_body'),
        form: $("#form-order"),
        quote_btn: ".reply",
        tab_link: $("#live-messages-tab"),
        old_message_counter: $(".old-messages"),
        new_message_counter: $(".new-messages"),
        message_input: $("[name*=order_messages]"),
        operator_message_tpl: $('.operator_message_tpl'),
        client_message_tpl: $('.client_message_tpl'),
        system_message_tpl: $('.system_message_tpl'),
        send_btn: $('.btn-send-live-message'),
        user_id: $("[name*=user_id]"),
        icon_is_new: $('.is_new'),
        types: {
            whatsapp: 'whatsapp'
        },
        temp: null,

        init: function () {
            console.log('LiveMessage init()');

            LiveMessage.quote();
            LiveMessage.readMessage();
            LiveMessage.checkInputMessage();
            LiveMessage.parseAction();
            LiveMessage.send_btn.unbind('click');
            LiveMessage.send_btn.on('click', LiveMessage.sendMessage);
            LiveMessage.tab_link.on('click', LiveMessage.scrollToBottom);

            LiveMessage.listenWS();

            LiveMessage.afterSubmitForm();

            //принимаем команды из iframe
            if (window.addEventListener) {
                window.addEventListener("message", LiveMessage.eventListener);
            } else {
                // IE8
                window.attachEvent("onmessage", LiveMessage.eventListener);
            }
        },
        afterSubmitForm: function () {
            LiveMessage.form.on('submit', function () {
                LiveMessage.countSubmit += 1;
                //ajax validation
                if (LiveMessage.countSubmit === 2) {
                    //to frontend/web/resources/widgets/navbar/order-messenger/order-messenger.js
                    toParent('submit_form_ok');
                }

            });
        },
        eventListener: function (event) {
            switch (event.data) {
                case 'form_order_on_save_from_messenger':
                    alert('form_order_on_save_from_messenger');
                    break;
            }
        },
        messengerIsActive: function () {
            return LiveMessage.tab_link.attr('aria-expanded') === 'true';
        },
        checkInputMessage: function () {
            LiveMessage.message_input.on('keyup keypress keydown paste', function () {
                console.log('------>', !($.trim($(this).val()).length > 0));
                LiveMessage.send_btn.prop('disabled', !($.trim($(this).val()).length > 0));
            });
        },
        parseAction: function () {
            LiveMessage.temp = LiveMessage.form.prop('action').match(/user_id=(\d{1,})/);
        },
        quote: function () {
            $(LiveMessage.message_container).on('click', LiveMessage.quote_btn, function () {
                LiveMessage.message_input.val('"' + $.trim($(this).closest('.testimonial-section').text()) + '"');
            });
        },
        readMessage: function () {
            LiveMessage.tab_link.on('click', function () {
                $.ajax({
                    url: '/order/live-message/read-messages',
                    type: 'post',
                    data: {
                        order_id: LiveMessage.form.data('id')
                    },
                    success: function (data) {
                        var response = JSON.parse(data);
                        var errorsRead = parseInt(response.countError);
                        var newMessages = parseInt(LiveMessage.new_message_counter.text());
                        var oldMessages = parseInt(LiveMessage.old_message_counter.text());

                        LiveMessage.old_message_counter.text(oldMessages + newMessages - errorsRead);
                        LiveMessage.new_message_counter.text(errorsRead > 0 ? newMessages - errorsRead : 0);

                        LiveMessage.setIsReaded(false);
                    },
                    error: function (error) {
                        console.log('Error reading message: ', error.responseText);
                    }
                });
            });
        },
        renderMessage: function (tplType, message) {
            var tpl;

            switch (tplType) {
                case LiveMessage.tplTypeOperator:
                    tpl = LiveMessage.operator_message_tpl;
                    break;
                case LiveMessage.tplTypeClient:
                    tpl = LiveMessage.client_message_tpl;
                    break;
                case LiveMessage.tplTypeSystem:
                    tpl = LiveMessage.system_message_tpl;
                    break;
            }

            if (tpl) {
                var classMessage = "o" + message.order_id;

                var messageTpl = '<div class="' + classMessage + ' hidden">' + tpl.html()
                        .replace('{date}', message.created_at)
                        .replace('{sender}', message.sender)
                        .replace('{message}', message.content) + '</div>';

                LiveMessage.message_container.append('<br/>');

                $(messageTpl).appendTo(LiveMessage.message_container);

                $('.' + classMessage).fadeIn(1500).removeClass('hidden');

                LiveMessage.scrollToBottom();

                if (message.is_new) {
                    tpl.find('.is_new').last().removeClass('hidden');
                }

                LiveMessage.message_container.find('.right').last().css('padding', '0px');
                LiveMessage.message_container.find('.left').last().css('padding', '0px');
            }
        },
        scrollToBottom: function () {
            LiveMessage.message_container.animate({scrollTop: 100000}, 1000);
        },
        sendMessage: function () {
            $('body').css('cursor', 'wait');

            if (!LOCAL) {
            LiveMessage.send_btn.prop('disabled', true);
            }
            LiveMessage.message_input.prop('disabled', true);

            $.ajax({
                url: '/order/live-message/add-operator-message',
                type: 'post',
                data: {
                    order_id: LiveMessage.form.data('id'),
                    user_id: LiveMessage.user_id.val() || LiveMessage.temp[1],
                    type: LiveMessage.types.whatsapp,
                    content: LiveMessage.message_input.val()
                },
                success: function (data) {
                    var response = JSON.parse(data);

                    console.log('response', response);

                    if (response.success) {
                        LiveMessage.message_input.prop('disabled', false);
                        LiveMessage.message_input.val('');

                        console.log('response', response);

                        LiveMessage.renderMessage('operator', response.message);
                    } else {
                        LiveMessage.message_input.prop('disabled', false);

                        toastr.options = {
                            "positionClass": "toast-bottom-right"
                        };
                        toastr.error(JSON.stringify(response.message));
                    }

                    $('body').css('cursor', 'pointer');
                },
                error: function (error) {
                    LiveMessage.message_input.prop('disabled', false);
                    LiveMessage.send_btn.prop('disabled', false);

                    console.log('Error adding message: ', error.responseText);

                    toastr.error(error.responseText);

                    $('body').css('cursor', 'pointer');
                }
            });
        },
        setIsReaded: function (timeout) {
            if (LiveMessage.messengerIsActive()) {
                setTimeout(function () {
                    $('.is_new').addClass('hidden');
                }, timeout ? 2500 : 0);
            }
        },
        listenWS: function () {
            var socket = io.connect(LiveMessage.socketServer);
            console.log('socket', socket);

            socket.emit('joinroom', LiveMessage.form.data('id').toString());

            if(LOCAL){
            console.log('live message join room: ', LiveMessage.form.data('id').toString())
            }

            socket.on('updatechat', function (msg) {
                console.log('event: updatechat')
                LiveMessage.eventFunction(msg);
            });
        },
        eventFunction: function (msg) {
            console.log('catched message', msg);

            if (msg.order_id.toString() === LiveMessage.form.data('id').toString()) {
                LiveMessage.renderMessage(LiveMessage.tplTypeClient, msg);

                if (!LiveMessage.messengerIsActive()) {
                    LiveMessage.addNotify(msg);
                }

                var new_increment;
                if (LiveMessage.messengerIsActive()) {
                    new_increment = parseInt(LiveMessage.old_message_counter.text()) + 1;
                    LiveMessage.old_message_counter.html(new_increment);
                } else {
                    new_increment = parseInt(LiveMessage.new_message_counter.text()) + 1;
                    LiveMessage.new_message_counter.html(new_increment);
                }

                LiveMessage.setIsReaded(true);
            }
        },
        addNotify: function (message) {
            toastr.info('You have new message from "' + message.sender + '": <i>' + message.content + '</i>');
        }

    };

    LiveMessage.init();
});

//отправка сообщений на родетельскуое
function toParent(command) {
    var parent = window.parent;
    parent.postMessage(command, '*');
}


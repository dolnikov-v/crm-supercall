$(function () {
    var Order = {
        package_id: '[name*=package_id]',
        quantity: '[name*=quantity]',
        init: function () {
            $('.products').on('change', Order.package_id, function (e) {
                Order.setPrice(e.target);
            });
        },
        setPrice: function (target) {
            $(target).closest(".product").find(Order.quantity).val(1);
            $(target).closest(".product").find(Order.quantity).trigger('change');
        },
    };

    Order.init();
});

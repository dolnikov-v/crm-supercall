$(function () {
    var QueueControl = {
        url: '/queue/control/get-queues-by-params',
        filter_queue_id: null,
        countries: $("[name*=countries]"),
        queues: $("[name*=last_queue_id]"),
        partner: $("[name*=partner_id]"),
        init: function () {
            QueueControl.countries.on('change', QueueControl.getQueues);
            QueueControl.countries.trigger('change');
        },
        setQueue: function () {
            $(QueueControl.queues).val(parseUrl('OrderSearch[last_queue_id]'))
        },
        getQueues: function () {
            QueueControl.filter_queue_id = QueueControl.queues.val();
            QueueControl.disabledQueues();

            if (QueueControl.countries.val() !== '')
                $.ajax({
                    url: QueueControl.url,
                    type: 'post',
                    data: {
                        partner_id: QueueControl.partner.val(),
                        secondary_country_id: QueueControl.countries.val()
                    },
                    success: function (data) {
                        var response = JSON.parse(data);
                        QueueControl.clearQueues();
                        QueueControl.enabledQueues(response);
                        QueueControl.addEmptyOption();
                        QueueControl.addOptions(response);

                        if (Object.keys(response).length === 1 && response[0].id === '') {
                            toastr.error('No queue found for this country');
                        }

                        QueueControl.queues.val(QueueControl.filter_queue_id);
                        QueueControl.setQueue();

                    },
                    error: function (error) {
                        toAstr.error(error.responseText)
                    }
                });
        },
        disabledQueues: function () {
            QueueControl.queues.prop('disabled', true);
            QueueControl.queues.val('')
        },
        enabledQueues: function () {
            QueueControl.queues.prop('disabled', false);
        },
        clearQueues: function () {
            QueueControl.queues.val('');
            QueueControl.queues.find('option').remove();
        },
        addOptions: function (options) {
            for (var k in options) {
                if (options[k].id !== '') {
                    var newOption = new Option(options[k].text, options[k].id, false, false);
                    QueueControl.queues.append(newOption)
                }
            }

            QueueControl.queues.select();
            QueueControl.queues.val(QueueControl.getParameterByName('OrderSearch[last_queue_id]'));
            QueueControl.queues.trigger('change');
        },
        addEmptyOption: function () {
            var emptyOption = new Option('—', '', false, false);
            QueueControl.queues.append(emptyOption)
        },
        getParameterByName: function (name, url) {
            if (!url)
                url = window.location.href;

            url = decodeURIComponent(url);
            name = name.replace(/[\[\]]/g, "\\$&");

            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
            var results = regex.exec(url);

            if (!results)
                return null;
            if (!results[2])
                return '';

            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    };


    QueueControl.init();
});
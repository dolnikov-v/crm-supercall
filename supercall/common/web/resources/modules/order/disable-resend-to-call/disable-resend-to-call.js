$(function () {
    var ResendToCall = {
        checkboxStatus: $("#ordersearch-status_queue_strategy"),
        checkIsDisabled: $('#check-disabled'),
        buttonResendToCall: $("#resend_to_call_button"),
        status: $("[name*=status]"),
        operator: $("[name*=operator]"),
        country: $('[name*=countries]'),

        init: function () {
            ResendToCall.disableSelectOperator();
            ResendToCall.disableCheckboxStatus();
            ResendToCall.disableButtonResendCall();

            ResendToCall.operator.on('change', ResendToCall.disableCheckboxStatus);
            ResendToCall.status.on('change', ResendToCall.disableCheckboxStatus);
            ResendToCall.checkboxStatus.on('click', ResendToCall.disableSelectOperator);
            ResendToCall.checkIsDisabled.on('click', ResendToCall.alertDisabledCheckbox);
            ResendToCall.country.on('change', ResendToCall.disableButtonResendCall);
        },

        /**Блокировка поля выбора оператора*/
        disableSelectOperator: function () {
            if (ResendToCall.checkboxStatus.prop('checked')) {
                ResendToCall.operator.prop('disabled', true);
            } else {
                ResendToCall.operator.prop('disabled', false);
            }
        },

        /**Блокировка чекбокса "Cтатус выставлен стратегией очереди"*/
        disableCheckboxStatus: function () {
            if (ResendToCall.operator.val() !== '' && ResendToCall.operator.val() != null) {
                ResendToCall.checkboxStatus.prop({
                    'disabled': true,
                    'checked': false
                });
                ResendToCall.checkboxStatus.css('z-index','0');
            } else {
                if (ResendToCall.status.val() !== '' && ResendToCall.status.val() != null) {
                    ResendToCall.checkboxStatus.prop('disabled', false);
                } else {
                    ResendToCall.operator.prop('disabled', false);
                    ResendToCall.checkboxStatus.prop({
                        'disabled': true,
                        'checked': false
                    });
                    ResendToCall.checkboxStatus.css('z-index','0');
                }
            }
        },

        /**Блокировка кнопки "Отправить повторно на обзвон" в групповых операциях*/
        disableButtonResendCall: function () {
            if (ResendToCall.country.val() != null && ResendToCall.country.val().length == 1) {
                ResendToCall.buttonResendToCall
                    .prop('disabled', false)
                    .addClass('btn-info')
                    .removeClass('btn-default')
                    .attr('title', '');
            } else {
                ResendToCall.buttonResendToCall
                    .prop('disabled', true)
                    .addClass('btn-default')
                    .removeClass('btn-info')
                    .attr('title', 'Должна быть указана одна страна');
            }
        },

        /** Сообщение при заблокированном чекбоксе */
        alertDisabledCheckbox: function () {
            if (ResendToCall.checkboxStatus.is(':disabled')) {
                toastr.error("Select order status and remove operator");
            }
        }

    };

    ResendToCall.init();
});
//применение в методе callBtnEvents файл frontend/web/resources/widgets/navbar/zoiper-slide-style/bootsrap_caller.js
$(function () {
    var FAP = {
        DEBUG: false,
        container: '#form_additional_params',
        attribute: 'data-additional_params',
        fieldParams: '[name*=form-additional-params]',
        undefined: 'Not determined',
        init: function () {
            FAP.setFieldParams();
            FAP.listenerFieldParams();
        },
        getParams: function () {
            var container = $(FAP.container);

            if (!isUndefined(container)) {
                return $(container).attr(FAP.attribute);
            } else {
                return {};
            }
        },
        parseParams: function () {
            return JSON.parse(FAP.getParams());
        },
        setFieldParams: function () {
            var params = FAP.parseParams();
            var collectionParams = {};

            for (var k in params) {
                collectionParams[k] = isUndefined($(params[k])) ? FAP.undefined : $(params[k]).val();
            }

            $(FAP.fieldParams).val(JSON.stringify(collectionParams));
        },
        listenerFieldParams: function () {
            var params = FAP.parseParams();
            //!Важно - если есть поля, которые в процессе могут менять свои значения - дополнять вызовом trigger('change'),
            //например - поле получит значение програмно, т.е. не от пользователя, а в процессе работы алгоритма, тогда назначение его значения должно сопутствоваться
            // old $(field).val(newValue)
            // new $(field).val(newValue).trigger('change')
            //т.к. необходимо пересобирать json строку с параметрами
            for (var k in params) {
                if (!isUndefined($(params[k]))) {
                    $(params[k]).on('change', function () {
                        FAP.setFieldParams();
                        if (FAP.DEBUG) {
                            console.log('init: ', $(this).prop('name'));
                            console.log('result FAP.fieldParams: ', $(FAP.fieldParams).val());
                        }
                    });
                }
            }
        }
    };

    try {
        FAP.init();
    } catch (e) {
        console.log(e);
    }
});
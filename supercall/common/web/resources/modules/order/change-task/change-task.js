document.domain = $("#document_domain").text();

$(document).ready(function () {
    window.$form_task = $("#form-order_task");
    defaultSP = $('.default-shipping-price input').val();
    $_productsPriceData = $('#products-price-data').data('products-price')

    window.order_task = {
        editable: null,
        id_param: null,
        id: null,
        partner_id: null,
        country: null,
        status: null,

        type: null,
        general: {},
        phones: [],
        products: [],
        shipping_id: null,
        customer_address: null,

        total_quantity: 0,
        total_price: 0,
        total_final: 0,
        total_final_no_vat: 0,
        shipping_price: 0,
        shipping_vat: null,

        update: function () {
            order_task.id_param = $form_task.data("id_param");
            order_task.id = $form_task.data("id");
            order_task.partner_id = $form_task.data("partner_id");
            order_task.country_id = $form_task.data("country_id");
            order_task.country = $form_task.data("country");
            order_task.status = $("#order-status").val();
            // default shippingPrice, if direct api request
            var defaultShippingPrice = $('.default-shipping-price input').val();

            order_task.type = $form_task.find("[name*=type_id]").val();

            order_task.general = {};
            $form_task.find("[name*=Order\\[general\\]]").each(function () {
                var name = $(this).attr("name").slice("Order[general]".length + 1, -1);
                order_task.general[name] = $(this).val();
            });


            order_task.phones = [];
            $form_task.find("[name*=Order\\[phones\\]]").each(function () {
                order_task.phones.push($(this).val());
            });

            order_task.shipping_id = $form_task.find("[name*=shipping_id]").val();
            order_task.customer_address = $form_task.find("[name*=customer_address]").val();


            order_task.total_quantity = 0;
            order_task.total_price = 0;
            order_task.total_final = 0;
            order_task.total_final_no_vat = 0;
            order_task.shipping_price = 0;

            order_task.products = [];
            $form_task.find(".product_task").each(function () {
                var $this = $(this);

                var product = {
                    product_id: $this.find("[name*=product_id]").val(),
                    product_name: $this.find("[name*=product_id] option:selected").text(),
                    quantity: $this.find("[name*=quantity]").val(),
                    price: $this.find("[name*=price]").val(),
                    cost: $this.find("[name*=cost]").val(),
                    gift: $this.find("[type=checkbox][name*=gift]").prop("checked")
                };
                if (product.product_id) {
                    product.vat = $_productsPriceData[product.product_id][product.quantity].vat;
                } else {
                    product.vat = 0;
                }

                order_task.products.push(product);

                var quantity = parseInt(product.quantity);
                if (isNaN(quantity)) {
                    return;
                }

                var price = parseFloat(product.price);
                if (isNaN(price)) {
                    return;
                }

                var cost = parseFloat(product.cost);
                if (isNaN(cost)) {
                    return;
                }

                var vat = parseFloat(product.vat);
                if (isNaN(vat)) {
                    vat = parseFloat($form_task.data("vat"));
                    if (isNaN(vat)) {
                        vat = 0.0;
                    }
                }

                order_task.total_quantity += quantity;
                order_task.total_price += cost;
                order_task.total_final_no_vat += cost;
                order_task.total_final += cost + (cost * vat / 100);
            });

            var extra_price = parseFloat($form_task.data("extra-price"));
            if (isNaN(extra_price)) {
                extra_price = 0;
            }
            order_task.total_final += extra_price;
            order_task.total_final_no_vat += extra_price;

            var dynamic_shipping_price = $form_task.find("[name*=shipping_id] option:selected").data("dynamic_price");
            if (dynamic_shipping_price) {
                if (typeof(calc_shipping) === "function") {
                    order_task.shipping_price = parseInt(calc_shipping(order));
                    if (isNaN(order_task.shipping_price)) {
                        throw "Некорректная функция расчета стоимости доставки";
                    }
                } else {
                    throw "Не определена функция расчета стоимости доставки";
                }

                if (typeof(calc_shipping_vat) === "function") {
                    order_task.shipping_vat = parseFloat(calc_shipping_vat(order));
                    if (isNaN(order_task.shipping_vat)) {
                        throw "Некорректная функция расчета НДС стоимости доставки";
                    }
                } else {
                    order_task.shipping_vat = parseFloat($form_task.data("vat"));
                    if (isNaN(order_task.shipping_vat)) {
                        order_task.shipping_vat = 0.0;
                    }
                }
            }
            else {
                order_task.shipping_price = parseFloat($form_task.find("[name*=shipping_id] option:selected").data("price"));
                if (isNaN(order_task.shipping_price)) {
                    // check find shipping_price in the input by direct api-request
                    if (defaultSP)
                        order_task.shipping_price = Number(defaultSP);
                    else
                        order_task.shipping_price = 0.0;
                }
                order_task.shipping_vat = parseFloat($form_task.data("vat"));
                if (isNaN(order_task.shipping_vat)) {
                    order_task.shipping_vat = 0.0;
                }

            }

            order_task.total_final_no_vat += order_task.shipping_price;
            order_task.total_final += order_task.shipping_price + (order_task.shipping_price * order_task.shipping_vat / 100);

            $form_task.find(".total-quantity_task").text(order_task.total_quantity);
            $form_task.find(".total-price_task").text(order_task.total_price.toFixed(2));
            $form_task.find(".total-final_task").text(order_task.total_final.toFixed(2));
            $form_task.find(".total-final-no-vat_task").text(order_task.total_final_no_vat.toFixed(2));

            $form_task.find("[name*=init_price_task]").val(order_task.total_price);
            $form_task.find("[name*=final_price_task]").val(order_task.total_final);
            $form_task.find("[name*=shipping_price_task]").val(order_task.shipping_price);
        }
    };

    window.form_task = {
        add_product: function () {
            var $products = $(this).closest("fieldset").find(".products_task");
            var index = 0;
            while ($("#product-" + index).length == 1) {
                index++;
            }

            var $products = $(this).closest("fieldset").find(".products_task")
            var $productForClone = $products.find('.product_task').first()
            var originalProductId = $($productForClone).attr('id')

            var originalIdArray = originalProductId.split('-')
            var originalId = originalIdArray[1]

            // replace productId
            var htmlRow = $($productForClone).html()
            htmlRow = htmlRow.replace(new RegExp(originalProductId, "igm"), 'product-' + index);

            // replace from OrderProduct[originalId] to OrderProduct[index]
            var orderProductIdReplace = 'OrderProduct\\[' + originalId + '\\]'
            var orderProductIdNew = 'OrderProduct\[' + index + '\]'
            htmlRow = htmlRow.replace(new RegExp(orderProductIdReplace, "igm"), orderProductIdNew)

            $('<div class="product_task row form-group" id="product-' + index + '">' + htmlRow + '</div>').appendTo(".products_task");
            var $newRow = $(".products_task").find('#orderproduct-' + index + '-id')
            $newRow.removeAttr('value')

            if (isCheckGiftById(originalId)) {
                $('#orderproduct-' + index + '-gift').prop("checked", true)
                useCheckGiftById(index)
            } else {
                $('#orderproduct-' + index + '-gift').prop("checked", false)
            }

            //set empty
            $("#orderproduct-" + index + "-product_id").val('');
            $("#orderproduct-" + index + "-quantity").val('');
            $("#orderproduct-" + index + "-price").val('');

            //Акции
            setFilterPackagesByProduct($(this));

            var product_row = $('.products').find('.product').last();
            var product = product_row.find('[name*=product_id]');
            var quantity = product_row.find('[name*=quantity]');
            var package = product_row.find('[name*=package_id]');

            setReadOnly(product, false);
            product.val('');

            setReadOnly(quantity, false);
            quantity.val(1);

            package.empty();
            package.append(new Option('-', '')).val('');

            package.val('');
        },
        add_product_task_gift: function () {
            var $products = $(this).closest("fieldset").find(".products_task_gift");
            var index = 0;
            while ($("#product-" + index).length == 1) {
                100*(index++);
            }

            var $products = $(this).closest("fieldset").find(".products_task_gift")
            var $productForClone = $products.find('.product_task_gift').first()
            var originalProductId = $($productForClone).attr('id')

            var originalIdArray = originalProductId.split('-')
            var originalId = originalIdArray[1]

            // replace productId
            var htmlRow = $($productForClone).html()
            htmlRow = htmlRow.replace(new RegExp(originalProductId, "igm"), 'product-' + index);

            // replace from OrderProduct[originalId] to OrderProduct[index]
            var orderProductIdReplace = 'OrderProduct\\[' + originalId + '\\]'
            var orderProductIdNew = 'OrderProduct\[' + index + '\]'
            htmlRow = htmlRow.replace(new RegExp(orderProductIdReplace, "igm"), orderProductIdNew)

            $('<div class="product_task_gift row form-group" id="product-' + index + '">' + htmlRow + '</div>').appendTo(".products_task_gift");
            var $newRow = $(".products_task").find('#orderproduct-' + index + '-id')
            $newRow.removeAttr('value')

            //акции
            var gifts_row = $('.products_task_gift').find('.product_task_gift').last();
            var product_gift = gifts_row.find('[name*=product_id]');
            var quantity_gift = gifts_row.find('[name*=quantity]');
            var package_gift = gifts_row.find('[name*=package_id]');

            setReadOnly(product_gift, false);
            product_gift.val('');

            setReadOnly(quantity_gift, false);
            quantity_gift.val(1);

            package_gift.val('');

            gifts_row.find('.gift-icon').html();
        },
        remove_product: function () {
            if ($(".products_task .product_task").length > 1) {
                $(this).closest(".product_task").remove();

                //акции
                if(typeof $(this).closest(".product_task").attr('identity') != 'undefined') {
                    var identity = $(this).closest(".product_task").attr('identity').replace('product_', '');

                    if (typeof identity != 'undefined') {
                        $('.products_task_gift').find('.product_task_gift').map(function () {
                            if ($(this).attr('identity') == 'gift_' + identity) {
                                $(this).closest('.product_task_gift').remove();
                            }
                        });
                    }
                }
                //end

                order_task.update();
            }
        },
        remove_product_task_gift: function () {
            //удаление подарков вместе с акциями
            if($(".products_task_gift .product_task_gift").length == 1){
                //акции
                var group_gifts = $('.products_task_gift').find('.product_task_gift').first();

                var gift_product = group_gifts.find('[name*=product_id]');
                var gift_quantity = group_gifts.find('[name*=quantity]');
                var package = group_gifts.find('[name*=package_id]');

                setReadOnly(gift_product, false);
                gift_product.val('');

                setReadOnly(gift_quantity, false);
                gift_quantity.val(1);

                package.val('');

                group_gifts.find('.gift-icon span').remove();

                group_gifts.attr('identity', '');
            }

            if ($(".products_task_gift .product_task_gift").length > 1) {
                $(this).closest(".product_task_gift").remove();
                order_task.update();
            }
        },

        //мониторим остатки на складе по данному товару
        set_stock: function (product_id, $amount, quantity) {
            //для новых заказов
            if (typeof $_productsPriceData[product_id] == 'undefined') {
                return;
            }

            if (quantity === null) {
                var $quantity = $amount.parent().closest(".product_task").find("[name*=quantity]");
                var quantity = $quantity.val();
            }

            if (typeof $_productsPriceData[product_id].amount == 'undefined') {
                $_productsPriceData[product_id].amount = $_productsPriceData[product_id][quantity].amount - parseInt(quantity);
            }

            //посчитать выбранное quantity по данному товару в заказе
            //этот функционал только на клиенте - для удобства работы оператора
            //фактическое "списание" - во время сохранения заказа - средствами и данными доступными уже в Yii
            var quantities = $("#form-order_task [name*=quantity]");
            var sum_quantity = 0;

            quantities.map(function () {
                var id = $(this).prop('id');
                //если заказ новый, то orderProduct не существует, поэтому product_id вытащить на сервере для формы не получится
                //сформируем правильный id для селекта товаров
                if ($(this).val() != '') {
                    if (/select_*/.test($(this).prop('id')) === false) {
                        $(this).prop('id', 'select_' + product_id);
                    }
                }

                var product_id_by_is_select = id.replace('select_', '');

                if (product_id_by_is_select == product_id) {
                    sum_quantity += parseInt($(this).val());
                }
            });

            //остаток "на складе" после указания кол-ва данного товара
            var in_stock = $_productsPriceData[product_id].amount - sum_quantity

            if (product_id in $_productsPriceData) {
                var value = in_stock <= 0 ? 0 : in_stock;
            } else {
                var value = 0;
            }
            //если вдруг нужна история
            //$amount.val('(' + value + ')');

            var amountes = $("#form-order_task [name*=amount]");

            amountes.map(function () {
                if ($(this).prop('id').replace('amount_', '') == product_id) {
                    $(this).val('(' + value + ')');
                    $(this).css('color', (value > 0 ? 'green' : 'red'));
                }
            });

        },

        on_product_change: function () {
            var $id = $(this).closest(".product_task").find("[name*=id]");
            var $price = $(this).closest(".product_task").find("[name*=price]");
            var $cost = $(this).closest(".product_task").find("[name*=cost]");
            var $vat = $(this).closest(".product_task").find("[name*=vat]");
            var $quantity = $(this).closest(".product_task").find("[name*=quantity]");
            var $amount = $(this).closest(".product_task").find("[name*=amount ]");

            //акции
            var package = $(this).closest(".product_task").find("[name*=package]");

            var quantity = parseInt($quantity.val());
            if (isNaN(quantity)) {
                quantity = 0;
            }

            $amount.prop('id', 'amount_' + $(this).val());

            // получим index
            var indexArray = $id.attr('id').split('-')
            var index = indexArray[1]

            if (isCheckGiftById(index) == false) {
                // get prices from Product
                var product_id = this.value

                $quantity.prop('id', 'select_' + product_id);

                if ((product_id in $_productsPriceData) && (quantity in $_productsPriceData[product_id])) {
                    response = $_productsPriceData[product_id][quantity]
                    $price.val(response.price);

                    var cost;
                    if (typeof(calc_price) === "function") {
                        cost = calc_price(response.price, quantity);
                    }
                    else {
                        cost = quantity * response.price;
                    }
                    $cost.val(cost);

                    $vat.val(response.vat).trigger("change-vat");
                    order_task.update();
                }

            }

            var $amount = $(this).closest(".product_task").find("[name*=amount]");
            window.form_task.set_stock(product_id, $amount, null);

            //акции фильтрация акций по продукту
            setFilterPackagesByProduct($(this));
        },
        on_quantity_change: function () {
            var $id = $(this).closest(".product_task").find("[name*=id]");
            var $price = $(this).closest(".product_task").find("[name*=price]");
            var $cost = $(this).closest(".product_task").find("[name*=cost]");
            var product_id = parseInt($(this).closest(".product_task").find("[name*=product_id]").val());
            var quantity = this.value;
            if (isNaN(product_id)) {
                return;
            }

            // получим index
            var indexArray = $id.attr('id').split('-')
            var index = indexArray[1]

            if (isCheckGiftById(index) == false) {
                // get prices from Product
                response = $_productsPriceData[product_id][quantity]
                $price.val(response.price);

                var cost;
                if (typeof(calc_price) === "function") {
                    cost = calc_price(response.price, quantity);
                }
                else {
                    cost = quantity * response.price;
                }
                $cost.val(cost);

                order_task.update();
            }

            var $amount = $(this).closest(".product_task").find("[name*=amount]");
            window.form_task.set_stock(product_id, $amount, null);
        },
        on_vat_change: function (e) {
            var $this = $(e.target);
            if (parseInt($this.val())) {
                $this.removeClass("hidden");
            }
            else {
                $this.addClass("hidden");
            }
        },
        on_gift_change: function (e) {
            var $this = $(e.target);
            $product = $this.closest(".product_task")

            if ($this.prop("checked")) {
                useCheckGift($this)
                order_task.update();
            }
            else {
                $product.find("[name*=quantity]").trigger("change");
                $product.find("[name*=product_id]").trigger("change");
            }
        },

        add_phone: function () {
            if ($(".order-phones_task .order-phone_task").length < 3) {
                $(".order-phones_task").append('<div class="form-group input-group order-phone_task">' +
                    '<input class="form-control pull-left" name="Order[phones][]" type="text">' +
                    '<span class="input-group-btn">' +
                    '<button class="btn btn-danger btn-sm pull-right btn-remove-phone_task" type="button"><span class="fa fa-minus"></span></button>' +
                    '</span>' +
                    '</div>');
            }
        },
        remove_phone: function () {
            $(this).closest(".order-phone_task").remove();
            order_task.update();
        },

        on_shipping_change: function () {
            order_task.update();
        },

        on_message_click: function () {
            var $modal = $("#modal-message");

            var to = $(this).closest(".input-group").find(":input").val();

            $modal.find("textarea").data("to", to);
            $modal.find("textarea").data("type", $(this).hasClass("btn-email") ? "email" : "sms");
            $modal.modal();
        },
        on_message_send: function () {
            var $button = $(this);
            var $modal = $("#modal-message");

            var data = $modal.find("textarea").data();
            data.text = $modal.find("textarea").val();
            data[yii.getCsrfParam()] = yii.getCsrfToken();

            $button.prop("disabled", true);
            $.post("message?id=" + order_task.id_param, data)
                .success(function (response) {
                    $button.prop("disabled", false);
                    $modal.modal("hide");
                    toastr.info(response.message);
                })
                .error(function (response) {
                    $button.prop("disabled", false);
                    toastr.error(response.message);
                });
        },

        gmap_autocomplete: null,
        on_gmap_autocomplete: function () {
            $form_task.find("[data-gmap-address-component]").val("");
            var components = form_task.gmap_autocomplete.getPlace().address_components;
            for (var i = 0; i < components.length; i++) {
                var component = components[i];
                $form_task.find("[data-gmap-address-component=" + component.types[0] + "]").val(component.long_name);
            }
        },

        genesys_status_map: {
            3: 40,
            4: 34,
            5: 35,
            6: 37,
            7: 36
        },

        on_status_open: function () {
            var statusValue = $(this).data("status")
            $form_task.find('#order-status').val(statusValue)

            // custom for recall-status
            $("#modal-status-" + $(this).data("status")).modal().find(":input").prop("disabled", 0)
            if (statusValue == 2) {
                $recall_input = $('#order-recall_date')
                if ($recall_input.val() == '') {
                    $btn_save_recall = $('#modal-status-' + statusValue + ' .modal-footer .btn-status-save')
                    $btn_save_recall.attr('disabled', 'disabled')

                    $recall_input.bind('keyup change', function () {
                        if ($recall_input.val() != '') {
                            $btn_save_recall.removeAttr('disabled')
                        } else {
                            $btn_save_recall.attr('disabled', 'disabled')
                        }
                    })
                }

            }
        },
        before_on_status_open : function(){
            form_task.check_zip_codes();
        },
        check_zip_codes : function(){
            if($("#form-order_task #google_zip_task").val() == ''){
                window.form_task.on_status_save();
            }else if($("#form-order_task #google_zip_task").val() == $("#form-order_task [name*=zip]").val()){
                window.form_task.on_status_save();
            }
            else{
                $("#field_task_google_zip").html($("#form-order_task #google_zip_task").val());
                $("#field_task_customer_zip").html($("#form-order_task [name*=zip]").val());
                $('#confirm_task_google_zip').modal('show');
            }
        },
        on_status_save: function () {
            var newStatus = $(this).data("status");
            $("#order-status").val(newStatus);

            // validate product before save
            var is_empty_product = 0;
            $('.product-select select').each(function () {
                if ($(this).val() == '') {
                    is_empty_product += 1;
                    var $currentId = $(this).attr('id')
                    $(this).parent('.field-' + $currentId).removeClass('has-success').addClass('has-error');
                }
            });

            if (is_empty_product > 0) {
                toastr.error('Необходимо заполнить «Товар»');
            } else {
                $("#form-order_task :input:not(:checkbox), .btn-status-save_task").prop("disabled", true);
            }
        },
        on_status_dismiss: function () {
            $(this).find(":input").prop("disabled", 1);
            // reset loader
            $('.btn-status-save').show()
            $('.loader').remove()
        },
        on_save: function () {
            order_task.update();
            if (typeof(wde_set_call_result) === "function" && form_task.genesys_status_map[order_task.status]) {
                wde_set_call_result(form_task.genesys_status_map[order_task.status]);
            }
            if (typeof(wde_set_callback_date_time) === "function" && order_task.status == 2) {
                wde_set_callback_date_time($("#order-recall_date").val());
            }
            if (typeof(window.parent.frontend_set_callback_param_get_order) === "function") {
                window.parent.frontend_set_callback_param_get_order();
            }
        },

        init: function () {
            var products = $("#form-order_task [name*=product_id]");

            products.map(function () {
                $(this).trigger('change');
                var product_id = $(this).val();
                var $quantity = $(this).parent().closest(".product_task").find("[name*=quantity]");

                $quantity.prop('id', 'select_' + product_id);

                var $amount = $(this).parent().closest(".product_task").find("[name*=amount]");

                if(typeof $_productsPriceData[product_id] != 'undefined'){
                    var value = $_productsPriceData[product_id].amount;
                    $amount.val('(' + value + ')');
                    $amount.css('color', (value > 0 ? 'green' : 'red'));
                }
            });

            //акции фильтрация акций
            initFilterPackages();
        },

        on_package_change: function () {
            //активация акции
            setGroupGiftsFromPackage($(this));
        }
    };

    //Инициализация функций акций в change.js

    $('.products_task .product_task').trigger('change');

    /**
     * Акции
     * Псевдо disabled для Select
     * @param  elem elem from DOM
     * @param action bool
     */
    function setReadOnly(elem, action){
        var color = action ? '#99A5C2' : 'black';
        var css = action ? 'none' : 'auto';
        elem.css('pointer-events', css);
        elem.css('color', color);

        if(!action){
            elem.attr('size', elem.children('option').length);
            elem.attr('size', 0);
        }
    }

    /**
     * Акции
     * Определяет - в readOnly элемент или нет
     * @param elem
     * @returns {boolean}
     */
    function isReadOnly(elem){
        return elem.css('pointer-events') == 'none';
    }

    /**
     * Акции
     * Отфильтровывает список доступных акций по выбранному товару
     */
    function initFilterPackages(){
        var paid_products = $('.product_task [name*=product_id]');

        paid_products.map(function(){
            var name = $(this).prop('name');
            var val = $(this).val();
            var index = name.replace('OrderProduct[', '').replace('][product_id]', '');
            var quantity = $('[name="OrderProduct['+index+'][quantity]"]');

            var package = $(this).closest(".product_task").find("[name*=package]");
            var packages_data = $('#order-task-package').data('order-package');
            var packageOfProduct = [];

            for (var k in packages_data) {
                if (packages_data[k].product == $(this).val()) {
                    packageOfProduct.push([packages_data[k].id, packages_data[k].name]);
                }
            }

            package.empty();
            package.append(new Option('-', '')).val('');

            for (id in packageOfProduct) {
                package.append(new Option(packageOfProduct[id][1], packageOfProduct[id][0]));
            }

            package.val($(this).attr('package'));
        });
    }

    /**
     * Акции
     * Выставит список акций по выбранному продукту
     * onchange select product
     * @param elemProduct
     */
    function setFilterPackagesByProduct(elemProduct){
        if (!isReadOnly(elemProduct)) {
            var package = elemProduct.closest(".product_task").find("[name*=package]");
            var packageOfProduct = [];

            for (var k in $_packages_data) {
                if ($_packages_data[k].product == elemProduct.val()) {
                    packageOfProduct.push([$_packages_data[k].id, $_packages_data[k].name]);
                }
            }

            package.empty();
            package.append(new Option('-', '')).val('');

            for (id in packageOfProduct) {
                package.append(new Option(packageOfProduct[id][1], packageOfProduct[id][0]));
            }
        }
    }

    /**
     * Акции
     * Выставить или создаст новую группу подарков и выставит значения акции, залочит поля
     * @param elem from DOM packageSelect
     */
    function setGroupGiftsFromPackage(packageSelect){
        var package_id = packageSelect.val();

        var product = packageSelect.closest('.product_task').find('[name*=product_id]');
        var quantity = packageSelect.closest('.product_task').find('[name*=quantity]');

        var nameProduct = product.prop('name');
        var split = nameProduct.match(/([0-9]{1,})/);
        var index = split[0];

        if(package_id != '') {
            var package = $_packages_data[package_id];

            setReadOnly(product, true);
            setReadOnly(quantity, true);

            if(product.closest('.product_task').attr('identity') =='' || typeof product.closest('.product_task').attr('identity') == 'undefined') {
                product.closest('.product_task').attr('identity', 'product_' + index);
                var group_gifts = getGroupGiftsByIdentity(index);
                group_gifts.attr('identity', 'gift_' + index);
            }else{
                var index = product.closest('.product_task').attr('identity').replace('product_', '');
                var group_gifts = getGroupGiftsByIdentity(index);
            }

            product.val(package.product)

            var gift_product = group_gifts.find('[name*=product_id]');
            var gift_quantity = group_gifts.find('[name*=quantity]');
            var gift_package = group_gifts.find('[name*=package_id]');

            group_gifts.find('.gift-icon').html('<span style="font-size: 16px; padding-top:6px" title="'+package.name+'" class="glyphicon glyphicon-gift"></span>');

            setReadOnly(gift_product, true);
            gift_product.val(package.product);

            setReadOnly(gift_quantity, true);
            gift_package.val(package_id);

            gift_quantity.val(package.free_amount);
            quantity.val(package.paid_amount);
            quantity.trigger('change');

        }else{
            if(product.closest('.product_task').attr('identity') !='' ){
                var index = product.closest('.product_task').attr('identity').replace('product_', '');
            }
            //акция была снята с группы товаров
            var group_gifts = getGroupGiftsByIdentity(index);
            var gift_product = group_gifts.find('[name*=product_id]');
            var gift_quantity = group_gifts.find('[name*=quantity]');
            var gift_package = group_gifts.find('[name*=package_id]');

            gift_product.val('');
            setReadOnly(gift_product, false);

            gift_quantity.val(1);
            setReadOnly(gift_quantity, false);

            gift_package.val('');

            setReadOnly(product, false);
            setReadOnly(quantity, false);

            group_gifts.find('.gift-icon span').remove();
        }
    }

    /**
     * Акции
     * Метод возвражает группу подарков - если она подходит по швутешен
     * если нет совпадений - будет выбрана группа с пустым продуктом
     * еслии такой не найдено - будет создана новая группа
     * @param identity
     * @returns {string}
     */
    function getGroupGiftsByIdentity(identity){
        var group_gifts = $('.products_task_gift').find('.product_task_gift');
        var founded_group = '';

        //если найдена группа подарков по identity
        group_gifts.map(function(){
            if($(this).attr('identity') == 'gift_' + identity){
                founded_group = $(this);
            }
        });

        //группа не была найдена - подойдёт любая свободная
        if(founded_group == '') {
            group_gifts.map(function () {
                if ($(this).find('[name*=product_id]').val() == '') {
                    founded_group = $(this);
                }
            });

            //нету свободных групп - создадим
            if(founded_group == '') {
                $('.btn-add-product_task_gift').trigger('click');
                founded_group = $('.products_task_gift').find('.product_task_gift').last();
            }
        }

        return founded_group;
    }

    //модальное окно проверки корректности zip кода, после его закрытия - вызвать весь функционал кнопока смены статуса
    $('#confirm_task_google_zip').on('hidden.bs.modal', function () {
        window.form_task.on_status_save;

        $.ajax({
            type: 'POST',
            url: '/order/change/index/' + $("#order-order_id").val(),
            data: $('form').serialize(),
            success: function (res) {
                res = jQuery.parseJSON(res);

                if (res.success === true) {
                    toastr.success(res.message);
                }
                else {
                    toastr.error(res.message);
                }

                $("#form-order_task :input:not(:checkbox), .btn-status-save_task").prop("disabled", false);

                //reload tab history tasks
                getOrderTasks();
            },
            error: function (res) {
                res = jQuery.parseJSON(res);
                toastr.error(res.message);
                $('.btn-status-save_task').prop('disabled', '');
                $("#form-order_task :input:not(:checkbox), .btn-status-save_task").prop("disabled", false);
            }
        });
    })

    //modal google zip confirm
    $("#google_task_confirm_success").on('click', function(){
        $("#form-order_task [name*=zip]").val($("#form-order_task #google_zip_task").val());
        $('#confirm_task_google_zip').modal('hide');
    });
    $("#google_task_confirm_cancel").on('click', function(){
        $('#confirm_task_google_zip').modal('hide');
    });


    window.form_task.init();

    $form_task.on("change", "[name*=package_id]", form_task.on_package_change);

    $form_task.on("beforeSubmit", form_task.on_save);

    $form_task.on("click", ".btn-add-phone_task", form_task.add_phone);
    $form_task.on("click", ".btn-remove-phone_task", form_task.remove_phone);

    $form_task.on("click", ".btn-add-product_task", form_task.add_product);
    $form_task.on("click", ".btn-remove-product_task", form_task.remove_product);

    $form_task.on("click", ".btn-add-product_task_gift", form_task.add_product_task_gift);
    $form_task.on("click", ".btn-remove-product_task_gift", form_task.remove_product_task_gift);

    $form_task.on("change", "[name*=product_id]", form_task.on_product_change);
    $form_task.on("change keyup", "[name*=quantity]", form_task.on_quantity_change);
    $form_task.on("change", "[type=checkbox][name*=gift]", form_task.on_gift_change);

    $form_task.on("change", "[name*=shipping_id]", form_task.on_shipping_change);

    $form_task.on("click", ".btn-status", form_task.on_status_open);
    $(document).on("click", ".btn-status-save_task", form_task.before_on_status_open);
    $(document).on("hidden.bs.modal", ".modal-status", form_task.on_status_dismiss);

    $form_task.on("change-vat", form_task.on_vat_change);

    $form_task.on("click", ".btn-email", form_task.on_message_click);
    $form_task.on("click", ".btn-phone", form_task.on_message_click);
    $(document).on("click", "#modal-message .btn-message-send", form_task.on_message_send);

    $form_task.on("change", "[name*=type_id], [name*=Order\\[general\\]], [name*=Order\\[phones\\]], [name*=customer_address]", order_task.update);

    if ($form_task.find("[data-gmap-autocomplete]").length == 1) {
        form_task.gmap_autocomplete = new google.maps.places.Autocomplete(
            $form_task.find("[data-gmap-autocomplete]").get(0),
            {types: ["geocode"]}
        );
        form_task.gmap_autocomplete.addListener('place_changed', form_task.on_gmap_autocomplete);
    }

    order_task.update();
    //$form_task.find(".product_task [name*=product_id]").first().trigger("change");

    if (typeof(wde_may_block_mark_done) === "function") {
        wde_may_block_mark_done();
    }

    // on autosize-plugin
    autosize(document.querySelectorAll('textarea.form-control'));

    //  is there post code?
    var post_code_block = '#order-address-zip';
    if ($form_task.find(post_code_block).length > 0) {
        if (typeof(post_code_mask) === "function") {
            var paramsMask = post_code_mask();
            $(post_code_block).mask(paramsMask)
        }
    }

    // name and phone are required
    /*
    $filedName = '#order-general-customer_first_name'
    $filedPhone = '#order-general-customer_phone'
    $($filedName + ', ' + $filedPhone).bind('keyup change', function () {
        if ($(this).val() == '') {
            $form_task.find('.field-' + $(this).attr('id')).removeClass('has-success').addClass('has-error');
            $form_task.errors = false;
        } else {
            $form_task.find('.field-' + $(this).attr('id')).removeClass('has-error');
            $form_task.errors = true;
        }
    });
    */

    // validate empty products
    $('.products_task').on('change', '.product-select select', function () {
        $currentId = $(this).attr('id')
        $parentBox = $('.product-select.field-' + $currentId)

        if ($('#' + $currentId).val() == '') {
            $parentBox.removeClass('has-success').addClass('has-error');
            $form_task.errors = true;
        } else {
            $parentBox.removeClass('has-error');
            $form_task.errors = false;
        }
    });

});

/**
 * for ajax reload history tasks
 * @param key
 */
function getOrderTasks() {
    setTimeout(function () {
        $.ajax({
            type: 'POST',
            url: '/order/order-task/get-order-tasks?order_id=' + $("#order-order_id").val(),
            data: $('form').serialize(),
            success: function (res) {
                //update tab task history
                $("#orderTaskHistoryTab").load(res);
            },
            error: function () {
                toastr.error('Произошла ошибка обновления данных. Обновите страницу');
            }
        })
    }, 1500);
}

function showComment(key) {
    $('div[data-cid="comment_' + key + '"]').removeClass('comment-flow');
}

function isCheckGiftById(id) {
    return $('#orderproduct-' + id + '-gift').prop("checked");
}

// switch on/off gift-function
function useCheckGift($object) {
    $product = $object.closest(".product_task")

    $product.find("[name*=price]").val(0);
    $product.find("[name*=cost]").val(0);
}

function useCheckGiftById(id) {
    var $object = $('#orderproduct-' + id + '-gift');
    useCheckGift($object)
}
// for zoiper call to phone number
function zoiperDial(id) {
    if (typeof(window.parent.Dial) === "function") {
        var number = $(id).val()
        if (number.length > 0) {
            window.parent.Dial(number)
        }
    }
}


//работа с select2 autocomplete в адресной форме - которые тянут данные из кладра
//по сути это зависимые списки, только без определённого порядка работы с ними

//включение/отключение автоматического режима - т.е. например - при выборе улицы - все верхнии уровни выставятся автоматически по структуре из КЛАДРа
use_task_auto = false;

$(window).load(function () {
    var list_select2Autocomplete = window.form_task.select2autocomplete;

    //завязка для google maps - поля с атрибюутом is_coordinates = true
    if(typeof  window.form_task.coordinatesFields != 'undefined'){
        for(var index in window.form_task.coordinatesFields){
            $("#form-order_task [name*="+window.form_task.coordinatesFields[index]+"]").on('keyup', function(){
                var address = getTaskAddress();
                address.push($(this).val());
                window.google_task.reDrawMap(address);
            });
        }
    }

    for (var k in list_select2Autocomplete) {
        var temp = list_select2Autocomplete[k].split(/_/);
        var key = temp[1];
        var attribute_id = temp[2];
        //при выборе в select2 значения - мы будем пробовать восстановить родительскую структуру (если select2 сам не является корнем всей структуры)
        //для этого добавем слушателей для них

        $("#"+list_select2Autocomplete[k]).on("select2:select", function (e) {
            var attribute_id = $(this).val();
            var value = $(this).find('option').last().text();

            createHiddenTaskInput(value, $(this));
            var address = getTaskAddress();

            window.google_task.reDrawMap(address);

            if (key > 0) {
                if (use_auto) {
                    //если в select2 было выбрано значение из результатов поиска - у него есть родители в КЛАДРе
                    if (list_select2Autocomplete[k] > 0) {
                        $($("#form-order fieldset#fieldset_address")).css('opacity', 0.3);
                        $($("#form-order fieldset#fieldset_address")).css('cursor', 'wait');
                        getTaskParentStructhure(attribute_id);
                    }
                }
            }
        });
    }

});

function createHiddenTaskInput(value, afterElem) {
    var hiddenInput = $('<input>').attr({
        type: 'hidden',
        id: afterElem.prop('id') + '_hidden',
        name: afterElem.prop('name'),
        value: value
    })

    hiddenInput.insertAfter('#' + afterElem.prop('id'));
}

function getTaskAddress(){
    console.log('starting getting addresses');
    var address = [];

    address.push($("#task_country").val());

    for (var k in window.form_task.select2autocomplete) {
        var select2 = $("#"+window.form_task.select2autocomplete[k]);

        if(select2.find('option').length > 1) {
            var value = $("#" + window.form_task.select2autocomplete[k]).find('option').last().text();
            if(value != "") {
                address.push(value);
            }
        }
    }
    return address;
}

//Функция определяет родителя и уровень вложенности выбранного зависимого select2
function getTaskParent(key) {
    if (key == 0) {
        return null;
    }
    else {
        $parentSelect2 = window.form_task.select2autocomplete[(key - 1)];

        return ($($parentSelect2).val() == '' || $($parentSelect2).val() == -1) ? null : {
                parent_id: $($parentSelect2).val(),
                depth: (key + 1)
            };
    }
}

//Функция фозращает структуру от выбранного элемента select2 до корневого уровня
//для автоподстановки адреса
function getTaskParentStructhure(id) {
    $.ajax({
        url: '/catalog/klader-countries/get-parent-structhure',
        type: 'POST',
        data: {
            child_id: id,
            _csrf: yii.getCsrfToken()
        },
        success: function (data) {
            var structhure = JSON.parse(data);

            for(var k in structhure){
                if(typeof window.form_task.select2autocomplete[k] != 'undefined' && k < structhure[k].depth){
                    //если начали заполнять не сначала - мы можем проставить парентов
                    if($(window.form_task.select2autocomplete[k]).val() <= 0) {
                        var selectOption = new Option(structhure[k].name, structhure[k].id, true, true);
                        $(window.form_task.select2autocomplete[k]).append(selectOption).trigger('change');
                    }
                }
            }

            $($("#form-order_task fieldset#fieldset_address")).css('opacity', 1);
            $($("#form-order_task fieldset#fieldset_address")).css('cursor', 'default');
        }
    });
}

//конец работы с select2autocomplete
$(window).load(function () {
    window.google_task.map = map_task_canvas;
    window.google_task.markers = [];

    var select2 = window.form_task.select2autocomplete;
    var coordinates = window.form_task.coordinatesFields;
    var address_data = [];
    var lat = $('#task_latitude').val();
    var lng = $('#task_longitude').val();

    address_data.push($("#task_country").val());

    for (var k in select2) {
        var val = $("#" + select2[k]).val();

        if (val != "") {
            address_data.push(val);
        }
    }

    for (var k in coordinates) {
        var val = $("#form-order_task [name*=" + coordinates[k] + "]").val();

        if (val != "") {
            address_data.push(val);
        }
    }

    var title = address_data.join(", ");

    if (lat != '' && lng != '') {
        setTimeout(function(){
            var position = new google.maps.LatLng(lat, lng);

            var marker = new google.maps.Marker({
                position: position,
                title: title,
                draggable: false
            });

            var infowindow = new google.maps.InfoWindow({
                content: '<p>' + title + '</p>'
            });

            marker.addListener('click', function () {
                infowindow.open(window.google_task.map, marker);
            });

            marker.setMap(window.google_task.map);

            window.google_task.address_data = address_data;
        },400);
    }

    window.google_task.reDrawMap(address_data);
});

function setTaskPosition(marker) {
    $('#task_latitude').val(marker.position.lat());
    $('#task_longitude').val(marker.position.lng());
}

if(typeof window.google_task == 'undefined'){
    window.google_task = {};
}
window.google_task.reDrawMap = function (address) {

    var geocoder = new google.maps.Geocoder();

    geocodeTaskAddress(geocoder, window.google_task.map, address);
}

function geocodeTaskAddress(geocoder, resultsMap, address) {
    var title = address.join(", ");
    var address = address.join(",+");

    geocoder.geocode({'address': address}, function (results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);


            if (typeof results[0].address_components != 'undefined') {

                for (var i = 0; i < results[0].address_components.length; i++) {
                    var types = results[0].address_components[i].types;

                    for (var typeIdx = 0; typeIdx < types.length; typeIdx++) {
                        if (types[typeIdx] == 'postal_code') {
                            //postal_code
                            $("#form-order_task #google_zip_task").val(results[0].address_components[i].short_name);
                            console.log('zip', results[0].address_components[i].short_name);
                        }
                    }
                }

                for (var k in window.google_task.markers) {
                    window.google_task.markers[k].setMap(null);
                }

                var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    title: title,
                });

                var infowindow = new google.maps.InfoWindow({
                    content: '<p>' + title + '</p>'
                });

                marker.addListener('click', function () {
                    infowindow.open(resultsMap, marker);
                });
                window.google_task.markers.push(marker);
                setTaskPosition(marker);

            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
                $('#task_latitude').val('');
                $('#task_longitude').val('');
            }
        }
    });
}
$(document).ready(function () {
    window.backup_update = false;

    try {
        console.log('localStorage', localStorage);
        console.group();

        var field_order = $("[name*=order_id]");
        var form_order = $("#form-order");
        var field_user = $("[name*=user_id]");

        if (field_user.val() == '') {
            field_user = $("#user_id", parent.document);
        }

        console.info('start backup order #' + field_order.val());

        $("input").on('keyup', function () {
            lsSet(getUserId(field_user), {
                order_id: field_order.val(),
                user_id: getUserId(field_user),
                pathname: location.pathname,
                url: location.href,
                data: form_order.serialize()
            });
        });

        $("input, textarea").on('blur', function () {
            sentToServer({
                order_id: field_order.val(),
                user_id: getUserId(field_user),
                pathname: location.pathname,
                url: location.href,
                data: form_order.serialize(),
                _csrf: yii.getCsrfToken()
            });
        });

        $("select").on('change', function () {
            lsSet(getUserId(field_user), {
                order_id: field_order.val(),
                user_id: getUserId(field_user),
                pathname: location.pathname,
                url: location.href,
                data: form_order.serialize()
            });

            sentToServer({
                order_id: field_order.val(),
                user_id: getUserId(field_user),
                pathname: location.pathname,
                url: location.href,
                data: form_order.serialize(),
                _csrf: yii.getCsrfToken()
            });
        });

        $(".btn-status").on('click', function () {
            console.info('sent to server backup order #' + field_order.val());
            sentToServer({
                order_id: field_order.val(),
                user_id: getUserId(field_user),
                pathname: location.pathname,
                url: location.href,
                data: form_order.serialize(),
                _csrf: yii.getCsrfToken()
            });
        });
    }
    catch(exception){
        console.info('backup is disabled');
        return false;
    }
});

function getUserId(elem) {
    return elem.prop('tagName') == 'DIV' ? elem.text() : elem.val();
}

function lsSet(key, value) {
    try {
        if (window.backup_update === false && typeof lsGet(key) == 'string') {
            var data = JSON.parse(lsGet(key));
            console.log('%c founded backup: ' + key + ' order_id: ' + data.order_id + ', user_id: ' + data.user_id + ' url: ' + data.url, 'background: pink; color: black; display: block;')

            var field_order = $("[name*=order_id]");
            var form_order = $("#form-order");
            var field_user = $("[name*=user_id]");

            if (field_user.val() == '' || typeof(field_user.val()) == 'undefined') {
                field_user = $("#user_id", parent.document);
            }

            sentToServer({
                order_id: field_order.val(),
                user_id: getUserId(field_user),
                pathname: location.pathname,
                url: location.href,
                data: form_order.serialize(),
                _csrf: yii.getCsrfToken()
            });
        }

        window.backup_update = true;
        localStorage.setItem(key, JSON.stringify(value));
        console.log('%c update item: ' + key, 'background: yellow; color: black; display: block;');
    }
    catch(Exeption){
        console.log('lsSet aborted');
    }
}

function lsGet(key) {
    return localStorage.getItem(key);
}

function lsRemove(user_id) {
    localStorage.removeItem(user_id);
}

function lsClear() {
    localStorage.clear();
    location.reload();
}

function sentToServer(orderData) {
    try {
        $.ajax({
            method: 'post',
            url: $("#url_backend", parent.document).text() + '/order/change/save-order-data-backup',
            datatype: 'html',
            data: orderData,
            success: function (data) {

                var field_user = $("[name*=user_id]");
                var field_order = $("[name*=order_id]");

                if (field_user.val() == '' || typeof(field_user.val()) == 'undefined') {
                    field_user = $("#user_id", parent.document);
                }

                var result = JSON.parse(data);
                if (result.success === true) {
                    console.info('%c remove from LS backup user #' + getUserId(field_user) + ' order #' + field_order.val(), 'background: green; color: white; display: block;');
                    lsRemove(getUserId(field_user))

                } else {
                    console.error('%c error to save backup user#' + getUserId(field_user) + ' order #' + $("[name*=order_id]").val() + ' from server', 'background: red; color: white; display: block;')
                    console.warn('result', result);
                }
                console.groupEnd();
            },
            error: function (data) {
                console.warn('result', data);
                console.error('%c error from server', 'background: red; color: white; display: block;');

            }
        });
    }
    catch(Exeption){
        console.log('sentToServer aborted');
    }
}

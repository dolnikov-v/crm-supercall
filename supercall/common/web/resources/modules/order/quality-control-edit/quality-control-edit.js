$('document').ready(function () {
    var form = $('#form-order');

    before_edit = form.serialize();

    function listener(event) {
        switch (event.data) {
            case 'save' :
                //вернуть статусы в окно аудите для сохранения
                var order_params = {
                    operator_status: $("[name*=operator_status]").val(),
                    auditor_status: $("[name*=auditor_status]").val(),
                    id: $('#form-order').data('id')
                }
                //save statuses audit
                toParent('order_params:' + JSON.stringify(order_params));
                toParent('order_params:' + JSON.stringify(order_params));

                var after_edit = form.serialize();

                var after = unserialize(after_edit);
                var before = unserialize(before_edit);
                
                //delete after['Order[auditor_status]'];
                //delete before['Order[auditor_status]'];

                var edit = JSON.stringify(after) != JSON.stringify(before);

                if (edit) {
                    toParent('is_edited');
                    form.fadeTo("slow", 0.5)
                    console.log('form opacity set 0.5')
                    form.submit();
                }
                else {
                    toParent('close');
                }
                break;
            case 'close' :
                console.log('close modal');
                break;
            case 'fade_in_after_validate' :
                form.fadeTo("fast", 1)
                break;
        }
        //получены записей разговоров
        if (/records/.test(event.data)) {
            var block_records = event.data.replace('records-', '');
            $('.place_records_quality_control').find('.records').html(block_records);

            //asset от виджета CallRecords подключим руками, т.к. виджет запрашивается по AJAX
            var domain = location.protocol + '//' + location.host.replace(/^api/, 'panel');
            var script = domain+'/resources/modules/order/audiojs/audio.min.js';

            $.getScript(script, function(){
                //подгрузили asset - инициализируем
                audiojs.events.ready(function() {
                    audiojs.createAll();

                    $('audio').map(function(){
                        var audio = $(this);
                        audio.on('loadedmetadata', function(){
                            audio[0].currentTime = audio.attr('currenttime');
                        });
                    });
                });
            });
        }
    }

    form.on('afterValidate', function (e) {
        if ($(this).find(".has-error").length) {
            $(this).scrollTop(0)
        } else {
            toParent('address-' + form.data('id') + '-' + $('#fieldset_address').find('select, input').serialize());
            toParent('close');
        }
        $(this).css('opacity', 1);
        toParent('validated');

    });

    if (window.addEventListener) {
        window.addEventListener("message", listener);
    } else {
        // IE8
        window.attachEvent("onmessage", listener);
    }

    //to child quality-control.js  listener()
    function toParent(command) {
        var parent = window.parent;
        parent.postMessage(command, '*');
    }

    toParent('is_load')

    //status new not needed
    $("[name*=auditor_status] option[value=1]").remove();
});

function unserialize(serializedString) {
    var str = decodeURI(serializedString).replace(/\+/g, ' ');
    var pairs = str.split('&');
    var obj = {}, p, idx, val;
    for (var i = 0, n = pairs.length; i < n; i++) {
        p = pairs[i].split('=');
        idx = p[0];

        if (idx.indexOf("[]") == (idx.length - 2)) {
            // Eh um vetor
            var ind = idx.substring(0, idx.length - 2)
            if (obj[ind] === undefined) {
                obj[ind] = [];
            }
            obj[ind].push(p[1]);
        }
        else {
            obj[idx] = p[1];
        }
    }
    return obj;
}
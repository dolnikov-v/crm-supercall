$(function(){
    var Recall = {
        checkOrderUrl: '/order/index/check-order',
        editUrlTpl: '/order/change/',
        buttons: 'a[data-editable="1"]',
        user_id: '#user_id',
        panelBody: '.panel-body',
        modal: '#recall-modal',
        modalText: '.recall-modal-text',
        init: function(){
            $(Recall.buttons).on('click', Recall.eventClick);
        },
        eventClick: function(){
            Recall.loadIn();

            $.when(Recall.checkOrder($(this).attr('data-order_id'))).done(function(data){
                if(data.success){
                    location.href = Recall.editUrlTpl + data.message;
                }else{
                    //toastr.error(data.message);
                    $(Recall.modalText).text(data.message);
                    $(Recall.modal).modal('show');
                }
            });

            return false;
        },
        checkOrder: function(order_id){
            return $.ajax({
                url: Recall.checkOrderUrl,
                type: 'post',
                dataType: 'json',
                data: {
                    order_id: order_id,
                    user_id: $(Recall.user_id).text()
                },
                success: function(data){
                    Recall.loadOut();
                },
                error: function(error){
                    Recall.loadOut();
                    toastr.error(error.responseText);
                }
            });
        },
        loadIn: function(){
            $(Recall.panelBody).css('opacity', 0.5);
        },
        loadOut: function(){
            $(Recall.panelBody).css('opacity', 1);
        }
    };

    Recall.init();
});
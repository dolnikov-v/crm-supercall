$(document).ready(function () {
    $('[name*=countries]').on('change', function () {
        if ($(this).val() != null) {
            $('[name*=operator]').prop('disabled', true);
            $.ajax({
                url: '/order/index/get-operators-by-country',
                method: 'post',
                data: {
                    country_id: $('[name*=countries]').val()
                },
                success: function (data) {
                    var result = JSON.parse(data);

                    if (result.data) {
                        var list = result.data;

                        addSelectedEmptyOption($('[name*=operator]'));

                        for (id in list) {
                            $('[name*=operator]').append(new Option(list[id], id));
                        }
                    } else {
                        toastr.error(result.message);
                    }

                    if (!$('#ordersearch-status_queue_strategy').prop('checked')) {
                        $('[name*=operator]').prop('disabled', false);
                    }

                    $('[name*=operator]').val(parseInt(parseUrl('OrderSearch[operator]')));
                    $('[name*=operator]').trigger('change');
                },
                error: function () {
                    $('[name*=operator]').prop('disabled', true);
                }
            });
        }
    });
});

function addSelectedEmptyOption(select) {
    select.empty();
    select.append(new Option('—', '')).val('');
    select.select2();
}

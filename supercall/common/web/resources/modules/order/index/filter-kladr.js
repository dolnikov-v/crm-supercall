$(function () {
    var FilterKladr = {
        get_type_url: '/catalog/klader-countries/get-types-address',
        get_value_url: '/catalog/klader-countries/get-values-address',
        country: "[name*=countries]",
        loader_type: "#loader_type",
        loader_value: "#loader_value",
        address_type: "[name*=address_type]",
        address_value: "[name*=address_value]",
        submit: "[type=submit]",
        getTypeQuery: null,
        getValueQuery: null,
        init: function () {
            var is_available = $(FilterKladr.country).val() && $(FilterKladr.country).val().length == 1;

            if (is_available) {
                FilterKladr.getTypes();
            } else {
                FilterKladr.setTypeDisabled();
            }

            if ($(FilterKladr.address_type).prop('disabled') || $(FilterKladr.address_type).val() == '') {
                FilterKladr.setValueDisabled();
            } else {
                FilterKladr.setValueEnabled();
            }
        },
        setTypeDisabled: function () {
            $(FilterKladr.address_type).val('');
            $(FilterKladr.address_type).trigger('change');
            $(FilterKladr.address_type).prop('disabled', true);
        },
        setTypeEnabled: function () {
            $(FilterKladr.address_type).prop('disabled', false);
        },
        setValueDisabled: function () {
            $(FilterKladr.address_value).val('');
            $(FilterKladr.address_value).trigger('change');
            $(FilterKladr.address_value).prop('disabled', true);
        },
        setValueEnabled: function () {
            $(FilterKladr.address_value).prop('disabled', false);
        },
        loadTypeStart: function () {
            $(FilterKladr.address_type).closest('div').addClass('hidden');
            $(FilterKladr.loader_type).css('margin-top', '-15px');
            $(FilterKladr.loader_type).removeClass('hidden');
        },
        loadTypeEnd: function () {
            $(FilterKladr.address_type).closest('div').removeClass('hidden');
            $(FilterKladr.loader_type).addClass('hidden');
        },
        loadValueStart: function () {
            $(FilterKladr.address_value).closest('div').addClass('hidden');
            $(FilterKladr.loader_value).css('margin-top', '-15px');
            $(FilterKladr.loader_value).removeClass('hidden');
        },
        loadValueEnd: function () {
            $(FilterKladr.address_value).closest('div').removeClass('hidden');
            $(FilterKladr.loader_value).addClass('hidden');
        },
        getTypes: function () {
            FilterKladr.loadTypeStart();
            FilterKladr.getTypeQuery = $.ajax({
                url: FilterKladr.get_type_url,
                dataType: 'json',
                type: 'post',
                data: {
                    country_id: $(FilterKladr.country).val()[0]
                },
                success: function (data) {
                    if (data.success) {
                        FilterKladr.setTypes(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                    FilterKladr.loadTypeEnd();
                },
                error: function (error) {
                    toastr.error(error.responseText);
                    FilterKladr.loadTypeEnd();
                }
            });
        },
        setTypes: function (types) {
            $(FilterKladr.address_type).find('option').remove();
            var option = new Option('—', '', true, true);
            $(FilterKladr.address_type).append(option);

            for (var k in types) {
                option = new Option(types[k].label, types[k].id, true, true);
                $(option).attr('data-name', types[k].name);
                $(FilterKladr.address_type).append(option);
            }
            FilterKladr.setTypeEnabled();

            var searchAddressType = FilterKladr.getParameterByName('OrderSearch[address_type]');

            $(FilterKladr.address_type).val(searchAddressType ? searchAddressType : '');
            $(FilterKladr.address_type).trigger('change');
        },
        getValues: function () {
            FilterKladr.loadValueStart();
            FilterKladr.getValueQuery = $.ajax({
                url: FilterKladr.get_value_url,
                dataType: 'json',
                type: 'post',
                data: {
                    partner_attribute_id: $(FilterKladr.address_type).val()
                },
                success: function (data) {
                    if (data.success) {
                        FilterKladr.setValues(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                    FilterKladr.loadValueEnd();
                },
                error: function (error) {
                    toastr.error(error.responseText);
                    FilterKladr.loadValueEnd();
                }
            });
        },
        setValues: function (values) {
            $(FilterKladr.address_value).find('option').remove();

            for (var k in values) {
                if (/[a-z]/gi.test(values[k])) {
                    option = new Option(values[k], k, true, true);
                    $(FilterKladr.address_value).append(option);
                }
            }

            var searchAddressValue = FilterKladr.getParametersByName('OrderSearch[address_value][]');

            $(FilterKladr.address_value).val(searchAddressValue);
            $(FilterKladr.address_value).trigger('change');
            FilterKladr.setValueEnabled();
        },
        getParameterByName: function (name) {
            var url = decodeURIComponent(window.location.href);
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)', 'g'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        },
        getParametersByName: function(name){
            var url = decodeURIComponent(window.location.href);
            var params = url.split(/&/);
            var values = [];
            for(var k in params){

                var data = params[k].split(/=/);

                if(data[0] === name){
                    if(typeof data[1] != 'undefined') {
                        values.push(decodeURIComponent(data[1].replace(/\+/g, ' ')));
                    }
                }

            }

            return values;
        }
    };

    $(FilterKladr.country).on('change', function () {
        FilterKladr.init();

        if ($(this).val() == null || $(this).val() == '') {
            FilterKladr.setTypeDisabled();
            FilterKladr.setValueDisabled();
        }
    });

    $(FilterKladr.address_type).on('change', function () {
        if ($(this).val() != '' && $(this).val() != null) {
            FilterKladr.getValues();
        } else {
            FilterKladr.setValueDisabled();
        }
    });
});
$(document).ready(function () {
    try {
        const MASK = 'XXXX';

        var MaskManager = {
            masked_data : [],
            masked_inputs: $('[data-ismask="true"]'),
            createDuplicateInputs: function (input) {
                var duplicate = $('<input>', {
                    'name': input.prop('name'),
                    'value': input.data('content')
                });

                duplicate.addClass('hidden');

                //for zoiper
                this.addBefore(duplicate, input);
                //for save form on server
                this.addAfter(duplicate.clone(), input);
            },
            addBefore: function (elem, target) {
                $(target).before(elem);
            },
            addAfter: function (elem, target) {
                $(target).after(elem);
            },
            setData : function(elem){
                var value = $(elem).val();

                var data = {
                    name: $(elem).prop('name'),
                    content : $(elem).data('content'),
                    value: value,
                    //оператору доступно сменить только первые 3 цифры или дописать в начало номера
                    tpl  : value.replace(MASK, '').slice(-3),
                    size : value.replace(MASK, '').slice(-3).length
                };

                MaskManager.masked_data.push(data);
            },
            checkModified : function(elem){
                for(var k in MaskManager.masked_data){
                    if($(elem).prop('name') == MaskManager.masked_data[k].name){
                        //проверка. оператор может дописать в начало номера или сменить максимум первые 2 цифры номера
                        var non_replace = $(elem).val().replace(MASK, '').slice(0-MaskManager.masked_data[k].size);
                        var non_replace_size = non_replace.length;

                        console.log('edit: ', non_replace, non_replace_size);
                        console.log('saved: ', MaskManager.masked_data[k].tpl, MaskManager.masked_data[k].size);

                        var diff = non_replace == MaskManager.masked_data[k].tpl;

                        if(!diff){
                            toastr.error('It is not permissible to replace the hidden digits of a number or change the number except the first 2 numbers');

                            var target = $("[name='"+MaskManager.masked_data[k].name+"']");

                            target.val(MaskManager.masked_data[k].value);
                            target.data('content', MaskManager.masked_data[k].content);
                            target.trigger('keyup');

                            return false;
                        }

                    }
                }
                return true;
            },
            init: function () {

                this.masked_inputs.map(function () {
                    MaskManager.createDuplicateInputs($(this));

                    if($(this).val() != '') {
                        MaskManager.setData($(this));
                    }

                    $(this).on('keyup', function () {
                        if ($(this).data('ismask') == true) {

                            if(!MaskManager.checkModified($(this))){
                                return;
                            }

                            var content = $(this).data('content');
                            var name = $(this).prop('name');
                            var fix_inputs = $('[name="' + name + '"]').not('[data-ismask="true"]');

                            fix_inputs.val($(this).val().replace(MASK, '') + content.toString().slice(-4));
                            fix_inputs.trigger('change');
                        }
                    });
                });
            }
        }

        MaskManager.init();

    } catch (e) {
        JSLogger({
            script: 'scripts-operators.js',
            order_id: $("[name*=order_id]").val(),
            user_id: $("[name*=user_id]").val(),
        }, e);
    }
});
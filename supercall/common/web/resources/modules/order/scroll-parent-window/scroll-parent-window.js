$(document).ready(function(){
    $('.btn-status').on('click', function(){
        //window.parent.scrollTo(0,0);
        window.scrollTo(0,0);
    });

    //закрыть модальные окна - если форма не прошла валидацию
    $('#form-order').on('afterValidate', function (e) {
        if($("#form-order .has-error").length > 0) {
            $('#modal-status-4').modal('hide');
            window.scrollTo(0,0);
        }
    });
});

$(window).load(function () {
    if(typeof map_canvas == 'undefined'){
        return true;
    }
    window.google.map = map_canvas;
    window.google.markers = [];

    var select2 = window.form.select2autocomplete;
    var coordinates = window.form.coordinatesFields;

    var address_data = [];
    var lat = $('#order_latitude').val();
    var lng = $('#order_longitude').val();

    address_data.push($("#form-order [name*=country_name]").val());

    for (var k in select2) {
        var val = $("#" + select2[k]).val();

        if (val != "") {
            address_data.push(val);
        }
    }

    for (var k in coordinates) {
        var val = $("#form-order [name*=" + coordinates[k] + "]").val();

        if (val != "") {
            address_data.push(val);
        }
    }

    var title = address_data.join(", ");

    if (lat != '' && lng != '') {
        setTimeout(function () {
            var position = new google.maps.LatLng(lat, lng);

            var marker = new google.maps.Marker({
                position: position,
                title: title,
                draggable: false
            });

            var infowindow = new google.maps.InfoWindow({
                content: '<p>' + title + '</p>'
            });

            marker.addListener('click', function () {
                infowindow.open(window.google.map, marker);
            });

            marker.setMap(window.google.map);

        }, 400);
    }

    window.google.reDrawMap(address_data);

    /** Операторам пока отключена возможность корректировать маркер
     *  + функционал не дописан (не заполняются поля при установки/смены позиции маркера)
     markers = [];

     window.google.map.addListener('click', function(e) {
        //remove old
        window.google.markers[0].setMap(null);

        for(var k in markers){
            markers[k].setMap(null);
        }

        //add new
        var position = e.latLng;

        var marker = new google.maps.Marker({
            position: position,
            title:'',
            draggable: false
        });

        getPosition(marker);

        markers.push(marker);

        marker.setMap(window.google.map);

        //window.google.map.setCenter(position);
    });
     */

    //автоматическое определение координат
    //setPosition(marker);

});


function setPosition(marker) {
    if(typeof map_canvas == 'undefined'){
        return true;
    }
    $('#order_latitude').val(marker.position.lat());
    $('#order_longitude').val(marker.position.lng());
}

if (typeof window.google == 'undefined') {
    window.google = {};
}

window.google.reDrawMap = function (address) {
    if(typeof map_canvas == 'undefined'){
        return true;
    }
    var geocoder = new google.maps.Geocoder();
    geocodeAddress(geocoder, window.google.map, address);
}

function geocodeAddress(geocoder, resultsMap, address) {
    if(typeof map_canvas == 'undefined'){
        return true;
    }
    var title = address.join(", ");
    var address = address.join(",+");

    geocoder.geocode({'address': address}, function (results, status) {
        if (status === 'OK') {
            window.geocoderData = {
                results: results[0].geometry.location,
                resultsMap: resultsMap,
                title: title
            };

            setCenterAndMarker(results[0], resultsMap, title);

        } else {
            if(typeof window.geocoderData != 'undefined') {
                console.log('Geocode was not successful for the following reason: ' + status);
                if (typeof window.geocoderData.results == 'object') {
                    setCenterAndMarker(window.geocoderData.results, window.geocoderData.resultsMap, window.geocoderData.title);
                }
                $('#order_latitude').val('');
                $('#order_longitude').val('');
            }
        }
    });
}

function setCenterAndMarker(location, resultsMap, title) {
    if(typeof map_canvas == 'undefined'){
        return true;
    }

    if (typeof location == 'undefined') {
        return;
    }

    if(typeof location.geometry != 'undefined') {
        resultsMap.setCenter(location.geometry.location);
    }

    if(typeof location.address_components != 'undefined'){
        for (var i = 0; i < location.address_components.length; i++) {
            var types = location.address_components[i].types;

            for (var typeIdx = 0; typeIdx < types.length; typeIdx++) {
                if (types[typeIdx] == 'postal_code') {
                    //postal_code
                    $("#form-order #google_zip").val(location.address_components[i].short_name);
                    console.log('zip', location.address_components[i].short_name);
                }
            }
        }

        for (var k in window.google.markers) {
            window.google.markers[k].setMap(null);
        }

        var marker = new google.maps.Marker({
            map: resultsMap,
            position: location.geometry.location,
            title: title,
        });

        window.google.markers.push(marker);

        var infowindow = new google.maps.InfoWindow({
            content: '<p>' + title + '</p>'
        });

        marker.addListener('click', function () {
            infowindow.open(resultsMap, marker);
        });

        setPosition(marker);
    }
}
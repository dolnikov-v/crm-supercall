var SearchOrderForm = {
    url_search: '/order/change/get-search-form-result',
    url_open_modal: '/order/change/lock-order',
    url_close_modal: '/order/change/unlock-order',
    form: '.order-search-form',
    modalForm: '#modal_order_form',
    modalFormBody: '#modal_order_form_body',
    modalEndWork: '#modal_end_work_with_search_form',
    btn_end_work: '#button_end_work_with_search_form',
    btn_modal_no: '#modal_end_work_with_search_form_no',
    btn_modal_yes: '#modal_end_work_with_search_form_yes',
    order_number: '.order-number',
    order_search_content: '.order-search-content',
    locked: false,
    callInProgress: false,
    init: function () {
        SearchOrderForm.callInProgress = true;
        $(SearchOrderForm.form).find('input').on('keyup', SearchOrderForm.search);
        $(SearchOrderForm.modalForm).on('hide.bs.modal', SearchOrderForm.closeModalForm);
        $(SearchOrderForm.btn_modal_no).on('click', SearchOrderForm.closeEndWorkForm);
        $(SearchOrderForm.btn_modal_yes).on('click', SearchOrderForm.endWorkWithSearchForm);
        $(SearchOrderForm.btn_end_work).on('click', SearchOrderForm.showEndWorkWithSearchForm);

        if (window.addEventListener) {
            window.addEventListener("message", SearchOrderForm.eventListener);
        } else {
            // IE8
            window.attachEvent("onmessage", SearchOrderForm.eventListener);
        }

        SearchOrderForm.initLinks();
    },
    initLinks: function () {
        $(SearchOrderForm.order_number).find('a').on('click', SearchOrderForm.openModalForm);
    },
    search: function (e) {
        e.stopImmediatePropagation();

        $.ajax({
            type: 'POST',
            url: SearchOrderForm.url_search,
            data: $(SearchOrderForm.form).serialize(),
            dataType: 'json',
            cache: false,
            success: function (response) {
                if (response.status === 'success') {
                    $(SearchOrderForm.order_search_content).replaceWith(response.content);
                    SearchOrderForm.initLinks();
                }
            }
        });
    },
    openModalForm: function (e) {
        e.stopImmediatePropagation();

        SearchOrderForm.locked = false;
        $.ajax({
            type: 'GET',
            url: SearchOrderForm.url_open_modal,
            data: {'id': $(this).data('order-id'), 'lock_time': 1800},
            success: function () {
                SearchOrderForm.locked = true;
            }
        });

        $(SearchOrderForm.modalFormBody).find('iframe').attr('src', $(this).data('iframe-url'));
        $(SearchOrderForm.modalForm).data('order-id', $(this).data('order-id'));
        $(SearchOrderForm.modalForm).modal('show');
    },
    closeModalForm: function (e) {
        e.stopImmediatePropagation();

        $(SearchOrderForm.modalFormBody).find('iframe').remove();
        $(SearchOrderForm.modalFormBody).append('<iframe>');
        $(SearchOrderForm.modalFormBody).find('iframe').attr('src', '');
        if (SearchOrderForm.locked) {
            SearchOrderForm.locked = false;
            $.ajax({
                type: 'GET',
                url: SearchOrderForm.url_close_modal,
                data: {'id': $(this).data('order-id'), 'lock_time': 1800}
            });
        }
    },
    eventListener: function (event) {
        //form_order_status_save - ajax валидация
        //form_order_on_save - submit формы
        //form-order-loaded - форма загружена
        console.log('EVENT', event.data);

        switch (event.data) {
            case 'form_order_on_save':
                $(SearchOrderForm.modalForm).modal('hide');

                if (!SearchOrderForm.callInProgress) {
                    SearchOrderForm.showEndWorkWithSearchForm();
                }

                break;
        }
    },
    closeEndWorkForm: function (e) {
        e.stopImmediatePropagation();

        $(SearchOrderForm.modalEndWork).modal('hide');
    },
    showEndWorkWithSearchForm: function () {
        $(SearchOrderForm.modalEndWork).modal('show');
    },
    endWorkWithSearchForm: function (e) {
        e.stopImmediatePropagation();

        SearchOrderForm.closeEndWorkForm(e);
        SearchOrderForm.callInProgress = false;

        if (window.param_search_form == 1) {
            $.ajax({
                type: 'GET',
                url: '/order/change/continue-operator-work',
                success: function () {
                    window.param_search_form = 0;
                    if (typeof(window.parent.frontend_set_callback_param_get_order) === "function") {
                        window.parent.frontend_set_callback_param_get_order();
                    }
                    window.internalOrderOnline();
                }
            });
        }
    },
    callWasEnd: function () {
        SearchOrderForm.callInProgress = false;
        $(SearchOrderForm.btn_end_work).prop('disabled', false);

        if (typeof $(SearchOrderForm.modalForm).data('bs.modal') == 'undefined' || !$(SearchOrderForm.modalForm).data('bs.modal').isShown) {
            SearchOrderForm.showEndWorkWithSearchForm();
        }
    }
};
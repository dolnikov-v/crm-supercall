try {

    $(document).ready(function () {
        $('.products .product').trigger('change');
        $('.products [name*=quantity]').trigger('change');

        $('.close-modal-status').on('click.addStatus', function () {
            toParent('close_modal_status: ' + $(this).closest('.modal-status').attr('data-order_id'));
        })
    });

    //Стоимость доставки - тарифы.
    window.use_price_shipping = true;

    document.domain = $("#document_domain").text();

    $(document).ready(function () {

        // if (typeof parent.KVS !== 'undefined') {
        //     $('.btn-status-save-cancel').on('click', function () {
        //         //parent.KVS.set(parent.KVS.ACTION_ORDER, parseInt($(this).attr('data-order_id')));
        //     });
        // }

        toParent('form-order-loaded');
        window.$form = $("#form-order");
        defaultSP = $('.default-shipping-price input').val();
        $_productsPriceData = $('#products-price-data').data('products-price');

        //Акции
        $_packages_data = $('#order-package').data('order-package');

        //Стоимость за единицу
        $_product_price = $('#order-package').data('order-product_price');

        //необходимо сохранить предедущее состояние селектов package_id - чтобы разлочивать подарки по данном акциям
        window.packages = [];
        $('.products [name*=package_id]').map(function () {
            $(this).on('focus', function () {
                window.packages[$(this).prop('id')] = $(this).val();
            });
        });

        window.order = {
            editable: null,
            id_param: null,
            id: null,
            partner_id: null,
            country: null,
            status: null,

            type: null,
            general: {},
            phones: [],
            products: [],
            shipping_id: null,
            customer_address: null,

            total_quantity: 0,
            total_price: 0,
            total_final: 0,
            total_final_no_vat: 0,
            shipping_price: 0,
            shipping_vat: null,

            update: function () {
                order.editable = $form.data("editable");
                order.va = $form.data("editable");
                order.id_param = $form.data("id_param");
                order.id = $form.data("id");
                order.partner_id = $form.data("partner_id");
                order.country_id = $form.data("country_id");
                order.country = $form.data("country");
                order.status = $("#order-status").val();
                order.vat_list = $form.data("vat_list");
                // default shippingPrice, if direct api request
                var defaultShippingPrice = $('.default-shipping-price input').val();

                order.type = $form.find("[name*=type_id]").val();

                order.general = {};
                $form.find("[name*=Order\\[general\\]]").each(function () {
                    var name = $(this).attr("name").slice("Order[general]".length + 1, -1);
                    order.general[name] = $(this).val();
                });


                order.phones = [];
                $form.find("[name*=Order\\[phones\\]]").each(function () {
                    order.phones.push($(this).val());
                });

                order.shipping_id = $form.find("select[name*=shipping_id]").val();
                order.customer_address = $form.find("[name*=customer_address]").val();


                order.total_quantity = 0;
                order.total_price = 0;
                order.total_final = 0;
                order.total_final_no_vat = 0;
                order.shipping_price = 0;

                order.products = [];
                $form.find(".product").each(function () {
                    var $this = $(this);

                    var product = {
                        product_id: $this.find("[name*=product_id]").val(),
                        product_name: $this.find("[name*=product_id] option:selected").text(),
                        quantity: $this.find("[name*=quantity]").val(),
                        price: $this.find("[name*=price]").val(),
                        cost: $this.find("[name*=cost]").val(),
                        gift: $this.find("[type=checkbox][name*=gift]").prop("checked")
                    };
                    //после мерджа с nsk_dev - заваливаться стал на order.vat_list
                    if (typeof order.vat_list != 'undefined' && order.vat_list.length > 0) {
                        if (order.vat_list[order.country_id][product.product_id]) {
                            product.vat = order.vat_list[order.country_id][product.product_id]
                        } else if (order.vat_list[order.country_id]) {
                            product.vat = order.vat_list[order.country_id]['main']
                        } else {
                            product.vat = 0;
                        }
                    }

                    order.products.push(product);

                    var quantity = parseInt(product.quantity);
                    if (isNaN(quantity)) {
                        return;
                    }

                    var price = parseFloat(product.price);
                    if (isNaN(price)) {
                        return;
                    }

                    var cost = parseFloat(product.cost);
                    if (isNaN(cost)) {
                        return;
                    }

                    var vat = parseFloat(product.vat);
                    if (isNaN(vat)) {
                        vat = parseFloat($form.data("vat"));
                        if (isNaN(vat)) {
                            vat = 0.0;
                        }
                    }

                    //Промо продукты - не зависимый функционал. Необходим пересчет цен и кол-ва
                    var reQuantity = 0;
                    var rePrice = 0;
                    var withOutPromoPrice = 0;

                    $('.product').find('[name*=quantity]').each(function () {
                        reQuantity += parseInt($(this).val());
                    });

                    var promoQuantity = $('#promo-products').find('[name="OrderProduct[promoProduct][quantity][]"]').val();

                    if (promoQuantity > 0) {
                        reQuantity += parseInt(promoQuantity);
                    }

                    $('.product').find('[name*=cost]').each(function () {
                        rePrice += parseFloat($(this).val() == '' ? 0 : $(this).val());
                        withOutPromoPrice += parseFloat($(this).val() == '' ? 0 : $(this).val());

                    });

                    var promoPrice = $('#promo-products').find('[name="OrderProduct[promoProduct][price][]"]').val();

                    if (promoPrice > 0) {
                        rePrice += parseFloat(promoPrice);
                    }

                    if (rePrice === 0) {
                        reQuantity = 0;
                    }

                    order.total_quantity = reQuantity;
                    order.total_price = rePrice;
                    order.total_final_no_vat = rePrice;
                    order.total_final = rePrice + (withOutPromoPrice * vat / 100);
                });

                var extra_price = parseFloat($form.data("extra-price"));
                if (isNaN(extra_price)) {
                    extra_price = 0;
                }
                order.total_final += extra_price;
                order.total_final_no_vat += extra_price;

                var dynamic_shipping_price = $form.find("select[name*=shipping_id] option:selected").data("dynamic_price");
                if (dynamic_shipping_price) {
                    if (typeof(calc_shipping) === "function") {
                        order.shipping_price = parseInt(calc_shipping(order));
                        if (isNaN(order.shipping_price)) {
                            throw "Некорректная функция расчета стоимости доставки";
                        }
                    } else {
                        throw "Не определена функция расчета стоимости доставки";
                    }

                    if (typeof(calc_shipping_vat) === "function") {
                        order.shipping_vat = parseFloat(calc_shipping_vat(order));
                        if (isNaN(order.shipping_vat)) {
                            throw "Некорректная функция расчета НДС стоимости доставки";
                        }
                    } else {
                        order.shipping_vat = parseFloat($form.data("vat"));
                        if (isNaN(order.shipping_vat)) {
                            order.shipping_vat = 0.0;
                        }
                    }
                }
                else {
                    order.shipping_price = parseFloat($form.find("select[name*=shipping_id] option:selected").data("price"));
                    if (isNaN(order.shipping_price)) {
                        // check find shipping_price in the input by direct api-request
                        if (defaultSP)
                            order.shipping_price = Number(defaultSP);
                        else
                            order.shipping_price = 0.0;
                    }
                    order.shipping_vat = parseFloat($form.data("vat"));
                    if (isNaN(order.shipping_vat)) {
                        order.shipping_vat = 0.0;
                    }

                }

                //Дополнительная цена из конструктора
                order = checkAdditionalPrice(order);

                order.total_final_no_vat += order.shipping_price;
                order.total_final += order.shipping_price + (order.shipping_price * order.shipping_vat / 100);

                $form.find(".total-quantity").text(order.total_quantity);
                $form.find(".total-price").text(order.total_price.toFixed(2));
                $form.find(".total-final").text(order.total_final.toFixed(2));
                $form.find(".total-final-no-vat").text(order.total_final_no_vat.toFixed(2));

                /** стоимость промо товаров */
                var promoSum = parseFloat($('[name*=promoProductSum]').val());

                if (isNaN(promoSum)) {
                    promoSum = 0;
                }

                order.total_final = parseFloat(order.total_final + promoSum);

                $form.find("[name*=init_price]").val(order.total_price);
                $form.find("[name*=final_price]").val(order.total_final);
                $form.find("[name*=shipping_price]").val(order.shipping_price);
            }
        };

        window.form = {
            add_product: function () {

                if (!order.editable) {
                    return;
                }

                var $products = $(this).closest("fieldset").find(".products");
                var index = 0;

                while ($("#product-" + index).length == 1) {
                    Date.now() + index++;
                }

                var $products = $(this).closest("fieldset").find(".products");
                var $productForClone = $products.find('.product').first();
                var originalProductId = $($productForClone).attr('id');

                var originalIdArray = originalProductId.split('-');
                var originalId = originalIdArray[1]

                // replace productId
                var htmlRow = $($productForClone).html()
                htmlRow = htmlRow.replace(new RegExp(originalProductId, "igm"), 'product-' + index);

                // replace from OrderProduct[originalId] to OrderProduct[index]
                var orderProductIdReplace = 'OrderProduct\\[' + originalId + '\\]';
                var orderProductIdNew = 'OrderProduct\[' + index + '\]'

                htmlRow = htmlRow.replace(new RegExp(orderProductIdReplace, "igm"), orderProductIdNew);

                $('<div class="product row form-group" id="product-' + index + '">' + htmlRow + '</div>').appendTo(".products");
                var $newRow = $(".products").find('#orderproduct-' + index + '-id');
                $newRow.removeAttr('value')

                //set empty
                $("#orderproduct-" + index + "-price").val('');
                $("#orderproduct-" + index + "-package").val('');
                $("#orderproduct-" + index + "-cost").val('');

                //Акции
                setFilterPackagesByProduct($(this));

                var product_row = $('.products').find('.product').last();
                var product = product_row.find('[name*=product_id]');
                var quantity = product_row.find('[name*=quantity]');
                var package = product_row.find('[name*=package_id]');

                setReadOnly(product, false);
                product.val('');
                setReadOnly(quantity, false);
                quantity.val(1);
                package.empty();
                package.append(new Option('-', '')).val('');
                package.val('');

                window.form.receive_shipping_list_from_partner();

            },
            add_product_gift: function () {
                if (!order.editable) {
                    return;
                }

                var index = 0;

                while ($("#product-" + index).length == 1) {
                    Date.now() + 100 * (index++);
                }
                var $products = $(this).closest("fieldset").find(".products-gift")
                var $productForClone = $products.find('.product-gift').first()
                var originalProductId = $($productForClone).attr('id')

                var originalIdArray = originalProductId.split('-')
                var originalId = originalIdArray[1]

                // replace productId
                var htmlRow = $($productForClone).html();

                htmlRow = htmlRow.replace(new RegExp(originalProductId, "igm"), 'product-' + index);

                // replace from OrderProduct[originalId] to OrderProduct[index]
                var orderProductIdReplace = 'OrderProduct\\[' + originalId + '\\]'
                var orderProductIdNew = 'OrderProduct\[' + index + '\]'
                htmlRow = htmlRow.replace(new RegExp(orderProductIdReplace, "igm"), orderProductIdNew)

                $('<div class="product-gift row form-group" id="product-' + index + '">' + htmlRow + '</div>').appendTo(".products-gift");
                var $newRow = $(".products-gift").find('#orderproduct-' + index + '-id')

                $newRow.removeAttr('value')

                //set empty
                var product = $("#orderproduct-" + index + "-product_id");
                product.val('');

                $("#orderproduct-" + index + "-price").val('');

                //акции
                var gifts_row = $('.products-gift').find('.product-gift').last();
                var product_gift = gifts_row.find('[name*=product_id]');
                var quantity_gift = gifts_row.find('[name*=quantity]');
                var package_gift = gifts_row.find('[name*=package_id]');

                setReadOnly(product_gift, false);
                product_gift.val('');

                setReadOnly(quantity_gift, false);
                quantity_gift.val(1);

                package_gift.val('');

                gifts_row.find('.gift-icon').html();

                window.form.receive_shipping_list_from_partner();
            },
            remove_product: function () {
                if (!order.editable) {
                    return;
                }

                //удаление подарков вместе с акциями
                if ($(".products-gift .product-gift").length == 1) {
                    //акции
                    var group_gifts = $('.products-gift').find('.product-gift').first();

                    var gift_product = group_gifts.find('[name*=product_id]');
                    var gift_quantity = group_gifts.find('[name*=quantity]');
                    var package = group_gifts.find('[name*=package_id]');

                    setReadOnly(gift_product, false);
                    gift_product.val('');

                    setReadOnly(gift_quantity, false);
                    gift_quantity.val(1);

                    package.val('');

                    group_gifts.find('.gift-icon span').remove();

                    group_gifts.attr('identity', '');
                }

                if ($(".products .product").length > 1) {
                    $(this).closest(".product").remove();
                    //акции
                    if (typeof $(this).closest(".product").attr('identity') != 'undefined') {
                        var identity = $(this).closest(".product").attr('identity').replace('product_', '');

                        if (typeof identity != 'undefined') {
                            $('.products-gift').find('.product-gift').map(function () {
                                if ($(this).attr('identity') == 'gift_' + identity) {
                                    $(this).closest('.product-gift').remove();
                                }
                            });
                        }
                    }
                    //end
                    order.update();
                }
                window.form.receive_shipping_list_from_partner();
            },
            remove_product_gift: function () {
                if (!order.editable) {
                    return;
                }
                if ($(".products-gift .product-gift").length > 1) {
                    $(this).closest(".product-gift").remove();
                    order.update();
                }

                window.form.receive_shipping_list_from_partner();
            },
            on_country_change: function () {
                $form.data("country_id", $(this).val());
                order.update();
            },
            //мониторим остатки на складе по данному товару
            set_stock: function (product_id, $amount, quantity) {
                //при редактировании цены - не менять сток
                if (!isUndefined($($amount).attr('data-change_price'))) {
                    return;
                }
                //для новых заказов
                if (typeof $_productsPriceData[product_id] == 'undefined') {
                    return;
                }

                if (quantity === null) {
                    var $quantity = $amount.parent().closest(".product").find("[name*=quantity]");
                    var quantity = $quantity.val();

                    //$_productsPriceData[product_id].amount = $_productsPriceData[product_id][quantity].amount-quantity;
                }

                if (typeof $_productsPriceData[product_id].amount == 'undefined') {
                    $_productsPriceData[product_id].amount = $_productsPriceData[product_id][quantity].amount - parseInt(quantity);
                }

                //посчитать выбранное quantity по данному товару в заказе
                //этот функционал только на клиенте - для удобства работы оператора
                //фактическое "списание" - во время сохранения заказа - средствами и данными доступными уже в Yii
                var quantities = $("#form-order [name*=quantity]");
                var sum_quantity = 0;

                quantities.map(function () {
                    var id = $(this).prop('id');
                    //если заказ новый, то orderProduct не существует, поэтому product_id вытащить на сервере для формы не получится
                    //сформируем правильный id для селекта товаров
                    if ($(this).val() != '') {
                        if (/select_*/.test($(this).prop('id')) === false) {
                            $(this).prop('id', 'select_' + product_id);
                        }
                    }

                    var product_id_by_is_select = id.replace('select_', '');

                    if (product_id_by_is_select == product_id) {
                        if (!isNaN(parseInt($(this).val()))) {
                            sum_quantity += parseInt($(this).val());

                        }
                    }
                });

                //остаток "на складе" после указания кол-ва данного товара
                var in_stock = $_productsPriceData[product_id].amount - sum_quantity
                var value = 0;

                if (product_id in $_productsPriceData) {
                    value = in_stock <= 0 ? 0 : in_stock;
                }

                var amountes = $("#form-order [name*=amount]");
                amountes.map(function () {
                    if ($(this).prop('id').replace('amount_', '') == product_id) {
                        $(this).val('(' + value + ')');
                        $(this).css('color', (value > 0 ? 'green' : 'red'));
                    }
                });
            },
            on_product_change: function () {
                if ((/promo/).test($(this).prop('name'))) {
                    return;
                }

                var $id = $(this).closest(".product").find("[name*=id]");
                var $price = $(this).closest(".product").find("[name*=price]");
                var $cost = $(this).closest(".product").find("[name*=cost]");
                var $vat = $(this).closest(".product").find("[name*=vat]");
                var $quantity = $(this).closest(".product").find("[name*=quantity]");
                var $amount = $(this).closest(".product").find("[name*=amount ]");
                //акции
                var package = $(this).closest(".product").find("[name*=package]");

                var quantity = parseInt($quantity.val());
                if (isNaN(quantity)) {
                    quantity = 0;
                }

                $amount.prop('id', 'amount_' + $(this).val());

                //для товаров-подарков дальнейшая логика не нужна
                if (typeof $id.attr('id') == 'undefined') {
                    window.form.receive_shipping_list_from_partner();
                    return;
                }
                // получим index
                var indexArray = $id.attr('id').split('-')
                var index = indexArray[1]

                if (isCheckGiftById(index) == false) {
                    // get prices from Product
                    var product_id = this.value

                    $quantity.prop('id', 'select_' + product_id);

                    if ((product_id in $_productsPriceData) && (quantity in $_productsPriceData[product_id])) {
                        response = $_productsPriceData[product_id][quantity];

                        if (response == null) {
                            response = {};
                            response.price = 0;
                        }

                        $price.val(response.price);

                        var cost;
                        if (typeof(calc_price) === "function") {
                            cost = calc_price(response.price, quantity);
                        }
                        else {
                            cost = quantity * response.price;
                        }
                        $cost.val(cost);

                        $vat.val(response.vat).trigger("change-vat");
                        order.update();
                    }

                }
                if (typeof $amount == 'undefined') {
                    return;
                }

                window.form.set_stock(product_id, $amount, null);

                //акции фильтрация акций по продукту
                setFilterPackagesByProduct($(this));

                window.form.receive_shipping_list_from_partner();
            },
            on_quantity_change: function () {
                var $id = $(this).closest(".product").find("[name*=id]");
                var $price = $(this).closest(".product").find("[name*=price]");
                var $cost = $(this).closest(".product").find("[name*=cost]");
                var product_id = parseInt($(this).closest(".product").find("[name*=product_id]").val());
                var $amount = $(this).closest(".product").find("[name*=amount]");

                var quantity = this.value;
                if (isNaN(product_id)) {
                    return;
                }

                // получим index
                var indexArray = $id.attr('id').split('-')
                var index = indexArray[1]

                var product_price = $(':selected', $(this).closest(".product").find("[name*=package_id]")).data('product_price');

                if (isCheckGiftById(index) == false) {
                    // get prices from Product
                    response = $_productsPriceData[product_id][quantity]

                    if (!product_price) {
                        product_price = response.price;
                    }

                    if (response == null) {
                        return;
                    }

                    $price.val(product_price);

                    var cost;
                    if (typeof(calc_price) === "function") {
                        cost = calc_price(product_price, quantity);
                    }
                    else {
                        cost = quantity * product_price;
                    }
                    $cost.val(cost);
                    order.update();
                }

                window.form.set_stock(product_id, $amount, quantity);
                window.form.receive_shipping_list_from_partner();
            },
            on_vat_change: function (e) {
                var $this = $(e.target);
                if (parseInt($this.val())) {
                    $this.removeClass("hidden");
                }
                else {
                    $this.addClass("hidden");
                }
            },
            on_gift_change: function (e) {
                var $this = $(e.target);
                $product = $this.closest(".product")

                if ($this.prop("checked")) {
                    useCheckGift($this)
                    order.update();
                }
                else {
                    $product.find("[name*=quantity]").trigger("change");
                    $product.find("[name*=product_id]").trigger("change");
                }

                window.form.receive_shipping_list_from_partner();
            },
            add_phone: function () {
                if (!order.editable) {
                    return;
                }

                if ($(".order-phones .order-phone").length < 3) {
                    $(".order-phones").append('<div class="form-group input-group order-phone">' +
                        '<input class="form-control pull-left" name="Order[phones][]" type="text">' +
                        '<span class="input-group-btn">' +
                        '<button class="btn btn-danger btn-sm pull-right btn-remove-phone" type="button"><span class="fa fa-minus"></span></button>' +
                        '</span>' +
                        '</div>');
                }
            },
            remove_phone: function () {
                if (!order.editable) {
                    return;
                }

                $(this).closest(".order-phone").remove();
                order.update();
            },
            on_shipping_change: function () {
                // Подставляем даты доставку указанные в настройках КС
                var min_delivery_date = $(this).find('option:selected').attr('data-min_delivery_date');
                var available_delivery_from = $('#available_delivery_from');

                var delivery_from = $form.find('[name*=delivery_from]');

                if (typeof min_delivery_date != 'undefined' && min_delivery_date != null) {
                    date = new Date(min_delivery_date);
                } else {
                    date = new Date(available_delivery_from.data('default-delivery-from'));
                }
                if (delivery_from.datetimepicker('getDate').getTime() < date.getTime()) {
                    delivery_from.datetimepicker('setDate', date);
                    delivery_from.datetimepicker('setValue');
                }

                delivery_from.datetimepicker('setStartDate', date);
                var format = available_delivery_from.data('format');

                available_delivery_from.text(moment(date).format(format));

                order.update();
            },
            on_message_click: function () {
                var $modal = $("#modal-message");

                var to = $(this).closest(".input-group").find(":input").val();

                $modal.find("textarea").data("to", to);
                $modal.find("textarea").data("type", $(this).hasClass("btn-email") ? "email" : "sms");
                $modal.modal();
            },
            on_message_send: function () {
                var $button = $(this);
                var $modal = $("#modal-message");

                var data = $modal.find("textarea").data();
                data.text = $modal.find("textarea").val();
                data[yii.getCsrfParam()] = yii.getCsrfToken();

                $button.prop("disabled", true);
                $.post("message?id=" + order.id_param, data)
                    .success(function (response) {
                        $button.prop("disabled", false);
                        $modal.modal("hide");
                        toastr.info(response.message);
                    })
                    .error(function (response) {
                        $button.prop("disabled", false);
                        toastr.error(response.message);
                    });
            },
            gmap_autocomplete: null,
            on_gmap_autocomplete: function () {
                $form.find("[data-gmap-address-component]").val("");
                var components = form.gmap_autocomplete.getPlace().address_components;
                for (var i = 0; i < components.length; i++) {
                    var component = components[i];
                    $form.find("[data-gmap-address-component=" + component.types[0] + "]").val(component.long_name);
                }
            },
            genesys_status_map: {
                3: 40,
                4: 34,
                5: 35,
                6: 37,
                7: 36
            },
            on_status_open: function () {
                var statusValue = window.dataStatus;
                $form.find('#order-status').val(statusValue)
                //console.log('$form', $form.data());
                // custom for recall-status
                $("#modal-status-" + dataStatus).attr('data-order_id', $form.data('id'));
                $("#modal-status-" + dataStatus).modal().find(":input").prop("disabled", 0);

                if (statusValue == 2) {
                    $recall_input = $('#order-recall_date')
                    if ($recall_input.val() == '') {
                        $btn_save_recall = $('#modal-status-' + statusValue + ' .modal-footer .btn-status-save')
                        $btn_save_recall.attr('disabled', 'disabled')

                        $recall_input.bind('keyup change', function () {
                            if ($recall_input.val() != '') {
                                $btn_save_recall.removeAttr('disabled')
                            } else {
                                $btn_save_recall.attr('disabled', 'disabled')
                            }
                        })
                    }

                }
                toParent('status_open');
                toParent('status_open-' + dataStatus);
            },
            before_on_status_open: function () {
                window.dataStatus = $(this).data("status");
                var inputFullNameCustomer = $('[name*=customer_full_name]');

                if (inputFullNameCustomer.val() === '') {
                    $(inputFullNameCustomer).val('Empty');
                }

                // if (window.dataStatus != 4) {
                //     $('#form-order').yiiActiveForm('remove', 'order-general-customer_full_name');
                // }

                form.check_zip_codes();
            },
            on_status_save: function () {
                var newStatus = $(this).data("status");
                $("#order-status").val(newStatus);

                // validate product before save
                var is_empty_product = 0;
                $('.product-select select').each(function () {
                    if ($(this).val() == '') {
                        is_empty_product += 1;
                        var $currentId = $(this).attr('id')
                        $(this).parent('.field-' + $currentId).removeClass('has-success').addClass('has-error');
                    }
                });

                if (is_empty_product > 0) {
                    toastr.error('Необходимо заполнить «Товар»');
                    return false;
                } else {
                    if (typeof(hideButtonAfterClick) === "function") {
                        hideButtonAfterClick(this);
                    }

                    // воткнуть диалог обратно в форму чтобы отправились все поля
                    $form.append($("#modal-status-" + $(this).data("status"))).submit();
                    toParent('form_order_status_save');
                }
            },
            on_status_dismiss: function () {
                $(this).find(":input").prop("disabled", 1);
                // reset loader
                $('.btn-status-save').show();
                $('.loader').remove()
                //дубль на close_modal_status
                //toParent('form_order_on_dismiss-');
            },
            on_save: function () {
                order.update();
                if (typeof(wde_set_call_result) === "function" && form.genesys_status_map[order.status]) {
                    wde_set_call_result(form.genesys_status_map[order.status]);
                }
                if (typeof(wde_set_callback_date_time) === "function" && order.status == 2) {
                    wde_set_callback_date_time($("#order-recall_date").val());
                }
                //quality-control edit
                try {
                    if (typeof(window.parent.frontend_set_callback_param_get_order) === "function") {
                        window.parent.frontend_set_callback_param_get_order();
                    }
                } catch (exception) {

                }
            },
            on_after_save: function () {
                toParent('form_order_on_save');
                toParent('form_order_on_save-' + order.status);
            },
            check_zip_codes: function () {
                var google_zip = typeof $("#form-order #google_zip").val() != 'undefined' ? $("#form-order #google_zip").val() : $("#order-online").contents().find("#form-order #google_zip").val();

                if (google_zip == '') {
                    window.form.on_status_open();
                } else if (google_zip == $("#form-order [name*=zip]").val()) {
                    window.form.on_status_open();
                }
                else {

                    var field_customer_zip = $("#form-order [name*=zip]");
                    var customer_zip = '';

                    if(field_customer_zip.prop('type') == 'select-one'){
                        customer_zip = /Select/.test(field_customer_zip) ? '' : field_customer_zip.find('option:selected').last().text();
                    }else{
                        customer_zip = field_customer_zip.val();
                    }

                    if(customer_zip == ''){
                        customer_zip = 'EMPTY';
                    }

                    console.log('-> customer_zip', customer_zip);
                    console.log('-> type', field_customer_zip.prop('type'));
                    console.log('-> option', field_customer_zip.find('option:selected').last().text());
                    console.log('-> text', field_customer_zip.text());
                    console.log('-> val', field_customer_zip.val());

                    $("#field_google_zip").html($("#form-order #google_zip").val());
                    $("#field_customer_zip").html(customer_zip);
                    $('#confirm_google_zip').modal('show');
                }

                return true;
            },
            init: function () {
                var products = $("#form-order [name*=product_id]");

                products.map(function () {

                    if ($(this).val() != '') {
                        $(this).trigger('change');
                        var product_id = $(this).val();

                        var $quantity = $(this).parent().closest(".product").find("[name*=quantity]");

                        $quantity.prop('id', 'select_' + product_id + '_' + (new Date()).getTime());

                        var $amount = $(this).parent().closest(".product").find("[name*=amount]");

                        //у промотовара фиксированная цена
                        if (!isUndefined($_productsPriceData[product_id])) {
                            var value = $_productsPriceData[product_id].amount;
                            $amount.val('(' + value + ')');
                            $amount.css('color', (value > 0 ? 'green' : 'red'));
                        }
                    }
                });

                //акции фильтрация акций
                initFilterPackages();
            },

            on_package_change: function () {
                //активация акции
                setGroupGiftsFromPackage($(this));
            },

            set_shipping_list: function (shipping_list) {
                var shipping_select = $form.find('select[name*=shipping_id]');
                var selected_id = shipping_select.find('option:selected').val();
                if (typeof selected_id == 'undefined') {
                    selected_id = shipping_select.data('default-shipping-id');
                }
                shipping_select.blur();
                shipping_select.empty();

                if (shipping_list.length > 0) {
                    $('.delivery_not_found').addClass('hidden');
                } else {
                    $('.delivery_not_found').removeClass('hidden');
                }

                for (var i = 0; i < shipping_list.length; i++) {
                    var shipping = shipping_list[i];
                    var option = $(document.createElement('option'));
                    option.text(shipping['name']);
                    option.val(shipping['id']);
                    if (shipping['id'] == selected_id) {
                        option.attr('selected', true);
                    }

                    if ('price' in shipping) {
                        option.attr('data-price', shipping['price'])
                    }

                    if ('dynamic_price' in shipping) {
                        option.attr('data-dynamic_price', shipping['dynamic_price']);
                    }

                    if ('min_delivery_date' in shipping) {
                        option.attr('data-min_delivery_date', shipping['min_delivery_date']);
                    }

                    if ('max_delivery_date' in shipping) {
                        option.attr('data-max_delivery_date', shipping['max_delivery_date']);
                    }

                    shipping_select.append(option);

                    shipping_select.find('option').first().prop('selected', 'selected');
                    shipping_select.trigger('change');
                }
            },
            receive_shipping_list_from_partner: function () {

                $('select[name*=shipping_id]').find('option').remove();

                var use_broker = $form.find('select[name*=shipping_id]').data('use-broker');
                if (use_broker == 0 || typeof use_broker == 'undefined' || !window.use_price_shipping) {
                    return false;
                }
                var data = {};

                var customer_zip = window.$form.find('[name*=customer_zip]').find('option:selected').text() || window.$form.find('[name*=customer_zip]').val();
                if (typeof customer_zip != 'undefined') {
                    data['customer_zip'] = customer_zip;
                }

                var products = [];

                //при пустом zip брокер не возвращает полный список, но если пустой зип не отправлять вообще - то гуд.
                //по идее нужна правка на пее, но пока можно обойтись своими силами
                if (customer_zip.replace(/  /, '').length == 0 || /[0-9]/.test(customer_zip) === false) {
                    delete data.customer_zip;
                }

                window.$form.find('.products').each(function () {
                    if ($(this).find("[name*=product_id]").val() != '') {
                        products.push({
                            id: $(this).find("[name*=product_id]").val(),
                            price: $(this).find("[name*=price]").val(),
                            quantity: $(this).find("[name*=quantity]").val(),
                            gift: 0,
                        });
                    }
                });

                window.$form.find('#promo-products').each(function () {
                    if ($(this).find("[name*=product_id]").val() != '') {
                        products.push({
                            id: $(this).find("[name*=product_id]").val(),
                            price: $(this).find("[name*=price]").val(),
                            quantity: $(this).find("[name*=quantity]").val() || $(this).find("[name*=promo_count]").val(),
                            gift: 0,
                        });
                    }
                });

                window.$form.find('.products-gift').each(function () {
                    if ($(this).find("[name*=product_id]").val() != '') {
                        products.push({
                            id: $(this).find("[name*=product_id]").val(),
                            price: $(this).find("[name*=price]").val(),
                            quantity: $(this).find("[name*=quantity]").val(),
                            gift: 1,
                        });
                    }
                });

                if (products.length > 0) {
                    data['products'] = products;
                }

                var fieldsetButtons = $('.modal-status').closest('fieldset');
                var fieldsetShipping = $('[name*=shipping_id]').closest('fieldset');
                var warning = $('.shipping-warning');
                var danger = $('.shipping-danger');

                $(danger).html('');
                $(warning).html('');

                $(fieldsetButtons).attr('disabled', 'disabled');
                $(fieldsetShipping).attr('disabled', 'disabled');
                $(warning).removeClass('hidden');
                $(warning).html('Please wait for the update of the list of delivery list');

                data['order_price'] = parseInt($('.total-price').text());

                //если есть влияние на стоимость доставки акцией - передаём данные о стоимости доставки
                if(!isUndefined(window.order_shipping_price)) {
                    data['shipping_price'] = window.order_shipping_price;
                }

                $.ajax({
                    type: 'POST',
                    url: $form.find('select[name*=shipping_id]').data('broker-url'),
                    data: data,
                    success: function (response) {
                        $(warning).addClass('hidden');
                        $(danger).addClass('hidden');

                        $(fieldsetButtons).removeAttr('disabled');
                        $(fieldsetShipping).removeAttr('disabled');
                        window.form.set_shipping_list(response);
                    },
                    error: function (error) {
                        $(fieldsetButtons).removeAttr('disabled');
                        $(fieldsetShipping).removeAttr('disabled');

                        $(danger).removeClass('hidden');
                        $(danger).html('An error occurred while retrieving the delivery list. <button>To repeat</button>');

                        $(danger).find('button').on('click', function () {
                            window.form.receive_shipping_list_from_partner();
                            return false;
                        });
                        $(warning).addClass('hidden');
                    }
                })
            }
        };

        window.form.init();

        $form.on("change", "[name*=package_id]", form.on_package_change);

        $form.on("beforeSubmit", form.on_save);
        $form.on("ajaxComplete", form.on_after_save);

        $form.on("click", ".btn-add-phone", form.add_phone);
        $form.on("click", ".btn-remove-phone", form.remove_phone);

        $form.on("click", ".btn-add-product", form.add_product);
        $form.on("click", ".btn-remove-product", form.remove_product);

        $form.on("click", ".btn-add-product-gift", form.add_product_gift);
        $form.on("click", ".btn-remove-product-gift", form.remove_product_gift);

        $form.on("change", "[name*=product_id]", form.on_product_change);
        $form.on("change keyup", "[name*=quantity]", form.on_quantity_change);
        $form.on("change", "[type=checkbox][name*=gift]", form.on_gift_change);

        $form.on("change", "select[name*=shipping_id]", form.on_shipping_change);

        $form.on("click", ".btn-status", form.before_on_status_open);
        $(document).on("click", ".btn-status-save", form.on_status_save);
        $(document).on("hidden.bs.modal", ".modal-status", form.on_status_dismiss);

        $form.on("change-vat", form.on_vat_change);

        $form.on("click", ".btn-email", form.on_message_click);
        $form.on("click", ".btn-phone", form.on_message_click);
        $(document).on("click", "#modal-message .btn-message-send", form.on_message_send);
        $form.on("change", "#order-country_id", form.on_country_change);

        $form.on("change", "[name*=type_id], [name*=Order\\[general\\]], [name*=Order\\[phones\\]], [name*=customer_address]", order.update);

        $form.on('change keyup', "[name*=customer_zip]", form.receive_shipping_list_from_partner);
        $form.on('change keyup', "#promo-products [name*=product_id]", form.receive_shipping_list_from_partner);
        $form.on('change keyup', "#promo-products [name*=promo_count]", form.receive_shipping_list_from_partner);

        if ($form.find("[data-gmap-autocomplete]").length == 1) {
            form.gmap_autocomplete = new google.maps.places.Autocomplete(
                $form.find("[data-gmap-autocomplete]").get(0),
                {types: ["geocode"]}
            );
            form.gmap_autocomplete.addListener('place_changed', form.on_gmap_autocomplete);
        }

        order.update();
        //мистическая строка, не понятно зачем только для первого товара в списке доступных. сбрасывала select акций
        //$form.find(".product [name*=product_id]").first().trigger("change");

        if (typeof(wde_may_block_mark_done) === "function") {
            wde_may_block_mark_done();
        }

        // on autosize-plugin
        autosize(document.querySelectorAll('textarea.form-control'));

        // validate empty products
        $('.products').on('change', '.product-select select', function () {
            $currentId = $(this).attr('id')
            $parentBox = $('.product-select.field-' + $currentId)

            if ($('#' + $currentId).val() == '') {
                $parentBox.removeClass('has-success').addClass('has-error');
            } else {
                $parentBox.removeClass('has-error');
            }
        });


    });

    function isCheckGiftById(id) {
        return $('#orderproduct-' + id + '-gift').prop("checked");
    }

    // switch on/off gift-function
    function useCheckGift($object) {
        $product = $object.closest(".product")

        $product.find("[name*=price]").val(0);
        $product.find("[name*=cost]").val(0);
    }

} catch (e) {
    if (typeof $('#form-order').data() == 'undefined') {
        var order_id = $('#order-online').contents().find('#form-order').data('id');
        var user_id = $('#order-online').contents().find('#form-order').find('#call_history_user_id').val();
    } else {
        var order_id = $('#form-order').data('id');
        var user_id = $('#call_history_user_id').val();
    }

    JSLogger({
        script: 'change.js',
        order_id: order_id,
        user_id: user_id,
    }, e);
}


$(function(){
    var HiddenPhones = {
        namesFieldsBlock: '#masked_inputs',
        init: function(){
            HiddenPhones.eventHide();
        },
        eventHide: function(){
            var names = $(HiddenPhones.namesFieldsBlock).html();

            if(names != ''){
                names = eval(names);
            }

            if(typeof names[0] != 'undefined') {
                $('[name*=' + names[0] + ']').closest('fieldset').on('contextmenu', function(){return false;});
            }

            for(var k in names){
                var selector = '[name*='+names[k]+']';
                var value = $(selector).val();

                if(typeof value != 'undefined'){
                    console.log('value', value);
                    $(selector).on('contextmenu', function(){return false;});
                }
            }
        }
    };

    HiddenPhones.init();
});
$(document).ready(function () {
    $('[name="appointment"]').on('change', function () {
        if ($('[name="appointment"]').val() != '') {
            $("#appointment-modal").modal('show');
        }else{
            $("#appointment-modal-release").modal('show');
        }

        $(".btn-save-appointment").click(function (e) {
            //e.preventDefault();

            $.ajax({
                url: '/order/change/appointment-operator',
                type: 'POST',
                data: {
                    order_id: $('[name="CallHistory[order_id]"]').val(),
                    user_id: $('[name="appointment"]').val()
                },
                success: function (data) {
                    var result = JSON.parse(data);

                    if (result.success) {
                        $("#blocked_user").html($('[name="appointment"]').val() + ' ' + $('[name="appointment"] option:selected').text())
                        toastr.success(result.message);
                    } else {
                        toastr.error(result.message);
                    }

                    $("#appointment-modal").modal('hide');
                },
                error: function (data) {
                    toastr.error(data.responseText);
                    $("#appointment-modal").modal('hide');
                }
            });
        });

        $(".btn-cancel-appointment").on('click', function () {
            $("#appointment-modal").modal('hide');
        });



        $(".btn-release").click(function (e) {
            //e.preventDefault();

            $.ajax({
                url: '/order/change/release-operator',
                type: 'POST',
                data: {
                    order_id: $('[name="CallHistory[order_id]"]').val()
                },
                success: function (data) {
                    var result = JSON.parse(data);

                    if (result.success) {
                        toastr.success(result.message);
                    } else {
                        toastr.error(result.message);
                    }

                    $("#appointment-modal-release").modal('hide');
                },
                error: function (data) {
                    toastr.error(data.statusText);
                    $("#appointment-modal-release").modal('hide');
                }
            });

        });

        $(".btn-cancel-release").on('click', function () {
            $(".btn-release").unbind('click')
            $("#appointment-modal-release").modal('hide');
        });
    });

});
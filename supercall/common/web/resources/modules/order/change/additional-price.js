function checkAdditionalPrice(order) {

    if(typeof order != 'object'){
        return order;
    }


    if(!$("#partner_additional_price").val()){
        return order;
    }

    var additionalPrice = JSON.parse($("#partner_additional_price").val());
    
    switch (additionalPrice.addition_type) {
        case 'product':
            break;
        case 'product_quantity':
            var quantity = 0;
            if(typeof order.products == 'object'){
                order.products.forEach(function (product) {
                    if(typeof product.gift == 'undefined'){
                        quantity += parseInt(product.quantity);
                    }
                });
            }
            //Тут по умолчанию считаем, что из заказа убираем цену за доставку
            switch (additionalPrice.condition) {
                case '1':
                    if(quantity == additionalPrice.quantity){
                        order.shipping_price = 0;
                        window.order_shipping_price = 0;
                    }
                    break;
                case '2':
                    if(quantity != additionalPrice.quantity){
                        order.shipping_price = 0;
                        window.order_shipping_price = 0;
                    }
                    break;
                case '3':
                    if(quantity > additionalPrice.quantity){
                        order.shipping_price = 0;
                        window.order_shipping_price = 0;
                    }
                    break;
                case '4':
                    if(quantity < additionalPrice.quantity){
                        order.shipping_price = 0;
                        window.order_shipping_price = 0;

                    }
                    break;
                case '5':
                    if(quantity >= additionalPrice.quantity){
                        order.shipping_price = 0;
                        window.order_shipping_price = 0;
                    }
                    break;
                case '6':
                    if(quantity <= additionalPrice.quantity){
                        order.shipping_price = 0;
                        window.order_shipping_price = 0;
                    }
                    break;
            }
    }

    return order;
}
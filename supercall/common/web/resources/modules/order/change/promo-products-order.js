$(function () {
    //в методе order.update() часть функционала
    //common/web/resources/modules/order/change/change.js
    //common/web/resources/modules/order/change/change.js:150
    var PromoProductsOrder = {
        countPromo: '#promoProductCountProduct',//2,
        form: '#form-order',
        promoProduct: '[name="OrderProduct[promoProduct][product_id][]"]',
        promoPrice: '.promo-price',
        promoQuantity: '.promo-count',
        promoFieldPrice: '[name="OrderProduct[promoProduct][price][]"]',
        promoFieldQuantity: '[name="OrderProduct[promoProduct][quantity][]"]',
        jsonBlockId: '#order-promo-products',
        jsonBlockAttr: 'data-promo-products',
        productArea: '.product',
        productAreaQuantity: '[name*=quantity]',
        productAreaCost: '[name*=cost]',
        productAreaProduct: '[name*=product_id]',
        btnAddProduct: '.btn-add-product',
        btnRemoveProduct: '.btn-remove-product',
        totalQuantity: '.total-quantity',
        totalPrice: '.total-price',
        finalPriceField: '[name*=final_price]',
        initPriceField: '[name*=init_price]',
        totalFinal: '.total-final',
        totalFinalNotVat: '.total-final-no-vat',
        addedPrice: 0,
        addedQuantity: 0,
        productBlockClass: '.product',
        promoProductBlockId: '#promo-products',
        promoProductSum: '[name*=promoProductSum]',
        init: function () {
            PromoProductsOrder.eventActivatePromo();
            PromoProductsOrder.eventChangeProduct();
            PromoProductsOrder.afterRender();
        },
        afterRender: function(){
            PromoProductsOrder.addedQuantity = $(PromoProductsOrder.promoFieldQuantity).val();
            PromoProductsOrder.addedPrice = $(PromoProductsOrder.promoFieldPrice).val();

            if(PromoProductsOrder.addedQuantity > 0){
                window.promo = true;
            }
        },
        eventActivatePromo: function () {
            PromoProductsOrder.eventChangeQuantityProductArea();
            PromoProductsOrder.eventChangeProductProductArea();

            setTimeout(function () {
                var countProducts = 0;

                $(PromoProductsOrder.productArea).find(PromoProductsOrder.productAreaQuantity).each(function () {
                    countProducts += parseInt($(this).val());
                });

                var disabledPromo = countProducts < parseInt($(PromoProductsOrder.countPromo).val());

                $(PromoProductsOrder.promoProduct).prop('disabled', disabledPromo);

                if (disabledPromo) {
                    PromoProductsOrder.resetPromo();
                }

                PromoProductsOrder.setFinalPrice();
            }, 500);
        },
        resetPromo: function () {
            $(PromoProductsOrder.promoProduct).val('');

            PromoProductsOrder.setPrice(0);
            PromoProductsOrder.setQuantity(0);

            PromoProductsOrder.removeQuantity();
            PromoProductsOrder.removeSum();
        },
        eventChangeQuantityProductArea: function () {
            setTimeout(function () {
                $(PromoProductsOrder.productArea).find(PromoProductsOrder.productAreaQuantity).on('change', function () {
                    PromoProductsOrder.eventActivatePromo();
                });
            }, 500);
        },
        eventChangeProductProductArea: function () {
            setTimeout(function () {
                $(PromoProductsOrder.productArea).find(PromoProductsOrder.productAreaProduct).on('change', function () {
                    PromoProductsOrder.eventActivatePromo();
                });
            }, 500);
        },
        eventChangeProduct: function () {
            $(PromoProductsOrder.promoProduct).on('change', function () {
                if ($(this).val() === '') {
                    PromoProductsOrder.removeSum();
                    PromoProductsOrder.removeQuantity();

                    PromoProductsOrder.setPrice(0);
                    PromoProductsOrder.setQuantity(0);

                } else {
                    var listPromoProducts = JSON.parse($(PromoProductsOrder.jsonBlockId).attr(PromoProductsOrder.jsonBlockAttr));

                    for (var k in listPromoProducts) {
                        if (($(this).val() === k)) {
                            var data = listPromoProducts[k];

                            PromoProductsOrder.addSum(data.price);
                            PromoProductsOrder.addQuantity(1);

                            PromoProductsOrder.setPrice(data.price);
                            PromoProductsOrder.setQuantity(1);
                        }
                    }
                }

                PromoProductsOrder.setFinalPrice();
            });
        },
        setPrice: function (price) {
            $(PromoProductsOrder.promoPrice).html(price);
            $(PromoProductsOrder.promoFieldPrice).val(price);
        },
        setQuantity: function (quantity) {
            $(PromoProductsOrder.promoQuantity).html(quantity);
            $(PromoProductsOrder.promoFieldQuantity).val(quantity);
        },
        addSum: function (sum) {
            if(PromoProductsOrder.addedPrice > 0){
                return;
            }

            var sumTotal = sum + parseFloat($(PromoProductsOrder.totalPrice).text());
            var sumFinal = sum + parseFloat($(PromoProductsOrder.totalFinal).text());
            var sumFinalVat = sum + parseFloat($(PromoProductsOrder.totalFinalNotVat).text());

            $(PromoProductsOrder.totalPrice).text(sumTotal.toFixed(2));
            $(PromoProductsOrder.totalFinal).text(sumFinal.toFixed(2));
            $(PromoProductsOrder.totalFinalNotVat).text(sumFinalVat.toFixed(2));

            $(PromoProductsOrder.promoFieldPrice).val(sum);

            PromoProductsOrder.addedPrice = sum;
        },
        removeSum: function () {
            var addedPrice = parseFloat(PromoProductsOrder.addedPrice);

            if(isNaN(addedPrice)){
                addedPrice = 0;
            }

            var sumTotal = parseFloat($(PromoProductsOrder.totalPrice).text() - addedPrice);
            var sumFinal = parseFloat($(PromoProductsOrder.totalFinal).text() - addedPrice);
            var sumFinalVat = parseFloat($(PromoProductsOrder.totalFinalNotVat).text() - addedPrice);

            $(PromoProductsOrder.totalPrice).text(sumTotal.toFixed(2));
            $(PromoProductsOrder.totalFinal).text(sumFinal.toFixed(2));
            $(PromoProductsOrder.totalFinalNotVat).text(sumFinalVat.toFixed(2));

            $(PromoProductsOrder.promoFieldPrice).val(0);

            PromoProductsOrder.addedPrice = 0;
        },
        addQuantity: function (quantity) {
            if(PromoProductsOrder.addedQuantity > 0){
                return;
            }

            $(PromoProductsOrder.totalQuantity).text(quantity + parseInt($(PromoProductsOrder.totalQuantity).text()));
            $(PromoProductsOrder.promoFieldQuantity).val(quantity);

            PromoProductsOrder.addedQuantity = quantity;
        },
        removeQuantity: function () {
            var addedQuantity = parseInt(PromoProductsOrder.addedQuantity);

            if(isNaN(addedQuantity)){
                addedQuantity = 0;
            }

            var quantity = parseInt($(PromoProductsOrder.totalQuantity).text()) - addedQuantity;

            $(PromoProductsOrder.totalQuantity).text(quantity);
            $(PromoProductsOrder.promoFieldQuantity).val(0);

            PromoProductsOrder.addedQuantity = 0;
        },
        setFinalPrice: function(){
            var price = $(PromoProductsOrder.totalFinalNotVat).text();

            if(!isNaN(price)) {
                $(PromoProductsOrder.finalPriceField).val(price);
            }
        },
        recalculate: function(){
            var reQuantity = 0;
            var rePrice = 0;

            $(PromoProductsOrder.productBlockClass).find(PromoProductsOrder.productAreaQuantity).each(function(){
                reQuantity += parseInt($(this).val());
            });

            reQuantity += parseInt($(PromoProductsOrder.promoProductBlockId).find(PromoProductsOrder.productAreaQuantity).val());


            $(PromoProductsOrder.productBlockClass).find(PromoProductsOrder.productAreaCost).each(function(){
                rePrice += parseFloat($(this).val() == '' ? 0 : $(this).val());
            });

            rePrice += parseFloat($(PromoProductsOrder.promoProductBlockId).find(PromoProductsOrder.promoFieldPrice).val());

            if(rePrice === 0){
                reQuantity = 0;
            }
        }
    };

    PromoProductsOrder.init();
    $(PromoProductsOrder.btnAddProduct).on('click', PromoProductsOrder.eventActivatePromo);
    $(PromoProductsOrder.btnRemoveProduct).on('click', PromoProductsOrder.eventActivatePromo);
});
$(function () {
    //modal google zip confirm
    $("#google_confirm_success").on('click', function () {
        $("#form-order [name*=zip]").val($("#form-order #google_zip").val());
        $('#confirm_google_zip').modal('hide');
    });
    $("#google_confirm_cancel").on('click', function () {
        $('#confirm_google_zip').modal('hide');
    });

    //  is there post code?
    var post_code_block = '#order-address-zip';
    if ($form.find(post_code_block).length > 0) {
        if (typeof(post_code_mask) === "function") {
            var paramsMask = post_code_mask();
            $(post_code_block).mask(paramsMask)
        }
    }

    //модальное окно проверки корректности zip кода, после его закрытия - вызвать весь функционал кнопока смены статуса
    $('#confirm_google_zip').on('hidden.bs.modal', function () {
        window.form.on_status_open();
    });
});
function calc_price(price, quantity) {
    var priceAll = 0;

    switch (parseInt(quantity)) {
        case 1:
            priceAll = 1*price;
            break;
        case 2:
            priceAll = 2*price;
            break;
        case 3:
            priceAll = 2*price;
            break;
        case 4:
            priceAll = 4*price;
            break;
        case 5:
            priceAll = 3*price;
            break;
        case 6:
            priceAll = 6*price;
            break;
        case 7:
            priceAll = 4*price;
            break;
        case 8:
            priceAll = 8*price;
            break;
        case 9:
            priceAll = 5*price;
            break;
        case 10:
            priceAll = 10*price;
            break;
        default:
            priceAll = price*quantity;
            break;
    }

    return priceAll;
}

function calc_shipping(order){
    return 0;
}

function calc_shipping_vat(order){
    return 0;
}

function post_code_mask() {
  return '000 00';
}
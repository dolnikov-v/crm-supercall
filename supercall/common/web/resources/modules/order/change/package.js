/**
 * Акции
 * Псевдо disabled для Select
 * @param  elem elem from DOM
 * @param action bool
 */
function setReadOnly(elem, action) {
    var color = action ? '#99A5C2' : 'black';
    var css = action ? 'none' : 'auto';
    elem.css('pointer-events', css);
    elem.css('color', color);

    if (!action) {
        elem.attr('size', elem.children('option').length);
        elem.attr('size', 0);
    }
}

/**
 * Акции
 * Определяет - в readOnly элемент или нет
 * @param elem
 * @returns {boolean}
 */
function isReadOnly(elem) {
    return elem.css('pointer-events') == 'none';
}

/**
 * Акции
 * Отфильтровывает список доступных акций по выбранному товару
 */
function initFilterPackages() {
    var paid_products = $('.products [name*=product_id]');

    paid_products.map(function () {
        var name = $(this).prop('name');
        var val = $(this).val();
        var index = name.replace('OrderProduct[', '').replace('][product_id]', '');
        var quantity = $('[name="OrderProduct[' + index + '][quantity]"]');

        var package = $(this).closest(".product").find("[name*=package]");
        var packages_data = $('#order-package').data('order-package');
        var packageOfProduct = [];

        for (var k in packages_data) {
            if (packages_data[k].product == $(this).val()) {
                packageOfProduct.push([packages_data[k].id, packages_data[k].name]);
            }
        }

        package.empty();
        package.append(new Option('-', '')).val('');

        for (id in packageOfProduct) {
            let packageId = packageOfProduct[id][0];
            let packageValue = packageOfProduct[id][1];
            package.append('<option value="' + packageId + '"' + 'data-product_price=' + $_product_price[packageId] + '>' + packageValue + '</option>');
        }

        //package.val($(this).closest('.product').data('package'));
        package.val($(this).attr('package'));
    });
}

/**
 * Акции
 * Выставит список акций по выбранному продукту
 * onchange select product
 * @param elemProduct
 */
function setFilterPackagesByProduct(elemProduct) {
    if (!isReadOnly(elemProduct)) {
        var package = elemProduct.closest(".product").find("[name*=package]");
        var packageOfProduct = [];

        for (var k in $_packages_data) {
            if ($_packages_data[k].product == elemProduct.val()) {
                packageOfProduct.push([$_packages_data[k].id, $_packages_data[k].name]);
            }
        }

        package.empty();
        package.append(new Option('-', '')).val('');

        for (id in packageOfProduct) {
            let packageId = packageOfProduct[id][0];
            let packageValue = packageOfProduct[id][1];
            package.append('<option value="' + packageId + '"' + 'data-product_price=' + $_product_price[packageId] + '>' + packageValue + '</option>');
        }
    }
}

/**
 * Акции
 * Выставить или создаст новую группу подарков и выставит значения акции, залочит поля
 * @param packageSelect - elem from DOM packageSelect
 */
function setGroupGiftsFromPackage(packageSelect) {
    var package_id = packageSelect.val();

    var product = packageSelect.closest('.product').find('[name*=product_id]');
    var quantity = packageSelect.closest('.product').find('[name*=quantity]');
    var nameProduct = product.prop('name');
    var split = nameProduct.match(/([0-9]{1,})/);
    var index = split[0];
    var html, group_gifts, gift_product, gift_quantity, gift_package;

    var packageData = $_packages_data[package_id];

    if (product.closest('.product').attr('identity') === '' || typeof product.closest('.product').attr('identity') === 'undefined') {
        product.closest('.product').attr('identity', 'product_' + index);
    } else {
        index = product.closest('.product').attr('identity').replace('product_', '');
    }
    var time = new Date();
    var stamp = time.getTime();

    if (package_id !== '') {
        for (var k in packageData.gifts) {
            group_gifts = getGroupGiftsByIdentity(index, stamp);
            var gift = packageData.gifts[k];


            for (var k in group_gifts) {
                var group = group_gifts[k];

                gift_product = group.find('[name*=product_id]');
                gift_quantity = group.find('[name*=quantity]');
                gift_package = group.find('[name*=package_id]');


                setReadOnly(product, true);
                setReadOnly(quantity, true);

                product.val(packageData.product);
                group.attr('identity', 'gift_' + index);

                html = '<span style="font-size: 16px; padding-top:6px" title="' + packageData.name + '" class="glyphicon glyphicon-gift"></span>';
                group.find('.gift-icon').html(html);

                setReadOnly(gift_product, true);
                gift_product.val(gift.product_id);

                setReadOnly(gift_quantity, true);
                gift_quantity.val(gift.free_amount);

                $(group).val(package_id);
                quantity.val(packageData.paid_amount);

                quantity.trigger('change');
            }
        }
    } else {
        if (product.closest('.product').attr('identity') !== '') {
            index = product.closest('.product').attr('identity').replace('product_', '');
            var gifts_parent = $('.products-gift');
            var gifts = gifts_parent.find('.product-gift');
            var gift_length = gifts.length;

            setReadOnly(product, false);
            setReadOnly(quantity, false);

            gifts.map(function () {

                if ($(this).attr('identity') === 'gift_' + index) {
                    if (gift_length !== 1) {
                        $(this).remove();
                        --gift_length;
                    } else {
                        gift_product = $(this).find('[name*=product_id]');
                        gift_quantity = $(this).find('[name*=quantity]');
                        gift_package = $(this).find('[name*=package_id]');

                        gift_product.val('');
                        setReadOnly(gift_product, false);

                        gift_quantity.val(1);
                        setReadOnly(gift_quantity, false);

                        $(this).find('.gift-icon span').remove();
                        $(this).attr('identity', '');
                    }
                }
            });
        }
    }

}

/**
 * Акции
 * Метод возвражает группу подарков - если она подходит по швутешен
 * если нет совпадений - будет выбрана группа с пустым продуктом
 * еслии такой не найдено - будет создана новая группа
 * @param identity
 * @param group
 * @returns {string}
 */
function getGroupGiftsByIdentity(identity, group) {
    var group_gifts = $('.products-gift').find('.product-gift');
    var founded_group = [];


    //группа не была найдена - подойдёт любая свободная
    if (founded_group.length === 0) {
        group_gifts.map(function () {
            if ($(this).find('[name*=product_id]').val() === '') {
                founded_group.push($(this));
            }
        });

        //нету свободных групп - создадим
        if (founded_group.length === 0) {
            $('.btn-add-product-gift').trigger('click');
            founded_group.push($('.products-gift').find('.product-gift').last());
        }
    }

    //если найдена группа подарков по identity
    group_gifts.map(function () {
        if ($(this).attr('identity') === 'gift_' + identity) {

            if ($(this).attr('group') != group) {
                $(this).remove();
            }
        }
    });

    for (var k in founded_group) {
        founded_group[k].attr('group', group);
    }

    return founded_group;
}
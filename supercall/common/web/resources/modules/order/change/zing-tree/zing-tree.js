$(document).ready(function () {
    $('.scriptsPanels').on('show.bs.collapse', function (e) {
        getZingTree();
    });

    $('.product').find("[name*=product_id]").on('change', function () {
        getZingTree();
    });
});


function getZingTree() {
    //нужно определить где мы - дев, мастер, локальный ?
    var domain = location.host.split(/\./)

    if (domain[1] == 'dev') {
        var host = location.protocol + '//' + 'panel.dev.2wcall.com'
    } else if (domain[1] == '2wcall') {
        var host = location.protocol + '//' + 'panel.2wcall.com/'
    } else if (domain[1] == 'vasinsky') {
        var host = location.protocol + '//' + 'panel.vasinsky.2wcall.com/'
    } else {
        var host = location.protocol + '//panel.' + $("#document_domain").text();
    }

    $(".scriptsPanels .panel-body").html('<div class="loader"></div>');

    $.ajax({
        url: host + '/operator/zing-tree/get-zing-tree',
        type: 'get',
        dataType: 'jsonp',
        jsonpCallback : 'zingtree',
        data : {
            country_id : $('#form-order').data('country_id'),
            product_id : $('#form-order').data('product_id'),
            order_type_id : $('#form-order').data('order_type_id')
        },
        error: function (error) {
            /*toastr.error('ZingTree: '+ error.status + ' ' + error.statusText)*/
        },
        success: function (data) {
            var panel = $(".scriptsPanels");

            if (data.success) {
                $(".scriptsPanels .panel-body").html('');
                $('<iframe>', {
                    src: data.url,
                    id: 'ZingTreeIframe',
                    width: 100 + '%',
                    height: 600 + 'px',
                    frameborder: 0,
                    scrolling: 'yes'
                }).appendTo('.scriptsPanels .panel-body');

                panel.css('height', '60px');

                var width = panel.find('.panel-group').width();

                var header = panel.find('.panel-group');
                header.css('position', 'absolute');
                header.css('top', '0px');
                header.css('right', '15px');
                header.css('width', width);
                header.css('z-index', '999999');
            } else {
                //console.log(data.message);
                $(".scriptsPanels .panel-body").html(data.message)
            }
        }
    });
}
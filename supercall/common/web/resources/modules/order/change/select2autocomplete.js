//работа с select2 autocomplete в адресной форме - которые тянут данные из кладра
//по сути это зависимые списки, только без определённого порядка работы с ними

//включение/отключение автоматического режима - т.е. например - при выборе улицы - все верхнии уровни выставятся автоматически по структуре из КЛАДРа
use_auto = false;

$(window).load(function () {
    var list_select2Autocomplete = window.form.select2autocomplete;

    findPriceShippingsByIsKladerParams(window.form.select2autocomplete, false);
    setConfirmAddress(window.form.select2autocomplete);
    window.form.receive_shipping_list_from_partner();

    //завязка для google maps - поля с атрибюутом is_coordinates = true
    if (typeof  window.form.coordinatesFields != 'undefined') {
        for (var index in window.form.coordinatesFields) {
            $("#form-order [name*=" + window.form.coordinatesFields[index] + "]").on('keyup', function () {
                var address = getAddress();
                address.push($(this).val());
                window.google.reDrawMap(address);
            });
        }
    }

    for (var k in list_select2Autocomplete) {
        var temp = list_select2Autocomplete[k].split(/_/);
        var key = temp[1];
        var attribute_id = temp[2];
        //при выборе в select2 значения - мы будем пробовать восстановить родительскую структуру (если select2 сам не является корнем всей структуры)
        //для этого добавем слушателей для них

        $("#" + list_select2Autocomplete[k]).on("select2:select", function (e) {
            var attribute_id = $(this).val();
            var value = $(this).find('option').last().text();

            createHiddenInput(value, $(this));
            var address = getAddress();

            window.google.reDrawMap(address);

            if (key > 0) {
                if (use_auto) {
                    //если в select2 было выбрано значение из результатов поиска - у него есть родители в КЛАДРе
                    if (list_select2Autocomplete[k] > 0) {
                        $($("#form-order fieldset#fieldset_address")).css('opacity', 0.3);
                        $($("#form-order fieldset#fieldset_address")).css('cursor', 'wait');
                        getParentStructhure(attribute_id);
                    }
                }
            }

            //price-shipping - для определения стоимости доставки
            findPriceShippingsByIsKladerParams(list_select2Autocomplete, true);
            setConfirmAddress(window.form.select2autocomplete);
        });
    }
});

function findPriceShippingsByIsKladerParams(list_select2Autocomplete, modal) {
    //Чтобы цена не пересчитывалась
    if (!window.use_price_shipping || $('select[name*=shipping_id]').data('use-broker') == 1) {
        return;
    }

    if (typeof window.defaultShipping == 'undefined') {
        window.defaultShipping = $('select[name*=shipping_id]').find('option');
    }

    var defaultShipping = window.defaultShipping;

    $('select[name*=shipping_id]').find('option').remove();

    for (var j in defaultShipping) {
        if (typeof defaultShipping[j] == 'object' && typeof defaultShipping[j].value != 'undefined') {
            $('select[name*=shipping_id]').append(defaultShipping[j]);
        }
    }
    //обнулим КС, она далее в скрипте будет выставлена
    if ($('select[name*=price_shipping_id]').val() == '') {
        $('select[name*=shipping_id]').val('');
    }

    var shipping_id = $('select[name*=shipping_id]').val();
    var price_shipping = $('#order-price-shipping').data('priceShipping');
    var param = {};
    //console.group('Price shipping');
    for (var j in price_shipping) {

        var param_shipping = JSON.parse(price_shipping[j].param_shipping);

        //если мы ещё не приводили к читаемому виду
        if (typeof param_shipping[0] != 'undefined') {
            var clear_param_shipping = {};

            for (var k in param_shipping) {
                clear_param_shipping[param_shipping[k].name] = param_shipping[k].val;
            }

            price_shipping[j].param_shipping = JSON.stringify(clear_param_shipping);
        }

        //возращаем читаемый вид {"city":"city 10","street":"street 10","house":"100"} для сравнени с формой
        var paramShipping = JSON.parse(price_shipping[j].param_shipping);

        for (var k in list_select2Autocomplete) {
            var select2 = $('#' + list_select2Autocomplete[k]);
            var value = select2.find('option:selected').last().text();

            var name = select2.prop('name');
            var attrName = name.replace('Order[address]', '').replace('[', '').replace(']', '');

            //атрибут должен быть из списка атрибута JSON price_shipping
            if (typeof paramShipping[attrName] != 'undefined') {
                //чтобы исключить placeholder "Select or input a Улица ..."
                if (!/\.\.\./.test(value)) {
                    if (value.length > 0) {
                        param[attrName] = value;
                    }
                }
            }
            //for local tests
            if (typeof paramShipping['customer_' + attrName] != 'undefined') {
                if (!/\.\.\./.test(value)) {
                    if (value.length > 0) {
                        param['customer_' + attrName] = value;
                    }
                }
            }
        }
    }

    params = JSON.stringify(param);

    //флаг - покажет - нашли ли мы подходящие тарифы по адресу
    var founded_flag = false;

    for (var k in price_shipping) {
        var param_shipping = price_shipping[k].param_shipping.replace(/: /g, ':').replace(/, /g, ',');
        console.log(param_shipping.toLowerCase() == params.toLowerCase(), param_shipping.toLowerCase(), params.toLowerCase())
        //если нашли price_shipping
        if (param_shipping.toLowerCase() == params.toLowerCase()) {
            founded_flag = true;

            console.info('catched', params)

            var value = price_shipping[k].shipping_id;
            var text = $('select[name*=shipping_id]  option[value=' + price_shipping[k].shipping_id + ']').first().text().replace(/\s[0-9]{1,}/, '') + ' ' + price_shipping[k].price_shipping;

            $('select[name*=shipping_id]').append('<option data-price_shipping_id="' + price_shipping[k].id + '" data-price="' + price_shipping[k].price_shipping + '" value="' + value + '">' + text + '</option>');
        }
    }
    console.groupEnd();

    //если список курьерок (тарифов) был обновлен
    if (founded_flag) {
        //грохнуть данные по курьеркам по стране (т.к. список уже строиться по тарифам)
        var shipping_with_country = $('select[name*=shipping_id]').find("[data-with_country=1]");

        shipping_with_country.map(function () {
            $("select[name*=shipping_id]").find("option:contains(" + $(this).text() + ")").remove();
        });

        //покажем окошко - список курьерок был обновлен
        //если происходит редактирование заказа
        if (modal) {
            $("#change_shipping_window").modal('show');
        } else {
            //если происходит открытие заказа
            var valueByData = $('option[data-price_shipping_id="' + $('[name*=price_shipping_id]').val() + '"]').last().val();
            $("select[name*=shipping_id]").val(valueByData);
            //чтобы калькулятор посчитал стоимость доставки
            $("select[name*=shipping_id]").trigger('change');
        }

        var cloned_shipping_id = $('select[name*=shipping_id]').first().clone();
        cloned_shipping_id.prop('id', 'clone_' + $('select[name*=shipping_id]').prop('id'));

        $("#cloned_shipping_id").html(cloned_shipping_id);

        $('#change_shipping').unbind("click");

        $('#change_shipping').on('click', function () {
            if ($('#clone_order-shipping_id').val() != '') {
                $('#order-shipping_id').val($('#clone_order-shipping_id').val());
                $("#order-shipping_id").trigger('change');
                $("#change_shipping_window").modal('hide');
            }
        });
        //конец работы с модальным окно

    } else {
        //в том случае, если была выбрана КС по стране - нужно на всякий случай обнулить price_shipping_id
        //это можно различить по data-with_country=1 - у тарифов этого атрибута нет
        if ($("select[name*=shipping_id] option:selected").data('with_country') == 1 || $("select[name*=shipping_id]").val() == '') {
            $('[name*=price_shipping_id]').val('');
            //чтобы сработал калькулятор доставки к цене заказа
            $("select[name*=shipping_id]").trigger('change');
        }
        //Сообщим оперу о том - что тарифов нет на адрес, но есть КС по стране
        toastr.info('No tariffs found. The list of courier services has been changed. Courier services are available in the country.');
    }

    //в ордер добавили поле price_shipping_id
    //order-shipping_id - КС в форме заказа - price_shipping_id - price_shipping_id нужно писать при change
    $("#order-shipping_id").on('change', function () {
        var price_shipping_id = $(this).last().find('option:selected').attr('data-price_shipping_id');

        if (typeof price_shipping_id != 'undefined') {
            $('[name*=price_shipping_id]').val(price_shipping_id);
        }

        //если не выбрана ни одна КС
        if ($(this).val() == '') {
            //сбросим тариф
            $('[name*=price_shipping_id]').val('');
            //сбросим цену доставки
            $('[name*=shipping_price]').val(0);
        }
    });

    //clone_order-shipping_id - КС в модальном окне - rice_shipping_id писать - только по клику по кнопке модального окна
    $('#change_shipping').on('click', function () {
        var price_shipping_id = $("#clone_order-shipping_id").last().find('option:selected').attr('data-price_shipping_id');

        if (typeof price_shipping_id != 'undefined') {
            $('[name*=price_shipping_id]').val(price_shipping_id);
        }
    });
}


//вынес этот функционал из findPriceShippingsByIsKladerParams()
function setConfirmAddress(list_select2Autocomplete) {
    //Подтверждение адреса
    var all_is_klader_fields_vals = [];

    for (var k in list_select2Autocomplete) {
        var select2 = $('#' + list_select2Autocomplete[k]);
        all_is_klader_fields_vals.push($(select2).val());
    }

    //елси по итогам заполнения адреса есть хоть одно отрицательное value - то значит какое - либо значение отсутствовало в КЛАДР
    //соотв-но необходимо заходить заказ на 1 час - дл того чтобы ТМ проверил корректность адреса и поддвердил его

    //Изначально считаем - что адрес не нуждается в подтверждении
    var flag_confirm_address = false;

    for (var k in all_is_klader_fields_vals) {
        if (all_is_klader_fields_vals[k] < 0) {
            //найдены данные не из КЛАДР
            flag_confirm_address = true;

            //AddressVerification::ADDRESS_NEED_CONFIRM
            $('[name*=confirm_address]').val(1);
            /*
            console.group();
            console.error(all_is_klader_fields_vals[k]);
            console.info('address need confirm');
            console.groupEnd();
            */
        }
    }
    //все данные из КЛАДР, обнулим признак
    if (flag_confirm_address) {
        all_is_klader_fields_vals.push($(select2).val());
    }
}


function createHiddenInput(value, afterElem) {

    var hiddenInput = $('<input>').attr({
        type: 'hidden',
        id: afterElem.prop('id') + '_hidden',
        name: afterElem.prop('name'),
        value: value
    });

    var checkInsert = '#' + afterElem.prop('id');
    checkInsert += '_hidden';
    if ($(checkInsert).length > 0) {
        hiddenInput.insertAfter(checkInsert);
    } else {
        hiddenInput.insertAfter('#' + afterElem.prop('id'));
    }


}

function getAddress() {
    console.log('starting getting addresses');
    var address = [];

    address.push($("#order_country").val());

    for (var k in window.form.select2autocomplete) {
        var select2 = $("#" + window.form.select2autocomplete[k]);

        if (select2.find('option').length > 1) {
            var value = $("#" + window.form.select2autocomplete[k]).find('option').last().text();
            if (value != "") {
                address.push(value);
            }
        }
    }

    return address;
}

//Функция определяет родителя и уровень вложенности выбранного зависимого select2
function getParent(key) {
    if (key == 0) {
        return null;
    }
    else {
        $parentSelect2 = window.form.select2autocomplete[(key - 1)];

        return ($($parentSelect2).val() == '' || $($parentSelect2).val() == -1) ? null : {
            path: $('#' + $parentSelect2).val(),
            depth: (key + 1)
        };
    }
}

//Функция фозращает структуру от выбранного элемента select2 до корневого уровня
//для автоподстановки адреса
function getParentStructhure(id) {
    $.ajax({
        url: '/catalog/klader-countries/get-parent-structhure',
        type: 'POST',
        data: {
            child_id: id,
            _csrf: yii.getCsrfToken()
        },
        success: function (data) {
            var structhure = JSON.parse(data);

            for (var k in structhure) {
                if (typeof window.form.select2autocomplete[k] != 'undefined' && k < structhure[k].depth) {
                    //если начали заполнять не сначала - мы можем проставить парентов
                    if ($(window.form.select2autocomplete[k]).val() <= 0) {
                        var selectOption = new Option(structhure[k].name, structhure[k].id, true, true);
                        $(window.form.select2autocomplete[k]).append(selectOption).trigger('change');
                    }
                }
            }

            $($("#form-order fieldset#fieldset_address")).css('opacity', 1);
            $($("#form-order fieldset#fieldset_address")).css('cursor', 'default');
        }
    });
}

//конец работы с select2autocomplete
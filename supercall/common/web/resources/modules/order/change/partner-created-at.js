$(document).ready(function(){
    try {
        var partner_created_at = $("#partner_created_at_timestamp").text();
        var time_now_with_timezone = $("#time_now").text();
        var time_go_place = $("#time_ago");

        setInterval(function () {
            var diff = parseInt(Date.now()/1000)-partner_created_at;

            var months = Math.floor(diff /30/24/60/60)%30;
            var days = Math.floor(diff /24/60/60)%24;
            var hours =   Math.floor(diff/60/60)%24;
            var mins = Math.floor(diff/60)%60;
            var secs = Math.floor(diff%60);

            var mins = mins < 10 ? '0'+mins : mins;
            var secs = secs < 10 ? '0'+secs : secs;

            if (months > 0) {
                time_go_place.html(months + " month(s) " + days + " day(s) " + hours + ":" + mins + ":" + secs);
            } else if (days > 0) {
                time_go_place.html(days + " day(s) " + hours + ":" + mins + ":" + secs)
            } else {
                time_go_place.html(hours + ":" + mins + ":" + secs)
            }
        }, 1000);
    } catch (e) {
        if (typeof $('#form-order').data() == 'undefined') {
            var order_id = $('#order-online').contents().find('#form-order').data('id');
            var user_id = $('#order-online').contents().find('#form-order').find('#call_history_user_id').val();
        } else {
            var order_id = $('#form-order').data('id');
            var user_id = $('#call_history_user_id').val();
        }

        JSLogger({
            script: 'partner-created-at.js',
            order_id: order_id,
            user_id: user_id
        }, e);
    }

});


$(document).ready(function () {
    try {
        var products = $('#form-order').find('.products .product');
        refreshScripts(products);

        products.map(function () {
            $(this).on('change.scripts.operators', function (e) {
                e.preventDefault();
                refreshScripts(products);
            });
        });

    } catch (e) {
        if (typeof $('#form-order').data() == 'undefined') {
            var order_id = $('#order-online').contents().find('#form-order').data('id');
            var user_id = $('#order-online').contents().find('#form-order').find('#call_history_user_id').val();
        } else {
            var order_id = $('#form-order').data('id');
            var user_id = $('#call_history_user_id').val();
        }

        JSLogger({
            script: 'scripts-operators.js',
            order_id: order_id,
            user_id: user_id,
        }, e);
    }
});

function refreshScripts(products) {

    var product_ids = [];

    products.map(function () {
        var product_id = $(this).find('[name*=product_id]').val();
        if (product_id > 0) {
            $(this).find('[name*=product_id]').prop('id');
            product_ids.push(product_id);
        }
    });

    var data = {
        product_ids: product_ids,
        country_id: $('#form-order').data('country_id'),
        partner_id: $('#form-order').data('partner_id'),
    }

    if (product_ids.length) {

        //нужно определить где мы - дев, мастер, локальный ?
        var domain = location.host.split(/\./)
        if (domain[1] == 'dev') {
            var host = location.protocol + '//' + 'panel.dev.2wcall.com'
        } else if (domain[1] == '2wcall') {
            var host = location.protocol + '//' + 'panel.2wcall.com/'
        } else {
            var host = location.protocol + '//panel.' + $("#document_domain").text();
        }

        var panel = $(".scriptsPanels");
        var header = panel.find('.panel-group');
        var linkToogle = '<a class="collapse-toggle" href="#w0-collapse1" data-toggle="collapse" data-parent="#w0">Operator scripts</a>';

        $.ajax({
            method: 'post',
            dataType: 'jsonp',
            url: host + '/catalog/scripts-operators/get-scripts',
            data: data,
            beforeSend: function () {
                header.find('h4').html('<img width="20px" src="/resources/images/' + (domain[0] == "api" ? "" : "common/") + 'spinner.gif">' + linkToogle);
            },
            error: function () {
                header.find('h4').html(linkToogle);
            },
            success: function (data) {
                var content = JSON.parse(data);

                if (content.lenght == 0) {
                    header.find('h4').html();
                }

                var width = $(".scriptsPanels").find('.panel-group').width();
                var html = '';

                for (var k in content) {
                    html += '<h3>' + content[k].product.name + '</h3><hr/>' + content[k].content + '<br/><br/>';
                }

                $("#form-order").find('.scriptsContent').html(html);

                header.find('h4').html(linkToogle);

                panel.css('height', '60px');
                header.css('position', 'absolute');
                header.css('top', '0px');
                header.css('right', '15px');
                header.css('width', width);
                header.css('z-index', '999999');
            }
        })
    }
}
$(function () {
    var ProductEditPrice = {
        updateUrl: '/order/change/update-order-product',
        sentToNPayUrl: '/order/group-operations/sent-to-sqs',
        orderField: '[name*=order_id]',
        modal: '#transfer_to_pay',
        dataAttr: 'data-type',
        dataAttrType: 'product-price',
        dataAttrEdit: 'data-edit',
        modalPriceEditor: 'modalPriceEditor',
        btnSave: '.btn-save',
        btnCancel: '.btn-cancel',
        btnSentToNpay: '.btn-sent-to-npay',
        divProductClass: '.product',
        productSelect: '[name*=product_id]',
        amountSelect: '[name*=amount]',
        fields: [],
        activeField: null,
        priceDataBlock: '#products-price-data',
        priceDataAttr: 'data-products-price',
        quickEditPriceAttr: 'data-quick-edit-price',
        quickEditPriceOn: "1",
        oldData: {},
        oldAmount: {},
        init: function () {
            if (ProductEditPrice.checkQuickEditPrice()) {
                ProductEditPrice.fields = ProductEditPrice.gitFieldProductPrice();
                ProductEditPrice.customizeFields();
                ProductEditPrice.enabledFieldset();

                $(ProductEditPrice.btnSentToNpay).on('click', ProductEditPrice.eventSentToNpay)
            }
        },
        checkEditablePrice: function (field) {
            return ProductEditPrice.isFixedPrice(
                ProductEditPrice.getProduct(field),
                ProductEditPrice.getPriceData()
            );
        },
        getParentFieldset: function () {
            return $(ProductEditPrice.divProductClass).closest('fieldset');
        },
        enabledFieldset: function () {
            ProductEditPrice.getParentFieldset().removeAttr('disabled');
        },
        checkQuickEditPrice: function () {
            return ProductEditPrice.getParentFieldset().attr(ProductEditPrice.quickEditPriceAttr) === ProductEditPrice.quickEditPriceOn;
        },
        getProduct: function (field) {
            return $(field).closest(ProductEditPrice.divProductClass).find(ProductEditPrice.productSelect);
        },
        getAmount: function (field) {
            return $(field).closest(ProductEditPrice.divProductClass).find(ProductEditPrice.amountSelect);
        },
        getPriceData: function () {
            return JSON.parse($(ProductEditPrice.priceDataBlock).attr(ProductEditPrice.priceDataAttr));
        },
        isFixedPrice: function (product, priceData) {
            //данные о цене товара не найдены в json
            if (isUndefined(priceData[$(product).val()])) {
                return false;
            }
            var price = 0;
            var sum = 0;
            var count = 0;

            for (var k in priceData) {
                if (k === $(product).val()) {
                    for (var j in priceData[k]) {
                        if (!isUndefined(priceData[k][j].price)) {
                            if (price === 0) {
                                price = priceData[k][j].price;
                            }
                            sum += parseInt(priceData[k][j].price);
                            count++;
                        }
                    }
                }
            }

            if (count > 0) {
                if (parseInt(price) === sum / count) {
                    return true;
                } else {
                    toastr.error('Product not having fixed price');
                    return false;
                }
            }

            toastr.error('Product price not found in PriceData');

            return false;
        },
        gitFieldProductPrice: function () {
            return $('input[' + ProductEditPrice.dataAttr + '="' + ProductEditPrice.dataAttrType + '"]').each(function () {
                if ($(this).is('[' + ProductEditPrice.dataAttrEdit + ']')) {
                    ProductEditPrice.fields.push($(this));
                }
            });
        },
        customizeFields: function () {
            $(ProductEditPrice.fields).each(function () {
                ProductEditPrice.setFieldCursor($(this), 'pointer');
                ProductEditPrice.setFieldTitle($(this));
                ProductEditPrice.setEventFocusField($(this));
                ProductEditPrice.setEventClickField($(this));

            });
        },
        setFieldCursor: function (field, cursor) {
            $(field).css({
                'cursor': cursor
            });
        },
        setFieldTitle: function (field) {
            $(field).attr('title', 'Click to edit');
        },
        setEventClickField: function (field) {
            $(field).on('click', function () {
                if (ProductEditPrice.checkEditablePrice($(this))) {
                    var field = $(this);
                    ProductEditPrice.activeField = field;

                    ProductEditPrice.saveOldData($(field).prop('name'), $(field).val());
                    ProductEditPrice.saveOldAmount($(field).prop('name'), ProductEditPrice.getAmount($(field)).val());

                    $(field).closest(ProductEditPrice.divProductClass).find(ProductEditPrice.amountSelect).attr('data-change_price', true);

                    $(field).removeAttr('readonly');
                    ProductEditPrice.setFieldCursor($(field), 'text');
                    ProductEditPrice.setNormal($(field));
                    ProductEditPrice.setBorder($(field));

                    ProductEditPrice.removeButtons();

                    $.when($(field).after(ProductEditPrice.renderButtons())).done(function () {
                        $(field).closest('div').find(ProductEditPrice.btnSave).on('click', ProductEditPrice.eventSave);
                        $(field).closest('div').find(ProductEditPrice.btnCancel).on('click', ProductEditPrice.eventCancel);
                    });
                } else {
                    $(field).prop('readonly', 'readonly');
                }
            });
        },
        setEventFocusField: function (field) {
            $(field).on('mouseover', function () {
                return ProductEditPrice.setBold($(this))
            });

            $(field).on('mouseout', function () {
                return ProductEditPrice.setNormal($(this))
            })
        },
        setOutEventFocusField: function () {
            $(ProductEditPrice.activeField).removeAttr('readonly');
            ProductEditPrice.setFieldCursor($(ProductEditPrice.activeField), 'pointer');
            ProductEditPrice.setNormal($(ProductEditPrice.activeField));
            ProductEditPrice.removeBorder($(ProductEditPrice.activeField));
            ProductEditPrice.removeBtns();
        },
        saveOldData: function (name, value) {
            ProductEditPrice.oldData[name] = value;
        },
        getOldData: function (name) {
            return ProductEditPrice.oldData[name];
        },
        saveOldAmount: function (name, value) {
            ProductEditPrice.oldAmount[name] = value;
        },
        getOldAmount: function (name) {
            return parseInt(ProductEditPrice.oldAmount[name].replace(/[^0-9]/, ''));
        },
        setBold: function (field) {
            $(field).css('font-weight', 'bold');
        },
        setNormal: function (field) {
            $(field).css('font-weight', 'normal');
        },
        setBorder: function (field) {
            $(field).removeClass('form-control').addClass('bordered');
        },
        removeBorder: function (field) {
            $(field).removeClass('bordered').addClass('form-control');
        },
        renderButtons: function () {
            var save = $('<button/>', {
                class: 'btn btn-success btn-save button-fill'
            }).html('<i class="fa fa-save" aria-hidden="true"></i>');

            var cancel = $('<button/>', {
                class: 'btn btn-danger btn-cancel button-fill'
            }).html('<i class="fa fa-repeat" aria-hidden="true"></i>');

            return $('<div/>').append(save).append(cancel);
        },
        removeButtons: function () {
            $(ProductEditPrice.activeField).closest(ProductEditPrice.divProductClass).find(ProductEditPrice.btnSave).remove();
            $(ProductEditPrice.activeField).closest(ProductEditPrice.divProductClass).find(ProductEditPrice.btnCancel).remove();
        },
        eventSave: function () {
            var productSelect = ProductEditPrice.getProductSelect();

            $.when(ProductEditPrice.setPriceData()).done(function () {
                $(ProductEditPrice.activeField).closest(ProductEditPrice.divProductClass).find(ProductEditPrice.productSelect).trigger('change');
                ProductEditPrice.removeButtons();
                ProductEditPrice.removeBorder($(ProductEditPrice.activeField));
                $(ProductEditPrice.activeField).attr('readonly', 'readonly');

                $.when(ProductEditPrice.updatePriceOnServer()).done(function (result) {
                    ProductEditPrice.unmuteProducts();

                    if (result.success) {
                        $(ProductEditPrice.modal).attr('data-order_id', $(ProductEditPrice.orderField).val());
                        $(ProductEditPrice.modal).attr('data-group', (new Date()).getTime());
                        $(ProductEditPrice.modal).modal('show');
                    }
                });
            });

            return false;
        },
        muteProducts: function () {
            $(ProductEditPrice.divProductClass).css('opacity', 0.3);
        },
        unmuteProducts: function () {
            $(ProductEditPrice.divProductClass).css('opacity', 1);
        },
        updatePriceOnServer: function () {
            ProductEditPrice.muteProducts();

            return $.ajax({
                url: ProductEditPrice.updateUrl,
                type: 'post',
                dataType: 'json',
                data: {
                    id: ProductEditPrice.getProduct($(ProductEditPrice.activeField)).attr('data-id'),
                    product_id: ProductEditPrice.getProduct($(ProductEditPrice.activeField)).val(),
                    order_id: $(ProductEditPrice.orderField).val(),
                    price: $(ProductEditPrice.activeField).val()
                },
                success: function (data) {

                },
                error: function (error) {
                    toastr.error(error.responseText);
                }
            });
        },
        eventCancel: function () {
            ProductEditPrice.setOutEventFocusField();
            $(ProductEditPrice.activeField).val(ProductEditPrice.getOldData($(ProductEditPrice.activeField).prop('name')));
            return false;
        },
        generateNewPriceData: function () {
            var priceData = ProductEditPrice.getPriceData();
            var productSelect = ProductEditPrice.getProductSelect();
            var newPrice = [];

            for (var k in priceData[productSelect.val()]) {
                if (typeof (priceData[productSelect.val()][k]) === 'object') {
                    newPrice.push({
                        price: parseInt($(ProductEditPrice.activeField).val()),
                        vat: priceData[productSelect.val()][k].vat
                    });
                } else {
                    newPrice[k] = ProductEditPrice.getOldAmount($(ProductEditPrice.activeField).prop('name'));
                }
            }
            priceData[productSelect.val()] = newPrice;
            $_productsPriceData = priceData;

            return JSON.stringify(priceData);
        },
        setPriceData: function () {
            return $(ProductEditPrice.priceDataBlock).prop(ProductEditPrice.priceDataAttr, ProductEditPrice.generateNewPriceData())
        },
        removeBtns: function () {
            $(ProductEditPrice.divProductClass).find(ProductEditPrice.btnSave).remove();
            $(ProductEditPrice.divProductClass).find(ProductEditPrice.btnCancel).remove();
        },
        getProductSelect: function () {
            return $(ProductEditPrice.activeField).closest(ProductEditPrice.divProductClass).find(ProductEditPrice.productSelect);
        },
        eventSentToNpay: function () {
            var order_id = $(ProductEditPrice.modal).attr('data-order_id');
            var group = $(ProductEditPrice.modal).attr('data-group');

            $.ajax({
                url: ProductEditPrice.sentToNPayUrl,
                type: 'post',
                dataType: 'json',
                data: {
                    id: order_id,
                    group: group
                },
                success: function (data) {
                    if (data.success) {
                        $(ProductEditPrice.modal).modal('hide');
                        toastr.success('The order has been sent');
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (error) {
                    toastr.error(error.responseTexts);
                }
            });
        },
    };

    ProductEditPrice.init();
});
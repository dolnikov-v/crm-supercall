$(document).ready(function () {
    //window.parent.statusReady = true;
    try {
        parent.duration = 0;
        window.action_address_time = false;

        //action_address_time SC158
        //for inputs
        $("#fieldset_address").find('input').map(function () {
            $(this).on('keydown', function () {
                setActionAddressTime($(this));
            });
        });
        //for select
        $("#fieldset_address").find('select').map(function () {
            $(this).on('change', function () {
                setActionAddressTime($(this));
            });
        });

        //for comment texarea
        $("#fieldset_address").find('textarea').map(function () {
            $(this).on('keydown', function () {
                setActionAddressTime($(this));
            });
        });

        //operator-activity reset
        $('body').on('keydown', function () {
            if (typeof parent.ActivityOperator !== 'undefined' && typeof parent.ActivityOperator.timer !== 'undefined') {
                parent.ActivityOperator.timer = 0;
            }
        });

        findByParent("#call_btn").on('click.callhistory.action', function () {
            reset_button = getResetButton();
            $(reset_button).prop('disabled', true);
            blockedHangUpButton(getResetButton());
            enableStatusButton();
            //sendOnServerStartCallHistory(); вызыв про прослушивании pluginAddonCaller 79 строка
        });

        $("#call_btn").on('click.callhistory.action', function () {
            reset_button = getResetButton();
            $(reset_button).prop('disabled', true);
            blockedHangUpButton(getResetButton());
            enableStatusButton();
            //sendOnServerStartCallHistory();  вызыв про прослушивании pluginAddonCaller 79 строка
        });

        console.log('initialize plugin caller buttons');

        var pluginAddonCaller = isUndefined($('span.input-group-addon')) ? $('#order-online').contents().find('span.input-group-addon') : $("span.input-group-addon");

        // pluginAddonCaller.map(function () {
        //
        //     $(this).css('background-color', 'red');
        //     $(this).find('a').css('visibility', 'hidden');
        //     $(this).find('a').css('text-decoration', 'none');
        //
        //     $(this).on('click.callhistory.action', function (e) {
        //
        //         e.preventDefault();
        //         e.stopImmediatePropagation();
        //         var phone_number = isUndefined($("#phoneNumper_dial", parent.document).prop('type')) ? $("#phoneNumper_dial") : findByParent("#phoneNumper_dial");
        //         phone_number.val($(this).next('input').val());
        //
        //         var call_id = "#call_btn";
        //         var call_btn = isUndefined($(call_id).prop('type')) ? findByParent(call_id) : $(call_id);
        //
        //         if (typeof iframeZoiper === 'undefined') {
        //             RingHang(call_btn, 'Hang', $(this));
        //         } else {
        //             RingHang(call_btn, "Hang", $(this));
        //         }
        //     });
        // });

        pluginAddonCaller.map(function () {

            $(this).css('background-color', 'red');
            $(this).find('a').css('visibility', 'hidden');
            $(this).find('a').css('text-decoration', 'none');

            $(this).on('click.callhistory.action', function (e) {
                var addon = $(this);

                if (addon.find('a').css('visibility') === 'hidden') {
                    console.log('dont load icon');
                    return;
                }

                e.preventDefault();
                e.stopImmediatePropagation();
                var phone_number = isUndefined($("#phoneNumper_dial", parent.document).prop('type')) ? $("#phoneNumper_dial") : findByParent("#phoneNumper_dial");
                phone_number.val($(this).next('input').val());

                var call_id = "#call_btn";
                var call_btn = isUndefined($(call_id).prop('type')) ? findByParent(call_id) : $(call_id);

                if ($(call_btn).hasClass('active')) {
                    eventRingHang(call_btn, 'Hang', addon);
                } else {
                    $.when(sendOnServerStartCallHistory()).done(function () {
                        eventRingHang(call_btn, 'Hang', addon);
                    });
                }
            });
        });

        function eventRingHang(call_btn, callback, icon) {
            RingHang(call_btn, callback, icon);
        }

        //функция звонит/збрасывает по нажатию на иконку plugin около номера телефона
        //первое нажатие - начать звонок, второе - закончить звонок
        //зависит от inhibitToReset() - например 15 секунд задержка для второго нажатия (для окончания разговора)
        //call_btn - кнопка звонка на интерфейсе
        //callback - функция - отрабатывающая по завершению звонка
        //icon - сам plugin елемент
        function RingHang(call_btn, callback, icon) {
            var iframeZoiper = window.parent.frames.iframeZoiper;

            if ($(call_btn).hasClass('active')) {
                if (inhibitToReset()) {
                    $(icon).closest("div").find('input').css('background-color', 'white');
                    $(call_btn).removeClass('active');

                    console.log('inhibitToReset is ready, reset call');
                    if (typeof callback === 'function') {
                        callback();
                    } else {
                        if (typeof iframeZoiper === 'undefined') {
                            toParent(callback, []);
                        } else {
                            IframeZoiper(callback)
                        }
                    }

                    sendServerUpdateCallHistory();
                    window.parent.$('#reset_btn').trigger('click');
                } else {
                    console.log('inhibitToReset not ready')
                }
            } else {
                if($(icon).closest("div").find('input').val() == ''){
                    return false;
                }

                $(call_btn).trigger('click');
                $(icon).closest("div").find('input').css('background-color', '#B1DAA1');
                $(call_btn).addClass('active');
                window.parent.$('#reset_btn').prop('disabled', true);
                parent.duration_ring = 0;
            }
        }

        //https://2wtrade-tasks.atlassian.net/browse/WCALL-9   2я часть с дайлера в
        //Zoiper resource/widjets/navbar/zoiper-slide-style/bootsrap_caller.js
        //SipML5 resource/widjets/navbar/sipml5-slide-style/sipml5core.js
        function inhibitToReset() {
            //console.log('parent.duration > parent.DURATION', parent.duration, parent.DURATION, parent.duration > parent.DURATION)
            //если разговор начался
            if (parent.duration > 0 && parent.DURATION) {
                return parent.duration > parent.DURATION;
            } else {
                console.log('parent.duration_ring && parent.DURATION_RING', parent.duration_ring, parent.DURATION_RING, parent.duration_ring > parent.DURATION_RING)
                //если идёт дозвон
                if (parent.duration_ring && parent.DURATION_RING) {
                    return parent.duration_ring > parent.DURATION_RING;
                }
            }
        }

        function IframeZoiper(command) {
            window.parent.frames.iframeZoiper.postMessage(command, "*");
        }

        //теперь кроме Zoiper есть SipML5
        function toParent(command, params) {
            window.parent.ListenerEvents(command, params);
        }

        var reset_button_elem = $("#reset_btn");
        var reset_button = !isUndefined(reset_button_elem.prop('type')) ? reset_button_elem : findByParent("#reset_btn");

        console.log('reset_button', $(reset_button).prop('type'));

        console.log('EVENT on reset_btn for end call_history');
        reset_button.on('click.endcallhistory', function (event) {
            event.preventDefault();
            endCallHistory();
        });


        function getCallHistoryId() {
            if (isUndefined($('#call_history_id').val())) {
                return findByIframe("#call_history_id").val();
            } else {
                return $("#call_history_id").val();
            }
        }

        function setCallHistoryId(value) {
            if (isUndefined($('#call_history_id').val())) {
                findByIframe("#call_history_id").val(value).trigger('change');
            } else {
                $("#call_history_id").val(value).trigger('change');
            }
        }

        //update CallHistory в ChangeController - при сохранении заказа
        function endCallHistory() {
            if (getCallHistoryId() != '' && typeof $("#ch_end_time").val() == 'undefined') {
                var duration = parent.duration;

                var fieldset_address_elem = $("#fieldset_address");
                var container = !isUndefined(fieldset_address_elem.html()) ? fieldset_address_elem : findByIframe("#fieldset_address");

                var inputEndTime = document.createElement("input");
                inputEndTime.type = "hidden";
                inputEndTime.id = "ch_end_time";
                inputEndTime.name = "CallHistory[end_time]";
                inputEndTime.value = !isUndefined($(".atimer-auto-init").text()) ? findByParent(".atimer-auto-init").text() : (".atimer-auto-init").text();
                container.append(inputEndTime);

                var inputDuration = document.createElement("input");
                inputDuration.type = "hidden";
                inputDuration.id = "ch_duration";
                inputDuration.name = "CallHistory[duration]";
                inputDuration.value = duration;
                container.append(inputDuration);

                var inputCallHistoryId = document.createElement("input");
                inputCallHistoryId.type = "hidden";
                inputCallHistoryId.id = "ch_id";
                inputCallHistoryId.name = "CallHistory[id]";
                inputCallHistoryId.value = getCallHistoryId();
                container.append(inputCallHistoryId);

                var inputEndStatus = document.createElement("input");
                inputEndStatus.type = "hidden";
                inputEndStatus.id = "ch_end_status";
                inputEndStatus.name = "CallHistory[end_status]";
                inputEndStatus.value = 0; //запишем при смене статуса
                container.append(inputEndStatus);
            }

            sendServerUpdateCallHistory();
        }


        $(".btn-status").on('click', function () {
            if (isUndefined($('#ch_end_status').val())) {
                findByIframe("#ch_end_status").val($(this).attr("data-status"));
            } else {
                $("#ch_end_status").val($(this).attr("data-status"));
            }
        });

        function sendOnServerStartCallHistory() {
            var duration = parent.duration;
            var start_time = findByParent(".atimer-auto-init").text();

            var isIframe = isUndefined($("#call_history_user_id").val());

            var user_id = isIframe ? findByIframe("#call_history_user_id").val() : $("#call_history_user_id").val();
            var order_id = isIframe ? findByIframe("#call_history_order_id").val() : $("#call_history_order_id").val();
            var start_status = isIframe ? findByIframe("#call_history_start_status").val() : $("#call_history_start_status").val();
            var phone_number = isUndefined($("#phoneNumper_dial", parent.document).prop('type')) ? $("#phoneNumper_dial") : findByParent("#phoneNumper_dial");
            var user_sip = isIframe ? findByIframe("#call_history_user_sip").val() : $("#call_history_user_sip").val();
            var group_id = (isIframe ? findByIframe('#call_history_group_id') : $('#call_history_group_id')).val();

            // $.post(
            //     "/call/call-history/insert-data-on-start-call",
            //     {
            //         user_id: user_id,
            //         user_sip: user_sip,
            //         order_id: order_id,
            //         start_status: start_status,
            //         start_time: start_time,
            //         phone: phone_number.val().replace(/[^0-9]/g, ''),
            //         group_id: group_id
            //     }
            // ).done(function (data) {
            //     var response = jQuery.parseJSON(data);
            //
            //     parent.duration = 0;
            //
            //     if (response.result) {
            //         setCallHistoryId(response.id);
            //     } else {
            //         setCallHistoryId(null);
            //     }
            // });

            return $.ajax({
                url: '/call/call-history/insert-data-on-start-call',
                type: 'post',
                dataType: 'json',
                data: {
                    user_id: user_id,
                    user_sip: user_sip,
                    order_id: order_id,
                    start_status: start_status,
                    start_time: start_time,
                    phone: phone_number.val().replace(/[^0-9]/g, ''),
                    group_id: group_id
                },
                success: function (data) {
                    parent.duration = 0;

                    if (data.result) {
                        setCallHistoryId(data.id);
                    } else {
                        toastr.error(data.error);
                        setCallHistoryId(null);
                    }


                },
                error: function (error) {
                    parent.duration = 0;
                    toastr.error(error.responseText);
                    setCallHistoryId(null);
                }
            });

        }

        window.sendOnServerStartCallHistory = function(){
            return sendOnServerStartCallHistory();
        };

        function sendServerUpdateCallHistory() {
            var duration = parent.duration;
            var end_time = findByParent(".atimer-auto-init").text();
            var isIframe = isUndefined($("#call_history_user_id").val());
            var user_id = isIframe ? findByIframe("#call_history_user_id").val() : $("#call_history_user_id").val();
            var order_id = isIframe ? findByIframe("#call_history_order_id").val() : $("#call_history_order_id").val();
            var call_history_id = isIframe ? findByIframe("#call_history_id").val() : $("#call_history_id").val();
            var phone_number = isIframe ? findByIframe("[name*=customer_phone]").val() : $("[name*=customer_phone]").val();
            var user_sip = isIframe ? findByIframe("#call_history_user_sip").val() : $("#call_history_user_sip").val();
            var group_id = (isIframe ? findByIframe('#call_history_group_id') : $('#call_history_group_id')).val();
            var additional_params = !isUndefined($('[name*=form-additional-params]')) ? $('[name*=form-additional-params]').val() : JSON.stringify({})

            // $.post(
            //     "/call/call-history/update-data-on-call",
            //     {
            //         user_id: user_id,
            //         order_id: order_id,
            //         end_time: end_time,
            //         duration: duration,
            //         user_sip: user_sip,
            //         phone: phone_number,
            //         group_id: group_id
            //     }
            // ).done(function (data) {
            //     try {
            //         var response = jQuery.parseJSON(data);
            //         //console.log('response', response);
            //     } catch (err) {
            //         console.error(err);
            //     }
            // });

            return $.ajax({
                url: '/call/call-history/update-data-on-call',
                type: 'post',
                dataType: 'json',
                data: {
                    user_id: user_id,
                    order_id: order_id,
                    end_time: end_time,
                    duration: duration,
                    user_sip: user_sip,
                    phone: phone_number,
                    group_id: group_id,
                    additional_params: additional_params
                },
                success: function(data){
                    console.log('DATA', data);
                    if(!data.result){
                        toastr.error(data.error);
                    }else{
                        $('#call_history_id').val(data.call_history_id)
                    }
                },
                error: function(error){
                    toastr.error(error.responseText);
                }
            });
        }


        function findByIframe(selector) {
            return $('#order-online').contents().find(selector);
        }


        setInterval(function () {
            $("span.input-group-addon").map(function () {
                if (parent.statusReady === true) {
                    $(this).css('background-color', '#E1E1E1');
                    $(this).find('a').css('visibility', 'visible');
                } else {
                    $(this).css('background-color', '#EE8C8C');
                    $(this).find('a').css('visibility', 'hidden');
                }

                var call_id = "#call_btn";
                var call_btn = isUndefined($(call_id).prop('type')) ? findByParent(call_id) : $(call_id);

                if (!call_btn.hasClass('active')) {
                    $(this).closest('div').find('input').css('background-color', 'white');
                }
            });
        }, 1000);

    } catch (e) {
        var order_id, user_id;
        var form = $('#form-order');

        if (typeof form.data() === 'undefined') {
            var iframe = $('#order-online');
            order_id = iframe.contents().find('#form-order').data('id');
            user_id = iframe.contents().find('#form-order').find('#call_history_user_id').val();
        } else {
            order_id = form.data('id');
            user_id = $('#call_history_user_id').val();
        }

        JSLogger({
            script: 'call-history.js',
            order_id: order_id,
            user_id: user_id
        }, e);
    }
});


function findByParent(selector) {
    return $(selector, parent.document);
}

function isUndefined(elem) {
    return typeof elem == 'undefined';
}

function enableStatusButton() {
    $('.after_call').removeAttr('disabled');
}

function blockedHangUpButton(el) {
    setTimeout(function (el) {
        $(getResetButton()).removeAttr('disabled');
    }, 30000);
}

function getResetButton() {
    var reset_button_elem = $("#reset_btn");
    var reset_button = !isUndefined(reset_button_elem.prop('type')) ? reset_button_elem : findByParent("#reset_btn");
    return reset_button;
}

/**
 * Записать начало ввода адреса операторам в сек, после начала разговора
 * @param elem
 */
function setActionAddressTime(elem) {
    console.log('window.action_address_time', window.action_address_time);
    console.log('parent.duration', parent.duration);

    if (window.action_address_time === false && parent.duration > 0) {
        $.ajax({
            method: 'post',
            url: $("#url_backend", window.parent.document).text() + '/call/call-history/add-action-address-time',
            data: {
                action_adress_time: parent.duration,
                call_history_id: $("[name*=call_history_id]").val()
            },
            success: function (data) {
                var response = jQuery.parseJSON(data);
                window.action_address_time = true;
            }
        });
    }
}


$(function () {
    var Transcribe = {
        modal_price: '#transcribe_price_modal',
        btn_price_ok: '#transcribe_price_ok',
        price_total_block: '.transcribe_price_total',
        attr_price_total: 'data-transcribe_price_total',
        button: '.get-transcribe-data',
        url: '/order/transcribe-queue/set-in-queue',
        fieldset: '#record-list',
        init: function () {
            $(Transcribe.button).on('click', function () {
                $(Transcribe.price_total_block).text('$' + $(this).attr(Transcribe.attr_price_total));
                $(Transcribe.modal_price).attr('data-call_history_id', $(this).attr('data-call_history_id'));
                Transcribe.showModalPrice();
            });

            $(Transcribe.btn_price_ok).on('click', function () {
                Transcribe.sentToQueue($(Transcribe.modal_price).attr('data-call_history_id'));
                Transcribe.setDisabled($(Transcribe.modal_price).attr('data-call_history_id'));
            });
        },

        showModalPrice: function () {
            $(Transcribe.modal_price).modal('show');
        },
        hideModalPrice: function () {
            $(Transcribe.modal_price).modal('hide');
        },
        setDisabled: function (id) {
            $('#transcribe-button-' + id).prop('disabled', true);
        },
        setEnabled: function (id) {
            $('#transcribe-button-' + id).prop('disabled', false);
        },
        sentToQueue: function (call_history_id) {
            $.ajax({
                url: Transcribe.url,
                dataType: 'json',
                type: 'post',
                data: {
                    call_history_id: call_history_id
                },
                success: function (data) {
                    Transcribe.setEnabled();
                    $('#transcribe-button-' + call_history_id).remove();

                    var className = data.success ? 'text-warning' : 'text-danger';
                    className = data.transcribe ? '' : className;

                    $('<div/>', {
                        class: className,
                        text: data.message
                    }).appendTo($(Transcribe.fieldset + '-' + call_history_id));
                },
                error: function (error) {
                    Transcribe.setEnabled(call_history_id);
                    toastr.error(error.responseText);
                }
            });
        }
    };

    Transcribe.init();
});
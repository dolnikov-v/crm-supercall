$(function () {
    TablePrice.init();
    TablePrice.changeInput();

    $("#price_data_type").change(function(){
        TablePrice.changeInput();
    });

});

var TablePrice = {
    init: function () {
        $('.btn-table-price-plus').off('click');
        $('.btn-table-price-minus').off('click');

        $('.btn-table-price-plus').on('click', TablePrice.addInput);
        $('.btn-table-price-minus').on('click', TablePrice.removeInput);

        if ($('.group_table_price .row').find('.btn-table-price-minus').size() == 1) {
            $('.btn-table-price-minus').hide();
        } else {
            $('.btn-table-price-minus').show();
            $('.btn-table-price-plus').hide();
            $('.btn-table-price-minus').last().hide();
            $('.btn-table-price-plus').last().show();
        }

    },
    addInput: function () {
        $('.group_table_price .row').last().clone().appendTo('.group_table_price');
        $('.group_table_price .row').last().find('span').remove();
        $('.group_table_price .row').last().find('select').select2();

        $('.select2-container ').css('width','238px');

        TablePrice.init();
    },
    removeInput: function () {
        $(this).parents().each(function () {
            if ($(this).hasClass('table_price')) {
                $(this).remove();
            }
        });

        TablePrice.init();
    },
    changeInput: function () {
        if($("#price_data_type").val() == 1){
            $("#price").hide();
            $("#group_table_price, h5").removeClass('hidden');
        }else{
            $("#price").show();
            $("#group_table_price, h5").addClass('hidden');
        }
    }
};

$(function () {
    window.getPartnerProduct = false;

    var parent = $('.package-gifts');
    var main = $('.main_data');

    var PackageGiftManager = {
        parent: parent,
        rows: '.package-gift',
        btn_add: '.add-package-gift',
        btn_remove: '.remove-package-gift',
        fieldset: '.gift_place',
        spinner: '.load',
        alert: '.empty-partner-product',
        init: function () {
            PackageGiftManager.disableGift();
            //PackageGiftManager.setName();
            PackageGiftManager.lookName()

            if ($(PackageGiftManager.rows).length === 1) {
                $(PackageGiftManager.btn_remove).hide();
            } else {
                $(PackageGiftManager.btn_add).hide();
                $(PackageGiftManager.btn_remove).show();
            }
            $(PackageGiftManager.btn_add).last().show();
            $(PackageGiftManager.btn_remove).last().hide();

            $(PackageGiftManager.btn_add).off('click');
            $(PackageGiftManager.btn_add).on('click', PackageGiftManager.add);

            $(PackageGiftManager.btn_remove).on('click', function () {
                PackageGiftManager.remove($(this));
            });
        },
        add: function () {
            var row = $(PackageGiftManager.parent.find(PackageGiftManager.rows)).last().clone();
            row.find('span').remove();
            row.find('select').select2().val('').trigger('change');
            row.appendTo($(PackageGiftManager.parent));

            PackageGiftManager.init();
        },
        remove: function (btn) {
            $(btn).closest(PackageGiftManager.rows).remove();
            PackageGiftManager.init();
        },
        disableGift: function () {
            var disabled = main.find('[name*=product_id]').val() === '';

            $(PackageGiftManager.btn_add).prop('disabled', disabled);
            $(PackageGiftManager.fieldset).find('select').prop('disabled', disabled);
            $(PackageGiftManager.fieldset).prop('disabled', disabled);
        },
        setName: function () {
            var prefix = main.find('[name*=prefix]').val();
            var paid_amount = main.find('[name*=paid_amount]').val();
            var product = main.find('[name*=product_id]').find('option:selected').text();
            var product_val = main.find('[name*=product_id]').val();

            if (product_val === '') {
                return;
            }

            var name = prefix
                + ' ' + product
                + ' ' + paid_amount;

            var name_from_gifts = '';

            parent.find(PackageGiftManager.rows).map(function () {
                var product_gift = $(this).find('select').find('option:selected').text();
                var product_gift_val = $(this).find('select').val();
                var count = $(this).find('input').val();

                if (product_gift_val !== '') {
                    name_from_gifts += ' + ' + product_gift + ' ' + count;
                }


            });

            name_from_gifts = name_from_gifts.length > 0 ? ' (' + name_from_gifts + ')' : name_from_gifts;

            $('[name="Package[name]"]').val(name + name_from_gifts);


        },
        lookName: function () {
            $(parent.closest('form')).find('select').on('change paste keyup', PackageGiftManager.disableGift);

            $(main.find('select')).on('paste keyup change', PackageGiftManager.setName);
            $(main.find('input')).on('keyup', PackageGiftManager.setName);

            $(parent.find('select')).on('change', PackageGiftManager.setName);
            $(parent.find('input')).on('paste keyup change', PackageGiftManager.setName);
            $(PackageGiftManager.btn_remove).on('click', PackageGiftManager.setName);
        },
        manageProducts: function () {

            window.getPartnerProduct = false;

            PackageGiftManager.spinnerShow();
            PackageGiftManager.alertHide();

            //clear products
            parent.find($(PackageGiftManager.rows).not(':first')).remove();
            parent.find($(PackageGiftManager.rows)).first().hide();
            //set default
            parent.find($(PackageGiftManager.rows)).first().find('input').val(1);
            parent.find($(PackageGiftManager.rows)).first().find('select').find('option').remove();

            window.getPartnerProductInterval = setInterval(function(){
                if(window.getPartnerProduct){
                    var row = parent.find($(PackageGiftManager.rows)).first();
                    var select_gift = row.find('select');
                    var options = $('[name="Package[product_id]"]').find('option');

                    options.map(function(){
                        var option = new Option($(this).text(), $(this).val())
                        console.log('-->', $(this).text(), $(this).val());
                        select_gift.append(option);
                    });

                    //empty option
                    if(options.length === 1){
                        PackageGiftManager.alertShow();
                    }else{
                        select_gift.val('');
                        select_gift.trigger('change');
                        row.show();
                    }

                    PackageGiftManager.init();
                    PackageGiftManager.stopGettingPartnerProducts();
                    PackageGiftManager.spinnerHide();


                    window.getPartnerProduct = false;

                    main.find('[name="Package[name]"]').val('');
                }
            }, 300);
        },
        stopGettingPartnerProducts: function(){
            clearInterval(window.getPartnerProductInterval);
        },
        spinnerShow: function(){
            $(PackageGiftManager.spinner).removeClass('hidden');
            $(PackageGiftManager.spinner).show();
        },
        spinnerHide: function(){
            $(PackageGiftManager.spinner).hide();
        },
        alertShow: function(){
            $(PackageGiftManager.alert).removeClass('hidden');
            $(PackageGiftManager.alert).show();
        },
        alertHide: function(){
            $(PackageGiftManager.alert).hide();
        }
    };

    PackageGiftManager.init();

    $('[name="Package[country_id]"]').on('change', PackageGiftManager.manageProducts);
});

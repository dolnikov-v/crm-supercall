$(document).ready(function () {
    var partner = $('[name="Package[partner_id]"]');
    var country = $('[name="Package[country_id]"]');
    var product = $('[name="Package[product_id]"]');
    var paid = $('[name="Package[paid_amount]"]');
    var free = $('[name="Package[free_amount]"]');

    if(partner.val() === '') {
        country.prop('disabled', true);
    }

    if(country.val() === '') {
        product.prop('disabled', true);
    }

    partner.change(function () {
        addSelectedEmptyOption(country);
        addSelectedEmptyOption(product);
        country.prop('disabled', true);
        $('[name="Package[name]"]').val('');
        product.prop('disabled', true);
        paid.val(1);

        if ($(this).val() !== '-') {
            $.ajax({
                url: '/partner/package/get-partner-countries',
                type: "POST",
                async: true,
                data: {'partner_id': partner.val()},
                complete: function () {

                },
                success: function (response) {
                    addSelectedEmptyOption(country);
                    addSelectedEmptyOption(product);

                    if(Object.keys(response).length === 0){
                        country.prop('disabled', true);
                        product.prop('disabled', true);
                        return;
                    }

                    for (id in response) {
                        country.append(new Option(response[id], id));
                    }
                    country.prop('disabled', false);
                },
                dataType: 'json'
            });
        }
    });

    country.change(function () {
        $('[name="Package[name]"]').val('');
        addSelectedEmptyOption(product);
        product.prop('disabled', true);
        paid.val(1);

        if ($(this).val() !== '-') {
            $.ajax({
                url: '/partner/package/get-partner-products',
                type: "POST",
                async: true,
                data: {'country_id': country.val(), 'partner_id': partner.val()},
                complete: function () {

                },
                success: function (response) {
                    addSelectedEmptyOption(product);

                    if(Object.keys(response).length === 0){
                        product.prop('disabled', true);
                        //взаимодействие с товарами в partner-gift.js
                        window.getPartnerProduct = true;
                        return;
                    }

                    for (id in response) {
                        product.append(new Option(response[id], id));
                    }
                    product.prop('disabled', false);
                    //взаимодействие с товарами в partner-gift.js
                    window.getPartnerProduct = true;
                },
                dataType: 'json'
            });
        }
    });


});

function addSelectedEmptyOption(select) {
    select.empty();
    select.append(new Option('-', '')).val('');
    select.select2();
}

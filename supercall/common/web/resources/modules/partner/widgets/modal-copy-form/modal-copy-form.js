$(function () {
    $(document).on('click', '.copy-form-link', ModalCopyForm.copyForm);
    if ($('#modal_copy_form').hasClass('visible')){
        $('#modal_copy_form').modal();
    }
});

var ModalCopyForm = {
    copyForm: function () {
        $('#partnerformcopy-form').val($(this).data('id'));
        $('#modal_copy_form').modal();

        return false;
    }
};

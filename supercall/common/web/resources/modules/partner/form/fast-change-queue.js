$(document).ready(function () {
    $('#save_fast_change_queue').on('click', function () {
        $.ajax({
            method: 'post',
            url: '/partner/form/fast-change-queue',
            dataType: 'json',
            data: $("#partner-form-fast-change-queue").serialize(),
            success: function (result) {
                if (result.success) {
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                }
            }
        })
    });

    $(".btn-fast-change-queue").on('click', function () {
        $("#modal-fast-change-status").modal('show');
    });

    $('.btn-fast-change-queue-save').on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/order/change/fast-change-queue',
            data: $("#partner-form-fast-change-queue").serialize(),
            dataType: "JSON",
            success: function (response) {
                if (response.success) {
                    if (typeof(parent.getOrderOnline()) === "function") {
                        parent.getOrderOnline();
                    }
                    $("#modal-fast-change-status").modal('hide');
                }
            }
        })
    });

});





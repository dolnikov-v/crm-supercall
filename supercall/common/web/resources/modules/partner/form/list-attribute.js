$(document).ready(function () {
    $("[name*=type]").on('change', function () {
        ListManagment.init();

        if ($(this).val() == 'list') {
            $('.list-values').removeClass('hidden');
        } else {
            $('.list-values').addClass('hidden');
            var row = $('.list-value').last().clone();
            row.find('.add-value').on('click', function () {
                ListManagment.addValue();
            });
            $('.list-value').remove();
            row.appendTo($('.list-values'));
            $("[name*=values]").val('');
        }
    });

    $('[name*=is_klader]').on('change', function() {
        ListManagment.changeCheckbox();
    });

    $('.add-value').on('click', function () {
        ListManagment.addValue();

    });

    $('.remove-value').on('click', function () {
        ListManagment.removeValue($(this));

    });

    var ListManagment = {
        values: $('.list-values input'),
        addValue: function () {
            var row = $('.list-value').last().clone();

            row.find('.add-value').on('click', function () {
                ListManagment.addValue();
            });

            row.appendTo($('.list-values'));
            $('.remove-value').show();
            $('.add-value').hide();

            $('.remove-value').on('click', function () {
                ListManagment.removeValue($(this));
            });

            $('.list-value').last().find('.remove-value').hide();
            $('.list-value').last().find('.add-value').show();
        },
        removeValue: function (button) {
            $(button).closest('.list-value').remove();
        },
        init: function () {
            console.log(this.values);

            if (this.values.length == 1) {
                $('.remove-value').hide();
            }else{
                $('.list-value').find('.add-value').hide();
                $('.list-value').last().find('.add-value').show();
                $('.list-value').last().find('.remove-value').hide();
            }
        },
        changeCheckbox: function () {
            if($('[name*=is_klader]').is(':checked')) {
                $('[name*=is_disabled_user_data]').removeAttr("disabled");
            }else{
                $('[name*=is_disabled_user_data]').attr('disabled', 'disabled');
                $('[name*=is_disabled_user_data]').prop('checked', false);
            }
        },
    }

    ListManagment.init();
    ListManagment.changeCheckbox();

});
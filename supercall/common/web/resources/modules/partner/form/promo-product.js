$(function () {
    var PromoProduct = {
        urlUpdatePartnerProducts: '/catalog/product/get-products-by-partner',
        urlSavePromoProducts: '/partner/promo-product/save',
        form: '#PromoProductForm',//'[name="PromoProduct[form_id]"]',
        partner: '[name="PartnerForm[partner_id]"]',
        country: '[name="PartnerForm[country_id]"]',
        products: '[name="product[]"]',//'[name="PromoProduct[product_id][]"]',
        price: '[name="price[]"]',//'[name="PromoProduct[price][]"]',
        btnSave: '#promo-product-save',
        bodyPanel: '.panel-body',
        btnUseAll: '#use-all-products',
        modal: '#promo-product-clear',
        textModal: '.text-promo-change',
        typeChangeCountry: 'country',
        typeChangePartner: 'partner',
        rows: '.promo-goods',
        row: '.promo-good',
        btnAdd: '.add-promo-product',
        btnRemove: '.remove-promo-product',
        init: function () {
            PromoProduct.eventChangePartner();
            PromoProduct.eventChangeCountry();
            PromoProduct.eventSavePromoProducts();
            PromoProduct.eventUseAll();
            PromoProduct.eventRemove();
            PromoProduct.eventAdd();
            PromoProduct.manageButtons();
        },
        showModal: function(type){
            $(PromoProduct.textModal).find('span').html(type);
            $(PromoProduct.modal).modal('show');
        },
        eventChangePartner: function () {
            $(PromoProduct.partner).on('change', PromoProduct.updateListProducts);
        },
        eventChangeCountry: function () {
            $(PromoProduct.country).on('change', PromoProduct.updateListProducts);
        },
        eventUseAll: function () {
            $(PromoProduct.btnUseAll).css({'cursor': 'pointer'});

            $(PromoProduct.btnUseAll).on('change', function () {
                if ($(this).prop('checked')) {
                    var products = [];

                    $(PromoProduct.products).find('option').each(function () {
                        products.push($(this).val());
                    });

                    $(PromoProduct.products).val(products);
                } else {
                    $(PromoProduct.products).val('');
                }

                $(PromoProduct.products).trigger('change');
            });
        },
        updateListProducts: function () {
            PromoProduct.showLoader();
            PromoProduct.resetProducts();
            PromoProduct.resetPrice();

            if($(PromoProduct.country).val() !== $(PromoProduct.modal).attr('data-country_id')) {
                PromoProduct.showModal('country');
            }

            if($(PromoProduct.partner).val() !== $(PromoProduct.modal).attr('data-partner_id')) {
                PromoProduct.showModal('partner');
            }

            $.ajax({
                url: PromoProduct.urlUpdatePartnerProducts,
                type: 'post',
                dataType: 'json',
                data: {
                    partner_id: $(PromoProduct.partner).val(),
                    country_id: $(PromoProduct.country).val()
                },
                success: function (data) {
                    PromoProduct.clearProductsSelect();
                    PromoProduct.addOptionProductsSelect(data.message);
                    PromoProduct.hideLoader();
                },
                error: function (error) {
                    toastr.error(error.responseText);
                    PromoProduct.hideLoader();
                }
            });
        },
        savePromoProducts: function () {
            PromoProduct.showLoader();

            /*if (!$(PromoProduct.products).val()) {
                toastr.error('You must specify a promotional product.');
                PromoProduct.addError(PromoProduct.products);
                PromoProduct.hideLoader();
            } else if (parseInt($(PromoProduct.price).val()) === 0 || parseInt($(PromoProduct.price).val()) === 'NaN') {
                toastr.error('You must specify a promotional price.');
                PromoProduct.addError(PromoProduct.price);
                PromoProduct.removeError(PromoProduct.products);
                PromoProduct.hideLoader();
            } else {*/
                //PromoProduct.removeError(PromoProduct.price);
                /*data:{
                    partner_product_id: $(PromoProduct.products).val(),
                        price: $(PromoProduct.price).val(),
                    form_id: $(PromoProduct.form).val()
                },*/
                PromoProduct.hideLoader();
                $.ajax({
                    url: PromoProduct.urlSavePromoProducts,
                    type: 'post',
                    dataType: 'json',
                    data: $(PromoProduct.form).serialize(),
                    success: function (data) {
                        PromoProduct.hideLoader();

                        if (data.success) {
                            toastr.success(data.message);
                        } else {
                            toastr.error(data.message)
                        }
                    },
                    error: function (error) {
                        toastr.error(error.responseText);
                        PromoProduct.hideLoader();
                    }
                });
            //}
        },
        eventSavePromoProducts: function () {
            $(PromoProduct.btnSave).on('click', PromoProduct.savePromoProducts);
        },
        addError: function (elem) {
            $(elem).closest('.form-group').closest('div').addClass('has-error');
        },
        removeError: function (elem) {
            $(elem).closest('.form-group').closest('div').removeClass('has-error');
        },
        showLoader: function () {
            PromoProduct.disableSave();
            PromoProduct.disableProducts();
            PromoProduct.disablePrice();
            PromoProduct.mutePanelBody();
        },
        hideLoader: function () {
            PromoProduct.enableSave();
            PromoProduct.enableProducts();
            PromoProduct.enablePrice();
            PromoProduct.unmutePanelBody();
        },
        mutePanelBody: function () {
            $(PromoProduct.btnSave).closest(PromoProduct.bodyPanel).css('opacity', 0.5);
        },
        unmutePanelBody: function () {
            $(PromoProduct.btnSave).closest(PromoProduct.bodyPanel).css('opacity', 1);
        },
        enableSave: function () {
            $(PromoProduct.btnSave).prop('disabled', false);
        },
        disableSave: function () {
            $(PromoProduct.btnSave).prop('disabled', true);
        },
        enableProducts: function () {
            $(PromoProduct.products).prop('disabled', false);
        },
        disableProducts: function () {
            $(PromoProduct.products).prop('disabled', true);
        },
        enablePrice: function () {
            $(PromoProduct.price).prop('disabled', false);
        },
        disablePrice: function () {
            $(PromoProduct.price).prop('disabled', true);
        },
        resetProducts: function () {
            $(PromoProduct.products).val('');
            $(PromoProduct.products).trigger('change');
        },
        resetPrice: function () {
            $(PromoProduct.price).val(0);
        },
        clearProductsSelect: function () {
            $(PromoProduct.products).find('option').remove();
        },
        addOptionProductsSelect: function (data) {
            for (var k in data) {
                if (data[k] !== '') {
                    console.log(data[k], k);
                    var newOption = new Option(data[k], k, false, false);
                    $(PromoProduct.products).append(newOption);
                }
            }

            $(PromoProduct.products).val('');
            $(PromoProduct.products).trigger('change');
        },
        clear: function () {
            $(PromoProduct.rows).find(PromoProduct.row + ':not(:first)').remove();
            return true;
        },
        eventAdd: function () {
            $(PromoProduct.btnAdd).on('click', function (e) {
                e.stopImmediatePropagation();
                return PromoProduct.add();
            });
        },
        eventRemove: function () {
            $(PromoProduct.btnRemove).on('click', function () {
                return PromoProduct.remove($(this));
            });
        },
        add: function () {
            var clone = $(PromoProduct.rows).find(PromoProduct.row).first().clone();

            $(PromoProduct.rows).append(clone);
            $(PromoProduct.rows).find(PromoProduct.row).last().find('span').remove();
            $(PromoProduct.rows).find(PromoProduct.row).last().find('select').select2();
            $(PromoProduct.rows).find(PromoProduct.row).last().find('span').removeAttr('style');

            PromoProduct.eventAdd();
            PromoProduct.eventRemove();
            PromoProduct.manageButtons();
        },
        remove: function (btn) {
            $(btn).closest(PromoProduct.row).remove();
        },
        manageButtons: function () {
            if ($(PromoProduct.row).length === 1) {
                $(PromoProduct.btnRemove).first().hide();
            } else {
                $(PromoProduct.btnRemove).show();
                $(PromoProduct.btnRemove).last().hide();
                $(PromoProduct.btnAdd).hide();
                $(PromoProduct.btnAdd).last().show();
            }
        },
    };

    PromoProduct.init();
});
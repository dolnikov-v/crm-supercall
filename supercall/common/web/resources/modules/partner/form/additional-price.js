$(document).ready(function () {
    var additionalPrice = {
        openModalButton: $('#open_condition_modal'),
        modalCondition: $('#condition_modal'),
        modalSaveButton: $('#condition_ok'),
        modalCloseButton: $('.close_btn'),
        modalContentClass: '.additional_modal_content',
        modalContentDefault: 'default',
        additionalTypeElement: $('#addition_type'),
        init: function () {
            this.modalContentShow();
            this.openModalEvent();
            this.additionalTypeChangeValue();
        },
        openModalEvent: function () {
            this.openModalButton.on('click', function () {
                additionalPrice.modalCondition.modal();
            });
        },
        additionalType: function () {
            return this.additionalTypeElement.val();
        },
        additionalTypeChangeValue: function () {
            this.additionalTypeElement.on('change', function () {
                additionalPrice.modalToggleContent(additionalPrice.additionalType());
            })
        },
        modalContents: function () {
            return $(this.modalContentClass);
        },
        modalToggleContent: function () {
            this.modalContentsHide();
            this.modalContentShow(this.additionalType());
        },
        modalContentsHide: function () {
            this.modalContents().hide();
        },
        modalContentShow: function (type) {
            this.modalContentsHide();
            if ($("div[data-type='" + type + "']" + this.modalContentClass).length > 0) {
                $("div[data-type='" + type + "']" + this.modalContentClass).show();
            } else { //если элемента нет то, берем контент по умолчанию
                $("div[data-type='" + this.modalContentDefault + "']" + this.modalContentClass).show();
            }
        }
    }

    additionalPrice.init();

});


$(document).ready(function () {
    $('#save_additional_price').on('click', function () {
        $.ajax({
            method: 'post',
            url: '/partner/form/additional-price',
            dataType: 'json',
            data: $("#partner-form-additional-price").serialize(),
            success: function (result) {
                if (result.success) {
                    toastr.success(result.message);
                } else {
                    toastr.error(result.message);
                }
            }
        })
    });
});

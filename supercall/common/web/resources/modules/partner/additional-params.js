$(function () {
    var AdditionalParams = {
        url: '/partner/form/save-additional-params',
        rows: '.additional-params',
        row: '.additional-param',
        btnAdd: '.add-param',
        btnRemove: '.remove-param',
        btnSave: '#save-additional-params',
        btnClear: '#clear-additional-params',
        textField: 'input[type=text]',
        form_id: '[name*=form_id]',
        fieldKey: '[name*=key]',
        fieldParam: '[name*=param]',
        fieldLength: '[name*=length]',
        defaultLength: 7,
        data: {
            pair: {},
            json: {}
        },
        init: function () {
            AdditionalParams.eventAdd();
            AdditionalParams.eventRemove();
            AdditionalParams.manageButtons();

            $(AdditionalParams.btnSave).on('click', AdditionalParams.save);
            $(AdditionalParams.btnClear).on('click', AdditionalParams.clear);
        },
        save: function () {
            AdditionalParams.disabledForm();

            $.when(AdditionalParams.organization()).done(function () {
                $.when(AdditionalParams.takeData()).done(function(){
                    var data = JSON.stringify(AdditionalParams.data);

                    $.ajax({
                        url: AdditionalParams.url,
                        type: 'post',
                        dataType: 'json',
                        data: {
                            data: data,
                            form_id: $(AdditionalParams.form_id).val()
                        },
                        success: function (data) {
                            if (!data.success) {
                                toastr.error(data.message);
                            } else {
                                toastr.success(data.message);
                            }

                            AdditionalParams.enabledForm();
                        },
                        error: function (error) {
                            toastr.error(error.responseText);

                            AdditionalParams.enabledForm();
                        }
                    });
                });
            });

            return false;
        },
        clear: function () {
            $(AdditionalParams.rows).find(AdditionalParams.row + ':not(:first)').remove();
            $(AdditionalParams.rows).find($(AdditionalParams.textField)).val('');
            return false;
        },
        disabledForm: function () {
            $(AdditionalParams.rows).find('input[type=text]').prop('disabled', true);
            $(AdditionalParams.btnSave).prop('disabled', true);
            $(AdditionalParams.btnClear).prop('disabled', true);
            $(AdditionalParams.rows).css('opacity', 0.5);

            $(AdditionalParams.rows).css('cursor', 'wait');
        },
        enabledForm: function () {
            $(AdditionalParams.rows).find('input[type=text]').prop('disabled', false);
            $(AdditionalParams.btnSave).prop('disabled', false);
            $(AdditionalParams.btnClear).prop('disabled', false);
            $(AdditionalParams.rows).css('opacity', 1);

            $(AdditionalParams.rows).css('cursor', 'default');
        },
        manageButtons: function () {
            if ($(AdditionalParams.row).length === 1) {
                $(AdditionalParams.btnRemove).first().hide();
            } else {
                $(AdditionalParams.btnRemove).show();
                $(AdditionalParams.btnRemove).last().hide();
                $(AdditionalParams.btnAdd).hide();
                $(AdditionalParams.btnAdd).last().show();
            }
        },
        eventAdd: function () {
            $(AdditionalParams.btnAdd).on('click', function (e) {
                e.stopImmediatePropagation();
                return AdditionalParams.add();
            });
        },
        eventRemove: function () {
            $(AdditionalParams.btnRemove).on('click', function () {
                return AdditionalParams.remove($(this));
            });
        },
        add: function () {
            var clone = $(AdditionalParams.rows).find(AdditionalParams.row).first().clone();

            $(AdditionalParams.rows).append(clone);

            AdditionalParams.eventAdd();
            AdditionalParams.eventRemove();
            AdditionalParams.manageButtons();
        },
        remove: function (btn) {
            $(btn).closest(AdditionalParams.row).remove();
        },
        organization: function () {
            return $(AdditionalParams.rows).find(AdditionalParams.row).each(function () {
                var row = $(this);

                $(row).find(AdditionalParams.fieldParam).attr('data-key', $(this).closest(AdditionalParams.row).find(AdditionalParams.fieldKey).val());
                $(row).find(AdditionalParams.fieldParam).attr('data-length', $(this).closest(AdditionalParams.row).find(AdditionalParams.fieldLength).val());
            });
        },
        takeData: function () {
            // var pair = [];
            // var json = [];

            return $(AdditionalParams.rows).find(AdditionalParams.row).find(AdditionalParams.fieldParam).each(function () {
                var key = $(this).attr('data-key');
                var param = $(this).val();

                if (key !== '' && param !== '') {
                    var length = $(this).attr('data-length') === '' ? AdditionalParams.defaultLength : $(this).attr('data-length');
                    AdditionalParams.data.pair[key] = param;
                    AdditionalParams.data.json[key] = 'X'.repeat(length);
                }
            });
        }
    };

    AdditionalParams.init();
});
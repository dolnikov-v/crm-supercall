$(document).ready(function () {
    $("#get_partner_shipping").click(function () {

        $(this).prop("disabled", true);
        $(this).css("cursor", "wait");

        $.ajax({
            url: '/partner/shipping/get-partner-delivery-list',
            type: 'POST',
            data: {
                partner: ['2wtrade']
            },
            success: function (data) {
                var result = JSON.parse(data);

                if (result.success === true) {
                    toastr.success(result.message);
                    endUpdateShipping();
                } else {
                    toastr.error(result.message);
                }

            },
            error: function (data) {
                var result = JSON.parse(data);
                toastr.error(result.message);
            }
        });

        return false;
    });

    function endUpdateShipping(){
        $(this).prop("disabled", false);
        $(this).css("cursor", "default");
        location.reload();
    }

});


<?php

namespace common\modules\partner\assets\product;

use yii\web\AssetBundle;

/**
 * Class TablePriceProductAsset
 * @package common\modules\partner\assets\product
 */
class TablePriceProductAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/partner/product';

    public $js = [
        'table-price-product.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

<?php

namespace common\modules\partner\assets;

use yii\web\AssetBundle;

class AdditionalParamsAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/partner/';

    public $js = [
        'additional-params.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
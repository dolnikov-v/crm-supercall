<?php
namespace common\modules\partner\assets\widgets;

use yii\web\AssetBundle;

/**
 * Class ModalCopyFormAsset
 * @package common\modules\partner\assets\widgets
 */
class ModalCopyFormAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/partner/widgets/modal-copy-form';

    public $js = [
        'modal-copy-form.js',
    ];

    public $depends = [
    ];
}

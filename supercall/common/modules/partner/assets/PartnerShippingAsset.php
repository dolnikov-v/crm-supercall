<?php
/**
 * Created by PhpStorm.
 * User: PCWORK
 * Date: 14.09.2017
 * Time: 15:49
 */

namespace common\modules\partner\assets;

use yii\web\AssetBundle;

class PartnerShippingAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/partner/';

    public $js = [
        'partner-shipping.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
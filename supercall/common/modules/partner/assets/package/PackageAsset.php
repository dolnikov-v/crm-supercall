<?php
namespace common\modules\partner\assets\package;

use yii\web\AssetBundle;

class PackageAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/partner/package';

    public $js = [
        'package.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
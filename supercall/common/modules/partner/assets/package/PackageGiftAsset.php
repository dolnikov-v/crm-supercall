<?php

namespace common\modules\partner\assets\package;

use yii\web\AssetBundle;

/**
 * Class PackageGiftAsset
 * @package common\modules\partner\assets\package
 */
class PackageGiftAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/partner/package';

    public $js = [
        'package-gift.js',
    ];

    public $css = [
        'package-gift.css',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
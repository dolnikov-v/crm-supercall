<?php

namespace common\modules\partner\assets\form;

use yii\web\AssetBundle;

/**
 * Class ListAttributeAsset
 * @package common\modules\partner\assets\form
 */
class ListAttributeAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/partner/form';

    public $js = [
        'list-attribute.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
<?php
/**
 * Created by PhpStorm.
 * User: paraf_000
 * Date: 09.11.2018
 * Time: 11:52
 */

namespace common\modules\partner\assets\form;

use yii\web\AssetBundle;

class FastChangeQueueAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/partner/form';

    public $js = [
        'fast-change-queue.js',
    ];
}
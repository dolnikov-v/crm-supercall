<?php

namespace common\modules\partner\assets\form;

use yii\web\AssetBundle;

/**
 * Class PromoProductAsset
 * @package common\modules\partner\assets\form
 */
class PromoProductAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/partner/form';

    public $js = [
        'promo-product.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
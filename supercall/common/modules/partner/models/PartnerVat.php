<?php
namespace common\modules\partner\models;

use common\components\db\ActiveRecord;
use common\models\Country;
use Yii;

/**
 * Class PartnerVat
 * @package common\modules\partner\models
 * @property integer $id
 * @property integer $partner_id
 * @property integer $country_id
 * @property double $value
 */
class PartnerVat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_vat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'country_id', 'value'], 'required'],
            [['partner_id', 'country_id'], 'integer'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'partner_id' => Yii::t('common', 'Партнер'),
            'country_id' => Yii::t('common', 'Страна'),
            'value' => Yii::t('common', 'НДС'),
            'partner.name' => Yii::t('common', 'Партнер'),
            'country.name' => Yii::t('common', 'Страна'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}

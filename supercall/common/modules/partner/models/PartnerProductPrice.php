<?php
namespace common\modules\partner\models;

use common\components\db\ActiveRecord;
use common\models\Country;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class PartnerProductPrice
 * @package common\modules\partner\models
 * @property integer $id
 * @property integer $partner_product_id
 * @property integer $country_id
 * @property array|null $value
 */
class PartnerProductPrice extends ActiveRecord
{
    /**
     * 1 - цена будет расчитываться из таблица цен
     * 2 - цена будет расчитываться по формуле цен
     */
    const PRICE_DATA_TYPE_TABLE = 1;
    const PRICE_DATA_TYPE_STATIC = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_product_price}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_product_id', 'country_id', 'value'], 'required'],
            [['partner_product_id', 'country_id'], 'integer'],
            [['value'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'partner_product_id' => Yii::t('common', 'Товар'),
            'country_id' => Yii::t('common', 'Страна'),
            'value' => Yii::t('common', 'Цена'),
            'partnerProduct.partner.name' => Yii::t('common', 'Партнер'),
            'partnerProduct.product.name' => Yii::t('common', 'Товар'),
            'country.name' => Yii::t('common', 'Страна'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->value = Json::encode($this->value);
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->value = Json::decode($this->value);
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerProduct()
    {
        return $this->hasOne(PartnerProduct::className(), ['id' => 'partner_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function getPrice($quantity)
    {
        $type = ArrayHelper::getValue($this->value, 'type');
        if ($type == self::PRICE_DATA_TYPE_TABLE) {
            $price = ArrayHelper::getValue($this->value, 'table.' . $quantity, 0);
            if ($price) {
                return $price;
            } else {
                return ArrayHelper::getValue($this->value, 'table.1', 0);
            }
        }
        if ($type == self::PRICE_DATA_TYPE_STATIC) {
            return ArrayHelper::getValue($this->value, 'price', 0);
        }
        return 0;
    }
}

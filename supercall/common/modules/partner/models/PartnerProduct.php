<?php
namespace common\modules\partner\models;

use common\components\db\ActiveRecord;
use common\models\Product;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use common\modules\partner\models\query\PartnerProductQuery;
use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use common\models\Country;

/**
 * Class PartnerProduct
 * @package common\modules\partner\models
 * @property integer $id
 * @property integer $partner_id
 * @property integer $partner_product_id
 * @property integer $product_id
 * @property integer $country_id
 * @property integer $active
 * @property integer $amount
 * @property integer $created_at
 * @property integer $updated_at
 * @property array|null $price_data
 */
class PartnerProduct extends ActiveRecord
{

    /**
     * 1 - цена будет расчитываться из таблица цен
     * 2 - цена будет расчитываться по формуле цен
     */
    const PRICE_DATA_TYPE_TABLE = 1;
    const PRICE_DATA_TYPE_STATIC = 2;

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;


    /**
     * @var array
     */
    public $table_price;

    /**
     * @var array
     */
    public $count_products;

    /**
     * @var integer
     */
    public $price_data_type;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_product}}';
    }

    /**
     * @return array
     */
    public static function getPriceDataTypeCollection()
    {
        return [
            self::PRICE_DATA_TYPE_STATIC => yii::t('common', 'Единая цена'),
            self::PRICE_DATA_TYPE_TABLE => yii::t('common', 'Разная цена'),
        ];
    }

    /**
     * @return array
     */
    public static function getPriceCountCollection()
    {
        return OrderProduct::countList();
    }

    /**
     * @return PartnerProductQuery
     */
    public static function find()
    {
        return new PartnerProductQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'country_id', 'partner_product_id', 'product_id'], 'required'],
            [['partner_id', 'country_id', 'partner_product_id', 'product_id', 'active', 'amount'], 'integer'],
            [['price_data', 'price_data_type', 'count_products', 'table_price'], 'safe'],
            ['active', 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'partner_id' => Yii::t('common', 'Партнер'),
            'partner_product_id' => Yii::t('common', 'Номер у партнера'),
            'product_id' => Yii::t('common', 'Товар'),
            'country_id' => Yii::t('common', 'Страна'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'price_data_type' => Yii::t('common', 'Тип цены'),
            'price_data' => Yii::t('common', 'Цена'),
            'partner.name' => Yii::t('common', 'Партнер'),
            'product.name' => Yii::t('common', 'Товар'),
            'table_price' => Yii::t('common', 'Цена за единицу'),
            'count_products' => Yii::t('common', 'Количество товара'),
            'amount' => Yii::t('common', 'статок на складе')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function getPartnerProductVat()
    {
        return $this->hasMany(PartnerProductVat::className(), ['partner_product_id' => 'id']);
    }

    /**
     * @inheritdoc
     */

    public function beforeSave($insert)
    {

        if (!is_null($this->price_data_type)) {
            //единая цена
            if ($this->price_data_type == self::PRICE_DATA_TYPE_STATIC) {
                $this->price_data = ['type' => self::PRICE_DATA_TYPE_STATIC, 'price' => $this->price_data];
            } //разная цена
            elseif ($this->price_data_type == self::PRICE_DATA_TYPE_TABLE) {
                $table_price = [];

                foreach ($this->count_products as $k => $count) {
                    $table_price[$count] = $this->table_price[$k];
                }

                $this->price_data = [
                    'type' => self::PRICE_DATA_TYPE_TABLE,
                    'table' => $table_price
                ];
            }
        }

        $this->price_data = Json::encode($this->price_data);

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->price_data = Json::decode($this->price_data);
        parent::afterFind();
    }

    /**
     * Вытаскиваем цену из JSON поля
     * @param $quantity
     * @return int|mixed
     */
    public function getPrice($quantity)
    {
        $type = ArrayHelper::getValue($this->price_data, 'type');

        if ($type == self::PRICE_DATA_TYPE_TABLE) {
            $price = ArrayHelper::getValue($this->price_data, 'table.' . $quantity, 0);
            if ($price) {
                return $price;
            } else {
                return ArrayHelper::getValue($this->price_data, 'table.1', 0);
            }
        }
        if ($type == self::PRICE_DATA_TYPE_STATIC) {

            return ArrayHelper::getValue($this->price_data, 'price', 0);
        }
        return 0;
    }


    /**
     * Возвращает список ндс для стран и товаров партнера
     * @return array
     */
    public static function getVatList($partner_id)
    {
        $vat_list = [];
        $partner_vats = PartnerVat::find()->where([
            'partner_id' => $partner_id,
        ])->all();

        if (!empty($partner_vats)) {
            foreach ($partner_vats as $vat) {
                $vat_list[$vat['country_id']] = ['main' => $vat['value']];
            }
        }

        $products = PartnerProduct::find()
            ->joinWith(['partnerProductVat'])
            ->where(['partner_id' => $partner_id])
            ->all();

        if (!empty($products)) {
            foreach ($products as $product) {
                if (!empty($product->partnerProductVat)) {
                    foreach ($product->partnerProductVat as $vat) {
                        $vat_list[$vat->country_id][$product->product_id] = $vat->value;
                    }
                }
            }
        }

        return $vat_list;
    }

    /**
     * @param $product_id
     * @param $partner_id
     * @return PartnerProduct
     */
    public static function getPartnerProduct($product_id, $partner_id, $country_id = false)
    {
        $condition = ['product_id' => $product_id];
        if ($country_id && is_numeric($country_id)) {
            $condition['country_id'] = $country_id;
        }
        return PartnerProduct::find()
            ->where($condition)
            ->andWhere(['partner_id' => $partner_id])
            ->andWhere(['active' => 1])
            ->one();

    }


    /**
     * Расчет НДС для указанной страны
     * @param $country_id
     * @return double
     */
    public function getVat($country_id)
    {
        /** @var PartnerProductVat $product_vat */
        $product_vat = PartnerProductVat::find()->where([
            'partner_product_id' => $this->id,
            'country_id' => $country_id,
        ])->one();
        if ($product_vat) {
            return $product_vat->value;
        }

        $partner_vat = PartnerVat::find()->where([
            'partner_id' => $this->partner_id,
            'country_id' => $country_id
        ])->one();
        if ($partner_vat) {
            return $partner_vat->value;
        }

        return 0.0;
    }

    public function getPriceData()
    {
        if ($this->price_data['type'] == self::PRICE_DATA_TYPE_STATIC) {
            if (!empty($this->price_data)) {
                return $this->price_data['price'];
            } else {
                return 0;
            }
        } elseif ($this->price_data['type'] == self::PRICE_DATA_TYPE_STATIC) {
            return $this->price_data['table'];
        }
    }

    /**
     * @param $partner_id
     * @param $country_id
     * @return PartnerProduct[]
     */
    public static function getPartnerProducts($partner_id, $country_id)
    {

        return PartnerProduct::find()
            ->leftJoin(Product::tableName(), Product::tableName() . '.id=' . PartnerProduct::tableName() . '.product_id')
            ->where(['partner_id' => $partner_id])
            ->andWhere(['country_id' => $country_id])
            ->andWhere([PartnerProduct::tableName().'.active' => PartnerProduct::ACTIVE])
            ->all();
    }
}

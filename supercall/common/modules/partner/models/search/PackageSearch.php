<?php
namespace common\modules\partner\models\search;

use common\modules\partner\models\Package;
use yii\data\ActiveDataProvider;

/**
 * Class PackageSearch
 * @package common\modules\partner\models\search
 */
class PackageSearch extends Package
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'country_id', 'product_id', 'active'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_ASC;
            $sort['id'] = SORT_ASC;
        }

        $where = [];
        if (!empty($params['where'])) {
            $where = $params['where'];
        }

        $query = Package::find()
            ->where($where)
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['partner_id' => $this->partner_id]);
        $query->andFilterWhere(['country_id' => $this->country_id]);
        $query->andFilterWhere(['product_id' => $this->product_id]);
        $query->andFilterWhere(['active' => $this->active]);

        return $dataProvider;
    }
}
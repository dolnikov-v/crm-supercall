<?php
namespace common\modules\partner\models\search;

use common\modules\partner\models\Partner;
use yii\data\ActiveDataProvider;

/**
 * Class PartnerSearch
 * @package common\modules\partner\models\search
 */
class PartnerSearch extends Partner
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_ASC;
            $sort['id'] = SORT_ASC;
        }

        $where = [];
        if (!empty($params['where'])) {
            $where = $params['where'];
        }

        $query = Partner::find()
            ->where($where)
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

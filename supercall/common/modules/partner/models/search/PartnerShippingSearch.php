<?php
namespace common\modules\partner\models\search;

use common\models\Country;
use common\models\Shipping;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerShipping;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class PartnerShippingSearch
 * @package common\modules\partner\models\search
 */
class PartnerShippingSearch extends PartnerShipping
{
    /** @var string */
    public $partner;

    /** @var string */
    public $shipping;

    /** @var string */
    public $country;

    /** @var string */
    public $active;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner', 'shipping', 'country', 'active'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'partner' => Yii::t('common', 'Партнер'),
            'shipping' => Yii::t('common', 'Курьерская служба'),
            'country' => Yii::t('common', 'Страна'),
            'active' => Yii::t('common', 'Активность'),
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = PartnerShipping::find()
            ->joinWith([
                'partner',
                'shipping',
                'country'
            ])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([Partner::tableName() . '.id' => $this->partner]);
        $query->andFilterWhere([Shipping::tableName() . '.id' => $this->shipping]);
        $query->andFilterWhere([Country::tableName() . '.id' => $this->country]);

        if ($this->active != "") {
            $query->andFilterWhere([PartnerShipping::tableName() . '.active' => $this->active]);
        }

        return $dataProvider;
    }
}

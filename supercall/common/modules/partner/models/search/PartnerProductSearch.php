<?php
namespace common\modules\partner\models\search;

use common\models\Country;
use common\models\Product;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerProduct;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class PartnerProductSearch
 * @package common\modules\partner\models\search
 */
class PartnerProductSearch extends PartnerProduct
{
    /** @var string */
    public $partner;

    /** @var string */
    public $product;

    /** @var string */
    public $active;

    public $country;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner', 'product', 'active', 'country'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'partner' => Yii::t('common', 'Партнер'),
            'product' => Yii::t('common', 'Товар'),
            'country' => Yii::t('common', 'Страна'),
            'active' => Yii::t('common', 'Активность'),
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = PartnerProduct::find()
            ->joinWith([
                'partner',
                'product',
                'country'
            ])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([Partner::tableName() . '.id' => $this->partner]);
        $query->andFilterWhere([Product::tableName() . '.id' => $this->product]);
        $query->andFilterWhere([Country::tableName() . '.id' => $this->country]);

        if ($this->active != "") {
            $query->andFilterWhere([PartnerProduct::tableName() . '.active' => $this->active]);
        }

        return $dataProvider;
    }
}

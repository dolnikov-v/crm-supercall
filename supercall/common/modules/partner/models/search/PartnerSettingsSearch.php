<?php
namespace common\modules\partner\models\search;

use common\models\Country;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerSettings;
use common\modules\partner\models\PartnerVat;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class PartnerSettingsSearch
 * @package common\modules\partner\models\search
 */
class PartnerSettingsSearch extends PartnerVat
{
    /** @var string */
    public $partner;

    /** @var string */
    public $country;

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'SettingsSearch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner', 'country'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'partner' => Yii::t('common', 'Партнер'),
            'country' => Yii::t('common', 'Страна'),
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['country.name'] = SORT_ASC;
        }

        $query = PartnerSettings::find()
            ->joinWith([
                'partner',
                'country'
            ])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([Partner::tableName() . '.id' => $this->partner]);
        $query->andFilterWhere([Country::tableName() . '.id' => $this->country]);

        return $dataProvider;
    }
}

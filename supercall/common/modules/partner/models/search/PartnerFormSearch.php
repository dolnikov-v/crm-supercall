<?php
namespace common\modules\partner\models\search;

use common\models\Country;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerProduct;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class PartnerFormSearch
 * @package common\modules\partner\models\search
 */
class PartnerFormSearch extends PartnerForm
{
    /** @var string */
    public $partner;

    /** @var string */
    public $name;

    /** @var string */
    public $active;

    /** @var  array */
    public $country;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner', 'name', 'active', 'country'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'partner' => Yii::t('common', 'Партнер'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Активность'),
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = PartnerForm::find()
            ->joinWith([
                'partner',
                'country'
            ])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([Country::tableName() . '.id' => $this->country]);
        $query->andFilterWhere([Partner::tableName() . '.id' => $this->partner]);
        $query->andFilterWhere(['name' => $this->name]);

        if ($this->active != "") {
            $query->andFilterWhere([PartnerForm::tableName() . '.active' => $this->active]);
        }

        return $dataProvider;
    }
}

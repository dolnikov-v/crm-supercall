<?php

namespace common\modules\partner\models;

use common\components\db\ActiveRecord;
use common\models\Product;
use common\modules\partner\models\query\PackageGiftQuery;
use Yii;

/**
 * This is the model class for table "package_gift".
 *
 * @property integer $id
 * @property integer $package_id
 * @property integer $product_id
 * @property integer $free_amount
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Package $package
 * @property PartnerProduct $product
 */



/**
 * Class PackageGift
 * @package common\modules\partner\models
 */
class PackageGift extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%package_gift}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['package_id', 'product_id', 'free_amount'], 'required'],
            [['package_id', 'product_id', 'free_amount', 'created_at', 'updated_at'], 'integer'],
            [['package_id'], 'exist', 'skipOnError' => true, 'targetClass' => Package::className(), 'targetAttribute' => ['package_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'package_id' => Yii::t('common', 'Акция'),
            'product_id' => Yii::t('common', 'Товар'),
            'free_amount' => Yii::t('common', 'Количество')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return PackageGiftQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PackageGiftQuery(get_called_class());
    }
}
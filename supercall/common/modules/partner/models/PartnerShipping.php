<?php
namespace common\modules\partner\models;

use common\components\db\ActiveRecord;
use common\models\Country;
use common\models\Shipping;
use common\modules\partner\models\query\PartnerShippingQuery;
use Yii;

/**
 * Class PartnerShipping
 * @package common\modules\partner\models
 * @property integer $id
 * @property integer $partner_id
 * @property integer $partner_shipping_id
 * @property integer $shipping_id
 * @property integer $country_id
 * @property float $price
 * @property boolean $dynamic_price
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property Country $country
 * @property Country $is_default
 */
class PartnerShipping extends ActiveRecord
{
    const ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_shipping}}';
    }

    /**
     * @return PartnerShippingQuery
     */
    public static function find()
    {
        return new PartnerShippingQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'partner_shipping_id', 'shipping_id'], 'required'],
            [['partner_id', 'partner_shipping_id', 'shipping_id', 'country_id', 'active'], 'integer'],
            ['price', 'number'],
            ['active', 'default', 'value' => 1],
            [['dynamic_price', 'is_default'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'partner_id' => Yii::t('common', 'Партнер'),
            'partner_shipping_id' => Yii::t('common', 'Номер у партнера'),
            'shipping_id' => Yii::t('common', 'Курьерская служба'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'partner.name' => Yii::t('common', 'Партнер'),
            'shipping.name' => Yii::t('common', 'Курьерская служба'),
            'country_id' => Yii::t('common', 'Страна'),
            'country.name' => Yii::t('common', 'Страна'),
            'price' => Yii::t('common', 'Цена'),
            'dynamic_price' => Yii::t('common', 'Определять скриптом'),
            'is_default' => Yii::t('common', 'По умолчанию'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipping()
    {
        return $this->hasOne(Shipping::className(), ['id' => 'shipping_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}

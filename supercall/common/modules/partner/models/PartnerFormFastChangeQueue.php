<?php
/**
 * Created by PhpStorm.
 * User: paraf_000
 * Date: 21.11.2018
 * Time: 17:33
 */

namespace common\modules\partner\models;


use backend\modules\queue\models\query\QueueQuery;
use backend\modules\queue\models\Queue;
use common\components\db\ActiveRecord;
use Yii;
use yii\helpers\Json;

/**
 * Class PartnerFormFastChangeQueue
 * @package common\modules\partner\models
 *
 * @var integer id
 * @var integer form_id
 * @var array queue
 * @var integer created_at
 * @var integer updated_at
 */
class PartnerFormFastChangeQueue extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%partner_form_fast_change_queue}}';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Ид'),
            'form_id' => Yii::t('common', 'Форма'),
            'queue' => Yii::t('common', 'Очереди'),
        ];
    }


    public function beforeSave($insert)
    {
        if (is_array($this->queue)) {
            $this->queue = Json::encode($this->queue);
        }

        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        if(is_string($this->queue)){
            $this->queue = Json::decode($this->queue);
        }

        parent::afterFind();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['form_id'], 'required'],
            [['form_id'], 'integer'],
            [['queue'], 'safe']

        ];
    }

    /**
     * @param PartnerForm $form
     */
    public function getQueueList($form)
    {
        return Queue::find()->byCountry($form->country_id, false)->byPartner($form->partner_id)->active()->asArray()->all();
    }

    /**
     *
     */
    public function getSelectedQueues($array = false)
    {
        if(!$array) {
            return Queue::find()->where(['in', 'id', $this->queue])->all();
        } else {
            return Queue::find()->where(['in', 'id', $this->queue])->asArray()->all();
        }
    }
}
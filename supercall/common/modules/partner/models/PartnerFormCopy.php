<?php

namespace common\modules\partner\models;

use common\models\Country;
use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class PartnerFormCopy extends Model
{
    /** @var integer */
    public $form;

    /** @var integer[] */
    public $countries;

    /** @var integer[] */
    public $partners;

    public function rules()
    {
        return [
            [['form', 'partners', 'countries'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'partners' => \Yii::t('common', 'Партнеры'),
            'countries' => \Yii::t('common', 'Страны'),
        ];
    }

    public function partnerOptions()
    {
        return Partner::find()->active()->collection();
    }

    public function countryOptions()
    {
        return Country::find()->active()->collection('common');
    }

    public function apply()
    {
        if (!is_array($this->partners)){
            $this->addError('partners', \Yii::t('common', 'Необходимо выбрать партнеров'));
        }
        if (!is_array($this->countries)){
            $this->addError('countries', \Yii::t('common', 'Необходимо выбрать страны'));
        }
        if ($this->hasErrors()){
            return false;
        }

        $src_form = PartnerForm::find()->where(['id' => $this->form])->one();
        foreach ($this->partners as $partner_id => $partner_name) {
            foreach ($this->countries as $country_id => $country_name) {
                $query_form = PartnerForm::find()
                    ->where(['partner_id' => $partner_id, 'country_id' => $country_id])
                    ->andWhere(['<>', 'id', $src_form->id]);
                // Если форма существует, то вытащим ее
                if ($query_form->exists()) {
                    $dst_form = $query_form->one();
                }
                // либо создаем новую форму
                else {
                    $dst_form = new PartnerForm();
                    $dst_form->attributes = $src_form->attributes;
                    $dst_form->country_id = $country_id;
                    $dst_form->name = $dst_form->country->name;
                    $dst_form->partner_id = $partner_id;
                    $dst_form->save(false);
    
                    \Yii::$app->notifier->addNotifierSuccess(\Yii::t('common', 'Форма <i>"' .$dst_form->name. '-' .$dst_form->partner->name. '"</i> успешно создана'));
                }
                
                // предварительно вытащить все текущие аттрибуты данной формы
                // необходимо для сохранения уникальных аттрибутов и добавления новых
                $attrByForm = PartnerFormAttribute::find()
                    ->select(['id', 'name'])
                    ->where([
                        'form_id' => $dst_form->id
                    ]);
                $currentAttrs = [];
                if ($attrByForm->exists()) {
                    $currentAttrs = ArrayHelper::getColumn($attrByForm->asArray()->all(), 'name');
                }
                // копируем аттрибуты для новой/существующей формы
                foreach ($src_form->attrs as $key => $src_attr) {
                    $is_remove = false;
                    // Если уже есть аттрибут, то старый проще удалить, и скопировать настройки нового
                    if (in_array($key, $currentAttrs)) {
                        $is_remove = true;
                    }
                    $this->copyAttr($dst_form->id, $src_attr->attributes, $is_remove);
                }
    
                \Yii::$app->notifier->addNotifierSuccess(\Yii::t('common', 'Аттрибуты для формы <i>"' .$dst_form->name. '-' .$dst_form->partner->name. '"</i> обновлены'));
            }
        }
        return !$this->hasErrors();
    }
    
    /**
     * Копируем аттрибуты для новой формы
     *
     * @param int $formId
     * @param $attributes
     * @param bool $is_remove - стоит ли искать текущий аттрибут и удалять его
     */
    private function copyAttr(int $formId, $attributes, $is_remove = false)
    {
        // Найдем и удалим уже существующий аттрибут
        if ($is_remove) {
            $useAttr = PartnerFormAttribute::find()->where(['form_id' => $formId, 'name' => $attributes['name'] ])->all();
            foreach ($useAttr as $attr) {
                PartnerFormAttribute::findOne($attr->id)->delete();
            }
        }
        
        $dst_attr = new PartnerFormAttribute();
        $dst_attr->attributes = $attributes;
        $dst_attr->form_id = $formId;
        if (!$dst_attr->save(false)) {
            \Yii::error('xxxx_Не сохранились настройки form_id=' .$formId .', attributes='.VarDumper::dumpAsString($attributes));
        }
    }
}
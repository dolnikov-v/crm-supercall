<?php
namespace common\modules\partner\models;

use common\components\db\ActiveRecord;
use common\models\Country;
use Yii;

/**
 * Class PartnerProductVat
 * @package common\modules\partner\models
 * @property integer $id
 * @property integer $partner_product_id
 * @property integer $country_id
 * @property double $value
 */
class PartnerProductVat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_product_vat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_product_id', 'country_id', 'value'], 'required'],
            [['partner_product_id', 'country_id'], 'integer'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'partner_product_id' => Yii::t('common', 'Товар'),
            'country_id' => Yii::t('common', 'Страна'),
            'value' => Yii::t('common', 'НДС'),
            'partnerProduct.partner.name' => Yii::t('common', 'Партнер'),
            'partnerProduct.product.name' => Yii::t('common', 'Товар'),
            'country.name' => Yii::t('common', 'Страна'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerProduct()
    {
        return $this->hasOne(PartnerProduct::className(), ['id' => 'partner_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}

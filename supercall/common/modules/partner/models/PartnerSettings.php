<?php
namespace common\modules\partner\models;

use backend\modules\queue\models\Queue;
use common\components\db\ActiveRecord;
use common\models\Country;
use common\modules\order\models\OrderType;
use Yii;

/**
 * Class PartnerSettings
 * @package common\modules\partner\models
 * @property integer $id
 * @property integer $partner_id
 * @property integer $country_id
 * @property boolean $genesys_sync_on_save
 * @property integer $queue_id - deprecated, необходимо удалить
 * @property integer $type_id
 * @property integer $buffer
 */
class PartnerSettings extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'country_id'], 'required'],
            [['partner_id', 'country_id', 'queue_id', 'type_id', 'buffer', 'daily_from', 'daily_till'], 'integer'],
            [['daily_from', 'daily_till'], 'in', 'range' => array_keys(self::dailyHours())],
            [['genesys_sync_on_save'], 'boolean'],
            [['queue_id'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => Queue::className(), 'targetAttribute' => ['queue_id' => 'id']],
            [['type_id'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => OrderType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'partner_id' => Yii::t('common', 'Партнер'),
            'country_id' => Yii::t('common', 'Страна'),
            'partner.name' => Yii::t('common', 'Партнер'),
            'country.name' => Yii::t('common', 'Страна'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),

            'genesys_sync_on_save' => Yii::t('common', 'Сохранять заказы в Genesys при создании'),
            'queue_id' => 'Очередь',
            'type_id' => 'Тип заказа',
            'buffer' => 'Буфер',
            'daily_from' => 'Ежедневно с .. часов',
            'daily_till' => 'Ежедневно до .. часов',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(OrderType::className(), ['id' => 'type_id']);
    }
    
    /**
     * Функция подготовит массив часов для настройки daily_from и daily_till
     * @return array
     */
    public static function dailyHours()
    {
        $data = [];
        for ($i=0; $i < 24; $i++) {
            $key = (int)$i * 3600; // количество секунд
            $data[ $key ] = $i;
        }
        
        return $data;
    }
    
    /**
     * По дефолту выставим daily_from с 9 часов
     * daily_till - до 21 часа
     *
     * @return bool
     */
    public function beforeValidate()
    {
        if ( !key_exists($this->daily_from, self::dailyHours() ) ) {
            $this->daily_from = 32400; // c 9 утра
        }
        
        if ( !key_exists($this->daily_till, self::dailyHours() ) ) {
            $this->daily_till = 75600; // по 21 вечера
        }
        
        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        // Для новой записи по дефолту Буффер = 60 минут
        if ($insert) {
            $this->buffer = 60;
        }
    
        return parent::beforeSave($insert);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: paraf_000
 * Date: 06.11.2018
 * Time: 11:48
 */

namespace common\modules\partner\models;


use common\components\db\ActiveRecord;
use common\modules\partner\components\form\AdditionalPriceCondition;
use Yii;

/**
 * Class PartnerFormAdditionPrice
 * @package common\modules\partner\models
 * @property integer id
 * @property integer form_id
 * @property integer addition_type
 * @property string condition
 * @property integer price
 * @property integer created_at
 * @property integer updated_at
 */
class PartnerFormAdditionPrice extends ActiveRecord
{
    const ADDITION_CONDITION_EQUAL = 1;
    const ADDITION_CONDITION_NOT_EQUAL = 2;
    const ADDITION_CONDITION_LESS_THAN = 3;
    const ADDITION_CONDITION_GREATER_THAN = 4;
    const ADDITION_CONDITION_LESS_THAN_OR_EQUAL = 5;
    const ADDITION_CONDITION_GREATER_THAN_OR_EQUAL = 6;

    const ADDITION_TYPE_PRODUCT = 'product';
    const ADDITION_TYPE_PRODUCT_QUANTITY = 'product_quantity';

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Ид'),
            'form_id' => Yii::t('common', 'Форма'),
            'addition_type' => Yii::t('common', 'Тип дополнительной цены'),
            'condition_text' => Yii::t('common', 'Условие дополнительной цены'),
            'condition' => Yii::t('common', 'Условие'),
            //'price' => Yii::t('common', 'Цена'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления')
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['form_id', 'addition_type', 'condition', 'quantity'], 'required'], //, 'price'
            [['form_id', 'quantity'], 'integer'], //, 'price'
            [['condition', 'addition_type'], 'string'],
        ];
    }

    /**
     * @param $condition
     * @return string
     */
    public static function mapCondition($condition)
    {
        switch (intval($condition)) {
            case null:
                $map = null;
                break;
            case self::ADDITION_CONDITION_EQUAL :
                $map = '=';
                break;
            case self::ADDITION_CONDITION_NOT_EQUAL :
                $map = '!=';
                break;
            case self::ADDITION_CONDITION_LESS_THAN :
                $map = '<';
                break;
            case self::ADDITION_CONDITION_GREATER_THAN :
                $map = '>';
                break;
            case self::ADDITION_CONDITION_LESS_THAN_OR_EQUAL :
                $map = '<=';
                break;
            case self::ADDITION_CONDITION_GREATER_THAN_OR_EQUAL :
                $map = '>=';
                break;
            default:
                $map = 'unknown';
                break;
        }

        return $map;
    }

    public static function tableName()
    {
        return '{{%partner_form_additional_price}}';
    }

    /**
     * @return array
     */
    public function getMappingCondition()
    {
        return [
            self::ADDITION_CONDITION_EQUAL => '=',
            self::ADDITION_CONDITION_NOT_EQUAL => '!=',
            self::ADDITION_CONDITION_LESS_THAN => '>',
            self::ADDITION_CONDITION_GREATER_THAN => '<',
            self::ADDITION_CONDITION_LESS_THAN_OR_EQUAL => '>=',
            self::ADDITION_CONDITION_GREATER_THAN_OR_EQUAL => '<=',
        ];
    }

    /**
     * @return array
     */
    public function getMappingType()
    {
        return [
            //self::ADDITION_TYPE_PRODUCT => Yii::t('common','Продукт'),
            self::ADDITION_TYPE_PRODUCT_QUANTITY => Yii::t('common', 'Количество продукта'),
        ];
    }
}
<?php

namespace common\modules\partner\models;

use common\components\base\FormModel;
use common\components\db\ActiveRecord;
use common\widgets\ActiveField;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class PartnerFormAttribute
 * @package common\modules\partner\models
 * @property integer $id
 * @property integer $form_id
 * @property string $name
 * @property string $label
 * @property bool $required
 * @property string $type
 * @property string $scope
 * @property string $regex
 * @property integer $length
 * @property boolean $is_klader
 * @property boolean $is_disabled_user_data
 * @property boolean $is_coordinate
 * @property string $gmap_address_component
 * @property PartnerForm $form
 * @property jsonb $values
 */
class PartnerFormAttribute extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%partner_form_attribute}}';
    }

    public function beforeValidate()
    {
        $this->values = json_encode($this->values, JSON_UNESCAPED_UNICODE);
        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(PartnerForm::className(), ['id' => 'form_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Код'),
            'label' => Yii::t('common', 'Название'),
            'required' => Yii::t('common', 'Обязательно'),
            'type' => Yii::t('common', 'Тип'),
            'scope' => Yii::t('common', 'Блок'),
            'regex' => Yii::t('common', 'Регулярное выражение'),
            'length' => Yii::t('common', 'Максимальная длина'),
            'is_klader' => Yii::t('common', 'из КЛАДРа'),
            'is_coordinate' => Yii::t('common', 'Координата'),
            'is_disabled_user_data' => Yii::t('common', 'Запрещать добавлять свои данные'),
            'gmap_address_component' => Yii::t('common', 'Компонент адреса Gmap'),
            'sort' => Yii::t('common', 'Сортировка'),
            'values' => Yii::t('common', 'Значения'),
        ];
    }

    public function rules()
    {
        return [
            [['name', 'label', 'type'], 'required'],
            ['name', 'match', 'pattern' => '/^[a-zA-Z0-9_]+$/', 'message' => 'Код может содержать только латинский буквы и цифры'],
            [['label', 'gmap_address_component'], 'string'],
            ['type', 'in', 'range' => array_keys(self::types())],
            [['required', 'is_klader', 'is_coordinate', 'is_disabled_user_data'], 'boolean'],
            ['required', 'default', 'value' => 0],
            ['is_klader', 'default', 'value' => 0],
            ['is_disabled_user_data', 'default', 'value' => 0],
            ['is_coordinate', 'default', 'value' => 0],
            ['scope', 'in', 'range' => [PartnerForm::SCOPE_GENERAL, PartnerForm::SCOPE_ADDRESS]],
            [['length', 'sort'], 'integer'],
            ['regex', 'validateRegex'],
            [['values'], 'string']
        ];
    }

    /**
     * @param FormModel $model
     * @param mixed $value
     * @throws InvalidConfigException
     */
    public function addToModel(FormModel $model, $value = null)
    {
        $model->defineAttribute($this->name, $this->label, $this->type, $value);
        if ($this->required) {
            $model->addRule($this->name, 'required');
        }
        if ($this->regex) {
            $model->addRule($this->name, 'match', ['pattern' => $this->regex, 'skipOnEmpty' => true]);
        }
        if ($this->gmap_address_component) {
            $model->gmapMapping[$this->name] = $this->gmap_address_component;
        }
        switch ($this->type) {
            case ActiveField::TYPE_INTEGER:
                $model->addRule($this->name, 'integer');
                break;
            case ActiveField::TYPE_STRING:
                $model->addRule($this->name, 'string', ['min' => 0, 'max' => $this->length ? $this->length : 255]);
                break;
            case ActiveField::TYPE_TEXT:
            case ActiveField::TYPE_LIST:
                $model->addRule($this->name, 'string', ['min' => 0, 'max' => $this->length ? $this->length : 65536]);
                break;
            default:
                //throw new InvalidConfigException(Yii::t('common', 'Неверный тип аттрибута "{type}"', ['type' => $this->type]));
        }
    }

    /**
     * @return array
     */
    public static function types()
    {
        return [
            ActiveField::TYPE_INTEGER => Yii::t('common', 'Число'),
            ActiveField::TYPE_STRING => Yii::t('common', 'Строка'),
            ActiveField::TYPE_TEXT => Yii::t('common', 'Текст'),
            ActiveField::TYPE_EMAIL => Yii::t('common', 'Email'),
            ActiveField::TYPE_PHONE => Yii::t('common', 'Телефон'),
            ActiveField::TYPE_LIST => Yii::t('common', 'Выпадающий список'),
        ];
    }

    public function validateRegex()
    {
        try {
            preg_match($this->regex, '');
        } catch (\Exception $e) {
            $this->addError('regex', Yii::t('common', 'Некорректное регулярное выражение'));
        }
    }

    /**
     * @param $form_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getFormAttributes($form_id)
    {
        return PartnerFormAttribute::find()
            ->where(['form_id' => $form_id])
            ->andWhere(['OR', ['is_klader' => true], ['is_coordinate' => true]])
            ->orderBy(['sort' => SORT_ASC])->all();
    }

    /**
     * @param $form_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getFormAttributesAll($form_id)
    {
        return PartnerFormAttribute::find()
            ->where(['form_id' => $form_id])
            ->orderBy(['sort' => SORT_ASC])->all();
    }

    /**
     * @param $attr
     * @param $form_id
     * @return null|PartnerFormAttribute
     */
    public static function getValues($attr, $form_id)
    {
        return PartnerFormAttribute::findOne(['name' => $attr, 'form_id' => $form_id]);
    }

    /**
     * @param $order
     * @return array|string
     */
    public static function getLabelCollection($order)
    {
        $form = PartnerForm::getFormByOrder($order);

        if (!$form) {
            return false;
        }

        $formAttributes = ArrayHelper::map(PartnerFormAttribute::getFormAttributesAll($form->id), 'label', 'name');
        $attrCollection = [];

        foreach ($formAttributes as $name => $label) {
            $attrCollection[$label] = yii::t('common', $name);
        }

        return $attrCollection;
    }

}
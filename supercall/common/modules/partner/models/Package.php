<?php

namespace common\modules\partner\models;

use common\components\db\ActiveRecord;
use common\models\Country;
use common\models\Product;
use common\modules\order\models\OrderProduct;
use common\modules\partner\models\query\PackageQuery;
use Yii;
use yii\helpers\Url;

/**
 * Class Package
 * @package common\modules\partner\models
 * @property integer $id
 * @property string $prefix
 * @property string $name
 * @property integer $partner_id
 * @property integer $country_id
 * @property integer $product_id
 * @property integer $paid_amount
 * @property integer $free_amount
 * @property integer $active
 * @property string $history
 * @property string $created_at
 * @property string $update_at
 * @property double $product_price
 */
class Package extends ActiveRecord
{
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    static $packageMapping = [
        1 => [
            'paid_amount' => 1,
            'free_amount' => 0,
        ],
        3 => [
            'paid_amount' => 2,
            'free_amount' => 1,
        ],
        5 => [
            'paid_amount' => 3,
            'free_amount' => 2,
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%package}}';
    }

    /**
     * @return PackageQuery
     */
    public static function find()
    {
        return new PackageQuery(get_called_class());
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->isNewRecord) {
            $history = [
                'prefix' => $this->prefix,
                'name' => $this->name,
                'partner_id' => $this->partner_id,
                'country_id' => $this->country_id,
                'product_id' => $this->product_id,
                'paid_amount' => $this->paid_amount,
                'active' => $this->active,
                'gifts' => PackageGift::find()->where(['package_id' => $this->id])->asArray()->all(),
                'user' => yii::$app->user->id
            ];

            if ($this->product_price) {
                $history['product_price'] = $this->product_price;
            }

            if ($insert) {
                $this->history = json_encode([date('YmdHis') => $history], JSON_UNESCAPED_UNICODE);
            } else {
                $old_history = json_decode($this->history, 1);
                $old_history[date('YmdHis')] = $history;
                $this->history = json_encode($old_history, JSON_UNESCAPED_UNICODE);
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'partner_id', 'country_id', 'product_id', 'paid_amount'], 'required'],
            [['partner_id', 'country_id', 'product_id', 'paid_amount', 'active', 'created_at', 'updated_at'], 'integer'],
            [['name', 'history', 'prefix'], 'string'],
            [['product_price'], 'double'],
            ['active', 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'prefix' => Yii::t('common', 'Префикс'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Активность'),
            'partner_id' => Yii::t('common', 'Партнёр'),
            'country_id' => Yii::t('common', 'Страна'),
            'product_id' => Yii::t('common', 'Товар'),
            'paid_amount' => Yii::t('common', 'Количество'),
            'history' => Yii::t('common', 'История изменений'),
            'product_price' => Yii::t('common', 'Стоимость за единицу')
        ];
    }

    /**
     * @param $partner_id
     * @param $country_id
     * @return static[]
     */
    public static function getPackagesByPartnerCountry($partner_id, $country_id)
    {
        return Package::find()
            ->joinWith('packageGifts')
            ->where([
            'partner_id' => $partner_id,
            'country_id' => $country_id,
            'active' => Package::ACTIVE
        ])->all();
    }

    /**
     * Автоматическое создание акций
     * @param $partner_id
     * @param $country_id
     * @param $product_id
     * @param $paid_amount
     * @param $free_amount
     * @param $product_price
     * @return bool
     */
    public function create($partner_id, $country_id, $product_id, $paid_amount, $free_amount, $product_price = null)
    {
        $package = new Package();
        $package->partner_id = $partner_id;
        $package->country_id = $country_id;
        $package->product_id = $product_id;
        $package->name = $product_id . ' ' . $paid_amount . '+' . $free_amount;
        $package->paid_amount = $paid_amount;
        $package->active = 1;

        if (isset($product_price)) {
            $package->product_price = $product_price;
        }

        if ($package->save()) {
            $packageGift = new PackageGift();
            $packageGift->package_id = $package->id;
            $packageGift->product_id = $product_id;
            $packageGift->free_amount = $free_amount;

            if (!$packageGift->save()) {
                return false;
            }
            return true;
        }

        return false;
    }

    /**
     * Проверка на стандартную акцию из АдКомбо
     * @param $paid_amount
     * @param $free_amount
     * @return bool
     */
    public static function checkPackageAdCombo($paid_amount, $free_amount = 0)
    {
        foreach (self::$packageMapping as $id => $amount) {
            if ($amount['paid_amount'] == $paid_amount && $amount['free_amount'] == $free_amount) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProduct::className(), ['package_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackageGifts()
    {
        return $this->hasMany(PackageGift::className(), ['package_id' => 'id']);
    }
}
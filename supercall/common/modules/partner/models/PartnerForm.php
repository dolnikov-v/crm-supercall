<?php

namespace common\modules\partner\models;

use backend\modules\administration\models\BaseLogger;
use common\components\db\ActiveRecord;
use common\models\Country;
use common\models\LoggerTag;
use common\modules\order\models\Order;
use common\modules\partner\models\query\PartnerProductQuery;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\HttpException;

/**
 * Class PartnerForm
 * @package common\modules\partner\models
 * @property integer $id
 * @property integer $partner_id
 * @property integer $name
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $country_id
 * @property integer $type
 * @property string $buttons
 * @property float $extra_price
 * @property bool $show_shipping
 * @property bool $show_countries
 * @property bool $use_map
 * @property bool $check_address
 * @property bool $allow_gifts
 * @property string $no_answer_notification_text
 * @property string $no_answer_notification_sender
 * @property PartnerVat $vat
 * @property PartnerFormAttribute[] $attrs
 * @property PartnerFormAttribute[] $generalAttrs
 * @property PartnerFormAttribute[] $addressAttrs
 * @property boolean $mask_phones
 * @property boolean $use_foreign_delivery_broker
 * @property Partner $partner
 * @property string $virtual_buttons
 * @property boolean $use_messenger
 * @property integer $min_delivery_days
 * @property boolean $disabled_buttons
 * @property PromoProduct[] $promoProducts
 * @property PartnerFormAdditionPrice $additionalPrice
 * @property PartnerFormFastChangeQueue $fastChangeQueue
 * @property string $use_additional_params
 */
class PartnerForm extends ActiveRecord
{
    const SCOPE_GENERAL = 'general';
    const SCOPE_ADDRESS = 'address';

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    const DEFAULT_LENGTH_ADDITIONAL_PARAM = 7;

    const LOGGER_TYPE = 'partner_form';
    const LOGGER_SUB_TYPE_BASE = 'base attributes';
    const LOGGER_SUB_TYPE_PROMO = 'promo products';
    const LOGGER_SUB_TYPE_ADDITIONAL_PRICE = 'additional price';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner_form}}';
    }

    /**
     * @return PartnerProductQuery
     */
    public static function find()
    {
        return new PartnerProductQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        $this->use_additional_params = Json::encode($this->use_additional_params);

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'name', 'country_id', 'type'], 'required'],
            [['partner_id', 'active', 'min_delivery_days'], 'integer'],
            [['name', 'no_answer_notification_text'], 'string'],
            [
                ['buttons', 'virtual_buttons'],
                'filter',
                'filter' => function ($value) {
                    return is_array($value) ? implode(',', $value) : '';
                }
            ],
            [['no_answer_notification_sender'], 'string', 'max' => 15],
            ['extra_price', 'number'],
            [
                [
                    'show_shipping',
                    'allow_gifts',
                    'show_countries',
                    'use_map',
                    'use_messenger',
                    'check_address',
                    'mask_phones',
                    'use_foreign_delivery_broker',
                    'disabled_buttons'
                ],
                'boolean'
            ],
            [['use_additional_params'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'partner_id' => Yii::t('common', 'Партнер'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'partner.name' => Yii::t('common', 'Партнер'),
            'country_id' => Yii::t('common', 'Страна'),
            'country.name' => Yii::t('common', 'Страна'),
            'type' => Yii::t('common', 'Тип заказа'),
            'buttons' => Yii::t('common', 'Кнопки'),
            'virtual_buttons' => Yii::t('common', 'Псевдо статусы'),
            'extra_price' => Yii::t('common', 'Надбавка к цене заказа'),
            'show_shipping' => Yii::t('common', 'Показывать доставку'),
            'show_countries' => Yii::t('common', 'Показывать страну'),
            'use_form' => Yii::t('common', 'Показывать карту'),
            'check_address' => Yii::t('common', 'Проверка адреса'),
            'allow_gifts' => Yii::t('common', 'Подарки'),
            'mask_phones' => Yii::t('common', 'Скрывать телефоны'),
            'no_answer_notification_text' => Yii::t('common', 'Текст оповещения при недозвоне'),
            'no_answer_notification_sender' => Yii::t('common', 'Отправитель при оповещении о недозвоне'),
            'use_foreign_delivery_broker' => Yii::t('common', 'Использовать брокер служб доставок партнера'),
            'use_messenger' => Yii::t('common', 'Использовать Messenger'),
            'min_delivery_days' => Yii::t('common', 'Минимальное количество дней доставки'),
            'min_delivery_days_name_in_grid_table' => Yii::t('common', 'Мин. кол-во дней доставки'),
            'disabled_buttons' => Yii::t('common', 'Заблокировать кнопки статуса до звонка'),
            'use_additional_params' => yii::t('common', 'Передавать дополнительные параметры')
        ];
    }

    /**
     * Настраиваемые аттрибуты
     * @return string[]
     */
    public static function configurableAttributes()
    {
        return [
            'customer_full_name',
            'customer_phone',
            'customer_mobile',
            'customer_email',
        ];
    }

    /**
     * Обязательные аттрибуты, у которых нельзя снять флаг "обязательный"
     */
    public static function requiredAttributes()
    {
        return [
            'customer_full_name',
            'customer_phone',
        ];
    }

    public function ensureConfigurableAttributes()
    {
        $dummy_order = new Order();
        foreach (self::configurableAttributes() as $name) {
            /** @var PartnerFormAttribute $attr */
            $attr = ArrayHelper::getValue($this->generalAttrs, $name);
            if ($attr) {
                if (in_array($name, self::requiredAttributes()) && !$attr->required) {
                    $attr->updateAttributes(['required' => true]);
                }
                continue;
            }
            $attr = new PartnerFormAttribute();
            $attr->form_id = $this->id;
            $attr->name = $name;
            $attr->label = $dummy_order->getAttributeLabel($name);
            $attr->required = in_array($name, self::requiredAttributes());
            $attr->type = \common\widgets\ActiveField::TYPE_STRING;
            $attr->scope = 'general';
            $attr->save();
        }
        unset($this->generalAttrs);
    }

    public function afterFind()
    {
        $this->ensureConfigurableAttributes();
        $this->use_additional_params = Json::decode($this->use_additional_params);
        parent::afterFind();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        //Если у формы сменился партнёр или страна - список промо товаров должен быть обнулён
        if (isset($changedAttributes['partner_id']) || isset($changedAttributes['country_id'])) {
            if ($changedAttributes['partner_id'] != $this->partner_id || $changedAttributes['country_id'] != $this->country_id) {
                PromoProduct::deleteAll(['form_id' => $this->id]);
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVat()
    {
        return $this->hasOne(PartnerVat::className(), ['partner_id' => 'partner_id', 'country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrs()
    {
        return $this->hasMany(PartnerFormAttribute::className(), ['form_id' => 'id'])->indexBy('name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoProducts()
    {
        return $this->hasMany(PromoProduct::className(), ['form_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneralAttrs()
    {
        return $this->getAttrs()->where(['scope' => self::SCOPE_GENERAL])->orderBy([
            'sort' => SORT_ASC,
            'id' => SORT_ASC
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressAttrs()
    {
        return $this->getAttrs()->where(['scope' => self::SCOPE_ADDRESS])->orderBy([
            'sort' => SORT_ASC,
            'id' => SORT_ASC
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['form_id' => 'id']);
    }

    /**
     * Список всех доступных кнопок
     * @return array
     */
    public static function getButtonsCollection()
    {
        return [
            Order::STATUS_RECALL => Yii::t('common', 'Перезвон'),
            Order::STATUS_FAIL => Yii::t('common', 'Недозвон'),
            Order::STATUS_APPROVED => Yii::t('common', 'Одобрен'),
            Order::STATUS_REJECTED => Yii::t('common', 'Отклонен'),
            Order::STATUS_TRASH => Yii::t('common', 'Треш'),
            Order::STATUS_DOUBLE => Yii::t('common', 'Дубль'),
        ];
    }

    /**
     * Список всех доступных кнопок
     * @return array
     */
    public static function getVirtualButtonsCollection()
    {
        $labels = Order::getCheckShippingSubStatusesCollection();
        return [
            Order::SUB_STATUS_ALREADY_RECEIVED => $labels[Order::SUB_STATUS_ALREADY_RECEIVED],
            Order::SUB_STATUS_REJECT_STILL_WAITING => $labels[Order::SUB_STATUS_REJECT_STILL_WAITING],
            Order::SUB_STATUS_REJECT_NO_LONGER_NEEDED => $labels[Order::SUB_STATUS_REJECT_NO_LONGER_NEEDED],
            Order::SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER => $labels[Order::SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER],
        ];
    }

    /**
     * Список кнопок для этой формы
     * @return array
     */
    public function getButtonsConfig()
    {
        $result = [];
        $buttons = explode(',', $this->buttons);
        foreach (self::getButtonsCollection() as $status => $label) {
            if (in_array($status, $buttons)) {
                $result[$status] = $label;
            }
        }
        return $result;
    }

    /**
     * Список псевдостатусов кнопок для этой формы
     * @return array
     */
    public function getVirtualButtonsConfig()
    {
        $result = [];
        $buttons = explode(',', $this->virtual_buttons);
        foreach (self::getVirtualButtonsCollection() as $status => $label) {
            if (in_array($status, $buttons)) {
                $result[$status] = $label;
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getForms($country_id)
    {
        $forms = PartnerForm::find()
            ->select([
                Partner::tableName() . '.*',
                PartnerForm::tableName() . '.*',
                'partnerName' => Partner::tableName() . '.name',
                'partnerFormName' => PartnerForm::tableName() . '.name',
            ])
            ->where([PartnerForm::tableName() . '.active' => 1])
            ->andWhere([PartnerForm::tableName() . '.country_id' => $country_id])
            ->leftJoin(Partner::tableName(), Partner::tableName() . '.id=' . PartnerForm::tableName() . '.partner_id')
            ->asArray()->all();

        $form_ids = [];

        foreach ($forms as $form) {
            $form_ids[$form['country_id']][$form['id']] = [
                'name' => $form['partnerFormName'] . ' (' . $form['partnerName'] . ')'
            ];
        }

        return $form_ids;
    }

    /**
     * @param $partner_id
     * @return PartnerProduct[]
     */
    public static function getPartnerCountries($partner_id)
    {

        return PartnerForm::find()
            ->distinct()
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . PartnerForm::tableName() . '.country_id')
            ->where(['partner_id' => $partner_id])
            ->andWhere([PartnerForm::tableName() . '.active' => PartnerForm::ACTIVE])
            ->andWhere([Country::tableName() . '.active' => Country::ACTIVE])
            ->all();
    }

    /**
     * @param $id
     * @return bool
     */
    public static function useMap($id)
    {
        return PartnerForm::findOne(['id' => $id])->use_map;
    }

    /**
     * @param $order
     * @return static
     * @throws HttpException
     */
    public static function getFormByOrder($order, $type = 1)
    {
        if ($order instanceof Order) {
            if (!$order->form_id) {
                $form = PartnerForm::findOne([
                    'id' => $order->form_id,
                    'active' => PartnerForm::ACTIVE,
                    'type' => $type
                ]);
            } else {
                $form = PartnerForm::findOne([
                    'country_id' => $order->country_id,
                    'partner_id' => $order->partner_id,
                    'active' => PartnerForm::ACTIVE,
                    'type' => $type
                ]);

                if (is_null($form)) {
                    throw new HttpException(404, Yii::t('common', 'Активная форма не найдена'));
                }
            }

            return $form;
        } else {
            throw new HttpException(403, Yii::t('common', 'Объект не является заказом'));
        }
    }

    public function getAdditionalPrice()
    {
        return $this->hasOne(PartnerFormAdditionPrice::className(), ['form_id' => 'id']);
    }

    public function getFastChangeQueue()
    {
        return $this->hasOne(PartnerFormFastChangeQueue::className(), ['form_id' => 'id']);
    }

    /**
     * @param $modelAttributes
     * @param $post
     */
    public static function saveOnLogAdditionalPrice($modelAttributes, $post)
    {
        unset($modelAttributes['id']);
        unset($modelAttributes['created_at']);
        unset($modelAttributes['updated_at']);

        $post['PartnerFormAdditionPrice']['form_id'] = $post['form_id'];

        $oldAttributes = $modelAttributes;
        $newAttributes = $post['PartnerFormAdditionPrice'];

        $oldAttributes['condition'] = PartnerFormAdditionPrice::mapCondition($oldAttributes['condition']);
        $newAttributes['condition'] = PartnerFormAdditionPrice::mapCondition($newAttributes['condition']);

        ksort($oldAttributes);
        ksort($newAttributes);

        self::saveOnLog($post['form_id'], self::LOGGER_SUB_TYPE_ADDITIONAL_PRICE, $oldAttributes, $newAttributes);
    }

    /**
     * @param $form_id
     * @param $oldPromoProducts
     * @param $newPromoProducts
     */
    public static function saveOnLogPromoProducts($form_id, $oldPromoProducts, $newPromoProducts)
    {
        self::saveOnLog($form_id, self::LOGGER_SUB_TYPE_PROMO, $oldPromoProducts, $newPromoProducts);
    }

    /**
     * @param $modelAttributes
     * @param $post
     */
    public static function saveOnLogBaseAttributes($form_id, $modelAttributes, $post)
    {
        unset($modelAttributes['id']);
        unset($modelAttributes['active']);
        unset($modelAttributes['use_additional_params']);
        unset($modelAttributes['created_at']);
        unset($modelAttributes['updated_at']);

        $oldAttributes = $modelAttributes;
        $newAttributes = $post['PartnerForm'];
        $newAttributes['buttons'] = is_array($newAttributes['buttons']) ? implode(",", $newAttributes['buttons']) : $newAttributes['buttons'];

        ksort($oldAttributes);
        ksort($newAttributes);

        self::saveOnLog($form_id, self::LOGGER_SUB_TYPE_BASE, $oldAttributes, $newAttributes);
    }

    /**
     * @param $form_id
     * @param $oldData
     * @param $newData
     */
    public static function saveOnLog($form_id, $type, $oldData, $newData)
    {
        $oldData = Json::encode($oldData);
        $newData = Json::encode($newData);

        if($oldData != $newData) {
            $logger = yii::$app->get('logger');
            $logger->action = 'edit-partner-form';
            $logger->type = self::LOGGER_TYPE;
            $logger->tags = [
                'user_id' => yii::$app->user->identity->id,
                'username' => yii::$app->user->identity->username,
                'form_id' => $form_id,
                'type' => $type,
                'old_data' => $oldData,
                'new_data' => $newData,
            ];
            $logger->error = null;
            $logger->save();
        }
    }

    public function getHistory()
    {
        $subQueryHistory = BaseLogger::find()
            ->select(['log_id'])
            ->leftJoin(LoggerTag::tableName(), LoggerTag::tableName() . '.log_id=' . BaseLogger::tableName() . '.id')
            ->where([
                'tag' => 'form_id',
                'type' => PartnerForm::LOGGER_TYPE,
                'value' => (string)$this->id
            ]);

        $queryHistory = BaseLogger::find()
            ->leftJoin(LoggerTag::tableName(), LoggerTag::tableName() . '.log_id=' . BaseLogger::tableName() . '.id')
            ->where([BaseLogger::tableName() . '.id' => $subQueryHistory])
            ->orderBy([BaseLogger::tableName() . '.id' => SORT_DESC]);

        $queryBase = clone $queryHistory;
        $queryAdditionalPrice = clone $queryHistory;
        $queryPromo = clone $queryHistory;

        $logBase = $queryBase->andWhere([
            'tag' => 'type',
            'value' => PartnerForm::LOGGER_SUB_TYPE_BASE
        ]);

        $logAdditionalPrice = $queryAdditionalPrice->andWhere([
            'tag' => 'type',
            'value' => PartnerForm::LOGGER_SUB_TYPE_ADDITIONAL_PRICE
        ]);

        $logPromoProducts = $queryPromo->andWhere([
            'tag' => 'type',
            'value' => PartnerForm::LOGGER_SUB_TYPE_PROMO
        ]);

        return [
            'base' => $logBase,
            'additionalPrice' => $logAdditionalPrice,
            'promoProducts' => $logPromoProducts
        ];
    }
}

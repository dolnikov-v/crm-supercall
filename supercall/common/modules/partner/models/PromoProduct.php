<?php

namespace common\modules\partner\models;

use common\models\Product;
use common\modules\partner\models\query\PromoProductQuery;
use Yii;

/**
 * This is the model class for table "promo_product".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $form_id
 * @property double $price
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PartnerForm $form
 * @property PartnerProduct $product
 */
class PromoProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'form_id'], 'required'],
            [['product_id', 'form_id', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            [['form_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnerForm::className(), 'targetAttribute' => ['form_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'product_id' => Yii::t('common', 'Продукт'),
            'form_id' => Yii::t('common', 'Форма'),
            'price' => Yii::t('common', 'Цена'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлёно'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(PartnerForm::className(), ['id' => 'form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return PromoProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromoProductQuery(get_called_class());
    }
}

<?php
namespace common\modules\partner\models\query;

use common\components\db\ActiveQuery;
use common\modules\partner\models\Package;

/**
 * Class PackageQuery
 * @package common\modules\partner\models\query
 */
class PackageQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['active' => Package::ACTIVE]);
        return $this;
    }

    /**
     * @param $partner_id
     * @return $this
     */
    public function byPartner($partner_id)
    {
        $this->andWhere(['partner_id' => $partner_id]);
        return $this;
    }

    /**
     * @param $country_id
     * @return $this
     */
    public function byCountry($country_id)
    {
        $this->andWhere(['country_id' => $country_id]);
        return $this;
    }
}
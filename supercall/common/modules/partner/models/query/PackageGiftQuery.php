<?php

namespace common\modules\partner\models\query;

use common\modules\partner\models\PackageGift;
use common\components\db\ActiveQuery;

class PackageGiftQuery extends ActiveQuery
{
    /**
     * @return PackageGift[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @return PackageGift|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
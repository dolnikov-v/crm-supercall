<?php

namespace common\modules\partner\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\partner\models\PromoProduct]].
 *
 * @see \common\modules\partner\models\PromoProduct
 */
class PromoProductQuery extends \yii\db\ActiveQuery
{
    public function byForm($form_id)
    {
        return $this->andWhere(['form_id' => $form_id]);
    }

    /**
     * @inheritdoc
     * @return \common\modules\partner\models\PromoProduct[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\partner\models\PromoProduct|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

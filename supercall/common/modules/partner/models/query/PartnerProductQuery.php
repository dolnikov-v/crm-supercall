<?php
namespace common\modules\partner\models\query;

use common\components\db\ActiveQuery;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerProduct;

/**
 * Class PartnerProductQuery
 * @package common\modules\partner\models\query
 * @method PartnerProduct one($db = null)
 * @method PartnerProduct[] all($db = null)
 */
class PartnerProductQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'product.id';

    /**
     * @var string
     */
    protected $collectionValue = 'product.name';

    /**
     * @var string
     */
    protected $groupValue = 'partner.name';

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([PartnerProduct::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function activeForm()
    {
        return $this->andWhere([PartnerForm::tableName() . '.active' => PartnerForm::ACTIVE]);
    }

    /**
     * @return $this
     */
    public function notActiveForm()
    {
        return $this->andWhere([PartnerForm::tableName() . '.active' => PartnerForm::NOT_ACTIVE]);
    }

    /**
     * @param string $partnerId
     * @return $this
     */
    public function byPartner($partnerId)
    {
        $this->andWhere(['partner_id' => $partnerId]);

        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function byId($id)
    {
        $this->andWhere(['id' => $id]);

        return $this;
    }

    /**
     * @param $countryId
     * @return $this
     */
    public function byCountry($countryId)
    {
        $this->andWhere(['country_id' => $countryId]);

        return $this;
    }

    /**
     * @param string $productId
     * @return $this
     */
    public function byPartnerProduct($productId)
    {
        $this->andWhere(['partner_product_id' => $productId]);

        return $this;
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere([PartnerProduct::tableName() . '.active' => 0]);
    }
}

<?php
namespace common\modules\partner\models\query;

use common\components\db\ActiveQuery;
use common\modules\partner\models\PartnerShipping;

/**
 * Class PartnerShippingQuery
 * @package common\modules\partner\models\query
 * @method PartnerShipping one($db = null)
 * @method PartnerShipping[] all($db = null)
 */
class PartnerShippingQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'shipping.id';

    /**
     * @var string
     */
    protected $collectionValue = 'shipping.name';

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([PartnerShipping::tableName() . '.active' => 1]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere([PartnerShipping::tableName() . '.active' => 0]);
    }
}

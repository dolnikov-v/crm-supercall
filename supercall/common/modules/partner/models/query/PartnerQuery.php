<?php

namespace common\modules\partner\models\query;

use common\components\db\ActiveQuery;
use common\models\User;
use common\models\UserPartner;
use common\modules\partner\models\Partner;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class PartnerQuery
 * @package common\modules\partner\models\query
 * @method Partner one($db = null)
 * @method Partner[] all($db = null)
 */
class PartnerQuery extends ActiveQuery
{
    /**
     * @param string $apiKey
     * @return $this
     */
    public function byApiKey($apiKey)
    {
        $this->andWhere(['api_key' => $apiKey]);

        return $this;
    }

    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['partner.active' => 1]);

        return $this;
    }

    /**
     * @param integer|array $partner_id
     * @return $this
     */
    public function byPartner($partner_id)
    {
        if (Yii::$app->user->isSuperadmin) {
            $partner_id = ArrayHelper::getColumn(Partner::findAll(['active' => Partner::ACTIVE]), 'id');
        } else {
            $partner_id = $this->toArray($partner_id);
        }

        return $this
            ->leftJoin(UserPartner::tableName(), UserPartner::tableName() . '.user_id=' . User::tableName() . '.id')
            ->andWhere(['partner_id' => $partner_id]);
    }

    /**
     * @return $this
     */
    public function byIdentityUser()
    {
        if (yii::$app->user->isSuperadmin) {
            return $this;
        }

        return $this
            ->leftJoin(UserPartner::tableName(), UserPartner::tableName() . '.user_id=' . yii::$app->user->identity->id)
            ->andWhere(['is not', UserPartner::tableName() . '.partner_id', null]);
    }
}

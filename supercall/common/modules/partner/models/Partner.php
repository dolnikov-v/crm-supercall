<?php

namespace common\modules\partner\models;

use common\components\db\ActiveRecord;
use common\models\Country;
use common\models\Shipping;
use common\modules\order\models\Order;
use common\modules\partner\models\query\PartnerQuery;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;

/**
 * Class Partner
 * @package common\modules\partner\models
 * @property integer $id
 * @property string $name
 * @property string $active
 * @property string $api_key
 * @property string $url_product
 * @property string $url_queue
 * @property string $token
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $url_foreign_delivery_broker
 * @property PartnerProduct[] $partnerProducts
 * @property string $buyout_url
 * @property string $header_auth
 */
class Partner extends ActiveRecord
{
    const PARTNER_2WTRADE = '2wtrade';
    const PARNTER_EUROPAY = 'europay.co';

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partner}}';
    }

    public function beforeSave($insert)
    {
        $this->token = sha1($this->name);
        return parent::beforeSave($insert);
    }

    /**
     * @return PartnerQuery
     */
    public static function find()
    {
        return new PartnerQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            [['name', 'api_key', 'url_product', 'token', 'buyout_url', 'header_auth'], 'string'],
            [['url_queue', 'url_foreign_delivery_broker'], 'url'],
            ['active', 'integer'],
            ['active', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Активность'),
            'api_key' => Yii::t('common', 'API ключ'),
            'url_product' => Yii::t('common', 'URL API продуктов'),
            'url_queue' => Yii::t('common', 'URL очереди'),
            'token' => Yii::t('common', 'Токен'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'url_foreign_delivery_broker' => Yii::t('common', 'URL API брокера служб доставок'),
            'buyout_url' => Yii::t('common', 'Ссылка на API выкупы'),
            'header_auth' => Yii::t('common', 'HTTP Header авторизация'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettings()
    {
        return $this->hasMany(PartnerSettings::className(), ['partner_id' => 'id']);
    }

    /**
     * @param $order
     * @return PartnerSettings
     */
    public function getSettingsByOrder($order)
    {
        $settings = $this->getSettings()
            ->where([
                'country_id' => $order->country_id,
                'partner_id' => $order->partner_id,
                'type_id' => $order->type_id,
            ])
            ->one();

        if (!$settings) {
            $settings = $this->getDefaultSettings();
            $settings->country_id = $order->country_id;
            $settings->partner_id = $order->partner_id;
            $settings->type_id = $order->type_id;
        }
        return $settings;
    }

    /**
     * @return PartnerSettings
     */
    public function getDefaultSettings()
    {
        $settings = new PartnerSettings();
        $settings->partner_id = $this->id;
        return $settings;
    }

    /**
     * @param $token
     * @return Partner
     */
    public static function getPartnerByToken($token)
    {
        return Partner::find()->where(['token' => $token])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerProducts()
    {
        return $this->hasMany(PartnerProduct::className(), ['partner_id' => 'id']);
    }

    /**
     * Получение списка КС от брокера партнера
     * @param Order $order
     * @param array $params
     * @return boolean|array
     */
    public function getShippingListByBroker(Order $order, $params)
    {
        $data = ['country' => $order->country->char_code];
        if (isset($params['customer_zip'])) {
            $data['customer_zip'] = $params['customer_zip'];
        }

        $quantity = 0;

        if (isset($params['products'])) {
            $data['products'] = [];
            $partnerProducts = ArrayHelper::map($this->getPartnerProducts()
                ->andWhere(['country_id' => $order->country_id])
                ->all(), 'product_id', 'partner_product_id');
            foreach ($params['products'] as $product) {

                if (!empty($product['id'])) {
                    $data['products'][] = array_merge($product, ['product_id' => $partnerProducts[$product['id']]]);
                }

                $quantity = array_sum(ArrayHelper::getColumn($data['products'], function($val){return $val['gift'] == 1 ? 0 : $val['price'] * $val['quantity'];}));
            }
        }

        $data['order_price'] = $quantity;

        $headers = array();
        $headers[] = 'Authorization: Bearer ' . base64_encode($this->api_key);
        $ch = curl_init($this->url_foreign_delivery_broker);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($info['http_code'] == 200) {
            $response = json_decode($response, true);
            if ($response['status'] != 'success') {
                return false;
            }

            $result = [];
            foreach ($response['data'] as $delivery) {
                $result[] = [
                    'id' => $delivery['id'],
                    'name' => $delivery['name'],
                    'price' => $delivery['delivery_price'] ?? 0,
                    'have_express' => ($delivery['express'] ?? false) ? true : false,
                    'country' => $order->country->char_code,
                    'min_delivery_date' => isset($delivery['min_delivery_days']) ? date('Y-m-d', strtotime('+' . max(0, intval($delivery['min_delivery_days'])) . 'days')) : null,
                    'max_delivery_date' => isset($delivery['max_delivery_days']) ? date('Y-m-d', strtotime('+' . max(0, intval($delivery['max_delivery_days'])) . 'days')) : null,
                ];
            }

            $this->insertNewPartnerDeliveries($result);
            return array_map(function ($item) use ($order) {
                $item['partner_id'] = $item['id'];
                $shipping = Shipping::find()
                    ->innerJoin(PartnerShipping::tableName(), [PartnerShipping::tableName() . '.shipping_id' => new Expression(Shipping::tableName() . '.id')])
                    ->where([
                        PartnerShipping::tableName() . '.partner_shipping_id' => $item['partner_id'],
                        PartnerShipping::tableName() . '.partner_id' => $this->id
                    ])
                    ->one();
                $item['id'] = $shipping->id;
                $item['name'] = $shipping->name;
                return $item;
            }, $result);
        }

        return false;
    }

    /**
     * Вставка новых связок Служба доставки партнера - Служба доставки КЦ
     * @param array $deliveries
     * @throws \Throwable
     */
    protected function insertNewPartnerDeliveries($deliveries)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($deliveries as $delivery) {
                if (!PartnerShipping::find()
                    ->where(['partner_id' => $this->id, 'partner_shipping_id' => $delivery['id']])
                    ->exists()
                ) {
                    $shipping = Shipping::find()->where(['name' => $delivery['name']])->one();
                    if (!$shipping) {
                        $shipping = new Shipping([
                            'name' => $delivery['name'],
                        ]);
                        if (!$shipping->save()) {
                            throw new \Exception($shipping->getFirstErrorAsString());
                        }
                    }
                    if (!isset($delivery['country']) || !($country = Country::find()
                            ->where(['char_code' => $delivery['country']])->one())
                    ) {
                        throw new \Exception(Yii::t('common', 'Страна не найдена.'));
                    }
                    $partnerShipping = new PartnerShipping([
                        'partner_id' => $this->id,
                        'partner_shipping_id' => $delivery['id'],
                        'shipping_id' => $shipping->id,
                        'active' => 1,
                        'country_id' => $country->id,
                        'price' => 0,
                    ]);
                    if (!$partnerShipping->save()) {
                        throw new \Exception($partnerShipping->getFirstErrorAsString());
                    }
                }
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}

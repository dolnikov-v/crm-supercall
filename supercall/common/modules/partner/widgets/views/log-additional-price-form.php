<?php

/** @var \yii\db\Query $query */

use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\widgets\base\Panel;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;

echo Panel::widget([
    'title' => false,
    'content' => GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]),
        'columns' => [
            [
                'attribute' => 'changes',
                'label' => yii::t('common', 'Изменения'),
                'content' => function ($model) {
                    $tags = Json::decode($model->tags, false);
                    $old = Json::decode($tags->old_data);
                    $new = Json::decode($tags->new_data);
                    $attributes = [];

                    foreach ($old as $attr => $value) {
                        if (isset($new[$attr]) && $new[$attr] != $value) {
                            $attributes[] = [
                                'name' => $attr,
                                'old' => is_null($value) || empty($value) ? 0 : $value,
                                'new' => is_null($new[$attr]) || empty($new[$attr]) ? 0 : $new[$attr],
                            ];
                        }
                    }

                    return GridView::widget([
                        'dataProvider' => new ArrayDataProvider([
                            'allModels' => $attributes,
                            'pagination' => false
                        ]),
                        'columns' => [
                            [
                                'attribute' => 'name',
                                'label' => yii::t('common', 'Атрибут'),
                            ],
                            [
                                'attribute' => 'new',
                                'headerOptions' => [
                                    'class' => 'width-200'
                                ],
                                'label' => yii::t('common', 'Новое значение'),
                            ],
                            [
                                'attribute' => 'old',
                                'headerOptions' => [
                                    'class' => 'width-200'
                                ],
                                'label' => yii::t('common', 'Старое значение'),
                            ],
                        ]
                    ]);
                }
            ],
            [
                'attribute' => 'user_id',
                'content' => function ($model) {
                    $tags = Json::decode($model->tags, false);
                    return $tags->user_id . (isset($tags->username) ? ' - ' . $tags->username : '');
                }
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
                'content' => function ($model) {
                    return yii::$app->formatter->asDatetime($model->created_at);
                }
            ],
        ]
    ])
]);
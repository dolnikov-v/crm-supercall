<?php

/** @var \yii\db\Query $query */

use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\widgets\base\Panel;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;

echo Panel::widget([
    'title' => false,
    'content' => GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]),
        'columns' => [
            [
                'attribute' => 'changes',
                'label' => yii::t('common', 'Изменения'),
                'content' => function ($model) {
                    $tags = Json::decode($model->tags, false);
                    $old = Json::decode($tags->old_data);
                    $new = Json::decode($tags->new_data);
                    $attributesOld = [];
                    $attributesNew = [];

                    foreach ($old as $value) {
                        $attributesOld[] = [
                            'product_id' => $value['product_id'],
                            'price' => $value['price']
                        ];
                    }

                    foreach ($new as $value) {
                        $attributesNew[] = [
                            'product_id' => $value['product_id'],
                            'price' => $value['price']
                        ];
                    }

                    return $this->render('promo-product-data', [
                            'data' => $attributesNew,
                            'title' => yii::t('common', 'Новые данные')
                        ])
                        . $this->render('promo-product-data', [
                            'data' => $attributesOld,
                            'title' => yii::t('common', 'Старые данные')
                        ]);
                }
            ],
            [
                'attribute' => 'user_id',
                'content' => function ($model) {
                    $tags = Json::decode($model->tags, false);
                    return $tags->user_id . (isset($tags->username) ? ' - ' . $tags->username : '');
                }
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
                'content' => function ($model) {
                    return yii::$app->formatter->asDatetime($model->created_at);
                }
            ],
        ]
    ])
]);
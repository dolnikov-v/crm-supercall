<?php

use common\components\grid\GridView;
use yii\data\ArrayDataProvider;

/** @var string $title */
/** @var array $data */
?>

<?php echo '<b>' . $title . '</b><br/>'; ?>

<?= GridView::widget([
    'dataProvider' => new ArrayDataProvider([
        'allModels' => $data,
        'pagination' => false
    ]),
    'columns' => [
        [
            'attribute' => 'product_id',
            'headerOptions' => [
                'class' => 'width-200'
            ],
            'label' => yii::t('common', 'product_id'),
        ],
        [
            'attribute' => 'price',
            'headerOptions' => [
                'class' => 'width-200'
            ],
            'label' => yii::t('common', 'Цена'),
        ],
    ]
])
?>

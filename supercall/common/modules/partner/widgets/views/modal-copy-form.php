<?php
use common\modules\partner\assets\widgets\ModalCopyFormAsset;
use common\modules\partner\models\PartnerFormCopy;
use common\widgets\ActiveForm;
use common\widgets\base\Button;

/** @var string $id */
/** @var string $title */
/** @var string $description */
/** @var boolean $visible */
/** @var string $color */
/** @var string $body */
/** @var PartnerFormCopy $model */
/** @var string $bodyStyle */
/** @var string $footer */
/** @var string $footerStyle */
/** @var string $labelOk */
/** @var string $labelCancel */

ModalCopyFormAsset::register($this);
?>

<div class="modal fade <?= $visible ? 'visible':'' ?> <?= $color ?>" id="<?= $id ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin(['encodeErrorSummary' => false]); ?>
            <div class="color-line"></div>
            <?php if ($title || $description) : ?>
                <div class="modal-header text-center">
                    <h4 class="modal-title"><?= $title ?></h4>
                    <small class="font-bold"><?= $description ?></small>
                </div>
            <?php endif; ?>
            <div class="modal-body <?= $bodyStyle ?>">
                <?= $form->errorSummary($model, ['header' => '']) ?>
                <?= $form->field($model, 'form', ['template' => '{input}'])->hiddenInput(); ?>
                <p><?= $model->getAttributeLabel('partners'); ?></p>
                <?php foreach ($model->partnerOptions() as $id => $name) {
                    echo $form->field($model, 'partners[' . $id . ']')->checkbox(['label' => $name, 'value' => $name]);
                } ?>
                <p><?= $model->getAttributeLabel('countries'); ?></p>
                <?php foreach ($model->countryOptions() as $id => $name) {
                    echo $form->field($model, 'countries[' . $id . ']')->checkbox(['label' => $name, 'value' => $name]);
                } ?>
                <?= $body ?>
            </div>
            <?php if ($footer): ?>
                <?= $footer ?>
            <?php else: ?>
                <div class="modal-footer <?= $footerStyle ?>">
                    <?= Button::widget([
                        'label' => Yii::t('common', $labelCancel),
                        'size' => Button::SIZE_SMALL,
                        'attributes' => [
                            'data-dismiss' => 'modal',
                        ],
                    ]) ?>

                    <?= Button::widget([
                        'id' => 'modal_confirm_delete_link',
                        'label' => Yii::t('common', $labelOk),
                        'style' => Button::STYLE_PRIMARY,
                        'size' => Button::SIZE_SMALL,
                        'type' => 'submit'
                    ]) ?>
                </div>
            <?php endif; ?>
            <?php $form->end(); ?>
        </div>
    </div>
</div>

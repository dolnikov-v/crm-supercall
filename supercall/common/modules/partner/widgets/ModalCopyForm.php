<?php

namespace common\modules\partner\widgets;

use common\widgets\base\Modal;
use Yii;

class ModalCopyForm extends Modal
{
    /**
     * @var
     */
    public $model;

    public function run()
    {
        return $this->render('modal-copy-form', [
            'id' => 'modal_copy_form',
            'visible' => $this->model->hasErrors(),
            'color' => self::COLOR_DANGER,
            'title' => Yii::t('common', 'Копирование формы'),
            'description' => Yii::t('common', 'Копирование формы'),
            'body' => $this->body,
            'model' => $this->model,
            'bodyStyle' => $this->bodyStyle,
            'footer' => $this->footer,
            'footerStyle' => $this->footerStyle,
            'labelOk' => Yii::t('common', 'Копировать'),
            'labelCancel' => $this->labelCancel,
        ]);
    }
}
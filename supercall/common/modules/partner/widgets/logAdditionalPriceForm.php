<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 06.01.2019
 * Time: 20:11
 */

namespace common\modules\partner\widgets;

use yii\base\Widget;

/**
 * Class logAdditionalPriceForm
 * @package common\modules\partner\widgets
 */
class logAdditionalPriceForm extends Widget
{
    public $query;

    public function run()
    {
        return $this->render('log-additional-price-form', [
            'query' => $this->query
        ]);
    }
}

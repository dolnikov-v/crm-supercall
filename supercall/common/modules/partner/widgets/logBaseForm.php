<?php

namespace common\modules\partner\widgets;

use yii\base\Widget;

/**
 * Class logBaseForm
 * @package common\modules\partner\widgets
 */
class logBaseForm extends Widget
{
    public $query;

    public function run()
    {
        return $this->render('log-base-form', [
            'query' => $this->query,
        ]);
    }
}
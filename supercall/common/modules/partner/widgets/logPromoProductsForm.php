<?php
/**
 * Created by PhpStorm.
 * User: Igor
 * Date: 06.01.2019
 * Time: 20:13
 */

namespace common\modules\partner\widgets;

use yii\base\Widget;

/**
 * Class logPromoProductsForm
 * @package common\modules\partner\widgets
 */
class logPromoProductsForm extends Widget
{
    public $query;

    public function run()
    {
        return $this->render('log-promo-products-form', [
            'query' => $this->query
        ]);
    }
}

<?php
namespace common\modules\partner\helpers;

use common\modules\partner\models\PartnerProduct as PartnerProductModel;
use Yii;
use yii\base\Object;

/**
 * Class PartnerProduct
 * @package common\modules\partner\helpers
 */
class PartnerProduct extends Object
{
    const CACHE_KEY = 'PartnerProduct.mapping#{id}';

    /**
     * @param integer $partnerId
     * @return string
     */
    public static function getCacheKey($partnerId)
    {
        return strtr(self::CACHE_KEY, ['{id}' => $partnerId]);
    }

    /**
     * По id-товара у партнера возвращает наш id-товара
     * @param integer $partnerId
     * @param integer $partnerProductId
     * @param boolean|null $active
     * @return null|integer
     */
    public static function get($partnerId, $partnerProductId, $active = true, $country_id = 0)
    {
        if($country_id > 0){
            $mapping = self::getMapping($partnerId, $country_id);
        } else {
            $mapping = self::getMapping($partnerId);
        }

        foreach ($mapping as $mappingProductId => $info) {
            if (is_null($active)) {
                if ($info['product_id'] == $partnerProductId) {
                    return $mappingProductId;
                }
            } else {
                if ($info['product_id'] == $partnerProductId && $info['active'] == $active) {
                    return $mappingProductId;
                }
            }
        }

        return null;
    }

    /**
     * По нашему id-товара возвращает id-товара у парнетра
     * @param integer $partnerId
     * @param integer $productId
     * @param boolean|null $active
     * @return int|null|string
     */
    public static function getForeign($partnerId, $productId, $active = true)
    {
        $mapping = self::getMapping($partnerId);

        foreach ($mapping as $mappingProductId => $info) {
            if (is_null($active)) {
                if ($mappingProductId == $productId) {
                    return $info['product_id'];
                }
            } else {
                if ($mappingProductId == $productId && $info['active'] == $active) {
                    return $info['product_id'];
                }
            }
        }

        return null;
    }

    /**
     * @param integer $partnerId
     * @return array
     */
    public static function getMapping($partnerId, $country_id = false)
    {
        $cacheKey = self::getCacheKey($partnerId);
        $mapping = Yii::$app->cache->get($cacheKey);

        //if ($mapping === false) {
            $mapping = [];

            $condition = ['partner_id' => $partnerId];

            if($country_id && is_numeric($country_id)){
                $condition['country_id'] = $country_id;
            }

            $items = PartnerProductModel::find()
                ->select([
                    'id',
                    'partner_product_id',
                    'product_id',
                    'active',
                ])
                ->where($condition)
                ->asArray()
                ->all();

            foreach ($items as $item) {
                $mapping[$item['product_id']] = [
                    'product_id' => $item['partner_product_id'],
                    'active' => $item['active'],
                ];
            }

            //Yii::$app->cache->set($cacheKey, $mapping, 3600);
        //}

        return $mapping;
    }
}

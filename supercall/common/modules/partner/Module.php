<?php
namespace common\modules\partner;

use backend\components\base\Module as CommonModule;

/**
 * Class Module
 * @package common\modules\partner
 */
class Module extends CommonModule
{
    public $controllerNamespace = 'common\modules\partner\controllers';

}

<?php

use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\modules\partner\models\PartnerProduct;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \common\modules\partner\models\search\PartnerProductSearch $modelSearch */
/** @var array $partners */
/** @var array $products */
/** @var array $countries */

$this->title = Yii::t('common', 'Товары');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнеры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'partners' => $partners,
        'products' => $products,
        'countries' => $countries,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с товарами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function (PartnerProduct $model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            'partner.name',
            [
                'attribute' => 'country',
                'content' => function ($data) {
                    return isset($data->country) ? $data->country->name : '-';
                }
            ],
            [
                'attribute' => 'partner_product_id',
                'enableSorting' => false,
            ],
            'product.name',
            [
                'attribute' => 'price_data',
                'headerOptions' => ['class' => 'width-50 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerProduct $model) {
                    $price = ArrayHelper::getValue($model->price_data, 'price');
                    return is_array($price) ? json_encode(ArrayHelper::getValue($price, 'table'), 256) : $price;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerProduct $model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function (PartnerProduct $model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.product.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function (PartnerProduct $model) {
                            return Url::toRoute(['/partner/product/activate', 'id' => $model->id]);
                        },
                        'can' => function (PartnerProduct $model) {
                            return Yii::$app->user->can('partner.product.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function (PartnerProduct $model) {
                            return Url::toRoute(['/partner/product/deactivate', 'id' => $model->id]);
                        },
                        'can' => function (PartnerProduct $model) {
                            return Yii::$app->user->can('partner.product.deactivate') && $model->active;
                        }
                    ]
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('partner.product.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить товар'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

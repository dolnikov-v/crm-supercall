<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use common\modules\partner\models\PartnerProduct;
use common\widgets\base\Button;
use common\modules\partner\assets\product\TablePriceProductAsset;

TablePriceProductAsset::register($this);


/** @var \common\modules\partner\models\PartnerProduct $model */
/** @var array $partners */
/** @var array $products */
/** @var array $countries */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'partner_id')->select2List($partners, ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'country_id')->select2List($countries, ['prompt' => '—']); ?>
    </div>

    <div class="col-lg-3">
        <?= $form->field($model, 'partner_product_id')->textInput(); ?>
    </div>

    <div class="col-lg-3">
        <?= $form->field($model, 'product_id')->select2List($products, ['prompt' => '—']); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'price_data_type')->select2List(PartnerProduct::getPriceDataTypeCollection(), [
            'value' => isset($model->price_data['type']) ? $model->price_data['type'] : 2,
            'id' => 'price_data_type'
        ]); ?>
    </div>

    <div class="col-lg-3" id="price">
        <?= $form->field($model, 'price_data')->textInput(['value' => $model->getPriceData()]); ?>
    </div>

</div>

<br>

<h5 class="hidden"><?= Yii::t('common', 'Таблица цен'); ?></h5>

<br>

<div class="group_table_price <?=(in_array($model->price_data['type'], [PartnerProduct::PRICE_DATA_TYPE_TABLE, null]) && !$model->isNewRecord) ? '' : 'hidden';?>" id="group_table_price">

    <?php if ($model->isNewRecord || $model->price_data['type'] == PartnerProduct::PRICE_DATA_TYPE_STATIC) : ?>

        <div class="row table_price">

            <div class="col-lg-2">
                <?= $form->field($model, 'count_products')->select2List(PartnerProduct::getPriceCountCollection(), ['id' => false, 'name' => 'PartnerProduct[count_products][]'])->label(false); ?>
            </div>
            <div class="col-lg-2">
                <?= $form->field($model, 'table_price')->textInput(['value' => 0, 'name' => 'PartnerProduct[table_price][]'])->label(false); ?>
            </div>

            <div class="col-lg-2">
                <?= Button::widget([
                    'type' => 'button',
                    'icon' => 'glyphicon glyphicon-minus',
                    'style' => Button::STYLE_DANGER . ' btn-table-price-minus',
                ]) ?>

                <?= Button::widget([
                    'type' => 'button',
                    'icon' => 'glyphicon glyphicon-plus',
                    'style' => Button::STYLE_SUCCESS . ' btn-table-price-plus',
                ]) ?>
            </div>

        </div>

    <?php else : ?>

        <?php if ($model->price_data['type'] == PartnerProduct::PRICE_DATA_TYPE_TABLE) : ?>

            <?php foreach ($model->price_data['table'] as $count => $price) : ?>

                <div class="row table_price">

                    <div class="col-lg-2">
                        <?= $form->field($model, 'count_products')->select2List(PartnerProduct::getPriceCountCollection(), [
                            'id' => false,
                            'name' => 'PartnerProduct[count_products][]',
                            'value' => $count
                        ])->label(false); ?>
                    </div>
                    <div class="col-lg-2">
                        <?= $form->field($model, 'table_price')->textInput(['value' => $price, 'name' => 'PartnerProduct[table_price][]'])->label(false); ?>
                    </div>

                    <div class="col-lg-2">
                        <?= Button::widget([
                            'type' => 'button',
                            'icon' => 'glyphicon glyphicon-minus',
                            'style' => Button::STYLE_DANGER . ' btn-table-price-minus',
                        ]) ?>

                        <?= Button::widget([
                            'type' => 'button',
                            'icon' => 'glyphicon glyphicon-plus',
                            'style' => Button::STYLE_SUCCESS . ' btn-table-price-plus',
                        ]) ?>
                    </div>

                </div>

            <?php endforeach; ?>

        <?php endif; ?>

    <?php endif; ?>

</div>

<br>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить товар') : Yii::t('common', 'Сохранить товар')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

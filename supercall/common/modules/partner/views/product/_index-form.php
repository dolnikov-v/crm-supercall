<?php
use common\helpers\Collection;
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Country;

/** @var \common\modules\partner\models\search\PartnerProductSearch $modelSearch */
/** @var array $partners */
/** @var array $products */
/** @var array $countries */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute('index'), 'method' => 'get']); ?>
<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($modelSearch, 'partner')->select2List($partners, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($modelSearch, 'country')->select2List($countries, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($modelSearch, 'product')->select2List($products, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($modelSearch, 'active')->select2List(Collection::getActiveStatuses(), [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

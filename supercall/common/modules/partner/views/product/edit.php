<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\PartnerProduct $model */
/** @var array $partners */
/** @var array $products */
/** @var array $countries */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление товара') : Yii::t('common', 'Редактирование товара');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнер'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/partner/product/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Товар'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'partners' => $partners,
        'products' => $products,
        'countries' => $countries,
    ])
]) ?>

<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\modules\partner\models\search\PartnerVatSearch $modelSearch */
/** @var array $partners */
/** @var array $countries */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute('index'), 'method' => 'get']); ?>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <?= $form->field($modelSearch, 'partner')->select2List($partners, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <?= $form->field($modelSearch, 'country')->select2List($countries, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\modules\partner\models\PartnerVat $model */
/** @var array $partners */
/** @var array $countries */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'partner_id')->select2List($partners, ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'country_id')->select2List($countries, ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'value')->textInput(); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить НДС') : Yii::t('common', 'Сохранить НДС')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

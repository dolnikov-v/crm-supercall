<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\PartnerVat $model */
/** @var array $partners */
/** @var array $countries */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление НДС') : Yii::t('common', 'Редактирование НДС');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнер'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'НДС'), 'url' => Url::toRoute('/partner/vat/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'НДС'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'partners' => $partners,
        'countries' => $countries,
    ])
]) ?>

<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\modules\partner\models\PartnerProductVat;
use common\modules\partner\models\PartnerVat;
use common\widgets\base\ButtonLink;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $vatDataProvider */
/** @var \common\modules\partner\models\search\PartnerVatSearch $vatSearch */
/** @var \yii\data\ActiveDataProvider $productVatDataProvider */
/** @var \common\modules\partner\models\search\PartnerProductVatSearch $productVatSearch */
/** @var array $partners */
/** @var array $countries */
/** @var array $products */

$this->title = Yii::t('common', 'НДС');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнеры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_index-form', [
        'modelSearch' => $vatSearch,
        'partners' => $partners,
        'countries' => $countries
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Базовый НДС'),
    'actions' => DataProvider::renderSummary($vatDataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $vatDataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            'partner.name',
            'country.name',
            [
                'attribute' => 'value',
                'enableSorting' => false
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function (PartnerVat $model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.vat.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function (PartnerVat $model) {
                            return Url::toRoute(['delete', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.vat.edit');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('partner.vat.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить запись'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $vatDataProvider->getPagination()]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'НДС по товарам'),
    'actions' => DataProvider::renderSummary($productVatDataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $productVatDataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            'partnerProduct.partner.name',
            'country.name',
            'partnerProduct.product.name',
            [
                'attribute' => 'value',
                'enableSorting' => false
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function (PartnerProductVat $model) {
                            return Url::toRoute(['edit-product', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.vat.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function (PartnerProductVat $model) {
                            return Url::toRoute(['delete-product', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.vat.edit');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('partner.vat.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить запись'),
            'url' => Url::toRoute('edit-product'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $vatDataProvider->getPagination()]),
]) ?>
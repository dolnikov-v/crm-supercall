<?php

use common\modules\partner\assets\form\AdditionalPriceAsset;
use yii\bootstrap\Modal;
use common\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\widgets\base\Panel;

/** @var \common\modules\partner\models\PartnerForm $model */
/** @var array $partners */
/** @var array $countries */
/** @var array $types */
/** @var \common\modules\partner\models\PartnerFormAdditionPrice $additionalPrice */
?>

<?php AdditionalPriceAsset::register($this); ?>

<?php $form = ActiveForm::begin([
    'action' => false,
    'method' => 'post',
    'options' => [
        'id' => 'partner-form-additional-price'
    ],
]); ?>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($additionalPrice, 'addition_type')->select2List($additionalPrice->getMappingType(), [
            'prompt' => '-',
            'id' => 'addition_type'
        ]); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($additionalPrice, 'condition')->select2List($additionalPrice->getMappingCondition(), []); ?>
    </div>
    <!--Пока не используется-->
    <!--<div class="col-lg-2">
        <?= $form->field($additionalPrice, 'condition_text')->input('button', [
        'class' => 'btn btn-sm btn-primary',
        'id' => 'open_condition_modal',
        'value' => !isset($additionalPrice->id) ? Yii::t('common', 'Добавить') : Yii::t('common', 'Редактировать')
    ]); ?>
    </div>-->
    <!--Пока не используется-->
    <div class="col-lg-2">
        <?= $form->field($additionalPrice, 'quantity')->textInput(); ?>
        <?= Html::hiddenInput('form_id',$model->id);//$form->field($additionalPrice, 'form_id')->hiddenInput()->label(false); ?>
    </div>
</div>
<?= Html::button(yii::t('common', 'Сохранить'), ['class' => 'btn btn-success', 'id' => 'save_additional_price']);
//$form->submit(Yii::t('common', 'Сохранить'))  ?>
<?php ActiveForm::end(); ?>
<!--Пока не используется-->
<?php Modal::begin([
    'id' => 'condition_modal',
    'size' => Modal::SIZE_LARGE,
    'header' => yii::t('common', 'Условие дополнительной цены'),
    'options' => [
        'style' => 'z-index: 90000'
    ],
    'footer' => Html::button(yii::t('common', 'Сохранить'), [
            'class' => 'btn btn-default btn-success',
            'data-dismiss' => 'modal',
            'id' => 'condition_ok'
        ]) . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-danger close_btn', 'data-dismiss' => 'modal']),
]); ?>
<div class="additional_modal_content" data-type="default">
    <h4><?= Yii::t('common', 'Сначала выберите тип дополнительной цены') ?></h4>
</div>
<div class="additional_modal_content" data-type="product">
</div>
<div class="additional_modal_content" data-type="product_quantity">
</div>
<?php Modal::end(); ?>
<!--Пока не используется-->

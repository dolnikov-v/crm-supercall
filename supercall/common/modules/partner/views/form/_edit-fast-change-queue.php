<?php

use common\modules\partner\assets\form\FastChangeQueueAsset;
use common\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/** @var \common\modules\partner\models\PartnerForm $model */
/** @var \common\modules\partner\models\PartnerFormFastChangeQueue $fastChangeQueue */
?>

<?php FastChangeQueueAsset::register($this); ?>

<?php $form = ActiveForm::begin([
    'action' => false,
    'method' => 'post',
    'options' => [
        'id' => 'partner-form-fast-change-queue'
    ],
]); ?>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($fastChangeQueue, 'queue')->select2List(ArrayHelper::map($fastChangeQueue->getQueueList($model), 'id', 'name'), [
            'multiple' => true
        ]); ?>
    </div>
</div>
<?= Html::hiddenInput('form_id',$model->id);?>
<?= Html::button(yii::t('common', 'Сохранить'), ['class' => 'btn btn-success', 'id' => 'save_fast_change_queue']);
//$form->submit(Yii::t('common', 'Сохранить'))  ?>
<?php ActiveForm::end(); ?>

<?php
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use common\widgets\ActiveField;
use common\widgets\ActiveForm;
use common\widgets\base\Button;
use common\widgets\base\GlyphIcon;
use yii\helpers\Url;
use common\modules\partner\assets\form\ListAttributeAsset;

/** @var \common\modules\partner\models\PartnerFormAttribute $model */

ListAttributeAsset::register($this);
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['attribute', 'form_id' => $model->form->id, 'id' => $model->id, 'scope' => $model->scope])]); ?>
<div class="row">
    <div class="col-lg-12"><?= $form->field($model, 'name')->textInput(['disabled' => !$model->isNewRecord]); ?></div>
    <div class="col-lg-12"><?= $form->field($model, 'label')->textInput(); ?></div>
    <div class="col-lg-12"><?= $form->field($model, 'type')->dropDownList(PartnerFormAttribute::types()); ?></div>
</div>

<div class="list-values <?= $model->type == ActiveField::TYPE_LIST ? '' : 'hidden' ?>">

    <?php if ($model->type == ActiveField::TYPE_LIST): ?>

        <?php $values = json_decode($model->values, 1); ?>

        <?php foreach ($values as $value): ?>

            <div class="list-value row">
                <div class="col-lg-3"><?= $form->field($model, 'values[]')->textInput(['value' => $value])->label(false); ?></div>
                <div class="list-col-lg-3">
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_SUCCESS . ' add-value dropdown-toggle',
                            'icon' => GlyphIcon::PLUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>
                    </div>
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_DANGER . ' remove-value dropdown-toggle',
                            'icon' => GlyphIcon::MINUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>
                    </div>

                </div>
            </div>

        <?php endforeach; ?>

    <?php else: ?>

        <div class="list-value row">
            <div class="col-lg-3"><?= $form->field($model, 'values[]')->textInput()->label(false); ?></div>
            <div class="list-col-lg-3">
                <div class="btn-group">
                    <?= Button::widget([
                        'style' => Button::STYLE_SUCCESS . ' add-value dropdown-toggle',
                        'icon' => GlyphIcon::PLUS,
                        'attributes' => ['data-toggle' => 'dropdown']
                    ]) ?>
                </div>
                <div class="btn-group">
                    <?= Button::widget([
                        'style' => Button::STYLE_DANGER . ' remove-value dropdown-toggle',
                        'icon' => GlyphIcon::MINUS,
                        'attributes' => ['data-toggle' => 'dropdown']
                    ]) ?>
                </div>

            </div>
        </div>

    <?php endif; ?>
</div>


<div class="row">
    <!--<div class="col-lg-12"><?= $form->field($model, 'gmap_address_component')->textInput(); ?></div>-->
    <div class="col-lg-12"><?= $form->field($model, 'length')->textInput(); ?></div>
    <div class="col-lg-12"><?= $form->field($model, 'regex')->textInput(); ?></div>
    <div class="col-lg-12"><?= $form->field($model, 'required')->checkbox(['checked' => $model->required, 'value' => 1, 'unchecked_value' => 0, 'disabled' => in_array($model->name, PartnerForm::requiredAttributes())]); ?></div>
</div>
<?php if ($model->scope == PartnerForm::SCOPE_ADDRESS): ?>

    <?= $form->field($model, 'is_klader')->checkbox(['checked' => $model->is_klader, 'value' => 1, 'unchecked_value' => 0]); ?>
    <?= $form->field($model, 'is_disabled_user_data')->checkbox(['checked' => $model->is_disabled_user_data, 'value' => 1, 'unchecked_value' => 0, 'disabled' => 'true']); ?>
    <?= $form->field($model, 'is_coordinate')->checkbox(['checked' => $model->is_coordinate, 'value' => 1, 'unchecked_value' => 0]); ?>

<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить аттрибут') : Yii::t('common', 'Сохранить аттрибут')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute(['edit', 'id' => $model->form->id])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

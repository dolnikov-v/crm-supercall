<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\PartnerFormAttribute $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление аттрибута') : Yii::t('common', 'Редактирование аттрибута');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнер'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Формы'), 'url' => Url::toRoute('/partner/form/index')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $model->form->name), 'url' => Url::toRoute(['/partner/form/edit', 'id' => $model->form->id])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Аттрибут'),
    'content' => $this->render('_attribute-form', [
        'model' => $model,
    ])
]) ?>
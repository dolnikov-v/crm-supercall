<?php

use backend\widgets\TokenCKEditor;
use common\modules\order\components\notification\NoAnswerNotificationQueue;
use common\modules\partner\models\PartnerForm;
use common\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \common\modules\partner\models\PartnerForm $model */
/** @var array $partners */
/** @var array $countries */
/** @var array $types */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'partner_id')->select2List($partners, ['prompt' => '—']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'name')->textInput(); ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'country_id')->select2List($countries, ['prompt' => '—']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'type')->select2List($types, ['prompt' => '—']); ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'extra_price')->textInput(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'no_answer_notification_sender')->textInput() ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'min_delivery_days')->textInput(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'no_answer_notification_text')->widget(TokenCKEditor::className(), [
                'tokens' => NoAnswerNotificationQueue::tokensForNotificationText(),
                'clientOptions' => [
                    'height' => 150,
                    'toolbar' => [
                        [
                            'name' => 'clipboard',
                            'items' => ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
                        ]
                    ],
                    'enterMode' => 2,
                    'UseBROnCarriageReturn' => 1,
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'show_shipping')->checkbox([
                'checked' => $model->show_shipping,
                'value' => 1,
                'unchecked_value' => 0
            ]); ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'show_countries')->checkbox([
                'checked' => $model->show_countries,
                'value' => 1,
                'unchecked_value' => 0
            ]); ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'use_map')->checkbox([
                'checked' => $model->use_map,
                'value' => 1,
                'unchecked_value' => 0
            ]); ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'check_address')->checkbox([
                'checked' => $model->check_address,
                'value' => 1,
                'unchecked_value' => 0
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'allow_gifts')->checkbox([
                'checked' => $model->allow_gifts,
                'value' => 1,
                'unchecked_value' => 0
            ]); ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'mask_phones')->checkbox([
                'checked' => $model->mask_phones,
                'value' => 1,
                'unchecked_value' => 0
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'use_foreign_delivery_broker')->checkbox([
                'checked' => $model->use_foreign_delivery_broker,
                'value' => 1,
                'unchecked_value' => 0,
            ]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'use_messenger')->checkbox([
                'checked' => $model->use_messenger,
                'value' => true,
                'unchecked_value' => false
            ]); ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'disabled_buttons')->checkbox([
                'checked' => $model->disabled_buttons,
                'value' => 1,
                'unchecked_value' => 0,
            ]) ?>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'buttons')
                ->checkboxList(PartnerForm::getButtonsCollection(), ['value' => explode(',', $model->buttons)]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'virtual_buttons')
                ->checkboxList(PartnerForm::getVirtualButtonsCollection(), ['value' => explode(',', $model->virtual_buttons)]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить форму') : Yii::t('common', 'Сохранить форму')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php Modal::begin([
    'id' => 'promo-product-clear',
    'size' => Modal::SIZE_DEFAULT,
    'header' => yii::t('common', 'Предупреждение'),
    'options' => [
        'style' => 'z-index: 90000',
        'data' => [
            'country_id' => $model->country_id,
            'partner_id' => $model->partner_id
        ]
    ],
    'footer' => Html::button(yii::t('common', 'Продолжить'), [
        'class' => 'btn btn-default btn-success',
        'data-dismiss' => 'modal',
    ]),
]); ?>

    <div class="text-promo-change"><?= yii::t('common', 'При смене <span>reason</span> список промо товаров будет удалён автоматически'); ?></div>

<?php Modal::end(); ?>
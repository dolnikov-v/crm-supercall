<?php
use common\components\grid\ActionColumn;
use common\components\grid\GridView;
use common\modules\partner\models\PartnerFormAttribute;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;


/** @var \common\modules\partner\models\PartnerForm $model */
/** @var array $partners */

echo GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => array_values($model->addressAttrs)]),
    'rowOptions' => function ($attr) {
        return [
            'data-sortable-id' => $attr->id,
            'style' => $attr->is_coordinate === true ? 'background: #ECFAEC' : ''
        ];
    },
    'columns' => [
        [
            'class' => \beatep\sortable\grid\Column::className(),
        ],
        'name',
        'label',
        [
            'attribute' => 'type',
            'value' => function($attr){ return $attr->types()[$attr->type]; }
        ],
        [
            'attribute' => 'required',
            'format' => 'boolean'
        ],
        [
            'attribute' => 'is_klader',
            'format' => 'boolean'
        ],
        [
            'attribute' => 'is_disabled_user_data',
            'format' => 'boolean'
        ],
        [
            'attribute' => 'is_coordinate',
            'format' => 'boolean'
        ],
        'regex',
        [
            'class' => ActionColumn::className(),
            'items' => [
                [
                    'label' => Yii::t('common', 'Редактировать'),
                    'url' => function (PartnerFormAttribute $model) {
                        return Url::toRoute(['attribute', 'form_id' => $model->form->id, 'id' => $model->id]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('partner.form.edit');
                    }
                ],
                [
                    'label' => Yii::t('common', 'Удалить'),
                    'url' => function (PartnerFormAttribute $model) {
                        return Url::toRoute(['delete-attribute', 'id' => $model->id]);
                    },
                    'can' => function () {
                        return Yii::$app->user->can('partner.form.edit');
                    }
                ],
            ]
        ]
    ],
    'options' => [
        'data' => [
            'sortable-widget' => 1,
            'sortable-url' => \yii\helpers\Url::toRoute(['sorting']),
        ]
    ],
]);

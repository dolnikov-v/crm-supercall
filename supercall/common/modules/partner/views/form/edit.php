<?php

use common\modules\partner\models\PartnerForm;
use common\widgets\base\Button;
use common\widgets\base\ButtonLink;
use common\widgets\base\Panel;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\PartnerForm $model */
/** @var array $partners */
/** @var array $countries */
/** @var array $types */
/** @var \common\modules\partner\models\PartnerFormAdditionPrice $additionalPrice */
/** @var  \common\modules\partner\models\PromoProduct $promoProductModel */
/** @var array $partner_products */
/** @var \common\modules\partner\models\PartnerFormFastChangeQueue $fastChangeQueue */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление формы') : Yii::t('common', 'Редактирование формы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнер'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Формы'), 'url' => Url::toRoute('/partner/form/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

//оч. большая нагрузка на бд - читает из base_log - в планах перенос логов в отдельную таблицу
//if (!yii::$app->user->can('partner.form.history')) {
    $tabs = [
        [
            'label' => Yii::t('common', 'Редактирование формы'),
            'content' => $this->render('_tab-edit-form', [
                'model' => $model,
                'partners' => $partners,
                'countries' => $countries,
                'types' => $types,
                'fastChangeQueue' => $fastChangeQueue,
                'additionalPrice' => $additionalPrice,
                'promoProductModel' => $promoProductModel,
                'partner_products' => $partner_products
            ])
        ]
    ];
//}else{
//    $tabs = [
//        [
//            'label' => Yii::t('common', 'Редактирование формы'),
//            'content' => $this->render('_tab-edit-form', [
//                'model' => $model,
//                'partners' => $partners,
//                'countries' => $countries,
//                'types' => $types,
//                'fastChangeQueue' => $fastChangeQueue,
//                'additionalPrice' => $additionalPrice,
//                'promoProductModel' => $promoProductModel,
//                'partner_products' => $partner_products
//            ])
//        ],
//        [
//            'label' => Yii::t('common', 'История изменений формы'),
//            'content' => $this->render('_tab-history-form', [
//                'history' => $model->getHistory()
//            ])
//        ],
//    ];
//}

?>

<?= Tabs::widget([
    'encodeLabels' => false,
    'items' => $tabs
]);
?>
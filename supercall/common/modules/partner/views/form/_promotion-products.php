<?php

use common\modules\partner\models\PromoProduct;
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\partner\assets\form\PromoProductAsset;
use yii\helpers\Html;
use common\widgets\base\GlyphIcon;
use common\widgets\base\Button;

/** @var \common\modules\partner\models\PromoProduct $promoProductModel */
/** @var \common\modules\partner\models\PartnerForm $model */
/** @var array $partner_products */

PromoProductAsset::register($this);
?>


<?php if (isset($model->id)) {
    $form = ActiveForm::begin(['action' => '#', 'id' => 'PromoProductForm']);
    $promoProductModel = PromoProduct::find()->where(['form_id' => $model->id])->all(); ?>

    <div class="row promo-goods">
    <div class="row">
        <div class="col-lg-3"><?= Html::label(yii::t('common', 'Продукт')); ?></div>
        <div class="col-lg-3"><?= Html::label(yii::t('common', 'Цена')); ?></div>
        <div class="col-lg-2"><?= Html::label(yii::t('common', 'Действие')); ?></div>
    </div>

    <?php if ($promoProductModel) {
        foreach ($promoProductModel as $promoProduct) { ?>
            <div class="row promo-good">
                <div class="col-lg-3">
                    <?= $form->field($promoProduct, 'product_id')->select2List($partner_products, [
                        'prompt' => '-',
                        'name' => 'product[]'
                    ])->label(false); ?>
                </div>

                <div class="col-lg-3">
                    <?= $form->field($promoProduct, 'price')->textInput([
                        'value' => isset($promoProduct->price) ? $promoProduct->price : 0,
                        'name' => 'price[]'
                    ])->label(false); ?>
                </div>

                <div class="col-lg-3">
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_SUCCESS . ' add-promo-product',
                            'icon' => GlyphIcon::PLUS,
                            'attributes' => []
                        ]) ?>
                    </div>
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_DANGER . ' remove-promo-product',
                            'icon' => GlyphIcon::MINUS,
                            'attributes' => []
                        ]) ?>
                    </div>
                </div>
            </div>
        <?php }
    } else {
        $promoProductModel = new PromoProduct(); ?>
        <div class="row promo-good">
                <div class="col-lg-3">
                    <?= $form->field($promoProductModel, 'product_id')->select2List($partner_products, [
                        'prompt' => '-',
                        'name' => 'product[]'
                    ])->label(false); ?>
                </div>

                <div class="col-lg-3">
                    <?= $form->field($promoProductModel, 'price')->textInput([
                        'value' => isset($promoProductModel->price) ? $promoProductModel->price : 0,
                        'name' => 'price[]'
                    ])->label(false); ?>
                </div>

                <div class="col-lg-3">
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_SUCCESS . ' add-promo-product',
                            'icon' => GlyphIcon::PLUS,
                            'attributes' => []
                        ]) ?>
                    </div>
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_DANGER . ' remove-promo-product',
                            'icon' => GlyphIcon::MINUS,
                            'attributes' => []
                        ]) ?>
                    </div>
                </div>
            </div>
    <?php }
    echo HTML::hiddenInput('form_id', $model->id);
} ?>
    </div>
<?php ActiveForm::end(); ?>
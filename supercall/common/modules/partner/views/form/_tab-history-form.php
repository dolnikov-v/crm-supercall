<?php
use common\modules\partner\widgets\logAdditionalPriceForm;
use common\modules\partner\widgets\logBaseForm;
use common\modules\partner\widgets\logPromoProductsForm;
use common\widgets\base\Panel;
use yii\bootstrap\Tabs;
use yii\helpers\Url;

/** @var \common\modules\partner\models\PartnerForm $model */
/** @var array $history */

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список изменений'),
    'content' => Tabs::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => Yii::t('common', 'Основные атрибуты'),
                'content' => logBaseForm::widget(['query' => $history['base']])
            ],
            [
                'label' => Yii::t('common', 'Дополнительная цена'),
                'content' => logAdditionalPriceForm::widget(['query' => $history['additionalPrice']])
            ],
            [
                'label' => Yii::t('common', 'Промо товары'),
                'content' => logPromoProductsForm::widget(['query' => $history['promoProducts']])
            ],
        ]
    ])
]) ?>

<?php
/** @var PartnerForm $model */

use common\modules\partner\assets\AdditionalParamsAsset;
use common\modules\partner\models\PartnerForm;
use common\widgets\base\Button;
use common\widgets\base\GlyphIcon;
use yii\helpers\Html;

AdditionalParamsAsset::register($this);
?>

<div class="additional-params">
    <div class="alert alert-info">
        <?= yii::t('common', 'Если значение берётся из DOM документа, то необходимо указать его DOM selector в формате dom:#order_id или dom:[name*=order_id]'); ?>
         <br/>
        <?= yii::t('common', '<b>Внимание! </b>Максимальная длина (вместе с номером телефона) строки json не должна превышать 255 символов (240 без номера телефона)'); ?>
    </div>

    <?= Html::input('hidden', 'form_id', $model->id, [
        'class' => 'form-control'
    ]) ?>

    <br/>

    <div class="row">
        <div class="col-lg-3"><?= Html::label(yii::t('common', 'Ключ')); ?></div>
        <div class="col-lg-3"><?= Html::label(yii::t('common', 'Значение')); ?></div>
        <div class="col-lg-2"><?= Html::label(yii::t('common', 'Макс. длина (ориентировочно)')); ?></div>
    </div>

    <?php if(!empty($model->use_additional_params)) :?>

    <?php foreach ((array)$model->use_additional_params as $key=> $param):?>

    <div class="row additional-param">
        <div class="col-lg-3">
            <div class="form-group">

                <?= Html::input('text', 'key', $key, [
                    'class' => 'form-control',
                    'placeholder' => 'oid'
                ]) ?>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">

                <?= Html::input('text', 'param', $param, [
                    'class' => 'form-control',
                    'placeholder' => '[name*=order_id]'
                ]) ?>
            </div>
        </div>
        <div class="col-lg-1">
            <div class="form-group">

                <?= Html::input('text', 'length', PartnerForm::DEFAULT_LENGTH_ADDITIONAL_PARAM, [
                    'class' => 'form-control',
                    'placeholder' => 7
                ]) ?>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="btn-group">
                <?= Button::widget([
                    'style' => Button::STYLE_SUCCESS . ' add-param',
                    'icon' => GlyphIcon::PLUS,
                    'attributes' => []
                ]) ?>
            </div>
            <div class="btn-group">
                <?= Button::widget([
                    'style' => Button::STYLE_DANGER . ' remove-param',
                    'icon' => GlyphIcon::MINUS,
                    'attributes' => []
                ]) ?>
            </div>
        </div>
    </div>

    <?php endforeach; ;?>

    <?php else: ?>

        <div class="row additional-param">
            <div class="col-lg-3">
                <div class="form-group">

                    <?= Html::input('text', 'key', '', [
                        'class' => 'form-control',
                        'placeholder' => 'oid'
                    ]) ?>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">

                    <?= Html::input('text', 'param', '', [
                        'class' => 'form-control',
                        'placeholder' => '[name*=order_id]'
                    ]) ?>
                </div>
            </div>
            <div class="col-lg-1">
                <div class="form-group">

                    <?= Html::input('text', 'length', '', [
                        'class' => 'form-control',
                        'placeholder' => 7
                    ]) ?>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="btn-group">
                    <?= Button::widget([
                        'style' => Button::STYLE_SUCCESS . ' add-param',
                        'icon' => GlyphIcon::PLUS,
                        'attributes' => []
                    ]) ?>
                </div>
                <div class="btn-group">
                    <?= Button::widget([
                        'style' => Button::STYLE_DANGER . ' remove-param',
                        'icon' => GlyphIcon::MINUS,
                        'attributes' => []
                    ]) ?>
                </div>
            </div>
        </div>

    <?php endif;?>

</div>
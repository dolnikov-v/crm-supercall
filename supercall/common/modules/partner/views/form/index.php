<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\widgets\ModalCopyForm;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \common\modules\partner\models\search\PartnerFormSearch $modelSearch */
/** @var \common\modules\partner\models\PartnerFormCopy $modelCopy */
/** @var array $partners */
/** @var array $order_types */

$this->title = Yii::t('common', 'Формы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнеры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'partners' => $partners,
        'countries' => $countries
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с формами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function (PartnerForm $model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            'partner.name',
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            'country.name',
            [
                'attribute' => 'type',
                'content' => function ($model) use ($order_types) {
                    return $order_types[$model->type];
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'min_delivery_days_name_in_grid_table',
                'content' => function($model) {
                    return $model->min_delivery_days;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'show_shipping',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->show_shipping ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->show_shipping ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            /*[
                'attribute' => 'show_countries',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->show_countries ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->show_countries ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],*/
            [
                'attribute' => 'use_map',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->use_map ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->use_map ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'check_address',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->check_address ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->check_address ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            /*[
                'attribute' => 'show_countries',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->show_countries ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->show_countries ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],*/
            [
                'attribute' => 'allow_gifts',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->allow_gifts ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->allow_gifts ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'mask_phones',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->mask_phones ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->mask_phones ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'use_messenger',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->use_messenger ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->use_messenger ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'use_foreign_delivery_broker',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->use_foreign_delivery_broker ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->use_foreign_delivery_broker ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'disabled_buttons',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerForm $model) {
                    return Label::widget([
                        'label' => $model->disabled_buttons ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->disabled_buttons ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function (PartnerForm $model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.form.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function (PartnerForm $model) {
                            return Url::toRoute(['/partner/form/activate', 'id' => $model->id]);
                        },
                        'can' => function (PartnerForm $model) {
                            return Yii::$app->user->can('partner.form.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function (PartnerForm $model) {
                            return Url::toRoute(['/partner/form/deactivate', 'id' => $model->id]);
                        },
                        'can' => function (PartnerForm $model) {
                            return Yii::$app->user->can('partner.form.deactivate') && $model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function (PartnerForm $model) {
                            return Url::toRoute(['/partner/form/delete', 'id' => $model->id]);
                        },
                        'can' => function (PartnerForm $model) {
                            return Yii::$app->user->can('partner.form.delete') && $model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Копировать'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'copy-form-link',
                        'attributes' => function ($data) {
                            return [
                                'data-id' => $data->id,
                            ];
                        },
                        'can' => function (PartnerForm $model) {
                            return Yii::$app->user->can('partner.form.edit') && $model->active;
                        }
                    ]
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('partner.form.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить форму'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalCopyForm::widget([
    'model' => $modelCopy,
]) ?>
<?php

use common\modules\partner\models\PartnerForm;
use common\widgets\base\Button;
use common\widgets\base\ButtonLink;
use common\widgets\base\Panel;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\PartnerForm $model */
/** @var array $partners */
/** @var array $countries */
/** @var array $types */
/** @var \common\modules\partner\models\PartnerFormAdditionPrice $additionalPrice */
/** @var  \common\modules\partner\models\PromoProduct $promoProductModel */
/** @var array $partner_products */
/** @var \common\modules\partner\models\PartnerFormFastChangeQueue $fastChangeQueue */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Форма'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'partners' => $partners,
        'countries' => $countries,
        'types' => $types
    ])
]) ?>

<?php
if (!$model->isNewRecord) {
    echo Panel::widget([
        'title' => Yii::t('common', 'Быстрая смена очереди'),
        'content' => $this->render('_edit-fast-change-queue', [
            'model' => $model,
            'fastChangeQueue' => $fastChangeQueue,
        ])
    ]);
} ?>

<?php
if (!$model->isNewRecord) {
    echo Panel::widget([
        'title' => Yii::t('common', 'Дополнительная цена'),
        'content' => $this->render('_edit-additional-price', [
            'model' => $model,
            'partners' => $partners,
            'countries' => $countries,
            'types' => $types,
            'additionalPrice' => $additionalPrice
        ])
    ]);
} ?>

<?php if (!$model->isNewRecord) {
    echo Panel::widget([
        'title' => Yii::t('common', 'Акционные товары'),
        'alertStyle' => Panel::ALERT_INFO,
        'alert' => yii::t('common', 'Список товаров зависит от партнёра и страны, указанного в форме. При смене партнёра или страны формы - список акционных товаров будет сброшен'),
        'content' => $this->render('_promotion-products', [
            'model' => $model,
            'promoProductModel' => $promoProductModel,
            'partner_products' => ArrayHelper::map($partner_products, 'product.id', 'product.name')
        ]),
        'footer' => Button::widget([
            'id' => 'promo-product-save',
            'label' => Yii::t('common', 'Сохранить список акционных товаров'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ])
    ]);

    echo Panel::widget([
        'title' => Yii::t('common', 'Атрибуты (основные)'),
        'content' => $this->render('_edit-attrs-general', [
            'model' => $model
        ]),
        'footer' => (Yii::$app->user->can('partner.form.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить аттрибут'),
            'url' => Url::toRoute(['attribute', 'form_id' => $model->id, 'scope' => PartnerForm::SCOPE_GENERAL]),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '')
    ]);

    echo Panel::widget([
        'title' => Yii::t('common', 'Атрибуты (адрес)'),
        'content' => $this->render('_edit-attrs-address', [
            'model' => $model
        ]),
        'footer' => (Yii::$app->user->can('partner.form.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить аттрибут'),
            'url' => Url::toRoute(['attribute', 'form_id' => $model->id, 'scope' => PartnerForm::SCOPE_ADDRESS]),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '')
    ]);
} ?>

<?php if (yii::$app->user->isSuperadmin): ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Параметры, передаваемые с номером телефона'),
        'content' => $this->render('_additional-params', [
            'model' => $model
        ]),
        'footer' => ButtonLink::widget([
                'id' => 'save-additional-params',
                'label' => Yii::t('common', 'Сохранить'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ]) .
            ButtonLink::widget([
                'id' => 'clear-additional-params',
                'label' => Yii::t('common', 'Очистить'),
                'style' => ButtonLink::STYLE_DEFAULT,
                'size' => ButtonLink::SIZE_SMALL,
            ])
    ]); ?>

<?php endif; ?>

<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\PartnerShipping $model */
/** @var array $partners */
/** @var array $shippings */
/** @var array $countries */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'partner_id')->select2List($partners, ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'partner_shipping_id')->textInput(); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'shipping_id')->select2List($shippings, ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'country_id')->select2List($countries, ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'price')->textInput(); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'dynamic_price')->checkbox(['checked' => $model->dynamic_price, 'value' => 1, 'unchecked_value' => 0]); ?>
        <?php $this->registerJs('
            $("#partnershipping-dynamic_price")
            .on("change", function(){ $("#partnershipping-price").get(0).disabled = this.checked; })
            .trigger("change");
        '); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить курьерскую службу') : Yii::t('common', 'Сохранить курьерскую службу')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

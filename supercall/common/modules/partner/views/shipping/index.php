<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\modules\partner\models\PartnerShipping;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\modules\partner\assets\PartnerShippingAsset;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \common\modules\partner\models\search\PartnerShippingSearch $modelSearch */
/** @var array $partners */
/** @var array $shippings */
/** @var array $countries */

PartnerShippingAsset::register($this);

$this->title = Yii::t('common', 'Курьерские службы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнеры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'partners' => $partners,
        'shippings' => $shippings,
        'countries' => $countries
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с курьерскими службами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function (PartnerShipping $model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            'partner.name',
            [
                'attribute' => 'partner_shipping_id',
                'enableSorting' => false,
            ],
            'shipping.name',
            'country.name',
            [
                'attribute' => 'price',
                'enableSorting' => false
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerShipping $model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'is_default',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function (PartnerShipping $model) {
                    return Label::widget([
                        'label' => $model->is_default ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->is_default ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function (PartnerShipping $model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.shipping.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function (PartnerShipping $model) {
                            return Url::toRoute(['/partner/shipping/activate', 'id' => $model->id]);
                        },
                        'can' => function (PartnerShipping $model) {
                            return Yii::$app->user->can('partner.shipping.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function (PartnerShipping $model) {
                            return Url::toRoute(['/partner/shipping/deactivate', 'id' => $model->id]);
                        },
                        'can' => function (PartnerShipping $model) {
                            return Yii::$app->user->can('partner.shipping.deactivate') && $model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'По умолчанию'),
                        'url' => function (PartnerShipping $model) {
                            return Url::toRoute(['/partner/shipping/is-default', 'id' => $model->id]);
                        },
                        'can' => function (PartnerShipping $model) {
                            return Yii::$app->user->can('partner.shipping.is-default') && $model->active && !$model->is_default;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Отключить "По умолчанию"'),
                        'url' => function (PartnerShipping $model) {
                            return Url::toRoute(['/partner/shipping/is-not-default', 'id' => $model->id]);
                        },
                        'can' => function (PartnerShipping $model) {
                            return Yii::$app->user->can('partner.shipping.is-default') && $model->active && $model->is_default;
                        }
                    ]
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('partner.shipping.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить курьерскую службу'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        (Yii::$app->user->can('partner.shipping.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Получить данные у партнёров'),
            'url' => '#',
            'id' => 'get_partner_shipping',
            'style' => ButtonLink::STYLE_INFO,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

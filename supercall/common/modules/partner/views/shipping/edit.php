<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\PartnerShipping $model */
/** @var array $partners */
/** @var array $shippings */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление курьерской службы') : Yii::t('common', 'Редактирование курьерской службы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнер'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Курьерские службы'), 'url' => Url::toRoute('/partner/shipping/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Курьерская служба'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'partners' => $partners,
        'shippings' => $shippings,
        'countries' => $countries
    ])
]) ?>

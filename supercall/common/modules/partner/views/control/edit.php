<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\Partner $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление партнера') : Yii::t('common', 'Редактирование партнера');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнеры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/partner/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Партнер'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]) ?>

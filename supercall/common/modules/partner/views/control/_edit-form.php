<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\modules\partner\models\Partner $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'api_key')->textInputGenerate([
            'length' => 40,
            'chars' => 'abcdef1234567890'
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'token')->textInput(['readonly' => true]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'url_product')->textInput() ?>
    </div>
    <div class="col-lg-8">
        <?= $form->field($model, 'url_queue')->textInput() ?>
    </div>

</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'url_foreign_delivery_broker')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'buyout_url')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'header_auth')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить партнера') : Yii::t('common', 'Сохранить партнера')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

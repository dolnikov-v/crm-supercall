<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\modules\partner\models\search\PartnerProductSearch;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список партнеров');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнеры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с партнерами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'api_key',
                'content' => function ($model) {
                    return $model->api_key ? $model->api_key : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'token',
                'content' => function ($model) {
                    return $model->token ? $model->token : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'url_queue',
                'content' => function ($model) {
                    return $model->url_queue;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            ['attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.control.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/partner/control/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('partner.control.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/partner/control/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('partner.control.deactivate') && $model->active;
                        }
                    ],
                    [
                        'can' => function ($model) {
                            return Yii::$app->user->can('partner.control.edit')
                            || (Yii::$app->user->can('partner.control.activate') && !$model->active)
                            || (Yii::$app->user->can('partner.control.deactivate') && $model->active);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Товары'),
                        'url' => function ($model) {
                            return Url::toRoute(['/partner/product/index',
                                PartnerProductSearch::shortClassName() => [
                                    'partner' => $model->id
                                ],
                            ]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('partner.product.index');
                        }
                    ]
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('partner.control.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить партнера'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

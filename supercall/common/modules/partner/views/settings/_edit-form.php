<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\modules\partner\models\PartnerSettings $model */
/** @var array $partners */
/** @var array $countries */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'partner_id')->select2List($partners, ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'country_id')->select2List($countries, ['prompt' => '—']); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'daily_from')->select2List($dailyHours, ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'daily_till')->select2List($dailyHours, ['prompt' => '—']); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'type_id')->select2List($types, ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'buffer')->textInput(); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <br/>
        <?= $form->field($model, 'genesys_sync_on_save')->checkbox(['checked' => $model->genesys_sync_on_save, 'value' => 1, 'unchecked_value' => 0]); ?>
        <br/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить') : Yii::t('common', 'Сохранить')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

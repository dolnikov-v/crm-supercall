<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\modules\partner\models\PartnerSettings;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \common\modules\partner\models\search\PartnerSettingsSearch $search */
/** @var array $partners */
/** @var array $countries */

$this->title = Yii::t('common', 'Настройки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнеры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title'   => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_index-form', [
        'modelSearch' => $search,
        'partners'    => $partners,
        'countries'   => $countries,
        'queues'      => $queues,
        'types'       => $types,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Настройки партнеров'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            'partner.name',
            'country.name',
            [
                'attribute' => 'type_id',
                'content' => function ($model) {
                    if ($model->type_id !== null)
                        return $model->type->name;
                },
                'enableSorting' => true,
            ],
            [
                'attribute' => 'genesys_sync_on_save',
                'content' => function ($model) {
                    if ($model->genesys_sync_on_save)
                        return Label::widget([
                            'label' => 'Да',
                            'style' => Label::STYLE_SUCCESS,
                        ]);
                },
                'enableSorting' => false
            ],
            'buffer',
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function (PartnerSettings $model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.settings.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function (PartnerSettings $model) {
                            return Url::toRoute(['delete', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.settings.edit');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('partner.settings.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить запись'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
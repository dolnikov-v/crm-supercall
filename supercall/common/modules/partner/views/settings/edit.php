<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\PartnerSettings $model */
/** @var array $partners */
/** @var array $countries */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление настроек') : Yii::t('common', 'Редактирование настроек');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнер'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'НАстройки'), 'url' => Url::toRoute('/partner/settings/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Настройки'),
    'content' => $this->render('_edit-form', [
        'model'     => $model,
        'partners'  => $partners,
        'countries' => $countries,
        'queues'    => $queues,
        'types'     => $types,
        'dailyHours'=> $dailyHours,
    ])
]) ?>

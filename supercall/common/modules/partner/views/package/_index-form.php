<?php
use common\helpers\Collection;
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\modules\partner\models\search\PackageSearch $modelSearch */
/** @var array $partners */
/** @var array $countries */
/** @var array $products */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute('index'), 'method' => 'get']); ?>
<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($modelSearch, 'partner_id')->select2List($partners, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($modelSearch, 'country_id')->select2List($countries, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($modelSearch, 'product_id')->select2List($products, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <?= $form->field($modelSearch, 'active')->select2List(Collection::getActiveStatuses(), [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

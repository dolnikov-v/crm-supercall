<?php

use common\modules\partner\assets\package\PackageGiftAsset;
use common\modules\partner\models\PackageGift;
use common\widgets\ActiveForm;
use common\widgets\base\GlyphIcon;
use yii\helpers\Url;
use common\modules\partner\models\Package;
use common\widgets\base\Button;
use common\modules\partner\assets\package\PackageAsset;

/** @var \common\modules\partner\models\Package $model */
/** @var PackageGift $modelGift */
/** @var array $partners */
/** @var array $products */
/** @var array $countries */

PackageAsset::register($this);
PackageGiftAsset::register($this);

?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="main_data">
    <div class="row">
        <div class="col-lg-3"><?= $form->field($model, 'prefix')->textInput(); ?></div>
        <div class="col-lg-9"><?= $form->field($model, 'name')->textInput(['readonly' => true]); ?></div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'partner_id')->select2List($partners, ['prompt' => '—']); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'country_id')->select2List($countries, ['prompt' => '—']); ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'product_id')->select2List($products, ['prompt' => '—']); ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'paid_amount')->textInput(); ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'product_price')->textInput(); ?>
        </div>
    </div>

</div>

<fieldset class="gift_place">
    <legend><h3><?= yii::t('common', 'Подарки'); ?></h3></legend>
    <div class="load hidden"><i class="fa fa-spinner fa-spin" style="font-size:50px"></i></div>
    <div class="alert alert-warning empty-partner-product hidden" role="alert">
        <?=yii::t('common', 'Товары партнёра в данной стране не найдены');?>
    </div>
    <div class="package-gifts">

        <?php if ($model->packageGifts): ?>

            <?php foreach ($model->packageGifts as $packageGift): ?>

                <div class="row package-gift">
                    <div class="col-lg-3"><?= $form->field($packageGift, 'product_id')->select2List($products, [
                            'prompt' => '—',
                            'name' => 'PackageGift[product_id][]'
                        ])->label(false);
                        ?></div>
                    <div class="col-lg-3"><?= $form->field($packageGift, 'free_amount')->textInput([
                            'type' => 'number',
                            'name' => 'PackageGift[free_amount][]',
                            'value' => $packageGift->free_amount
                        ])->label(false);
                        ?></div>
                    <div class="col-lg-3 padding-30">
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_SUCCESS . ' add-package-gift dropdown-toggle',
                                'icon' => GlyphIcon::PLUS,
                                'attributes' => ['data-toggle' => 'dropdown']
                            ]) ?>
                        </div>
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_DANGER . ' remove-package-gift dropdown-toggle',
                                'icon' => GlyphIcon::MINUS,
                                'attributes' => ['data-toggle' => 'dropdown']
                            ]) ?>
                        </div>

                    </div>
                </div>

            <?php endforeach; ?>

        <?php else: ?>

            <div class="row package-gift">
                <div class="col-lg-3"><?= $form->field($modelGift, 'product_id')->select2List($products, [
                        'prompt' => '—',
                        'name' => 'PackageGift[product_id][]'
                    ])->label(false);
                    ?></div>
                <div class="col-lg-3"><?= $form->field($modelGift, 'free_amount')->textInput([
                        'value' => 1,
                        'type' => 'number',
                        'name' => 'PackageGift[free_amount][]'
                    ])->label(false);
                    ?></div>
                <div class="col-lg-3 padding-30">
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_SUCCESS . ' add-package-gift dropdown-toggle',
                            'icon' => GlyphIcon::PLUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>
                    </div>
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_DANGER . ' remove-package-gift dropdown-toggle',
                            'icon' => GlyphIcon::MINUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>
                    </div>

                </div>
            </div>

        <?php endif; ?>

    </div>
</fieldset>


<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить акцию') : Yii::t('common', 'Сохранить акцию')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\ArrayHelper;
use common\modules\partner\models\Partner;
use common\models\Country;
use common\models\Product;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var  \common\modules\partner\models\search\PackageSearch $modelSearch */
/** @var \common\modules\partner\models\Package $model */
/** @var [] $partners */
/** @var [] $countries */
/** @var [] $products */

$this->title = Yii::t('common', 'Акции');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнеры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'partners' => $partners,
        'countries' => $countries,
        'products' => $products,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с акциями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-left'],
                'content' => function ($model) {
                    return $model->prefix . ' ' . $model->name;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'partner_id',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $partner = Partner::findOne(['id' => $model->partner_id]);
                    return $partner['name'];
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country_id',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $country = Country::findOne(['id' => $model->country_id]);
                    return $country['name'];
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'product_id',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    $product = Product::findOne(['id' => $model->product_id]);
                    return $product['name'];
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('partner.package.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/partner/package/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('partner.package.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/partner/package/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('partner.package.deactivate') && $model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function ($model) {
                            return Url::toRoute(['/partner/package/delete', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('partner.package.delete') && $model->active;
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('partner.package.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить акцию'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>


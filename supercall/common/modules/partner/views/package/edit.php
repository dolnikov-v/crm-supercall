<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\Package $model */
/** @var \common\modules\partner\models\PackageGift $modelGift */
/** @var array $partners */
/** @var array $products */
/** @var array $countries */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление акции') : Yii::t('common', 'Редактирование акции');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Партнеры'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Акции'), 'url' => Url::toRoute('/partner/package/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Акция'),
    'alert' => "<b>" . yii::t('common', 'Внимание') . ":</b> " . yii::t('common', 'Если стоимость товара за единицу не указана - стоимость товара берётся из заказа') . "<br />" .
        yii::t('common', 'Наименование акции составляется автоматически и включает в себя перфикс + продукт + продукты по акции'),
    'alertStyle' => Panel::ALERT_INFO,
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'modelGift' => $modelGift,
        'partners' => $partners,
        'products' => $products,
        'countries' => $countries,
    ])
]) ?>

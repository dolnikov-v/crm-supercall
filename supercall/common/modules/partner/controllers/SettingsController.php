<?php
namespace common\modules\partner\controllers;

use backend\modules\queue\models\Queue;
use common\components\web\Controller;
use common\models\Country;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerSettings;
use common\modules\partner\models\search\PartnerSettingsSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class SettingsController
 * @package common\modules\partner\controllers
 */
class SettingsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $search = new PartnerSettingsSearch();
        $filterSessId = $this->route;
        $query = null;
        $this->getFilterBySession($filterSessId, $query);

        return $this->render('index', [
            'search'       => $search,
            'contentVisible' => Yii::$app->session->get($filterSessId.'.isVisible', true),
            'dataProvider' => $search->search($query),
            'partners'     => Partner::find()->active()->collection(),
            'countries'    => Country::find()->collection('common'),
            'queues'       => Queue::find()->active()->collection(),
            'types'        => OrderType::find()->collection(),
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new PartnerSettings();
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Настройки успешно добавлены.') : Yii::t('common', 'Настройки успешно отредактированы.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }
        
        return $this->render('edit', [
            'model'     => $model,
            'partners'  => Partner::find()->active()->collection(),
            'countries' => Country::find()->active()->collection('common'),
            'queues'    => Queue::find()->active()->collection(),
            'types'     => OrderType::find()->collection(),
            'dailyHours'=> PartnerSettings::dailyHours(),
        ]);
    }

    /**
     * @param integer $id
     * @return PartnerSettings
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = PartnerSettings::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Настройки не найдены.'));
        }

        return $model;
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Настройки успешно удалены.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }
}

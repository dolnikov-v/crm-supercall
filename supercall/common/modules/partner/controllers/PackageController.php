<?php

namespace common\modules\partner\controllers;

use common\components\web\Controller;
use common\modules\partner\models\Package;
use common\modules\partner\models\PackageGift;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerProduct;
use common\modules\partner\models\search\PackageSearch;
use common\modules\partner\models\Partner;
use common\modules\order\models\OrderProduct;
use common\models\Country;
use common\models\Product;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;
use common\modules\order\models\Order;
use yii;

/**
 * Class PackageController
 * @package common\modules\partner\controllers
 */
class PackageController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new PackageSearch();
        $param = yii::$app->request->queryParams;

        $dataProvider = $modelSearch->search($param);

        $products = ArrayHelper::map(Product::findAll(['active' => true]), 'id', 'name');

        foreach ($products as $key => $item) {
            $products[$key] = Yii::t('common', $item);
        }

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'partners' => Partner::find()->active()->collection(),
            'countries' => Country::find()->active()->collection('common'),
            'products' => $products,
        ]);
    }

    /**
     * @return string
     */
    public function actionEdit($id = null)
    {
        $error = false;
        $transaction = yii::$app->db->beginTransaction();

        if ($id) {
            $model = $this->getModel($id);
            PackageGift::deleteAll(['package_id' => $id]);
        } else {
            $model = new Package();
        }

        if (yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {

                $post = yii::$app->request->post('PackageGift');
                $products = $post['product_id'];
                $free_amounts = $post['free_amount'];

                if (empty($products)) {
                    yii::$app->notifier->addNotifier(yii::t('common', 'Не указаны товары по акции'));
                    Yii::$app->response->redirect(Url::to(['/partner/package/edit/', 'id' => $this->id]));
                    Yii::$app->end();
                }

                foreach ($products as $num => $product) {
                    if (!empty($product && $free_amounts[$num] > 0)) {
                        $data = [
                            'PackageGift' => [
                                'package_id' => $model->id,
                                'product_id' => $product,
                                'free_amount' => $free_amounts[$num],
                                'created_at' => time()
                            ]
                        ];

                        $packageGift = new PackageGift();
                        $packageGift->load($data);

                        if (!$packageGift->save()) {
                            yii::$app->notifier->addNotifier(json_encode($packageGift->getErrors(), JSON_UNESCAPED_UNICODE));
                            $transaction->rollBack();

                            $error = true;

                            Yii::$app->response->redirect(Url::to(['/partner/package/edit/', 'id' => $model->id]));
                            Yii::$app->end();
                        }
                    }
                }

                if (!$error) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }


                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Акция успешно добавлена.') : Yii::t('common', 'Акция успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $products = $model->isNewRecord ? [] : ArrayHelper::map(PartnerProduct::findAll([
            'country_id' => $model->country_id,
            'partner_id' => $model->partner_id
        ]), 'product.id', 'product.name');

        return $this->render('edit', [
            'model' => $model,
            'modelGift' => new PackageGift(),
            'partners' => Partner::find()->active()->collection(),
            'countries' => Country::find()->active()->collection('common'),
            'products' => $products,
        ]);
    }

    /**
     * @param $id
     * @return yii\web\Response
     */
    public function actionDelete($id)
    {
        $orders = $this->getOrderProductsFromPackage($id);

        if (count($orders) == 0) {
            $model = Package::findOne(['id' => $id]);

            if ($model->delete()) {
                Yii::$app->notifier->addNotifier(Yii::t('common', 'Акция успешно удалена.'), 'success');
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Невозможно удалить акцию, т.к. она уже используется в заказах'));
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $package_id
     * @return OrderProduct[]
     */
    public function getOrderProductsFromPackage($package_id)
    {
        return OrderProduct::find()
            ->where(['package_id' => $package_id])
            ->all();
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = Package::ACTIVE;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Укция успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = Package::NOT_ACTIVE;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Акция успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return array|null|yii\db\ActiveRecord
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Package::find()->joinWith('packageGifts')->where([Package::tableName() . '.id' => $id])->one();

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Акция не найдена.'));
        }

        return $model;
    }

    /**
     * @return string
     */
    public function actionGetPartnerCountries()
    {
        $partner_id = yii::$app->request->post('partner_id');

        if ($partner_id == '') {
            $listCountries = [];
        } else {
            $countries = PartnerForm::getPartnerCountries($partner_id);
            $listCountries = ArrayHelper::map($countries, 'country_id', 'country.name');
        }

        return json_encode($listCountries, JSON_UNESCAPED_UNICODE);

    }

    /**
     * @return string
     */
    public function actionGetPartnerProducts()
    {
        $partner_id = yii::$app->request->post('partner_id');
        $country_id = yii::$app->request->post('country_id');

        if ($country_id == '' || $partner_id == '') {
            $listProducts = [];
        } else {
            $products = PartnerProduct::getPartnerProducts($partner_id, $country_id);
            $listProducts = ArrayHelper::map($products, 'product_id', 'product.name');
        }

        return json_encode($listProducts, JSON_UNESCAPED_UNICODE);
    }
}
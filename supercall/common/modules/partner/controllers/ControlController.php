<?php
namespace common\modules\partner\controllers;

use common\components\web\Controller;
use common\modules\partner\models\Partner;
use common\modules\partner\models\search\PartnerSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class ControlController
 * @package common\modules\partner\controllers
 */
class ControlController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new PartnerSearch();

        $param = Yii::$app->request->queryParams;

        $dataProvider = $modelSearch->search($param);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Partner();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Партнер успешно добавлен.') : Yii::t('common', 'Партнер успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'parents' => Partner::find()->collection(),
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Партнер успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Партнер успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Partner
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Partner::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Партнер не найден.'));
        }

        return $model;
    }
}

<?php
namespace common\modules\partner\controllers;

use common\components\web\Controller;
use common\models\Country;
use common\models\Product;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerProduct;
use common\modules\partner\models\PartnerProductVat;
use common\modules\partner\models\PartnerVat;
use common\modules\partner\models\search\PartnerProductVatSearch;
use common\modules\partner\models\search\PartnerVatSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class VatController
 * @package common\modules\partner\controllers
 */
class VatController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);
        
        $vatSearch = new PartnerVatSearch();
        $productVatSearch = new PartnerProductVatSearch();

        return $this->render('index', [
            'vatSearch' => $vatSearch,
            'vatDataProvider' => $vatSearch->search($query),
            'contentVisible' => Yii::$app->session->get($filterSessId.'.isVisible', true),
            'productVatSearch' => $productVatSearch,
            'productVatDataProvider' => $productVatSearch->search($query),
            'partners' => Partner::find()->active()->collection(),
            'countries' => Country::find()->collection('common'),
            'products' => Product::find()->collection(),
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new PartnerVat();
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'НДС успешно добавлен.') : Yii::t('common', 'НДС успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'partners' => Partner::find()->active()->collection(),
            'countries' => Country::find()->active()->collection('common'),
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEditProduct($id = null)
    {
        if ($id) {
            $model = $this->getProductModel($id);
        } else {
            $model = new PartnerProductVat();
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'НДС успешно добавлен.') : Yii::t('common', 'НДС успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit-product', [
            'model' => $model,
            'products' => PartnerProduct::find()->active()->collectionKey('id')->collection(),
            'countries' => Country::find()->active()->collection('common'),
        ]);
    }

    /**
     * @param integer $id
     * @return PartnerProductVat
     * @throws HttpException
     */
    private function getProductModel($id)
    {
        $model = PartnerProductVat::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'НДС не найден.'));
        }

        return $model;
    }

    /**
     * @param integer $id
     * @return PartnerVat
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = PartnerVat::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'НДС не найден.'));
        }

        return $model;
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'НДС успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeleteProduct($id)
    {
        $model = $this->getProductModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'НДС успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }
}

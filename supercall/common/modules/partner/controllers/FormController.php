<?php

namespace common\modules\partner\controllers;

use backend\modules\administration\models\BaseLogger;
use common\components\web\Controller;
use common\models\Country;
use common\models\LoggerTag;
use common\models\Product;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAdditionPrice;
use common\modules\partner\models\PartnerFormAttribute;
use common\modules\partner\models\PartnerFormCopy;
use common\modules\partner\models\PartnerProduct;
use common\modules\partner\models\PromoProduct;
use common\modules\partner\models\PartnerFormFastChangeQueue;
use common\modules\partner\models\search\PartnerFormSearch;
use Yii;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Class FormController
 * @package common\modules\partner\controllers
 */
class FormController extends Controller
{
    public function beforeAction($action)
    {
        if ($action->id == 'get-form-attributes') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'sorting' => ['POST'],
                    'save-additional-params' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new PartnerFormSearch();

        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);
        $dataProvider = $modelSearch->search($query);

        $modelCopy = new PartnerFormCopy();
        if ($modelCopy->load(Yii::$app->request->post()) && $modelCopy->apply()) {
            return $this->refresh();
        }

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'contentVisible' => Yii::$app->session->get($filterSessId . '.isVisible', true),
            'modelCopy' => $modelCopy,
            'dataProvider' => $dataProvider,
            'partners' => Partner::find()->active()->collection(),
            'products' => Product::find()->collection(),
            'order_types' => OrderType::find()->collection(),
            'countries' => Country::find()->active()->collection('common'),
        ]);
    }

    public function actionGetFormAttributes()
    {
        $form_id = yii::$app->request->post('partner_form_id');

        $attributes = PartnerFormAttribute::getFormAttributes($form_id);

        $preparedAttributes = [];

        foreach ($attributes as $attribute) {
            $preparedAttributes[] = [
                'id' => $attribute->id,
                'name' => yii::t('common', $attribute->label)
            ];
        }

        return json_encode([
            'result' => empty($preparedAttributes) ? 'fail' : 'success',
            'data' => $preparedAttributes,
            'error' => empty($preparedAttributes) ? yii::t('common', 'У данной формы не найдены поля с пометкой "из КЛАДРа"') : ''
        ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new PartnerForm();
        }

        $additionalPrice = PartnerFormAdditionPrice::findOne(['form_id' => $id]);
        if (empty($additionalPrice)) {
            $additionalPrice = new PartnerFormAdditionPrice();
        }

        $fastChangeQueue = PartnerFormFastChangeQueue::findOne(['form_id' => $id]);
        if (empty($fastChangeQueue)) {
            $fastChangeQueue = new PartnerFormFastChangeQueue();
        }

        $oldAttributes = $model->getAttributes();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {

                PartnerForm::saveOnLogBaseAttributes($model->id, $oldAttributes, Yii::$app->request->post());

                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Форма успешно добавлена.') : Yii::t('common', 'Форма успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }


        return $this->render('edit', [
            'model' => $model,
            'partners' => Partner::find()->active()->collection(),
            'countries' => Country::find()->active()->collection('common'),
            'types' => OrderType::find()->collection(),
            'promoProductModel' => new PromoProduct(),
            'partner_products' => PartnerProduct::getPartnerProducts($model->partner_id, $model->country_id),
            'additionalPrice' => $additionalPrice,
            'fastChangeQueue' => $fastChangeQueue
        ]);
    }

    /**
     * Передаем form_id т.к. у формы может быть несколько условий
     */
    public function actionAdditionalPrice()
    {
        $form_id = Yii::$app->request->post('form_id');

        $model = PartnerFormAdditionPrice::findOne(['form_id' => $form_id]);

        if (!$model) {
            $model = new PartnerFormAdditionPrice();
        }

        $oldAttributes = $model->getAttributes();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
//не вижу - что в интерфейсе есть кнопка для нескольких промо акций
            if ($form_id) {
                $model->deleteAll(['form_id' => $form_id]);
                $model->form_id = $form_id;
            }

            if (!empty($model->addition_type) && !$model->save()) {
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка при сохранение. ' . $model->getFirstErrorAsString())
                ];
            } else {
                PartnerForm::saveOnLogAdditionalPrice($oldAttributes, Yii::$app->request->post());

                $result = [
                    'success' => true,
                    'message' => yii::t('common', 'Дополнительная цена успешна сохранена')
                ];
            }

            return json_encode($result, JSON_UNESCAPED_UNICODE);
        }
    }

    public function actionFastChangeQueue()
    {
        $model = new PartnerFormFastChangeQueue();
        $form_id = Yii::$app->request->post('form_id');

        $result = [
            'success' => false,
            'message' => yii::t('common', 'Ошибка при сохранение.')
        ];

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($form_id) {
                $model->deleteAll(['form_id' => $form_id]);
                $model->form_id = $form_id;
            }

            if ($model->save()) {
                $result = [
                    'success' => true,
                    'message' => yii::t('common', 'Быстрая семна очереди успешна сохранена')
                ];
            }
        } elseif ($form_id) {
            $model->deleteAll(['form_id' => $form_id]);
            $result = [
                'success' => true,
                'message' => yii::t('common', 'Быстрая семна очереди успешна сохранена')
            ];
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param integer $id
     * @return PartnerForm
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = PartnerForm::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Форма не найдена.'));
        }

        return $model;
    }

    public function actionAttribute($form_id, $id = null, $scope = null)
    {
        $form = $this->getModel($form_id);

        if ($id) {
            $model = PartnerFormAttribute::findOne($id);
            if (!$model) {
                throw new HttpException(404, Yii::t('common', 'Аттрибут не найден.'));
            }
            if ($model->form_id != $form->id) {
                throw new HttpException(404, Yii::t('common', 'Аттрибут не найден.'));
            }
        } else {
            if (!$scope) {
                throw new HttpException(400);
            }
            $model = new PartnerFormAttribute();
            $model->form_id = $form->id;
            $model->scope = $scope;
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->is_klader == false) {
                $model->is_disabled_user_data = false;
            }

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Аттрибут успешно добавлен.') : Yii::t('common', 'Аттрибут успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute(['edit', 'id' => $model->form->id]));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('attribute', [
            'model' => $model,
        ]);
    }

    public function actionDeleteAttribute($id)
    {
        $model = PartnerFormAttribute::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Аттрибут успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute(['edit', 'id' => $model->form->id]));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active', 'updated_at'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Форма успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active', 'updated_at'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Форма успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($count = $model->getOrders()->count() > 0) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Форма используется в заказах ({count}).', ['count' => $count]), 'error');
            return $this->redirect(Url::toRoute('index'));
        }

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Форма успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }


    /**
     * Задается сортировка Атрибутов формы Партнеров
     * @return bool
     */
    public function actionSorting()
    {
        // POST приходит новая сортировка
        $sorting = Yii::$app->request->post('sorting');

        foreach ($sorting as $key => $sort) {
            Yii::$app->db->createCommand()->update('{{%partner_form_attribute}}',
                ['sort' => ($key + 1)],
                ['id' => $sort])->execute();
        }

        // @todo Добавить возможность возвращать Yii::$app->notifier->addNotifier асинхронно
        return true;

    }

    public function actionSaveAdditionalParams()
    {
        $result = [
            'success' => true,
            'message' => yii::t('common', 'Дополнительные параметры сохранены')
        ];
//
//        $data = yii::$app->request->post('data');
//        $form_id = yii::$app->request->post('form_id');
//
//        $form = PartnerForm::findOne(['id' => $form_id]);
//
//
//        if (!$form) {
//            $result = [
//                'success' => false,
//                'message' => yii::t('common', 'Форма #{id} не найдена', ['form_id' => $form_id])
//            ];
//        } else {
//            if (empty($data)) {
//                $additional_params = null;
//            } else {
//                $converted_data = Json::decode($data);
//
//                if (strlen(Json::encode($converted_data['json'])) > 240) {
//                    $result = [
//                        'success' => false,
//                        'message' => yii::t('common', 'Превышен размер 240 символов у Json строки')
//                    ];
//                } else {
//                    $additional_params = Json::encode($converted_data['pair']);
//                }
//            }
//
//            if (isset($additional_params)) {
//                $form->use_additional_params = $additional_params;
//
//                if (!$form->save(false)) {
//                    $result = [
//                        'success' => false,
//                        'message' => $form->getFirstErrorAsString()
//                    ];
//                } else {
//                    $result = [
//                        'success' => true,
//                        'message' => yii::t('common', 'Дополнительные параметры сохранены')
//                    ];
//                }
//            }
//        }

        return Json::encode($result);
    }
}

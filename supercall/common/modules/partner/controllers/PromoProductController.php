<?php

namespace common\modules\partner\controllers;

use api\components\filters\VerbFilter;
use common\components\web\Controller;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PromoProduct;
use Yii;
use yii\db\Exception;
use yii\helpers\Html;
use yii\helpers\Json;

class PromoProductController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'save' => ['POST'],
                ],
            ],
        ];
    }

    public function actionSave()
    {
        $partner_products = yii::$app->request->post('product');
        $price = yii::$app->request->post('price');
        $form_id = yii::$app->request->post('form_id');

        /** @var PartnerForm $form */
        if (!$form = PartnerForm::find()->activeForm()->byId($form_id)->one()) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Форма не найдена или не активна')
            ];
        } elseif (empty($partner_products) || !is_array($partner_products)) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Некорректно указаны промо товары')
            ];
        } elseif (empty($price) || !is_array($price)) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Некорректно указаны цена по акции')//: {price}', ['price' => Html::encode($price)])
            ];
        } else {
            $result = [
                'success' => true,
                'message' => yii::t('common', 'Промо товары успешно сохранены')
            ];

            $transaction = yii::$app->db->beginTransaction();

            try {
                $oldPromoProducts = $form->promoProducts;

                PromoProduct::deleteAll(['form_id' => $form->id]);

                $promoProducts = [];

                foreach ($partner_products as $key => $partner_product) {
                    if(empty($partner_product)){
                        continue;
                    }
                    $promoProducts[] = [
                        'product_id' => $partner_product,
                        'form_id' => $form->id,
                        'price' => $price[$key],
                        'created_at' => time(),
                        'updated_at' => time()
                    ];
                }

                yii::$app->db->createCommand()->batchInsert(PromoProduct::tableName(), [
                    'product_id',
                    'form_id',
                    'price',
                    'created_at',
                    'updated_at'
                ], $promoProducts)->execute();

                $transaction->commit();

                PartnerForm::saveOnLogPromoProducts($form->id, $oldPromoProducts, $promoProducts);

            } catch (Exception $e) {
                $transaction->rollBack();
                $result = [
                    'success' => false,
                    'message' => $e->getMessage()
                ];
            }


        }

        return Json::encode($result);
    }
}
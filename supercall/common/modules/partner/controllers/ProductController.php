<?php
namespace common\modules\partner\controllers;

use common\components\web\Controller;
use common\models\Country;
use common\models\Product;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerProduct;
use common\modules\partner\models\search\PartnerProductSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class ProductController
 * @package common\modules\partner\controllers
 */
class ProductController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new PartnerProductSearch();
        
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);
        $dataProvider = $modelSearch->search($query);

        $products = ArrayHelper::map(Product::findAll(['active' => true]), 'id', 'name');

        foreach ($products as $key => $item) {
            $products[$key] = Yii::t('common', $item);
        }

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'contentVisible' => Yii::$app->session->get($filterSessId.'.isVisible', true),
            'dataProvider' => $dataProvider,
            'partners' => $this->getPartners(),
            'countries' => $this->getCountries(),
            'products' => $products,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new PartnerProduct();
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Товар успешно добавлен.') : Yii::t('common', 'Товар успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'partners' => $this->getPartners(),
            'products' => Product::find()->collection(),
            'countries' => $this->getCountries()
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active', 'updated_at'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Товар успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active', 'updated_at'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Товар успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Товар успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return PartnerProduct
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = PartnerProduct::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Товар не найден.'));
        }

        return $model;
    }

    /**
     * @return array
     */
    public function getCountries()
    {
        return ArrayHelper::map(yii::$app->user->getCountries(), 'id', function($val){
            return yii::t('common', $val->name);
        });
    }

    /**
     * @return array
     */
    public function getPartners()
    {
        return ArrayHelper::map(Partner::find()->byIdentityUser()->active()->all(), 'id', 'name');
    }
}

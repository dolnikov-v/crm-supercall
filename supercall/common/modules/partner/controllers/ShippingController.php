<?php
namespace common\modules\partner\controllers;

use api\models\ApiLog;
use common\components\web\Controller;
use common\models\Country;
use common\models\Shipping;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerShipping;
use common\modules\partner\models\search\PartnerShippingSearch;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\httpclient\Client;

/**
 * Class ShippingController
 * @package common\modules\partner\controllers
 */
class ShippingController extends Controller
{
    const NO_COUNTRY_ON_PARTNER_FOUND_ERROR_2WTRADE = 'Неверное значение страны';

    /** @var  ApiLog */
    public $log;

    /**
     * етод возвращает коллекцию игнорируемых (не ошибок) сообщений от партнёра - при попытке получить данные КС
     * @param $partner
     * @return array
     */
    public function getIgnoreErrorsOnPartner($partner)
    {
        $ignored_errors = [];

        switch ($partner) {
            case Partner::PARTNER_2WTRADE :
                $ignored_errors = [
                    self::NO_COUNTRY_ON_PARTNER_FOUND_ERROR_2WTRADE
                ];
                break;
        }

        return $ignored_errors;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new PartnerShippingSearch();

        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);
        $dataProvider = $modelSearch->search($query);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'contentVisible' => Yii::$app->session->get($filterSessId . '.isVisible', true),
            'dataProvider' => $dataProvider,
            'partners' => Partner::find()->active()->collection(),
            'shippings' => Shipping::find()->collection(),
            'countries' => Country::find()->collection('common')
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new PartnerShipping();
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Курьерская служба успешно добавлена.') : Yii::t('common', 'Курьерская служба успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'partners' => Partner::find()->active()->collection(),
            'shippings' => Shipping::find()->collection(),
            'countries' => Country::find()->collection('common')
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active', 'updated_at'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Курьерская служба успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active', 'updated_at'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Курьерская служба успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Курьерская служба успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return PartnerShipping
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = PartnerShipping::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Курьерская служба не найдена.'));
        }

        return $model;
    }

    /**
     * Функция определяет Курьер.службу по умолчанию
     * по двум параметрам: "country_id" и "partner_id"
     *
     * для всех остальных с этими параметрами is_default=FALSE
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionIsDefault($id)
    {
        $model = $this->getModel($id);
        $model->is_default = true;

        if ($model->save(true, ['is_default', 'updated_at'])) {
            $countryId = $model->country_id;
            $partnerId = $model->partner_id;

            // Изменим всем остальным с такими параметрами is_default на FALSE
            $changeShippings = PartnerShipping::find()->where([
                'country_id' => $countryId,
                'partner_id' => $partnerId,
            ])
                ->andWhere(['not in', 'id', $model->id])
                ->all();

            foreach ($changeShippings as $itemModel) {
                $itemModel->is_default = false;
                $itemModel->update(false, ['is_default', 'updated_at']);
            }
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Курьерская служба успешно установлена по умолчанию.'), 'success');

        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Отключить "по умолчанию"
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionIsNotDefault($id)
    {
        $model = $this->getModel($id);
        $model->is_default = false;

        if ($model->save(true, ['is_default', 'updated_at'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Курьерская служба отключена от использования "По умолчанию".'), 'success');

        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     *  метод определяет у какого партнёра забирать список курьерок
     * @ajax (post) param [] partner [name_partner1, name_partner2]
     * @return string;
     */
    public function actionGetPartnerDeliveryList()
    {
        $partners = yii::$app->request->post('partner');

        $errors = [];

        foreach ($partners as $partner) {
            switch ($partner) {
                case Partner::PARTNER_2WTRADE :
                    $result = $this->get2WtradeDeliveryList();

                    foreach ($this->getIgnoreErrorsOnPartner($partner) as $ignored) {
                        if (!$result['success'] && !preg_match("#" . $ignored . "#", $result['message'])) {
                            $errors[Partner::PARTNER_2WTRADE] = $result['message'];
                        }
                    }
                    break;
                default :
                    break;
            }
        }

        if (empty($errors)) {
            return json_encode(['success' => true, 'message' => yii::t('common', 'Список курьерских служб обновлён')], JSON_UNESCAPED_UNICODE);
        } else {
            $errorFormat = [];
            foreach ($errors as $partner => $error) {
                $errorFormat[] = yii::t('common', 'Ошибка {partner} : {error}', ['partner' => $partner, 'error' => $error]);
            }

            return json_encode(['success' => false, 'message' => implode("<br/>" . PHP_EOL, $errorFormat)], JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * @return string
     */
    public function get2WtradeDeliveryList()
    {
        $country_list = ArrayHelper::getColumn(Country::findAll(['active' => Country::ACTIVE]), 'char_code');

        $partner = Partner::find()->where(['name' => Partner::PARTNER_2WTRADE])->one();

        if (!$partner || !$partner['api_key'])
            return false;

        $errors = [];

        foreach ($country_list as $country) {
            if (is_null($country)) {
                continue;
            }

            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('get')
                ->setData([
                    'active' => 1,
                    'country' => $country
                ])
                ->setUrl(yii::$app->params['2wtradeApiDeliveryList'])
                ->addHeaders(['Authorization' => 'Bearer ' . base64_encode($partner['api_key'])])
                ->send();

            if (!$response->isOk) {
                $answer = json_decode($response->content, 1);
                $errors[$country] = yii::t('common', 'Ошибка получения {partner} по стране {country}, {error}', [
                    'partner' => Partner::PARTNER_2WTRADE,
                    'country' => $country,
                    'error' => implode("<br/>\n", $answer['data']['messages'])
                ]);
            } else {

                $list = json_decode($response->content, 1);

                if (!isset($list['status']) || $list['status'] == 'fail') {
                    $errors[$country] = json_encode($response, JSON_UNESCAPED_UNICODE);
                } else {
                    foreach ($list['data'] as $id => $delivery) {
                        $saveResult = $this->savePartnerDelivery($delivery, $country, $partner->id);
                        if (!$saveResult) {
                            $errors[$country] = yii::t('common', 'Ошибка сохранения {partner} по стране {country}', [
                                'partner' => Partner::PARTNER_2WTRADE,
                                'country' => $country
                            ]);
                        }
                    }
                }
            }


            $this->log = new ApiLog();
            $this->log->url = yii::$app->params['2wtradeApiDeliveryList'] . '?active=1&country=' . $country;
            $this->log->auth = json_encode(['Authorization' => 'Bearer ' . base64_encode($partner['api_key'])]);
            $this->log->request = json_encode(['active' => 1, 'country' => $country]);
            $this->log->response = $response->content;
            $this->log->save();
        }

        if (empty($errors)) {
            return ['success' => true, 'message' => yii::t('common', 'Данные по партнёру {partner} получены.', ['partner' => Partner::PARTNER_2WTRADE])];
        } else {
            return ['success' => false, 'message' => implode("<br/>" . PHP_EOL, $errors)];
        }
    }

    /**
     * @param [] $delivery
     * @param string $country
     * @param integer $partner_id
     * @return boolean
     */
    public function savePartnerDelivery($delivery, $country, $partner_id)
    {
        $transaction = yii::$app->db->beginTransaction();
        //определить страну
        $country = Country::findOne(['char_code' => $country]);

        if (is_null($country)) {
            return false;
        }

        //найти саму курьерку в колле
        $partnerShipping = PartnerShipping::find()
            ->where(['partner_shipping_id' => $delivery['id']])
            ->andWhere(['country_id' => $country->id]);

        //партнёрская КС не найдена по стране
        if (!$partnerShipping->exists()) {
            //есть ли вообще на колле такая кс ?
            $shipping = Shipping::find()->where(['name' => $delivery['name']]);

            //если не найдена
            if (!$shipping->exists()) {
                $shipping = new Shipping();
                $shipping->name = $delivery['name'];
                $shipping->created_at = time();
                $shipping->updated_at = time();

                if (!$shipping->save()) {
                    $transaction->rollBack();
                    return false;
                } else {
                    $shipping_id = $shipping->id;
                }
            } else {
                $shipping = $shipping->one();
                $shipping_id = $shipping->id;
            }

            if (isset($shipping_id)) {
                //добавим данные в партнёрские курьерки
                $partnerShipping = new PartnerShipping();
                $partnerShipping->partner_id = $partner_id;
                $partnerShipping->country_id = $country->id;
                $partnerShipping->price = 0;
                $partnerShipping->shipping_id = $shipping_id;
                $partnerShipping->partner_shipping_id = $delivery['id'];
                $partnerShipping->active = PartnerShipping::ACTIVE;
                $partnerShipping->created_at = time();
                $partnerShipping->updated_at = time();

                if (!$partnerShipping->save()) {
                    $transaction->rollBack();
                    return false;
                } else {
                    return true;
                }
            }
        } //курьерка по стране найдена, больше тут делать нечего
        else {
            return true;
        }

        $transaction->commit();
        return true;
    }
}

<?php

namespace common\modules\order\controllers;

use api\models\ApiLog;
use api\models\UrlRateLimiter;
use backend\modules\auth\models\LoginForm;
use backend\modules\catalog\models\PriceShipping;
use backend\modules\operator\models\ListOperators;
use backend\modules\order\controllers\OrderTaskController;
use backend\modules\order\models\OrderBackup;
use backend\modules\order\models\OrderTask;
use backend\modules\queue\models\Queue;
use backend\modules\stats\models\OrderProcessingTime;
use common\components\base\AjaxFilter;
use common\components\ChatApiCom;
use common\components\Platinum;
use common\components\web\Controller;
use common\models\Asterisk;
use common\models\Country;
use common\models\IncomingCall;
use common\models\LeadSQS;
use common\models\LiveMessage;
use common\models\QueueOrder;
use common\models\Timezone;
use common\models\User;
use common\modules\call\models\CallHistory;
use common\modules\order\components\notification\NoAnswerNotificationQueue;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderMessage;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderType;
use common\modules\order\models\OrderVirtualStatus;
use common\modules\partner\models\Package;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAdditionPrice;
use common\modules\partner\models\PartnerFormAttribute;
use common\modules\partner\models\PartnerProduct;
use common\modules\partner\models\PartnerShipping;
use common\widgets\ActiveForm;
use DateTime;
use DateTimeZone;
use frontend\modules\profile\models\Profile;
use Yii;
use yii\base\ExitException;
use yii\filters\RateLimiter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\i18n\Formatter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ChangeController
 * @package api\modules\order\controllers
 */
class ChangeController extends Controller
{
    const ORDER_DATA_BACKUP = 'order_backup';

    protected $current_time;

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = ($action->id != 'save-order-data-backup');
        return parent::beforeAction($action);
    }

    /**
     * List of allowed domains.
     * Note: Restriction works only for AJAX (using CORS, is not secure).
     *
     * @return array List of domains, that can access to this API
     */
    public static function allowedDomains()
    {
        return [
            yii::$app->params['urlApi'],
            yii::$app->params['urlBackend'],
            yii::$app->params['urlFrontend'],
        ];
    }

    public function behaviors()
    {
        return [
            'rateLimiter' => [
                'class' => RateLimiter::className(),
                'enableRateLimitHeaders' => false, // не передавать в хедере оставш. кол-во запросов и время
                'errorMessage' => Yii::t('api', 'Слишком много запросов'),
                'only' => ['index'], // Ограничения только для страницы /order/change/index
                'user' => new UrlRateLimiter(),
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600,
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'lock-order',
                    'unlock-order',
                    'continue-operator-work',
                    'shipping-list-by-broker'
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionSaveOrderDataBackup()
    {
        $orderBackup = new OrderBackup();
        return $orderBackup->saveOrderDataBackup();
    }


    /**
     * @param Order $order
     * @return bool
     */
    public function checkUpdateOrder(Order $order)
    {
        return !in_array($order->status, array_keys(Order::getFinalStatusesCollection())) ||
            in_array(User::ROLE_LOGISTICIAN, array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)));
    }

    /**
     * @param $id
     * @param string $operator
     * @return array|string|Response|boolean
     * @throws \Exception
     */
    public function actionIndex($id, $operator = '')
    {
        //$OrderTaskController = new OrderTaskController('order-task', yii::$app->getModule('order'));

        //возможно есть backup заказа
        //todo ни где не используется
        //$user_id = yii::$app->request->get('user_id');

        //ордера с финальными статусами не отдаются по API, нужен параметр - для игнорирования
        $order = $this->findOrder($id, yii::$app->request->get('quality-control'));
        //если есть бекап - то вернём бекап, если нет - то этот же ордер будет на выходе - без доп. запроса в бд
        if (yii::$app->params['useOrderBackup'] === true) {
            $order = OrderBackup::getBackupByOrder($order);
        }

        $orderValidate = clone $order;
        $order->operator = $operator;

        //ордер может оказаться бекапом ордера
        if (yii::$app->params['useOrderBackup'] === true) {
            $order = $order instanceof OrderBackup ? OrderBackup::backupTransferToOrder($order) : $order;
        }

        //$orderTasks = $OrderTaskController->getOrderTasks($order->id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $apiLog = new ApiLog();
            $apiLog->url = '/common/order/change/';
            $apiLog->request = json_encode(array_merge($post, Yii::$app->request->get()));
            $apiLog->order_id = $id;
            $apiLog->operator = Yii::$app->request->get('operator');
            $apiLog->save();

            if (yii::$app->request->get('quality-control')) {
                if ($post['CallHistory']['start_status'] == Order::STATUS_APPROVED) {
                    $order->setScenario(Order::APPROVE_STATUS_SCENARIO);
                }
            } else {
                if (!isset($post['Order']['status'])) {
                    $post['Order']['status'] = Order::STATUS_FAIL;
                }
                if (isset($post['Order']) && $post['Order']['status'] == Order::STATUS_APPROVED) {
                    $order->setScenario(Order::APPROVE_STATUS_SCENARIO);
                }
            }

            //обновить конечный статус заказа после звонка
            if (yii::$app->request->get('quality-control') != 1 && isset($post['CallHistory'])) {
                CallHistory::updateEndStatus($order->id, $post['Order']['status'], $post['CallHistory']['group_id']);
            }

            if (Yii::$app->user->isGuest) {
                $user = User::findOne(['id' => yii::$app->request->get('user_id')]);

                if ($user) {
                    /** @var LoginForm $loginForm */
                    $loginForm = new LoginForm();
                    $loginForm->username = $user->username;
                    $loginForm->login(false);
                }

                unset($user);
            }

            if ($this->checkUpdateOrder($order)) {
                if (empty($order->operator)) {
                    $order->operator = Yii::$app->user->identity->username;
                }

                //this not order - this task
                /*if (isset($post['Order']['is_task'])) {
                    //create task;
                    return $OrderTaskController->saveTask();
                }*/
                $callHistoryData = yii::$app->request->post('CallHistory');

                /** Для отчета /audit/stats-key-word/index нужно логи связать с CallHistory */
                if (isset($callHistoryData['group_id']) && !empty($callHistoryData['group_id'])) {
                    //для ordeLogs
                    // common/modules/order/models/Order.php:981
                    // $log->group_id = $this->group_id ? $this->group_id : $groupId;
                    $order->group_id = $callHistoryData['group_id'];
                    // для orderProductLogs
                    // common/modules/order/models/OrderProduct.php:151
                    // $log->group_id = $this->group_id ? $this->group_id : $groupId;

                }

                $order->load($post);

                /** @var OrderProduct[] $products */
                $products = [];
                /** @var OrderProduct[] $existing_products */
                if (isset($order->is_backup)) {
                    $product_list = OrderProduct::findAll(['order_id' => $order->id]);
                    $existing_products = ArrayHelper::index($product_list, 'id');
                } else {
                    $existing_products = ArrayHelper::index($order->orderProducts, 'id');
                }

                //отделить товары и promo товары
                $orderProducts = OrderProduct::selectProducts(Yii::$app->request->post('OrderProduct'));

                //Promo products
                //определить список promo товаров
                $orderPromoProducts = OrderProduct::selectPromoProducts(Yii::$app->request->post('OrderProduct'));
                //end Promo products

                if ($orderProducts) {
                    foreach ($orderProducts as $item) {
                        // c пустым product_id игнорируем
                        if (!empty($item['product_id'])) {
                            $product = ArrayHelper::remove($existing_products, $item['id']);
                            if (!$product) {
                                $product = new OrderProduct();

                                //если модель ордера по модели бекапа собрана
                                $product->order_id = $order->id;
                            }
                            //явно указать текущую модель заказа в виде связи чтобы иметь доступ к иду группы для логов
                            $product->populateRelation('order', $order);
                            $product->load($item, '');
                            $products[] = $product;
                        }
                    }
                }

                if (Yii::$app->request->post('ajax') == 'form-order') {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    if ($order->status == Order::STATUS_APPROVED) {
                        if (!empty($products)) {
                            $order->populateRelation('orderProducts', $products);
                        }
                        return ActiveForm::validate($order, $order->getGeneralModel(), $order->getAddressModel());
                    } else {
                        unset($post['Order']['address']);
                        $orderValidate->load($post);
                        if (!empty($products)) {
                            $orderValidate->populateRelation('orderProducts', $products);
                        }
                        return  ActiveForm::validate($orderValidate, $orderValidate->getGeneralModel());
                    }
                }

                $order->beginSave();

                //Promo products
                //охранить promo товары к заказу
                //Именно в транзакции
                OrderProduct::savePromoProducts($orderPromoProducts, $order);
                //end Promo products

                //Order::beforeValidate содержит логику с виртуальными статусами - подстатусами

                //Увеличиваем кол-во попыток обработки заказа
                if (Yii::$app->user->isGuest) {
                    $order->attempts++;
                } else {
                    $userRoles = array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id));
                    if (in_array(Profile::ROLE_OPERATOR, $userRoles)) {
                        $order->attempts++;
                    }
                }

                try {
                    if (yii::$app->request->get('quality-control') != 1) {
                        if (!$order->save()) {
                            throw new \Exception(Yii::t('common', 'Не удалось сохранить заказ'));
                        }
                    } else {
                        if (!$order->saveWithoutLogs(true, ['status'])) {
                            throw new \Exception(Yii::t('common', 'Не удалось сохранить заказ'));
                        }
                    }

                    //Обновляем данные в КЛАДРе

                    if ($order->status == Order::STATUS_APPROVED) {
                        $order->updateCountOrdersOnPhone();
                    }
                    // Если POST-запрос от iframe страницы заказа, то необходимо сделать проверку на связь с очередью у данного заказа
                    // и если связь с очередью есть, то необходимо статус данного заказа сверить со стратегией очереди и при
                    // необходимости внести изменения
                    // ВАЖНО! Только для заказов со статусом: НЕДОЗВОН
                    //для теста
                    //if (1==1 /*$this->checkUseStrategyQueue()*/) {
                    if ($this->checkUseStrategyQueue()) {
                        if (($order->status == $order::STATUS_FAIL || $order->status == $order::STATUS_RECALL) && $order->useStrategyByQueue()) {
                            $params = $order->dirtyAttributes;
                            if (key_exists('status', $params)) {
                                // $order->update(false, array_keys($params));
                                // сделать запись логов в order_logs
                                $order->saveLogsByOrder();
                            }
                            // прямая запись в таблицу order
                            Order::updateByParams($params, ['id' => $order->id]);

                        } // Для финальных статусов сбросить last_queue_id
                        else if (in_array($order->status, array_keys(Order::getFinalStatusesCollection()))) {
                            Order::updateByParams(['last_queue_id' => null], ['id' => $order->id]);
                        }
                    }

                    //Check partner form on turn on use messenger
                    if ($order->form_id) {
                        if ($order->form->use_messenger) {
                            if ($order->status == Order::STATUS_FAIL) {
                                if (OrderLog::find()
                                        ->where([
                                            'order_id' => $order->id,
                                            'field' => 'status',
                                            'new' => Order::STATUS_FAIL
                                        ])
                                        //->andWhere(['is', 'prevent_queue_id', null])
                                        ->count() >= Order::COUNT_NOANSWER_FOR_START_MESSAGE
                                ) {
                                    /**
                                     * инициация чата с клиентом по Whatsapp
                                    **/

                                    foreach([$order->customer_phone, $order->customer_mobile] as $k=>$phone){
                                        if(!empty($phone)) {
                                            $liveMessageData = [
                                                'order_id' => $order->id,
                                                'user_id' => yii::$app->user->id,
                                                'is_new' => LiveMessage::IS_NEW,
                                                'created_at' => time(),
                                                'updated_at' => time(),
                                                'type' => LiveMessage::TYPE_WHATSAPP,
                                                'content' => null,
                                                'phone' => $phone,
                                                'queue_number' => null,
                                                'chat_id' => null
                                            ];

                                            $liveMessage = new LiveMessage($liveMessageData);
                                            $liveMessage->save(false);
                                        }
                                    }
                                    /**
                                     * окончание работы с Whatsapp
                                     **/

                                    if (!is_null($order->last_queue_id)) {
                                        $order->updateByParams(['prevent_queue_id' => $order->last_queue_id], ['id' => $order->id]);
                                        $order->updateByParams(['last_queue_id' => null], ['id' => $order->id]);
                                    }
                                }
                            }
                        }
                    }

                    // Удалить из сессии ID заказа, чтобы, чтоб освободить "место" для нового заказа
                    $this->removeOrderFromQueue($order->id);

                    if (!empty($products)) {
                        foreach ($products as $item) {
                            if (!$item->save()) {
                                throw new \Exception(Yii::t('common', 'Не удалось сохранить заказ'));
                            }
                        }
                    }

                    if (!empty($existing_products)) {
                        foreach ($existing_products as $existing_product) {
                            if ($existing_product->gift || !empty($products)) {
                                if (!$existing_product->delete()) {
                                    throw new \Exception(Yii::t('common', 'Не удалось сохранить заказ'));
                                }
                            }
                        }
                    }

                    $order->completeSave();

                    if($user_id = Yii::$app->request->get('user_id')){
                        $user = User::findOne(['id' => $user_id]);
                        //снять с паузы на asterisk
                        if($user && $user->auto_call){
                            $userSip = $user->sipArray;
                            Asterisk::setUserPaused(0, $userSip['sip_login']);
                        }
                    }

                    //заказ сохранён. бекапы больше не нужны
                    $isBackup = OrderBackup::getBackupByOrder($order);

                    if ($isBackup instanceof OrderBackup) {
                        OrderBackup::dropBackup($isBackup, yii::$app->user->id);
                    }

                    //Статус заказа был изменён, необходимо эти изменения отправит в очередь на Амазон для пея
                    $leadSQSModel = new LeadSQS();

                    $order->refresh();

                    //для продуктов нужны partner_product_id
                    //если нет foreign_id то пей сам создасть его, для этого у него есть метод
                    foreach ($order->orderProducts as $product) {
                        $product->partner_product = $this->getPartnerProduct($product->product_id, $order->partner_id, $order->country_id);
                    }

                    //АПРУВ c confirm_address = 1 - адрес не из КЛАДР и требует подтверждения
                    /*if (
                        //отправить сходу все НЕ апрув с неподтверждённым адресом (т.к. нам не важно пока - адрес может быть изменён)
                        $order->confirm_address == AddressVerification::ADDRESS_NEED_CONFIRM && $order->status != Order::STATUS_APPROVED
                        //и все заказы у которых признак подверждение адреса - не соответствует - "нуждается в проверке"
                        or $order->confirm_address != AddressVerification::ADDRESS_NEED_CONFIRM
                    ) {*/
                    //Проверяем, если у заказа есть foreign_id значит это заказ пришел к нам с пея
                    if ($order->foreign_id) {
                        //отправить в очередь
                        $leadSQSModel->sendLeadStatus($order);
                    } else { //Иначе считаем что этот заказ был создан в КЦ
                        $order->partner_created_at = time();
                        $leadSQSModel->sendNewLead($order);
                    }
                    //}
                    //"Списание остатков" товаров у партнёра
                    Order::withdrawProductsOfStock($order->id);

                    OrderProcessingTime::updateData([
                        'order_id' => $order->id,
                        'status' => $order->status
                    ]);

                    // Отправка клиенту сообщения о недозвоне
                    if (NoAnswerNotificationQueue::shouldSendNotification($order)) {
                        NoAnswerNotificationQueue::add([
                            'order' => $order,
                            'groupId' => $post['CallHistory']['group_id']
                        ]);
                    }

                    //Удаляем заказ из queue_order
                    /*if(isset($order->id) && is_numeric($order->id)){
                        Yii::$app->db->createCommand('DELETE FROM "queue_order" WHERE order_id = '. $order->id)->execute();
                    }*/

                } catch (\Exception $e) {
                    $order->cancelSave([$e->getFile(), $e->getLine(), $e->getMessage()]);
                    throw $e;
                }
            } else {
                if (Yii::$app->request->post('ajax') == 'form-order') {
                    // Удалить из сессии ID заказа, чтобы, чтоб освободить "место" для нового заказа
                    $this->removeOrderFromQueue($order->id);
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [];
                }

            }

            //работа с триггерами очередей
            $this->ApplyEventsQueue($order);

            if (!Yii::$app->request->get('hold_on_pause')) {
                Asterisk::setUserPaused(0);
            }

            return $this->responseAfterOrderSaved($order);
        }

        $types = ArrayHelper::map(
            OrderType::find()->all(),
            'id',
            'name'
        );

        // Вытащим все товары с ценами, установленные Партнером
        $productsWithPrice = PartnerProduct::find()
            ->select(['partner_product_id', 'product_id', 'price_data', 'amount'])
            ->where([
                'country_id' => $order->country_id,
                'partner_id' => $order->partner_id,
                PartnerProduct::tableName() . '.active' => 1
            ])
            ->orderBy('partner_product_id')
            ->with('product')
            ->active()
            ->all();

        // Далее, необходимо вытащить их имена и ID в нашей системе
        $products = ArrayHelper::map(
            $productsWithPrice,
            'product_id',
            'product.name'
        );

        $products_price_data = [];
        foreach ($productsWithPrice as $partnerProduct) {
            // для каждого количества
            foreach (OrderProduct::countList() as $keyProduct => $quantity) {
                $data = $this->productPrice($partnerProduct->product_id, $order, $quantity);
                $products_price_data[$partnerProduct->product_id]['amount'] = is_null($data['amount']) ? 0 : $data['amount'];
                unset($data['amount']);
                $products_price_data[$partnerProduct->product_id][$keyProduct] = $data;
            }
        }

        $query = PartnerShipping::find()->where([
            'partner_id' => $order->partner_id,
            'country_id' => $order->country_id
        ])->active();

        $priceShipping = PriceShipping::find()
            ->where(['partner_id' => $order->partner_id])
            ->where(['country_id' => $order->country_id])
            ->asArray()
            ->all();

        // UN-56 Доставка по-умолчанию
        $is_default = false; // по умолчанию на параметр is_default не смотрим
        if ($order->form && !$order->form->show_shipping) {
            // Необходимо выбрать курьер.службу по умолчанию
            $queryCopy = clone $query;
            // уточнить запрос для клонированного объекта
            $queryCopy->andWhere([
                'is_default' => true,
            ]);

            // Если дефолтный курьер есть
            if ($queryCopy->exists()) {
                $query = $queryCopy; // тогда присвоим основному запросу копию
                // И определим дефолтного курьера как основного для данного заказа
                $order->shipping_id = $queryCopy->one()->id;
                $is_default = true; // указать, что используется Кур.служба по умолчанию
            }
        }

        $full_name_shippings = [];
        $shippings = $query->all();
        $shipping_options = ArrayHelper::map($shippings, 'shipping.id', function (PartnerShipping $shipping) {
            return [
                'data-price' => Yii::$app->formatter->asDecimal($shipping->price, 2),
                'data-dynamic_price' => $shipping->dynamic_price ? 1 : 0,
                'data-with_country' => 1
            ];
        });


        foreach ($shippings as $k => $v) {
            $full_name_shippings[$k] = $v;
            $full_name_shippings[$k]->shipping->name = $v->shipping->name . ' ' . $v->price;
        }

        $shippings = ArrayHelper::map($full_name_shippings, 'shipping.id', 'shipping.name');
        $countries = ['' => ''] + ArrayHelper::map(Country::find()->all(), 'id', 'name');;
        $vat_list = PartnerProduct::getVatList($order->partner_id);

        $view = $this->getOrderView($order);

        //информация о последнем таске - для формы создания таска - т.к. там информация из заказа в том случае - если нет не принятых тасков или
        //подойдёт любой таск, только не неактуальный - т.к. предполагается - что неактуальный таск оприори быть последним не может
        /*$lastOrderTask = $OrderTaskController->getLastTask($order->id, [
            OrderTask::TASK_STATUS_REJECTED,
            OrderTask::TASK_STATUS_APPROVED,
            OrderTask::TASK_STATUS_NEW,
            OrderTask::TASK_STATUS_PENDING
        ]);*/

        /*if ($lastOrderTask) {
            $orderTask = json_decode($lastOrderTask->order_new);
            $customer_components = json_decode($orderTask->customer_components);
            $orderTasksProducts = json_decode($lastOrderTask->product_new);

            $orderTaskData = [
                'orderTask' => $orderTask,
                'customer_components' => $customer_components,
                'orderTasksProducts' => $orderTasksProducts
            ];
        }*/

        $packages = Package::getPackagesByPartnerCountry($order->partner_id, $order->country_id);
        $packagesList = [];
        $packagesData = [];
        $packagesList[''] = '-';
        $productPrices = [];

        foreach ($packages as $package) {
            $full_name_package = (!empty($package['prefix'])) ? ($package['prefix'] . ' ' . $package['name']) : $package['name'];
            $packagesList[$package['id']] = $full_name_package;

            $packageGifts = [];

            foreach ($package->packageGifts as $gift) {
                $packageGifts[] = [
                    'product_id' => $gift->product_id,
                    'free_amount' => $gift->free_amount,
                    'product_name' => $gift->product->name
                ];
            }

            $packagesData[$package['id']] = [
                'id' => $package['id'],
                'name' => $full_name_package,
                'partner' => $package['partner_id'],
                'country' => $package['country_id'],
                'product' => $package['product_id'],
                'paid_amount' => $package['paid_amount'],
                'gifts' => $packageGifts,
                'product_price' => $package['product_price']
            ];

            $productPrices[$package['id']] = $package['product_price'];
        }

        //если используется модель backup
        if ($order instanceof OrderBackup) {
            $order_id = $order->order_id;
            $order = OrderBackup::backupTransferToOrder($order);
            $order->setAttributes(['id' => $order_id]);
        }

        if (isset(Yii::$app->user)
            && method_exists(Yii::$app->user, 'getRoles')
            && (array_key_exists(User::ROLE_AUDITOR, Yii::$app->user->getRoles())
                || array_key_exists(User::ROLE_LOGISTICIAN, Yii::$app->user->getRoles()))
        ) {
            $editable = true;
        } else {
            $editable = $this->getOrderEditable($order);
        }

        $formatter = new Formatter;
        $formatter->dateFormat = 'php:d/m/Y H:i:s';
        $formatter->timeZone = $order->country->timezone->timezone_id;

        if (is_numeric($order->partner_created_at) && yii::$app->params['show_time_from_creation']) {
            $partner_created_at = [
                'timestamp' => $order->partner_created_at,
                'date' => $formatter->asDate($order->partner_created_at)
            ];
        } else {
            $partner_created_at = null;
        }

        $time_now_with_timezone = $formatter->asDate(time());

        $disabled_buttons_from_attribute_form = true;

        if ($order->form) {
            $disabled_buttons_from_attribute_form = !$order->form->disabled_buttons;
        }

        $hide_status_buttons = Yii::$app->request->get('hide_status_buttons');
        $disabled_status_buttons = $disabled_buttons_from_attribute_form || Yii::$app->request->get('disabled_status_buttons') !== null ? Yii::$app->request->get('disabled_status_buttons') : true;

        if (!empty(Yii::$app->request->get('user_id')) && array_key_exists(User::ROLE_LOGISTICIAN, Yii::$app->authManager->getRolesByUser(Yii::$app->request->get('user_id')))) {

            $editable = true;
            $hide_status_buttons = false;
            $disabled_status_buttons = false;
        }

        $partnerFormAdditionalPrice = PartnerFormAdditionPrice::findOne(['form_id' => $order->form_id]);
        if (!empty($partnerFormAdditionalPrice)) {
            $partnerFormAdditionalPrice = json::encode($partnerFormAdditionalPrice);
        }

        return $this->render($view, [
            'order' => $order,
            'editable' => $editable,
            'types' => $types,
            'products' => $products,
            'vat_list' => $vat_list,
            'shippings' => $shippings,
            'countries' => $countries,
            'is_default_shipping' => $is_default,
            'shipping_options' => $shipping_options,
            'products_price_data' => Json::encode($products_price_data),
            'operator' => $operator,
            'orderTasks' => [],//$orderTasks,
            'lastOrderTask' => isset($orderTaskData) ? $orderTaskData : false,
            'countOrdersOnPhone' => $order->getCountOrdersOnPhone(),
            'formAttributes' => $this->getFormAttribute($order->country_id, $order->partner_id, $order->form_id),
            'packageList' => $packagesList,
            'packageData' => json_encode($packagesData, JSON_UNESCAPED_UNICODE),
            'priceShipping' => json_encode((is_null($priceShipping) ? [] : $priceShipping), JSON_UNESCAPED_UNICODE),
            'masked_phones' => false,
            'partner_created_at' => $partner_created_at,
            'time_now_with_timezone' => $time_now_with_timezone,
            'operators' => ArrayHelper::map(ListOperators::getOperators($order->country->id), 'id', 'username'),
            'hide_status_buttons' => $hide_status_buttons,
            'disabled_status_buttons' => $disabled_status_buttons,
            'partnerFormAdditionalPrice' => $partnerFormAdditionalPrice,
            'productPrice' => json_encode($productPrices)
        ]);
    }

    /**
     * Снаять пользователся с заказа
     * Разблокировать заказ
     */
    public function actionReleaseOperator()
    {
        $order_id = yii::$app->request->post('order_id');

        try {
            yii::$app->db->createCommand("DELETE FROM queue_order WHERE order_id = :order_id")->bindValues([
                ':order_id' => $order_id
            ])->execute();
        } catch (ExitException $e) {
            return json_encode($e->getMessage(), JSON_UNESCAPED_UNICODE);
        }

        return json_encode([
            'success' => true,
            'message' => yii::t('common', 'Оператор снят с заказа.')
        ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Метод назначения оператора на заказ
     * @return string
     */
    public function actionAppointmentOperator()
    {
        $post = yii::$app->request->post();
        $error = '';

        if (is_null($post['order_id'])) {
            $error = yii::t('common', 'Ошибка определения заказа');
        }

        if (is_null($post['user_id'])) {
            $error = yii::t('common', 'Ошибка определения оператора');
        }

        if (empty($error)) {
            try {
                $transaction = yii::$app->db->beginTransaction();

                Yii::$app->db->createCommand()->delete('{{%queue_order}}', [
                    'order_id' => $post['order_id']
                ])->execute();

                $blocked_to = new DateTime('now', new DateTimeZone(Yii::$app->user->timezone->timezone_id));

                Yii::$app->db->createCommand()->insert('{{%queue_order}}', [
                    'user_id' => $post['user_id'],
                    'order_id' => $post['order_id'],
                    'blocked_to' => $blocked_to->getTimestamp() + (30 * 60),
                ])->execute();
            } catch (\Exception $e) {
                $transaction->rollBack();
                return json_encode([
                    'success' => false,
                    'message' => yii::t('common', 'Произошла ошибка : ' . $e->getMessage())
                ], JSON_UNESCAPED_UNICODE);
            }

            $transaction->commit();
        }

        return json_encode([
            'success' => true,
            'message' => yii::t('common', 'Оператор успешно назначен')
        ], JSON_UNESCAPED_UNICODE);
    }

    //применение триггеров очередей к заказам
    /**
     * @param Order $order
     */
    public function ApplyEventsQueue($order)
    {
        //этот алгоритм должен работать только на фронтенде, когда с заказом из очереди работает оператор
        if (is_null(Yii::$app->request->get('operator'))) {
            return;
        }

        //у заказа нет очереди
        if (is_null($order->last_queue_id)) {
            return;
        }

        $queue = Queue::find()
            ->where(['id' => $order->last_queue_id])
            ->andWhere(['active' => Queue::STATUS_ACTIVE])
            ->one();

        //очередь не найдена
        if (is_null($queue)) {
            return;
        }

        //триггер очереди
        $event = ArrayHelper::getValue($queue, 'event');

        //триггер не используется
        if (empty($event)) {
            return;
        }

        $eventStrategy = empty($event['changeStatus']) ? Queue::EVENT_SCENARIO_CHANGE_QUEUE : Queue::EVENT_SCENARIO_CHANGE_STATUS;

        //заказ попал под триггер очереди
        $eventStatuses = explode("_", $event['eventType']);

        if (in_array($order->status, $eventStatuses)) {
            //заваз ещё не проходил по триггеру очереди
            if (is_null($order->event)) {
                $order->event = json_encode([
                    strval($event['eventType']) => [
                        'count' => 1
                    ]
                ]);
            } //к заказу был ранее применён триггер (не факт, что заказ был изменён триггером, но статистика имеется)
            else {
                $orderEvent = json_decode($order->event, 1);

                //был ли данный триггер использован на данном ордере ?
                if (isset($orderEvent[$event['eventType']])) {
                    $orderEvent[$event['eventType']]['count'] += 1;
                    //для стратегии триггера по смене статуса - история счётчиков нам нужна
                    $order->event = json_encode($orderEvent);
                    //применить триггер
                    if ($orderEvent[$event['eventType']]['count'] >= $event['eventCount']) {
                        //если меняем очередь - очистить статистику евентов заказа
                        if ($eventStrategy == Queue::EVENT_SCENARIO_CHANGE_QUEUE) {
                            //назначить очередь
                            $order->last_queue_id = $event['changeQueue'];
                            //очистить счётчики
                            $order->event = null;
                        } //стратегия триггера - сменить статус заказа
                        elseif ($eventStrategy == Queue::EVENT_SCENARIO_CHANGE_STATUS) {
                            //предполагается, что статус в стратегии тригера будет последним - т.е., например - треш
                            //в очередь данный заказ больше попадать не должен
                            $order->last_queue_id = null;
                            $order->status = $event['changeStatus'];

                            if (isset($event['changeSubStatus']) && !empty($event['changeSubStatus'])) {
                                $order->sub_status = $event['changeSubStatus'];
                            }
                        }
                    }
                    //заказ ещё не попадал под триггер данной очереди
                } else {

                    $orderEvent[strval($event['eventType'])] = ['count' => 1];
                    $order->event = json_encode($orderEvent, JSON_UNESCAPED_UNICODE);
                }
            }

            $order->save();

        }
    }

    /**
     * @param $country_id
     * @param $partner_id
     * @return array
     */
    public function getFormAttribute($country_id, $partner_id, $form_id = false)
    {
        $attributes = [];

        if (!$form_id) {
            $form = PartnerForm::find()
                ->where(['country_id' => $country_id])
                ->andWhere(['partner_id' => $partner_id])
                ->andWhere(['active' => 1])
                ->one();

            if ($form) {
                $attributes = PartnerFormAttribute::getFormAttributes($form->id);
            }
        } else {
            $attributes = PartnerFormAttribute::getFormAttributes($form_id);
        }


        $preparedAttributes = [];
        $coordinateAttributes = [];

        foreach ($attributes as $attribute) {
            if ($attribute->is_klader) {
                $preparedAttributes[] = [
                    'id' => $attribute->id,
                    'name' => yii::t('common', $attribute->name),
                    'label' => yii::t('common', $attribute->label),
                    'is_disabled_user_data' => $attribute->is_disabled_user_data
                ];
            }

            if ($attribute->is_coordinate) {
                $coordinateAttributes[] = [
                    'id' => $attribute->id,
                    'name' => yii::t('common', $attribute->name),
                    'label' => yii::t('common', $attribute->label),
                    'is_disabled_user_data' => $attribute->is_disabled_user_data
                ];
            }
        }

        return [
            'form_id' => isset($form) ? $form->id : null,
            'attributes' => $preparedAttributes,
            'coordinates' => $coordinateAttributes,
        ];


    }

    /**
     * @param $id
     * @return Order|bool
     * @throws NotFoundHttpException
     */
    protected function findOrder($id, $quality_control = false)
    {
        $order = Order::findOne($id);
        if (!$order) {
            throw new NotFoundHttpException(Yii::t('common', 'Заказ не найден.'));
        }
        return $order;
    }

    /**
     * Функция делает проверку на необходимость применения Стратегии очереди к заказу
     * Если неавторизованный пользователь, то предполагаем, что это обращение через iframe, по API,
     * либо если авторизованный, то это Оператор из кабинета (@frontend)
     *
     * @return bool
     */
    private function checkUseStrategyQueue()
    {
        // Запрос по API, из iframe
        if (Yii::$app->user->isGuest) {
            return true;
        } // Определить роль, т.к. стратегия очереди применима только для Операторов
        else {
            $userRoles = array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id));
            if (in_array(Profile::ROLE_OPERATOR, $userRoles)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Функция определит наличие в сессии ID заказа, который находится на редактировании у Оператора
     */
    protected function removeOrderFromQueue($orderId) // removeSessionOrder()
    {
        QueueOrder::deleteAll(['order_id' => $orderId]);
    }

    /**
     * @param $product_id
     * @param $partner_id
     * @return PartnerProduct
     */
    public function getPartnerProduct($product_id, $partner_id, $country_id = false)
    {
        $condition = ['product_id' => $product_id];
        if ($country_id && is_numeric($country_id)) {
            $condition['country_id'] = $country_id;
        }
        return PartnerProduct::find()
            ->where($condition)
            ->andWhere(['partner_id' => $partner_id])
            ->andWhere(['active' => 1])
            ->one();

    }

    /**
     * @param Order $order
     * @return Response
     */
    protected function responseAfterOrderSaved(Order $order)
    {
        return $this->refresh();
    }

    /**
     * @param integer $product_id
     * @param Order $order
     * @param integer $quantity
     * @return array|bool
     */
    protected function productPrice($product_id, $order, $quantity)
    {
        if ($order && $product_id) {
            $product = PartnerProduct::find()->where([
                'partner_id' => $order->partner_id,
                'product_id' => $product_id,
                'country_id' => $order->country_id,
                'active' => 1
            ])->one();

            if (!$product) {
                return false;//throw new NotFoundHttpException(Yii::t('common', 'Товар не найден.'));
            }

            if ($order->orderProducts) {
                foreach ($order->getOrderProducts()->where(['gift' => false, 'promo' => false])->all() as $orderProduct) {
                    if ($orderProduct->product_id == $product_id && !$orderProduct->gift && !$orderProduct->package_id) {
                        return [
                            //для заказов пришедших извне - стоимость товара указана в orderProduct и она фиксирована
                            'price' => $orderProduct->price,
                            'vat' => $product->getVat($order->country_id),
                            'amount' => $product->amount
                        ];
                    } else {
                        return [
                            'price' => $product->getPrice($quantity),
                            'vat' => $product->getVat($order->country_id),
                            'amount' => $product->amount
                        ];
                    }
                }
            } else {
                return [
                    'price' => $product->getPrice($quantity),
                    'vat' => $product->getVat($order->country_id),
                    'amount' => $product->amount
                ];
            }
        }
    }

    /**
     * Функция вернет цену товара за 1 шт, для корректного расчета
     * на странице заказа
     * Функционал пересекается с change.js
     * @todo Вынести в json-файлы
     *
     * @param $orderProduct
     * @return float|int
     */
    private function _priceForOne($orderProduct)
    {
        $price = !empty($orderProduct->cost)
            ? $orderProduct->cost // если уже старый заказ
            : $orderProduct->price; // если новый заказ

        return ($orderProduct->quantity == 0) ? 0 : $price / $orderProduct->quantity;
    }

    /**
     * @param Order $order
     * @return string
     */
    protected function getOrderView($order)
    {
        $result = 'index';

        $countryView = mb_strtolower($order->country->char_code) . DIRECTORY_SEPARATOR . 'index';
        $countryViewPath = $this->viewPath . DIRECTORY_SEPARATOR . $countryView;

        if (file_exists($countryViewPath . '.php')) {
            $result = $countryView;
        }

        return $result;
    }

    /**
     * @param Order $order
     * @return bool
     */
    protected function getOrderEditable(Order $order)
    {
        return !in_array($order->status, array_keys(Order::getFinalStatusesCollection()));
    }

    /**
     * @param $id
     * @param $index
     * @return string
     */
    public function actionAddProduct($id, $index)
    {
        $order = $this->findOrder($id);

        $products = ['' => ''] + ArrayHelper::map(
                PartnerProduct::find()->joinWith('product')->where(['partner_id' => $order->partner_id])->all(),
                'product.id',
                'product.name'
            );

        $product = new OrderProduct();
        $product->populateRelation('order', $order);

        return $this->renderAjax('add-product', [
            'order' => $order,
            'product' => $product,
            'products' => $products,
            'index' => $index
        ]);
    }

    /**
     * @param $id
     * @param $product_id
     * @param $country_id
     * @param $quantity
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionProductPrice($id, $product_id, $country_id, $quantity)
    {
        $order = $this->findOrder($id);
        Yii::$app->response->format = Response::FORMAT_JSON;

        return $this->productPrice($product_id, $order, $quantity);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionMessage($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $order = $this->findOrder($id);

        $message = new OrderMessage();
        $message->order_id = $order->id;
        $message->load(Yii::$app->request->post(), '');
        $message->status = OrderMessage::STATUS_WAITING;
        if ($message->save()) {
            return [
                'message' => Yii::t('common', 'Отправлено')
            ];
        }
        return [
            'error' => Yii::t('common', 'Не удалось отправить сообщение')
        ];
    }

    /**
     * Блокировка заказа на lock_time
     * @param $id
     * @return array
     */
    public function actionLockOrder($id)
    {
        $lockTime = Yii::$app->request->get('lock_time') ?? 3600;

        $order = $this->findOrder($id);
        $currentTime = (new \DateTime())
            ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
            ->getTimestamp();
        if ($order->blocked_to > $currentTime) {
            $blockedTo = $order->blocked_to + $lockTime;
        } else {
            $blockedTo = $currentTime + $lockTime;
        }

        Order::updateByParams(['blocked_to' => $blockedTo], ['id' => $order->id]);

        return [
            'status' => 'success',
            'blocked_to' => $blockedTo,
        ];
    }

    /**
     * Разблокировка заказа
     * @param $id
     * @return array
     */
    public function actionUnlockOrder($id)
    {
        $lockTime = Yii::$app->request->get('lock_time') ?? 3600;
        $order = $this->findOrder($id);
        $blockedTo = $order->blocked_to - $lockTime;

        $currentTime = (new \DateTime())
            ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
            ->getTimestamp();

        if ($blockedTo > $currentTime) {
            Order::updateByParams(['blocked_to' => $blockedTo], ['id' => $order->id]);
        }

        return [
            'status' => 'success',
            'blocked_to' => $blockedTo,
        ];
    }

    /**
     * Снимаем с паузы оператора
     */
    public function actionContinueOperatorWork()
    {
        Asterisk::setUserPaused(0);

        $user = User::findOne(['id' => Yii::$app->user->id]);

        if ($user) {
            $sipData = json_decode($user->sip, 1);
            IncomingCall::removeByUserSip($sipData['sip_login']);
        }

        return [
            'status' => 'success',
        ];
    }

    /**
     * Получение от партнера списка служб доставок
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionShippingListByBroker($id)
    {
        $order = Order::findOne($id);
        if (!$order) {
            throw new NotFoundHttpException(Yii::t('common', 'Заказ не найден.'));
        }
        $params = Yii::$app->request->post();
        $deliveries = $order->form->partner->getShippingListByBroker($order, $params);

        if ($deliveries) {
            return array_map(function ($item) use ($order) {
                $price = Yii::$app->formatter->asDecimal($item['price'] ?? $order->shipping_price, 2);
                $item['name'] = "{$item['name']} {$price}";
                return $item;
            }, $deliveries);
        }
        return [];
    }

    public function actionFastChangeQueue()
    {
        if (Yii::$app->request->isPost) {

            $order = Order::findOne(['id' => Yii::$app->request->post('order_id')]);

            if (is_null($order)) {
                return Json::encode([
                    'success' => false,
                ]);
            }

            $order->load(Yii::$app->request->post());
            $order->blocked_to = time();

            if (!$order->save()) {
                return Json::encode([
                    'success' => false,
                ]);
            } else {
                QueueOrder::deleteAll(['order_id' => $order->id]);
                Queue::publisher($order->lastQueue, $order->id);
                return Json::encode([
                    'success' => true,
                ]);
            }
        }

    }
}
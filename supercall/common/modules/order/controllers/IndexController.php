<?php

namespace common\modules\order\controllers;

use common\components\web\Controller;
use common\models\Product;
use common\models\QueueOrder;
use common\models\User;
use common\models\UserCountry;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderProductLog;
use common\modules\order\models\OrderType;
use common\modules\order\models\search\OrderSearch;
use common\modules\partner\models\Partner;
use backend\modules\order\models\OrderTask;
use backend\modules\order\controllers\OrderTaskController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use backend\modules\queue\models\Queue;

/**
 * Class IndexController
 * @package backend\modules\order\controllers
 */
class IndexController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $data = $this->beforeActionIndex();

        if (!$data['modelSearch']->countries) {
            $countries = ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id');
        } else {
            $countries = $data['modelSearch']->countries;
        }

        $queryOperators = UserCountry::getUsersByCountry($countries, true, [User::ROLE_OPERATOR, User::ROLE_SENIOR_OPERATOR]);

        $operators = ArrayHelper::map($queryOperators, 'id', 'username');

        return $this->render('index', [
            'modelSearch' => $data['modelSearch'],
            'contentVisible' => $data['contentVisible'],
            'showButtons' => $data['showButtons'],
            'dataProvider' => $data['dataProvider'],
            'statuses' => $data['statuses'],
            'partners' => $data['partners'],
            'types' => $data['types'],
            'products' => $data['products'],
            'countries' => $data['countries'],
            'visiblePartner' => true,
            'visiblePartnerNumber' => true,
            'queues' => ArrayHelper::map(Queue::find()->all(), 'id', 'name'),
            'operators' => $operators
        ]);
    }

    /**
     * Для фильтра
     * @return string
     */
    public function actionGetOperatorsByCountry()
    {
        $countries = yii::$app->request->post('country_id');

        $user_country_ids = ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id');

        $country_id = is_null($countries) || empty($countries) ? $user_country_ids : $countries;

        $listOperators = UserCountry::getUsersByCountry($country_id, true, [User::ROLE_OPERATOR, User::ROLE_SENIOR_OPERATOR]);

        if ($listOperators) {
            $result = [
                'success' => true,
                'data' => ArrayHelper::map($listOperators, 'id', 'username'),
                'message' => yii::t('common', 'Список операторов обновлен')
            ];
        } else {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Ошибка обновления списка операторов')
            ];
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function actionGetCityByCountry()
    {
        //$country_id = yii::$app->re
    }

    /**
     * Здесь работает вся логика, готовятся данные перед передачей во view
     *
     * Данная функция необходима для работы API и для Backend
     * @return array
     */
    public function beforeActionIndex()
    {
        $countries = ArrayHelper::map(yii::$app->user->getCountries(), 'id', function ($val) {
            return yii::t('common', $val->name);
        });

        $products = ArrayHelper::map(Product::findAll(['active' => true]), 'id', 'name');

        foreach ($products as $key => $item) {
            $products[$key] = Yii::t('common', $item);
        }

        $modelSearch = new OrderSearch();
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);
        $dataProvider = $modelSearch->search($query);

        return [
            'modelSearch' => $modelSearch,
            'contentVisible' => Yii::$app->session->get($filterSessId . '.isVisible', true),
            'showButtons' => true,
            'filterSessId' => $filterSessId,
            'dataProvider' => $dataProvider,
            'statuses' => Order::getStatusesCollection(),
            'partners' => Partner::find()->collection(),
//            'types'                => ArrayHelper::map( OrderType::find()->all(), 'id', 'name' ),
            'types' => OrderType::find()->collection('common'),
            'products' => $products,
            'countries' => $countries,
        ];
    }

    /**
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $modelProducts = $this->findProducts($model->id);

        $OrderTaskController = new OrderTaskController('order-task', yii::$app->getModule('order'));

        $lastOrderTask = $OrderTaskController->getLastTask($id, array_keys(OrderTask::getTaskStatuses()));

        return $this->render('view', [
            'model' => $model,
            'invalid_address_orders' => Order::find()
                ->leftJoin(CallHistory::tableName(), CallHistory::tableName() . '.order_id=' . Order::tableName() . '.id')
                ->where(['foreign_id' => $model->foreign_id])
                ->andWhere(['<>', Order::tableName() . '.id', $model->id])
                ->andWhere(['is not', 'foreign_id', null])
                ->andWhere(['<>', 'recordingfile', ''])
                ->andWhere(['disposition' => CallHistory::DISPOSITION_ANSWERED])
                ->all(),
            'modelProducts' => $modelProducts,
            'statuses' => Order::getStatusesCollection('common'),
            'partners' => Partner::find()->collection('common'),
            'products' => Product::find()->collection('common'),
            'lastOrderTask' => is_null($lastOrderTask) ? false : [$lastOrderTask],
            'records' => []//CallHistory::getCallHistoryByOrder($model->id), //Records::getLinkByPhones($phones),
        ]);
    }


    /**
     * @param integer $id
     * @return string
     */
    public function actionLogs($id)
    {
        $model = $this->findModel($id);

        $logsCommon = OrderLog::find()
            ->where(['order_id' => $model->id])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        $logsProductQuery = OrderProductLog::find()
            ->joinWith('product')
            ->where(['order_id' => $model->id])
            ->orderBy(['id' => SORT_DESC]);

        $logsProduct = new ActiveDataProvider(['query' => $logsProductQuery]);

        $logsStatusQuery = OrderLog::find()
            ->where(['order_id' => $model->id])
            ->andWhere(['field' => 'status'])
            ->orderBy(['id' => SORT_DESC]);

        $logsStatus = new ActiveDataProvider(['query' => $logsStatusQuery]);

        return $this->render('logs', [
            'model' => $model,
            'logsCommon' => $logsCommon,
            'logsProduct' => $logsProduct,
            'logsStatus' => $logsStatus,
        ]);
    }

    /**
     * @param integer $id
     * @return OrderProduct[]
     */
    protected function findProducts($id)
    {
        return OrderProduct::find()->byOrderId($id)->all();
    }

    /**
     * @param $id
     * @return Order
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if ($model = Order::find()->joinWith(['country', 'partner'])->byId($id)->one()) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Запись не найдена.'));
        }
    }

    /**
     * Функция показывает проблемные заказы, апрув которых не подтвердился по запросу Статуса доставки
     * у которых status=4 (approve) и approve_components={code=5}
     * Т.е. апрувы, которые имеют ошибку в статусе заказа (code=5)
     *
     * @return string
     */
    public function actionWrong()
    {
        $query = Order::getWrongOrders();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query,
        ]);

        return $this->render('wrong', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRecall()
    {
        $query = Order::getRecallOrders();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query,
        ]);

        return $this->render('recall', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCheckOrder()
    {
        $order_id = yii::$app->request->post('order_id');
        $user_id = yii::$app->request->post('user_id');

        $build = QueueOrder::find()
            ->addSelect(['username' => User::tableName().'.username'])
            ->where(['order_id' => $order_id])
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . QueueOrder::tableName() . '.user_id')
            ->andWhere(['<>', 'user_id', $user_id]);

        $check = yii::$app->db->createCommand($build->createCommand()->getRawSql())->queryOne();

        if (!$check) {
            try {
                QueueOrder::clearBlocks($user_id);

                $block = QueueOrder::setBlockOrder($user_id, $order_id);

                if ($block['success']) {
                    $result = [
                        'success' => true,
                        'message' => $order_id
                    ];
                } else {
                    $result = $block;
                }
            } catch (\Throwable $e) {
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка очистки списка блокированных заказов')
                ];
            }
        } else {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'С заказом #{number}, в данный момент, работает другой пользователь "{username}"', [
                    'number' => $order_id,
                    'username' => $check['username']
                ])
            ];
        }

        return Json::encode($result);
    }

}

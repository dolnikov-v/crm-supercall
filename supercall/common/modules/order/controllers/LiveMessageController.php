<?php

namespace common\modules\order\controllers;

use backend\modules\administration\models\BaseLogger;
use common\components\ChatApiCom;
use common\components\Platinum;
use common\components\web\Controller;
use common\models\LiveMessage;
use common\models\QueueOrder;
use common\models\UserReady;
use common\modules\order\models\Order;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * Class LiveMessageController
 * @package common\modules\order\controllers
 */
class LiveMessageController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'read-messages' => ['post'],
                    'add-operator-message' => ['post']
                ],
            ],
        ];
    }

    /**
     * Чтение чообщений
     * @return string
     */
    public function actionReadMessages()
    {
        $order_id = yii::$app->request->post('order_id');
        $messages = LiveMessage::findAll(['order_id' => $order_id]);
        $countErrors = 0;

        foreach ($messages as $message) {
            $message->is_new = LiveMessage::IS_NOT_NEW;

            if (!$message->save(false, ['is_new'])) {
                $countErrors++;
            }
        }

        return json_encode([
            'countError' => $countErrors
        ], 256);
    }

    /**
     * @return string
     */
    public function actionCheckOrderInWork()
    {
        $result = [
            'success' => true,
            'message' => null
        ];

        if(!yii::$app->request->post('user_id')){
            $result = [
                'success' => false,
                'message' => yii::t('common', 'User not transferred')
            ];
        }else{
            /** @var QueueOrder $orderInWork */
            $orderInWork = QueueOrder::findOne(['user_id' => yii::$app->request->post('user_id')]);

            if($orderInWork){
                $result['message'] = $orderInWork->order_id;
            }
        }


        return Json::encode($result);
    }

    /**
     * @return string
     */
    public function actionAddOperatorMessage()
    {
        $post = yii::$app->request->post();

        /** @var BaseLogger $logger */
        $logger = yii::$app->get('logger');
        $logger->action = __METHOD__;
        $logger->type = LiveMessage::LOGGER_TYPE;
        $logger->route = $this->route;

        $liveMessage = LiveMessage::find()->where(['order_id' => $post['order_id']])->andWhere(['is not', 'phone', null])->one();

        if ($liveMessage) {
            $model = new LiveMessage();
            $model->type = $post['type'];
            $model->order_id = $post['order_id'];
            $model->user_id = $post['user_id'];
            $model->is_new = LiveMessage::IS_NEW;
            $model->content = $post['content'];
            $model->created_at = time();
            $model->updated_at = time();

            if ($model->save()) {
                $model->created_at = yii::$app->formatter->asDate($model->created_at, 'php:d/m/Y H:i:s');

                $result = [
                    'success' => true,
                    'message' => array_merge($model->getAttributes(), ['sender' => is_null($model->user_id) ? $model->order->customer_full_name : $model->user->username])
                ];

                $chatApiCom = yii::$app->get('ChatApiCom');
                $response = $chatApiCom->sendToChat($liveMessage->phone, $post['content'], $liveMessage->order);

                $decoded = Json::decode($response);



                if(isset($decoded['error'])){
                    $result = [
                        'success' => false,
                        'message' => $decoded['error']
                    ];
                }else {
                    if (isset($decoded['sent']) && $decoded['sent']) {
                        $model->chat_id = $decoded['id'];
                        $model->is_new = LiveMessage::IS_NOT_NEW;
                        $model->queue_number = $decoded['queueNumber'];
                        $model->save(false);
                    } else {
                        $result = [
                            'success' => false,
                            'message' => $decoded['content']
                        ];
                    }
                }

                $result['response'] = $response;

            } else {
                $result = [
                    'success' => false,
                    'message' => $model->getErrors()
                ];

                $logger->tags = $model->getAttributes();
                $logger->error = json_encode($model->getErrors(), 256);
                $logger->save();
            }
        } else {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Oшибка определения номера телефона получателя')
            ];
        }

        return json_encode($result, 256);
    }
}
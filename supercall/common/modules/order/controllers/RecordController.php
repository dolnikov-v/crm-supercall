<?php
namespace common\modules\order\controllers;


use common\components\web\Controller;
use common\modules\order\models\asterisk\Records;
use common\modules\order\models\Order;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii;

/**
 * Class RecordController
 * @package common\modules\order\controllers
 */
class RecordController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'start-load'  => ['post'],
                ],
            ],
        ];
    }

    /**
     * Функция предоставит для прослушивания файл записи разговора
     *
     * @param $id
     * @return bool|string
     */
    public function actionFile($id)
    {
        $path = Records::find()->select(['path'])->where(['id' => $id])->scalar();
        $record_path = \Yii::getAlias('@app') . '/../../records/';

        $fileArray = explode('/', $path);
        // Сам файл
        $file = $fileArray[2];

        if (file_exists($record_path . $path)) {
            $response = \Yii::$app->response;
            $response->getHeaders()
                ->setDefault('Content-Description', 'File Transfer')
                ->setDefault('Content-Transfer-Encoding', 'binary')
                ->setDefault('Content-Type', 'audio/wav, audio/x-wav')
                ->setDefault('Content-length', filesize($record_path . $path))
                ->setDefault('Content-Disposition', 'attachment;filename='.$path)
                ->setDefault('X-Accel-Redirect', '/records/' . $path);
//
            $response->format = Response::FORMAT_RAW;
            return $response;

//            header('Content-Description: File Transfer');
//            header("Content-Transfer-Encoding: binary");
//            header('Content-Type: audio/wav, audio/x-wav, audio/wave, audio/x-pn-wav, audio/vnd.wave, application/x-wav');
//            header('Content-length: ' . filesize($record_path . $path));
//            header('Content-Disposition: attachment;filename="' . $path . '"');
//            header("X-Accel-Redirect: /records/" . $path);
        } else {
            header("HTTP/1.0 404 Not Found");
        }
    }


    /**
     * Активирует консольную команду для скачивания записи для данного заказа
     * с сервера астериска
     */
    public function actionStartLoad()
    {
        $orderId = \Yii::$app->request->post('orderId');
        $order = Order::find()->byId($orderId)->one();

        $phones = $order->getAllPhones();

        $recordQuery = Records::find()
            ->asArray()
            ->select(['id' ,'path'])
            ->where(['in', 'phone', $phones])
            ->andWhere(['saved' => false]);

        $records = $recordQuery->exists() ? $recordQuery->all() : [];

        $response = \Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'result' => 'ok',
            'data' => $records
        ];

        return $response;
    }


    public function actionLoadFile()
    {
        $path = \Yii::$app->request->post('path');
        $id = (int) \Yii::$app->request->post('id');

        $result = 'error';
        $data = [];
        if (Records::loading($path)) {
            $result = 'ok';
            $data = [
                'url' => Records::getUrlFile($id),
                'id' => $id,
                'path' => $path,
            ];
        }

        $response = \Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'result' => $result,
            'data' => $data,
        ];

        return $response;


//        $consolePath = \Yii::getAlias('@console');
//        foreach ($pathes as $id) {
//            exec("php yii record/load $id >> $consolePath/runtime/logs/records.log 2>&1 &");
//        }



    }

}

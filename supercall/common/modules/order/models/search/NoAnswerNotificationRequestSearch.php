<?php

namespace common\modules\order\models\search;


use common\models\Country;
use common\modules\order\models\NoAnswerNotificationRequest;
use yii\data\ActiveDataProvider;

class NoAnswerNotificationRequestSearch extends NoAnswerNotificationRequest
{
    public $country_id;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'country_id'], 'safe'],
            ['status', 'in', 'range' => array_keys(self::statusLabels())],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = NoAnswerNotificationRequest::find()->joinWith(['order.country']);
        $query->orderBy([NoAnswerNotificationRequest::tableName() . '.created_at' => SORT_DESC]);

        if ($this->country_id) {
            $query->andFilterWhere([Country::tableName() . '.id' => $this->country_id]);
        }
        if ($this->order_id) {
            $query->andFilterWhere([NoAnswerNotificationRequest::tableName() . '.order_id' => explode(',', str_replace(' ', '', $this->order_id))]);
        }
        if ($this->status) {
            $query->andFilterWhere([NoAnswerNotificationRequest::tableName() . '.status' => $this->status]);
        }

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }
}
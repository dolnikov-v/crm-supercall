<?php
namespace common\modules\order\models\search;

use common\modules\order\models\OrderType;
use yii\data\ActiveDataProvider;

/**
 * Class OrderTypeSearch
 * @package common\modules\order\models\search
 */
class OrderTypeSearch extends OrderType
{
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = OrderType::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->name) {
            $query->where(['like', 'name', '%'.$this->name.'%']);
        }

        return $dataProvider;
    }
}

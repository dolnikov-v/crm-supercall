<?php

namespace common\modules\order\models\search;

use backend\modules\order\models\OrderExport;
use common\models\UserPartner;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerFormAttribute;
use common\widgets\base\Link;
use common\widgets\base\LinkPager;
use Yii;
use yii\base\Object;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use common\components\validator\ArrayValueIntegerValidator;

/**
 * Class OrderSearch
 * @package common\modules\order\models\search
 */
class OrderSearch extends Order
{
    /** @var string */

    public $product;
    public $country;
    public $createdRange;
    public $updatedRange;
    public $customerPhone;
    public $customerMobile;
    public $partnerName;
    public $type_id;
    public $date_range;
    public $type_date;
    public $countries; // Параметр для фильтра по нескольким странам
    // Если выбраны все страны, по умолчанию всегда выбраны все доступные
    public $is_all_countries = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'product',
                    'partner_id',
                    'type_id',
                    'partnerName',
                    'status',
                    'last_queue_id',
                    'id',
                    'foreign_id',
                    'customer_full_name',
                    'createdRange',
                    'updatedRange',
                    'customerPhone',
                    'customerMobile',
                    'country',
                    'phone_error',
                    'countries',
                    'type_date',
                    'date_range',
                    'with_timezone',
                    'operator',
                    'date_first_call',
                    'time_getting_order_crm',
                    'address_type',
                    'address_value',
                    'original_id',
                    'status_queue_strategy',
                ], 'safe'],
            ['is_all_countries', 'boolean'],
            [['id', 'foreign_id'], ArrayValueIntegerValidator::className()],
        ];
    }

    /**
     * @return array
     */
    public function exportFields()
    {
        return OrderExport::getDefaultExportList();
    }

    /**
     * @param $order_ids
     * @return ActiveDataProvider
     */
    public function SearchById($order_ids)
    {
        $order_ids = is_array($order_ids) ? $order_ids : [$order_ids];

        $query = Order::find()
            ->where(['id' => $order_ids]);

        $config = [
            'query' => $query,
        ];

        return new ActiveDataProvider($config);
    }

    /**
     * @param array $params
     * @return ActiveDataProvider|ArrayDataProvider
     */
    public function search($params = [], $useSort = true)
    {
        if (empty($params)) {
            return new ActiveDataProvider([
                'query' => Order::find()->where(['id' => null])
            ]);
        }

        $sort = [];

        if (empty($params['sort'])) {
            $sort[Order::tableName() . '.created_at'] = SORT_DESC;
        }

        //ищем по истории
        $byOperator = isset($params['OrderSearch']) && isset($params['OrderSearch']['operator']) && is_numeric($params['OrderSearch']['operator']);


        //актуальные статусы по ордерам
        $query = Order::find()
            ->distinct()
            ->joinWith('orderProducts')
            ->joinWith('partner')
            ->joinWith('queue')
            ->joinWith(['queueOrder'])
            // ->byCountryId(Yii::$app->user->country->id)
            ->orderBy($sort);

        // для обычного пользователя "включаем" фильтр доступных Партнеров, если они указаны
        if (!Yii::$app->user->isSuperadmin) {
            $userPartners = UserPartner::find()->select(['partner_id'])->where(['user_id' => Yii::$app->user->id]);

            if ($userPartners->exists()) {
                $partnerIds = ArrayHelper::getColumn($userPartners->asArray()->all(), 'partner_id');
                $query->byPartnerIds($partnerIds);
            }
        }

        $config = [
            'query' => $query,
            'pagination' => LinkPager::getPaginationSizer()
        ];

        $dataProvider = new ActiveDataProvider($config);

        if ($useSort) {
            // добавим сортировку полей
            $dataProvider->setSort([
                'attributes' => [
                    'partnerName' => [
                        'asc' => ['partner.name' => SORT_ASC],
                        'desc' => ['partner.name' => SORT_DESC],
                        'label' => 'partnerName',
                        'default' => SORT_ASC,
                    ],
                    'foreign_id',
                    'status',
                    'customer_full_name' => [
                        'asc' => ['customer_full_name' => SORT_ASC],
                        'desc' => ['customer_full_name' => SORT_DESC],
                        'label' => 'customer_full_name',
                        'default' => SORT_ASC,
                    ],
                    'customer_phone',
                    Order::tableName() . '.created_at',
                    'updated_at',
                    'ordered_at'
                ],
                'defaultOrder' => [
                    Order::tableName() . '.created_at' => SORT_DESC,
                ],
            ]);
        }

        $this->load($params);

        // В случае ошибки показывать уведомление
        if (!$this->validate()) {
            Yii::$app->notifier->addNotifierErrorByModel($this);
            $dataProvider = new ArrayDataProvider([
                'allModels' => [],
                'pagination' => LinkPager::getPaginationSizer()
            ]);

            return $dataProvider;
        }

        //если подключили оператора, либо отмечен "статус выставлен стратегией очереди" - то ищем в order_log
        if ($byOperator || $this->status_queue_strategy) {
            $queryLog = OrderLog::find()
                ->select([OrderLog::tableName() . '.order_id'])
                ->leftJoin(Order::tableName(), Order::tableName() . '.id=' . OrderLog::tableName() . '.order_id')
                ->groupBy(OrderLog::tableName() . '.order_id');

            if ($this->id) {
                $primary_id = array_map('trim', explode(',', $this->id));

                foreach ($primary_id as $key=>$value){
                    $primary_id[$key] = (int)$value;
                }

                $queryLog->andWhere([Order::tableName() . '.id' => $primary_id]);
            }

            if ($this->foreign_id) {
                $foreign_id = array_map('trim', explode(',', $this->foreign_id));

                foreach ($foreign_id as $key=>$value){
                    $foreign_id[$key] = (int)$value;
                }

                $queryLog->andWhere([Order::tableName() . '.foreign_id' => $foreign_id]);
            }

            if ($this->country) {
                $queryLog->andWhere([Order::tableName() . '.country_id' => $this->country]);
            } else {
                if (!Yii::$app->user->isGuest) {
                    $queryLog->andWhere(['in', Order::tableName() . '.country_id', $this->getCountries()]);
                }
            }

            if ($this->customerPhone) {
                $queryLog->andWhere(['like', Order::tableName() . '.customer_phone', $this->customerPhone]);
            }

            if ($this->customerMobile) {
                $queryLog->andWhere(['like', Order::tableName() . '.customer_mobile', $this->customerMobile]);
            }

            if ($this->type_date != self::IGNORE_DATE_RANGE) {
                if ($this->date_range) {
                    /** @var Object $dateCreatedRange */
                    $dateCreatedRange = Yii::$app->formatter->asConvertToFrom($this->date_range);

                    if ($this->with_timezone) {
                        $from = $dateCreatedRange->from->getTimestamp();
                        $to = $dateCreatedRange->to->getTimestamp();
                    } else {
                        $dateCreatedRange->from = is_object($dateCreatedRange->from) ? $dateCreatedRange->from->format('Y-m-d H:i:00') : $dateCreatedRange->from;
                        $dateCreatedRange->to = is_object($dateCreatedRange->to) ? $dateCreatedRange->to->format('Y-m-d H:i:59') : $dateCreatedRange->to;

                        if ($this->with_timezone) {
                            $date_from = new \DateTime($dateCreatedRange->from, new \DateTimeZone(yii::$app->user->timezone->timezone_id));
                            $from = $date_from->getTimestamp();

                            $date_to = new \DateTime($dateCreatedRange->to, new \DateTimeZone(yii::$app->user->timezone->timezone_id));
                            $to = $date_to->getTimestamp();
                        } else {
                            $date_from = new \DateTime($dateCreatedRange->from);
                            $from = $date_from->getTimestamp();

                            $date_to = new \DateTime($dateCreatedRange->to);
                            $to = $date_to->getTimestamp();
                        }
                    }
                    if ($this->type_date == self::UPDATED_DATA_RANGE) {
                        $queryLog->andWhere(['between', OrderLog::tableName() . '.' . self::DATE_TYPE_UPDATED_AT, $from, $to]);
                    } else {
                        if ($this->type_date != self::DATE_TYPE_SHIPPING_DATE) {
                            $queryLog->andWhere(['between', Order::tableName() . '.' . $this->type_date, $from, $to]);
                        } else {
                            $queryLog->andWhere(['>=', Order::tableName() . '.delivery_from', $from]);
                            $queryLog->andWhere(['<=', Order::tableName() . '.delivery_to', $to]);
                        }
                    }
                }
            }

            if ($this->customer_full_name) {
                $queryLog->andWhere(['like', Order::tableName() . '.customer_full_name', $this->customer_full_name]);
            }

            if ($this->partner_id) {
                $queryLog->andWhere([Order::tableName() . '.partner_id' => $this->partner_id]);
            }

            if ($this->type_id) {
                $queryLog->andWhere([Order::tableName() . '.type_id' => $this->type_id]);
            }
            //выводим все заказы, всех типов 26/02/2018
            /*else{
                $queryLog->andWhere([Order::tableName() . '.type_id' => [OrderType::TYPE_NEW, OrderType::TYPE_CREATED]]);
            }*/

            if ($this->last_queue_id) {
                $queryLog->andWhere([Order::tableName() . '.last_queue_id' => $this->last_queue_id]);
            }

            if ($this->product) {
                $queryLog->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id=' . Order::tableName() . '.id');
                $queryLog->andWhere(['in', OrderProduct::tableName() . '.product_id', $this->product]);
            }

            if ($this->operator && !$this->status_queue_strategy) {
                if ($this->status) {
                    //по дате обновления - история по ордер логам
                    if ($this->type_date == self::UPDATED_DATA_RANGE) {
                        $queryLog->andWhere([OrderLog::tableName() . '.field' => 'status', OrderLog::tableName() . '.new' => $this->status]);
                    } else {
                        //иначе - актуальный статус
                        $query->andWhere([Order::tableName() . '.status' => $this->status]);
                    }
                }

                $queryLog->andWhere([OrderLog::tableName() . '.user_id' => $this->operator]);
            }

            if ($this->status_queue_strategy && !$this->operator) {
                if ($this->status) {
                    $queryLog->andWhere([OrderLog::tableName() . '.field' => 'status', OrderLog::tableName() . '.new' => $this->status]);
                    $queryLog->andWhere([Order::tableName() . '.status' => $this->status]);
                } else {
                    $queryLog->andWhere([OrderLog::tableName() . '.field' => 'status']);
                }

                $queryLog->andWhere([OrderLog::tableName() . '.user_id' => null]);
                $queryLog->andWhere([OrderLog::tableName() . '.operator' => null]);
                $queryLog->limit(20);
            }

            $ids = ArrayHelper::getColumn($queryLog->all(), 'order_id');

            $query->andWhere(['in', Order::tableName() . '.id', $ids]);

        } else {
            // ID заказа
            if ($this->id) {
                $query->byId($this->id);
            }

            if ($this->foreign_id) {
                $query->byForeignId($this->foreign_id);
            }

            // в частности для API, н-р /v1/order/iframe-list
            if ($this->country) {
                $query->byCountryId($this->country);
            } else {
                if (!Yii::$app->user->isGuest)
                    // Если фильтруется по стране
                    $query->byCountryIds($this->getCountries());
            }

            // customer_phone
            if ($this->customerPhone) {
                $query->andWhere(['like', Order::tableName() . '.customer_phone', $this->customerPhone]);
            }
            // customer_mobile
            if ($this->customerMobile) {
                $query->andWhere(['like', Order::tableName() . '.customer_mobile', $this->customerMobile]);
            }

            if ($this->customer_full_name) {
                $query->andWhere(['like', Order::tableName() . '.customer_full_name', $this->customer_full_name]);
            }
            // учитывать, то содержит время, в формате '23/06/2017 00:30 - 23/06/2017 14:30'
            if ($this->type_date != self::IGNORE_DATE_RANGE) {
                if ($this->date_range) {
                    /** @var Object $dateCreatedRange */
                    $dateCreatedRange = Yii::$app->formatter->asConvertToFrom($this->date_range);

                    if ($this->with_timezone) {
                        $from = $dateCreatedRange->from->getTimestamp();
                        $to = $dateCreatedRange->to->getTimestamp();
                    } else {
                        $dateCreatedRange->from = is_object($dateCreatedRange->from) ? $dateCreatedRange->from->format('Y-m-d H:i:00') : $dateCreatedRange->from;
                        $dateCreatedRange->to = is_object($dateCreatedRange->to) ? $dateCreatedRange->to->format('Y-m-d H:i:59') : $dateCreatedRange->to;


                        if ($this->with_timezone) {
                            $date_from = new \DateTime($dateCreatedRange->from, new \DateTimeZone(yii::$app->user->timezone->timezone_id));
                            $from = $date_from->getTimestamp();

                            $date_to = new \DateTime($dateCreatedRange->to, new \DateTimeZone(yii::$app->user->timezone->timezone_id));
                            $to = $date_to->getTimestamp();
                        } else {
                            $date_from = new \DateTime($dateCreatedRange->from);
                            $from = $date_from->getTimestamp();

                            $date_to = new \DateTime($dateCreatedRange->to);
                            $to = $date_to->getTimestamp();
                        }
                    }

                    if ($this->type_date != self::DATE_TYPE_SHIPPING_DATE) {
                        $query->andWhere(['between', Order::tableName() . '.' . $this->type_date, $from, $to]);
                    } else {
                        $query->andWhere(['>=', Order::tableName() . '.delivery_from', $from]);
                        $query->andWhere(['<=', Order::tableName() . '.delivery_to', $to]);
                    }
                }
            }

            if ($this->partner_id) {
                $query->byPartnerId($this->partner_id);
            }

            if ($this->type_id) {
                $query->byTypeId($this->type_id);
            }

            if ($this->status) {
                $query->byStatus($this->status);
            }

            if ($this->last_queue_id) {
                $query->byLastQueueId($this->last_queue_id);
            }

            if ($this->product) {
                $query->andWhere(['in', OrderProduct::tableName() . '.product_id', $this->product]);
            }
        }

        $query->andWhere([Order::tableName() . '.partner_id' => ArrayHelper::getColumn(Partner::find()->byIdentityUser()->active()->all(), 'id')]);

        if($this->original_id){
            $query->andWhere(['is not', 'original_id', null]);
        }

        if ($this->address_type && $this->address_value) {
            $formAttribute = PartnerFormAttribute::findOne(['id' => $this->address_type]);

            $valueWhere = [
                'or'
            ];

            if ($formAttribute) {
                foreach ($this->address_value as $num => $value) {
                    $valueWhere[] = new Expression("lower(trim(customer_components::jsonb->'address'->>:type" . $num . ")) = :value" . $num, [
                        ':type' . $num => $formAttribute->name,
                        ':value' . $num => $value
                    ]);
                }
            }

            if (count($valueWhere) > 1) {
                $query->andWhere($valueWhere);
            }
        }

        return $dataProvider;
    }

    /**
     * Функция вернет массив стран для фильтра списка заказов
     * По умолчанию фильтр пустой, потому будут возвращены все доступные Пользователю страны.
     * @return array
     */
    public function getCountries()
    {
        if (!$this->countries) {
            $this->countries = ArrayHelper::getColumn(\Yii::$app->getUser()->countries, 'id');
            $this->is_all_countries = true;
        } else {
            $this->is_all_countries = false;
        }

        return $this->countries;
    }

}

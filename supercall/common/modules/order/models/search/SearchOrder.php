<?php

namespace common\modules\order\models\search;

use common\modules\order\models\Order;
use yii\base\Model;
use yii\data\ArrayDataProvider;

/**
 * Class SearchOrder
 * @package common\modules\order\models\search
 */
class SearchOrder extends Model
{
    public $orderId;

    public $phone;

    public $customerFullName;

    public $country_id;

    public $type_id;

    public $foreign_id;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['orderId','$foreign_id', 'type_id'], 'integer'],
            ['country_id','each', 'rule' => ['integer']],
            [['phone', 'customerFullName'], 'string'],
        ];
    }

    /**
     * @param $params
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        $models = [];
        $this->load($params);
        $haveAttributeValues = false;
        foreach ($this->attributes as $attribute) {
            if ($attribute) {
                $haveAttributeValues = true;
            }
        }
        if ($haveAttributeValues) {

            $query = Order::find();

            $query->orderBy([Order::tableName() . '.id' => SORT_DESC]);
            $query->limit(25);

            if($this->country_id){
                $query->andWhere([Order::tableName() . '.country_id' => $this->country_id]);
            }

            if($this->type_id){
                $query->andWhere([Order::tableName() . '.type_id' => $this->type_id]);
            }

            if ($this->orderId) {
                $query->andWhere([Order::tableName() . '.id' => $this->orderId]);
            }

            if($this->foreign_id) {
                $query->andWhere([Order::tableName() . '.foreign_id' => $this->foreign_id]);
            }

            if ($this->phone) {
                $query->andWhere([
                    'or',
                    ['like', Order::tableName() . '.customer_phone', $this->phone],
                    ['like', Order::tableName() . '.customer_mobile', $this->phone],
                ]);
            }

            if ($this->customerFullName) {
                $query->andWhere(['like', Order::tableName() . '.customer_full_name', $this->customerFullName]);
            }

            $models = $query->all();
        }

        return new ArrayDataProvider([
            'models' => $models
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'orderId' => \Yii::t('common', 'Номер заказа'),
            'phone' => \Yii::t('common', 'Телефон'),
            'customerFullName' => \Yii::t('common', 'Полное имя заказчика'),
        ];
    }
}
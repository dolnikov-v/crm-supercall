<?php
namespace common\modules\order\models\search;

use common\modules\order\models\OrderSubStatus;
use yii\data\ActiveDataProvider;

/**
 * Class OrderSubStatusSearch
 * @package common\modules\order\models\search
 */
class OrderSubStatusSearch extends OrderSubStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['group_id', 'integer']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = OrderSubStatus::find()->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([OrderSubStatus::tableName() .'.group_id' => $this->group_id]);

        return $dataProvider;
    }
}

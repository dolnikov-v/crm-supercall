<?php

namespace common\modules\order\models\search;

use common\modules\order\models\OrderLog;
use yii\data\ActiveDataProvider;

/**
 * Class OrderLogSearch
 * @package app\modules\api\models\search
 */
class OrderLogSearch extends OrderLog
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'group_id', 'created_at', 'updated_at'], 'integer'],
            [['field', 'old', 'new'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // @todo Сменить на OrderLogs
        $query = OrderLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

<?php

namespace common\modules\order\models;

use api\models\ApiLog;
use backend\modules\administration\models\BaseLogger;
use backend\modules\administration\models\JsLogger;
use backend\modules\audit\models\AggregateWord;
use backend\modules\catalog\models\KladerCountries;
use backend\modules\catalog\models\PriceShipping;
use backend\modules\operator\models\QuestionnaireOrder;
use backend\modules\order\models\OrderBackup;
use backend\modules\order\models\OrderExport;
use backend\modules\order\models\OrderTask;
use backend\modules\order\models\OrderUserBackup;
use backend\modules\presets\models\Presets;
use backend\modules\queue\models\Queue;
use backend\modules\queue\models\QueueLanding;
use backend\modules\stats\models\OrderProcessingTime;
use common\components\Amqp;
use common\components\base\FormModel;
use common\components\db\ActiveRecord;
use common\components\Platinum;
use common\models\Country;
use common\models\CountryPhoneCode;
use common\models\Language;
use common\models\LiveMessage;
use common\models\QueueOrder;
use common\models\Shipping;
use common\models\Timezone;
use common\models\User;
use common\modules\call\models\CallHistory;
use common\modules\order\controllers\LiveMessageController;
use common\modules\order\models\genesys\Call;
use common\modules\order\models\query\OrderQuery;
use common\modules\order\models\OrderSubStatus;
use common\modules\order\models\OrderSubStatusGroup;
use common\modules\order\models\query\OrderSubStatusQuery;
use common\modules\order\models\query\OrderSubStatusGroupQuery;
use common\modules\partner\helpers\PartnerProduct;
use common\modules\partner\models\PartnerProduct as PartnerProductModel;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use common\modules\order\models\CountOrdersOnPhone;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $type_id
 * @property integer $status
 * @property integer $sub_status
 * @property integer $partner_id
 * @property integer $foreign_id
 * @property string $customer_phone
 * @property string $customer_mobile
 * @property string $customer_address
 * @property string $customer_ip
 * @property string $customer_components
 * @property double $init_price
 * @property double $final_price
 * @property double $shipping_price
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $form_id
 * @property string $recall_date
 * @property string $customer_first_name
 * @property string $customer_last_name
 * @property string $customer_middle_name
 * @property integer $shipping_id
 * @property string $customer_email
 * @property integer $ordered_at
 * @property string $order_phones
 * @property integer $language_id
 * @property integer $last_queue_id
 * @property boolean $finished
 * @property string $approve_components
 * @property string $source_uri
 * @property boolean $phone_error
 * @property integer $blocked_to
 * @property integer $attempts
 * @property string $queue_type
 * @property string $order_comment
 * @property string $customer_full_name
 * @property boolean $sent_to_sqs
 * @property integer $delivery_from
 * @property integer $delivery_to
 * @property string $latitude
 * @property string $longitude
 * @property string $event
 * @property string $google_zip
 * @property integer $price_shipping_id
 * @property integer $partner_created_at
 * @property integer $confirm_address
 * @property integer $queue_counter
 *
 * @property CallHistory[] $callHistories
 * @property CountOrdersData[] $countOrdersDatas
 * @property CountOrdersOnPhone[] $countOrdersOnPhones
 * @property JsLogger[] $jsLoggers
 * @property NoAnswerNotificationRequest[] $noAnswerNotificationRequests
 * @property Country $country
 * @property I18nLanguage $language
 * @property OrderType $type
 * @property Partner $partner
 * @property PartnerForm $form
 * @property Queue $lastQueue
 * @property Shipping $shipping
 * @property OrderBackup[] $orderBackups
 * @property OrderGenesys[] $orderGenesys
 * @property OrderLog[] $orderLogs
 * @property OrderLogs[] $orderLogs0
 * @property OrderMessage[] $orderMessages
 * @property OrderProcessingTime[] $orderProcessingTimes
 * @property OrderProduct[] $orderProducts
 * @property OrderProduct[] $orderPromoProducts
 * @property OrderProduct[] $orderGiftProducts
 * @property OrderProductLog[] $orderProductLogs
 * @property OrderQualityControl[] $orderQualityControls
 * @property OrderTask[] $orderTasks
 * @property OrderUserBackup[] $orderUserBackups
 * @property QuestionnaireOrder[] $questionnaireOrders
 * @property integer $prevent_queue_id
 * @property AggregateWord[] $aggregateWords
 * @property string $partner_token
 * @property QueueOrder $queueOrder
 * @property boolean $check_zorra
 * @property integer $original_id
 * @property boolean $status_queue_strategy
 */
class Order extends ActiveRecord
{
    const COUNT_NOANSWER_FOR_START_MESSAGE = 2;

    const IGNORE_DATE_RANGE = 'ignore';
    const UPDATED_DATA_RANGE = 'updated_at';
    const CREATED_AT_IN_CRM_DATA_RANGE = 'created_at';
    const DATE_TYPE_UPDATED_AT = 'updated_at';
    const DATE_TYPE_PARTNER_CREATED_AT = 'partner_created_at';
    const DATE_TYPE_SHIPPING_DATE = 'shipping_date';
    const LOGGER_TYPE = 'order_create';
    /**
     * @var bool
     */
    public $useUtc = false;

    /** @var  integer */
    public $avg_time;

    /**
     * QualityControl
     * @var integer
     */
    public $operator_status;

    /**
     * QualityControl
     * @var integer
     */
    public $auditor_status;

    /** for saveWithOutLogs() */
    /**
     * @var bool
     */
    public $withOutLogs = false;
    /**
     * @var array
     */
    public $ignoreAttributes = [];
    /** end saveWithOutLogs() */

    public $operator;
    public $comment;
    public $comment_author;
    /** @var  string */
    public $type_date;

    /** @var integer for filter widget */
    public $queue_id;

    /** @var integer for filter widget */
    public $product_id;

    //for tasks
    public $is_task;
    public $init_price_task;
    public $final_price_task;
    public $shipping_price_task;

    public $country_char_code;
    /* @var array дополнительные телефоны из поля order_phones */
    public $phones = [];

    /* @var string ид группы изменений, при сохранении заказа */
    public $group;
    /* @var Transaction транзакция при сохранении заказа */
    protected $transaction;

    public $is_backup = false;

    public $date_first_call;
    public $date_first_call_on_crm;

    public $time_getting_order_crm;

    //Т.к. через апи при обновление заказа не проходит валидацию формы
    public $updateFromAPI = false;

    /** @var  integer */
    public $count_new;

    /** @var  integer */
    public $address_type;

    /** @var  string */
    public $address_value;

    /**
     * Для фильтра заказов
     * @var  boolean
     */
    public $with_timezone;

    /** @var  boolean */
    public $status_queue_strategy;

    /** @var  string */
    public $partner_token;

    /** @var  null|string  by CallHistotry */
    public $group_id = null;

    /** @var  User|null */
    public $blockedUser;

    public $count;

    const EVENT_AFTER_SAVE_COMPLETE = 'after_save_complete';

    const STATUS_NEW = 1; // Новый
    const STATUS_RECALL = 2; // Перезвон
    const STATUS_FAIL = 3; // Недозвон
    const STATUS_APPROVED = 4; // Одобрен
    const STATUS_REJECTED = 5; // Отклонен
    const STATUS_TRASH = 6; // Треш
    const STATUS_DOUBLE = 7; // Дубль
    const STATUS_SYSTEM_TRASH = 8; // Заказы которые трэшим руками чтобы они не портили статистику в случае чего

    // Группы субстатусов
    const SUB_STATUS_GROUP_BUYOUT = 1;
    const SUB_STATUS_GROUP_REJECT = 2;
    const SUB_STATUS_GROUP_TRASH = 3;
    const SUB_STATUS_GROUP_UNDELIVERED = 4;

    const SUB_STATUS_BUYOUT = 402;
    const SUB_STATUS_NO_BUYOUT = 403;
    const SUB_STATUS_REJECT_STILL_WAITING = 404;

    const SUB_STATUS_REJECT_REASON_EXPENSIVE = 501;
    const SUB_STATUS_REJECT_REASON_CHANGED_MIND = 502;
    const SUB_STATUS_REJECT_REASON_MEDICAL = 503;
    const SUB_STATUS_REJECT_REASON_NO_COMMENTS = 504;
    const SUB_STATUS_REJECT_REASON_AUTO = 505;
    const SUB_STATUS_REJECT_REASON_PRODUCT_UNKNOWN = 506;
    const SUB_STATUS_REJECT_REASON_ANOTHER_SHOP = 507;
    const SUB_STATUS_REJECT_REASON_BAD_FEEDBACK = 508;
    const SUB_STATUS_REJECT_REASON_PERSONAL_REASON = 509;
    const SUB_STATUS_REJECT_REASON_AFFORD = 510;
    const SUB_STATUS_ALREADY_RECEIVED = 511;
    const SUB_STATUS_REJECT_NO_LONGER_NEEDED = 512;
    const SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER = 513;
    const SUB_STATUS_REJECT_REASON_CONSULTING = 514;

    const SUB_STATUS_TRASH_REASON_WRONG_NUMBER = 601;
    const SUB_STATUS_TRASH_REASON_DOUBLE = 602;
    const SUB_STATUS_TRASH_REASON_UNAWARE = 603;

    //перенесён в реджекты
    const SUB_STATUS_TRASH_REASON_CONSULTING = 604;

    const SUB_STATUS_TRASH_REASON_COMPETITOR = 605;
    const SUB_STATUS_TRASH_REASON_RETURN = 606;
    const SUB_STATUS_TRASH_REASON_DENIED = 607;
    const SUB_STATUS_TRASH_REASON_INCORRECT_FORMAT = 608;
    const SUB_STATUS_TRASH_REASON_JOKE_ORDER = 609;
    const SUB_STATUS_TRASH_REASON_OTHER_REASON = 610;

    //Статусы для очереди "заказ не доставлен"
    const SUB_STATUS_CHECK_UNDELIVERED_DELIVERED = 2001;
    const SUB_STATUS_CHECK_UNDELIVERED_CLIENT_NOT_RESPOND = 2002;
    const SUB_STATUS_CHECK_UNDELIVERED_NUMBER_BUSY = 2003;
    const SUB_STATUS_CHECK_UNDELIVERED_NUMBER_NOT_SERVICED = 2004;
    const SUB_STATUS_CHECK_UNDELIVERED_AUTORESPONDER = 2005;
    const SUB_STATUS_CHECK_UNDELIVERED_NOT_ENOUGH_MONEY = 2006;
    const SUB_STATUS_CHECK_UNDELIVERED_CHANGED_MIND = 2007;
    const SUB_STATUS_CHECK_UNDELIVERED_NOT_ORDERING = 2008;
    const SUB_STATUS_CHECK_UNDELIVERED_BAD_REVIEWS = 2009;
    const SUB_STATUS_CHECK_UNDELIVERED_INCORRECT_ADDRESS = 2010;

    const APPROVE_STATUS_SCENARIO = 'approve_scenario';

    const MASKED_CUSTOMER_MOBILE = 'customer_mobile';
    const MASKED_CUSTOMER_PHONE = 'customer_phone';
    const MASKED_PHONES = 'phones';
    const MASK_PHONES = 'XXXX';
    const MASK_PATTERN = '#\d{4}$#';

    /** @var FormModel */
    protected $_extra_model;

    /** @var FormModel */
    protected $_address_model;

    /** @var  string */
    public $country_name;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order}}';
    }


    /**
     * @return array
     */
    public function attributes()
    {
        $attrs = parent::attributes();
        foreach (OrderExport::getDinamicFields() as $dinamicAttr) {
            $attrs[] = $dinamicAttr;
        }

        return $attrs;
    }

    public static function getMaskedInputs()
    {
        return [
            self::MASKED_CUSTOMER_MOBILE,
            self::MASKED_CUSTOMER_PHONE,
            self::MASKED_PHONES
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return OrderQuery
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    /**
     * @return int|string
     */
    public function getCountOrdersOnPhone()
    {
        return CountOrdersOnPhone::find()
            ->byPhones(($this->customer_phone) ? $this->customer_phone : $this->customer_mobile)
            ->count();
    }

    /**
     * @return CountOrdersData[]
     */
    public function getOrdersByPhone()
    {
        return CountOrdersData::find()->byOrderId($this->id)->all();
    }

    /**
     * @return \common\modules\order\models\CountOrdersOnPhone
     */
    public function updateCountOrdersOnPhone()
    {
        return CountOrdersOnPhone::updatePhoneCount(($this->customer_phone) ? $this->customer_phone : $this->customer_mobile, $this->id);
    }

    /**
     *  Списание товаров со склада продуктов партнёра
     * @params integer $order_id
     */
    public static function withdrawProductsOfStock($order_id)
    {
        $order = Order::find()->where(['id' => $order_id])->one();

        foreach ($order->orderProducts as $product) {
            $partner_product = PartnerProductModel::find()
                ->where(['product_id' => $product->product_id])
                ->andWhere(['country_id' => $order->country_id])
                ->andWhere(['partner_id' => $order->partner_id])
                ->one();

            $balance = ($partner_product->amount - $product->quantity) < 0 ? 0 : $partner_product->amount - $product->quantity;
            $partner_product->amount = $balance;
            $partner_product->save();
        }
    }

    /**
     * @param string $uid
     * @return Order|boolean
     */
    public static function findByUid($uid)
    {
        $parts = explode('-', $uid);

        if (count($parts) === 2) {
            $orderId = $parts[0];

            $order = Order::find()
                ->joinWith([
                    'country',
                ])
                ->byId($orderId)
                ->one();

            if ($order) {
                if ($order->generateIframeUid() === $uid) {
                    return $order;
                }
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public static function getCheckShippingSubStatusesCollection()
    {
        return [
            self::SUB_STATUS_ALREADY_RECEIVED => yii::t('common', 'Заказ доставлен'),
            self::SUB_STATUS_REJECT_STILL_WAITING => yii::t('common', 'Ожидает заказ'),
            self::SUB_STATUS_REJECT_NO_LONGER_NEEDED => yii::t('common', 'Клиент отказался'),
            self::SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER => yii::t('common', 'КС не связалась с клиентом'),
        ];
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Новый'),
            self::STATUS_RECALL => Yii::t('common', 'Перезвон'),
            self::STATUS_FAIL => Yii::t('common', 'Недозвон'),
            self::STATUS_APPROVED => Yii::t('common', 'Одобрен'),
            self::STATUS_REJECTED => Yii::t('common', 'Отклонен'),
            self::STATUS_TRASH => Yii::t('common', 'Треш'),
            self::STATUS_DOUBLE => Yii::t('common', 'Дубль'),
            self::STATUS_SYSTEM_TRASH => Yii::t('common', 'Системный треш'),
        ];
    }

    public static function getStatusByNumber($number)
    {
        $statuses = self::getStatusesCollection();

        return isset($statuses[$number]) ? $statuses[$number] : null;
    }

    public static function getFinalStatusesCollection()
    {
        return [
            self::STATUS_APPROVED => Yii::t('common', 'Одобрен'),
            self::STATUS_REJECTED => Yii::t('common', 'Отклонен'),
            self::STATUS_TRASH => Yii::t('common', 'Треш'),
            self::STATUS_DOUBLE => Yii::t('common', 'Дубль'),
            self::STATUS_SYSTEM_TRASH => Yii::t('common', 'Системный треш'),
        ];
    }

    public static function getFinalStatusesForQueueStrategyCollection()
    {
        return [
            self::STATUS_REJECTED => Yii::t('common', 'Отклонен'),
            self::STATUS_TRASH => Yii::t('common', 'Треш'),
        ];
    }

    public static function getFinalStatusesForCountryStrategyCollection()
    {
        return [
            self::STATUS_REJECTED => Yii::t('common', 'Отклонен'),
            self::STATUS_TRASH => Yii::t('common', 'Треш'),
            self::STATUS_SYSTEM_TRASH => Yii::t('common', 'Системный треш'),
        ];
    }

    public static function getQuestionnaireStatuses()
    {
        return [
            self::STATUS_APPROVED => Yii::t('common', 'Одобрен'),
            self::STATUS_REJECTED => Yii::t('common', 'Отклонен'),
            self::STATUS_TRASH => Yii::t('common', 'Треш'),
        ];
    }

    public static function getNotFinalStatuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Новый'),
            self::STATUS_RECALL => Yii::t('common', 'Перезвон'),
            self::STATUS_FAIL => Yii::t('common', 'Недозвон'),
        ];
    }

    /**
     * Возвращаем маппинг субстатусов
     * @param $groupId
     * @return array
     */
    public static function getMapSubStatuses($groupId = false)
    {

        if (!$groupId) return [];
        $statusesList = OrderSubStatus::find()->where([OrderSubStatus::tableName() . '.group_id' => $groupId]);

        //раньше 604 был в треше, сейчас он в реджекте и код 514
        $statusesList->andWhere(['<>', 'code', self::SUB_STATUS_TRASH_REASON_CONSULTING]);

        $mapSubStatuses = [];
        foreach ($statusesList->all() as $status) {
            if (!empty($status->id)) {
                $mapSubStatuses += [$status->code => yii::t('common', $status->name)];
            }
        }

        return $mapSubStatuses;
    }

    /**
     * @return array
     */
    public static function getRejectReasonCollection()
    {
        return self::getMapSubStatuses(Order::SUB_STATUS_GROUP_REJECT);
    }

    /**
     * @return array
     */
    public static function getTrashReasonCollection()
    {
        return self::getMapSubStatuses(Order::SUB_STATUS_GROUP_TRASH);
    }

    /**
     * @return array
     */
    public static function getSubStatusesCollection()
    {
        return ArrayHelper::map(OrderSubStatus::find()->all(), 'code', 'name');
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['country_id', 'status', 'partner_id', 'customer_phone', 'customer_full_name'],
                'required',
                'on' => self::APPROVE_STATUS_SCENARIO
            ],
            [
                [
                    'country_id',
                    'status',
                    'sub_status',
                    'foreign_id',
                    'shipping_id',
                    'price_shipping_id',
                    'last_queue_id',
                    'prevent_queue_id',
                    'attempts',
                    'original_id'
                ],
                'integer'
            ],
            [['init_price', 'final_price', 'shipping_price'], 'number'],
            // Разрешаем хранить дубли заказов
            // [['foreign_id'], 'unique', 'targetAttribute' => 'foreign_id'],
            [['created_at', 'updated_at', 'phones'], 'safe'],
            [['source_uri'], 'safe'],
            [['ordered_at'], 'integer', 'min' => '-2147483648', 'max' => 2147483647],
            ['comment', 'string', 'max' => 1000],
            [
                'type_id',
                'exist',
                'targetClass' => OrderType::class,
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неизвестный тип заказа.')
            ],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
            [
                'partner_id',
                'exist',
                'targetClass' => '\common\modules\partner\models\Partner',
                'targetAttribute' => 'id',
                'message' => Yii::t('common', 'Неизвестный партнер.')
            ],
            [
                ['last_queue_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Queue::className(),
                'targetAttribute' => ['last_queue_id' => 'id']
            ],
            [['last_queue_id'], 'integer'],
            ['finished', 'boolean'],
            [['order_phones'], 'string'],
            ['confirm_address', 'integer'],
            [['delivery_from', 'delivery_to'], 'integer'],
            [['longitude', 'latitude', 'google_zip'], 'string'],
            [['recall_date'], 'safe'],
            [['check_zorra'], 'boolean']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'type_id' => Yii::t('common', 'Тип'),
            'type.name' => Yii::t('common', 'Тип'),
            'country_id' => Yii::t('common', 'Страна'),
            'status' => Yii::t('common', 'Статус'),
            'sub_status' => 'Sub ' . Yii::t('common', 'Статус'),
            'partner_id' => Yii::t('common', 'Партнер'),
            'foreign_id' => Yii::t('common', 'Номер у партнера'),
            'customer_full_name' => Yii::t('common', 'Full name client'),
            'customer_first_name' => Yii::t('common', 'Имя'),
            'customer_last_name' => Yii::t('common', 'Фамилия'),
            'customer_middle_name' => Yii::t('common', 'Отчество'),
            'customer_phone' => Yii::t('common', 'Телефон'),
            'customer_mobile' => Yii::t('common', 'Мобильный'),
            'customer_address' => Yii::t('common', 'Адрес присланный клиентом'),
            'customer_ip' => Yii::t('common', 'IP-адрес'),
            'customer_components' => Yii::t('common', 'Дополнительные данные'),
            'approve_components' => Yii::t('common', 'Approve данные'),
            'comment' => Yii::t('common', 'Комментарий'),
            'order_comment' => Yii::t('common', 'Комментарий'),
            'init_price' => Yii::t('common', 'Цена начальная '),
            'final_price' => Yii::t('common', 'Цена конечная'),
            'shipping_id' => Yii::t('common', 'Служба доставки'),
            'price_shipping_id' => Yii::t('common', 'Тариф доставки'),
            'shipping_price' => Yii::t('common', 'Цена доставки'),
            'created_at' => Yii::t('common', 'Дата создания в CRM'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'ordered_at' => Yii::t('common', 'Дата создания у партнёра'),
            'country.name' => Yii::t('common', 'Страна'),
            'product' => Yii::t('common', 'Продукт'),
            'recall_date' => Yii::t('common', 'Дата повторного звонка'),
            'customerFullName' => Yii::t('common', 'Полное имя'),
            'productString' => Yii::t('common', 'Продукты'),
            'last_queue_id' => Yii::t('common', 'Очередь'),
            'form_id' => Yii::t('common', 'Форма'),
            'source_uri' => Yii::t('common', 'Url'),
            'phone_error' => Yii::t('common', 'Ошибки в телефоне'),
            'with_timezone' => Yii::t('common', 'Учитывать часовой пояс'),
            'attempts' => Yii::t('common', 'Количество попыток'),
            'delivery_from' => Yii::t('common', 'с'),
            'delivery_to' => Yii::t('common', 'по'),
            'confirm_address' => yii::t('common', 'Подтверждение адреса'),
            'date_first_call' => yii::t('common', 'Время первого до звонка (клиент)'),
            'date_first_call_on_crm' => yii::t('common', 'Время до первого звонка (КЦ)'),
            'time_getting_order_crm' => yii::t('common', 'Время до получения заказа КЦ'),
            'check_zorra' => yii::t('common', 'Проверка номера Zorra'),
            'original_id' => yii::t('common', 'Номер оригинала'),
            'status_queue_strategy' => Yii::t('common', 'Cтатус выставлен стратегией очереди'),
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $result = parent::load($data, $formName);

        $this->status = (int)$this->status; // хак

        if ($this->recall_date) {
            $recall_date = \DateTime::createFromFormat('d/m/Y H:i:s', $this->recall_date);
            if ($recall_date) {
                $this->recall_date = $recall_date->format('Y-m-d H:i:s');
            } else {
                $this->recall_date = null;
            }
        }

        if ($this->delivery_from) {
            $delivery_from = \DateTime::createFromFormat('d/m/Y H:i:s', $this->delivery_from, new \DateTimeZone(Timezone::DEFAULT_TIMEZONE));
            if ($delivery_from) {
                $this->delivery_from = $delivery_from->format('Y-m-d H:i:s');
            } else {
                $this->delivery_from = null;
            }
        }

        if ($this->delivery_to) {
            $delivery_to = \DateTime::createFromFormat('d/m/Y H:i:s', $this->delivery_to, new \DateTimeZone(Timezone::DEFAULT_TIMEZONE));
            if ($delivery_to) {
                $this->delivery_to = $delivery_to->format('Y-m-d H:i:s');
            } else {
                $this->delivery_to = null;
            }
        }

        $this->order_phones = implode(', ', $this->phones);

        if ($this->getGeneralModel()) {
            $this->getGeneralModel()->load(ArrayHelper::getValue($data, 'Order.general'), '');

            $general_attributes = $this->getGeneralModel()->attributes;
            //т.к. getValue удаляет из массива данные
            $general_attributes_main = $this->getGeneralModel()->attributes;

            foreach (PartnerForm::configurableAttributes() as $name) {
                $this->{$name} = ArrayHelper::getValue($general_attributes_main, $name);
            }

            $this->customer_components = Json::encode(ArrayHelper::merge(
                Json::decode($this->customer_components, true),
                ['general' => $general_attributes]
            ));
        }

        $customer_address_delivery = [];
        if ($this->getAddressModel()) {
            $customer_address = ArrayHelper::getValue($data, 'Order.address');

            if (is_numeric($this->partner_id) && is_numeric($this->country_id)) {

                $partnerForm = PartnerForm::find()->where([
                    'partner_id' => $this->partner_id,
                    'country_id' => $this->country_id
                ])->asArray()->one();
                $form_id = ArrayHelper::getColumn($partnerForm, 'id');

                if (isset($partnerForm['id']) && !empty($customer_address)) {
                    $attributes = PartnerFormAttribute::getFormAttributes($form_id);

                    foreach ($attributes as $attribute) {
                        if ($attribute->is_coordinate == 1) {
                            foreach ($customer_address as $column => $value) {
                                if ($attribute->name == $column) {
                                    if (!empty($value) && !in_array($value, $customer_address_delivery)) {
                                        $customer_address_delivery[] = $value;
                                    }
                                }
                            }
                        }
                    }
                    //признака кородинаты у индекса нет, но в строке он нужен
                    if (isset($customer_address['zip'])) {
                        $customer_address_delivery[] = $customer_address['zip'];
                    }
                }
            }
            $this->getAddressModel()->load($customer_address, '');
            $this->customer_components = Json::encode(ArrayHelper::merge(
                Json::decode($this->customer_components, true),
                [
                    'address' => $this->getAddressModel()->attributes + ['customer_address' => implode(", ", $customer_address_delivery)]
                ]
            ));
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        $res = parent::validate($attributeNames, $clearErrors);
        if ($this->status == self::STATUS_APPROVED && !$this->updateFromAPI) {
            $generalAttributeNames = is_array($attributeNames) ? $attributeNames : $this->getGeneralModel()
                ->attributes();
            $generalAttributeNames = array_diff($generalAttributeNames, PartnerForm::configurableAttributes());
            $res &= $this->getGeneralModel()->validate($generalAttributeNames);

            $addressAttributeNames = is_array($attributeNames) ? $attributeNames : $this->getAddressModel()
                ->attributes();
            $addressAttributeNames = array_diff($addressAttributeNames, PartnerForm::configurableAttributes());
            $res &= $this->getAddressModel()->validate($addressAttributeNames);
        }

        return $res;
    }

    public function normalizePhones()
    {
//        $util = PhoneNumberUtil::getInstance();
//        $region = $this->country->char_code;
//        $valid = true;
//
//        if (strlen($this->customer_phone) <= 18){
//            try {
//                $phone = $util->parse($this->customer_phone, $region);
//                $valid &= $util->isValidNumber($phone);
//                if ($valid) {
//                    $this->customer_phone = ltrim($util->format($phone, PhoneNumberFormat::E164), '+');
//                }
//            } catch (NumberParseException $e){
//                $valid = false;
//            }
//        }
//
//        $phones = array_filter(explode(', ', $this->order_phones));
//        $phones = array_map(function($number) use($util, $region, &$valid){
//            if (strlen($number) > 18){
//                return $number;
//            }
//
//            try{
//                $phone = $util->parse($number, $region);
//                $valid &= $util->isValidNumber($phone);
//                return ltrim($util->format($phone, PhoneNumberFormat::E164), '+');
//            }
//            catch (NumberParseException $e){
//                $valid = false;
//                return $number;
//            }
//        }, $phones);
//        $this->order_phones = implode(', ', $phones);
//
//        $this->phone_error = !$valid;
    }

    public function beginSave()
    {
        $this->group = uniqid();
        $this->transaction = $this->db->beginTransaction();
    }

    public function completeSave()
    {
        $this->group = null;
        $this->transaction->commit();

        $products = [];

        foreach ($this->orderProducts as $product) {
            $products[] = Json::encode($product->getAttributes());
        }

        /** @var BaseLogger $logger */
        $logger = yii::$app->get('logger');
        $logger->action = basename(__METHOD__);
        $logger->route = null;
        $logger->type = Order::LOGGER_TYPE;
        $logger->tags = [
            'order_id' => $this->id,
            'status' => $this->status,
            'order' => Json::encode($this->getAttributes()),
            'products' => Json::encode($products)
        ];
        $logger->save(false);

//        /** @var OrderLogs $current_log */
//        $current_log = $this->getOrderLogs()->one();
//
//        //пересчет статистики
//        $queue = ArrayHelper::getValue(\Yii::$app->params, 'orderQueueName', 'order.update');
//        /** @var Amqp $amqp */
//        $amqp = \Yii::$app->amqp;
//        $amqp->declareQueue($queue);
//
//        $previous_log = $this->getOrderLogs()->offset(1)->one();
//
//        //если нет прошлого состояния и заказ не новый
//        //задать данные для удаления из статуса "новый"
//        if (!$previous_log && $current_log) {
//            //достать данные товаров из апи-лога
//            $logQuery = ApiLog::find()
//                ->where(['order_id' => $this->id, 'url' => '/v1/order/create'])
//                ->orderBy(['created_at' => SORT_ASC]);
//
//            $products = [];
//            if ($logQuery->exists()) {
//                $log = $logQuery->one();
//                $products = array_map(function ($product) {
//                    $product['product_id'] = PartnerProduct::get($this->partner_id, $product['id']);
//                    return $product;
//                }, Json::decode($log->request)['products']);
//            }
//
//            $previous_log = [
//                'new' => self::STATUS_NEW,
//                'product' => Json::encode($products),
//                'created_at' => $this->created_at,
//                'fields' => Json::encode(['sub_status' => $this->sub_status])
//            ];
//        }
//
//        //если заказ новый
//        //задать данные для добавления в статус "новый"
//        if (!$current_log) {
//            $current_log = [
//                'new' => self::STATUS_NEW,
//                'product' => Json::encode(ArrayHelper::toArray($this->orderProducts)),
//                'created_at' => $this->created_at,
//                'fields' => Json::encode(['sub_status' => $this->sub_status])
//            ];
//        }
//
//        $amqp->publish_message(Json::encode([
//            'id' => $this->id,
//            'time' => time(),
//            'old' => $previous_log,
//            'new' => $current_log
//        ]), '', $queue);
//
//        $this->trigger(self::EVENT_AFTER_SAVE_COMPLETE);
    }

    public function cancelSave($error = [])
    {
        $this->group = null;
        $this->transaction->rollback();
        $this->transaction = null;

        $products = [];

        foreach ($this->orderProducts as $product) {
            $products[] = Json::encode($product->getAttributes());
        }

        /** @var BaseLogger $logger */
        $logger = yii::$app->get('logger');
        $logger->action = basename(__METHOD__);
        $logger->route = null;
        $logger->type = Order::LOGGER_TYPE;
        $logger->tags = [
            'order_id' => $this->id,
            'status' => $this->status,
            'order' => Json::encode($this->getAttributes()),
            'products' => Json::encode($products)
        ];
        $logger->error = Json::encode($error);
        $logger->save(false);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        //если были использованы виртуальные статусы - сабстатусы
        if ($status = OrderVirtualStatus::setStatusByVirtualSubStatus($this->status)) {
            $this->sub_status = $this->status;
            $this->status = $status;
        }

        if (isset($this->country) && isset($this->country->timezone) && isset($this->country->timezone->timezone_id)) {

            if ($this->delivery_from && !is_numeric($this->delivery_from)) {
                $this->delivery_from = yii::$app->formatter->asTimestampWithoutTimeZone($this->delivery_from) - $this->country->timezone->time_offset;
            }

            if ($this->delivery_to && !is_numeric($this->delivery_to)) {
                $this->delivery_to = yii::$app->formatter->asTimestampWithoutTimeZone($this->delivery_to) - $this->country->timezone->time_offset;
            }
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function clearDinamicProperty()
    {
        unset($this->date_first_call);


        if (isset($this->date_first_call_on_crm)) {
            unset($this->date_first_call_on_crm);
        }

        unset($this->time_getting_order_crm);
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        $this->clearDinamicProperty();

        if (isset($this->oldAttributes['last_queue_id']) && is_numeric($this->oldAttributes['last_queue_id'])) {
            $this->prevent_queue_id = $this->oldAttributes['last_queue_id'];
        }

        //Считаем, что если статус заказа мнятся на финальный то, мы должны снять его с очереди
        if (in_array($this->status, array_keys(Order::getFinalStatusesCollection()))) {
            if (!is_null($this->last_queue_id)) {
                $this->prevent_queue_id = $this->last_queue_id;
                $this->last_queue_id = null;
            }
        }

        // Если на статус Перезвон, то указать blocked_to
        if ($this->status == self::STATUS_RECALL) {
            $timeZone = new \DateTimeZone(Yii::$app->user->timezone->timezone_id);
            $dateTime = new \DateTime($this->recall_date, $timeZone);
            //если сохраняем бекап
            if (isset($this->blocked_to)) {
                $this->blocked_to = $dateTime->getTimestamp();
            }
        }

        if ($insert && !$this->form_id) {
            $form = PartnerForm::find()->where([
                'country_id' => $this->country_id,
                'type' => $this->type_id,
                'active' => 1
            ])->one();
            if (empty($form) || is_null($form)) {
                $form = PartnerForm::find()->where([
                    'country_id' => $this->country_id,
                    'type' => OrderType::TYPE_NEW,
                    'active' => 1
                ])->one();
            }

            if (isset($form->id)) {
                $this->form_id = $form->id;
            }

            //$this->customer_phone = CountryPhoneCode::checkPhone($this->customer_phone, $this->country_id);
        }

        $groupId = $this->group ? $this->group : uniqid();

        // Для нового заказа в order_log запишем все аттрибуты заказа

        if ($this->is_backup) {
            return parent::beforeSave($insert);
        }

        if (!$this->withOutLogs) {

            $user_id = null;

            if (method_exists(Yii::$app->request, 'get')) {
                $user_id = !empty(Yii::$app->request->get('user_id')) ? Yii::$app->request->get('user_id') : Yii::$app->user->id;
            } else {
                $user_id = !is_null(Yii::$app->user->id) ? Yii::$app->user->id : null;
            }

            if ($insert) {
                $dirtyAttributes = $this->attributes;
                unset($dirtyAttributes['status']);
                $this->on(self::EVENT_AFTER_INSERT, function () use ($groupId, $dirtyAttributes) {
                    // Старый метод записи логов
                    foreach ($dirtyAttributes as $key => $item) {
                        $log = new OrderLog();
                        $log->order_id = $this->id;
                        $log->group_id = $this->group_id ? $this->group_id : $groupId;
                        $log->field = $key;
                        $log->old = '';
                        $log->new = is_array($item) ? mb_substr(json_encode($item), 0, 250) : mb_substr($item, 0, 250);

                        if (method_exists(yii::$app->request, 'get') || !is_null(Yii::$app->user->id)) {
                            $log->user_id = is_null(Yii::$app->user->id) ? yii::$app->request->get('user_id') : Yii::$app->user->id;
                        }

                        $log->operator = $this->operator;
                        if (!empty($this->comment)) {
                            $log->comment = Html::decode($this->comment); // сохраним коммент
                        }
                        $log->save(false);
                    }
                });
            } // Сохраним новый статус заказа в таблице order_logs
            else {
                // Новый метод записи логов
                $this->saveLogsByOrder();

                if (!in_array('status', $this->ignoreAttributes)) {
                    // Старый метод записи логов
                    $log = new OrderLog();
                    $log->order_id = $this->id;
                    $log->group_id = $this->group_id ? $this->group_id : $groupId;
                    $log->field = 'status';
                    $log->old = (string)$this->getOldAttribute('status');
                    $log->new = (string)$this->status;
                    $log->user_id = $user_id;
                    $log->operator = $this->operator;
                    $log->queue_id = $this->last_queue_id; // при какой очереди сменили статус
                    $log->comment = Html::decode($this->comment); // сохраним коммент
                    $log->save(false);
                }
                // запишем только измененные аттрибуты
                foreach ($this->getDirtyAttributes() as $key => $item) {
                    if ($key != 'status' && $item != $this->getOldAttribute($key)) {
                        if (!in_array($key, $this->ignoreAttributes)) {
                            $log = new OrderLog();
                            $log->order_id = $this->id;
                            $log->group_id = $this->group_id ? $this->group_id : $groupId;
                            $log->field = $key;
                            $log->old = (string)$this->getOldAttribute($key);//mb_substr((string)$this->getOldAttribute($key), 0, 250);
                            $log->new = (string)$item;//mb_substr($item, 0, 250);
                            $log->user_id = $user_id;
                            $log->operator = $this->operator;
                            $log->save(false);
                        }
                    }
                }
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->id) {
            $this->phones = array_filter(explode(', ', $this->order_phones));
            $this->partner_token = $this->partner->token;
            $this->date_first_call = $this->callHistory ? ($this->callHistory[0]['created_at'] - $this->partner_created_at) : null;
            $this->date_first_call_on_crm = $this->callHistory ? ($this->callHistory[0]['created_at'] - $this->created_at) : null;
            $this->time_getting_order_crm = $this->created_at - $this->partner_created_at;

            if ($this->delivery_from) {
                $this->delivery_from = $this->delivery_from + $this->country->timezone->time_offset;
            }

            if ($this->delivery_to) {
                $this->delivery_to = $this->delivery_to + $this->country->timezone->time_offset;
            }

        }

        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(OrderType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProduct::className(), ['order_id' => 'id'])->andWhere(['promo' => false])->indexBy('id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPromoProducts()
    {
        return $this->hasMany(OrderProduct::className(), ['order_id' => 'id'])->andWhere(['promo' => true])->indexBy('id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderGiftProducts()
    {
        return $this->hasMany(OrderProduct::className(), ['order_id' => 'id'])->andWhere(['gift' => true])->indexBy('id');
    }

    /**
     * Для пустой связи, н-р, при создании заказа
     */
    public function setOrderProducts($products)
    {
        $this->orderProducts = $products;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalls()
    {
        return $this->hasMany(Call::className(), ['PRODUCT_ID' => 'id'])->orderBy(['CALL_START' => SORT_ASC]);
    }

    /**
     * @return string
     */
    public function generateIframeUid()
    {
//        if (empty(Yii::$app->genesys->secretKey)) {
//            throw new InvalidParamException(Yii::t('common', 'Отсутствует SecretKey "Genesys".'));
//            return '';
//        }

//        $hash = $this->id . $this->country_id . $this->partner_id . Yii::$app->genesys->secretKey;
        $hash = $this->id . $this->country_id . $this->partner_id;
        $id = str_pad($this->id, 6, '0', STR_PAD_LEFT);

        return $id . '-' . sha1($hash);
    }

    /**
     * @param $identity
     * @return string
     */
    public function generateIframeUrl($identity, $quality_control = false, $disabledStatusButtons = null, $hideStatusButtons = null, $holdOnPause = null)
    {
        $quality_control_param = '&quality-control=' . intval($quality_control);

        if (!is_null($disabledStatusButtons)) {
            $quality_control_param .= '&disabled_status_buttons=' . intval($disabledStatusButtons);
        }

        if (!is_null($hideStatusButtons)) {
            $quality_control_param .= '&hide_status_buttons=' . intval($hideStatusButtons);
        }

        if (!is_null($holdOnPause)) {
            $quality_control_param .= '&hold_on_pause=' . intval($holdOnPause);
        }

        $uid = $this->generateIframeUid();

        if (empty($identity)) {
            return Yii::$app->params['urlApi'] . '/order/change/' . $uid . '.html?operator=' . $quality_control_param;
        }

        return Yii::$app->params['urlApi'] . '/order/change/' . $uid . '.html?operator=' . $identity->username . '&user_id=' . $identity->id . $quality_control_param;
    }

    /**
     * Функция формирует ссылку на iframe для списка заказов
     *
     * @param $country
     * @param $phone
     * @return string
     */
    public function generateIframeUidList($country, $phone)
    {
//        if (empty(Yii::$app->genesys->secretKey)) {
//            throw new InvalidParamException(Yii::t('common', 'Отсутствует SecretKey "Genesys".'));
//        }

//        $hash = $country . $phone . Yii::$app->genesys->secretKey;
        $hash = $country . $phone;

        $country_id = str_pad($country, 6, '0', STR_PAD_LEFT);
        $phone_num = str_pad($phone, 20, '0', STR_PAD_LEFT);

        return $country_id . '-' . $phone_num . '-' . sha1($hash);
    }

    /**
     * Функция генерирует Список заказов и Фильтр для iFrame
     *
     * в формате /order/index/000002-00000000039099944334-2e5f7b3dbbb802941faf5420b80af21cb2e2b598.html?operator=op
     * @param string $country
     * @param string $phone
     * @param string $operator
     * @return string
     */
    public function generateIframeUrlList(string $country, string $phone, string $operator)
    {
        $uid = $this->generateIframeUidList($country, $phone);

        return Yii::$app->params['urlApi'] . '/order/index/' . $uid . '.html?operator=' . Html::encode($operator);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(PartnerForm::className(), ['id' => 'form_id']);
    }

    /**
     * @return FormModel
     */
    public function getGeneralModel()
    {

        if (!$this->_extra_model) {
            $attrs = ArrayHelper::getValue(Json::decode($this->customer_components, true), 'general', []);

            $model = new FormModel();
            $model->name = $this->formName() . '[general]';

            if ($this->form_id) {
                /** @var PartnerFormAttribute $attr */
                foreach ($this->form->generalAttrs as $attr) {
                    if (in_array($attr->name, PartnerForm::configurableAttributes())) {
                        $value = $this->{$attr->name};
                    } else {
                        $value = ArrayHelper::getValue($attrs, $attr->name);
                    }
                    $attr->addToModel($model, $value);
                }
            }
            $this->_extra_model = $model;
        }

        return $this->_extra_model;
    }

    /**
     * @return FormModel
     */
    public function getAddressModel()
    {
        if (!$this->_address_model) {
            $attrs = ArrayHelper::getValue(Json::decode($this->customer_components, true), 'address', []);
            $model = new FormModel();
            $model->name = $this->formName() . '[address]';
            if ($this->form_id) {
                /** @var PartnerFormAttribute $attr */
                foreach ($this->form->addressAttrs as $attr) {
                    $attr->addToModel($model, ArrayHelper::getValue($attrs, $attr->name));
                }
            }
            $this->_address_model = $model;
        }
        return $this->_address_model;
    }

    /**
     * Собрать комменты, из табл.orders + order_log(s), где комменты привязаны к изменениям статуса
     * @return array
     */
    public function getComments()
    {
        $components = json_decode($this->customer_components);
        $comp_comments = !empty($components->comments) ? $components->comments : null;

        // по дефолту массив комментов из 2 таблиц
        $comments = [
            'order' => [],
            'order_logs' => [],
        ];
        if ($comp_comments) {
            // удалим все кроме текста коммента
            foreach (ArrayHelper::toArray($comp_comments) as $comment) {
                $comments['order'][] = [
                    'text' => $comment['text'],
                    'operator_name' => '',
                ];
            }
        }

        // Вытащим сохраненные комменты при смене статуса заказа
        // @todo Сменить на OrderLogs
        $log_comment = OrderLog::find()
            ->where(['order_id' => $this->id])
            ->andWhere(['is not', 'comment', null])
            ->andWhere(['<>', 'comment', ''])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        $map_log_comments = array_map(function ($value) {
            return [
                'created_at' => $value->created_at,
                'text' => $value->comment,
                'operator_name' => $value->operatorName,
            ];
        }, $log_comment);

        $comments['order_logs'] = ArrayHelper::index($map_log_comments, 'created_at');

        // итог
        return ArrayHelper::merge($comments['order'], $comments['order_logs']);
    }

    public function getTextComments()
    {
        $log_comment = OrderLog::find()
            ->select(['comment'])
            ->where(['order_id' => $this->id])
            ->andWhere(['is not', 'comment', null])
            ->andWhere(['<>', 'comment', ''])
            ->orderBy(['id' => SORT_DESC])
            ->asArray()
            ->all();


        return $log_comment ?? [];
    }

    public function getLastComment()
    {
        $comments = $this->getTextComments();
        return !empty($comments) ? $comments[0]['comment'] : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        // @todo Сменить на OrderLogs и на таблицу "order_logs"
        return $this->hasMany(OrderLog::className(), ['order_id' => 'id'])->orderBy(['order_log.id' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusLogs()
    {
        return $this->getLogs()->where(['field' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductLogs()
    {
        return $this->hasMany(OrderProductLog::className(), ['order_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrderLogs()
    {
        return $this->hasMany(OrderLogs::className(), ['order_id' => 'id'])->orderBy(['order_logs.id' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'last_queue_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSimilar()
    {
        return self::find()->where(['not', ['id' => $this->id]])->andWhere(['customer_phone' => $this->customer_phone]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenesysRecords()
    {
        return $this->hasMany(OrderGenesys::className(), ['order_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getCustomerFullName()
    {
        return implode(' ', [$this->customer_last_name, $this->customer_first_name, $this->customer_middle_name]);
    }

    /**
     * Выводит в строку продукты (с количеством)
     * @return string
     */
    public function getProductString()
    {
        $products = [];
        foreach (ArrayHelper::merge($this->orderProducts, $this->orderPromoProducts) as $product) {
            $products[] = $product->product->name . ($product->quantity >= 1 ? ' <small>(' . $product->quantity . ' ' . ($product->gift ? 'gift' : ($product->promo ? 'promo' : '')) . ')</small>' : null);
        }

        return implode('<br/> ', $products);
    }

    /**
     * @return string
     */
    public function getPartnerName()
    {
        return $this->partner->name;
    }

    /**
     * Возвращает Код языка заказа
     * @return mixed|string
     */
    public function getLanguageCode()
    {
        $code = 'EN';
        $locale = null;
        $charUsed = false;
        if (!empty($this->language)) {
            $locale = $this->language->locale;
        } elseif (!empty($this->country->language)) {
            $locale = $this->country->language->locale;
        } else {
            $code = $this->country->char_code;
            $charUsed = true;
        }

        if (!empty($locale) && !$charUsed) {
            $code = explode('-', $locale);
            $code = end($code);
        }
        return $code;
    }

    /**
     * Возвращает название языка заказа
     * @return string
     */
    public function getLanguageName()
    {
        $default_name = 'English';

        $language = null;
        if (!empty($this->language)) {
            $language = $this->language;
        } elseif (!empty($this->country->language)) {
            $language = $this->country->language;
        }

        return $language ? $language->genesys_name : $default_name;
    }

    /**
     * Функция вернет кнопки в форму заказа в определенном порядке
     * @param Order $order
     * @return array|mixed
     */
    public static function getButtonsStatus(self $order)
    {
        $buttons = ArrayHelper::getValue($order, 'form.buttonsConfig', []);
        $approved = self::STATUS_APPROVED;

        // если кнопка Одобрено есть, то делаем ее 1-ой
        if (key_exists($approved, $buttons)) {
            $btnApproved[$approved] = $buttons[$approved];
            unset($buttons[$approved]);

            $buttons = ArrayHelper::merge($btnApproved, $buttons);
        }

        return $buttons;
    }

    public static function getVirtualButtonsStatus(self $order)
    {
        $buttons = ArrayHelper::getValue($order, 'form.virtualButtonsConfig', []);

        return $buttons;
    }

    /**
     * Обновить параметры с учетом $where
     *
     * н-р, обновить поле "last_queue_id"
     * @param array $params
     * @param array $where
     * @return bool
     */
    public static function updateByParams(array $params, array $where)
    {
        if (Yii::$app->db->createCommand()->update('{{%order}}', $params, $where)->execute()) {
            return true;
        }

        return false;
    }

    public function addToQueue($queueId = false)
    {
        if ($queueId) {
            $queue['id'] = $queueId;
        } else {
            // поле "primary_components" является jsonb
            $queryQueues = Yii::$app->db->createCommand('SELECT * FROM "queue"
                      WHERE primary_components @>:QUERY
                      AND is_primary=:is_primary
                      AND active=:active
                      ORDER BY priority ASC
                ')
                ->bindValue(':QUERY', Json::encode([
                    'primary_partner' => (string)$this->partner_id,
                    'primary_type' => (string)$this->type_id,
                ]))
                ->bindValue(':is_primary', true)
                ->bindValue(':active', 1)
                ->queryAll();

            if (count($queryQueues) > 0) {
                // Далее необходимо найди вхождение для "primary_product" (это указанный продукт),
                // т.к. у очереди может быть задан данный параметр
                $queue = null;
                $availableQueues = [];
                foreach ($queryQueues as $queryQueue) {
                    /**
                     * Проверка на соответствие страны
                     */
                    $is_country = false; // по дефолту
                    $components = Json::decode($queryQueue['primary_components'], true);
                    // отфильтруем сразу те очереди, что не подходят нам по Параметру "country_id"
                    $primary_country = Json::decode($components['primary_country']);
                    // Если это массив, то ищем нужную страну внутри

                    if (is_array($primary_country)) {
                        if (in_array($this->country_id, $primary_country)) {
                            $is_country = true;
                        }
                    } // Если это строка, то сравним
                    else if ($primary_country == $this->country_id) {
                        $is_country = true;
                    }

                    // если страна не совпадает, то next iteration
                    if (!$is_country) {
                        continue;
                    } else {
                        $availableQueues[] = $queryQueue;
                    }

                    /**
                     * Проверка на соответствие товара
                     */
                    if (key_exists('primary_product', $components) && !empty($components['primary_product'])) {
                        // массив ID продуктов из настроек очереди
                        $products = Json::decode($components['primary_product'], true);
                        // массив ID продуктов из API запроса
                        $orderProducts = ArrayHelper::getColumn($this->orderProducts, 'product_id');
                        // найдем схождение значений массивов
                        $intersectArray = array_intersect($products, $orderProducts);

                        if (!empty($intersectArray)) {
                            $queue = $queryQueue;
                        } else { //Если у очереди оказались включенны продукты, но в это заказе не оказалось подходящего продукта то, удаляем эту очередь из общедоступных
                            //На всякий случай удалим ее из общедоступных
                            foreach ($availableQueues as $key => $value) {
                                if ($queryQueue['id'] == $value['id']) {
                                    unset($availableQueues[$key]);
                                }
                            }
                        }

                    }

                    $queryQueueLandings = QueueLanding::findAll(['queue_id' => $queryQueue['id']]);

                    if (!empty($queryQueueLandings)) {
                        if (in_array($this->source_uri, ArrayHelper::getColumn($queryQueueLandings, 'url_landing'))) {
                            $queue = $queryQueue;
                        } else {
                            //На всякий случай удалим ее из общедоступных
                            foreach ($availableQueues as $key => $value) {
                                if ($queryQueue['id'] == $value['id']) {
                                    unset($availableQueues[$key]);
                                }
                            }
                        }
                    }
                }

                $availableQueues = array_values($availableQueues);
                // если есть очередь, с ID товара в настройках, то используется $queue
                // либо присваиваем первой очереди из массива $availableQueues
                if (!$queue && !empty($availableQueues[0])) {
                    $queue = $availableQueues[0];
                }
            }
        }

        // привоим ID этой очереди данному заказу в виде "last_queue_id"
        if (isset($queue) && self::updateByParams(['last_queue_id' => $queue['id']], ['id' => $this->id])) {
            echo 'QUEUE ' . $queue['id'] . PHP_EOL;
            //$this->addOrderIdToQueue($this->id, $queue['name']);
        }
    }


    public function addToQueueNew()
    {
        $queryQueues = Queue::find()
            ->primary()
            ->active()
            ->byPartner($this->partner_id)
            ->byType($this->type_id)
            ->byCountry($this->country_id)
            ->orderBy(['priority' => SORT_ASC])
            ->all();

        if (count($queryQueues) > 0) {
            // Далее необходимо найди вхождение для "primary_product" (это указанный продукт),
            // т.к. у очереди может быть задан данный параметр
            $queue = null;
            $availableQueues = [];
            foreach ($queryQueues as $queryQueue) {
                $components = Json::decode($queryQueue['primary_components'], true);

                $availableQueues[] = $queryQueue;

                $products = isset($components['primary_product']) ? Json::decode($components['primary_product']) : [];

                /**
                 * Проверка на соответствие товара
                 */
                if (!empty($products)) {
                    // массив ID продуктов из настроек очереди

                    // массив ID продуктов из API запроса
                    $orderProducts = ArrayHelper::getColumn($this->orderProducts, 'product_id');
                    // найдем схождение значений массивов
                    $intersectArray = array_intersect($products, $orderProducts);

                    if (!empty($intersectArray)) {
                        $queue = $queryQueue;
                    } else { //Если у очереди оказались включенны продукты, но в это заказе не оказалось подходящего продукта то, удаляем эту очередь из общедоступных
                        //На всякий случай удалим ее из общедоступных
                        foreach ($availableQueues as $key => $value) {
                            if ($queryQueue['id'] == $value['id']) {
                                unset($availableQueues[$key]);
                            }
                        }
                    }

                }

                $queryQueueLandings = QueueLanding::findAll(['queue_id' => $queryQueue['id']]);

                if (!empty($queryQueueLandings)) {
                    if (in_array($this->source_uri, ArrayHelper::getColumn($queryQueueLandings, 'url_landing'))) {
                        $queue = $queryQueue;
                    } else {
                        //На всякий случай удалим ее из общедоступных
                        foreach ($availableQueues as $key => $value) {
                            if ($queryQueue['id'] == $value['id']) {
                                unset($availableQueues[$key]);
                            }
                        }
                    }
                }
            }

            $availableQueues = array_values($availableQueues);

            // если есть очередь, с ID товара в настройках, то используется $queue
            // либо присваиваем первой очереди из массива $availableQueues
            if (!$queue && !empty($availableQueues[0])) {
                $queue = $availableQueues[0];
                $this->last_queue_id = $queue['id'];
                echo 'QUEUE ' . $queue['id'] . PHP_EOL;

                // привоим ID этой очереди данному заказу в виде "last_queue_id"
//            if (self::updateByParams(['last_queue_id' => $queue['id']], ['id' => $this->id])) {
//                echo 'QUEUE ' . $queue['id'] . ' for order_id #' . $this->id . PHP_EOL;
//                //$this->addOrderIdToQueue($this->id, $queue['name']);
//            }

                return true;
            }

            return false;


        }
    }

    /**
     * Складывает ID заказа в нужную очередь RabbitMQ
     */
    public function addOrderIdToQueue()
    {
        $amqp = Yii::$app->amqp;
        $connection = new AMQPStreamConnection($amqp->host, $amqp->port, $amqp->user, $amqp->password, $amqp->vhost);
        $channel = $connection->channel();

        $msg = new AMQPMessage($this->id, [
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
        ]);
        $channel->basic_publish($msg, Queue::EXCHANGE, $this->queue_id);
    }

    /**
     * Функция определяет стратегию для заказа, который относится к конкретной очереди
     *
     * Стратегия хранится в формате:
     * {
     *  "3": {
     *          "strategyCount": 5,
     *          "blockTime": 5, // в минутах
     *          "transferToQueue": 3
     *      }
     *  }
     * ... либо ...
     * {
     *  "3": {
     *          // "strategyPreset" - это ID пресета из модели Presets
     *          "strategyPreset": 1,
     *          "blockTime": 300,
     *          "transferToQueue": 3
     *      }
     *  }
     *
     * где ключ - это статус, для которого действует правило стратегии.
     * При наступлении повторов этого статуса count-раз, необходимо применит правило:
     * "changeStatus" - смена статуса заказа на changeStatus
     * "transferToQueue" - перенести заказ в очередь transferToQueue, т.е. изменить поле last_queue_id
     *
     * @return bool|void
     */
    public function useStrategyByQueue()
    {
        // Только если для данного заказа есть связь с Очередями
        if (!$this->lastQueue) {
            return false;
        }
        // Выполнить проверку на повтор статуса и сверка его со Стратегией исполненной очереди
        if (isset($this->lastQueue->strategy[$this->status])) {
            $strategy = $this->lastQueue->strategy[$this->status];
        } else {
            return false;
        }

        // сколько всего было последних попыток с одинаковым статусом
        // Если предыд.статус был такой же (в частном случае НЕДОЗВОН),
        // либо, если Пре.статус был НОВЫЙ

        /*$lastOrderLogs = OrderLogs::find()
            ->select(['old', 'new'])
            ->where(['order_id' => $this->id])
            ->orderBy(['created_at' => SORT_DESC])
            ->one();*/

        $this->queue_counter++;

        // тек.время по дефолт.таймзоне с временем блокировки
        $current_time = (new \DateTime())
            ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
            ->getTimestamp();
        // по дефолту
        $countStrategy = 0;
        //может использоваться режим "авто"
        $strategy = is_array($strategy) ? $strategy : [];

        // Стратегия для указ.вручную кол-ва Попыток "strategyCount" и для пресета "strategyPreset"
        if (key_exists('strategyCount', $strategy)) {
            $countStrategy = $strategy['strategyCount'];

            $this->blocked_to = $current_time +
                (key_exists('blockTime', $strategy) ? $strategy['blockTime'] : 0)
                * 60;
        } // Если использовалась очередь с настройками пресета
        //TODO отключили во вьюхе отображение чекбокса для пресетов
        else if (key_exists('strategyPreset', $strategy)) {
            $presetQuery = Presets::find()->where(['id' => $strategy['strategyPreset']]);
            if ($presetQuery->exists()) {
                $preset = $presetQuery->one();
                $countStrategy = count($preset->attempts);
                $this->blocked_to = $current_time + $preset->attempts[$this->queue_counter] * 60;
            }
        }

        if ($this->status == self::STATUS_RECALL) {
            if (!empty($this->recall_date)) {
                $this->blocked_to = strtotime($this->recall_date);
            }
        }

        // применить стратегию, при условии выполнении условий
        if ($countStrategy !== 0 && $countStrategy <= $this->queue_counter) {
            $params = [];
            // Изменить статус на финальный
            if (key_exists('changeStatus', $strategy)) {
                $this->status = $strategy['changeStatus'];
                //Сбрасываем счетчик в 0
                $this->queue_counter = 0;
                if (in_array($this->status, array_keys(self::getFinalStatusesCollection()))) {
                    // Для финальных статусов сбросить last_queue_id
                    $this->last_queue_id = null;
                    // для статуса Отклонено добавить субстатус АВТООТКЛОН
                    if ($this->status == self::STATUS_REJECTED) {
                        $this->sub_status = self::SUB_STATUS_REJECT_REASON_AUTO;
                        $this->operator = null;
                    }
                }
            } // Изменить принадлежность заказа к очереди
            else if (key_exists('transferToQueue', $strategy)) {
                //Сбрасываем счетчик в 0
                $this->queue_counter = 0;
                $this->last_queue_id = $strategy['transferToQueue'];
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function getFinal()
    {
        return in_array($this->status, array_keys(self::getFinalStatusesCollection()));
    }

    /**
     * @return bool
     */
    public function getBuffered()
    {
        /** @var OrderLog $latest_status_log */
        $latest_status_log = $this->getStatusLogs()->one();
        if (!$latest_status_log) {
            if (!in_array($this->status, array_keys(self::getFinalStatusesCollection()))) {
                return false;
            } else {
                return (time() - $this->created_at) / 60 < $this->partner->getSettingsByOrder($this)->buffer;
            }
        } else {
            if (!in_array($latest_status_log->new, array_keys(self::getFinalStatusesCollection()))) {
                return false;
            } else {
                return (time() - $latest_status_log->created_at) / 60 < $this->partner->getSettingsByOrder($this)->buffer;
            }
        }
    }

    /**
     * Вернет заказы, у которых status=2 (recall Перезвон)
     * Отобразит заказы с перезвоном, доступные текущему оператору
     */
    public static function getRecallOrders()
    {
        $query = Order::find()
            ->select(Order::tableName() . '.*')
            ->leftJoin(OrderLogs::tableName(), OrderLogs::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->where([Order::tableName() . '.status' => Order::STATUS_RECALL])
            ->andWhere([OrderLogs::tableName() . '.new' => Order::STATUS_RECALL])
            ->andWhere(['like', OrderLogs::tableName() . '.operator', \Yii::$app->user->identity->username])
            ->all();

        return $query;
    }

    /**
     * Вернет заказы, у которых status=4 (approve) и approve_components={code=5}
     * Т.е. апрувы, которые имеют ошибку в статусе заказа (code=5)
     */
    public static function getWrongOrders()
    {
        $query = \Yii::$app->db->createCommand('SELECT * FROM "order"
                      WHERE approve_components @>:approve
                      ORDER BY id DESC
                ')
            ->bindValue(':approve', '{"code":5}')
            ->queryAll();

        return $query;
    }

    /**
     * Функция определит кол-во последних одинаковых статусов
     * н-р, если посл.статус 3 - Недозвон,
     * то функция подсчитает кол-во последних подряд идущих статусов
     *
     * @return int
     */
    public function getCountSerialStatus()
    {
        // Вытащить последние логи, чтобы узнать сколько уже было попыток
        $orderLogs = $this->getOrderLogs()->asArray()->select(['new', 'queue_id'])->all();

        $count = 0;
        foreach ($orderLogs as $log) {
            // считаем только одинаковые последние статусы,
            // а также статусы были выставлены при одной очереди "queue_id"
            if ($log['new'] == (int)$this->status && $log['queue_id'] == (int)$this->last_queue_id)
                $count++;
            else
                break;
        }

        return $count;
    }

    /**
     * Метод запишет в логи данные заказа по требованию
     */
    public function saveLogsByOrder()
    {
        //если модель ордера по модели бекапа была собрана
        $this->id = isset($this->order_id) ? $this->order_id : $this->id;

        $user_id = null;
        if (!is_null(Yii::$app->user->id)) {
            $user_id = Yii::$app->user->id;
        } else if (method_exists(yii::$app->request, 'get')) {
            $user_id = Yii::$app->request->get('user_id') ? Yii::$app->request->get('user_id') : Yii::$app->request->post('user_id') ? Yii::$app->request->post('user_id') : null;
        }

        $logs = new OrderLogs();
        $logs->order_id = $this->id;
        $logs->queue_id = $this->last_queue_id; // при какой очереди сменили статус
        $logs->old = (string)$this->getOldAttribute('status');
        $logs->new = (string)$this->status;
        $logs->user_id = $user_id;//is_null(Yii::$app->user->id) ? (method_exists(yii::$app->request, 'get') ? yii::$app->request->get('user_id') : null) : Yii::$app->user->id;
        $logs->operator = $this->operator;
        $logs->comment = Html::decode($this->comment); // сохраним коммент
        $logs->fields = Json::encode($this->getDirtyAttributes());
        $logs->product = Json::encode(ArrayHelper::toArray($this->orderProducts));
        $logs->save(false);
    }

    /**
     * Получить массив всех телефонов заказа
     * @return array
     */
    public function getAllPhones()
    {
        $customer_phones[] = [];
        // найти все звонки, которые соответствуют телефонам этого заказа
        foreach ([$this->customer_mobile, $this->customer_phone] as $item) {
            if (!empty($item)) {
                $customer_phones[] = $item;
            }
        }

        $order_phones = explode(', ', $this->order_phones);
        $phones = ArrayHelper::merge($customer_phones, $order_phones);

        return $phones;
    }

    /**
     * @return ActiveQuery
     */
    public function getQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'last_queue_id']);
    }

    /**
     * @param bool $runValidation
     * @param array $ignoreAttributeNames
     * @param null $attributeNames
     * @return bool
     */
    public function saveWithoutLogs($runValidation = true, $ignoreAttributeNames = [], $attributeNames = null)
    {
        $this->withOutLogs = false;

        if (!empty($ignoreAttributeNames)) {
            $this->ignoreAttributes = $ignoreAttributeNames;
        }

        return $this->save($runValidation, $attributeNames);
    }

    /**
     * внутр.метод для получения формы заказа по API-ссылке
     *
     * @param bool $isExists
     * @param bool $isBackup
     * @param bool $isBlock
     * @return array
     */
    public function prepareForm($isExists = false, $isBackup = false, $isBlock = true)
    {
        $useBackup = false;
        $language = Yii::$app->user->getLanguage();
        Yii::$app->language = $language->locale;
        Yii::$app->formatter->setLocale($language->locale);

        //начало работы с бекапами
        //если бекапы включены и запрашиваемый ордер не является бекапом
        if (yii::$app->params['useOrderBackup'] === true && $isBackup === false) {
            $order_id = $this->id;
            //попытаемся найти бекап выбранного ордера
            $orderBackupQuery = OrderBackup::find()->where(['order_id' => $order_id]);

            //бекап найден
            if ($orderBackupQuery->exists()) {
                $orderBackup = $orderBackupQuery->one();

                //отдадим бекап в модели ордера
                $order = OrderBackup::backupTransferToOrder($orderBackup);

                //нужно заменить url по которому будет запрошен заказ
                $backupUserOrderQuery = OrderUserBackup::find()->where(['order_id' => $order_id]);

                //если данные по url найдены - то работаем с данным бекапом
                if ($backupUserOrderQuery->exists()) {
                    $useBackup = true;

                    $backupUserOrder = $backupUserOrderQuery->one();

                    //урл может быть 2х видов - по апи и если заказ создаёт сам оператор
                    //http://2wcall.com/order/change/139878
                    //http://api.2wcall.com/order/change/139955-77186d6cf84f3fbbd532927be96fe8e3df530391.html?operator=IDOP1AM-57&user_id=57
                    //http://api.2wcall.com/order/change/139525-8b8863177fe22acb7123540e1701e5ff69ae0fd6.html?operator=IDOP1AM-77&user_id=77&order_backup_id=353
                    $url_params = parse_url($backupUserOrder->url);

                    if (isset($url_params['query'])) {
                        //это те варианты ссылок - которые формируются для api
                        //http://api.2wcall.com/order/change/139955-77186d6cf84f3fbbd532927be96fe8e3df530391.html?operator=IDOP1AM-57&user_id=57
                        //http://api.2wcall.com/order/change/139525-8b8863177fe22acb7123540e1701e5ff69ae0fd6.html?operator=IDOP1AM-77&user_id=77&order_backup_id=353
                        //при формировании ссылки - добавим get param backup
                        $backupUserOrder->url = explode("&order_backup_id", $backupUserOrder->url);
                        $url = $backupUserOrder->url[0] . '&order_backup_id=' . $orderBackup->id;
                    } else {
                        //http://2wcall.com/order/change/139878
                        $url = $backupUserOrder->url . '?order_backup_id=' . $orderBackup->id;
                    }

                }
            }
        }
        //конец работы с бекапами

        // обязательно передаем логин Оператора
        // url мог быть определён в backup
        if (!$useBackup || !isset($url)) {
            $url = $this->generateIframeUrl(Yii::$app->user->identity);
            // $url = 'http://unicall.dev:8080/order/change/109462-b494bd0abb01d27c5ec8478de47af3d930acc912.html?operator=...&user_id=..';
        }

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);

        if (yii::$app->params['is_local']) {
            $options = [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 60,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false
            ];
        } else {
            $options = [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 60,
            ];
        }

        // @TODO 21.03.2018 Использовалась ранее для получения формы, сейчас все получается в iframe по ссылке
        /*$response = $client->createRequest()
            ->setUrl($url)
            ->setOptions($options)
            ->send();*/

        // Блокируем данный заказ на 30 мин
        $currentTime = (new \DateTime())
            ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
            ->getTimestamp();
        $blocked_to = $currentTime + 1800; // в секундах
        $orderId = $this->id;
        if ($isBlock) {
            Order::updateByParams(['blocked_to' => $blocked_to], ['id' => $orderId]);
        }

        // Запишем в таблицу "queue_order"
        if (!$isExists) {
            Yii::$app->db->createCommand()->insert('{{%queue_order}}', [
                'user_id' => \Yii::$app->user->id,
                'order_id' => $orderId,
                'blocked_to' => $blocked_to,
            ])->execute();
        }

        $data = [
            'result' => 'ok',
            'url' => $url,
            'order' => [
                'order_id' => $orderId,
                'blocked_to' => $blocked_to,
            ],
            'content' => '' //$response->content,
        ];


        return $data;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallHistory()
    {
        return $this->hasMany(CallHistory::className(), ['order_id' => 'id'])->orderBy(['id' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiveMessages()
    {
        return $this->hasMany(LiveMessage::className(), ['order_id' => 'id'])->orderBy(['id' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipping()
    {
        return $this->hasOne(Shipping::className(), ['id' => 'shipping_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceShipping()
    {
        return $this->hasOne(PriceShipping::className(), ['id' => 'price_shipping_id']);
    }


    /**
     * Метод возвращает массив с
     * @return array
     */
    public function verificateAddress()
    {
        /** @var Order $this */
        $data = json_decode($this->customer_components);

        if (isset($data->address)) {
            $form = $this->form;

            if ($form instanceof PartnerForm) {
                $formAttributes = ArrayHelper::map($this->form->addressAttrs, 'name', 'id');
                $addressData = (array)$data->address;
                $where = [];

                foreach ($formAttributes as $attr => $id) {
                    if (isset($addressData[$attr])) {
                        $where[$id] = $addressData[$attr];
                    }
                }

                if (!empty($where)) {
                    $findByKlader = KladerCountries::find();

                    foreach ($where as $id => $name) {
                        $findByKlader->andWhere([
                            'partner_form_attribute_id' => $id,
                            'name' => $name
                        ]);
                    }

                    return ArrayHelper::getColumn($findByKlader->all(), 'partner_form_attribute_id');
                }
            }
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAggregateWords()
    {
        return $this->hasMany(AggregateWord::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQueueOrder()
    {
        return $this->hasOne(QueueOrder::className(), ['order_id' => 'id']);
    }
}

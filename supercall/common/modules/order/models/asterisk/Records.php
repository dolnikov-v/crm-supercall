<?php

namespace common\modules\order\models\asterisk;

use common\modules\call\models\CallHistory;
use Yii;
use \yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\FileHelper;
use yii\httpclient\Client;
use common\modules\order\models\OrderLog;
use common\modules\order\models\Order;
use common\models\User;
use yii\db\Expression;

/**
 * This is the model class for table "{{%records}}".
 *
 * @property integer $id
 * @property string $phone
 * @property integer $datetime
 * @property string $path
 * @property boolean $saved
 */
class Records extends ActiveRecord
{
    const LOGGER_TYPE = 'asterisk';
    /**
     * Old
     */
    private static $secret_key = 'd11e051a205ed3efd3d8bfea69c36aca';
    private static $pid = 'd3efd3d8bfe';
    private static $host_record = 'http://m-call.in/api/method/Record';
    private static $host_remove = 'http://m-call.in/api/method/RemoveRecord';

    const RECORDS_PATH = 'records';
    const RECORD_FULL_PATH = '/upload/records';
    const ASTERISK_URL = 'http://mcall-aster2.rtbads.com/acdr/getrecord.php';
    const ASTERISK_DOWNLOAD_URL = 'http://mcall-aster2.rtbads.com/acdr/download.php?audio=/var/recording/in';
    const ASTERISK_USER = 'user1';
    const ASTERISK_PASSWORD = 'user1';
    const ASTERISK_FULL_DATA_URL = 'http://mcall-aster2.rtbads.com/acdr/getfullrecord.php';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%records}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'datetime', 'order_id', 'created_at', 'updated_at'], 'integer'],
            [['path'], 'string'],
            [['saved'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
            [['phone'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'datetime' => 'Datetime',
            'path' => 'Path',
            'saved' => 'Saved',
        ];
    }

    /**
     * Функция вернет массив ссылок для доступа к записям
     * OLD
     * @param array $phones
     * @return array
     */
    public static function getLinkByPhones(array $phones)
    {
        $data = [];
        $recordQuery = Records::find()->select(['id', 'saved'])->where(['in', 'phone', $phones]);
        $pathes = $recordQuery->exists() ? $recordQuery->all() : [];
        foreach ($pathes as $record) {
            $data[] = [
                'id' => $record->id,
                'saved' => $record->saved,
                'url' => self::getUrlFile($record->id),
            ];
        }

        return $data;
    }

    /**
     * @param $order
     * @return static[]
     */
    public static function getRecordsByOrder($order)
    {
        return Records::find()
            ->joinWith('user')
            ->where(['order_id' => $order->id])
            ->all();
    }

    /**
     * OLD
     * @param int $id
     * @return string
     */
    public static function getUrlFile($id)
    {
        return Url::to(['/order/record/file', 'id' => $id], true);
    }

    /**
     * Функция принимает путь до файла на сервере, и выкачивает его
     * OLD
     * @param $pathToFile
     * @return bool
     */
    public static function loading($pathToFile)
    {
        // вытащить запись
        $time = time();
        $token = md5(static::$secret_key . $time . static::$pid);
        $record_path = \Yii::getAlias('@console') . '/../../records/';

        $url = static::$host_record . '?token=' . $token . '&pid=' . static::$pid . '&time=' . $time . '&path=' . $pathToFile;
        $rmurl = static::$host_remove . '?token=' . $token . '&pid=' . static::$pid . '&time=' . $time . '&path=' . $pathToFile;

        $path = str_replace(basename($pathToFile), '', $pathToFile);

        if (!is_dir($record_path . $path)) {
            if (!mkdir($record_path . $path, 0777, true)) {
                return false;
            }
        }

        // Делает запись файла
        file_put_contents($record_path . $pathToFile, fopen($url, 'r'));
        if (file_exists($record_path . $pathToFile)) {
            if (Yii::$app->db->createCommand()->update('{{%records}}', ['saved' => true], [
                'path' => $pathToFile
            ])->execute()
            ) {
                return true;
                // удаляет по ссылке
                // file_get_contents($rmurl);
            }
        }
    }

    /**
     * @return bool|string
     */
    public static function getPathRecords()
    {
        return yii::getAlias('@backend/web/upload/' . self::RECORDS_PATH);
    }

    /**
     * @return string
     */
    public static function getAbsolutePathRecords()
    {
        return yii::$app->params['urlBackend'] . '/upload/' . self::RECORDS_PATH;
    }

    /**
     * Проверка наличия файлов для данного заказа
     * @param $phone
     * @param $order_id
     * @return \yii\db\ActiveQuery
     */
    public static function checkExistsFile($phone, $order_id)
    {
        return Records::find()->where(['phone' => $phone, 'order_id' => $order_id]);
    }

    /**
     * @param Order $order
     * @return []
     */
    public static function getDataByOrderLog(Order $order)
    {
        $query = Order::find()
            ->select([
                'user_id' => OrderLog::tableName() . '.user_id',
                'sip_login' => new Expression("sip::jsonb->>'sip_login'"),
            ])
            ->leftJoin(OrderLog::tableName(), OrderLog::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . OrderLog::tableName() . '.user_id')
            ->where(['order_id' => $order->id])
            ->andWhere(['is not', OrderLog::tableName() . '.user_id', null])
            ->groupBy('user_id, sip_login');

        $data = $query->asArray()->all();

        $phones = [];

        if (!empty($order->phones)) {
            $phones = array_merge($phones, $order->phones);
        }

        if (!empty($order->customer_phone)) {
            $phones = array_merge($phones, [$order->customer_phone]);
        }

        if (!empty($order->customer_mobile)) {
            $phones = array_merge($phones, [$order->customer_mobile]);
        }

        $dataLogs = [];

        if (is_array($data)) {
            foreach ($data as $k => $value) {
                $dataLogs[] = [
                    'user_id' => $value['user_id'],
                    'sip_login' => $value['sip_login'],
                    'phones' => $phones
                ];
            }
        }
        return $dataLogs;
    }


    /**
     * @param integer $sip_user
     * @param [] $phones
     * @return bool|mixed
     */
    public static function getDataFromAsterisk($sip_user, $phones)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(self::ASTERISK_URL)
            ->setHeaders(['Authorization' => 'Basic ' . base64_encode(self::ASTERISK_USER . ':' . self::ASTERISK_PASSWORD)])
            ->setData([
                'sip_user' => $sip_user,
                'phone' => $phones
            ])
            ->send();

        if (!$response->isOk) {
            return false;
        } else {
            $response = $response->content;
            return json_decode($response, true);
        }

    }

    /**
     * @param $phones
     * @return bool|mixed
     */
    public static function getFullDataFromAsterisk($phones)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(self::ASTERISK_FULL_DATA_URL)
            ->setHeaders(['Authorization' => 'Basic ' . base64_encode(self::ASTERISK_USER . ':' . self::ASTERISK_PASSWORD)])
            ->setData([
                'phone' => $phones
            ])
            ->send();

        if (!$response->isOk) {
            return false;
        } else {
            $response = $response->content;
            return json_decode($response, true);
        }

    }


    public static function downloadRecords($records, Order $order)
    {
        FileHelper::createDirectory(self::getPathRecords(), 0777, true);

        $logger = yii::$app->get('logger');
        $logger->action = Yii::$app->controller->action->id;

        if(method_exists(Yii::$app->getRequest(), 'getPathInfo')) {
            $logger->route = Yii::$app->getRequest()->getPathInfo();
        }else{
            $logger->route = 'console';
        }

        $logger->type = self::LOGGER_TYPE;

        if (!isset($records[0])) {
            $logger->tags = [
                'message' => '!isset($records[0])'
            ];
            $logger->save();
            return null;
        } else {
            $dataRecords = $records[0]['data'];
            $user_id = $records[0]['user_id'];

            if (!is_array($dataRecords)) {
                $logger->tags = [
                    'message' => '!is_array($dataRecords)'
                ];
                $logger->save();
                return;
            }

            $recordFiles = ArrayHelper::getColumn($dataRecords, 'recordingfile');
            $dates = ArrayHelper::getColumn($dataRecords, 'calldate');
            $client = new Client();

            foreach ($recordFiles as $num => $record) {
                $filename = basename($record);
                //fix record name
                $filename = strtr($filename, [':' => '_']);
                $phoneTemp = explode('-', $filename);
                $phone = $phoneTemp[0];

                if (!file_exists(self::getPathRecords() . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . $order->id . $filename)) {
                        $response = $client->createRequest()
                            ->setMethod('post')
                            ->setUrl(self::ASTERISK_DOWNLOAD_URL . '/' . $record)
                            ->setHeaders(['Authorization' => 'Basic ' . base64_encode(self::ASTERISK_USER . ':' . self::ASTERISK_PASSWORD)])
                            ->setOptions([
                                //CURLOPT_FILE => $file,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_HEADER => true
                            ])
                            ->send();

                        //если скачали
                        if ($response->isOk) {
                            if(method_exists(Yii::$app->getRequest(), 'getPathInfo')) {
                                $filename = strtr($filename, [':' => '_']);
                            }

                            //дубли записей нам не нужны
                            if (!FileHelper::createDirectory(self::getPathRecords() . DIRECTORY_SEPARATOR . $order->id, 0777, true)) {
                                $logger->error = 'not created dir: ' . self::getPathRecords() . DIRECTORY_SEPARATOR . $order->id;
                            }

                            if (file_put_contents(self::getPathRecords() . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $filename, $response->content)) {

                                if (!Records::find()->where([
                                    'phone' => $phone,
                                    'datetime' => strtotime($dates[$num]),
                                    'order_id' => $order->id,
                                    'user_id' => $user_id
                                ])->exists()
                                ) {
                                    //пишем в бд
                                    $model = new Records();
                                    $model->phone = $phone;
                                    $model->datetime = strtotime($dates[$num]);
                                    $model->path = self::getPathRecords() . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $filename;
                                    $model->saved = true;
                                    $model->order_id = $order->id;
                                    $model->user_id = $user_id;
                                    $model->created_at = time();
                                    $model->updated_at = time();
                                    /**
                                     * Не удалось сохранить, возможно логируем, но зачем?.
                                     */
                                    if (!$model->save()) {
                                        $model->validate();
                                        $logger->error = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
                                    } else {
                                        $message = 'file added: ' . self::getPathRecords() . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $filename;
                                    }
                                }
                            } else {
                                $logger->tags = [
                                    'target' => self::ASTERISK_DOWNLOAD_URL . '/' . $record
                                ];
                                $logger->error = 'not copied file: ' . self::getPathRecords() . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $filename;
                            }
                        } else {
                            $logger->error = json_encode($response->content, JSON_UNESCAPED_UNICODE);
                        }
                }
            }
        }

        $tags = [
            'order_id' => $order->id,
            'user_id' => $user_id,
            'headers' => isset($response) ? json_encode($response->getHeaders(), JSON_UNESCAPED_UNICODE) : 'not sended',
            'message' => isset($message) ? $message : null
        ];

        if(isset($logger->tags)){
            $logger->tags = array_merge($logger->tags, $tags);
        }
        else {
            $logger->tags = $tags;
        }
        $logger->save();
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param $phone
     * @param $order_id
     * @param $user_id
     * @return static[]
     */
    public static function getRecords($phone, $order_id, $user_id){
        return Records::findAll([
            'phone' => $phone,
            'order_id' => $order_id,
            'user_id' => $user_id
        ]);
    }

}

<?php

namespace common\modules\order\models;


use backend\modules\stats\models\report\OrderBuyoutReport;
use common\components\db\ActiveRecord;

/**
 * Class OrderBuyout
 * @package common\modules\order\models
 *
 * @property integer id
 * @property integer order_id
 * @property integer buyout
 * @property integer created_at
 * @property integer updated_at
 */
class OrderBuyout extends ActiveRecord
{
    const PROCESSING = 0;
    const BUYOUT = 1;
    const NO_BUYOUT = 2;

    /**
     * @var array
     *
     * Маппинг для респонсов от АПИ пея
     */
    public static $mappingBuyout = [
        self::PROCESSING => null,
        self::BUYOUT => 1,
        self::NO_BUYOUT => 0,
    ];

    public function table_name()
    {
        return '{{%order_buyout}}';
    }


    public function rules()
    {
        return [
            [['order_id', 'buyout'], 'required'],
            [['order_id', 'buyout', 'created_at', 'updated_at'], 'integer'],
        ];
    }


    public function setBuyoutFromAPIResponse($response)
    {

        $order = OrderBuyout::findOne(['order_id' => $response['order_id']]);
        if ($order) {
            $order->buyout = $this->mappingBuyoutTypeForAPI($response['buyout']);
            $order->save();
        }
    }

    public static function mappingBuyoutTypeForAPI($response)
    {
        return !array_search($response, self::$mappingBuyout) ? 0 : array_search($response, self::$mappingBuyout);
    }


}
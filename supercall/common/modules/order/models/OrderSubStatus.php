<?php

namespace common\modules\order\models;

use Yii;
use yii\db\ActiveRecord;
use common\modules\order\models\query\OrderSubStatusQuery;

/**
 * This is the model class for table "{{%order_substatus}}".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $code
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 */
class OrderSubStatus extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_substatus}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['group_id', 'created_at', 'updated_at'], 'integer'],
            [['code', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Индекс'),
            'group_id' => Yii::t('common', 'Группа'),
            'code' => Yii::t('common', 'Код'),
            'name' => Yii::t('common', 'Наименование'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return OrderSubStatusQuery
     */
    public static function find()
    {
        return new OrderSubStatusQuery(get_called_class());
    }

    /**
     * @param [] $group_id
     * @return static[]
     */
    public static function getSubStatusesByGroup($group_id)
    {
        return OrderSubStatus::find()
            ->where(['in', 'group_id', $group_id])
            ->all();
    }

}

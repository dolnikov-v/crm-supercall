<?php

namespace common\modules\order\models;

use common\components\db\ActiveRecord;
use common\models\Product;
use common\modules\order\models\query\OrderProductQuery;
use common\modules\partner\models\PartnerProduct;
use Yii;

/**
 * Class OrderProduct
 * @package common\modules\order\models
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property double $price
 * @property integer $quantity
 * @property double $cost
 * @property double $vat
 * @property boolean $gift
 * @property boolean $promo
 * @property boolean $fixed_price
 * @property string $created_at
 * @property string $updated_at
 * @property Order $order
 * @property Product $product
 * @property Product $amount
 * @property PartnerProduct $partnerProduct
 */
class OrderProduct extends ActiveRecord
{
    /** @var double */
    protected $_vat;

    /** @var  PartnerProduct */
    public $partner_product;

    /** @var  integer */
    public $amount;

    public $is_backup = false;

    public $promoProduct;

    public $promoProductSum;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_product}}';
    }

    /**
     * @return query\OrderProductQuery
     */
    public static function find()
    {
        return new OrderProductQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'quantity'], 'required'],
            [['order_id', 'product_id', 'quantity', 'package_id'], 'integer'],
            [['price', 'cost'], 'double', 'min' => 0],
            [['created_at', 'updated_at'], 'safe'],

            [['order_id', 'product_id', 'quantity'], 'filter', 'filter' => 'intval'],
            [['price'], 'filter', 'filter' => 'floatval'],
            [['gift'], 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'order_id' => Yii::t('common', 'Заказ'),
            'product_id' => Yii::t('common', 'Товар'),
            'price' => Yii::t('common', 'Цена'),
            'cost' => Yii::t('common', 'Стоимость'),
            'vat' => Yii::t('common', 'НДС'),
            'quantity' => Yii::t('common', 'Количество'),
            'gift' => Yii::t('common', 'Подарок'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'package_id' => Yii::t('common', 'Акция')
        ];
    }

    /**
     * @return double
     */
    public function getVat()
    {
        if (!$this->_vat) {
            if (!$this->partnerProduct) {
                return 0.0;
            }
            $this->_vat = $this->partnerProduct->getVat($this->order->country_id);
        }
        return $this->_vat;
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (empty($this->price)) {
            $this->price = 0.0;
        }

        if (!$this->is_backup) {
            $this->createGoodsLogs();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $orderLogProduct = new OrderProductLog();
        $orderLogProduct->product_id = $this->product_id;
        $orderLogProduct->order_id = $this->order_id;
        $orderLogProduct->user_id = (Yii::$app->user->id) ? Yii::$app->user->id : yii::$app->request->get('user_id');
        $orderLogProduct->action = OrderProductLog::ACTION_DELETE;
        $orderLogProduct->price = $this->price;
        $orderLogProduct->quantity = $this->quantity;
        $orderLogProduct->save();

        parent::afterDelete();
    }

    /**
     * Логи продуктов при создании и обновлении
     */
    private function createGoodsLogs()
    {
        $model = new OrderProductLog();
        $model->product_id = $this->product_id;
        $model->order_id = $this->order_id;
        $model->user_id = (Yii::$app->user->id) ? Yii::$app->user->id : method_exists(yii::$app->request, 'get') ? yii::$app->request->get('user_id') : '';
        $model->group_id = $this->order->group_id ? $this->order->group_id : $this->order->group;
        $model->operator = $this->order->operator;

        if (!$this->isNewRecord) {
            $model->action = OrderProductLog::ACTION_UPDATE;

            if (array_key_exists('price', $this->dirtyAttributes) || array_key_exists('quantity', $this->dirtyAttributes)) {
                $model->price = $this->price;
                $model->quantity = $this->quantity;
                $model->save();
            }
        } else {
            $model->action = OrderProductLog::ACTION_CREATE;
            $model->price = $this->price;
            $model->quantity = $this->quantity;
            $model->save();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerProduct()
    {
        return $this->hasOne(PartnerProduct::className(), ['product_id' => 'product_id'])->where(['partner_id' => $this->order->partner_id]);
    }

    /**
     * Возвращает массив количества товаров для select в форме Заказа
     *
     * @return array
     */
    public static function countList()
    {
        return [
            1 => 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        ];
    }

    /**
     * Исключит из набора orderProducts все promo товары
     * Вернёт список обычных, не акционных, товаров заказа
     * @param array $orderProducts
     * @return array
     */
    public static function selectProducts(array $orderProducts)
    {
        $onlyOrderProducts = [];

        // данные об promo product приходят из формы в ключах promoProduct и promoProductSum
        foreach ($orderProducts as $k => $product) {
            if (is_numeric($k)) {
                $onlyOrderProducts[] = $product;
            }
        }

        return $onlyOrderProducts;
    }

    /**
     * Вернёт только данные о promo товарах
     * @param array $orderProducts
     * @return array|mixed
     */
    public static function selectPromoProducts(array $orderProducts)
    {
        return isset($orderProducts['promoProduct']) ? $orderProducts['promoProduct'] : [];
    }

    /**
     * Сохранить promo Товары заказа
     * @param array $promoProducts
     * @param Order $order
     * @throws \Exception
     */
    public static function savePromoProducts(array $promoProducts, Order $order)
    {
        if (isset($promoProducts['product_id'])) {
            foreach ($promoProducts['product_id'] as $k => $orderPromoProduct) {
                if (!empty($orderPromoProduct)) {
                    $promoProduct = new OrderProduct();
                    $promoProduct->order_id = $order->id;
                    $promoProduct->gift = false;
                    $promoProduct->promo = true;
                    $promoProduct->fixed_price = true;
                    $promoProduct->quantity = $promoProducts['quantity'][$k];
                    $promoProduct->price = $promoProducts['price'][$k];
                    $promoProduct->cost = $promoProducts['price'][$k];
                    $promoProduct->product_id = $orderPromoProduct;

                    if (!$promoProduct->save()) {
                        throw new \Exception(Yii::t('common', $promoProduct->getFirstErrorAsString()));
                    }
                }
            }
        }
    }

}

<?php
namespace common\modules\order\models\query;

use common\components\db\ActiveQuery;
use common\modules\order\models\OrderType;

/**
 * Class OrderTypeQuery
 * @package common\modules\order\models\query
 *
 * @method OrderType one($db = null)
 * @method OrderType[] all($db = null)
 */
class OrderTypeQuery extends ActiveQuery
{

}

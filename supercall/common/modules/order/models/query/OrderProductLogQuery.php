<?php
namespace common\modules\order\models\query;

use common\modules\order\models\OrderProductLog;
use common\components\db\ActiveQuery;

/**
 * Class OrderProductLogQuery
 * @package common\modules\order\models\query
 * @method OrderProductLog one($db = null)
 * @method OrderProductLog[] all($db = null)
 */
class OrderProductLogQuery extends ActiveQuery
{

}

<?php

namespace common\modules\order\models\query;

use common\modules\order\models\OrderSubStatusGroup;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class OrderSubStatusGroupQuery
 * @package common\modules\order\models\query
 */
class OrderSubStatusGroupQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'name';

    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|\yii\db\ActiveRecord
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $groupId
     * @return $this
     */
    public function byGroup($groupId = false)
    {
        if (!$groupId) return $this;
        return $this->andWhere([OrderSubStatusGroup::tableName() .'.id' => $groupId]);
    }

    /**
     * @return array
     */
    public function collection()
    {
        return ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);
    }

}

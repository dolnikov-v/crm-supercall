<?php
namespace common\modules\order\models\query;

use common\components\db\ActiveQuery;
use common\modules\order\models\CountOrdersOnPhone;


/**
 * Class CountOrdersOnPhoneQuery
 * @package common\modules\order\models\query
 * @method CountOrdersOnPhone one($db = null)
 * @method CountOrdersOnPhone[] all($db = null)
 */
class CountOrdersOnPhoneQuery extends ActiveQuery
{
    /**
     * @param $phones array|string
     * @return $this
     */
    public function byPhones($phones)
    {
        $phones = is_array($phones) ? $phones : [$phones];
        return $this->andWhere(['in', CountOrdersOnPhone::tableName() . '.phone', $phones]);
    }
}
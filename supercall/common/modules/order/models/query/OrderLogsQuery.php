<?php

namespace common\modules\order\models\query;


use common\modules\order\models\OrderLogs;
use common\components\db\ActiveQuery;

/**
 * Class OrderLogsQuery
 * @package common\modules\order\models\query
 *
 * @method OrderLogs one($db = null)
 * @method OrderLogs[] all($db = null)
 */
class OrderLogsQuery extends ActiveQuery
{
    
}

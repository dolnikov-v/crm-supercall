<?php
namespace common\modules\order\models\query;

use common\modules\order\models\OrderProduct;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class OrderProductQuery
 * @package common\modules\order\models\query
 * @method OrderProduct one($db = null)
 * @method OrderProduct[] all($db = null)
 */
class OrderProductQuery extends ActiveQuery
{
    /**
     * @param $orderId
     * @return $this
     */
    public function byOrderId($orderId)
    {
        return $this->andWhere(['order_id' => $orderId]);
    }

    /**
     * @return $this
     */
    public function selectTotal()
    {
        return $this->select(['total' => new Expression('SUM(price * quantity)')])->groupBy('order_id');
    }
}

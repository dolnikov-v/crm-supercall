<?php
namespace common\modules\order\models\query;

use common\modules\order\models\CountOrdersData;
use common\components\db\ActiveQuery;
use common\modules\order\models\CountOrdersOnPhone;

/**
 * Class CountOrdersDataQuery
 * @package common\modules\order\models\query
 * @method CountOrdersData one($db = null)
 * @method CountOrdersData[] all($db = null)
 */
class CountOrdersDataQuery extends ActiveQuery
{
    /**
     * @param $order_ids array|integer
     * @return $this
     */
    public function byOrderId($order_ids)
    {
        $order_ids = is_array($order_ids) ? $order_ids : [$order_ids];
        return $this->andWhere(['in', CountOrdersData::tableName() . '.order_id', $order_ids]);
    }

    /**
     * @param integer $count_orders_on_phone_id
     * @return $this
     */
    public function byCountOrdersOnPhoneId($count_orders_on_phone_id)
    {
        return $this->andWhere([CountOrdersOnPhone::tableName() . '.count_orders_on_phone_id', $count_orders_on_phone_id]);
    }
}
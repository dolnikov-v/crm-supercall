<?php

namespace common\modules\order\models\query;

use yii\db\ActiveQuery;

/**
 * Class OrderSubStatusQuery
 * @package app\common\modules\order\models\query
 */
class OrderSubStatusQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'name';

    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|\yii\db\ActiveRecord
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


}

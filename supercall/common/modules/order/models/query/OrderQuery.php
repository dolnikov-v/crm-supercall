<?php

namespace common\modules\order\models\query;

use common\modules\order\models\Order;
use common\components\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class OrderQuery
 * @package common\modules\order\models\query
 * @method Order one($db = null)
 * @method Order[] all($db = null)
 */
class OrderQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionValue = 'id';

    public $id;

    /**
     * @param $id
     * @return $this
     */
    public function byId($id)
    {
        $primary_id = array_map('trim', explode(',', $id));

        foreach ($primary_id as $key=>$value){
            $primary_id[$key] = (int)$value;
        }

        return $this->andWhere([Order::tableName() . '.id' => $primary_id]);
    }

    /**
     * @param $partnerId
     * @return $this
     */
    public function byPartnerId($partnerId)
    {
        return $this->andWhere([Order::tableName() . '.partner_id' => $partnerId]);
    }

    /**
     * @param $typeId
     * @return $this
     */
    public function byTypeId($typeId)
    {
        return $this->andWhere([Order::tableName() . '.type_id' => $typeId]);
    }

    /**
     * Фильтр для списка заказов
     * по массиву ID партнеров
     *
     * @param array $partnerIds
     * @return $this
     */
    public function byPartnerIds(array $partnerIds)
    {
        return $this->andWhere(['in', Order::tableName() . '.partner_id', $partnerIds]);
    }

    /**
     * @param $foreignId
     * @return $this
     */
    public function byForeignId($foreignId)
    {
        $foreign_id = array_map('trim', explode(',', $foreignId));

        foreach ($foreign_id as $key=>$value){
            $foreign_id[$key] = (int)$value;
        }

        return $this->andWhere([Order::tableName() . '.foreign_id' => $foreign_id]);
    }

    /**
     * @param array $foreignIds
     * @return $this
     */
    public function byForeignIds($foreignIds)
    {
        return $this->andWhere(['in', Order::tableName() . '.foreign_id', $foreignIds]);
    }

    /**
     * @param array $internalIds
     * @return $this
     */
    public function byInternalIds($internalIds)
    {
        return $this->andWhere(['in', Order::tableName() . '.id', $internalIds]);
    }

    /**
     * @param integer $countryId
     * @return $this
     */
    public function byCountryId($countryId)
    {
        return $this->andWhere([Order::tableName() . '.country_id' => $countryId]);
    }

    /**
     * @param array $countryIds
     * @return $this
     */
    public function byCountryIds($countryIds)
    {
        return $this->andWhere(['in', Order::tableName() . '.country_id', $countryIds]);
    }

    /**
     * @param integer $status
     * @return $this
     */
    public function byStatus($status)
    {
        return $this->andWhere([Order::tableName() . '.status' => $status]);
    }

    /**
     * @param integer $last_queue_id
     * @return $this
     */
    public function byLastQueueId($last_queue_id)
    {
        return $this->andWhere([Order::tableName() . '.last_queue_id' => $last_queue_id]);
    }

    /**
     * Если необходимо наполнить конкретную очередь
     * согласно приоритету выборки
     * $type:
     * 1- НОВЫЕ - только самые свеживе заказы
     * 2- СТАРЫЕ - сначала самые старые заказы
     * 3- ПО ПРОЗВОНУ - сначала заказы с наименьшим кол-ом попыток, а затем приоритет самые новые
     *
     * @param int $type
     * @return $this
     */
    public function byPrioritySelection(int $type)
    {
        switch ($type) {
            // НОВЫЕ
            case 1:
                $caseWhen = 'CASE WHEN ' . Order::tableName() . '."status" = ' . Order::STATUS_RECALL . ' THEN '
                    . Order::tableName() . '."blocked_to" ELSE '
                    . Order::tableName() . '."partner_created_at" END';
                $this->orderBy([$caseWhen => SORT_DESC]);
                break;
            // СТАРЫЕ
            case 2:
                $this->addOrderBy([Order::tableName() . '.partner_created_at' => SORT_ASC]);
                break;
            // ПО ПРОЗВОНУ
            case 3:
                $this->addOrderBy([
                    Order::tableName() . '.attempts' => SORT_ASC,
                    Order::tableName() . '.partner_created_at' => SORT_DESC,
                ]);
                break;
        }

        return $this;
    }

    public function byDateFrom($date, $column = 'created_at')
    {
        return $this->andWhere(['>=', Order::tableName() . '.' . $column, $date]);
    }

    public function byDateTill($date, $column = 'updated_at')
    {
        return $this->andWhere(['<=', Order::tableName() . '.' . $column, $date]);
    }
}

<?php
namespace common\modules\order\models\query;

use common\modules\order\models\OrderLog;
use common\components\db\ActiveQuery;

/**
 * Class OrderLogQuery
 * @package common\modules\order\models\query
 *
 * @method OrderLog one($db = null)
 * @method OrderLog[] all($db = null)
 */
class OrderLogQuery extends ActiveQuery
{

}

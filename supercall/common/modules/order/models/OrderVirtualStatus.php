<?php

namespace common\modules\order\models;

use common\components\db\ActiveRecord;

/**
 * Class OrderVirtualStatus
 * @package common\modules\order\models
 */
class OrderVirtualStatus extends ActiveRecord
{
    /**
     * @return array
     */
    public static function getCheckShippingStatusCollection()
    {
        $labels = Order::getCheckShippingSubStatusesCollection();

        return [
            Order::STATUS_APPROVED => [
                Order::SUB_STATUS_REJECT_STILL_WAITING => $labels[Order::SUB_STATUS_REJECT_STILL_WAITING],
            ],
            Order::STATUS_REJECTED => [
                Order::SUB_STATUS_ALREADY_RECEIVED => $labels[Order::SUB_STATUS_ALREADY_RECEIVED],
                Order::SUB_STATUS_REJECT_NO_LONGER_NEEDED => $labels[Order::SUB_STATUS_REJECT_NO_LONGER_NEEDED],
                Order::SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER => $labels[Order::SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER],
            ]
        ];
    }

    /**
     * Поиск статуса по виртуальному сабстатусу
     * @param $virtual_sub_sub_status
     * @return bool|int|string
     */
    public static function setStatusByVirtualSubStatus($virtual_sub_sub_status)
    {
        $collection = self::getCheckShippingStatusCollection();

        foreach ($collection as $status => $sub_status_info) {
            foreach ($sub_status_info as $sub_status => $label) {
                if ($virtual_sub_sub_status == $sub_status) {
                    $founded_status = $status;
                    break;
                }
            }
        }

        return isset($founded_status) ? $founded_status : false;
    }
}
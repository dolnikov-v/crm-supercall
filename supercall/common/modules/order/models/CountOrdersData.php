<?php
namespace common\modules\order\models;

use common\components\db\ActiveRecord;
use common\modules\order\models\CountOrdersOnPhone;
use common\modules\order\models\query\CountOrdersDataQuery;
use Yii;

/**
 * Class CountOrdersData
 * @package common\modules\order\models
 */
class CountOrdersData extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%count_orders_data}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['count_orders_on_phone_id', 'order_id'], 'required'],
            [['count_orders_on_phone_id', 'order_id', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'count_orders_on_phone_id' => Yii::t('common', 'Ссылка'),
            'order_id' => Yii::t('common', 'Заказ'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return CountOrdersDataQuery
     */
    public static function find()
    {
        return new CountOrdersDataQuery(get_called_class());
    }

    /**
     * @param integer $count_order_on_phone_id
     * @param integer $order_id
     * @return bool|CountOrdersData
     */
    public static function insertCountOrdersData($count_order_on_phone_id, $order_id)
    {
        $model = CountOrdersData::find()
            ->byOrderId($order_id)
            ->andWhere(['count_orders_on_phone_id' => $count_order_on_phone_id])
            ->one();

        if(!$model){
            $model = new CountOrdersData();
            $model->count_orders_on_phone_id = $count_order_on_phone_id;
            $model->order_id = $order_id;
            $model->save();
        }

        return $model;
    }
}
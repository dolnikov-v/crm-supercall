<?php

namespace common\modules\order\models;

use backend\modules\queue\models\Queue;
use common\components\db\ActiveRecord;
use common\models\User;
use common\modules\order\models\query\OrderLogQuery;
use Yii;

/**
 * Class OrderLog
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $group_id
 * @property string $field
 * @property string $old
 * @property string $new
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 * @property integer $queue_id
 * @property string $operator
 * @property string $comment
 *
 * @property Order $order
 * @property User $user
 * @property OrderProductLog[] $productLogs
 */
class OrderLog extends ActiveRecord
{
    const CUSTOMER_PHONE_COLUMN = 'customer_phone';
    const CUSTOMER_MOBILE_COLUMN = 'customer_mobile';
    const ORDER_PHONES_COLUMN = 'order_phones';
    const CUSTOMER_COMPONENTS_COLUMN = 'customer_components';
    /**
     * for quality control
     */
    public $username;
    public $call_date;
    public $record;
    public $action_address_time;
    public $address;
    public $phone;
    public $order_url;
    public $status;

    public $partnerName;
    public $foreign_id;
    public $country;

    public $call_history_id;

    public $total;
    public $group;
    public $status_quantity_1;
    public $status_quantity_2;
    public $status_quantity_3;
    public $status_quantity_4;
    public $status_quantity_5;
    public $status_quantity_6;
    public $status_quantity_7;
    public $status_quantity_8;
    public $midcheck;
    public $online;
    public $buyout;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_log}}';
    }

    /**
     * @return OrderLogQuery
     */
    public static function find()
    {
        return new OrderLogQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'group_id', 'field'], 'required'],
            [['order_id', 'created_at', 'updated_at', 'queue_id', 'user_id'], 'integer'],
            ['operator', 'string'],
            [['field', 'old', 'new'], 'string', 'max' => 255],
            [['comment'], 'string', 'max' => 1000],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => 'id'],
            [['queue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Queue::className(), 'targetAttribute' => ['queue_id' => 'id']],
        ];
    }

    /**
     *  По возможности переписать на циклы (цикл в цикле)
     */
    public function afterFind()
    {
        if (in_array($this->field, [self::CUSTOMER_MOBILE_COLUMN, self::CUSTOMER_PHONE_COLUMN, self:: ORDER_PHONES_COLUMN])) {
            if (!Yii::$app->user->can('order.index.viewcontacts')) {
                $this->old = empty($this->old) ? $this->old : substr_replace($this->old, 'XXXX', -4);
                $this->new = empty($this->new) ? $this->new : substr_replace($this->new, 'XXXX', -4);
            }
        }
        if ($this->field == self::CUSTOMER_COMPONENTS_COLUMN) {
            if (!Yii::$app->user->can('order.index.viewcontacts')) {
                $temp_old = json_decode($this->old, true);

                if (isset($temp_old['general'])) {
                    if (isset($temp_old['general'][self::CUSTOMER_PHONE_COLUMN])) {
                        $temp_old['general'][self::CUSTOMER_PHONE_COLUMN] = empty($temp_old['general'][self::CUSTOMER_PHONE_COLUMN])
                            ? $temp_old['general'][self::CUSTOMER_PHONE_COLUMN]
                            : substr_replace($temp_old['general'][self::CUSTOMER_PHONE_COLUMN], 'XXXX', -4);

                        $this->old = json_encode($temp_old, JSON_UNESCAPED_UNICODE);
                    }

                    if (isset($temp_old['general'][self::CUSTOMER_MOBILE_COLUMN])) {
                        $temp_old['general'][self::CUSTOMER_MOBILE_COLUMN] = empty($temp_old['general'][self::CUSTOMER_MOBILE_COLUMN])
                            ? $temp_old['general'][self::CUSTOMER_MOBILE_COLUMN]
                            : substr_replace($temp_old['general'][self::CUSTOMER_MOBILE_COLUMN], 'XXXX', -4);

                        $this->old = json_encode($temp_old, JSON_UNESCAPED_UNICODE);
                    }
                }

                $temp_new = json_decode($this->new, true);

                if (isset($temp_new['general'])) {
                    if (isset($temp_new['general'][self::CUSTOMER_PHONE_COLUMN])) {
                        $temp_new['general'][self::CUSTOMER_PHONE_COLUMN] = empty($temp_new['general'][self::CUSTOMER_PHONE_COLUMN])
                            ? $temp_new['general'][self::CUSTOMER_PHONE_COLUMN]
                            : substr_replace($temp_new['general'][self::CUSTOMER_PHONE_COLUMN], 'XXXX', -4);

                        $this->new = json_encode($temp_new, JSON_UNESCAPED_UNICODE);
                    }

                    if (isset($temp_new['general'][self::CUSTOMER_MOBILE_COLUMN])) {
                        $temp_new['general'][self::CUSTOMER_MOBILE_COLUMN] = empty($temp_new['general'][self::CUSTOMER_MOBILE_COLUMN])
                            ? $temp_new['general'][self::CUSTOMER_MOBILE_COLUMN]
                            : substr_replace($temp_new['general'][self::CUSTOMER_MOBILE_COLUMN], 'XXXX', -4);

                        $this->new = json_encode($temp_new, JSON_UNESCAPED_UNICODE);
                    }
                }
            }
        }


        parent::afterFind();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Заказ'),
            'group_id' => Yii::t('common', 'Группа'),
            'field' => Yii::t('common', 'Поле'),
            'old' => Yii::t('common', 'Старое значение'),
            'new' => Yii::t('common', 'Новое значение'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'queue_id' => Yii::t('common', 'Очередь'),
            'comment' => Yii::t('common', 'Комментарий'),
            'operatorName' => Yii::t('common', 'Оператор'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductLogs()
    {
        return $this->hasMany(OrderProductLog::className(), ['group_id' => 'group_id']);
    }

    /**
     * Вернет либо Имя оператора, либо Username пользователя-администратора unicall
     * в завистимости от того, кто вносил изменения
     * @return string
     */
    public function getOperatorName()
    {
        if ($this->operator) {
            return $this->operator;
        } elseif ($this->user_id) {
            return $this->user->username;
        }
    }
}

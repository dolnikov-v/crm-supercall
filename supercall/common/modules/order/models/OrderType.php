<?php
namespace common\modules\order\models;

use common\components\db\ActiveRecord;
use common\modules\order\models\query\OrderTypeQuery;
use common\modules\partner\models\PartnerSettings;
use Yii;

/**
 * Class OrderType
 * @property integer $id
 * @property string $name
 */
class OrderType extends ActiveRecord
{
    //TODO переделать либо под БД либо убрать из БД типы ордеров
    //TODO а то получается, что два раза делаем одно и тоже
    /**
     * Новый
     */
    const TYPE_NEW = 1;

    /**
     * Созданный
     */
    const TYPE_CREATED = 2;

    /***
     * Доставленный
     */
    const TYPE_DELIVERED = 3;

    /**
     * Не выкуп
     */
    const TYPE_NON_PURCHASE = 4;
    const TYPE_NON_BUYOUT = 5;
    const TYPE_NEW_RETURN = 6;
    const TYPE_ORDER_RETURN = 7;
    const TYPE_ORDER_ANALYSIS_TRASH = 8;
    const TYPE_ORDER_STILL_WAITING = 9;
    const TYPE_RETURN_NO_PROD = 10;
    const TYPE_2WSTORE = 15;

    //TODO не использовать
    const TYPE_CHECK_UNDELIVERY = 4;
    const TYPE_CHECK_ADDRESS = 12;

    const TYPE_OLD_CRM = 13;
    const TYPE_POST_SALE = 14;

    const TYPE_INFORMATION = 16;
    const TYPE_TOPHOT = 17;

    //совпадает с нашим типом 1
    const TYPE_CC_ORDER = 'order';
    const TYPE_CC_NEW_RETURN = 'new_return';
    const TYPE_CC_ORDER_RETURN = 'return';
    const TYPE_CC_ORDER_ANALYSIS_TRASH = 'trash_check';
    const TYPE_CC_ORDER_STILL_WAITING = 'still_waiting';
    const TYPE_CC_RETURN_NO_PROD = 'return_no_prod';
    //совпадает с нашим типом 4
    const TYPE_CC_CHECK_UNDELIVERY = 'check_undelivery';
    //совпадает с нашим типом 3
    const TYPE_CC_CHECK_DELIVERY = 'check_delivery';
    const TYPE_CC_CHECK_ADDRESS = 'check_address';
    //Для заазов со старого КЦ
    const TYPE_CC_OLD_CRM = 'old_crm';
    const TYPE_CC_POST_SALE = 'post_sale';

    const TYPE_CC_2WSTORE = '2wstore';
    const TYPE_CC_TOPHOT = 'tophot';

    const TYPE_CC_INFORMATION = 'information';

    //типы маркетплейсов
    const TYPE_CC_MERCADOLIBRE = 'mercadolibre';
    const TYPE_MERCADOLIBRE = 18;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_type}}';
    }

    /**
     * Map с типами пея
     * данные приходят в type - в виде строки
     * @return array
     */
    public static function mapType()
    {
        return [
            //совпадает с нашим типом 1
            self::TYPE_CC_ORDER => self::TYPE_NEW,
            self::TYPE_CC_NEW_RETURN => self::TYPE_NEW_RETURN,
            self::TYPE_CC_ORDER_RETURN => self::TYPE_ORDER_RETURN,
            self::TYPE_CC_ORDER_ANALYSIS_TRASH => self::TYPE_ORDER_ANALYSIS_TRASH,
            self::TYPE_CC_ORDER_STILL_WAITING => self::TYPE_ORDER_STILL_WAITING,
            self::TYPE_CC_RETURN_NO_PROD => self::TYPE_RETURN_NO_PROD,
            //совпадает с нашим типом 4
            self::TYPE_CC_CHECK_UNDELIVERY => self::TYPE_NON_PURCHASE,
            //совпадает с нашим типом 3
            self::TYPE_CC_CHECK_DELIVERY => self::TYPE_DELIVERED,
            self::TYPE_CC_CHECK_ADDRESS => self::TYPE_CHECK_ADDRESS,
            self::TYPE_CC_OLD_CRM => self::TYPE_OLD_CRM,
            self::TYPE_CC_POST_SALE => self::TYPE_POST_SALE,
            self::TYPE_CC_2WSTORE => self::TYPE_2WSTORE,
            self::TYPE_CC_INFORMATION => self::TYPE_INFORMATION,
            self::TYPE_CC_TOPHOT => self::TYPE_TOPHOT,
            //маркетплейсы
            self::TYPE_CC_MERCADOLIBRE => self::TYPE_MERCADOLIBRE
        ];
    }

    /**
     * @param $type
     * @return bool|mixed
     */
    public static function getMappedType($type)
    {
        $types = self::mapType();

        return isset($types[$type]) ? $types[$type] : false;
    }

    /**
     * @param $type
     * @return bool
     */
    public static function getReMappedType($type)
    {
        $types = array_flip(self::mapType());

        return isset($types[$type]) ? $types[$type] : false;
    }

    /**
     * @return OrderTypeQuery
     */
    public static function find()
    {
        return new OrderTypeQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
        ];
    }

    public function getPartnerSettings()
    {
        return $this->hasMany(PartnerSettings::className(), ['type_id' => 'id']);
    }
}

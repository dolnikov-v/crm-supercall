<?php
namespace common\modules\order\models;

use common\components\db\ActiveRecord;

/**
 * Class OrderGenesys
 * @property integer $order_id
 * @property integer $genesys_id
 * @property integer $index
 * @property string $phone
 */
class OrderGenesys extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_genesys}}';
    }
}

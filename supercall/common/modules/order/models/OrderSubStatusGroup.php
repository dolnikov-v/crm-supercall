<?php

namespace common\modules\order\models;

use Yii;
use yii\db\ActiveRecord;
use common\modules\order\models\query\OrderSubStatusGroupQuery;

/**
 * This is the model class for table "{{%order_substatus_group}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 */
class OrderSubStatusGroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_substatus_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Индекс'),
            'name' => Yii::t('common', 'Наименование'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return OrderSubStatusGroupQuery
     */
    public static function find()
    {
        return new OrderSubStatusGroupQuery(get_called_class());
    }

}

<?php

namespace common\modules\order\models;

use common\components\db\ActiveRecord;
use common\components\Sms;

/**
 * @property integer $id
 * @property integer $order_id
 * @property string $type
 * @property string $operator
 * @property string $to
 * @property string $text
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order $order
 */
class OrderMessage extends ActiveRecord
{
    const TYPE_SMS = 'sms';
    const TYPE_EMAIL = 'email';

    const STATUS_WAITING = 'waiting';
    const STATUS_SENDING = 'sending';
    const STATUS_SENT = 'sent';
    const STATUS_ERROR = 'error';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_message}}';
    }

    public function rules()
    {
        return [
            ['order_id', 'exist', 'targetClass' => Order::className(), 'targetAttribute' => 'id'],
            ['type', 'in', 'range' => self::types()],
            [['operator', 'to', 'text'], 'string'],
            ['status', 'in', 'range' => self::statuses()],
            ['type', 'in', 'range' => self::types()]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return string[]
     */
    public static function types()
    {
        return [
            self::TYPE_SMS,
            self::TYPE_EMAIL
        ];
    }

    /**
     * @return string[]
     */
    public static function statuses()
    {
        return [
            self::STATUS_WAITING,
            self::STATUS_SENDING,
            self::STATUS_SENT,
            self::STATUS_ERROR
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert){
            $this->send();
        }
    }

    /**
     * Отправка сообщения, если статус отправлено - повторная отправка не производится
     */
    public function send()
    {
        //не отправлять повторно
        if ($this->status == self::STATUS_SENT){
            return;
        }

        //не отправлять сообщения которые уже отправляются
        if ($this->status == self::STATUS_SENDING){
            return;
        }

        $this->status = self::STATUS_SENDING;
        $this->save(false);
        try{
            if ($this->type == self::TYPE_EMAIL){
                $mailer = \Yii::$app->mailer;
                $mailer->compose()
                    ->setTo($this->to)
                    ->setSubject(\Yii::t('common', 'Сообщение от 2wcall.com'))
                    ->setHtmlBody($this->text)
                    ->send();

            }
            if ($this->type == self::TYPE_SMS){
                /** @var Sms $sms */
                $sms = \Yii::$app->sms;
                $sms->send($this->to, $this->text);
            }
            $this->status = self::STATUS_SENT;
            $this->save(false);
        }
        catch (\Exception $e){
            $this->status = self::STATUS_ERROR;
            $this->save(false);
            throw $e;
        }
    }
}
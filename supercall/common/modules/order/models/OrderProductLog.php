<?php

namespace common\modules\order\models;

use common\components\db\ActiveRecord;
use common\models\Product;
use common\models\User;
use common\modules\order\models\query\OrderProductLogQuery;
use Yii;

/**
 * Class OrderProductLog
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property string $action
 * @property string $price
 * @property string $quantity
 * @property string $created_at
 * @property integer $updated_at
 * @property integer $user_id
 * @property string $group_id
 * @property string $operator
 *
 * @property Product $product
 * @property Order $order
 * @property User $user
 */
class OrderProductLog extends ActiveRecord
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    /**
     * @return array
     */
    public static function getProductActions()
    {
        return [
            self::ACTION_CREATE => Yii::t('common', 'Создано'),
            self::ACTION_UPDATE => Yii::t('common', 'Обновлено'),
            self::ACTION_DELETE => Yii::t('common', 'Удалено'),
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_product_log}}';
    }

    /**
     * @return OrderProductLogQuery
     */
    public static function find()
    {
        return new OrderProductLogQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id'], 'required'],
            [['order_id', 'product_id', 'price', 'quantity', 'created_at', 'updated_at'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_id' => Yii::t('common', 'Заказ'),
            'product_id' => Yii::t('common', 'Товар'),
            'action' => Yii::t('common', 'Действие'),
            'price' => Yii::t('common', 'Цена'),
            'quantity' => Yii::t('common', 'Количество'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

?>

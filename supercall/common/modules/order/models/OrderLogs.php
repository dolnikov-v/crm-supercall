<?php

namespace common\modules\order\models;

use backend\modules\queue\models\Queue;
use common\components\db\ActiveRecord;
use common\models\Product;
use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%order_logs}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $old
 * @property integer $new
 * @property string $comment
 * @property integer $queue_id
 * @property integer $user_id
 * @property string $operator
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $fields
 * @property string $product
 *
 * @property Order $order
 * @property Queue $queue
 * @property User $user
 */
class OrderLogs extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_logs}}';
    }
    
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'old', 'new', 'queue_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['comment', 'fields', 'product'], 'string'],
            [['operator'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['queue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Queue::className(), 'targetAttribute' => ['queue_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('common', 'ID'),
            'order_id'   => Yii::t('common', 'Заказ'),
            'old'        => Yii::t('common', 'Старое значение'),
            'new'        => Yii::t('common', 'Новое значение'),
            'comment'    => Yii::t('common', 'Comment'),
            'queue_id'   => Yii::t('common', 'Очередь'),
            'user_id'    => Yii::t('common', 'Пользователь'),
            'operator'   => Yii::t('common', 'Оператор'),
            'operatorName'=> Yii::t('common', 'Оператор'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'fields'     => Yii::t('common', 'Поля'),
            'product'    => Yii::t('common', 'Товары'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\order\models\query\OrderLogsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\order\models\query\OrderLogsQuery(get_called_class());
    }
    
    /**
     * Вернет либо Имя оператора, либо Username пользователя-администратора unicall
     * в завистимости от того, кто вносил изменения
     * @return string
     */
    public function getOperatorName()
    {
        if ($this->operator) {
            return $this->operator;
        }
        elseif ($this->user_id) {
            return $this->user->username;
        }
    }

    /**
     * Выводит в строку продукты (с количеством)
     * @return string
     */
    public function getProductString()
    {
        $products = [];
        foreach (Json::decode($this->product) as $product) {
            $product_model = Product::find()->where(['id' => $product['product_id']])->one();
            $products[] = $product_model->name . ($product['quantity'] > 1 ? ' (' . $product['quantity'] . ')' : null);
        }

        return implode(', ', $products);
    }
}

<?php

namespace common\modules\order\models;


use common\components\db\ModelTrait;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * Class NoAnswerNotificationRequest
 * @package common\modules\order\models
 * @property integer $order_id
 * @property string $group_id
 * @property string $status
 * @property string $phone
 * @property array $response
 * @property integer $sent_at
 * @property integer $created_at
 *
 * @property Order $order
 */
class NoAnswerNotificationRequest extends ActiveRecord
{
    use ModelTrait;

    const STATUS_SENT = 'sent';
    const STATUS_PENDING = 'pending';
    const STATUS_FAIL = 'fail';
    const STATUS_WAIT_INFORMATION_FROM_ASTERISK = 'wait_asterisk';
    const STATUS_CANT_SENT = 'cant_sent';
    const STATUS_ERROR = 'error';

    const LOGGER_TYPE = 'no_answer_notif_request';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
            'responseEncode' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['response'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['response'],
                ],
                'value' => function () {
                    return json_encode($this->response);
                }
            ],
            'responseDecode' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_AFTER_FIND => ['response'],
                ],
                'value' => function () {
                    return json_decode($this->response, true);
                }
            ],
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return "{{%no_answer_notification_request}}";
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'group_id'], 'required'],
            [['order_id', 'created_at', 'sent_at'], 'integer'],
            [['group_id', 'status', 'phone'], 'string'],
            ['response', 'safe'],
            ['status', 'default', 'value' => self::STATUS_PENDING],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'order_id' => \Yii::t('common', 'Номер заказа'),
            'group_id' => \Yii::t('common', 'Группа звонков'),
            'phone' => \Yii::t('common', 'Номер телефона'),
            'status' => \Yii::t('common', 'Статус'),
            'response' => \Yii::t('common', 'Ответ'),
            'created_at' => \Yii::t('common', 'Дата создания'),
            'sent_at' => \Yii::t('common', 'Дата отправки'),
        ];
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_SENT => \Yii::t('common', 'Отправлено'),
            self::STATUS_PENDING => \Yii::t('common', 'Ожидает'),
            self::STATUS_FAIL => \Yii::t('common', 'Ошибка при отправке'),
            self::STATUS_WAIT_INFORMATION_FROM_ASTERISK => \Yii::t('common', 'В ожидании информации о статусе звонка'),
            self::STATUS_CANT_SENT => \Yii::t('common', 'Не стоит отправлять'),
            self::STATUS_ERROR => \Yii::t('common', 'Ошибка'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $status = parent::save($runValidation, $attributeNames);
        if (!$status) {
            $logger = Yii::$app->get('logger');
            $logger->action = Yii::$app->controller->action->id;
            $logger->route = Yii::$app->controller->route;
            $logger->type = NoAnswerNotificationRequest::LOGGER_TYPE;
            $logger->tags = [
                'order_id' => $this->order_id,
                'group_id' => $this->group_id,
            ];
            $logger->error = $this->getFirstErrorAsString();
            $logger->save();
        }

        return $status;
    }
}
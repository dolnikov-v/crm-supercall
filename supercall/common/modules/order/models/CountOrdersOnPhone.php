<?php
namespace common\modules\order\models;

use common\components\db\ActiveRecord;
use common\modules\order\models\query\CountOrdersOnPhoneQuery;
use common\modules\order\models\CountOrdersData;
use Yii;

/**
 * Class CountOrdersOnPhone
 * @package common\modules\order
 * @property integer $id
 * @property string $phone
 * @property integer $count_orders
 * @property integer $created_at
 * @property integer $updated_at
 */
class CountOrdersOnPhone extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%count_orders_on_phone}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['phone', 'count_orders'], 'required'],
            ['phone', 'string'],
            [['count_orders', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'phone' => Yii::t('common', 'Телефон'),
            'count_orders' => Yii::t('common', 'Количество'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @return CountOrdersOnPhoneQuery
     */
    public static function find()
    {
        return new CountOrdersOnPhoneQuery(get_called_class());
    }

    /**
     * @param $phone
     * @return CountOrdersOnPhone
     */
    public static function updatePhoneCount($phone, $order_id)
    {
        $model = CountOrdersOnPhone::find()->where(['phone' => $phone])->one();

        if (!is_null($model)) {
            $model->count_orders += 1;
        } else {
            $model = new CountOrdersOnPhone();
            $model->phone = $phone;
            $model->count_orders = 1;
        }

        if($model->save()){
            CountOrdersData::insertCountOrdersData($model->id, $order_id);
        }

        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountOrdersData()
    {
        return $this->hasMany(CountOrdersData::className(), ['id' => 'count_orders_on_phone_id']);
    }

}
<?php

namespace common\modules\order\models\genesys;

use common\components\db\ActiveRecord;

/**
 * @property string $CALLID
 * @property string $CALL_START
 * @property string $DNIS
 * @property string $CPD_RESULT
 * @property string $CPD_RESULT_DESC
 * @property string $CALL_RESULT
 * @property string $CALL_RESULT_DESC
 * @property string $PRODUCT_LANGUAGE
 * @property string $PRODUCT_ID
 * @property string $CUSTOMER_NAME
 * @property string $LAST_ATTEMPT
 * @property string $TIMEZONE
 * @property string $CALL_DURATION
 * @property string $TIME_ALERTING
 * @property string $TIME_CONNECTED
 * @property string $TIME_IN_QUEUE
 * @property string $TIME_ON_HOLD
 * @property string $RECORD
 */
class Call extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%genesys_call}}';
    }
}
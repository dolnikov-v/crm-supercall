<?php
namespace common\modules\order\widgets;

use common\modules\order\models\Order;
use yii\i18n\Formatter;
use common\widgets\base\PanelGroup;
use common\widgets\base\PanelTab;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * Class PanelGroupLogs
 * @package common\modules\order\widgets
 */
class PanelGroupLogs extends PanelGroup
{
    public $logs;

    /**
     * @return string
     */
    public function run()
    {
        $this->preparePanels();

        return parent::run();
    }

    /**
     * Подготовка табов
     */
    protected function preparePanels()
    {

        $groups = [];

        $formatter = new Formatter;
        $formatter->dateFormat = 'php:Y/m/d H:i:s';

        $infoChange = [];

        foreach ($this->logs as $key => $item) {
            if (in_array($item->field, ['delivery_to', 'delivery_from', 'blocked_to'])) {
                if(in_array($item->field, ['delivery_to', 'delivery_from'])) {
                    if (!empty($item->old)) {
                        $item->old = $formatter->asDate($item->old + $item->order->country->timezone->time_offset);
                    }
                    if (!empty($item->new)) {
                        $item->new = $formatter->asDate($item->new + $item->order->country->timezone->time_offset);
                    }
                }else{
                    if (!empty($item->old)) {
                        $item->old = $formatter->asDate($item->old);
                    }
                    if (!empty($item->new)) {
                        $item->new = $formatter->asDate($item->new);
                    }
                }
            }

            $groups[$item->group_id]['items'][$key] = $item;
            $groups[$item->group_id]['created_at'] = $item->created_at;
            $groups[$item->group_id]['operator'] = $item->operator ? Yii::t('common', 'Пользователь') . ' ' . $item->operator : '';
            $groups[$item->group_id]['user_id'] = $item->user_id ? '(' . $item->user_id . ')' : '';
        }

        $number = 0;

        foreach ($groups as $key => $items) {
            $dataProvider = new ArrayDataProvider([
                'allModels' => $groups[$key]['items']
            ]);

            $panel = new PanelTab([
                'id' => 'panel_tab_' . $key,
                'title' => Yii::t('common', 'Изменение #{number}  {date} {operator}{user_id}', ['number' => count($groups) - $number, 'date' => $formatter->asDate($groups[$key]['created_at']), 'operator' => $groups[$key]['operator'], 'user_id' => $groups[$key]['user_id']]),
                'content' => $this->render('panel-group-logs', [
                    'dataProvider' => $dataProvider,
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover table-sortable table-condensed'
                    ],
                ])
            ]);

            $number++;
            $this->panels[] = $panel;
        }
    }
}

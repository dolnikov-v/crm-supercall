<?php

namespace common\modules\order\widgets;


use common\widgets\base\Widget;

/**
 * Class ModalEndWorkWithSearchForm
 * @package common\modules\order\widgets
 */
class ModalEndWorkWithSearchForm extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-end-work-with-search-form');
    }
}
<?php

namespace common\modules\order\widgets;


use common\widgets\base\Widget;

/**
 * Class ModalOrderForm
 * @package common\modules\order\widgets
 */
class ModalOrderForm extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal-order-form/modal');
    }
}
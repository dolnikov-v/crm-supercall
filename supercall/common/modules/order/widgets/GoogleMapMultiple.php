<?php
namespace common\modules\order\widgets;

use yii2mod\google\maps\markers\GoogleMaps;
use yii2mod\google\maps\markers\GoogleMapsAsset;
use yii\web\View;


class GoogleMapMultiple extends GoogleMaps
{
    /**
     * Register assets
     */
    protected function registerAssets()
    {
        $view = $this->getView();
        GoogleMapsAsset::register($view);
        $view->registerJsFile($this->getGoogleMapsApiUrl(), ['position' => View::POS_HEAD]);
        $options = $this->getClientOptions();
        $decoded_options = json_decode($options, 1);

        $view->registerJs("
            var container = document.getElementById('{$decoded_options['containerId']}');
            container.style.width = '100%';
            container.style.height = '100%';
            {$decoded_options['containerId']} = new google.maps.Map(container, ".json_encode($decoded_options['mapOptions']).");
            
            $('li').click(function(){
                setTimeout(function(){
                    google.maps.event.trigger({$decoded_options['containerId']}, 'resize');
                    //fix for task map
                    if(typeof window.google_task == 'object'){
                        window.google_task.reDrawMap(window.google_task.address_data);
                    }
                }, 300);
            });", $view::POS_END);
    }
}
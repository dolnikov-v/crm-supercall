<?php
namespace common\modules\order\widgets;

use common\widgets\base\PanelGroup;
use common\widgets\base\PanelTab;
use Yii;
use yii\data\ArrayDataProvider;
use backend\modules\order\models\OrderTask;

/**
 * Class PanelGroupTasks
 * @package common\modules\order\widgets
 */
class PanelGroupTasks extends PanelGroup
{
    public $tasks;

    /**
     * @return string
     */
    public function run()
    {
        $this->preparePanels();

        return parent::run();
    }

    /**
     * Подготовка табов
     */
    protected function preparePanels()
    {
        $statuses = OrderTask::getTaskStatuses();

        $groups = [];

        foreach ($this->tasks as $task) {
            $groups[$task->id][] = $task;
        }

        $number = 0;

        foreach ($groups as $key => $items) {

            $dataProvider = new ArrayDataProvider([
                'allModels' => $items
            ]);

            switch($items[0]->status){
                case (OrderTask::TASK_STATUS_NEW) : $type = 'info'; break;
                case (OrderTask::TASK_STATUS_REJECTED) : $type = 'danger'; break;
                case (OrderTask::TASK_STATUS_APPROVED) : $type = 'success'; break;
                case (OrderTask::TASK_STATUS_NOT_ACTUAL) : $type ='default'; break;
                case (OrderTask::TASK_STATUS_PENDING) : $type ='warning'; break;
                default : $type = 'default'; break;
            }

            $panel = new PanelTab([
                'id' => 'panel_tab_' . $key,
                'title' => Yii::t('common', 'Таск #{number} <span class="label label-{type}">{status}</span>', [
                    'number' => $items[0]->id,
                    'status' => $statuses[$items[0]->status],
                    'type' => $type
                    ]
                ),
                'content' => $this->render('panel-group-tasks', [
                    'dataProvider' => $dataProvider,
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover table-sortable table-condensed'
                    ],
                ])
            ]);

            $number++;
            $this->panels[] = $panel;
        }

    }
}

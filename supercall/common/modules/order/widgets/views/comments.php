<?php
    use yii\helpers\Html;

?>
    
<?php foreach ($comments as $keyComment => $comment): ?>

    <?php $author = $comment['operator_name'] ?? $comment['author'] ?? '';?>

    <div class="form-group">
        <div class="col-xs-8 col-md-8 col-lg-9 pr__5 pl__5">
            <div class="pull-left"><i class="pe-7s-comment fs-icon clr-cadetblue"></i>&nbsp;&nbsp;</div>
            <div class="pull-left">
                <div data-cid="comment_<?= $keyComment ?>"
                     class="comment-disabled comment-flow"
                     disabled><strong><?= Html::encode($author) ?>:</strong>&nbsp;<?= Html::a(Html::encode($comment['text']), null, [
                        'onclick' => 'showComment("' .$keyComment. '")'
                    ]); ?></div>
            </div>
            <div class="clearfix"></div>
        
        </div>
    </div>
<?php endforeach; ?>
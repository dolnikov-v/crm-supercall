<?php
use common\components\grid\GridView;

/** @var \yii\data\ArrayDataProvider $dataProvider */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $index, $widget, $grid) {
        return ['class' => 'align-baseline'];
    },
    'columns' => [
        [
            'attribute' => 'column',
            'contentOptions' => ['class' => 'align-baseline'],
            'label' => yii::t('common', 'Поле')
        ],
        [
            'attribute' => 'old',
            'format' => 'raw',
            'label' => yii::t('common', 'Старое значение'),
            'contentOptions' => ['class' => 'align-top'],
            'content' => function ($data) {
                $content = '';

                if (!is_array($data['old'])) {
                    $content = in_array($data['column'], ['delivery_from', 'delivery_to']) ? date('d/m/Y H:i:s', $data['old']) : $data['old'];
                } else {
                    foreach ($data['old'] as $k => $v) {
                        $content .= is_numeric($k) ? '' : '<b>' . $k . '</b>:<br/>';
                        if (is_array($v)) {
                            foreach ($v as $name => $value) {
                                $content .= '&nbsp&nbsp;' . $name . ' : <i>' . (is_array($value) ? json_encode($value, JSON_UNESCAPED_UNICODE)
                                         : $value) . '</i><br/>';
                            }
                        } else {
                            $content .= '&nbsp&nbsp;' . $v . '<br/>';
                        }
                    }
                }
                return $content;
            }
        ],
        [
            'attribute' => 'new',
            'label' => yii::t('common', 'Новое значение'),
            'contentOptions' => ['class' => 'align-baseline'],
            'content' => function ($data) {
                $content = '';

                if (!is_array($data['new'])) {
                    $content = in_array($data['column'], ['delivery_from', 'delivery_to']) ? date('d/m/Y H:i:s', $data['new']) : $data['new'];
                } else {
                    foreach ($data['new'] as $k => $v) {
                        $content .= is_numeric($k) ? '' : '<b>' . $k . '</b>:<br/>';
                        if (is_array($v)) {
                            foreach ($v as $name => $value) {
                               $content .= '&nbsp&nbsp;' . $name . ' : <i>' . (is_array($value) ? json_encode($value, JSON_UNESCAPED_UNICODE) : $value) . '</i><br/>';
                            }
                        } else {
                            $content .= '&nbsp&nbsp;' . $v . '<br/>';
                        }
                    }
                }
                return $content;
            }
        ]
    ]
]) ?>
<?php
/**
 * @var $this \common\components\web\View
 */
use yii\bootstrap\Modal;
use yii\helpers\Html;

?>

<?php Modal::begin([
    'id' => 'modal_order_form',
    'header' => Html::tag('h4', Yii::t('common', 'Просмотр заказа')),
    'size' => 'modal-fullscreen',
    'options' => [
        'style' => 'z-index: 80000',
        'data-backdrop' => 'static',
        'data-keyboard' => false
    ],
    'headerOptions' => [
        'class' => 'modal-small-header'
    ]
]) ?>
    <div id="modal_order_form_body" class="modal-order-form-body">
        <iframe></iframe>
    </div>
<?php Modal::end(); ?>
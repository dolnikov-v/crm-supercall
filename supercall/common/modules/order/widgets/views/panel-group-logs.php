<?php
use common\components\grid\GridView;

/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'field',
        'old',
        'new'

    ]
]) ?>

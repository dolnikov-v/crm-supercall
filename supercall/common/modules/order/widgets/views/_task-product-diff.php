<?php
use common\components\grid\GridView;

/** @var \yii\data\ArrayDataProvider $dataProductProvider */
/** @var [] $igrnoreOrderProductParam */
?>

<?= GridView::widget([
    'dataProvider' => $dataProductProvider,
    'rowOptions' => function ($model, $index, $widget, $grid) {
        return ['class' => 'align-baseline'];
    },
    'columns' => [
        [
            'attribute' => 'old',
            'format' => 'raw',
            'label' => yii::t('common', 'Старое значение'),
            'contentOptions' => ['class' => 'align-top'],
            'content' => function ($data) use ($igrnoreOrderProductParam) {
                $content = '';
                foreach($data['old'] as $k=>$product){
                    asort($product);
                    foreach($product as $param => $value){
                        if(!in_array($param, $igrnoreOrderProductParam)) {
                            $content .= $param . ' : <i>' . $value . '</i><br/>';
                        }
                    }

                    $content .= '<br/>';
                }

                return $content;
            }
        ],
        [
            'attribute' => 'new',
            'label' => yii::t('common', 'Новое значение'),
            'contentOptions' => ['class' => 'align-baseline'],
            'content' => function ($data) use ($igrnoreOrderProductParam) {
                $content = '';
                foreach($data['new'] as $k=>$product){
                    asort($product);
                    foreach($product as $param => $value){
                        if(!in_array($param, $igrnoreOrderProductParam)) {
                            $content .= $param . ' : <i>' . $value . '</i><br/>';
                        }
                    }
                    $content .= '<br/>';
                }

                return $content;
            }
        ]
    ]
]) ?>
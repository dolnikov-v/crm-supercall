<?php
use common\components\grid\GridView;
use yii\data\ArrayDataProvider;

/** @var \yii\data\ArrayDataProvider $dataProvider */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'order_old',
            'label' => '',
            'format' => 'raw',
            'content' => function ($data) {
                $ignoreList = [
                    'id',
                    'country_id',
                    'type_id',
                    'status',
                    'sub_status',
                    'partner_id',
                    'foreign_id',
                    'created_at',
                    'updated_at',
                    'form_id',
                    'recall_date',
                    'shipping_id',
                    'ordered_at',
                    'language_id',
                    'last_queue_id',
                    'finished',
                    'approve_components',
                    'source_uri',
                    'phone_error',
                    'blocked_to',
                    'attempts',
                    'customer_ip',
                ];

                $order_old = json_decode($data->order_old, 1);
                $order_new = json_decode($data->order_new, 1);
                $product_old = json_decode($data->product_old, 1);
                $product_new = json_decode($data->product_new, 1);

                $order_diff = [];

                foreach ($order_old as $field => $value) {
                    if (!in_array($field, $ignoreList)) {
                        if (isset($order_new[$field]) && $value != $order_new[$field]) {
                            if (is_array($value) || is_array($order_new[$field])) {
                                $order_diff[$field] = [
                                    'column' => $field,
                                    'old' => $value,
                                    'new' => $order_new[$field],
                                ];
                            } else {
                                $order_diff[$field] = [
                                    'column' => $field,
                                    'old' => json_decode($value, 1) ? json_decode($value, 1) : $value,
                                    'new' => json_decode($order_new[$field], 1) ? json_decode($order_new[$field], 1) : $order_new[$field],
                                ];
                            }
                        }
                    }
                }

                $dataProvider = new ArrayDataProvider([
                    'allModels' => $order_diff
                ]);

                //order diff template
                echo $this->render('_task-order-diff', ['dataProvider' => $dataProvider]);


                $igrnoreOrderProductParam = ['id', 'order_id', 'created_at', 'updated_at', 'fixed_price'];

                $dataProductProvider = new ArrayDataProvider([
                    'allModels' => [
                        [
                            'old' => $product_old,
                            'new' => $product_new
                        ]
                    ]
                ]);
                //product diff template
                echo $this->render('_task-product-diff', ['dataProductProvider' => $dataProductProvider, 'igrnoreOrderProductParam' => $igrnoreOrderProductParam]);

            }
        ]
    ]
]) ?>

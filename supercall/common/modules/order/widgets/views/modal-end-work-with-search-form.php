<?php
/**
 * @var $this \common\components\web\View
 */
use yii\bootstrap\Modal;

?>

<?php Modal::begin([
    'id' => 'modal_end_work_with_search_form',
    'header' => Yii::t('common', 'Завершить работу с заказом?'),
    'size' => Modal::SIZE_SMALL,
    'options' => [
        'style' => 'z-index: 80000',
        'data-keyboard' => false
    ],
    'headerOptions' => [
        'class' => 'modal-small-header'
    ]
]) ?>
<div id="modal_end_work_with_search_form_body" class="text-center">
    <button type="button" id="modal_end_work_with_search_form_yes"
            class="btn btn-success"><?= yii::t('common', 'Да'); ?></button>
    <button type="button" id="modal_end_work_with_search_form_no"
            class="btn btn-primary"><?= yii::t('common', 'Нет'); ?></button>
</div>
<?php Modal::end(); ?>

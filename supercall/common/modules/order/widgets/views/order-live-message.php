<?php

use common\widgets\base\GlyphIcon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/** @var \common\modules\order\models\Order $order */
?>

<div class="row">
    <div class="col-lg-12 messages_body container">


        <?php foreach ($order->liveMessages as $message): ?>

            <div class="row">

                <?php if (!$message->user_id): ?>

                    <div class="col-sm-12 left">
                        <div class="client_message">
                            <div id="tb-testimonial" class="testimonial testimonial-primary">
                                <div class="testimonial-desc">
                                    <div class="testimonial-writer">
                                        <div class="testimonial-writer-name"
                                             title="<?= yii::t('common', 'Сообщение клиента') ?> (<?=$message->id;?>)">
                                            <small>
                                                <i class="glyphicon <?= GlyphIcon::CLOCK ?>"></i> <?= $message->created_at; ?>
                                            </small>
                                            <br/>
                                            <i class="glyphicon <?= GlyphIcon::USER ?>"></i> <?= $message->sender; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-section">
                                    <?php if ($message->is_new): ?>
                                        <i class="is_new glyphicon <?= GlyphIcon::MESSAGE; ?>"> </i>
                                    <?php endif; ?>

                                    <?= trim($message->content); ?>

                                    <i class="reply glyphicon <?= GlyphIcon::REPLY ?>"
                                       title="<?= yii::t('common', 'Цитировать') ?>"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php else: ?>

                    <div class="col-sm-12 right">
                        <div class="operator_message">
                            <div id="tb-testimonial" class="testimonial testimonial-success">
                                <div class="testimonial-desc">
                                    <div class="testimonial-writer">
                                        <div class="testimonial-writer-name"
                                             title="<?= yii::t('common', 'Сообщение оператора') ?>">
                                            <small>
                                                <i class="glyphicon <?= GlyphIcon::CLOCK ?>"></i> <?= $message->created_at; ?>
                                            </small>
                                            <br/>
                                            <i class="glyphicon <?= GlyphIcon::USER ?>"></i> <?= $message->sender; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-section">
                                    <?= trim($message->content); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>

            </div>

        <?php endforeach; ?>


    </div>
</div>
<hr/>
<div class="row">
    <div class="col-lg-11">
        <?= Html::textInput('order_messages', '', [
            'class' => 'form-control'
        ]); ?>
    </div>
    <div class="col-lg-1">
        <?= Html::button(\Yii::t('common', 'Отправить'), [
            'class' => 'btn btn-default btn-send-live-message',
            'name' => 'order_messages',
            'disabled' => true
        ]); ?>
    </div>
</div>

<div class="templates_message hidden">

    <div class="system_message_tpl">
        <div class="col-sm-12 right">
            <div class="system_message">
                <div id="tb-testimonial" class="testimonial testimonial-warning">
                    <div class="testimonial-desc">
                        <div class="testimonial-writer">
                            <div class="testimonial-writer-name">{sender}</div>
                        </div>
                    </div>
                    <div class="testimonial-section">
                        {message}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="client_message_tpl">
        <div class="col-sm-12 left">
            <div class="client_message">
                <div id="tb-testimonial" class="testimonial testimonial-primary">
                    <div class="testimonial-desc">
                        <div class="testimonial-writer">
                            <div class="testimonial-writer-name"
                                 title="<?= yii::t('common', 'Сообщение клиента') ?>">
                                <small>
                                    <i class="glyphicon <?= GlyphIcon::CLOCK ?>"></i> {date}
                                </small>
                                <br/>
                                <i class="glyphicon <?= GlyphIcon::USER ?>"></i> {sender}
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-section">
                        <i class="is_new hidden glyphicon <?= GlyphIcon::MESSAGE; ?>"> </i>
                        {message}
                        <i class="reply glyphicon <?= GlyphIcon::REPLY ?>"
                           title="<?= yii::t('common', 'Цитировать') ?>"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="operator_message_tpl">
        <div class="col-sm-12 right">
            <div class="operator_message">
                <div id="tb-testimonial" class="testimonial testimonial-success">
                    <div class="testimonial-desc">
                        <div class="testimonial-writer">
                            <div class="testimonial-writer-name"
                                 title="<?= yii::t('common', 'Сообщение оператора') ?>">
                                <small>
                                    <i class="glyphicon <?= GlyphIcon::CLOCK ?>"></i> {date}
                                </small>
                                <br/>
                                <i class="glyphicon <?= GlyphIcon::USER ?>"></i> {sender}
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-section">
                        {message}
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

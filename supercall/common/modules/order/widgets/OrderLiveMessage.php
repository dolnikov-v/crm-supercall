<?php

namespace common\modules\order\widgets;

use common\modules\order\models\Order;
use common\widgets\base\Widget;
use Yii;

/**
 * Class OrderLiveMessage
 * @package common\modules\order\widgets
 */
class OrderLiveMessage extends Widget
{
    /** @var  Order */
    public $order;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('order-live-message', [
            'order' => $this->order
        ]);
    }
}
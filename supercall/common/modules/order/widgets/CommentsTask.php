<?php

namespace common\modules\order\widgets;


use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\widgets\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Виджет для вывода комментариев в форме таска
 *
 * Class CommentsTask
 * @package common\modules\order\widgets
 */
class CommentsTask extends Widget
{
    public $comments;

    /**
     * @return bool|string
     */
    public function run()
    {
        return $this->render('comments', [
            'comments' => $this->comments,
        ]);
    }
}
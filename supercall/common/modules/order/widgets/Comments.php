<?php
    
namespace common\modules\order\widgets;


use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\widgets\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Виджет для вывода комментариев в форме заказа
 *
 * Class Comments
 * @package common\modules\order\widgets
 */
class Comments extends Widget
{
    public $orderId;
    // итоговый массив
    private static $comments = [];
    
    public function run()
    {
        // Комментарии из order->customer_components
        $order = Order::findOne($this->orderId);
        if ($order) {
            
            return $this->render('comments', [
                'comments' => $order->comments,
            ]);
        }
        
        return false;
        
    }
}
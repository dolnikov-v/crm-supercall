<?php

namespace common\modules\order\assets;


use yii\web\AssetBundle;

/**
 * Class SearchOrderAsset
 * @package common\modules\order\assets
 */
class SearchOrderAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/change';

    public $css = [
        'search-form.css'
    ];

    public $js = [
        'search-form.js'
    ];
}
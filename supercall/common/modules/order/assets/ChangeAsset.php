<?php
namespace common\modules\order\assets;

use common\modules\order\models\Order;
use yii\web\AssetBundle;

/**
 * Class ChangeAsset
 * @package common\modules\order\assets
 */
class ChangeAsset extends AssetBundle
{
    /** @var Order */
    public $order;

    public $sourcePath = '@common/web/resources/modules/order/change';

    public $css = [
        'change.css',
    ];

    public $js = [
        'autosize.min.js',
        'additional-price.js',
        'change.js',
        'select2autocomplete.js',
        'google.js',
        'zoiper.js',
        'package.js',
        'hidden-phones.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\MomentAsset',
    ];

    public function init()
    {
        parent::init();
        if ($this->order){
            if (is_file($this->sourcePath.'/'.$this->order->partner_id.'/'.$this->order->country->char_code.'/change.js')) {
                $this->js[] = [$this->order->partner_id . '/' . $this->order->country->char_code . '/change.js'];
            }
            else {
                if (is_file($this->sourcePath.'/'.$this->order->partner_id.'/change.js')) {
                    $this->js[] = [$this->order->partner_id . '/change.js'];
                }
            }
        }
    }
}

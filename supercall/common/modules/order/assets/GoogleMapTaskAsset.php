<?php
namespace common\modules\order\assets;
use yii\web\AssetBundle;

/**
 * Class GoogleMapTaskAsset
 * @package common\modules\order\assets
 */
class GoogleMapTaskAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/change-task/google-map';

    public $js = [
        'google-map.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}

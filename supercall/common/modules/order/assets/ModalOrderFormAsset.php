<?php

namespace common\modules\order\assets;


use yii\web\AssetBundle;

/**
 * Class ModalOrderFormAsset
 * @package common\modules\order\assets
 */
class ModalOrderFormAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/widgets/order/modal-order-form';

    public $js = [];

    public $css = [
        'modal-order-form.css'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class FilterKladrAsset
 * @package common\modules\order\assets
 */
class FilterKladrAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/index/';

    public $js = [
        'filter-kladr.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

class ScrollParentWindowAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/scroll-parent-window';

    public $js = [
        'scroll-parent-window.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\BootstrapAsset',
    ];
}
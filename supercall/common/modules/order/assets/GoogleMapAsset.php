<?php
namespace common\modules\order\assets;
use yii\web\AssetBundle;

/**
 * Class GoogleMapAsset
 * @package common\modules\order\assets
 */
class GoogleMapAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/change/google-map';

    public $js = [
        'google-map.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}

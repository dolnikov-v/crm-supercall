<?php
namespace common\modules\order\assets;

use yii\web\AssetBundle;

class PartnerCreatedAtAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/change/';

    public $js = [
        'partner-created-at.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
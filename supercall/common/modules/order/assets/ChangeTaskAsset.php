<?php
namespace common\modules\order\assets;

use common\modules\order\models\Order;
use yii\web\AssetBundle;

/**
 * Class ChangeTaskAsset
 * @package common\modules\order\assets
 */
class ChangeTaskAsset extends AssetBundle
{
    /** @var Order */
    public $order;

    public $sourcePath = '@common/web/resources/modules/order/change-task';

    public $css = [
        'change-task.css',
    ];

    public $js = [
        'autosize.min.js',
        'change-task.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];

    public function init()
    {
        parent::init();
        if ($this->order){
            if (is_file($this->sourcePath.'/'.$this->order->partner_id.'/'.$this->order->country->char_code.'/change-task.js')) {
                $this->js[] = [$this->order->partner_id . '/' . $this->order->country->char_code . '/change-task.js'];
            }
            else {
                if (is_file($this->sourcePath.'/'.$this->order->partner_id.'/change-task.js')) {
                    $this->js[] = [$this->order->partner_id . '/change-task.js'];
                }
            }
        }
    }
}

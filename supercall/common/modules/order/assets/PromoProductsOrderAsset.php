<?php

namespace common\modules\order\assets;

use common\modules\order\models\Order;
use yii\web\AssetBundle;

/**
 * Class PromoProductsOrderAsset
 * @package common\modules\order\assets
 */
class PromoProductsOrderAsset extends AssetBundle
{
    /** @var Order */
    public $order;

    public $sourcePath = '@common/web/resources/modules/order/change/';

    public $js = [
        'promo-products-order.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
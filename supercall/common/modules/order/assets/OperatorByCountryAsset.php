<?php
namespace common\modules\order\assets;
use yii\web\AssetBundle;

/**
 * Class OperatorByCountryAsset
 * @package common\modules\order\assets
 */
class OperatorByCountryAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/index/';

    public $js = [
        'operator-by-country.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}

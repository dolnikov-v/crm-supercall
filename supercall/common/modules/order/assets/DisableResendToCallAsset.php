<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class DisableResendToCallAsset
 * @package common\modules\order\assets
 */
class DisableResendToCallAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/disable-resend-to-call';

    public $js = [
        'disable-resend-to-call.js'
    ];
}
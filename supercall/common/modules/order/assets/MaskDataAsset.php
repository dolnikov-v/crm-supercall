<?php
namespace common\modules\order\assets;

use yii\web\AssetBundle;

class MaskDataAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/mask-data';

    public $js = [
        'mask-data.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
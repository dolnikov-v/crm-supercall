<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ProductEditPriceAsset
 * @package common\modules\order\assets
 */
class ProductEditPriceAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/change/';

    public $js = [
        'product-edit-price.js'
    ];

    public $css = [
        'product-edit-price.css'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
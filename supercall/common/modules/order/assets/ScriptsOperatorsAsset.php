<?php
namespace common\modules\order\assets;

use yii\web\AssetBundle;

class ScriptsOperatorsAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/change';

    public $js = [
        'scripts-operators.js'
    ];

    public $css = [
        'scripts-operators.css'
    ];
}
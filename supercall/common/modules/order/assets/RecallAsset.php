<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class RecallAsset
 * @package common\modules\order\assets
 */
class RecallAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/recall';

    public $js = [
        'recall.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
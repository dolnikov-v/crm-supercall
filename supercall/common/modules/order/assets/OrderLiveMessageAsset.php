<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class OrderLiveMessageAsset
 * @package common\modules\order\assets
 */
class OrderLiveMessageAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/order-live-message/';

    public $css = [
        'order-live-message.css'
    ];

    public $js = [
        'order-live-message.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
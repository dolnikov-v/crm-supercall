<?php

namespace common\modules\order\assets;


use yii\web\AssetBundle;

class ZingTreeAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/change/zing-tree';

    public $js = [
        'zing-tree.js'
    ];

    public $css = [
        'zing-tree.css'
    ];
}
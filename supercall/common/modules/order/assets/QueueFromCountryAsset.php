<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class QueueFromCountryAsset
 * @package common\modules\order\assets
 */
class QueueFromCountryAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/queue-from-country';

    public $js = [
        'queue-from-country.js'
    ];
}
<?php
namespace common\modules\order\assets;

use yii\web\AssetBundle;

class AppointmentOperatorAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/change';

    public $css = [
        'appointment-operator.css'
    ];


    public $js = [
        'appointment-operator.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\BootstrapAsset',
    ];
}
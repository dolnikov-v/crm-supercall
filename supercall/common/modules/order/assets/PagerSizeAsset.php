<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

class PagerSizeAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/pager-size/';

    public $css = [
        'pager-size.css'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
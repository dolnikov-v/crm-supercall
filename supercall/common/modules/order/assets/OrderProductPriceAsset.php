<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

class OrderProductPriceAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/';

    public $js = [
        'order-product-price.js',
    ];

    public $css = [

    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\BootstrapAsset',
    ];
}

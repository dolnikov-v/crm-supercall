<?php
namespace common\modules\order\assets;

use common\modules\order\models\Order;
use yii\web\AssetBundle;


class GetBrokerPay extends AssetBundle
{
    /** @var Order */
    public $order;

    public $sourcePath = '@common/web/resources/modules/order/broker-pay';

    public $js = [
        'broker-pay.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class AutoCallAsset
 * @package common\modules\order\assets
 */
class AutoCallAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/widgets/navbar/auto_call';

    public $css = [
        'auto_call.css',
    ];

    public $js = [
        'auto_call.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
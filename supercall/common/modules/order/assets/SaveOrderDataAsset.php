<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

class SaveOrderDataAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/save-order-data';

    public $js = [
        'save-order-data.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
<?php
namespace common\modules\order\assets;

use yii\web\AssetBundle;

class QualityControlEditAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/quality-control-edit';

    public $js = [
        'quality-control-edit.js'
    ];
}
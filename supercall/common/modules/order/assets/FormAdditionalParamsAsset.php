<?php

namespace common\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class FormAdditionalParamsAsset
 * @package common\modules\order\assets
 */
class FormAdditionalParamsAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/order/form-additional-params';

    public $js = [
        'form-additional-params.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
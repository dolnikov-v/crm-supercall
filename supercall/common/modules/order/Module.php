<?php
namespace common\modules\order; 

use common\components\base\Module as CommonModule;

/**
 * Class Module
 * @package common\modules\order
 */
class Module extends CommonModule
{
    public $controllerNamespace = 'common\modules\order\controllers';
}

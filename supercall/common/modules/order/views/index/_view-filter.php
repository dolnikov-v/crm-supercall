<?php

use common\modules\partner\models\Partner;
use common\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\modules\order\assets\OperatorByCountryAsset;
use common\modules\order\assets\QueueFromCountryAsset;
use common\modules\order\assets\DisableResendToCallAsset;

/** @var \common\models\search\ProductSearch $modelSearch */
/** @var array $statuses */
/** @var array $products */
/** @var array $partners */

OperatorByCountryAsset::register($this);
QueueFromCountryAsset::register($this);
DisableResendToCallAsset::register($this);

$defaulPartner = Partner::findOne(['name' => Partner::PARTNER_2WTRADE]);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= /** @var array $countries */
        $form->field($modelSearch, 'countries')->select2List($countries, [
            'value' => !$modelSearch->is_all_countries // Если выбраны все доступные страны
                ? $modelSearch->countries
                : '',
            'multiple' => true,
            'placeholder' => yii::t('common', 'Все'),
        ])->label(\Yii::t('common', 'Страна')) ?>
    </div>



    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'address_type')->select2List([], [
            'prompt' => '—'
        ]) ?>
        <div class="col-lg-3 col-sm-3 col-xs-4 loader hidden" id="loader_type"></div>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'address_value')->select2List([], [
            'multiple' => true,
        ]) ?>
        <div class="col-lg-3 col-sm-3 col-xs-4 loader hidden" id="loader_value"></div>
    </div>

    <?php /** @var boolean $visiblePartner */
    if ($visiblePartner): ?>
        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= $form->field($modelSearch, 'partner_id')->select2List(ArrayHelper::map(yii::$app->user->identity->partners, 'id', 'name'), [
                'value' => $modelSearch->partner_id ? $modelSearch->partner_id : ($defaulPartner ? $defaulPartner->id : '')
            ]) ?>
        </div>
    <?php endif; ?>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= /** @var array $types */
        $form->field($modelSearch, 'type_id')->select2List($types, [
            'prompt' => '—'
        ])->label(yii::t('common', 'Тип заказа')) ?>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'type_date')->select2List([
            'created_at' => yii::t('common', 'Дата создания в CRM'),
            'updated_at' => yii::t('common', 'Дата обновления'),
            'partner_created_at' => yii::t('common', 'Дата создания у партнёра'),
            'shipping_date' => yii::t('common', 'Дата доставки'),
            'ignore' => yii::t('common', 'Игнорировать')
        ])->label(Yii::t('common', 'Тип даты')) ?>
    </div>


    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'product')->select2List($products, [
            'prompt' => '—'
        ]) ?>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'id')->textInput();?>
    </div>

    <?php /** @var boolean $visiblePartnerNumber */
    if ($visiblePartnerNumber): ?>
        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= $form->field($modelSearch, 'foreign_id')->textInput();?>
        </div>
    <?php endif; ?>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'date_range')->widget(DateRangePicker::className(), [
            'convertFormat' => false,
            'presetDropdown' => true,
            'hideInput' => true,
            'pluginOptions' => [
                'autoUpdateOnInit' => true,
                'autoUpdateInput' => false,
                'timePicker' => true,
                'timePicker24Hour' => true,
                'timePickerIncrement' => 1,
                'locale' => [
                    'format' => 'DD/MM/YYYY H:mm',
                ],
            ],
            'pluginEvents' => [
                'cancel.daterangepicker' => 'function(ev, picker) { $("input#ordersearch-updatedrange").val(""); $(this).find("span.range-value").text("") }',
            ],
        ])->label(yii::t('common', 'Дата')); ?>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'status')->select2List($statuses, [
            'prompt' => '—'
        ]) ?>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'customer_full_name')->textInput() ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'customerPhone')->textInput()->label(Yii::t('common', 'Телефон')) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'customerMobile')->textInput()->label(Yii::t('common', 'Мобильный')) ?>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= /** @var array $queues */
        $form->field($modelSearch, 'last_queue_id')->select2List([], [
            'prompt' => '—',
            'disabled' => true
        ]) ?>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= /** @var array $operators */
        $form->field($modelSearch, 'operator')->select2List($operators, [
            'prompt' => '—'
        ]) ?>
    </div>


</div>
<div class="row">
    <div class="col-lg-2 col-sm-2 col-xs-3">
        <?php $with_timezone = yii::$app->request->get('OrderSearch')['with_timezone']; ?>

        <?= $form->field($modelSearch, 'with_timezone')->checkbox([
            'checked' => $with_timezone,
            'value' => true,
            'unchecked_value' => false
        ]) ?>
    </div>
    <div class="col-lg-2 col-sm-2 col-xs-3">
        <?= $form->field($modelSearch, 'phone_error')->checkbox() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2 col-sm-2 col-xs-3">
        <?= $form->field($modelSearch, 'original_id')->checkbox([
            'checked' => $modelSearch->original_id,
            'value' => true,
            'unchecked_value' => false,
            'label' => yii::t('common', 'Не оригинал')
        ]) ?>
    </div>
    <div class="col-lg-2 col-sm-2 col-xs-3" id="check-disabled">
        <?= $form->field($modelSearch, 'status_queue_strategy')->checkbox([
            'checked' => $modelSearch->status_queue_strategy,
            'value' => true,
            'unchecked_value' => false,
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
$this->registerJs("
        $(function () {
            $('#ordersearch-is_all_countries').on('click', function() {
                if ( $('#ordersearch-is_all_countries').prop('checked') ) {
                    // switch on
                    $('#ordersearch-countries').select2('val', '');
                }
            })
            
            $('#ordersearch-countries').on('select2:select', function (evt) {
                $('#ordersearch-is_all_countries').prop('checked', false)
            });
            
        });
"); ?>

<?php $this->registerCss("
    .select2-search--hide {
        display: block !important; 
    }
") ?>

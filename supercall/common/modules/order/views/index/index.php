<?php

use backend\modules\order\widgets\groupoperations\GroupOperationsWidget;
use common\assets\base\CustomCommonAsset;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\fonts\FontAwesome;
use common\helpers\grid\DataProvider;
use common\modules\order\models\Order;
use common\modules\order\models\OrderType;
use common\modules\order\models\search\OrderSearch;
use common\widgets\base\Button;
use common\widgets\base\LinkPager;
use common\widgets\base\Panel;
use common\widgets\export\Export;
use common\widgets\exportcallrecords\ExportCallRecords;
use juniq\helper\Time;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use backend\modules\order\assets\ExportAsset;
use common\modules\order\assets\PagerSizeAsset;
use common\modules\order\assets\FilterKladrAsset;

ExportAsset::register($this);
PagerSizeAsset::register($this);
FilterKladrAsset::register($this);
CustomCommonAsset::register($this);

$types = OrderType::find()->collection('common');
?>

<?= Tabs::widget([
    'encodeLabels' => false,
    'items' => [
        [
            'label' => Yii::t('common', 'Фильтр поиска'),
            'content' => Panel::widget([
                'alertStyle' => Panel::ALERT_INFO,
                'alert' => yii::t('common', '<b>Внимание!</b><br/> При использовании фильтра <i>"Оператор"</i> совместно с фильтром <i>"Тип даты" = Дата обновления</i> в столбце статуса заказа будут отражены исторические данные (статусы заказов на период)<br/>Если совместно с фильтром <i>"Оператор"</i> будет использоваться фильтр <i>"Тип даты" = Дата создания в CRM</i>, то в столбце будут отражены текущие (актуальные) статусы заказов'),
                'showButtons' => $showButtons,
                'contentVisible' => $contentVisible,
                'content' => $this->render('_view-filter', [
                    'modelSearch' => $modelSearch,
                    'statuses' => $statuses,
                    'products' => $products,
                    'partners' => $partners,
                    'types' => $types,
                    'countries' => $countries,
                    'visiblePartner' => $visiblePartner,
                    'visiblePartnerNumber' => $visiblePartnerNumber,
                    'queues' => $queues,
                    'operators' => $operators
                ])
            ])
        ],
        [
            'label' => yii::t('common', 'Групповые операции'),
            'content' => Panel::widget([
                'content' => GroupOperationsWidget::widget()
            ])
        ]
    ]
]); ?>

<div class="row">
    <div class="col-lg-9"></div>

    <div class="col-lg-2  text-right export-call-record-box">
        <?= ExportCallRecords::widget([
            'place' => 'orderList'
        ]) ?>
    </div>

    <?php if (yii::$app->user->can('order.orderexport.getexportcolumns')) : ?>

        <div class="col-lg-1">
            <?= Export::widget([
                'model' => OrderSearch::className(),
                'controllerPathWithAction' => '/order/order-export',
                'title' => yii::$app->formatter->asDateFullTime(time()) . '_export_orders_',
                'queryParams' => Yii::$app->request->queryParams,
                'getAll' => true,
                'csvCharset' => 'utf-8',
                'blockClass' => 'pull-right',
                'blockStyle' => 'padding: 5px;',
                'xls' => true,
                'csv' => true,
                'word' => false,
                'html' => false,
                'pdf' => false,
            ]) ?>
        </div>

    <?php endif; ?>

</div>

<br/>


<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заказами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'id' => 'orderList',
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => \yii\grid\CheckboxColumn::className(),
            ],
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'partnerName',
                'label' => Yii::t('common', 'Партнер'),
                'enableSorting' => true,
            ],
            [
                'attribute' => 'foreign_id',
                'enableSorting' => true,
            ],
            [
                'attribute' => 'country_id',
                'enableSorting' => false,
                'content' => function ($model) {
                    return yii::t('common', $model->country->name);
                }
            ],
            [
                'attribute' => 'type_id',
                'enableSorting' => false,
                'content' => function ($model) use ($types) {
                    return isset($types[$model->type_id]) ? $types[$model->type_id] : 'unknown';
                }
            ],
            [
                'attribute' => 'status',
                'enableSorting' => true,
                'content' => function ($model) {
                    return Order::getStatusesCollection()[$model->status];
                }
            ],
            [
                'attribute' => 'customer_full_name',
                'enableSorting' => true,
            ],
            [
                'attribute' => 'productString',
                'format' => 'html',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'customer_phone',
                'value' => function ($model) {
                    if (!Yii::$app->user->can('order.index.viewcontacts')) {
                        return empty($model->customer_phone) ? $model->customer_phone : substr_replace($model->customer_phone, 'XXXX', -4);
                    } else {
                        return $model->customer_phone;
                    }
                },
                'enableSorting' => true,
            ],
            [
                'attribute' => 'last_queue_id',
                'value' => function ($model) {
                    return !is_null($model->queue) ? $model->queue->name : '';
                },
                'enableSorting' => true,
            ],
            [
                'attribute' => 'blocked_user',
                'value' => function ($model) {
                    return $model->queueOrder ? $model->queueOrder->blockedUser->username : '';
                },
            ],
            [
                'attribute' => 'recall_date',
                'value' => function ($model) {
                    /** @var Order $model */
                    return $model->recall_date ? $model->getDatetimeByTimezone(strtotime($model->recall_date), true) : '';
                },
            ],
            [
                'attribute' => 'block_with_user',
                'value' => function ($model) {
                    return $model->queueOrder ? $model->getDatetimeByTimezone($model->queueOrder->blocked_to) : '';
                },
            ],
            [
                'attribute' => 'block_by_queue',
                'value' => function ($model) {
                    return $model->blocked_to && ! $model->queueOrder && !$model->recall_date ? $model->getDatetimeByTimezone($model->blocked_to) : '';
                },
            ],
            [
                'attribute' => 'original_id',
                'enableSorting' => true,
            ],
            [
                'attribute' => 'partner_created_at',
                'value' => function ($model) {
                    return $model->getDatetimeByTimezone($model->partner_created_at);
                },
                'enableSorting' => true,
                'label' => Yii::t('common', 'Дата создания у партнёра')
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return $model->getDatetimeByTimezone($model->created_at);
                },
                'enableSorting' => true,
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($model) {
                    return $model->getDatetimeByTimezone($model->updated_at);
                },
                'enableSorting' => true,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотр'),
                        'url' => function ($model) {
                            return Url::toRoute(['/order/index/view', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('order.change.index');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/order/change', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.change.index');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Iframe'),
                        'url' => function ($model) {
                            return $model->generateIframeUrl(yii::$app->user->identity);
                        },
                        'attributes' => function () {
                            return [
                                'target' => '_blank'
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.change.index');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => LinkPager::widget([
        'pagination' => $dataProvider->getPagination(),
        'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
    ]),
]) ?>

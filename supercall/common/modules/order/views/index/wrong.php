<?php
    use common\components\grid\ActionColumn;
    use common\components\grid\GridView;
    use common\components\grid\IdColumn;
    use common\helpers\grid\DataProvider;
    use common\models\Country;
    use common\widgets\base\Panel;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    use common\modules\partner\models\Partner;

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица заказов с ошибками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'partnerName',
                'label' => Yii::t('common', 'Партнер'),
                'enableSorting' => true,
                'content' => function ($model) {
                    return Partner::findOne($model['partner_id'])->name;
                }
            ],
            [
                'attribute' => 'foreign_id',
                'label' => 'ID заказа у партнера',
                'enableSorting' => true,
            ],
            [
                'attribute' => 'country_id',
                'label' => 'Страна',
                'enableSorting' => true,
                'content' => function ($model) {
                    return Country::findOne($model['country_id'])->name;
                }
            ],
            [
                'attribute' => 'customer_full_name',
                'label' => 'Имя киента',
                'enableSorting' => true,
                'content' => function ($model) {
                    return $model['customer_full_name'];
                }
            ],
            [
                'attribute' => 'customer_phone',
                'enableSorting' => true,
                'content' => function ($model) {
                    if (!Yii::$app->user->can('order.index.viewcontacts')) {
                        return empty($model['customer_phone']) ? $model['customer_phone'] : substr_replace($model['customer_phone'], 'XXXX', -4);
                    }else {
                        return $model['customer_phone'];
                    }
                }
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотр'),
                        'url' => function ($model) {
                            return Url::toRoute([ '/order/index/view', 'id' => $model['id'] ]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('order.change.index');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute([ '/order/change', 'id' => $model['id'] ]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.change.index');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
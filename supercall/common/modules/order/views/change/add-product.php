<?php

use common\modules\order\models\OrderProduct;
use yii\helpers\Json;

/** @var OrderProduct $product */
/** @var integer $index */
/** @var array $products */

$form = new \common\widgets\ActiveForm();
?>
<div class="product row form-group" id="product-<?= $index; ?>" data-attributes="">
    <?= $form->field($product, '[' . $index . ']id', ['template' => '{input}'])->hiddenInput(); ?>
    <div class="col-xs-3 col-sm-3 col-lg-5 pr__5">
        <?= $form->field($product, '[' . $index . ']product_id', ['template' => '{input}', 'options' => ['class' => 'has-error product-select']])->dropDownList($products); ?>
    </div>
    <div class="col-xs-2 col-sm-2 col-lg-2 pl__5 pr__5">
        <?= $form->field($product, '[' . $index . ']quantity', ['template' => '{input}'])
                ->dropDownList(OrderProduct::countList()); ?>
    </div>
    <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
        <?= $form->field($product, '[' . $index . ']price', ['template' => '{input}'])->textInput(['readonly' => true]); ?>
    </div>
    <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
        <?= $form->field($product, '[' . $index . ']cost', ['template' => '{input}'])->textInput(['readonly' => true]); ?>
    </div>
    <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
        <?= $form->field($product, '[' . $index . ']vat', ['template' => '{input}'])->textInput([
            'size' => $product->vat ? '' : 'hidden',
            'disabled' => true
        ]); ?>
    </div>
    <div class="col-xs-1 col-sm-1 col-lg-1 pr__5">
        <?= $form->field($product, '[' . $index . ']gift', ['template' => '{input}'])
            ->checkbox(['label' => ' ', 'checked' => $product->gift, 'value' => 1, 'unchecked_value' => 0]); ?>
    </div>
    <div class="col-xs-1 col-sm-1 col-lg-1">
        <button class="btn btn-link btn-xs btn-remove-product pull-right pt__0 mt__8" type="button">
            <i class="pe-7s-close-circle fs-icon-20 clr-red fs-icon-bold" title="<?= Yii::t('common', 'Удалить'); ?>"></i>
        </button>
    </div>
</div>

<script type="application/javascript">
    new_product_attributes = <?= Json::htmlEncode($form->attributes); ?>;
</script>

<?php
use common\modules\order\models\Order;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use backend\modules\order\models\OrderTaskCustomer;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var string $operator */
/** @var \common\widgets\ActiveForm $form */
/** @var array $lastOrderTask */

$OrderTaskCustomer = new OrderTaskCustomer();
$OrderTaskCustomer->order_id = $order->id;
?>

    <div class="row pr__15 pl__15">
        <?php
        $form_model = !$lastOrderTask ? $order->getGeneralModel() : $OrderTaskCustomer->getGeneralModel();
        if ($form_model) {
            foreach ($form_model->attributes() as $attr) {
                echo $form->field($form_model, $attr, ['template' => '<div class="col-md-6 col-sm-6 col-xs-12 pr__5 pl__5"><div class="form-group">{input}</div></div>']);
            }
        }
        //print_r($order->form->show_countries);
        // Если в форме Партнера выключен флаг "Показывать страну"
        if (ArrayHelper::getValue($order, 'form.show_countries', true)) {
            echo $form->field($order, 'country_id', ['template' => '<div class="form-group" style=""><div class="col-xs-8 col-md-8 col-lg-6 pr__5 pl__5">{input}</div></div>'])
                ->dropDownList($countries);
        }
        ?>
    </div>


<?php Modal::begin([
    'id' => 'modal-message',
    'header' => Yii::t('common', 'Сообщение'),
    'footer' => Html::button('Отправить', ['class' => 'btn btn-primary btn-message-send'])
]); ?>
    <div class="form-group">
        <?= Html::textarea('text', '', [
            'class' => 'form-control',
            'placeholder' => Yii::t('common', 'Сообщение'),
            'data' => [
                'order_id' => $order->id,
                'type' => '',
                'operator' => $operator,
                'to' => '',
            ]
        ]); ?>
    </div>
<?php Modal::end(); ?>
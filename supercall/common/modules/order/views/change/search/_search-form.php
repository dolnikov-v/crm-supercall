<?php
/* @var \common\modules\order\models\search\SearchOrder $searchModel */
/* @var $this \yii\web\View */

use common\widgets\ActiveForm;

$form = ActiveForm::begin([
    'options' => [
        'class' => 'order-search-form'
    ]
]);
?>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($searchModel, 'orderId')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($searchModel, 'foreign_id')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($searchModel, 'phone')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($searchModel, 'customerFullName')->textInput() ?>
        </div>
    </div>
<?php ActiveForm::end() ?>
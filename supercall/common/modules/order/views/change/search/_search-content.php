<?php
/* @var \yii\data\ArrayDataProvider $dataProvider */
/* @var $this \yii\web\View */
use common\components\grid\GridView;
use common\modules\order\models\Order;
use yii\helpers\Html;

?>

<div class="order-search-content">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'label' => Yii::t('common', 'Номер заказа'),
                'content' => function ($model) {
                    /** @var Order $model */
                    return Html::a($model->id, '#', [
                        'data' => [
                            'iframe-url' => $model->generateIframeUrl(Yii::$app->user->identity, false, false, in_array($model->status, array_keys(Order::getFinalStatusesCollection())), true),
                            'order-id' => $model->id,
                        ]
                    ]);
                },
                'contentOptions' => [
                    'class' => 'order-number',
                ]
            ],
            [
                'label' => Yii::t('common', 'Статус'),
                'attribute' => 'status',
                'content' => function ($model) {
                    return Order::getStatusesCollection()[$model->status];
                }
            ],
            [
                'attribute' => 'customer_phone',
                'label' => Yii::t('common', 'Телефон'),
            ],
            [
                'attribute' => 'customer_mobile',
                'label' => Yii::t('common', 'Мобильный'),
            ],
            [
                'attribute' => 'customer_full_name',
                'label' => Yii::t('common', 'Полное имя заказчика'),
            ],
            [
                'attribute' => 'productString',
                'label' => Yii::t('common', 'Продукты'),
            ],
            [
                'attribute' => 'partner_created_at',
                'value' => function ($model) {
                    return $model->getDatetimeByTimezone($model->partner_created_at);
                },
                'label' => Yii::t('common', 'Дата создания'),
            ]
        ]
    ]) ?>
</div>
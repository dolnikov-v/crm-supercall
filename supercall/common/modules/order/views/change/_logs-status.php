<?php
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\modules\order\models\Order;
use yii\helpers\ArrayHelper;

/** @var \yii\data\ActiveDataProvider $logsStatus */
?>

<?= GridView::widget([
    'dataProvider' => $logsStatus,
    'tableOptions' => [
        'class' => 'table table-striped table-hover table-sortable table-condensed'
    ],
    'columns' => [
        [
            'attribute' => 'id',
            'label' => '#',
            'headerOptions' => ['class' => 'width-50'],
            'enableSorting' => false,
        ],
        [
            'label' => Yii::t('common', 'Старый статус'),
            'enableSorting' => false,
            'content' => function ($model) {
                return ArrayHelper::getValue(Order::getStatusesCollection(), $model->old);
            }
        ],
        [
            'label' => Yii::t('common', 'Новый статус'),
            'enableSorting' => false,
            'content' => function ($model) {
                return Order::getStatusesCollection()[$model->new];
            }
        ],
        [
            'label' => Yii::t('common', 'Оператор'),
            'enableSorting' => false,
            'content' => function ($model) {
                return $model->operatorName;
            }
        ],
        [
            'label' => Yii::t('common', 'Дата изменения'),
            'class' => DateColumn::className(),
            'attribute' => 'created_at',
            'enableSorting' => false,
            'timezone' => function($model){
                return ArrayHelper::getValue($model, 'order.country.timezone.timezone_id');
            }
        ],
    ],
]); ?>

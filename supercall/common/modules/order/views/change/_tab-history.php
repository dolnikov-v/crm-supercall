<?php
use common\widgets\base\Nav;
use yii\data\ArrayDataProvider;

/** @var yii\web\View $this */
/** @var \common\modules\order\models\Order $order */
?>

<div class="mt__10">
<?= Nav::widget([
    'tabs' => [
        [
            'label' => Yii::t('common', 'Общее'),
            'content' => $this->render('_logs-common', [
                'logsCommon' => $order->logs
            ])
        ],
        [
            'label' => Yii::t('common', 'Статусы'),
            'content' => $this->render('_logs-status', [
                'logsStatus' => new ArrayDataProvider(['allModels' => $order->statusLogs])
            ])
        ],
        [
            'label' => Yii::t('common', 'Продукты'),
            'content' => $this->render('_logs-product', [
                'logsProduct' => new ArrayDataProvider(['allModels' => $order->productLogs])
            ])
        ]
    ]
]); ?>
</div>


<?php
use common\modules\order\models\Order;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use common\modules\order\assets\MaskDataAsset;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var string $operator */
/** @var \common\widgets\ActiveForm $form */
/** @var boolean $quality_control */
/** @var bool  $masked_phones*/

if ($order->form->mask_phones === true) {
    MaskDataAsset::register($this);
}
?>

    <div id="masked_inputs"
         class="hidden"><?= json_encode(($quality_control) ? [] : Order::getMaskedInputs(), JSON_UNESCAPED_UNICODE) ?></div>
    <div class="row pr__15 pl__15">
        <?php
        $form_model = $order->getGeneralModel();

        $attributeLabels = $form_model->attributeLabels();

        if ($form_model) {
            foreach ($form_model->attributes() as $attr) {

                $options = [
                    'placeholder' => yii::t('common', $attributeLabels[$attr])
                ];
                //маскировка данных
                if (in_array($attr, Order::getMaskedInputs()) && !$quality_control && ($masked_phones === true || !yii::$app->user->can('order.index.viewcontacts'))  && $order->form->mask_phones === true) {
                    if(!empty($form_model->getAttributes()[$attr])) {
                        $options['value'] = preg_replace(Order::MASK_PATTERN, Order::MASK_PHONES, $form_model->getAttributes()[$attr]);
                    }

                    $options['attributes'] = [
                        'data-ismask' => "true",
                        'data-content' => $form_model->getAttributes()[$attr]
                    ];
                }

                echo $form->field($form_model, $attr, ['template' => '<div class="col-md-6 col-sm-6 col-xs-12 pr__5 pl__5"><div class="form-group">{input}</div></div>',
                ])->textInput($options);
                //конец маскировки
            }
        }
        //print_r($order->form->show_countries);
        // Если в форме Партнера выключен флаг "Показывать страну"
        if (ArrayHelper::getValue($order, 'form.show_countries', true)) {
            echo $form->field($order, 'country_id', ['template' => '<div class="form-group" style=""><div class="col-xs-8 col-md-8 col-lg-6 pr__5 pl__5">{input}</div></div>'])
                ->dropDownList($countries);
        }
        ?>
    </div>


<?php Modal::begin([
    'id' => 'modal-message',
    'header' => Yii::t('common', 'Сообщение'),
    'footer' => Html::button('Отправить', ['class' => 'btn btn-primary btn-message-send'])
]); ?>
    <div class="form-group">
        <?= Html::textarea('text', '', [
            'class' => 'form-control',
            'placeholder' => Yii::t('common', 'Сообщение'),
            'data' => [
                'order_id' => $order->id,
                'type' => '',
                'operator' => $operator,
                'to' => '',
            ]
        ]); ?>
    </div>
<?php Modal::end(); ?>
<?php
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;

/** @var [] $packageList */
/** @var array $promo_products */
/** @var array $products */
/** @var array $packageList */
/** @var Order $order */
?>

<div class="products">
    <?= $this->render('../_row_product', [
        'i' => 0,
        'product' => new OrderProduct(),
        'products' => $products,
        'packageList' => $packageList,
        'order' => $order,
        'form' => $form,
        'is_gift' => false
    ]); ?>

</div>
<hr/>
<div class="row form-group">
    <div class="col-md-12">
        <button type="button"
                class="btn btn-default btn-sm btn-add-product"><?= Yii::t('common', 'Добавить товары'); ?></button>
    </div>
</div>
<br/>

<!-- PROMO PRODUCTS -->

<?php if ($order->form && !empty($order->form->promoProducts)) : ?>
    <fieldset id="promo-products">
        <legend><?= yii::t('common', 'Промо продукты') ?></legend>

        <?= $this->render('@common/modules/order/views/change/_tab-promo-products', [
            'order' => $order,
            'form' => $form,
            'promo_products' => $promo_products,
        ]); ?>
    </fieldset>
    <hr/>
    <br/>
<?php endif; ?>


<!-- END PROMO PRODUCTS -->

<div class="row form-group">

    <div class="col-lg-2"><?= Yii::t('common', 'Итого') . ': ' . $order->init_price; ?></div>
    <div class="col-lg-1 total-quantity"></div>
    <div class="col-lg-1 total-price pl__5"></div>

</div>
    
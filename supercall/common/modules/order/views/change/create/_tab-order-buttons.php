<?php
use backend\assets\widgets\navbar\MaskAsset;
use common\modules\order\models\Order;
use kartik\datetime\DateTimePicker;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var \common\widgets\ActiveForm $form */
/** @var bool $editable */

    MaskAsset::register($this);
?>
<?php
echo $form->field($order, 'status', ['template' => '{input}'])->hiddenInput(['value' => Order::STATUS_NEW]);
echo $form->field($order, 'init_price', ['template' => '{input}'])->hiddenInput();
echo $form->field($order, 'final_price', ['template' => '{input}'])->hiddenInput();
echo $form->field($order, 'shipping_price', [
    'template' => '{input}',
    'options'  => [
        'class' => 'default-shipping-price',
    ]])->hiddenInput();
    
// Определим порядок кнопок в форме
$buttons = Order::getButtonsStatus($order);
    
echo Html::button(\Yii::t('common', 'Сохранить'), [
    'class' => 'btn btn-primary btn-status-save mt__5 mb__5',
    'data-status' => Order::STATUS_NEW,
]);
?>
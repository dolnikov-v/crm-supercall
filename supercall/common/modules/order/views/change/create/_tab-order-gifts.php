<?php
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
?>

<div class="products-gift">
    <?= $this->render('../_row_product', [
        'i'        => 100,
        'product'  => new OrderProduct(),
        'products' => $products,
        'empty_field' => true,
        'order'    => $order,
        'form'     => $form,
        'is_gift' => true
    ]); ?>

</div>
<hr />
<div class="row form-group">
    <div class="col-xs-3 col-sm-3 col-lg-5">

    </div>
</div>
<div class="row form-group">
    <div class="col-md-12">
        <button type="button" class="btn btn-default btn-sm btn-add-product-gift"><?= Yii::t('common', 'Добавить подарки'); ?></button>
    </div>
</div>

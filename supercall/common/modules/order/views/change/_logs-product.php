<?php
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\modules\order\models\OrderProductLog;
use common\widgets\base\Label;
use yii\helpers\ArrayHelper;

/** @var array $logsProduct */
?>

<?= GridView::widget([
    'dataProvider' => $logsProduct,
    'tableOptions' => [
        'class' => 'table table-striped table-hover table-sortable table-condensed'
    ],
    'columns' => [
        'product.name',
        [
            'attribute' => 'action',
            'enableSorting' => false,
            'content' => function ($model) {
                $style = Label::STYLE_PRIMARY;
                if ($model->action == OrderProductLog::ACTION_CREATE) {
                    $style = Label::STYLE_SUCCESS;
                } elseif ($model->action == OrderProductLog::ACTION_DELETE) {
                    $style = Label::STYLE_DANGER;
                }

                return Label::widget([
                    'label' => array_key_exists($model->action, OrderProductLog::getProductActions()) ? OrderProductLog::getProductActions()[$model->action] : Yii::t('common', 'Неопределено'),
                    'style' => $style
                ]);
            }
        ],
        [
            'attribute' => 'price',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'quantity',
            'enableSorting' => false,
        ],
        [
            'class' => DateColumn::className(),
            'attribute' => 'created_at',
            'enableSorting' => false,
            'timezone' => function($model){ return ArrayHelper::getValue($model, 'order.country.timezone.timezone_id'); }
        ]
    ],
]) ?>

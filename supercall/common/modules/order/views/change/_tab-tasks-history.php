<?php
use common\widgets\base\Panel;

/** @var \yii\web\View $this */
/** $var [] $orderTasks */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список тасков'),
    'content' => $this->render('@backend/modules/order/views/order-task/_tab-order-tasks',['orderTasks' => $orderTasks])
]); ?>
<?php

use backend\modules\administration\assets\JsLoggerAsset;
use common\components\ModalCustom;
use common\helpers\fonts\PeIcon;
use common\helpers\Utils;
use common\models\GmapKey;
use common\models\User;
use common\modules\call\models\CallHistory;
use common\modules\order\assets\AppointmentOperatorAsset;
use common\modules\order\assets\FormAdditionalParamsAsset;
use common\modules\order\assets\GoogleMapAsset;
use common\modules\order\assets\OrderProductPriceAsset;
use common\modules\order\assets\PartnerCreatedAtAsset;
use common\modules\order\assets\SaveOrderDataAsset;
use common\modules\order\assets\ZingTreeAsset;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLogs;
use common\modules\order\models\OrderType;
use common\modules\order\widgets\GoogleMapMultiple;
use common\modules\partner\assets\form\FastChangeQueueAsset;
use common\modules\partner\models\PartnerForm;
use common\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\bootstrap\Collapse;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html as HtmlHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var bool $editable */
/** @var array $types */
/** @var array $products */
/** @var array $shippings */
/** @var array $shipping_options */
/** @var array $countries */
/** @var string $operator */
/** @var integer $countOrdersOnPhone */
/** @var array $formAttributes */
/** @var \common\widgets\ActiveForm $form */
/** @var array $packageList */
/** @var string $packageData */
/** @var string $productPrice */
/** @var boolean $masked_phones */
/** @var array|null $partner_created_at */
/** @var boolean $time_now_with_timezone */
/** @var array $operators */
/** @var  $quality_control */
/** @var bool $hide_status_buttons */

OrderProductPriceAsset::register($this);

$quality_control = yii::$app->request->get('quality-control') == 1;
//если работа в режиме контроля качества - открыть доступ для редактирования
$editable = $quality_control ? $quality_control : $editable;

GoogleMapAsset::register($this);
JsLoggerAsset::register($this);
FormAdditionalParamsAsset::register($this);

if (yii::$app->params['useOrderBackup']) {
    SaveOrderDataAsset::register($this);
}

ZingTreeAsset::register($this);

if (yii::$app->params['show_time_from_creation'] && $partner_created_at) {
    PartnerCreatedAtAsset::register($this);
}

$appointment_operator = ArrayHelper::getColumn((new Query())->from('{{%queue_order}}')
    ->where(['order_id' => $order->id])
    ->limit(1)
    ->all(), 'user_id');

//при реколе - нет данных в queue_order
if(empty($appointment_operator)) {
    $recall_operator = OrderLogs::find()->where(['order_id' => $order->id, 'new' => Order::STATUS_RECALL])->orderBy(['id' => SORT_DESC])->one();

    if($recall_operator && $recall_operator->operator){
        $user = User::findByUsername($recall_operator->operator);
        $appointment_operator[0] = $user->id;
    }
}

$roles = [];

$userModel = User::findOne(['id' => yii::$app->user->id]);
if (!empty($userModel)) {
    $roles = explode(', ', $userModel->getRolesNames());
}

if (empty($userModel) && Yii::$app->request->get('user_id')) {
    $userModel = User::findOne(['id' => Yii::$app->request->get('user_id')]);
}

$user_sip = json_decode($userModel->sip, 1);
if (isset($user_sip['sip_login'])) {
    $user_sip = $user_sip['sip_login'];
}

$blockThisOrder = (new Query())->from('{{%queue_order}}')
    ->leftJoin(User::tableName(), User::tableName() . '.id=user_id')
    ->where(['order_id' => $order->id])->one();

AppointmentOperatorAsset::register($this);

$defaultAvailableDeliveryFrom = strtotime('+' . $order->form->min_delivery_days . 'days');

//Берем первый товар
$productIdForScript = false;
foreach ($order->orderProducts as $i => $product) {
    if (!$product->gift) {
        $productIdForScript = $product->product_id;
        break;
    }
}

$types = OrderType::find()->collection('common');

$promo_products = [];

if ($order->form) {
    foreach ($order->form->promoProducts as $promoProduct) {
        /** @var \common\models\Product $promoProduct ->product */
        $promo_products[$promoProduct->product->id] = [
            'name' => $promoProduct->product->name,
            'id' => $promoProduct->product_id,
            'price' => $promoProduct->price
        ];
    }
}

if (empty($promo_products)) {
    foreach ($order->orderPromoProducts as $promoProduct) {
        $promo_products[$promoProduct->product_id] = [
            'id' => $promoProduct->product_id,
            'name' => $promoProduct->product->name,
            'price' => $promoProduct->price,

        ];
    }
}
?>
<?php

$form = ActiveForm::begin([
    'id' => 'form-order',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validateOnType' => false,
    'validateOnSubmit' => true,
    'method' => 'post',
    'action' => '',
    'options' => [
        'data' => [
            'editable' => $editable,
            'id_param' => $order->id,
            'id' => $order->id,
            'partner_id' => $order->partner_id,
            'country_id' => $order->country_id,
            'country' => $order->country->char_code,
            'status' => $order->status,
            'extra-price' => ArrayHelper::getValue($order, 'form.extra_price', 0),
            'vat' => ArrayHelper::getValue($order, 'form.vat.value', 0.0),
            'product_id' => $productIdForScript
        ]
    ]
]); ?>

<div class="hidden" id="form_additional_params" data-additional_params='<?= Html::encode(Json::encode($order->form ? $order->form->use_additional_params : Json::encode([])))?>'></div>

<?= Html::input('hidden','form-additional-params', '');?>

<div id="order-form-attributes"
     data-attributes="<?= json_encode($formAttributes['attributes'], JSON_UNESCAPED_UNICODE) ?>"></div>

<div class="row">
    <div class="col-md-6 col-lg-6 col-xs-6">
        <fieldset>
            <div style="padding-left: 3px; margin-top: 7px">
                <?php if ($order->type) : ?>
                    <p style="color:#2813c7">
                        <strong><?= yii::t('common', 'Тип'); ?></strong>
                        : <?= (isset($types[$order->type_id]) ? $types[$order->type_id] : '-'); ?>
                    </p>
                <?php endif; ?>

                <?php if (User::getAllowCountries()->all() > 1): ?>
                    <p>
                        <strong><?= yii::t('common', 'Страна'); ?></strong>
                        : <?= yii::t('common', $order->country->name); ?>
                    </p>
                <?php endif; ?>

                <?php if ($partner_created_at && yii::$app->params['show_time_from_creation']): ?>
                    <p>
                        <strong><?= yii::t('common', 'Время от создания'); ?>: </strong>
                        <span id="time_ago"></span> (<span
                                id="partner_created_at"><?= $partner_created_at['date']; ?></span>)
                        <span id="partner_created_at_timestamp"
                              class="hidden"><?= $partner_created_at['timestamp']; ?></span>
                        <span id="time_now" class="hidden"><?= $time_now_with_timezone; ?></span>
                    </p>
                <?php endif; ?>

                <?php if ($order->source_uri): ?>
                    <p>
                        <strong>url</strong>: <?= Html::a(StringHelper::truncate($order->source_uri, 30), $order->source_uri, ['target' => '_blank']) ?>
                    </p>
                <?php endif; ?>
                <?php if (isset($countOrdersOnPhone)): ?>
                    <p>
                        <strong><?= yii::t('common', 'количество заказов на данный номер телефона'); ?>
                            : </strong><?= $countOrdersOnPhone; ?>
                    </p>
                <?php endif; ?>
            </div>
        </fieldset>

        <?php if (!in_array(User::ROLE_OPERATOR, $roles) && is_null(yii::$app->request->get('operator'))) : ?>

            <!-- For not operator -->
            <fieldset <?= $editable ? '' : 'disabled'; ?>>
                <legend><?= yii::t('common', 'Назначение оператора'); ?></legend>

                <div class="col-xs-12 col-sm-3 col-lg-5 pr__5">
                    <div>
                    <span class="text-primary"><?= yii::t('common', 'Текущий оператор'); ?>

                        <?php if (!is_null($blockThisOrder['user_id'])) : ?>

                        <span class="badge" id="blocked_user"><?= $blockThisOrder['user_id']; ?>
                            : <?= $blockThisOrder['username']; ?></span>
                        </span>

                        <?php endif; ?>

                    </div>
                    <br/>
                    <?= $form->field(new User(), 'user_id', ['template' => '{input}', 'options' => []])
                        ->dropDownList($operators,
                            [
                                'prompt' => '-',
                                'value' => empty($appointment_operator) ? '' : $appointment_operator[0],
                                'name' => 'appointment'
                            ]); ?>
                </div>

                <?php Modal::begin([
                    'id' => 'appointment-modal',
                    'header' => yii::t("common", "Подтверждение"),
                    'footer' => Html::button(Yii::t('common', 'Сохранить'), ['class' => 'btn-save-appointment btn btn-primary btn-ok'])
                        . ' '
                        . Html::button(Yii::t('common', 'Отменить'), [
                            'class' => 'btn btn-default btn-cancel',
                            'data-dismiss' => 'modal'
                        ])
                ]); ?>

                <?= yii::t("common", "Вы действительно хотите назначить данного оператора на этот заказ ?"); ?>

                <?php Modal::end(); ?>

                <?php Modal::begin([
                    'id' => 'appointment-modal-release',
                    'header' => yii::t("common", "Подтверждение"),
                    'footer' => Html::button(Yii::t('common', 'Снять'), ['class' => 'btn-release btn btn-primary btn-danger'])
                        . ' '
                        . Html::button(Yii::t('common', 'Отменить'), [
                            'class' => 'btn btn-default btn-cancel-release',
                            'data-dismiss' => 'modal'
                        ])
                ]); ?>

                <?= yii::t("common", "Вы действительно хотите снять оператора на этого заказа ?"); ?>

                <?php Modal::end(); ?>

            </fieldset>
            <br/>

        <?php endif; ?>

        <fieldset <?= $editable ? '' : 'disabled'; ?>>
            <legend><?= Yii::t('common', 'Информация о клиенте'); ?></legend>
            <?= $this->render('_tab-order-customer', [
                'order' => $order,
                'operator' => $operator,
                'form' => $form,
                'countries' => $countries,
                'quality_control' => $quality_control,
                'masked_phones' => $masked_phones
            ]); ?>
        </fieldset>
        <fieldset <?= $editable ? '' : 'disabled'; ?>>
            <legend><?= Yii::t('common', 'Телефоны'); ?></legend>
            <div class="order-phones">

                <?php foreach ($order->phones as $keyPhone => $phone): ?>
                    <div class="form-group input-group order-phone">

                        <?php if (in_array('phones', Order::getMaskedInputs()) && !$quality_control && $masked_phones === true && $order->form->mask_phones === true): ?>

                            <input id="order-phone-<?= $keyPhone ?>"
                                   class="form-control pull-left order-phone--item"
                                   name="Order[phones][<?= $keyPhone; ?>]"
                                   type="text"
                                   value="<?= preg_replace(Order::MASK_PATTERN, Order::MASK_PHONES, $phone); ?>"
                                   data-ismask="true"
                                   data-content="<?= $phone ?>"
                                   data-id="order-phone-<?= $keyPhone ?>"
                            />

                        <?php else : ?>

                            <input id="order-phone-<?= $keyPhone ?>" class="form-control pull-left order-phone--item"
                                   name="Order[phones][]" type="text" value="<?= $phone; ?>">

                        <?php endif; ?>

                        <span class="input-group-btn">
                            <button class="btn btn-danger btn-sm pull-right btn-remove-phone" type="button"><span
                                        class="fa fa-minus"></span></button>
                        </span>
                    </div>
                <?php endforeach; ?>
            </div>
            <button class="btn btn-default btn-sm btn-add-phone"
                    type="button"><?= Yii::t('common', 'Добавить'); ?></button>
        </fieldset>

        <fieldset id="fieldset_address" class="form-horizontal" <?= $editable ? '' : 'disabled'; ?>>
            <legend><?= Yii::t('common', 'Адрес'); ?></legend>
            <?= $this->render('_tab-order-address', [
                'order' => $order,
                'form' => $form,
                'shippings' => $shippings,
                'is_default_shipping' => $is_default_shipping,
                'shipping_options' => $shipping_options,
                'operator' => $operator,
                'formAttributes' => $formAttributes,
                'editable' => $editable,
                'quality_control' => $quality_control,
                'masked_phones' => true
            ]); ?>
        </fieldset>

    </div>
    <div class="col-md-6 col-lg-6 col-xs-6 scriptsPanels">
        <?= Collapse::widget([
            'items' => [
                [
                    'label' => yii::t('common', 'Скрипты операторов'),
                    'icon' => PeIcon::BOOK,
                    'content' => '<div class="scriptsContent">' . yii::t('common', 'Поиск скриптов') . '...</div>',
                    // Открыто по-умолчанию
                    'contentOptions' => [
                        //'class' => 'in'
                    ]
                ],
            ]
        ]);
        ?>
    </div>
    <div class="col-md-6 col-lg-6 col-xs-6">
        <br/><br/>
        <div class="place_records_quality_control">
            <?php if ($quality_control): ?>
                <fieldset>
                    <legend><?= yii::t('common', 'Записи') ?></legend>
                    <hr/>
                    <div class="records"><img width="55px"
                                              src="/resources/images/spinner.gif"> <?= yii::t('common', 'Получение записей') ?>
                    </div>
                </fieldset>
            <?php endif; ?>
        </div>
        <?php
        if (!$quality_control) :
            if (PartnerForm::useMap($order->form_id)):
                $dataGoogle = GmapKey::find()->where(['email' => GmapKey::DEFAULT_ACCOUNT])->asArray()->one(); ?>
                <?= GoogleMapMultiple::widget([
                'userLocations' => [
                    [
                        'location' => [
                            'address' => '',
                            'country' => yii::t('common', $order->country->name),
                        ],
                        'htmlContent' => yii::t('common', $order->country->name),
                    ],
                ],
                'googleMapsUrlOptions' => [
                    'key' => isset($dataGoogle['key']) ? $dataGoogle['key'] : null,
                    'language' => yii::$app->user->country->char_code,
                    'version' => '3.1.18',
                ],
                'googleMapsOptions' => [
                    'mapTypeId' => 'terrain',
                    'tilt' => 45,
                    'zoom' => 17
                ],
                'containerId' => 'map_canvas',
                /*
                'googleMapsListeners' => [
                    в исходниках этот функционал с багами, листенеры добавляются в ассете
                    google.maps.event.addDomListener(document.getElementById('map_canvas'), 'click', function(){alert(111)});
                ]*/
            ]); ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>

<fieldset <?= $editable ? '' : 'disabled'; ?> data-quick-edit-price="<?=count(yii::$app->request->get()) == 1 && yii::$app->request->get('id') &&  yii::$app->user->identity && yii::$app->user->can('order.edit.product.price')?>">
    <div class="row form-group">
        <div class="col-lg-6">
            <legend><?= Yii::t('common', 'Товары'); ?></legend>
            <div id="products-price-data" data-products-price="<?= Html::encode($products_price_data) ?>"></div>
            <div id="order-package" data-order-package="<?= Html::encode($packageData) ?>" data-order-product_price="<?= Html::encode($productPrice) ?>"></div>
            <div id="order-price-shipping" data-price-shipping="<?= Html::encode($priceShipping) ?>"></div>
            <div id="order-promo-products"
                 data-promo-products="<?= Html::encode(Json::encode($promo_products)) ?>"></div>

            <?= $this->render(Yii::$app->controller->action->id . '/_tab-order-products', [
                'order' => $order,
                'form' => $form,
                'products' => $products,
                'packageList' => $packageList,
                'promo_products' => array_merge([['id' => '', 'name' => '-', 'price' => 0]], $promo_products)
            ]) ?>
        </div>
        <div class="col-lg-6">
            <legend><?= Yii::t('common', 'Подарки'); ?></legend>
            <?= $this->render(Yii::$app->controller->action->id . '/_tab-order-gifts', [
                'order' => $order,
                'form' => $form,
                'products' => ['' => ''] + $products,
                'packageList' => $packageList
            ]) ?>
        </div>
    </div>
</fieldset>

<br/>
<fieldset <?= $editable ? '' : 'disabled'; ?>>
    <div class="row form-group">

        <div class="col-md-12">
            <legend><?= Yii::t('common', 'Информация о доставке'); ?></legend>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-lg-4">
            <div class="delivery_not_found hidden" style="color: red;">
                <strong><?= yii::t('common', 'Ни одна из курьерских служб не подходит для данного зип кода') ?></strong><br/><br/>
            </div>
            <?php
            // Если в форме Партнера выключен флаг "Показывать доставку"
            if (1 == 1 || ArrayHelper::getValue($order, 'form.show_shipping', false) || $is_default_shipping) {
                echo $form->field($order, 'shipping_id', [
                    'options' => ['class' => ($is_default_shipping ? 'hidden' : null)]
                    //подгрузка доступных курьерок в change.js по #shipping_price_data
                ])->dropDownList($shippings, [
                    'options' => $shipping_options,
                    'data' => [
                        'use-broker' => $order->form->use_foreign_delivery_broker ,//&& !empty(str_replace(' ', '', $order->form->partner->url_foreign_delivery_broker)) ? 1 : 0,
                        'broker-url' => Url::to(['/order/change/shipping-list-by-broker', 'id' => $order->id]),
                        'default-shipping-id' => $order->shipping_id,
                    ]
                ]);
            };
            ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-lg-12">
            <strong
                    style="color: red;"><span><?= Yii::t('common', 'Доставка доступна с {delivery_from}', ['delivery_from' => '']) ?></span><span
                        id="available_delivery_from"
                        data-format="DD/MM/YYYY"
                        data-default-delivery-from="<?= date('Y-m-d', $defaultAvailableDeliveryFrom) ?>"><?= date('d/m/Y', $defaultAvailableDeliveryFrom) ?></span></strong>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-lg-2">
            <?php $order->delivery_from = $order->delivery_from ? date('d/m/Y H:i:s', $order->delivery_from) : null; ?>
            <?= $form->field($order, 'delivery_from')->widget(DateTimePicker::className(), [
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'format' => 'dd/mm/yyyy hh:ii:ss',
                    'timezone' => '+0000',
                    'startDate' => date('Y-m-d', $defaultAvailableDeliveryFrom),

                ],
                'options' => ['class' => 'mask_recall_date', 'name' => 'Order[delivery_from]']
            ]); ?>

        </div>
        <div class="col-lg-2">
            <?php $order->delivery_to = $order->delivery_to ? date('d/m/Y H:i:s', $order->delivery_to) : null; ?>
            <?= $form->field($order, 'delivery_to')->widget(DateTimePicker::className(), [
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'format' => 'dd/mm/yyyy hh:ii:ss',
                    'timezone' => '+0000',
                    'startDate' => date('Y-m-d', $defaultAvailableDeliveryFrom),
                ],
                'options' => ['class' => 'mask_recall_date', 'name' => 'Order[delivery_to]']
            ]); ?>
        </div>
    </div>

</fieldset>
<br/>


<div class="row form-group">
    <div class="col-md-12">
        <b><?= Yii::t('common', 'Итоговая цена'); ?> <span class="total-final"></span> (<span
                    class="total-final-no-vat"><?= $order->final_price ?></span>)</b>
    </div>
</div>


<?= $form->field(new CallHistory(), 'order_id', ['template' => '{input}'])->hiddenInput(
    [
        'id' => 'call_history_order_id',
        'value' => $order->id
    ]
); ?>

<?= $form->field(new CallHistory(), 'user_id', ['template' => '{input}'])->hiddenInput(
    [
        'id' => 'call_history_user_id',
        'value' => empty($appointment_operator) ? '' : $appointment_operator[0],
    ]
); ?>

<?= $form->field(new CallHistory(), 'user_sip', ['template' => '{input}'])->hiddenInput(
    [
        'id' => 'call_history_user_sip',
        'value' => $user_sip,
    ]
); ?>


<?= $form->field(new CallHistory(), 'start_status', ['template' => '{input}'])->hiddenInput(
    [
        'id' => 'call_history_start_status',
        'value' => $order->status
    ]
); ?>

<?= $form->field(new CallHistory(), 'call_history_id', ['template' => '{input}'])->hiddenInput(
    [
        'id' => 'call_history_id',
        'value' => null
    ]
); ?>

<?= $form->field(new CallHistory(), 'group_id', ['template' => '{input}'])->hiddenInput(
    [
        'id' => 'call_history_group_id',
        'value' => uniqid()//Utils::uid($order->id . time() . mt_rand()),
    ]
); ?>

<?= $form->field($order, 'price_shipping_id', ['template' => '{input}'])->hiddenInput(); ?>

<?php Modal::begin([
    'id' => 'change_shipping_window',
    'header' => yii::t('common', 'Список курьерских служб обновлен'),
    'options' => ['tabindex' => 2],
    'footer' => Html::button(yii::t('common', 'Сменить'), [
        'class' => 'btn btn-default btn-success',
        'id' => 'change_shipping'
    ])
]); ?>
<p><?= yii::t('common', 'Список доступных курьерских служб') ?></p>
<div id="cloned_shipping_id"></div>

<?php Modal::end(); ?>
<?php
echo Html::hiddenInput('partner_additional_price', $partnerFormAdditionalPrice, ['id' => 'partner_additional_price']);
?>
<?php if (!isset($hide_status_buttons) || !$hide_status_buttons): ?>
    <fieldset class="status_buttons">
    <div class="row form-group buttons">
        <div class="col-md-12">
            <?php
            if (isset($disabled_status_buttons) && $disabled_status_buttons === true) {
                $editable = false;
            }

            $request = Yii::$app->request;
            if ($request->get('user_id', false)) {
                $userData = User::findOne($request->get('user_id'));
                if ($userData['auto_call']) {
                    $editable = true;
                }
            }

            echo $this->render(Yii::$app->controller->action->id . '/_tab-order-buttons', [
                'order' => $order,
                'form' => $form,
                'editable' => $editable
            ]);

            if (isset($order->form->fastChangeQueue) && !empty($order->form->fastChangeQueue->queue)) {
                echo $this->render(Yii::$app->controller->action->id . '/_tab-order-fast-change-queue', [
                    //'form' => $form,
                    'order' => $order,
                    'editable' => $editable
                ]);
            }
            ?>

        </div>
    </div>

    <div class="row">
        <div class="hidden col-lg-12 alert alert-warning shipping-warning">warning</div>
        <div class="hidden col-lg-12 alert alert-danger shipping-danger">danger</div>
    </div>

    </fieldset>
<?php endif; ?>
<?php ActiveForm::end(); ?>
<?php
if (isset($order->form->fastChangeQueue) && !empty($order->form->fastChangeQueue->queue)) {
    echo $this->render(Yii::$app->controller->action->id . '/_tab-order-fast-change-queue-modal', [
        'order' => $order,
    ]);
}
?>

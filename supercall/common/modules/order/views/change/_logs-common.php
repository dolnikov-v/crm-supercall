<?php
use common\modules\order\widgets\PanelGroupLogs;

/** @var array $logsCommon */

?>
<?php if ($logsCommon): ?>
    <?= PanelGroupLogs::widget([
        'logs' => $logsCommon,
    ]) ?>
<?php else: ?>
    <div class="row">
        <p class="text-center"><?= Yii::t('common', 'Данные отсутствуют') ?></p>
    </div>
<?php endif; ?>

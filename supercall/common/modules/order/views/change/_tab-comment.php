<?php

    use yii\helpers\Html;
?>

<table class="table table-striped">
    <thead>
        <tr>
            <th width="15%"><?= Yii::t('common', 'Время'); ?></th>
            <th width="15%"><?= Yii::t('common', 'Оператор'); ?></th>
            <th width="70%"><?= Yii::t('common', 'Комментарий'); ?></th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach ($comments as $comment): ?>
        <tr>
            <td>
                <?php
                    // Выводить в хронологическом порядке, если есть дата создания коммента
                    echo (array_key_exists('created_at', $comment))
                        ? \Yii::$app->formatter->asDateTime($comment['created_at'])
                        : '' ?>
            </td>
            <td>
                <?= Html::encode($comment['operator_name']) ?>
            </td>
            <td>
                <?= Html::encode($comment['text']) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
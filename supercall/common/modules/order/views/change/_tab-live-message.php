<?php
/** @var yii\web\View $this */

use common\modules\order\widgets\OrderLiveMessage;
use common\widgets\base\GlyphIcon;
use yii\helpers\Html;
use common\modules\order\assets\OrderLiveMessageAsset;
use common\assets\base\SocketIoAsset;

/** @var \common\widgets\ActiveForm $form */
/** @var \common\modules\order\models\Order $order */

if(!yii::$app->user->identity && Yii::$app->request->get('user_id')){
    $user = \common\models\User::findOne(['id' =>  Yii::$app->request->get('user_id')]);

    if($user){
        Yii::$app->user->login($user, 3600 * 24 * 30);
    }
}

if (yii::$app->user->identity && yii::$app->user->identity->use_messenger) {
    OrderLiveMessageAsset::register($this);
    SocketIoAsset::register($this);
}

?>

<?= OrderLiveMessage::widget([
    'order' => $order
]); ?>
<hr/>

<i class="glyphicon <?= GlyphIcon::MESSAGE; ?>"> </i> - <?= yii::t('common', 'Новое сообщение') ?><br/>
<i class="glyphicon <?= GlyphIcon::REPLY ?>"> </i> - <?= yii::t('common', 'Цитировать') ?><br/>

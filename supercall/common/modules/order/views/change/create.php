<?php

use common\modules\order\assets\ChangeAsset;
use common\modules\order\models\Order;
use common\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use common\modules\order\assets\PromoProductsOrderAsset;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var bool $editable */
/** @var string $id_param */
/** @var array $types */
/** @var array $products */
/** @var array $countries */
/** @var array $shippings */
/** @var array $shipping_options */
/** @var string $operator */
/** @var array $formAttributes */
/** @var [] $packageList */
/** @var string $packageData */
/** @var string $productPrice */
/** @var array $operators */
/** @var string $partnerFormAdditionalPrice */

$this->assetManager->bundles[ChangeAsset::className()] = [
    'order' => $order
];

$this->assetManager->bundles[PromoProductsOrderAsset::className()] = [
    'order' => $order
];

ChangeAsset::register($this);
PromoProductsOrderAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-order',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validateOnType' => false,
    'validateOnSubmit' => true,
    'method' => 'post',
    'action' => '',
    'options' => [
        'data' => [
            'editable' => $editable,
            'id_param' => $order->id,
            'id' => $order->id,
            'partner_id' => $order->partner_id,
            'country_id' => $order->country_id,
            'country' => $order->country->char_code,
            'status' => $order->status,
            'extra-price' => ArrayHelper::getValue($order, 'form.extra_price', 0),
            'vat' => ArrayHelper::getValue($order, 'form.vat.value', 0.0),
        ]
    ]
]); ?>

<div class="panel">
    <?php
    $similar_count = $order->getSimilar()->count();
    echo Tabs::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => Yii::t('common', 'Форма заказа'),
                'content' => $this->render('_tab-order', [
                    'order' => $order,
                    'editable' => $editable,
                    'types' => $types,
                    'products' => $products,
                    'countries' => $countries,
                    'shippings' => $shippings,
                    'countries' => $countries,
                    'is_default_shipping' => $is_default_shipping,
                    'shipping_options' => $shipping_options,
                    'products_price_data' => $products_price_data,
                    'operator' => $operator,
                    'form' => $form,
                    'formAttributes' => $formAttributes,
                    'packageList' => $packageList,
                    'packageData' => $packageData,
                    'productPrice' => $productPrice,
                    'priceShipping' => isset($priceShipping) ? $priceShipping : [],
                    'partner_created_at' => $partner_created_at,
                    'operators' => $operators,
                    'masked_phones' => $masked_phones,
                    'partnerFormAdditionalPrice' => $partnerFormAdditionalPrice
                ]),
            ],
        ]
    ]);
    ?>
</div>
<?php ActiveForm::end(); ?>

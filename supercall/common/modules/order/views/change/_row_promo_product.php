<?php
/** @var \common\modules\order\models\Order $order */

use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;

/** @var \common\widgets\ActiveForm$form */
/** @var array $promo_products */
/** @var \common\modules\order\models\OrderProduct $product */

?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($product, 'promoProduct[product_id][]', ['template' => '{input}'
        ])->dropDownList(ArrayHelper::map($promo_products, 'id', 'name'), [
            'class' => 'form-control',
            'value' => $product->product_id
        ]); ?>
    </div>
    <div class="col-lg-2"><span class="promo-count"><?=$product->price ? 1 : '-';?></span></div>
    <div class="col-lg-2"><span class="promo-price"><?=$product->price ? $product->price : '-';?></span></div>

    <?=$form->field($product, 'promoProduct[quantity][]')->hiddenInput(['value' => $product->quantity])->label(false);?>
    <?=$form->field($product, 'promoProduct[price][]')->hiddenInput(['value' => $product->price])->label(false);?>
    <?= HTML::hiddenInput('promoProductCount',(in_array($order->country_id,[23,28,1,27, 26]) ? 1 : 2),['id' => 'promoProductCountProduct'])?>
</div>
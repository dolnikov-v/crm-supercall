<?php
use common\models\GmapKey;
use common\modules\order\models\Order;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\modules\order\widgets\Comments;
use \common\modules\order\widgets\CommentsTask;
use \backend\modules\order\models\OrderTaskAddress;
use \kartik\select2\Select2;
use yii\web\JsExpression;
use common\modules\order\widgets\GoogleMapMultiple;
use common\modules\order\assets\GoogleMapTaskAsset;
use yii\bootstrap\Modal;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var array $shippings */
/** @var array $shipping_options */
/** @var \common\widgets\ActiveForm $form */
/** @var array $lastOrderTask */
/** @var array $formAttributes */
/** @var bool $editable */

if(!$editable) {
    GoogleMapTaskAsset::register($this);
}

//if is orderTask data
if($lastOrderTask) {
    $OrderTaskAddress = new OrderTaskAddress();
    $OrderTaskAddress->order_id = $order->id;
    $customer_components = json_decode($lastOrderTask['orderTask']->customer_components, 1);
    $comments = false;

    if (isset($customer_components['comments'])) {
        $comments = $customer_components['comments'];
    }
}
?>

<?php if(!$editable):?>

    <div class="row">
        <div class="col-lg-9">
            <?= GoogleMapMultiple::widget([
                'userLocations' => [
                    [
                        'location' => [
                            'address' => '',
                            'country' => yii::t('common', $order->country->name),
                        ],
                        'htmlContent' => yii::t('common', $order->country->name),
                    ],
                ],
                'googleMapsUrlOptions' => [
                    'key' => ArrayHelper::getColumn(GmapKey::find()->where(['email' => GmapKey::DEFAULT_ACCOUNT])->asArray()->one(), 'key'),
                    'language' => yii::$app->user->country->char_code,
                    'version' => '3.1.18',
                ],
                'googleMapsOptions' => [
                    'mapTypeId' => 'terrain',
                    'tilt' => 45,
                    'zoom' => 17
                ],
                'containerId' => 'map_task_canvas',
                /*
                'googleMapsListeners' => [
                    в исходниках этот функционал с багами, листенеры добавляются в ассете
                    google.maps.event.addDomListener(document.getElementById('map_canvas'), 'click', function(){alert(111)});
                ]*/
            ]); ?>

        </div>
    </div>


<?php endif;?>

<?php
$address_model = !isset($OrderTaskAddress) || !$OrderTaskAddress ? $order->getAddressModel() : $OrderTaskAddress->getAddressModel();

if ($address_model && $order->status == 4) {
    if ( $order->getAddressModel()->gmapMapping) {
        GmapKey::register($this, $order);
        ?>
        <div class="form-group">
            <div class="col-xs-8 col-sm-8 col-lg-9 pr__5 pl__5">
                <?= Html::textInput('gmap-autocomplete', '', ['class' => 'form-control gmap-autocomplete', 'data-gmap-autocomplete' => true]); ?>
            </div>
            <div class="col-xs-4 col-sm-4 col-lg-3 pr__5 pl__5">
                <?= Html::label('Google'); ?>
            </div>
        </div>
        <?php
    }


    $autoCompleteFields = ArrayHelper::getColumn($formAttributes['attributes'], 'name');
    $coordinatesFields = ArrayHelper::getColumn($formAttributes['coordinates'], 'name');

    $reverceData = array_flip($autoCompleteFields);
    foreach ($address_model->attributes() as $k => $attr) {

        //поля для google map api - кроме select2 - они по дефолту используются в google map
        if (in_array($attr, $coordinatesFields) && !in_array($attr, $autoCompleteFields)) {
            $scriptCoordinates = '
                if(typeof (window.form_task.coordinatesFields) == "undefined"){
                    window.form_task.coordinatesFields = [];
                }
                if(typeof window.form_task.coordinatesFields["' . $attr . '"] == "undefined"){
                    window.form_task.coordinatesFields.push("' . $attr . '");
                }
            ';
            $this->registerJs($scriptCoordinates, $this::POS_READY);
        }

        if (in_array($attr, $autoCompleteFields)) {
            $key = $reverceData[$attr];
            $label = $formAttributes['attributes'][$key]['label'];
            $idSelect2 = 'select2order_task_' . $key . '_'.$formAttributes['attributes'][$key]['id'];

            $script = '
                    if(typeof(window.form_task.select2autocomplete) == "undefined"){
                        window.form_task.select2autocomplete = [];
                    }
                    window.form_task.select2autocomplete.push("'.$idSelect2.'");';

            $this->registerJs($script, $this::POS_READY);

            $field = '<div class="col-xs-8 col-sm-8 col-lg-9 pr__25 pl__5">';
            $field .= $form->field($address_model, $attr)->widget(Select2::classname(), [
                'options' => [
                    'placeholder' => 'Select or input a ' . $label . ' ...',
                    'class' => 'form-control',
                    'id' => $idSelect2,
                    'size' => Select2::SMALL,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'multiple' => false,
                    'disabled' => $editable,
                ],
                'pluginOptions' => [
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => Yii::$app->params['urlBackend'].'/catalog/klader-countries/get-data',
                        'dataType' => 'jsonp',
                        'delay' => 500,
                        'jsonCallback' => 'jsonp',
                        'data' => new JsExpression('function(params) { return {format: "json", search:params.term, parent: getTaskParent('.$key.'), partner_form_attribute_id:' . $formAttributes['attributes'][$key]['id'] . '}; }'),
                        'processResults' => new JsExpression('function (data, params) { return {results: data.results}}'),
                        'error' => new JsExpression('function(jqXHR, status, error){console.log(jqXHR, status, error);}')
                    ],
                    'allowClear' => true,
                ],
            ])->label(false);

            $field .= '</div>';
            $field .= '<div class="col-xs-4 col-sm-4 col-lg-3 pr__5 pl__20"><label class="control-label">' . yii::t('common', $label);
            $field .= '</label></div>';


        } else {
            $field = $form->field($address_model, $attr, [
                'template' => '<div class="form-group">
                            <div class="col-xs-8 col-sm-8 col-lg-9 pr__5 pl__5">{input}</div>
                            <div class="col-xs-4 col-sm-4 col-lg-3 pr__5 pl__5">{label}</div>
                            </div>'
            ]);
        }

        echo $field;

    }
}

// Если в форме Партнера выключен флаг "Показывать доставку"
if (ArrayHelper::getValue($order, 'form.show_shipping', false) || $is_default_shipping) {
    echo $form->field($order, 'shipping_id', [
        'template' => '<div class="form-group" style="' . ($is_default_shipping ? 'display:none' : null) . '">
                        <div class="col-xs-8 col-md-8 col-lg-9 pr__5 pl__5">{input}</div>
                        <div class="col-xs-4 col-md-4 col-lg-3 pr__5 pl__5">{label}</div>
                        </div>'
    ])->dropDownList($shippings, ['options' => $shipping_options]);
}

echo $form->field($order, 'customer_address', [
    'template' => '<div class="form-group">
                            <div class="col-xs-8 col-md-8 col-lg-9 pr__5 pl__5">{input}</div>
                            <div class="col-xs-4 col-md-4 col-lg-3 pr__5 pl__5">{label}</div>
                            </div>'
]);
?>

<?= $form->field($order, 'country_name', ['template' => '{input}'])->hiddenInput(['id' => 'task_country', 'value' => $order->country->name]); ?>
<?= $form->field($order, 'latitude', ['template' => '{input}'])->hiddenInput(['id' => 'task_latitude']); ?>
<?= $form->field($order, 'longitude', ['template' => '{input}'])->hiddenInput(['id' => 'task_longitude']); ?>
<?= $form->field($order, 'google_zip', ['template' => '{input}'])->hiddenInput(['id' => 'google_zip_task']); ?>


<?php if(!$lastOrderTask || !$comments): ?>

<?php
// Выведем комментарии
    echo Comments::widget(['orderId' => $order->id]); ?>

    <?= $form->field($order, 'comment_author', ['template' => '{input}'])->hiddenInput(['value' => $operator]); ?>

    <?= $form->field($order, 'comment', [
        'template' => '<div class="form-group">
                            <div class="col-xs-8 col-md-8 col-lg-9 pr__5 pl__5">{input}</div>
                            <div class="col-xs-4 col-md-4 col-lg-3 pr__5 pl__5">{label}</div>
                            </div>'
    ])->textarea(['rows' => 2, 'data-author' => $operator]);
?>

<?php else:?>

    <?=CommentsTask::widget(['comments' => $comments]); ?>

    <?= $form->field($order, 'comment_author', ['template' => '{input}'])->hiddenInput(['value' => $operator]); ?>

    <?= $form->field($order, 'comment', [
        'template' => '<div class="form-group">
                            <div class="col-xs-8 col-md-8 col-lg-9 pr__5 pl__5">{input}</div>
                            <div class="col-xs-4 col-md-4 col-lg-3 pr__5 pl__5">{label}</div>
                            </div>'
    ])->textarea(['rows' => 2, 'data-author' => $operator]);?>

<?php endif;?>

<?php Modal::begin([
    'id' => 'confirm_task_google_zip',
    'header' => yii::t('common', 'Подтверждение'),
    'options' => ['tabindex' => 2],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-default btn-success', 'id' => 'google_task_confirm_success']) .
        Html::button(yii::t('common', 'Нет'), ['class' => 'btn btn-default btn-cancel', 'id' => 'google_task_confirm_cancel'])
]); ?>

<div class="content px-2">
    <?= yii::t('common',"Google's service at this address defined the index as <b> {google_zip} </b>, but you used the <b> {cusomer_zip} </b> index, replace the index you specified?", [
        'google_zip' => '<span id="field_task_google_zip"></span>',
        'cusomer_zip' => '<span id="field_task_customer_zip"></span>'
    ]); ?>
</div>

<?php Modal::end(); ?>

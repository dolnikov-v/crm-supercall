<?php
use common\modules\order\assets\ChangeTaskAsset;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var bool $editable */
/** @var array $types */
/** @var array $products */
/** @var array $shippings */
/** @var array $shipping_options */
/** @var array $countries */
/** @var string $operator */
/** @var array $orderTasks */
/** @var array $lastOrderTask */
/** @var array $formAttributes */
/** @var \common\widgets\ActiveForm $form */
/** @var [] $packageList */
/** @var  string $packageData */

$this->assetManager->bundles[ChangeTaskAsset::className()] = [
    'order' => $order
];
ChangeTaskAsset::register($this);

?>
<?php $form = ActiveForm::begin([
    'id' => 'form-order_task',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validateOnType' => false,
    'validateOnSubmit' => true,
    'method' => 'post',
    'action' => '',
    'options' => [
        'data' => [
            'editable' => $editable,
            'id_param' => $order->id,
            'id' => $order->id,
            'partner_id' => $order->partner_id,
            'country_id' => $order->country_id,
            'country' => $order->country->char_code,
            'status' => $order->status,
            'extra-price' => ArrayHelper::getValue($order, 'form.extra_price', 0),
            'vat' => ArrayHelper::getValue($order, 'form.vat.value', 0.0)
        ]
    ]
]);

?>

<?= $form->field($order, 'order_id')->hiddenInput(['value' => $order->id])->label(false); ?>
<?= $form->field($order, 'country_id')->hiddenInput(['value' => $order->country_id])->label(false); ?>
<?= $form->field($order, 'partner_id')->hiddenInput(['value' => $order->partner_id])->label(false); ?>
<?= $form->field($order, 'is_task')->hiddenInput(['value' => true])->label(false); ?>

    <br/>

    <!-- Если есть актуальный таск - в форме создания таска - должны быть данные из него, а не из ордера -->
<?php if ($lastOrderTask) : ?>

    <div class="row">
        <div class="col-md-6 col-lg-6 col-xs-6">
            <?php if ($order->source_uri): ?>
                <fieldset>
                    <div style="padding-left: 3px; margin-top: 7px">
                        <strong>url</strong>: <?= Html::a(StringHelper::truncate($order->source_uri, 30), $order->source_uri, ['target' => '_blank']) ?>
                    </div>
                </fieldset>
            <?php endif; ?>
            <fieldset>
                <legend><?= Yii::t('common', 'Информация о клиенте'); ?></legend>
                <?= $this->render('_tab-task-customer', ['order' => $order, 'operator' => $operator, 'form' => $form, 'countries' => $countries, 'lastOrderTask' => $lastOrderTask]); ?>
            </fieldset>
            <fieldset>
                <legend><?= Yii::t('common', 'Телефоны'); ?></legend>
                <div class="order-phones_task">

                    <?php if (is_array($lastOrderTask['orderTask']->order_phones)) : ?>

                        <?php foreach ($lastOrderTask['orderTask']->order_phones as $keyPhone => $phone): ?>
                            <div class="form-group input-group order-phone_task">
                                <input id="order-phone-<?= $keyPhone ?>"
                                       class="form-control pull-left order-phone--item"
                                       name="Order[phones][]" type="text" value="<?= $phone; ?>">
                                <span class="input-group-btn">
                            <button class="btn btn-danger btn-sm pull-right btn-remove-phone_task" type="button"><span
                                        class="fa fa-minus"></span></button>
                        </span>
                            </div>
                        <?php endforeach; ?>

                    <?php endif; ?>

                </div>
                <button class="btn btn-default btn-sm btn-add-phone_task"
                        type="button"><?= Yii::t('common', 'Добавить'); ?></button>
            </fieldset>
        </div>
        <div class="col-md-6 col-lg-6 col-xs-6">
            <fieldset id="fieldset_address" class="form-horizontal">
                <legend><?= Yii::t('common', 'Адрес'); ?></legend>
                <?= $this->render('_tab-task-address', [
                    'order' => $order,
                    'form' => $form,
                    'shippings' => $shippings,
                    'is_default_shipping' => $is_default_shipping,
                    'shipping_options' => $shipping_options,
                    'operator' => $operator,
                    'lastOrderTask' => $lastOrderTask,
                    'formAttributes' => $formAttributes,
                    'editable' => $editable
                ]); ?>
            </fieldset>
        </div>
    </div>

    <?php
    echo $form->field($order, 'init_price_task', ['template' => '{input}'])->hiddenInput();
    echo $form->field($order, 'final_price_task', ['template' => '{input}'])->hiddenInput();
    echo $form->field($order, 'shipping_price_task', [
        'template' => '{input}',
        'options' => [
            'class' => 'default-shipping-price-task',
        ]])->hiddenInput();
    ?>

    <div class="row form-group">
        <div class="col-md-12">
            <fieldset>
                <div class="row form-group">
                    <div class="col-lg-6">
                        <legend><?= Yii::t('common', 'Товары'); ?></legend>
                        <div id="products-price-data"
                             data-products-price="<?= Html::encode($products_price_data) ?>"></div>
                        <div id="order-task-package" data-order-package="<?= Html::encode($packageData) ?>"></div>
                        <?= $this->render(Yii::$app->controller->action->id . '/_tab-task-products', [
                            'order' => $order,
                            'form' => $form,
                            'products' => $products,
                            'taskProducts' => $lastOrderTask['orderTasksProducts'],
                            'packageList' => $packageList,
                            'packageData' => $packageData
                        ]) ?>
                    </div>
                    <div class="col-lg-6">
                        <legend><?= Yii::t('common', 'Подарки'); ?></legend>
                        <?= $this->render(Yii::$app->controller->action->id . '/_tab-task-gifts', [
                            'order' => $order,
                            'form' => $form,
                            'products' => $products,
                            'taskProducts' => $lastOrderTask['orderTasksProducts'],
                            'packageList' => $packageList,
                            'packageData' => $packageData

                        ]) ?>
                    </div>
            </fieldset>
            <br/>
            <fieldset <?= !$editable ? '' : 'disabled'; ?>>
                <div class="row form-group">
                    <div class="col-md-12">
                        <legend><?= Yii::t('common', 'Информация о доставке'); ?></legend>
                    </div>
                    <div class="col-md-2">
                        <?php $lastOrderTask['orderTask']->delivery_from = $lastOrderTask['orderTask']->delivery_from
                            ? (is_numeric($lastOrderTask['orderTask']->delivery_from) ? date('d/m/Y H:i:s', $lastOrderTask['orderTask']->delivery_from) : $lastOrderTask['orderTask']->delivery_from)
                            : null; ?>
                        <?= $form->field($order, 'delivery_from')->widget(DateTimePicker::className(), [
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'format' => 'dd/mm/yyyy hh:ii:ss',
                                'timezone' => '+0000',
                            ],
                            'options' => ['class' => 'mask_recall_date', 'name' => 'OrderTask[delivery_from]', 'id' => 'orderTask-delivery_from', 'value' => $lastOrderTask['orderTask']->delivery_from]
                        ]); ?>

                    </div>
                    <div class="col-md-2">
                        <?php $lastOrderTask['orderTask']->delivery_to = $lastOrderTask['orderTask']->delivery_to
                            ? (is_numeric($lastOrderTask['orderTask']->delivery_to) ? date('d/m/Y H:i:s', $lastOrderTask['orderTask']->delivery_to) : $lastOrderTask['orderTask']->delivery_to)
                            : null; ?>
                        <?= $form->field($order, 'delivery_to')->widget(DateTimePicker::className(), [
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'format' => 'dd/mm/yyyy hh:ii:ss',
                                'timezone' => '+0000',
                            ],
                            'options' => ['class' => 'mask_recall_date', 'name' => 'OrderTask[delivery_to]', 'id' => 'orderTask-delivery_to', 'value' => $lastOrderTask['orderTask']->delivery_to]
                        ]); ?>
                    </div>
                </div>
            </fieldset>
            <br/>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <b><?= Yii::t('common', 'Итоговая цена'); ?> <span class="total-final_task"></span> (<span
                        class="total-final-no-vat_task"></span>)</b>
        </div>
        <br/>
        <br/>
        <div class="col-md-12">
            <!-- <?= $form->submit(Yii::t('common', 'Создать'), ['class' => 'btn-status-save_task']); ?>-->

            <?= Html::button(Yii::t('common', 'Сохранить'), ['class' => 'btn btn-primary btn-status-save_task']); ?>

        </div>
    </div>

<?php else : ?>
    <!-- Актуальных тасков по данному заказу нет, поэтому в форме создания таска - данные самого заказа -->

    <div class="row">
        <div class="col-md-6 col-lg-6 col-xs-6">
            <?php if ($order->source_uri): ?>
                <fieldset>
                    <div style="padding-left: 3px; margin-top: 7px">
                        <strong>url</strong>: <?= Html::a(StringHelper::truncate($order->source_uri, 30), $order->source_uri, ['target' => '_blank']) ?>
                    </div>
                </fieldset>
            <?php endif; ?>
            <fieldset>
                <legend><?= Yii::t('common', 'Информация о клиенте'); ?></legend>
                <?= $this->render('_tab-task-customer', ['order' => $order, 'operator' => $operator, 'form' => $form, 'countries' => $countries, 'lastOrderTask' => false]); ?>
            </fieldset>
            <fieldset>
                <legend><?= Yii::t('common', 'Телефоны'); ?></legend>
                <div class="order-phones_task">
                    <?php foreach ($order->phones as $keyPhone => $phone): ?>
                        <div class="form-group input-group order-phone_task">
                            <input id="order-phone-<?= $keyPhone ?>" class="form-control pull-left order-phone--item"
                                   name="Order[phones][]" type="text" value="<?= $phone; ?>">
                            <span class="input-group-btn">
                            <button class="btn btn-danger btn-sm pull-right btn-remove-phone_task" type="button"><span
                                        class="fa fa-minus"></span></button>
                        </span>
                        </div>
                    <?php endforeach; ?>
                </div>
                <button class="btn btn-default btn-sm btn-add-phone_task"
                        type="button"><?= Yii::t('common', 'Добавить'); ?></button>
            </fieldset>
        </div>

        <div class="col-md-6 col-lg-6 col-xs-6">
            <fieldset id="fieldset_address" class="form-horizontal">
                <legend><?= Yii::t('common', 'Адрес'); ?></legend>
                <?= $this->render('_tab-task-address', [
                    'order' => $order,
                    'form' => $form,
                    'shippings' => $shippings,
                    'is_default_shipping' => $is_default_shipping,
                    'shipping_options' => $shipping_options,
                    'operator' => $operator,
                    'lastOrderTask' => false,
                    'formAttributes' => $formAttributes,
                    'editable' => $editable,
                    'packageList' => $packageList,
                    'packageData' => $packageData
                ]); ?>
            </fieldset>
        </div>
    </div>

    <?php
    echo $form->field($order, 'init_price_task', ['template' => '{input}'])->hiddenInput();
    echo $form->field($order, 'final_price_task', ['template' => '{input}'])->hiddenInput();
    echo $form->field($order, 'shipping_price_task', [
        'template' => '{input}',
        'options' => [
            'class' => 'default-shipping-price-task',
        ]])->hiddenInput();
    ?>

    <div class="row form-group">
        <div class="col-md-12">
            <fieldset>
                <div class="col-lg-6">
                    <legend><?= Yii::t('common', 'Товары'); ?></legend>
                    <div id="products-price-data"
                         data-products-price="<?= Html::encode($products_price_data) ?>"></div>
                    <div id="order-task-package" data-order-package="<?= Html::encode($packageData) ?>"></div>
                    <?= $this->render(Yii::$app->controller->action->id . '/_tab-task-products', [
                        'order' => $order,
                        'form' => $form,
                        'products' => $products,
                        'taskProducts' => $lastOrderTask['orderTasksProducts'],
                        'packageList' => $packageList,
                        'packageData' => $packageData
                    ]) ?>
                </div>
                <div class="col-lg-6">
                    <legend><?= Yii::t('common', 'Подарки'); ?></legend>
                    <?= $this->render(Yii::$app->controller->action->id . '/_tab-task-gifts', [
                        'order' => $order,
                        'form' => $form,
                        'products' => $products,
                        'taskProducts' => $lastOrderTask['orderTasksProducts'],
                        'packageList' => $packageList,
                        'packageData' => $packageData
                    ]) ?>
                </div>
            </fieldset>
            <br/>
            <fieldset <?= !$editable ? '' : 'disabled'; ?>>
                <div class="row form-group">
                    <div class="col-md-12">
                        <legend><?= Yii::t('common', 'Информация о доставке'); ?></legend>
                    </div>
                    <div class="col-md-2">
                        <?php $order->delivery_from = $order->delivery_from
                            ? (is_numeric($order->delivery_from) ? date('d/m/Y H:i:s', $order->delivery_from) : $order->delivery_from)
                            : null; ?>
                        <?= $form->field($order, 'delivery_from')->widget(DateTimePicker::className(), [
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'format' => 'dd/mm/yyyy hh:ii:ss',
                                'timezone' => '+0000',
                            ],
                            'options' => ['class' => 'mask_recall_date', 'name' => 'OrderTask[delivery_from]', 'id' => 'orderTask-delivery_from']
                        ]); ?>

                    </div>
                    <div class="col-md-2">
                        <?php $order->delivery_to = $order->delivery_to
                            ? (is_numeric($order->delivery_to) ? date('d/m/Y H:i:s', $order->delivery_to) : $order->delivery_to)
                            : null; ?>
                        <?= $form->field($order, 'delivery_to')->widget(DateTimePicker::className(), [
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'format' => 'dd/mm/yyyy hh:ii:ss',
                                'timezone' => '+0000',
                            ],
                            'options' => ['class' => 'mask_recall_date', 'name' => 'OrderTask[delivery_to]', 'id' => 'orderTask-delivery_to']
                        ]); ?>
                    </div>
                </div>
            </fieldset>
            <br/>
        </div>
    </div>


    <br/>
    <div class="row form-group">
        <div class="col-md-12">
            <b><?= Yii::t('common', 'Итоговая цена'); ?> <span class="total-final_task"></span> (<span
                        class="total-final-no-vat_task"></span>)</b>
        </div>
        <br/>
        <br/>
        <div class="col-md-12">
            <?= Html::button(Yii::t('common', 'Сохранить'), ['class' => 'btn btn-primary btn-status-save_task']); ?>

        </div>
    </div>

<?php endif; ?>

<?php ActiveForm::end(); ?>
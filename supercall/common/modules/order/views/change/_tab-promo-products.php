<?php
/** @var Order $order */

/** @var array $promo_products */

/** @var \common\widgets\ActiveForm $form */

use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;

?>

<div class="row">
    <div class="col-lg-3"><?= yii::t('common', 'Наименование') ?></div>
    <div class="col-lg-2"><?= yii::t('common', 'Количество') ?></div>
    <div class="col-lg-2"><?= yii::t('common', 'Цена') ?></div>
</div>

<?php if (!empty($order->orderPromoProducts)): ?>

    <?php foreach ($order->orderPromoProducts as $product): ?>

        <?= $this->render('_row_promo_product', [
            'order' => $order,
            'form' => $form,
            'promo_products' => $promo_products,
            'product' => $product
        ]) ?>

    <?php endforeach; ?>

<?php else: ?>

    <?= $this->render('_row_promo_product', [
        'order' => $order,
        'form' => $form,
        'promo_products' => $promo_products,
        'product' => new OrderProduct()
    ]) ?>

<?php endif; ?>

<?= $form->field(new OrderProduct(), 'promoProductSum')->hiddenInput(['value' => 0])->label(false); ?>

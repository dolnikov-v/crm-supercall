<?php

use common\modules\order\assets\ProductEditPriceAsset;
use common\modules\order\models\OrderProduct;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/** @var bool $is_gift */
/** @var bool $empty_field */
/** @var [] $packageList */
/** @var array $products */
/** @var OrderProduct $product */
/** @var integer $i */
/** @var array $packageList */

ProductEditPriceAsset::register($this);

$identity = is_null($product->package_id) ? null : ($is_gift ? 'gift_' : 'product_') . $product->product->id . '_' . $product->package_id;
?>


<?php if (!$is_gift): ?>

    <div class="row">
        <div class="col-xs-3 col-sm-3 col-lg-3 pr__3"><?= yii::t('common', 'Наименование'); ?></div>
        <div class="col-xs-3 col-sm-3 col-lg-3 pr__3"><?= yii::t('common', 'Акция'); ?></div>
        <div class="col-xs-2 col-sm-2 col-lg-2 pr__2"><?= yii::t('common', 'Количество'); ?></div>
    </div>

<?php endif; ?>

    <div class="<?= (!$is_gift) ? 'product' : 'product-gift'; ?> row form-group" id="product-<?= $i ?>"
         identity="<?= $identity; ?>">
        <?= $form->field($product, '[' . $i . ']id', ['template' => '{input}'])->hiddenInput(); ?>
        <div class="col-xs-3 col-sm-3 col-lg-3 pr__3">

            <?php if (!$is_gift): ?>
                <?= $form->field($product, '[' . $i . ']product_id', ['template' => '{input}'
                ])->dropDownList($products, [
                    'package' => $product->package_id,
                    'class' => 'form-control',
                    'style' => !is_null($product->package_id) ? 'pointer-events:none; color: #99A5C2' : '',
                    'data-id' => $product->id
                ]); ?>

            <?php else: ?>
                <?= $form->field($product, '[' . $i . ']product_id', ['enableAjaxValidation' => false], ['template' => '{input}', 'options' => [
                    'class' => 'product-select '
                ]])
                    ->dropDownList($products, [
                        'value' => $empty_field ? '' : $product->product->id,
                        'class' => 'form-control ' . (is_null($product->package_id) ? '' : 'package_id-' . $product->package_id),
                        'style' => !is_null($product->package_id) ? 'pointer-events:none; color: #99A5C2' : ''
                    ])->label(false); ?>

            <?php endif; ?>
        </div>

        <?php if (!$is_gift): ?>
            <div class="col-xs-3 col-sm-3 col-lg-3 pr__3">
                <?= $form->field($product, '[' . $i . ']package_id', ['template' => '{input}', 'options' => [
                    'class' => 'package-select',
                    'value' => $product->package_id
                ]])
                    ->dropDownList($packageList); ?>
            </div>
        <?php endif; ?>

        <div class="col-xs-2 col-sm-2 col-lg-2 pl__5 pr__5 ">
            <?= $form->field($product, '[' . $i . ']quantity', ['template' => '{input}'])
                ->dropDownList(OrderProduct::countList(), [
                    'id' => is_null($product->product) ? time() . rand(2, 19) : 'select_' . $product->product->id,
                    'class' => 'form-control ' . (is_null($product->package_id) ? '' : 'package_id-' . $product->package_id),
                    'style' => !is_null($product->package_id) ? 'pointer-events:none; color: #99A5C2' : ''
                ]); ?>
        </div>


        <?php if ($is_gift): ?>
            <div class="col-lg-1 gift-icon">
                <?php if (!is_null($product->package_id)): ?>
                    <span style="font-size: 16px; padding-top:6px" title="<?= $packageList[$product->package_id] ?>"
                          class="glyphicon glyphicon-gift"></span>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <?php if (!$is_gift): ?>

            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product, '[' . $i . ']amount', ['template' => '{input}'])->textInput([
                    'readonly' => true,
                    'placeholder' => null
                ]); ?>
            </div>
            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product, '[' . $i . ']price', ['template' => '{input}'])->textInput([
                    'readonly' => true,
                    'placeholder' => null,
                    'attributes' => [
                        'data-edit' => yii::$app->user->can('order.edit.product.price'),
                        'data-type' => 'product-price'
                    ]
                ]); ?>
            </div>
            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product, '[' . $i . ']cost', ['template' => '{input}'])->textInput([
                    'value' => $product->price * $product->quantity,
                    'readonly' => true,
                    'placeholder' => null,
                ]); ?>
            </div>

        <?php else: ?>

            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product, '[' . $i . ']price', [
                    'template' => '{input}',
                    'options' => [
                        'style' => 'visibility:hidden'
                    ]
                ])->textInput([
                    'readonly' => true,
                    'placeholder' => null,
                    'value' => 0,
                ]); ?>
            </div>

        <?php endif; ?>

        <?php if (!$is_gift): ?>
            <?= $form->field($product, '[' . $i . ']gift', ['template' => '{input}'])->hiddenInput(['value' => 0, '']); ?>
        <?php else : ?>
            <?= $form->field($product, '[' . $i . ']gift', ['template' => '{input}'])->hiddenInput(['value' => 1]); ?>
        <?php endif; ?>

        <div class="col-xs-1 col-sm-1 col-lg-1">
            <button class="btn btn-link btn-xs <?= (!$is_gift) ? 'btn-remove-product' : 'btn-remove-product-gift'; ?> pull-right pt__0 mt__8 ml__5"
                    type="button">
                <i class="pe-7s-close-circle fs-icon-20 clr-red fs-icon-bold"
                   title="<?= Yii::t('common', 'Удалить'); ?>"></i>
            </button>
        </div>

        <?php if ($is_gift): ?>

            <div class="col-lg-2">
                <?= $form->field($product, '[' . $i . ']package_id', ['template' => '{input}'])->hiddenInput(['value' => $product->package_id]); ?>
            </div>

        <?php endif; ?>
    </div>

<?php Modal::begin([
    'id' => 'transfer_to_pay',
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4>' . yii::t('common', 'Сообщение') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Отправить'), ['class' => 'btn btn-sent-to-npay btn-success close_btn', 'data-dismiss' => 'modal'])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-danger close_btn', 'data-dismiss' => 'modal'])
]); ?>

    <div class="row">
        <div class="col-lg-12">
            <?= yii::t('common', 'Отправить изменения на NPAY?'); ?>
        </div>
    </div>

<?php Modal::end(); ?>
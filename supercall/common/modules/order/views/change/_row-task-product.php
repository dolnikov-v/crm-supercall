<?php
use common\modules\order\models\OrderProduct;

$product_clone = new OrderProduct();

/** @var [] $products */
/** @var boolean $is_gift */
/** @var boolean $emptyField */
/** @var  stsObject|OrderProduct $product */
/** @var bool $empty_field */
/** @var [] $packageList */

$identity = !isset($product->package_id) || is_null($product->package_id) ? null : ($is_gift ? 'gift_' : 'product_') . ($product instanceof OrderProduct ? $product->product->id : $product->product_id) . '_' . $product->package_id;
?>
<!--  if is order data - not task -->
<?php if ($product instanceof OrderProduct) : ?>

    <div class="<?= (!$is_gift ? 'product_task' : 'product_task_gift') ?> row form-group" id="product-<?= $i ?>"
         identity="<?= $identity; ?>">

        <?= $form->field($product, '[' . $i . ']id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="col-xs-3 col-sm-3 col-lg-3 pr__5">
            <?= $form->field($product, '[' . $i . ']product_id', ['template' => '{input}', 'options' => ['class' => 'product-select']])
                ->dropDownList($products, [
                    'package' => $product->package_id,
                    'value' => (!$is_gift || !$emptyField) ? $product->product->id : '',
                    'style' => !is_null($product->package_id) ? 'pointer-events:none; color: #99A5C2' : '',
                ]); ?>
        </div>

        <?php if (!$is_gift): ?>
            <div class="col-xs-3 col-sm-3 col-lg-3 pr__3">
                <?= $form->field($product, '[' . $i . ']package_id', ['template' => '{input}', 'options' => [
                    'class' => 'package-select',
                    'value' => $product->package_id
                ]])
                    ->dropDownList($packageList); ?>
            </div>
        <?php endif; ?>

        <div class="col-xs-2 col-sm-2 col-lg-2 pl__5 pr__5">
            <?= $form->field($product, '[' . $i . ']quantity', ['template' => '{input}'])
                ->dropDownList(OrderProduct::countList(), [
                    'id' => is_null($product->product) ? '' : 'select_' . $product->product->id,
                    'class' => 'form-control ' . (is_null($product->package_id) ? '' : 'package_id-' . $product->package_id),
                    'style' => !is_null($product->package_id) ? 'pointer-events:none; color: #99A5C2' : ''
                ]); ?>
        </div>

        <?php if ($is_gift): ?>
            <div class="col-lg-1 gift-icon">
                <?php if (!is_null($product->package_id)): ?>
                    <span style="font-size: 16px; padding-top:6px" title="<?= $packageList[$product->package_id]?>" class="glyphicon glyphicon-gift"></span>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <?php if (!$is_gift && !$emptyField): ?>

            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product, '[' . $i . ']amount', ['template' => '{input}'])->textInput([
                    'readonly' => true,
                    'placeholder' => null,
                    'id' => is_null($product->product) ? '' : 'amount_' . $product->product->id
                ]); ?>
            </div>
            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product, '[' . $i . ']price', ['template' => '{input}'])->textInput([
                    'readonly' => true,
                    'placeholder' => null,
                ]); ?>
            </div>
            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product, '[' . $i . ']cost', ['template' => '{input}'])->textInput([
                    'readonly' => true,
                    'placeholder' => null,
                ]); ?>
            </div>

            <?= $form->field($product, '[' . $i . ']gift', ['template' => '{input}'])->hiddenInput(['value' => 0]); ?>

        <?php else: ?>

            <?= $form->field($product, '[' . $i . ']gift', ['template' => '{input}'])->hiddenInput(['value' => 1]); ?>

            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product_clone, '[' . $i . ']price', [
                    'template' => '{input}',
                    'options' => [
                        'style' => 'visibility:hidden'
                    ]
                ])->textInput([
                    'readonly' => true,
                    'placeholder' => null,
                    'value' => 0,
                ]); ?>
            </div>


        <?php endif; ?>

        <div class="col-xs-1 col-sm-1 col-lg-1">
            <button class="btn btn-link btn-xs <?= (!$is_gift ? 'btn-remove-product_task' : 'btn-remove-product_task_gift') ?> pull-right pt__0 mt__8 ml__5"
                    type="button">
                <i class="pe-7s-close-circle fs-icon-20 clr-red fs-icon-bold"
                   title="<?= Yii::t('common', 'Удалить'); ?>"></i>
            </button>
        </div>

        <?php if($is_gift):?>

            <div class="col-lg-2">
                <?= $form->field($product, '[' . $i . ']package_id', ['template' => '{input}'])->textInput(['value' => $product->package_id]); ?>
            </div>

        <?php endif;?>
    </div>

<?php else : ?>
    <div class="<?= (!$product->gift ? 'product_task' : 'product_task_gift') ?> row form-group" id="product-<?= $i ?>" identity="<?= $identity; ?>">

        <?= $form->field($product_clone, '[' . $i . ']id', ['template' => '{input}'])->hiddenInput(); ?>

        <div class="col-xs-3 col-sm-3 col-lg-3 pr__5">

            <?= $form->field($product_clone, '[' . $i . ']product_id', ['template' => '{input}', 'options' => ['class' => 'product-select']])
                ->dropDownList($products, [
                    'value' => (!$emptyField) ? $product->product_id : '',
                    'style' => !empty($product->package_id) ? 'pointer-events:none; color: #99A5C2' : '',
                    'package' =>  isset($product->package_id) ? $product->package_id : ''
                ]); ?>
        </div>

        <?php if (!$is_gift): ?>
            <div class="col-xs-3 col-sm-3 col-lg-3 pr__3">
                <?= $form->field($product_clone, '[' . $i . ']package_id', ['template' => '{input}', 'options' => [
                    'class' => 'package-select',
                    'value' => $product->package_id
                ]])
                    ->dropDownList($packageList); ?>
            </div>
        <?php endif; ?>

        <div class="col-xs-2 col-sm-2 col-lg-2 pl__5 pr__5">
            <?= $form->field($product_clone, '[' . $i . ']quantity', ['template' => '{input}'])
                ->dropDownList(OrderProduct::countList(), [
                    'value' => isset($product->quantity) ? $product->quantity : '',
                    'style' => !empty($product->package_id) ? 'pointer-events:none; color: #99A5C2' : '',
                ]); ?>
        </div>

        <?php if (!$is_gift && !$emptyField): ?>

            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product_clone, '[' . $i . ']amount', ['template' => '{input}'])->textInput([
                    'readonly' => true,
                    'placeholder' => null,
                    'id' => is_null($product->product_id) ? '' : 'amount_' . $product->product_id
                ]); ?>
            </div>

            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product_clone, '[' . $i . ']price', ['template' => '{input}'])->textInput([
                    'readonly' => true,
                    'placeholder' => null,
                    'value' => $product->price
                ]); ?>
            </div>
            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product_clone, '[' . $i . ']cost', ['template' => '{input}'])->textInput([
                    'readonly' => true,
                    'placeholder' => null,
                    'value' => $product->cost
                ]); ?>
            </div>

            <?= $form->field($product_clone, '[' . $i . ']gift', ['template' => '{input}'])->hiddenInput(['value' => 0]); ?>

        <?php else: ?>

            <?= $form->field($product_clone, '[' . $i . ']gift', ['template' => '{input}'])->hiddenInput(['value' => 1]); ?>

            <div class="col-xs-1 col-sm-1 col-lg-1 pl__5 pr__5 input-bg-white">
                <?= $form->field($product_clone, '[' . $i . ']price', [
                    'template' => '{input}',
                    'options' => [
                        'style' => 'visibility:hidden'
                    ]
                ])->textInput([
                    'readonly' => true,
                    'placeholder' => null,
                    'value' => 0,
                ]); ?>
            </div>

        <?php endif; ?>

        <div class="col-xs-1 col-sm-1 col-lg-1">
            <button class="btn btn-link btn-xs <?= (!$product->gift ? 'btn-remove-product_task' : 'btn-remove-product_task_gift') ?> pull-right pt__0 mt__8 ml__5"
                    type="button">
                <i class="pe-7s-close-circle fs-icon-20 clr-red fs-icon-bold"
                   title="<?= Yii::t('common', 'Удалить'); ?>"></i>
            </button>
        </div>

        <?php if($is_gift):?>

            <div class="col-lg-2">
                <?= $form->field($product_clone, '[' . $i . ']package_id', ['template' => '{input}'])->hiddenInput(['value' => $product->package_id]); ?>
            </div>

        <?php endif;?>


    </div>

<?php endif; ?>

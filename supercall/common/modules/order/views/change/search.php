<?php
/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var \common\modules\order\models\search\SearchOrder $searchModel
 * @var \yii\web\View $this
 */
use common\widgets\base\Panel;

?>

<div class="row" style="margin-bottom: 10px;">
    <div class="col-lg-12 text-right">
        <button class="btn btn-success" id="button_end_work_with_search_form"><?= Yii::t('common', 'Завершить работу с заказом') ?></button>
    </div>
</div>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('search/_search-form', [
        'searchModel' => $searchModel
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Результаты поиска'),
    'content' => $this->render('search/_search-content', [
        'dataProvider' => $dataProvider,
    ])
]) ?>


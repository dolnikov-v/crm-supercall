<?php
use common\modules\order\assets\ChangeAsset;
use common\modules\order\models\Order;

use common\modules\order\assets\PromoProductsOrderAsset;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use common\widgets\ActiveForm;
use common\modules\call\assets\CallHistoryAsset;
use common\modules\order\assets\ScriptsOperatorsAsset;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var bool $editable */
/** @var string $id_param */
/** @var array $types */
/** @var array $products */
/** @var array $shippings */
/** @var array $vat_list */
/** @var array $shipping_options */
/** @var array $countries */
/** @var string $operator */
/** @var array $orderTasks */
/** @var array $lastOrderTask */
/** @var integer $countOrdersOnPhone */
/** @var array $formAttributes */
/** @var array $packageList */
/** @var string $packageData */
/** @var string $partner_created_at */
/** @var string $time_now_with_timezone */
/** @var array $operators */
/** @var bool $hide_status_buttons */
/** @var bool $disabled_status_buttons */
/** @var string $partnerFormAdditionalPrice */
/** @var string $productPrice */

$this->assetManager->bundles[ChangeAsset::className()] = [
    'order' => $order
];

$this->assetManager->bundles[PromoProductsOrderAsset::className()] = [
    'order' => $order
];

CallHistoryAsset::register($this);
PromoProductsOrderAsset::register($this);
ScriptsOperatorsAsset::register($this);
ChangeAsset::register($this);

//Берем первый товар
$productIdForScript = false;
foreach ($order->orderProducts as $i => $product){
    if(!$product->gift){
        $productIdForScript = $product->product_id;
        break;
    }
}

?>

<?php $form = ActiveForm::begin([
    'id' => 'form-order',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validateOnType' => false,
    'validateOnSubmit' => true,
    'method' => 'post',
    'action' => '',
    'options' => [
        'data' => [
            'editable' => $editable,
            'id_param' => $order->id,
            'id' => $order->id,
            'partner_id' => $order->partner_id,
            'country_id' => $order->country_id,
            'vat_list' => $vat_list,
            'country' => $order->country->char_code,
            'status' => $order->status,
            'extra-price' => ArrayHelper::getValue($order, 'form.extra_price', 0),
            'vat' => ArrayHelper::getValue($order, 'form.vat.value', 0.0),
            'product_id'  => $productIdForScript,
            'order_type_id' => $order->type_id
        ]
    ]
]); ?>

<div class="panel">
    <p>
        <?= Yii::t('common', 'Номер заказа') ?>: <strong><?= $order->id ?></strong>,
        <?= Yii::t('common', 'Дата создания') ?>:
        <strong><?= Yii::$app->formatter->asDate($order->ordered_at) ?></strong>
    </p>
    <?php
    $similar_count = $order->getSimilar()->count();
    echo Tabs::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => Yii::t('common', 'Форма заказа'),
                'content' => $this->render('_tab-order', [
                    'order' => $order,
                    'editable' => $editable,
                    'types' => $types,
                    'products' => $products,
                    'shippings' => $shippings,
                    'is_default_shipping' => $is_default_shipping,
                    'shipping_options' => $shipping_options,
                    'countries' => $countries,
                    'products_price_data' => $products_price_data,
                    'operator' => $operator,
                    'countOrdersOnPhone' => isset($countOrdersOnPhone) ? $countOrdersOnPhone : 0,
                    'formAttributes' => $formAttributes,
                    'packageList' => $packageList,
                    'packageData' => $packageData,
                    'priceShipping' => $priceShipping,
                    'disabled_status_buttons' => $disabled_status_buttons,
                    'masked_phones' => $masked_phones,
                    'partner_created_at' => $partner_created_at,
                    'time_now_with_timezone' => $time_now_with_timezone,
                    'operators' => $operators,
                    'hide_status_buttons' => $hide_status_buttons,
                    'partnerFormAdditionalPrice' => $partnerFormAdditionalPrice,
                    'productPrice' => $productPrice
                ]),
            ],
            [
                'label' => Yii::t('common', 'История заказа'),
                'content' => $this->render('_tab-history', [
                    'order' => $order,
                ])
            ],
            [
                'label' => Yii::t('common', 'Похожие заказы')
                    . ($similar_count > 0
                        ? ' ' . Html::tag('span', $similar_count, ['class' => 'label label-warning'])
                        : ''
                    ),
                'content' => $this->render('_tab-similar', [
                    'order' => $order,
                ])
            ],

            /*[
                'label' => Yii::t('common', 'Таски заказа'),
                'content' => $this->render('_tab-order-tasks', [
                    'editable' => $editable,
                    'order_tasks' => [time(), time(), time()],
                    'order' => $order,
                    'types' => $types,
                    'products' => $products,
                    'shippings' => $shippings,
                    'is_default_shipping' => $is_default_shipping,
                    'shipping_options' => $shipping_options,
                    'countries' => $countries,
                    'products_price_data' => $products_price_data,
                    'operator' => $operator,
                    'orderTasks' => $orderTasks,
                    'lastOrderTask' => $lastOrderTask,
                    'formAttributes' => $formAttributes,
                    'packageList' => $packageList,
                    'packageData' => $packageData
                ])
            ],*/
        ]
    ]);
    ?>
</div>


<?php
use common\modules\order\models\Order;
use common\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;

/** @var Order $order */
/** @var array $order_tasks */
/** @var array $types */
/** @var array $products */
/** @var array $shippings */
/** @var array $shipping_options */
/** @var array $countries */
/** @var string $operator */
/** @var array $editable */
/** @var array $orderTasks */
/** @var array $lastOrderTask */
/** @var \common\widgets\ActiveForm $form */
/** @var array $formAttributes */
/** @var bool $editable */
/** @var [] $packageList */
/** @var  string $packageData*/
?>
<br/>
<?php

echo Tabs::widget([
    'encodeLabels' => false,
    'items' => [
        [
            'label' => Yii::t('common', 'Таска на изменение заказа'),
            'visible' => !$editable,
            'content' => $this->render('_tab-create-task', [
                'order' => $order,
                'types' => $types,
                'products' => $products,
                'shippings' => $shippings,
                'is_default_shipping' => $is_default_shipping,
                'shipping_options' => $shipping_options,
                'countries' => $countries,
                'products_price_data' => $products_price_data,
                'operator' => $operator,
                'editable' => $editable,
                'orderTasks' => $orderTasks,
                'lastOrderTask' => $lastOrderTask,
                'formAttributes' => $formAttributes,
                'editable' => $editable,
                'packageList' => $packageList,
                'packageData' => $packageData
            ])
        ],
        [
            'label' => Yii::t('common', 'История изменений'),
            'options' => ['id' => 'orderTaskHistoryTab'],
            'content' => $this->render('_tab-tasks-history', [
                'order' => $order,
                'orderTasks' => $orderTasks
            ])
        ],
    ]
]);
?>


<?php

use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\modules\order\models\Order;
use common\widgets\base\Panel;
use yii\data\ActiveDataProvider;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var Order $order */
$dataProvider = new ActiveDataProvider(['query' => $order->getSimilar(), 'pagination' => ['pageSize' => 20]]);
?>

<?php Pjax::begin([
    'id' => 'w0',
    'enablePushState' => true,
    'timeout' => 10000
]); ?>

<?=Panel::widget([
    'title' => Yii::t('common', 'Таблица с курьерскими службами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped table-hover table-sortable table-condensed'
        ],
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            'partner.name',
            [
                'attribute' => 'foreign_id',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'enableSorting' => false,
                'content' => function ($model) {
                    return Order::getStatusesCollection()[$model->status];
                }
            ],
            [
                'attribute' => 'customer_full_name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'productString',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'customer_phone',
                'value' => function ($model) {
                    if (!Yii::$app->user->can('order.index.viewcontacts')) {
                        return empty($model->customer_phone) ? $model->customer_phone : substr_replace($model->customer_phone, 'XXXX', -4);
                    } else {
                        return $model->customer_phone;
                    }
                }
            ],


            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]);?>

<?php Pjax::end(); ?>
<?php
use backend\modules\order\models\AddressVerification;
use common\models\GmapKey;
use common\modules\order\models\Order;
use common\modules\partner\models\PartnerFormAttribute;
use common\widgets\ActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\modules\order\widgets\Comments;
use \kartik\select2\Select2;
use yii\web\JsExpression;

use yii\bootstrap\Modal;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var array $shippings */
/** @var array $shipping_options */
/** @var array $formAttributes */
/** @var bool $editable */
/** @var \common\widgets\ActiveForm $form */


?>

<?php
$address_model = $order->getAddressModel();
if ($address_model) {
    if ($address_model->gmapMapping) {
        GmapKey::register($this, $order);
        ?>
        <div class="form-group">
            <div class="col-xs-8 col-sm-8 col-lg-9 pr__5 pl__5">
                <?= Html::textInput('gmap-autocomplete', '', ['class' => 'form-control gmap-autocomplete', 'data-gmap-autocomplete' => true]); ?>
            </div>
            <div class="col-xs-4 col-sm-4 col-lg-3 pr__5 pl__5">
                <?= Html::label('Google'); ?>
            </div>
        </div>
        <?php
    }

    $autoCompleteFields = ArrayHelper::getColumn($formAttributes['attributes'], 'name');
    $coordinatesFields = ArrayHelper::getColumn($formAttributes['coordinates'], 'name');
    $reverseData = array_flip($autoCompleteFields);

    $types = $address_model->attributesTypes();
    $labels = $address_model->attributeLabels();

    foreach ($address_model->attributes() as $k => $attr) {
        $type = $types[$attr];

        //поля для google map api - кроме select2 - они по дефолту используются в google map
        if (in_array($attr, $coordinatesFields) && !in_array($attr, $autoCompleteFields)) {
            $scriptCoordinates = '
                if(typeof (window.form.coordinatesFields) == "undefined"){
                    window.form.coordinatesFields = [];
                }
                if(typeof window.form.coordinatesFields["' . $attr . '"] == "undefined"){
                    window.form.coordinatesFields.push("' . $attr . '");
                }
            ';
            $this->registerJs($scriptCoordinates, $this::POS_READY);
        }

        //поля select2 autocomplete
        if (in_array($attr, $autoCompleteFields)) {
            $key = $reverseData[$attr];
            $label = $formAttributes['attributes'][$key]['label'];
            $idSelect2 = 'select2order_' . $key . '_' . $formAttributes['attributes'][$key]['id'];

            $script = '
                    if(typeof(window.form.select2autocomplete) == "undefined"){
                        window.form.select2autocomplete = [];
                    }
                    window.form.select2autocomplete.push("' . $idSelect2 . '");';

            $this->registerJs($script, $this::POS_READY);

            $field = '<div class="col-xs-8 col-sm-8 col-lg-9 pr__25 pl__5">';
            $field .= $form->field($address_model, $attr)->widget(Select2::classname(), [
                'options' => [
                    'placeholder' => 'Select or input a ' . $label . ' ...',
                    'class' => 'form-control',
                    'id' => $idSelect2,
                    'size' => Select2::SMALL,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'multiple' => false,
                    'disabled' => !$editable,
                ],
                'pluginOptions' => [
                    'minimumInputLength' => 1,
                    'ajax' => [
                        'url' => Yii::$app->params['urlBackend'] . '/catalog/klader-countries/get-data',
                        'dataType' => 'jsonp',
                        'delay' => 500,
                        //'jsonp' => 'callback_kladercountries',
                        'jsonpCallback' => 'kladercountries',
                        'data' => new JsExpression('function(params) { return {search:params.term, parent: getParent(' . $key . '), partner_form_attribute_id:' . $formAttributes['attributes'][$key]['id'] . ', klader_in_all_string:'.(int)$order->country->klader_in_all_string.' , is_disabled_user_data:"' . $formAttributes['attributes'][$key]['is_disabled_user_data'] . '"}; }'),
                        'processResults' => new JsExpression('function (data, params) { return {results: data.results}}'),
                    ],
                    'allowClear' => true,
                ],
            ])->label(false);

            $field .= '</div>';
            $field .= '<div class="col-xs-4 col-sm-4 col-lg-3 pr__5 pl__20"><label class="control-label">' . yii::t('common', $label);
            $field .= '</label></div>';


        } else {
            $field = $form->field($address_model, $attr, [
                'template' => '<div class="form-group">
                            <div class="col-xs-8 col-sm-8 col-lg-9 pr__5 pl__5">{input}</div>
                            <div class="col-xs-4 col-sm-4 col-lg-3 pr__5 pl__5">{label}</div>
                            </div>'
            ]);
        }

        //Новый тип - Список
        if ($type == ActiveField::TYPE_LIST) {
            $attribute = PartnerFormAttribute::getValues($attr, $order->form_id);
            $values = json_decode($attribute->values, 1);
            $items = array_combine(array_values($values), array_values($values));

            $field = '<div class="col-xs-8 col-sm-8 col-lg-9 pr__25 pl__5">' . $form->field($address_model, $attr)->select2List($items)->label(false) . '</div>';
            $field .= '<div class="col-xs-4 col-sm-4 col-lg-3 pr__5 pl__20"><label class="control-label">' . yii::t('common', $labels[$attr]) . '</label></div>';
            echo $field;
        } else {
            //обычные поля
            echo $field;
        }


    }
}

echo $form->field($order, 'customer_address', [
    'template' => '<div class="form-group">
                            <div class="col-xs-8 col-md-8 col-lg-9 pr__5 pl__5">{input}</div>
                            <div class="col-xs-4 col-md-4 col-lg-3 pr__5 pl__5">{label}</div>
                            </div>'
]);

?>
<?= $form->field($order, 'country_name', ['template' => '{input}'])->hiddenInput(['id' => 'order_country', 'value' => $order->country->name]); ?>
<?= $form->field($order, 'latitude', ['template' => '{input}'])->hiddenInput(['id' => 'order_latitude', 'value' => $order->latitude]); ?>
<?= $form->field($order, 'longitude', ['template' => '{input}'])->hiddenInput(['id' => 'order_longitude']); ?>
<?= $form->field($order, 'google_zip', ['template' => '{input}'])->hiddenInput(['id' => 'google_zip']); ?>
<?= $form->field($order, 'confirm_address', ['template' => '{input}'])->hiddenInput(['value' => AddressVerification::ADDRESS_CONFIRM_DEFAULT]); ?>

<?php
// Выведем комментарии
echo Comments::widget(['orderId' => $order->id]); ?>

<?= $form->field($order, 'comment_author', ['template' => '{input}'])->hiddenInput(['value' => $operator]); ?>
<?= $form->field($order, 'comment', [
    'template' => '<div class="form-group">
                            <div class="col-xs-8 col-md-8 col-lg-9 pr__5 pl__5">{input}</div>
                            <div class="col-xs-4 col-md-4 col-lg-3 pr__5 pl__5">{label}</div>
                            </div>'
])->textarea(['rows' => 2, 'data-author' => $operator]); ?>

<?php Modal::begin([
    'id' => 'confirm_google_zip',
    'header' => yii::t('common', 'Подтверждение'),
    'options' => ['tabindex' => 2],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-default btn-success', 'id' => 'google_confirm_success']) .
        Html::button(yii::t('common', 'Нет'), ['class' => 'btn btn-default btn-cancel', 'id' => 'google_confirm_cancel'])
]); ?>
<div class="content px-2">
    <?= yii::t('common', "Google's service at this address defined the index as <b> {google_zip} </b>, but you used the <b> {cusomer_zip} </b> index, replace the index you specified?", [
        'google_zip' => '<span id="field_google_zip"></span>',
        'cusomer_zip' => '<span id="field_customer_zip"></span>'
    ]); ?>
</div>

<?php Modal::end(); ?>

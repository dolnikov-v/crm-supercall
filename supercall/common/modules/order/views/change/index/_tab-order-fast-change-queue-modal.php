<?php

use common\components\ModalCustom;
use common\modules\order\models\Order;
use common\modules\partner\assets\form\FastChangeQueueAsset;
use common\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var \common\widgets\ActiveForm $form */
/** @var bool $editable */

FastChangeQueueAsset::register($this);
ModalCustom::begin([
    'id' => 'modal-fast-change-status',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
    'options' => ['class' => 'modal-status'],
    'header' => Yii::t('common', 'Сменить очередь'),
    'footer' => Html::button(Yii::t('common', 'Сменить'), ['class' => 'btn btn-primary btn-fast-change-queue-save'])
        . ' '
        . Html::button(Yii::t('common', 'Отменить'), ['class' => 'btn btn-default close-modal-status', 'data-dismiss' => 'modal'])
]);

$form = ActiveForm::begin([
    'action' => false,
    'method' => 'post',
    'options' => [
        'id' => 'partner-form-fast-change-queue'
    ],
]);


echo $form->field($order, 'last_queue_id')->dropDownList(ArrayHelper::map($order->form->fastChangeQueue->getSelectedQueues(true), 'id', 'name'));
echo HTML::hiddenInput('order_id', $order->id);
ActiveForm::end();
ModalCustom::end();
?>


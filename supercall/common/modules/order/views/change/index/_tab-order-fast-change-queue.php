<?php

use common\components\ModalCustom;
use common\modules\order\models\Order;
use common\modules\partner\assets\form\FastChangeQueueAsset;
use common\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var \common\widgets\ActiveForm $form */
/** @var bool $editable */

FastChangeQueueAsset::register($this);
?>


<div class="row form-group buttons">
    <div class="col-md-12">
        <?php
        echo Html::button(Yii::t('common', 'Сменить очередь'), [
            'name' => 'status',
            'class' => 'btn btn-info btn-sm btn-fast-change-queue mt__5 mb__5 after_call',
        ]);
        ?>
    </div>
</div>


<?php
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;

/** @var Order $order */
/** @var array $products */
/** @var array $taskProducts */
/** @var \common\widgets\ActiveForm $form */
/** @var [] $packageList */
/** @var  string $packageData */

$orderProducts = !isset($taskProducts) || $taskProducts === false ? $order->orderProducts : $taskProducts;

?>
<div class="products_task">
    <?php
    foreach ($orderProducts as $i => $product): ?>

        <?php if ((isset($product->gift) && !$product->gift) || isset($product->cost)): ?>
            <?= $this->render('../_row-task-product', [
                'i' => $i,
                'product' => $product,
                'products' => $products,
                'order' => $order,
                'form' => $form,
                'is_gift' => false,
                'emptyField' => false,
                'packageList' => $packageList,
                'packageData' => $packageData
            ]); ?>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
<hr/>
<div class="row form-group">
    <div class="col-md-12">
        <button type="button"
                class="btn btn-default btn-sm btn-add-product_task"><?= Yii::t('common', 'Добавить товары'); ?></button>
    </div>
</div>

<div class="row form-group">

    <div class="col-lg-2"><?= Yii::t('common', 'Итого') . ': ' . $order->init_price; ?></div>
    <div class="col-lg-1 total-quantity_task"></div>
    <div class="col-lg-1 total-price_task pl__5"></div>

</div>
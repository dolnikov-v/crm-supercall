<?php
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;

/** @var Order $order */
/** @var array $products */
/** @var array $taskProducts */
/** @var boolean $is_gift */
/** @var \common\widgets\ActiveForm $form */
/** @var  [] $packageList */
/** @var  string $packageData */

$orderProducts = !isset($taskProducts) || $taskProducts === false ? $order->orderProducts : $taskProducts;

?>
<div class="products_task_gift">
    <?php $count = 0;?>
    <?php
    foreach ($orderProducts as $i => $product): ?>

        <?php if ((isset($product->gift) && $product->gift) || !isset($product->cost)): ?>
            <?php ++$count;?>
            <?= $this->render('../_row-task-product', [
                'i' => $count*100,
                'product' => isset($product) ? $product : new OrderProduct(),
                'products' => $products,
                'order' => $order,
                'form' => $form,
                'is_gift' => true,
                'emptyField' => false,
                'packageList' => $packageList,
                'packageData' => $packageData
            ]); ?>
        <?php endif; ?>
    <?php endforeach; ?>

    <?php if($count == 0):?>

        <?= $this->render('../_row-task-product', [
            'i' => $count*100,
            'product' => isset($product) ? $product : new OrderProduct(),
            'products' => $products,
            'order' => $order,
            'form' => $form,
            'is_gift' => true,
            'emptyField' => true,
            'packageList' => $packageList,
            'packageData' => $packageData
        ]); ?>
    <?php endif;?>

</div>



<hr/>
<div class="row form-group">
    <div class="col-md-12">
        <button type="button"
                class="btn btn-default btn-sm btn-add-product_task_gift"><?= Yii::t('common', 'Добавить подарки'); ?></button>
    </div>
</div>
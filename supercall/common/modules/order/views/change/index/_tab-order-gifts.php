<?php
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
?>

<div class="products-gift">
    <?php $count = 0;?>
    <?php foreach ($order->orderProducts as $i => $product): ?>
        <?php if($product->gift):?>
            <?php ++$count;?>
            <?= $this->render('../_row_product', [
                'i' => (int)microtime()*rand(10,19),
                'product' => isset($product) ? $product : new OrderProduct(),
                'empty_field' => false,
                'products' => $products,
                'order' => $order,
                'form' => $form,
                'is_gift' => true,
                'packageList' => $packageList
            ]); ?>

        <?php endif;?>

    <?php endforeach; ?>
    <?php if($count == 0) :?>
        <?= $this->render('../_row_product', [
            'i' => (int)microtime()*rand(10,19),
            'product' => isset($product) ? $product : new OrderProduct(),
            'empty_field' => true,
            'products' => $products,
            'order' => $order,
            'form' => $form,
            'is_gift' => true,
            'packageList' => $packageList
        ]); ?>
    <?php endif;?>

</div>
<hr />
<div class="row form-group">
    <div class="col-xs-3 col-sm-3 col-lg-5">

    </div>
</div>
<div class="row form-group">
    <div class="col-md-12">
        <button type="button" class="btn btn-default btn-sm btn-add-product-gift"><?= Yii::t('common', 'Добавить подарки'); ?></button>
    </div>
</div>

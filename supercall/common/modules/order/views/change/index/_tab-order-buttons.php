<?php

use backend\assets\widgets\navbar\MaskAsset;
use common\components\ModalCustom;
use common\modules\order\assets\ScrollParentWindowAsset;
use common\modules\order\models\Order;
use kartik\datetime\DateTimePicker;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use common\modules\order\assets\QualityControlEditAsset;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var \common\widgets\ActiveForm $form */
/** @var bool $editable */


MaskAsset::register($this);
ScrollParentWindowAsset::register($this);

$quality_control = yii::$app->request->get('quality-control') == 1;

?>

<?php if ($quality_control) : ?>
    <?php QualityControlEditAsset::register($this); ?>
    <br/>
    <br/>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($order, 'operator_status')->hiddenInput(['value' => $order->status])->label(false); ?>
            <?= $form->field($order, 'auditor_status')->select2List(Order::getStatusesCollection(), ['value' => $order->status])->label(yii::t('common', 'Статус')); ?>
        </div>
    </div>

<?php else: ?>

    <?php

    echo $form->field($order, 'status', ['template' => '{input}'])->hiddenInput();
    echo $form->field($order, 'init_price', ['template' => '{input}'])->hiddenInput();
    echo $form->field($order, 'final_price', ['template' => '{input}'])->hiddenInput();
    echo $form->field($order, 'shipping_price', [
        'template' => '{input}',
        'options' => [
            'class' => 'default-shipping-price',
        ]])->hiddenInput();
    ?>

    <div class="row form-group buttons">
        <div class="col-md-12">
            <?php
            // Определим порядок кнопок в форме
            $buttonsVirtual = Order::getVirtualButtonsStatus($order);
            $buttonsOrigin = Order::getButtonsStatus($order);
            $buttons = [];

            if (is_array($buttonsVirtual) && is_array($buttonsOrigin)) {
                $buttons = $buttonsOrigin + $buttonsVirtual;
            } elseif (!is_array($buttonsVirtual)) {
                $buttons = $buttonsOrigin;
            } elseif (!is_array($buttonsOrigin)) {
                $buttons = $buttonsVirtual;
            }

            foreach ($buttons as $status => $label) {
                echo Html::button($label, [
                    'name' => 'status',
                    'class' => 'btn btn-default btn-sm btn-status mt__5 mb__5 after_call',
                    'data-status' => $status,
                    'disabled' => !$editable
                ]);
                ModalCustom::begin([
                    'id' => 'modal-status-' . $status,
                    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
                    'options' => ['class' => 'modal-status'],
                    'header' => $label,
                    'footer' => Html::button(Yii::t('common', 'Сохранить'), ['class' => 'btn btn-primary btn-status-save', 'data-status' => $status])
                        . ' '
                        . Html::button(Yii::t('common', 'Отменить'), ['class' => 'btn btn-default close-modal-status', 'data-dismiss' => 'modal'])
                ]);

                if (in_array($status, [
                    Order::STATUS_APPROVED,
                    Order::STATUS_FAIL, Order::STATUS_DOUBLE,
                    Order::SUB_STATUS_REJECT_STILL_WAITING,
                    Order::SUB_STATUS_ALREADY_RECEIVED,
                    Order::SUB_STATUS_REJECT_NO_LONGER_NEEDED,
                    Order::SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER
                ])) {
                    echo yii::t('common', 'Вы уверены, что хотите изменить статус на {status} ?', ['status' => $label]);
                }

                if ($status == Order::STATUS_REJECTED) {
                    echo $form->field($order, 'sub_status')->dropDownList(Order::getRejectReasonCollection(), ['disabled' => 1]);
                    echo $form->field($order, 'comment')->textarea(['disabled' => 1]);
                }


                if ($status == Order::STATUS_TRASH) {
                    echo $form->field($order, 'sub_status')->dropDownList(Order::getTrashReasonCollection(), ['disabled' => 1]);
                    echo $form->field($order, 'comment')->textarea(['disabled' => 1]);
                }
                if ($status == Order::STATUS_RECALL) {
                    $order->recall_date = $order->recall_date ? date('d/m/Y H:i:s', strtotime($order->recall_date)) : null;

                    //остаток дня
                    //т.к. по задаче - необходимо дать только 3 дня - то есть вероятность, что время последнего дня будет ограничено
                    $restOfDay = (24-date('H'))*3600;

                    echo $form->field($order, 'recall_date')->widget(DateTimePicker::className(), [
                        'type' => DateTimePicker::TYPE_INPUT,
                        'pluginOptions' => [
                            'format' => 'dd/mm/yyyy hh:ii:ss',
                            'timezone' => '+0000',
                            'startDate' => date('d-m-Y H:i:s',time()),
                            'endDate' => date('d-m-Y H:i:s', (time() + 72*3600 + $restOfDay))
                        ],
                        'options' => ['disabled' => 1, 'class' => 'mask_recall_date', 'value' => date('d-m-Y H:i:s',time())]
                    ]);
                    echo $form->field($order, 'comment')->textarea(['disabled' => 1]);
                }
                ModalCustom::end();
            }
            ?>
        </div>
    </div>

    <script>
        function hideButtonAfterClick(obj) {
            $(obj).hide()
            $('<button type="button" class="loader"></button>').insertAfter(obj)
        }
    </script>

<?php endif; ?>

<?php
$this->registerJs("
        $(function () {
            $('.mask_recall_date').mask('00/00/0000 00:00:00');
        });
"); ?>
<?php

use common\models\Product;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;

/** @var Order $order */
/** @var array $products */
/** @var \common\widgets\ActiveForm $form */
/** @var [] $packageList */
/** @var array $promo_products */

?>
<div class="products">
    <?php if (!empty($order->orderProducts)) : ?>

        <?php
        foreach ($order->orderProducts as $i => $product): ?>
            <?php if (!$product->gift) : ?>
                <?= $this->render('../_row_product', [
                    'i' => $i,
                    'product' => $product,
                    'products' => $products,
                    'packageList' => $packageList,
                    'empty_field' => false,
                    'order' => $order,
                    'form' => $form,
                    'is_gift' => false
                ]); ?>
            <?php endif; ?>
        <?php endforeach; ?>

    <?php else: ?>

        <?= $this->render('../_row_product', [
            'i' => 0,
            'product' => new OrderProduct(),
            'products' => $products,
            'packageList' => $packageList,
            'empty_field' => false,
            'order' => $order,
            'form' => $form,
            'is_gift' => false
        ]); ?>

    <?php endif; ?>

</div>
<hr/>

<div class="row form-group">
    <div class="col-md-12">
        <button type="button"
                class="btn btn-default btn-sm btn-add-product"><?= Yii::t('common', 'Добавить товары'); ?></button>
    </div>
</div>
<br/>

<!-- PROMO PRODUCTS -->

<?php if ($order->form && !empty($order->form->promoProducts)) : ?>
    <fieldset id="promo-products">
        <legend><?= yii::t('common', 'Промо продукты') ?></legend>

        <?= $this->render('@common/modules/order/views/change/_tab-promo-products', [
            'order' => $order,
            'form' => $form,
            'promo_products' => $promo_products,
        ]); ?>
    </fieldset>
    <hr/>
    <br/>
<?php endif; ?>


<!-- END PROMO PRODUCTS -->


<fieldset>
    <legend><?= Yii::t('common', 'Итого'); ?></legend>
</fieldset>
<div class="row form-group">
    <div class="col-lg-2" title="Init price">
        <strong><?= Yii::t('common', 'Итого') . ':</strong> ' . $order->init_price; ?></div>
    <div class="col-lg-1 total-quantity" title="Quantity products"></div>
    <div class="col-lg-1 total-price pl__5" title="Final price"></div>

</div>
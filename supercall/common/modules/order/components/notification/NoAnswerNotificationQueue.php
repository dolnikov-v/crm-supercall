<?php

namespace common\modules\order\components\notification;


use common\components\notification\Queue;
use common\components\notification\transport\Message;
use common\components\notification\transport\SmsMessage;
use common\components\notification\transport\SmsTransport;
use common\components\notification\transport\Transport;
use common\modules\call\models\CallHistory;
use common\modules\order\models\NoAnswerNotificationRequest;
use common\modules\order\models\Order;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class NoAnswerNotificationQueue
 * @package common\modules\order\components\notification
 */
class NoAnswerNotificationQueue extends Queue
{
    /**
     * @param array $data
     * @return bool
     */
    public static function add($data)
    {
        $message = self::prepareMessageForQueue($data['order'], $data['groupId']);
        $queue = new NoAnswerNotificationSQS();

        $request = new NoAnswerNotificationRequest([
            'order_id' => $data['order']->id,
            'group_id' => $data['groupId'],
        ]);
        $response = $queue->send($message);
        if (!$response) {
            $request->status = NoAnswerNotificationRequest::STATUS_ERROR;
        }
        $request->save();
        return $response;
    }

    /**
     * @param Order $order
     * @param string $groupId
     * @return array
     */
    protected static function prepareMessageForQueue(Order $order, $groupId)
    {
        return [
            'order_id' => $order->id,
            'status' => $order->status,
            'group_id' => $groupId,
            'country_char_code' => $order->country->char_code,
            'created_at' => time(),
        ];
    }

    /**
     * Первичная проверка на необходимость отправки сообщения
     * До того как получили данные с астера
     * @param Order $order
     * @return bool
     */
    public static function shouldSendNotification($order)
    {
        if (in_array($order->status, [
                Order::STATUS_FAIL,
                Order::STATUS_RECALL
            ]) && !empty($order->form->no_answer_notification_text)
        ) {
            return true;
        }
        return false;
    }

    /**
     * @param null $limit
     * @return bool
     */
    public function sendNotifications($limit = null)
    {
        $queue = new NoAnswerNotificationSQS();
        $receivedMessageCount = 0;

        while ($limit ? $receivedMessageCount < $limit : true) {
            if ($messages = $queue->getMessagesFromQueue()) {
                foreach ($messages as $messageId => $messageData) {
                    $request = NoAnswerNotificationRequest::find()->where([
                        'order_id' => $messageData['order_id'],
                        'group_id' => $messageData['group_id']
                    ])->one();
                    if (!$request) {
                        $request = new NoAnswerNotificationRequest([
                            'order_id' => $messageData['order_id'],
                            'group_id' => $messageData['group_id']
                        ]);
                    }
                    if (!self::receivedAllCallHistoryFromAsterisk($messageData['order_id'], $messageData['group_id'])) {
                        unset($queue->handlers[$messageId]);
                        if ($request->status != NoAnswerNotificationRequest::STATUS_WAIT_INFORMATION_FROM_ASTERISK) {
                            $request->status = NoAnswerNotificationRequest::STATUS_WAIT_INFORMATION_FROM_ASTERISK;
                            $request->save();
                        }
                        continue;
                    }

                    $order = Order::find()->byId($messageData['order_id'])->one();
                    if (($phone = self::canSendNotification($messageData['order_id'], $messageData['status'], $messageData['group_id'])) && !empty($order->form->no_answer_notification_text)) {
                        $message = self::prepareNotification($order, $phone);
                        $result = $this->getTransport()->send($message);
                        $request->response = $result['response'];
                        $request->phone = (string)$phone;
                        $request->sent_at = time();
                        if ($result['status'] != Transport::STATUS_SUCCESS) {
                            unset($queue->handlers[$messageId]);
                            if ($request->status != NoAnswerNotificationRequest::STATUS_FAIL) {
                                $request->status = NoAnswerNotificationRequest::STATUS_FAIL;
                                $request->save();
                            }
                        } else {
                            $request->status = NoAnswerNotificationRequest::STATUS_SENT;
                            $request->save();
                        }
                    } else {
                        $request->status = NoAnswerNotificationRequest::STATUS_CANT_SENT;
                        $request->save();
                    }
                }
                $receivedMessageCount += count($messages);
                $queue->removeMessages();
            } else {
                return $receivedMessageCount > 0;
            }
        }

        return true;
    }

    /**
     * Проверка получения с астериска всех данных по звонкам
     * @param integer $orderId
     * @param string $groupId
     * @return bool
     */
    protected static function receivedAllCallHistoryFromAsterisk($orderId, $groupId)
    {
        return !CallHistory::find()
                ->where(['order_id' => $orderId, 'group_id' => $groupId])
                ->exists() || CallHistory::find()
                ->where(['order_id' => $orderId, 'group_id' => $groupId])
                ->andWhere(['is not', 'uniqueid', null])
                ->exists();
    }

    /**
     * Окончательная проверка необходимости отправки сообщения
     * @param integer $orderId
     * @param integer $status
     * @param string $groupId
     * @return bool
     */
    protected static function canSendNotification($orderId, $status, $groupId)
    {
        $callHistoryModel = CallHistory::find()->select(['disposition', 'phone'])->where([
            'order_id' => $orderId,
            'group_id' => $groupId,
            'disposition' => [
                CallHistory::DISPOSITION_BUSY,
                CallHistory::DISPOSITION_NO_ANSWER,
                CallHistory::DISPOSITION_ANSWERED,
            ]
        ])->orderBy(['id' => SORT_DESC])->one();

        if (!$callHistoryModel) {
            return false;
        }

        switch ($status) {
            case Order::STATUS_FAIL:
                return intval($callHistoryModel->phone);
            case Order::STATUS_RECALL:
                return in_array($callHistoryModel->disposition, [
                    CallHistory::DISPOSITION_BUSY,
                    CallHistory::DISPOSITION_NO_ANSWER
                ]) ? intval($callHistoryModel->phone) : false;
            default:
                return false;
        }
    }

    /**
     * @param Order $order
     * @param string $phone
     * @return Message
     */
    protected static function prepareNotification(Order $order, $phone)
    {
        return new SmsMessage([
            'text' => self::prepareTextForMessage($order),
            'to' => $phone,
            'country_char_code' => $order->country->char_code,
            'from' => !empty($order->form->no_answer_notification_sender) ? $order->form->no_answer_notification_sender : null,
        ]);
    }

    /**
     * @param Order $order
     * @return string
     */
    protected static function prepareTextForMessage(Order $order)
    {
        $text = $order->form->no_answer_notification_text;
        $tokens = array_fill_keys(array_keys(self::tokensForNotificationText()), '');
        foreach ($order->attributes as $name => $value) {
            $tokens['order.' . $name] = $value;
        }
        $products = [];
        if ($order->orderProducts) {
            foreach ($order->orderProducts as $orderProduct) {
                $productName = Yii::t('common', $orderProduct->product->name, [], $order->country->char_code);
                if (!isset($products[$productName])) {
                    $products[$productName] = 0;
                }
                $products[$productName] += $orderProduct->quantity;
            }
        }

        $tmp = [];

        foreach ($products as $name => $quantity) {
            $tmp[] = $name . ' - ' . $quantity;
        }
        $tokens['order.products'] = implode(' ,', $tmp);

        foreach ($tokens as $token => $value) {
            $text = str_replace("{{$token}}", $value, $text);
        }
        return $text;
    }

    /**
     * @return array
     */
    public static function tokensForNotificationText()
    {
        $tokens = [];
        $attributes = [
            'id',
            'foreign_id',
            'customer_first_name',
            'customer_middle_name',
            'customer_last_name',
            'customer_phone',
            'customer_mobile',
            'customer_address',
            'customer_ip',
            'customer_init_price',
            'customer_final_price',
            'customer_shipping_price',
            'customer_full_name',
            'customer_email',
        ];

        $labels = (new Order())->attributeLabels();

        foreach ($attributes as $attr) {
            $tokens["order.{$attr}"] = Yii::t('common', 'Заказ') . '.' . ArrayHelper::getValue($labels, $attr, $attr);
        }

        $tokens['order.products'] = Yii::t('common', 'Заказ') . '.' . Yii::t('common', 'Товары');

        return $tokens;
    }

    /**
     * @return SmsTransport|\common\components\notification\transport\Transport
     */
    public function getTransport()
    {
        if (is_null($this->transport)) {
            $this->transport = new SmsTransport([
                'serviceName' => 'NoAnswerSmsService'
            ]);
        }
        return $this->transport;
    }
}
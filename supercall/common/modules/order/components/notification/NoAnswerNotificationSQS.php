<?php

namespace common\modules\order\components\notification;


use common\models\AmazonSQS;

/**
 * Class NoAnswerNotificationSQS
 * @package common\components\sqs
 */
class NoAnswerNotificationSQS extends AmazonSQS
{
    const AMAZON_QUEUE_URL = 'https://sqs.eu-west-1.amazonaws.com/636470419474/CallCenterNoAnswerNotification.fifo';

    /**
     * @param array $message
     * @return bool
     */
    public function send($message)
    {
        $data = self::prepareMessage($message);
        return $this->sendMessage($data);
    }

    /**
     * @return array|bool
     */
    public function getMessagesFromQueue()
    {
        $messages = $this->getMessages(10, [], self::AMAZON_QUEUE_URL);
        if ($messages) {
            $messages = array_map(function ($item) {
                return json_decode($item, true);
            }, $messages);
        }

        return $messages;
    }

    /**
     * @param array $message
     * @return array
     */
    protected static function prepareMessage($message)
    {
        return [
            'QueueUrl' => self::AMAZON_QUEUE_URL,
            'MessageBody' => json_encode($message, JSON_UNESCAPED_UNICODE),
            'MessageGroupId' => $message['country_id'] ?? $message['country_char_code'] ?? 'common_group'
        ];
    }


    public function removeMessages()
    {
        $this->deleteMessages(self::AMAZON_QUEUE_URL, $this->handlers);
    }
}
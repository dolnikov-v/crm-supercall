<?php

use kartik\file\FileInput;
use yii\bootstrap\Modal;
use yii\helpers\Html;

?>


<?php Modal::begin([
    'id' => 'modal-common-profile',
    'size' => Modal::SIZE_DEFAULT,
    'header' => Yii::t('common', 'Сменить картинку профиля'),
    'footer' => Html::button('Применить', ['class' => 'btn btn-primary btn-change-profile-image'])
]); ?>

    <div class="form-group body">

        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">

                <?=FileInput::widget([
                    'model' => yii::$app->user->identity,
                    'attribute' => 'profile_image',
                    'options' => [
                        'accept' => 'image/*',
                    ],
                    'pluginOptions' => [
                        'showUpload' => false,
                        'browseLabel' => '',
                        'removeLabel' => '',
                        'allowedFileExtensions' => ['jpg', 'png', 'jpeg'],
                    ]
                ]); ?>
            <br/>
                <span class="glyphicon glyphicon-remove-sign text-danger"></span> <a class="set-default" href="#">удалить картинку профиля</a>
            </div>
            <div class="col-lg-3"></div>
        </div>
    </div>

    <div class="default-avatar" data-img="/resources/images/common/avatar-profile.png"></div>

<?php Modal::end(); ?>
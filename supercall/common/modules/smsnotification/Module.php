<?php
namespace common\modules\smsnotification;

use backend\components\base\Module as CommonModule;

/**
 * Class Module
 * @package common\modules\smsnotification
 */
class Module extends CommonModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\smsnotification\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

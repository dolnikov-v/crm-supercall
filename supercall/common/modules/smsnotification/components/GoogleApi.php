<?php
namespace common\modules\smsnotification\components;
/**
 * Класс сокращения URL с использованием Google Short Url API
 * https://developers.google.com/url-shortener/v1/getting_started
 * ограничение на токен 1 000 000 запросов в день
 * Class GoogleApi
 *
 */
class GoogleApi
{
    const API_SHORT_URL = 'https://www.googleapis.com/urlshortener/v1/url';
    const TOKEN = 'AIzaSyC9f4OsO56tNFkoBE6xbyfWqcA1EKchInk';
    const SUCCESS = true;
    const FAIL = false;

    /**
     * Сокращение урлов
     *
     * Пример запроса короткого урла
     * GoogleApi::getShortUrl(['long_url' => 'http://2wtrade-pay.com/']
     *
     * Ожидаемый успешного формат ответа API
     * Array
     * (
     *      [error] => 0
     *      [answer] => stdClass Object
     *      (
     *          [kind] => urlshortener#url
     *          [id] => https://goo.gl/Wv10hb
     *          [longUrl] => https://geektimes.ru/post/105546/
     *      )
     * )
     *
     * формат ошибки ответа API
     * Array
     * (
     *      [error] => 0
     *      [answer] => stdClass Object
     *      (
     *          [error] => stdClass Object
     *          (
     *              [errors] => Array
     *              (
     *                  [0] => stdClass Object
     *                  (
     *                      [domain] => usageLimits
     *                      [reason] => dailyLimitExceededUnreg
     *                      [message] => Daily Limit for Unauthenticated Use Exceeded. Continued use requires signup.
     *                      [extendedHelp] => https://code.google.com/apis/console
     *                   )
     *
     *               )
     *              [code] => 403
     *              [message] => Daily Limit for Unauthenticated Use Exceeded. Continued use requires signup.
     *          )
     *      )
     * )
     *
     * Формат ответа при ошибки работы Curl
     * Array
     * (
     *      [error] => 6 Could not resolve host: Non-existent URL
     *      [answer] =>
     * )
     *
     * @param $params []
     * @return []
     */
    public static function getShortUrl($params)
    {
        $post_fields = [
            'longUrl' => $params['longUrl']
        ];

        $options = [
            CURLOPT_URL => self::API_SHORT_URL . '?key=' . self::TOKEN,
            CURLOPT_HEADER => false,
            CURLOPT_HTTPHEADER => [
                'Content-Type:application/json'
            ],
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($post_fields),
            CURLOPT_RETURNTRANSFER => true
        ];

        return self::send($options, true);
    }

    public static function send($options, $json = false)
    {
        $c = curl_init();
        curl_setopt_array($c, $options);
        $result = curl_exec($c);
        $error = curl_errno($c) . ' ' . curl_error($c);
        echo $error;
        curl_close($c);

        return [
            'error' => $error,
            'answer' => ($json) ? json_decode($result) : $result
        ];
    }

}

?>
<?php
namespace common\modules\call;

use common\components\base\Module as CommonModule;

/**
 * Class Module
 * @package common\modules\call
 */
class Module extends CommonModule
{
    public $controllerNamespace = 'common\modules\call\controllers';
}

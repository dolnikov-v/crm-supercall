<?php
namespace common\modules\call\controllers;

use common\components\web\Controller;
use common\models\AsteriskRecords;
use common\models\UserReady;
use common\modules\call\models\CallHistory;
use yii;

/**
 * Class CallHistoryController
 * @package common\modules\call\controllers
 */
class CallHistoryController extends Controller
{
    const LOGGER_TYPE = 'call_history';
    const ACTION_ADD_ACTION_ADDRESS_TIME = 'add action address time';

    public function beforeAction($action)
    {
        if (in_array($action->id, ['user-ready', 'insert-data-on-start-call', 'update-data-on-call', 'add-action-address-time'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600,
                ],
            ],

        ]);
    }

    /**
     * List of allowed domains.
     * Note: Restriction works only for AJAX (using CORS, is not secure).
     *
     * @return array List of domains, that can access to this API
     */
    public static function allowedDomains()
    {
        return [
            yii::$app->params['urlApi'],
            yii::$app->params['urlBackend'],
            yii::$app->params['urlFrontend'],
        ];
    }

    /**
     * Метод создание записи в call_history
     * на данном этапе есть: user_id, order_id, group_id, start_time, start_status
     *
     * @return  integer id записиk
     */
    public function actionInsertDataOnStartCall()
    {
        $data = yii::$app->request->post();
        $result = CallHistory::insertData($data);

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function actionUpdateDataOnCall()
    {
        $post = yii::$app->request->post();

        $asteriskRecord = new AsteriskRecords();
        $data = $asteriskRecord->getRecords($post['user_sip'], $post['phone'], 'array');

        if (!empty($data) && is_array($data)) {
            $post = array_merge($post, $data[0]);
        }

        $result = CallHistory::updateData($post);

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function actionAddActionAddressTime()
    {
        $action_adress_time = yii::$app->request->post('action_adress_time');
        $call_history_id = yii::$app->request->post('call_history_id');

        $result = CallHistory::addActionAddressTime($call_history_id, $action_adress_time);

        if (!$result['result']) {
            $logger = yii::$app->get('logger');
            $logger->action = self::ACTION_ADD_ACTION_ADDRESS_TIME;
            $logger->route = '/call/acl-history-add-action-address-time';
            $logger->type = self::LOGGER_TYPE;
            $logger->tags = ['error' => json_encode($result['error'], JSON_UNESCAPED_UNICODE)];
            $logger->error = json_encode($result['error'], JSON_UNESCAPED_UNICODE);
            $logger->save();
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function actionUserReady()
    {
        $user_id = yii::$app->request->post('user_id');
        //закрыть старый
        UserReady::changeIsReady($user_id, false);
        //открыть новый
        UserReady::changeIsReady($user_id, true);
    }
}
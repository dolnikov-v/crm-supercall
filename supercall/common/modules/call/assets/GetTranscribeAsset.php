<?php

namespace common\modules\call\assets;

use yii\web\AssetBundle;

/**
 * Class GetTranscribeAsset
 * @package common\modules\call\assets
 */
class GetTranscribeAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/call/get-transcribe';


    public $js = [
        'get-transcribe.js'
    ];

    public $css = [
        'get-transcribe.css'
    ];


    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
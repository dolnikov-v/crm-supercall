<?php
namespace common\modules\call\assets;

use yii\web\AssetBundle;

class CallHistoryAsset extends AssetBundle
{

    public $sourcePath = '@common/web/resources/modules/call/call-history';


    public $js = [
        'call-history.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
<?php

namespace common\modules\call\models\query;

use common\components\db\ActiveQuery;

/**
 * Class CallHistoryQuery
 * @package common\modules\call\models\query
 */
class CallHistoryQuery extends ActiveQuery
{
    /**
     * @param $uniqueid
     * @return $this
     */
    public function byUniqueid($uniqueid)
    {
        return $this->andWhere(['uniqueid' => $uniqueid]);
    }

    /**
     * @param $phone
     * @return $this
     */
    public function byPhone($phone)
    {
        return $this->andWhere(['phone' => $phone]);
    }

    /**
     * @param $sip
     * @return $this
     */
    public function bySip($sip)
    {
        return $this->andWhere(['user_sip' => $sip]);
    }

    /**
     * @param $disposition
     * @return $this
     */
    public function byDisposition($disposition)
    {
        return $this->andWhere(['disposition' => $disposition]);
    }

    /**
     * @return $this
     */
    public function notEmptyGroupId()
    {
        return $this->andWhere(['is not', 'group_id', null]);
    }

    public function EmptyRecord()
    {
        return $this->andWhere(['is null', 'recordingfile', null]);
    }
}
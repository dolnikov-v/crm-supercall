<?php

namespace common\modules\call\models;


use backend\modules\audit\models\AggregateWord;
use backend\modules\order\models\TranscribeQueue;
use common\components\db\ActiveRecord;
use common\components\db\ModelTrait;
use common\models\User;
use common\modules\call\models\query\CallHistoryQuery;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProductLog;
use Yii;
use yii\db\Expression;

/**
 * Class CallHistory
 * @package common\modules\call\models
 * @property integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property string $group_id
 * @property integer $start_time
 * @property integer $end_time
 * @property integer $start_status
 * @property integer $end_status
 * @property integer $duration
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $action_address_time
 * @property string $phone
 * @property float $uniqueid
 * @property string $recordingfile
 * @property string $disposition
 * @property integer $billsec
 * @property string $user_sip
 * @property TranscribeQueue $transcribeQueue
 * @property AggregateWord[] $aggregateWords
 * @property OrderLog[] $orderLogs
 * @property OrderProductLog[] $orderProductLogs
 * @property float $transcribe_price
 * @property string $additional_params
 *
 * @property Order $order
 * @property User $user
 */
class CallHistory extends ActiveRecord
{
    use ModelTrait;

    const TRUNSCRIBE_PRICE_ONE_SECOND = 0.0004;

    const DISPOSITION_WAITING = 'WAITING';
    const DISPOSITION_ANSWERED = 'ANSWERED';
    const DISPOSITION_NO_ANSWER = 'NO ANSWER';
    const DISPOSITION_BUSY = 'BUSY';
    const DISPOSITION_FAILED = 'FAILED';
    const DISPOSITION_FAIL = 'FAIL';
    const DISPOSITION_LOST = 'LOST';

    const FULL_PATH_PATTERN = '/var/recording/in/';
    const ASTERISK_SERVER_URL = 'http://136.243.130.196/acdr/get.php?audio=';

    /** @var  integer  e/g id */
    public $call_history_id;

    /** @var  integer for widget filter */
    public $queue_id;

    /** @var  integer for widget filter */
    public $product_id;

    /** @var string */
    public $record;

    /** @var string */
    public $username;

    /** @var integer */
    public $call_date;

    /** @var string (json) */
    public $address;

    /** @var integer */
    public $status;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%call_history}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id', 'start_time', 'start_status'], 'required'],
            [
                [
                    'user_id',
                    'order_id',
                    'start_time',
                    'end_time',
                    'duration',
                    'start_status',
                    'end_status',
                    'created_at',
                    'updated_at',
                    'action_address_time',
                    'billsec'
                ],
                'integer'
            ],
            [['phone', 'disposition', 'recordingfile', 'additional_params'], 'string'],
            [['transcribe'], 'string'],
            ['disposition', 'default', 'value' => self::DISPOSITION_WAITING],
            [['group_id', 'user_sip'], 'string', 'max' => 255],
            [['uniqueid'], 'string'],
            [['transcribe_price'], 'double'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'order_id' => Yii::t('common', 'Заказ'),
            'start_time' => Yii::t('common', 'Время начала'),
            'end_time' => Yii::t('common', 'Время окончания'),
            'duration' => Yii::t('common', 'Продолжительность'),
            'start_status' => Yii::t('common', 'Начальный статус'),
            'end_status' => Yii::t('common', 'Конечный статус'),
            'phone' => Yii::t('common', 'Телефон'),
            'action_address_time' => Yii::t('common', 'Заполнение адреса, сек.'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'transcribe' => Yii::t('common', 'Распознование речи'),
            'group_id' => Yii::t('common', 'Группа'),
            'user_sip' => Yii::t('common', 'SIP логин'),
            'transcribe_price' => Yii::t('common', 'Стоимость транскрибации, $'),
            'additional_params' => yii::t('common', 'Дополнительные параметры')
        ];
    }

    /**
     * @param $recordinfile
     * @return string
     */
    public static function checkRecordFilePath($recordinfile)
    {
        return self::ASTERISK_SERVER_URL . (preg_match("#" . self::FULL_PATH_PATTERN . "#", $recordinfile)
                ? $recordinfile
                : self::FULL_PATH_PATTERN . $recordinfile);
    }

    /**
     * @return CallHistoryQuery
     */
    public static function find()
    {
        return new CallHistoryQuery(get_called_class());
    }

    /**
     * @param $data
     * @return array
     */
    public static function insertData($data)
    {
        if (is_array($data)) {
            $model = new CallHistory();
            if ($model->load($data, '') && $model->save()) {

                $model->order->group_id = $data['group_id'];

                return [
                    'result' => true,
                    'id' => $model->id
                ];
            } else {
                return [
                    'result' => false,
                    'error' => $model->getFirstErrorAsString()
                ];
            }
        } else {
            return [
                'result' => false,
                'error' => yii::t('common', 'При вставки истории данные должны быть в виде массива')
            ];
        }
    }

    /**
     * @param $call_history_id
     * @param $duration
     * @return array
     */
    public static function addActionAddressTime($call_history_id, $duration)
    {
        if(!is_numeric($call_history_id)){
            return [
                'result' => false,
                'error' => 'call_history_id is not isset'
            ];
        }

        $model = CallHistory::findOne(['id' => $call_history_id]);

        if ($model) {
            $model->action_address_time = (int)$duration;

            if ($model->save(false)) {
                return [
                    'result' => true
                ];
            } else {
                return [
                    'result' => false,
                    'error' => $model->getErrors()
                ];
            }
        } else {
            return [
                'result' => false,
                'error' => 'call_history_id : ' . $call_history_id . ' not found'
            ];
        }
    }

    public static function updateEndStatus($order_id, $end_status, $groupId)
    {
        $modelQuery = CallHistory::find()->where([
            'order_id' => $order_id,
            'end_status' => null,
            'group_id' => $groupId
        ]);

        if ($modelQuery->exists()) {
            $model = $modelQuery->all();
            foreach ($model as $data) {
                $data->end_status = $end_status;
                $data->save();
            }
        }
    }

    public static function updateData($data)
    {
        /** @var self $callHistoryModel */
        $callHistoryModel = CallHistory::find()->where([
            'order_id' => $data['order_id'],
            'group_id' => $data['group_id'],
            'uniqueid' => null
        ])->orderBy(['id' => SORT_DESC])->one();

        if(!$callHistoryModel){
            $callHistoryModel = CallHistory::find()->where(['order_id' => $data['order_id']])->orderBy(['id' => SORT_DESC])->one();
        }

        if ($callHistoryModel) {
            if ($callHistoryModel->load($data, '') && $callHistoryModel->validate() && $callHistoryModel->save()) {
                return [
                    'result' => true,
                    'call_history_id' => $callHistoryModel->id
                ];
            } else {
                return [
                    'result' => false,
                    'error' => $callHistoryModel->getFirstErrorAsString()
                ];
            }
        } else {
            return [
                'result' => false,
                'error' => 'CallHistory not found'
            ];
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!is_null($this->start_time) && !is_numeric($this->start_time)) {
            $this->start_time = yii::$app->formatter->asTimestamp(date("Y-m-d H:i", strtotime($this->start_time)));
        }

        if (!is_null($this->end_time) && !is_numeric($this->end_time)) {
            $this->end_time = yii::$app->formatter->asTimestamp(date("Y-m-d H:i", strtotime($this->end_time)));
        }

        return parent::beforeValidate();
    }

    /**
     * @param $order_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCallHistoryByOrder($order_id)
    {
        $query = CallHistory::find()
            ->select([
                'id' => CallHistory::tableName() . '.id',
                'user_id' => CallHistory::tableName() . '.user_id',
                'order_id' => CallHistory::tableName() . '.order_id',
                'recordingfile' => CallHistory::tableName() . '.recordingfile',
                'username' => User::tableName() . '.username',
                'created_at' => CallHistory::tableName() . '.created_at',
                'disposition' => CallHistory::tableName() . '.disposition',
                'end_status' => CallHistory::tableName() . '.end_status',
                'billsec' => CallHistory::tableName() . '.billsec',
                'duration' => CallHistory::tableName() . '.duration',
            ])
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . CallHistory::tableName() . '.user_id')
            ->where(['order_id' => $order_id])
            ->orderBy(CallHistory::tableName() . '.id DESC');

        return $query->asArray()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranscribeQueue()
    {
        return $this->hasOne(TranscribeQueue::className(), ['call_history_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAggregateWords()
    {
        return $this->hasMany(AggregateWord::className(), ['call_history_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderLogs()
    {
        return $this->hasMany(OrderLog::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProductLogs()
    {
        return $this->hasMany(OrderProductLog::className(), ['group_id' => 'group_id']);
    }
}
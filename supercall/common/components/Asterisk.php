<?php
namespace common\components;

use yii\base\Component;

/**
 * Class Asterisk2
 * Класс для тонкой настройки интеграции с Asterisk2
 * Используется для передачи данных о записи разговоров операторов
 *
 * @package api\components
 */
class Asterisk extends Component
{
    /**
     * @var string
     */
    public $token = '4bac654ecfb4a973dc3a86cb3015aa135c16ce6d';

}

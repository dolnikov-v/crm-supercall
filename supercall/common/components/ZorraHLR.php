<?php

namespace common\components;


use api\models\ApiLog;
use Yii;
use yii\base\Component;
use yii\helpers\Console;
use yii\httpclient\Client;

class ZorraHLR extends Component
{
    const LOGGER_TYPE = 'zorra-phone-validator-pre-check';
    const ERRPR_TYPE_UNKNOWN = 'unknown error';
    const ERROR_API_FORMAT_ANSWER = 'Api zorra format answer error';
    const ERROR_API_ERROR = 'Api zorra error';
    const CODE_SUCCESS = 0;

    /** @var  boolean */
    public $test;

    /** @var  boolean */
    public $console;

    /** @var  string */
    public $url;

    /** @var  string */
    public $api_key;

    /** @var  boolean */
    public $global_off;

    /** @var  integer */
    public $batch_size_to_check;

    public $errors = [
        1 => 'Unknown subscriber',
        2 => 'Unknown subscriber - NR changed',
        5 => 'Unidentified subscriber',
        6 => 'Absent subscriber for SM',
        7 => 'Unknown equipment',
        8 => 'Roaming not alllowed',
        9 => 'Illegal subscriber',
        10 => 'Bearer service not provisioned',
        11 => 'Teleservice not provisioned',
        12 => 'Illegal equipment',
        13 => 'Call barred',
        21 => 'Facility not supported',
        27 => 'Absent subscriber',
        28 => 'Incompatible MS terminal error',
        31 => 'Subscriber busy for SMS MT',
        32 => 'Equipment failure',
        33 => 'MS memory capacity exceeded',
        34 => 'GSM system failure',
        35 => 'GSM Data Error - data missing'
    ];

    /**
     * @param $code
     * @return mixed|string
     */
    public function mapError($code)
    {
        if (isset($this->errors[$code])) {
            return $this->errors[$code];
        } else {
            return self::ERRPR_TYPE_UNKNOWN;
        }
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return new Client();
    }

    /**
     * проверка телефонов по api Zorra - по новому api 10 штук за раз
     * @param $data [[key = foreign_id => value = phone], [key = foreign_id => value = phone]]
     * @return array
     */
    public function checkPhone($data)
    {
        $phones = implode(',', array_values($data));

        //Console::output('PHONES: ' . $phones);

        $resultData = [];

        $response = $this->getClient()->createRequest()
            ->setMethod('get')
            ->setUrl($this->url)
            ->setData([
                'api_key' => $this->api_key,
                'msisdn' => $phones
            ])
            ->send();

        $logger = yii::$app->get('logger');
        $logger->action = 'check-phone';
        $logger->type = self::LOGGER_TYPE;

        if ($response->isOk) {
            if ($results = json_decode($response->content, true)) {

                if($this->console){
                    Console::output('COUNT ARRAY ANSWER: ' . count($results));
                }

                foreach ($results as $result) {

                    if (isset($result['response']) && isset($result['msisdn'])) {
                        $order_id = array_search($result['msisdn'], $data);

                        if ($this->console) {
                            Console::output('order_id: ' . $order_id . ' phone: ' . $result['msisdn']);
                        }

                        //не прошёл валидацию
                        if ($result['response'] != self::CODE_SUCCESS) {
                            //не прошёл валидацию
                            $logger->tags = [
                                'url' => $this->url,
                                'phone' => $result['msisdn'],
                                'response' => $response
                            ];

                            $logger->error = $this->mapError($result['response']);
                            $logger->save();

                            if ($this->console) {
                                Console::error('logger: error: ' . $logger->error);
                                Console::output('search order_id: ' . $order_id . ' - ' . (($order_id) ? 'true' : 'false'));
                            }

                            if ($order_id) {
                                $resultData[$order_id] = false;
                            }
                        }else{
                            if ($order_id) {
                                $resultData[$order_id] = true;
                            }
                        }
                    } else {
                        //апи ответило некорректно - пропускаем заказ
                        $logger->tags = [
                            'url' => $this->url,
                            'data' => $data,
                            'phones' => implode(',', array_values($data)),
                            'response' => $response
                        ];
                        $logger->error = self::ERROR_API_FORMAT_ANSWER;
                        $logger->save();

                        if ($this->console) {
                            Console::error('logger: error : ' . $logger->error);
                            Console::error('response 1:' . $response);
                        }
                    }
                }
            }
        } else {
            $logger->tags = [
                'url' => $this->url,
                'data' => $data,
                'phone' => implode(',', array_values($data)),
                'response' => $response
            ];

            if ($this->console) {
                Console::error('logger: error : ' . $logger->error);
                Console::error('response 2: ' . $response);
            }

            //работа в воркере, будет много логов, апи работает не стабильно
            //$logger->error = self::ERROR_API_ERROR;
            //$logger->save();
        }

        return $resultData;
    }
}
<?php

namespace common\components;

use AMQPQueue;
use backend\modules\queue\models\Queue;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Yii;
use yii\base\Component;
use yii\helpers\Json;

class Amqp extends Component
{
    const QUEUE_LOGS = 'crm-auto-call';
    const EXCHANGE = 'router';

    public $host = '';
    public $port = '';
    public $vhost = '';
    public $user = '';
    public $password = '';
    private $_connect = null;
    /** @var AMQPChannel */
    private $_channel = null;

    public $logs = [];

    public function getHost()
    {
        return $this->host;
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function setPort($port)
    {
        $this->port = $port;
    }

    public function getVhost()
    {
        return $this->vhost;
    }

    public function setVhost($vhost)
    {
        $this->vhost = $vhost;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getConnection()
    {
        return $this->_connect;
    }

    /**
     * @return AMQPChannel
     */
    public function getChannel()
    {
        return $this->_channel;
    }

    public function init()
    {
        parent::init();
        $this->_connect = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password, $this->vhost);
        $this->_channel = $this->_connect->channel();
    }

    /**
     * name: $exchange
     * type: direct
     * passive: false
     * durable: true // the exchange will survive server restarts
     * auto_delete: false //the exchange won't be deleted once the channel is closed.
     */
    public function declareExchange($name, $type = 'fanout', $passive = false, $durable = true, $auto_delete = false)
    {
        return $this->_channel->exchange_declare($name, $type, $passive, $durable, $auto_delete);
    }

    /*
      name: $queue
      passive: false
      durable: true // the queue will survive server restarts
      exclusive: false // the queue can be accessed in other channels
      auto_delete: false //the queue won't be deleted once the channel is closed.
     */
    public function declareQueue($name, $passive = false, $durable = true, $exclusive = false, $auto_delete = false)
    {
        return $this->_channel->queue_declare($name, $passive, $durable, $exclusive, $auto_delete);
    }

    public function bindQueueExchanger($queueName, $exchangeName, $routingKey = '')
    {
        $this->_channel->queue_bind($queueName, $exchangeName, $routingKey);
    }

    public function publish_message(
        $message,
        $exchangeName,
        $routingKey = '',
        $content_type = 'text/plain',
        $app_id = ''
    )
    {
        $toSend = new AMQPMessage($message, [
            'content_type' => $content_type,
            'content_encoding' => 'utf-8',
            'app_id' => $app_id,
            'delivery_mode' => 2
        ]);
        $this->_channel->basic_publish($toSend, $exchangeName, $routingKey);
        //$this->closeConnection();
        //$msg = $this->_channel->basic_get('q1');
        //var_dump($msg);
    }

    public function closeConnection()
    {

        $this->_channel->close();
        $this->_connect->close();

    }

    public function exchangeDelete($name)
    {
        $this->_channel->exchange_delete($name);
    }

    public function basicConsume(
        $queue = '',
        $consumer_tag = '',
        $no_local = false,
        $no_ack = false,
        $exclusive = false,
        $nowait = false,
        $callback = null,
        $ticket = null,
        $arguments = array())
    {
        return $this->_channel->basic_consume($queue, $consumer_tag, $no_local, $no_ack, $exclusive, $nowait, $callback);
    }

    /**
     * Очистка очереди
     * @param type $queue
     * @return type
     */
    public function queuePurge($queue)
    {
        return $this->_channel->queue_purge($queue);
    }

    /**
     * Удаление очереди
     * @param type $queue
     * @return type
     */
    public function queueDelete($queue)
    {
        return $this->_channel->queue_delete($queue);
    }

    /**
     * Получить сообщение из очереди
     *
     * @param string $queue
     * @param bool $no_ack
     * @param null $ticket
     * @return mixed
     */
    public function basicGet($queue = '', $no_ack = false, $ticket = null)
    {
        return $this->_channel->basic_get($queue, $no_ack);
    }


    public function basicAck($delivery_tag, $multiple = false)
    {
        return $this->_channel->basic_ack($delivery_tag, $multiple);
    }

    /**
     * отправка лог в очередь
     * @param string|array $data
     * @param null|string $toQueue
     */
    public function saveLog($data, $toQueue = null)
    {
        $queue = !$toQueue ? self::QUEUE_LOGS : $toQueue;

        $this->declareQueue($queue);
        $this->bindQueueExchanger($queue, self::EXCHANGE, $routingKey = $queue);

        $data = !$data ? 'false' : $data;
        $data = is_array($data) ? $data : [$data];
        $data['type'] = Yii::$app->params['urlFrontend'];

        $this->publish_message(Json::encode($data), self::EXCHANGE, $routingKey = $queue);
    }

    /**
     * прочитать логи из раббита
     * @param null|string $byQueue
     * @return null|string
     */
    public function getLog($byQueue = null, $sentKibana = false)
    {
        /** @var Logstash $logstash */
        $logstash = \Yii::$app->logstash;

        $queue = !$byQueue ? self::QUEUE_LOGS : $byQueue;

        echo ' [*] Waiting for messages.', "\n";

        $callback = function ($msg) use ($sentKibana, $logstash) {

            $message = $msg->body;

            echo " [x] Received ", $message, "\n";
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

            if ($sentKibana) {

                $logstash->debug($message);
            }

            return $msg->body;
        };

        $this->_channel->basic_qos(null, 1, null);

        $this->_channel->basic_consume(
            $queue,
            $consumer_tag = '',
            $no_local = false,
            $no_ack = false,
            $exclusive = false,
            $nowait = false,
            $callback
        );

        while (count($this->_channel->callbacks)) {
            $this->_channel->wait();
        }
    }


}

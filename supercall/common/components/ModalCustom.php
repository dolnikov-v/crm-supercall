<?php
/**
 * Created by PhpStorm.
 * User: PCWORK
 * Date: 29.11.2017
 * Time: 16:56
 */

namespace common\components;


use yii\bootstrap\Modal;
use yii\bootstrap\Html;

class ModalCustom extends Modal
{
    /**
     * * @var array body options
     */
    public $bodyOptions = ['class' => 'modal-body'];

    /**
     * Renders the opening tag of the modal body.
     * @return string the rendering result
     */
    protected function renderBodyBegin()
    {
        return Html::beginTag('div', ['class' => $this->bodyOptions]);
    }
}
<?php

namespace common\components\validator;
use yii\validators\Validator;

/**
 * Class ArrayValueIntegerValidator
 * @package common\components\validator
 */
class ArrayValueIntegerValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        $validate = array_map('trim', explode(',', $model->$attribute));

        foreach ($validate as $value) {
            if ($value > 2147483647) {
                $this->addError($model, $attribute, 'Value should not be more than 2147483647.');
                break;
            }
        }

    }
}
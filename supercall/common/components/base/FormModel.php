<?php

namespace common\components\base;

use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;

/**
 * @property string[] $attributeTypes
 */
class FormModel extends DynamicModel
{
    public $gmapMapping = [];

    /** @var string */
    public $name;

    /** @var string[] */
    protected $_attribute_labels;
    /** @var string[] */
    protected $_attribute_types;

    /**
     * @param string $name
     * @param null $label
     * @param null $type
     * @param null $value
     */
    public function defineAttribute($name, $label = null, $type = null, $value = null)
    {
        parent::defineAttribute($name, $value);
        if ($label){
            $this->_attribute_labels[$name] = $label;
        }
        if ($type){
            $this->_attribute_types[$name] = $type;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), $this->_attribute_labels);
    }

    /**
     *
     * @param $attribute
     * @return mixed
     */
    public function getAttributeType($attribute)
    {
        return ArrayHelper::getValue($this->_attribute_types, $attribute);
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return $this->name;
    }

    /**
     * @return \string[]
     */
    public function attributesTypes()
    {
        return $this->_attribute_types;
    }
}
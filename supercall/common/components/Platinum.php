<?php

namespace common\components;

use backend\modules\administration\models\BaseLogger;
use common\components\web\User;
use common\models\LiveMessage;
use common\models\QueueOrder;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;

class Platinum extends Component
{
    const CURL_TIMEOUT = 300;
    const NAME = 'Platinum';
    const LOGGER_TYPE = self::NAME;

    const TYPE_PHONE = 'customer_phone';
    const TYPE_MOBILE = 'customer_mobile';

    public $debug;

    /** @var  @var array */
    public $params;

    /** @var yii\httpclient\Client Client */
    public $client;

    /**
     * Platinum constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->client = new Client();

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public static function mapCrmFields($type)
    {
        return [
            'order_id' => 'OrderID',
            'order_products' => 'Product',
            'customer_full_name' => 'ClientName',
            $type => 'Phone',
            'char_code' => 'Geo',
            'content' => 'MessageToClient'
        ];
    }

    /**
     * @param $field
     * @return bool|mixed
     */
    public static function mapFieldToCrm($field)
    {
        $fields = self::mapPlatinumFields();

        return isset($fields[$field]) ? $fields[$field] : false;
    }

    /**
     * @return array|null
     */
    public static function mapPlatinumFields()
    {
        return [
            'text' => 'content',
            'order' => 'order_id'
        ];
    }

    /**
     * @param $field
     * @return bool|mixed
     */
    public static function mapFieldToPlatinum($field, $type)
    {
        $fields = self::mapCrmFields($type);

        return isset($fields[$field]) ? $fields[$field] : false;
    }

    /**
     * @param Order $order
     * @return array
     */
    public function createMessageFromOrder($order)
    {
        $dataFromOrder = [
            'order_id' => $order->id,
            'customer_full_name' => $order->customer_full_name,
            'order_products' => implode(', ', ArrayHelper::getColumn(
                OrderProduct::find()
                    ->joinWith(['product'])
                    ->where(['order_id' => $order->id])
                    ->andWhere(['<>', 'price', 0])
                    ->all(),
                'product.name')),
            'customer_phone' => $order->customer_phone,
            'customer_mobile' => $order->customer_mobile,
            'char_code' => $order->country->char_code,
            'content' => null
        ];


        return $this->combineData($dataFromOrder);
    }

    /**
     * @param LiveMessage $message
     * @return array
     */
    public function prepareData($message)
    {
        //формат уже подобран
        if (is_array($message)) {
            return $message;
        }

        $attributes = [
            'char_code' => $message->order->country->char_code,
            'customer_mobile' => $message->order->customer_mobile,
            'customer_phone' => $message->order->customer_phone,
            'customer_full_name' => $message->order->customer_full_name,
            'order_products' => implode(', ', ArrayHelper::getColumn(
                OrderProduct::find()
                    ->joinWith(['product'])
                    ->where(['order_id' => $message->order->id])
                    ->andWhere(['<>', 'price', 0])
                    ->asArray()->all(),
                'product.name'))
        ];

        $compared_attributes = array_merge($message->getAttributes(), $attributes);

        return $this->combineData($compared_attributes);
    }

    /**
     * @param $attributes
     * @return array
     */
    public function combineData($attributes)
    {
        //если оба номера указаны в заказе - будет соображать на пару чатов
        $types_phone = [self::TYPE_PHONE, self::TYPE_MOBILE];

        $data = [];

        foreach ($types_phone as $type_phone) {

            foreach ($attributes as $field => $value) {

                $mappedField = self::mapFieldToPlatinum($field, $type_phone);

                if ($mappedField) {
                    $data[$type_phone][$mappedField] = $value;
                }
            }

        }

        sort($data);

        return $data;
    }

    /**
     * @param array $data e.g. $data = ['OrderID' => "Order - 7888",  'Phone' => '+7999999999999', 'Product' =>  'Phone'  , 'ClientName' => 'Bob',  'Geo'=>'MY', 'MessageToClient' => $text];
     * @return array
     */
    public function addToChat($data)
    {
        /** @var BaseLogger $logger */
        $logger = yii::$app->get('logger');
        $logger->action = __METHOD__;
        $logger->type = self::LOGGER_TYPE;
        $logger->route = __CLASS__;

        $options = [
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_CONNECTTIMEOUT => self::CURL_TIMEOUT,
            CURLOPT_SSL_VERIFYPEER => false
        ];


        $ch = curl_init($this->params['WhatsappApi']['url_create']);
        curl_setopt_array($ch, $options);

        if (curl_error($ch)) {
            $error = curl_error($ch);
        }

        $result = curl_exec($ch) . "\n";
        curl_close($ch);

        if (!isset($error)) {
            $logger->tags = [
                'data' => $data,
                'url' => $this->params['WhatsappApi']['url_create'],
                'curl_options' => $options,
                'response' => $result
            ];

            $logger->save();

            return [
                'success' => true,
                'message' => $result
            ];
        } else {
            $logger->tags = $data;
            $logger->error = $error;
            $logger->save();

            return [
                'success' => false,
                'message' => $error
            ];
        }
    }

    /**
     * @param $data
     * @return array
     */
    public function setAnswer($data)
    {
        /** @var BaseLogger $logger */
        $logger = yii::$app->get('logger');
        $logger->action = __METHOD__;
        $logger->type = LiveMessage::LOGGER_TYPE;
        $logger->route = __CLASS__;

        if (!isset($data['type'])) {
            $error = 'Required parameter [type] not passed. Allowable types: ' . implode(', ', LiveMessage::getTypesCollection());
        } elseif (!in_array($data['type'], LiveMessage::getTypesCollection())) {
            $error = 'Incorrect parameter [type]. Allowable types: ' . implode(', ', LiveMessage::getTypesCollection());
        } elseif (!isset($data['order_id'])) {
            $error = 'Required parameter [order_id] not passed';
        } elseif (!isset($data['content'])) {
            $error = 'Required parameter [content] not passed';
        } elseif (empty($data['content'])) {
            $error = 'Parameter [content] value  can not be empty';
        }

        if (isset($error)) {
            $result = [
                'success' => false,
                'message' => $error
            ];
        } else {
            $model = new LiveMessage();
            $model->order_id = $data['order_id'];
            $model->type = $data['type'];
            $model->content = $data['content'];
            $model->user_id = null;
            $model->is_new = true;
            $model->created_at = time();
            $model->updated_at = time();

            if (!$model->save()) {
                $result = [
                    'success' => false,
                    'message' => $model->getErrors()
                ];

                $logger->tags = $model->getAttributes();
                $logger->error = json_encode($model->getErrors(), 256);
                $logger->save();

            } else {
                $attributes = $model->getAttributes();
                $attributes['created_at'] = yii::$app->formatter->asDate($attributes['created_at'], 'php:d/m/Y H:i:s');
                $attributes['sender'] = $model->order->customer_full_name;

                $result = [
                    'success' => true,
                    'message' => $attributes
                ];
            }
        }

        return $result;
    }

    /**
     * Отправка свежих сообщений на NodeJS
     * @param $data
     * @return string
     */
    public function sentToNodeJS($url, $data)
    {
        $data = array_merge($data, [
            'token' => base64_encode(User::ACCESS_TYPE_PLATINUM),
            'room' => $data['order_id']
        ]);
        $json_data = json_encode($data, 256);

        /*
        $this->client->setTransport(CurlTransport::className());
        $response = $this->client->createRequest()
            ->setMethod('post')
            ->setUrl($this->params['WhatsappApi']['nodeJSserver'])
            ->setOptions([
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => $json_data,
            ])->setHeaders([
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data)
            ]);

        $result = $response->send();

        //return $result->content;
        */

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data))
        );

        $res = curl_exec($ch) . "\n";
        curl_close($ch);

        return $res;
    }

    /**
     * @param $message
     */
    public function sendToPlatinum($message)
    {
        /** @var Platinum $platinum */
        $platinum = yii::$app->get(Platinum::NAME);
        $data = $platinum->prepareData($message);

        /**
         * 2 номера у заказа = 2 попытки
         */

        if (!$platinum->debug) {
            foreach ($data as $params) {
                if (!empty($params['Phone'])) {
                    $platinum->addToChat($params);
                }
            }
        }
    }
}
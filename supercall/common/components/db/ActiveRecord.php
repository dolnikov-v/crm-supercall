<?php

namespace common\components\db;

use common\models\User;
use Yii;

/**
 * Class ActiveRecord
 * @package common\components\db
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    use ModelTrait;
    const ACTION_UPDATE = 'update';
    const ACTION_CREATE = 'create';
    const ACTION_DELETE = 'delete';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        //set last activity user
        if (method_exists(yii::$app, 'getSession')) {
            if (yii::$app->user->identity && (Yii::$app->user->isGuest && !empty($_GET['user_id']))) {
                User::setLastActivity();
            }
        }

        parent::afterSave($insert, $changedAttributes);

        $newAttributes = $this->oldAttributes;
        $oldAttributes = $changedAttributes;
        $action = self::ACTION_UPDATE;
        if ($insert) {
            $action = self::ACTION_CREATE;
            $oldAttributes = [];
        }

        $tags = array_intersect_key($newAttributes, $oldAttributes);

        $_route = method_exists(yii::$app->request, 'getUrl') ? Yii::$app->request->getUrl() : 'console';

        if (isset($this->log_history) && $this->log_history === true) {
            if($action == self::ACTION_CREATE){
                $route_path = explode('/', $_route);
                $route_path[] = $this->id;
                $route = implode('/', $route_path);
            }else{
                $route = $_route;
            }

            $logger = yii::$app->get('logger');
            $logger->action = $action;
            $logger->route = $route;
            $logger->type = 'HISTORY';
            $logger->tags = $tags;
            $logger->user_id = Yii::$app->user->id;
            $logger->old_tags = $oldAttributes;
            $logger->model = get_called_class();
            $logger->save();
        }
    }
}

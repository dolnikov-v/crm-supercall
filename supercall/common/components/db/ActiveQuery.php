<?php
namespace common\components\db;

use common\models\Product;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ActiveQuery
 * @package common\components\db
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'name';

    /**
     * @var string
     */
    protected $groupValue = null;

    /**
     * @param $key
     * @return $this
     */
    public function collectionKey($key)
    {
        $this->collectionKey = $key;
        return $this;
    }

    /**
     * @param string|null $translateCategory
     * @return array
     */
    public function collection($translateCategory = null)
    {
        $all = ($this instanceof Product) ? $this->where([Product::tableName().'.active' => true])->all() : $this->all();
        $items = ArrayHelper::map($all, $this->collectionKey, $this->collectionValue, $this->groupValue);

        if (!is_null($translateCategory)) {
            foreach ($items as $key => $item) {
                $items[$key] = Yii::t($translateCategory, $item);
            }
        }

        return $items;
    }

    /**
     * @param $data
     * @return array
     */
    public function toArray($data)
    {
        return is_array($data) ? $data : [$data];
    }
}

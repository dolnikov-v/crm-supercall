<?php

namespace common\components\db;


/**
 * Trait ModelTrait
 * @package common\components\db
 */
trait ModelTrait
{
    /**
     * @return string
     */
    public static function shortClassName()
    {
        $model = new static();
        $reflect = new \ReflectionClass($model);

        return $reflect->getShortName();
    }

    /**
     * @param null $attribute
     * @return array
     */
    public function getErrorsAsArray($attribute = null)
    {
        $result = [];

        $errorsAttributes = $this->getErrors($attribute);

        foreach ($errorsAttributes as $errorsAttribute) {
            foreach ($errorsAttribute as $error) {
                $result[] = $error;
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getFirstErrorAsString()
    {
        $errors = $this->getErrorsAsArray();

        return array_shift($errors);
    }

    /**
     * Функция отобразит datetime в формате "d.m.Y H:i" в списке заказов
     * для полей created_at и updated_at
     *
     * @param $timestamp
     * @return string
     */
    public function getDatetimeByTimezone($timestamp, $ignore = false)
    {
        $date = new \DateTime();
        $date->setTimestamp($timestamp);

        if(!$ignore) {
            $date->setTimezone(new \DateTimeZone(\Yii::$app->user->timezone->timezone_id));
        }

        return $date->format('d.m.Y H:i');
    }
}
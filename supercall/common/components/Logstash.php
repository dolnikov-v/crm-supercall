<?php
namespace common\components;

use snapsuzun\yii2logger\logstash\Logger;
use yii\base\Component;
use yii\di\Instance;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;

class Logstash extends Logger
{
    /**
     * @var Amqp|string|array
     */
    public $queue;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->queue = Instance::ensure($this->queue, Amqp::class);
    }

    /**
     * @param array $data
     */
    public function saveLog(array $data)
    {
        parent::saveLog($data);
    }
}
<?php
namespace common\components;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use yii\base\Component;
use common\models\UserReady;
use yii;

class RatchetService extends Component implements MessageComponentInterface
{
    public $server_port;
    public $single_access;

    protected $users;
    protected $history;
    protected $storeUsers;
    public $conn;

    const ACTION_OPEN = 'open';
    const ACTION_CLOSE = 'close';

    public function __construct(array $config = [])
    {
        $this->users = [];
        $this->history = [];
        $this->storeUsers = new \SplObjectStorage();

        parent::__construct($config);
    }

    /**
     * event Ratchet
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->conn = $conn;
        $this->storeUsers->attach($conn);

        echo "New connection! ({$conn->resourceId})" . PHP_EOL . PHP_EOL;
    }

    /**
     * event Ratchet
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        $data = json_decode($msg);

        foreach ($this->storeUsers as $user) {
            if ($from == $user) {
                $current_user = $user;
            }
        }

        $this->history[$data->user_id][] = [
            'resourceId' => $this->conn->resourceId,
            'url' => $data->url
        ];

        $this->usersOnline();

        if (isset($current_user)) {
            //checkSingleAccess - вернёт false (0) - если нужно закрыть доступ - на клиенте - этот 0 развернёт модальное окно - и не даст работать оператору
            $current_user->send((int)$this->checkSingleAccess($data->url, $data->user_id));
        }

        //необходимо начинать учитывать время работы оператора - если он зашёл не работать с заказами, а, например - читать доки
        //но учитывать его нужно - только - если мы уже не начали его учитывать
        $this->checkUserReady(self::ACTION_OPEN);
    }


    /**
     * event Ratchet
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->storeUsers->detach($conn);

        if (isset($this->users[$conn->resourceId])) {
            unset($this->users[$conn->resourceId]);
            UserReady::changeIsReady((int)$this->users[$conn->resourceId], false);
            echo "Connection: {$conn->resourceId}, user_id: {$this->users[$conn->resourceId]} has disconnected" . PHP_EOL . PHP_EOL;
        }

        $this->usersOnline();
        $this->checkUserReady(self::ACTION_CLOSE, $conn);
        $this->dropConnectionOnHistory($conn);

        echo "Connection: {$conn->resourceId}  has disconnected" . PHP_EOL . PHP_EOL;
    }


    /**
     * event Ratchet
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        if (isset($this->users[$conn->resourceId])) {
            echo "For user_id: {$this->users[$conn->resourceId]} an error has occurred: {$e->getMessage()}" . PHP_EOL . PHP_EOL;
        }

        echo "For connection : {$conn->resourceId} an error has occurred: {$e->getMessage()}" . PHP_EOL . PHP_EOL;

        $this->usersOnline();
        $this->dropConnectionOnHistory($conn);

        //возможно пользователь вообще ушёл с сервиса, нужно закрыть его рабочее время
        $this->checkUserReady(self::ACTION_CLOSE, $conn);

        $conn->close();
    }

    public function usersOnline()
    {
        foreach ($this->history as $k => $data) {
            echo PHP_EOL . 'user_id: ' . $k . PHP_EOL;
            echo '------------------------' . PHP_EOL;

            foreach ($data as $num => $history) {
                echo $num . '. resourseId: ' . $history['resourceId'] . ' url: ' . $history['url'] . PHP_EOL;
            }
        }

        echo PHP_EOL;
    }

    public function checkSingleAccess($url_access, $user_access)
    {
        $countAccessedUrl = [];

        foreach ($this->history as $user => $data) {
            //проверка только по текущему пользователю
            if ($user_access == $user) {
                foreach ($data as $num => $history) {
                    foreach ($this->single_access as $url_single_access) {
                        if ($url_single_access == rtrim($url_access, '/')) {
                            if (isset($countAccessedUrl[$url_single_access])) {
                                $countAccessedUrl[$url_single_access] += 1;
                            } else {
                                $countAccessedUrl[$url_single_access] = 1;
                            }
                        }
                    }
                }
            }
        }
        echo 'Accessed url кол-во:' . PHP_EOL;
        echo '--------------------------------' . PHP_EOL;
        foreach ($countAccessedUrl as $url => $count) {
            echo $url . ' : ' . ($count / 2) . PHP_EOL;
            if ($count / 2 > 1) {
                $this->dropConnectionOnHistory();
                return false;
            }
        }

        return true;
    }

    public function dropConnectionOnHistory($conn = false)
    {
        if($conn){
            $this->conn = $conn;
        }

        echo 'Текущий коннект: ' . $this->conn->resourceId . PHP_EOL;

        foreach ($this->history as $k => &$data) {
            foreach ($data as $num => $history) {
                if ($history['resourceId'] == $this->conn->resourceId) {
                    echo 'Текущий коннект: #' . $this->conn->resourceId . ' исключен из активных' . PHP_EOL;
                    unset($this->history[$k][$num]);
                }
            }
        }
    }

    /**
     * @param string $action
     * @param bool|mixed $conn
     */
    public function checkUserReady($action, $conn = false)
    {
        if($conn){
            $this->conn = $conn;
        }

        $opened_sockets = 0;

        //необходимо определить пользователя по коннекту
        foreach ($this->history as $k => $data) {
            foreach ($data as $num => $history) {
                if ($history['resourceId'] == $this->conn->resourceId) {
                    $user_id = $k;
                    $opened_sockets = count($data);
                    break;
                }
            }
        }

        if (isset($user_id)) {

            echo 'Определение пользователя по коннекту: #'.$user_id.PHP_EOL;

            if (isset($user_id)) {
                $user_ready_data = UserReady::find()
                    ->where([
                        'user_id' => $user_id
                    ])->orderBy(['id' => SORT_DESC])
                    ->one();

                //если у пользователя 1 активное соединение и он уходит - закроем ему рабочий период
                if ($action == self::ACTION_CLOSE) {
                    if (is_null($user_ready_data->stop_time) && $opened_sockets == 1) {
                        UserReady::changeIsReady($user_id, false);
                        echo 'Время закрытия выставлено.' . PHP_EOL;
                    }
                }

                //если пользователь пришёл, и у него нет открытого рабочего периода - то нужно начать учитывать его рабочее время
                if ($action == self::ACTION_OPEN) {
                    if (!is_null($user_ready_data->stop_time)) {
                        UserReady::changeIsReady($user_id, true);
                        echo 'Время открытия выставлено.' . PHP_EOL;
                    }
                }

                echo PHP_EOL;
                echo 'Action: ' . $action . PHP_EOL;
                echo '--------------------' . PHP_EOL;
                echo 'Создано подключений для пользователя #' . $user_id . ': ' . $opened_sockets . PHP_EOL;
                echo '>> Stop time: '.$user_ready_data->stop_time.PHP_EOL.PHP_EOL;
            }
        }
    }

    /**
     * Проверит - сколько сокетов (сколько авторизаций, вкладок у юзера) на текущий момент
     * @param $user_id
     * @return bool
     */
    public function countConnectionOnUser($user_id)
    {
        $results = [];

        foreach ($this->users as $resId => $user) {
            if ($user == $user_id) {
                $results[] = $resId;
            }
        }

        return count($results);
    }
}
<?php
namespace common\components\i18n;

use common\helpers\i18n\Formatter as FormatterHelper;
use common\models\Language;
use common\models\Timezone;
use Yii;

/**
 * Class Formatter
 * @package common\components\i18n
 */
class Formatter extends \yii\i18n\Formatter
{
    public $locale = Language::DEFAULT_LOCALE;

    public $dateFormat;
    public $timeFormat;
    public $fullTimeFormat;
    public $datetimeFormat;
    public $dateFullTimeFormat;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->dateFormat = FormatterHelper::getDateFormat();
        $this->timeFormat = FormatterHelper::getTimeFormat();
        $this->fullTimeFormat = FormatterHelper::getFullTimeFormat();
        $this->datetimeFormat = FormatterHelper::getDatetimeFormat();
        $this->dateFullTimeFormat = FormatterHelper::getDateFullTimeFormat();
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @param \DateTime|integer|string $value
     * @param null $format
     * @return string
     */
    public function asDate($value, $format = null)
    {
        return parent::asDate($value, ($format ? $format : $this->dateFormat));
    }

    /**
     * @param \DateTime|integer|string $value
     * @param null $format
     * @return string
     */
    public function asTime($value, $format = null)
    {
        return parent::asTime($value, ($format ? $format : $this->timeFormat));
    }

    /**
     * @param $value
     * @param null $format
     * @return string
     */
    public function asFullTime($value, $format = null)
    {
        return parent::asTime($value, ($format ? $format : $this->fullTimeFormat));
    }

    /**
     * @param \DateTime|integer|string $value
     * @param null $format
     * @return string
     */
    public function asDatetime($value, $format = null)
    {
        return parent::asDatetime($value, ($format ? $format : $this->datetimeFormat));
    }

    /**
     * @param \DateTime|integer|string $value
     * @param null $format
     * @return string
     */
    public function asDateFullTime($value, $format = null)
    {
        return parent::asDatetime($value, ($format ? $format : $this->dateFullTimeFormat));
    }

    /**
     * @param \DateInterval|\DateTime|integer|string $value
     * @param null $format
     * @return string
     */
    public function asRelativeTime($value, $format = null)
    {
        return parent::asRelativeTime($value);
    }

    /**
     * @param \DateTime|integer|string $value
     * @return boolean|integer|string
     */
    public function asTimestamp($value)
    {
        $timestamp = parent::asTimestamp($value);

        if (isset(Yii::$app->user->timezone)) {
            $timestamp = (int)$timestamp - Yii::$app->user->timezone->time_offset;
        }

        return $timestamp;
    }

    /**
     * @param \DateTime|integer|string $value
     * @return boolean|integer|string
     */
    public function asTimestampWithoutTimeZone($value)
    {
        $timestamp = parent::asTimestamp($value);

        return $timestamp;
    }

    /**
     * Преобразует в объект
     * возвращает $from и $to в виде объектов \DateTime каждый
     * строку получает в формате "22/06/2017 00:00 - 22/06/2017 23:59"
     * @param string $value
     * @return array
     */
    public function asConvertToFrom($value, $timeZone = 'UTC', $useUserTimeZone = false)
    {
        $rangeArray = explode(' - ', $value);
        $utc = new \DateTimeZone($timeZone);
        if(!$useUserTimeZone){
            $timezone = new \DateTimeZone(Timezone::DEFAULT_TIMEZONE);
        } else {
            $timezone = new \DateTimeZone(Yii::$app->user->timezone->timezone_id);
        }


        // разделим на дату и время
        $fromDateAndTime = explode(' ', $rangeArray[0]);
        // время отдельно
        $fromTime = explode(':', $fromDateAndTime[1]);
        $from = \DateTime::createFromFormat('d/m/Y', $fromDateAndTime[0], $timezone)
            ->setTimezone($utc) // перевести время в дефолтный часовой пояс (по дефолту UTC)
            ->setTime($fromTime[0], $fromTime[1], 0);

        // разделим на дату и время
        $toDateAndTime = explode(' ', $rangeArray[1]);
        // время отдельно
        $toTime = explode(':', $toDateAndTime[1]);
        $to = \DateTime::createFromFormat('d/m/Y', $toDateAndTime[0], $timezone)
            ->setTimezone($utc)
            ->setTime($toTime[0],$toTime[1],59);

        $response = new \stdClass();
        $response->from = $from;
        $response->to = $to;

        return $response;
    }
}

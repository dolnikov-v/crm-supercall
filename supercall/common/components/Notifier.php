<?php
namespace common\components;

use Yii;
use yii\base\Component;

/**
 * Class Notifier
 * @package common\components
 */
class Notifier extends Component
{
    const TYPE_SUCCESS = 'success';
    const TYPE_ERROR = 'error';
    const TYPE_WARNING = 'warning';
    const TYPE_INFO = 'info';

    const SESSION_KEY = 'notifier.messages';

    /**
     * @return array
     */
    public function getNotifiers()
    {
        $notifiers = Yii::$app->session->getFlash(self::SESSION_KEY);

        return empty($notifiers) ? [] : $notifiers;
    }

    /**
     * @param string $message
     * @param string $type
     */
    public function addNotifier($message, $type = self::TYPE_ERROR)
    {
        $notifier = [
            'message' => $message,
            'type' => $type,
        ];

        $notifiers = Yii::$app->session->getFlash(self::SESSION_KEY);

        if (empty($notifiers)) {
            $notifiers = [];
        }

        $notifiers[] = $notifier;

        Yii::$app->session->setFlash(self::SESSION_KEY, $notifiers);
    }

    /**
     * @param string $message
     */
    public function addNotifierSuccess($message)
    {
        $this->addNotifier($message, self::TYPE_SUCCESS);
    }

    /**
     * @param string $message
     */
    public function addNotifierError($message)
    {
        $this->addNotifier($message, self::TYPE_ERROR);
    }

    /**
     * @param string $message
     */
    public function addNotifierWarning($message)
    {
        $this->addNotifier($message, self::TYPE_WARNING);
    }

    /**
     * @param string $message
     */
    public function addNotifierInfo($message)
    {
        $this->addNotifier($message, self::TYPE_INFO);
    }

    /**
     * @param \yii\base\Model $model
     * @param boolean $first
     */
    public function addNotifierErrorByModel($model, $first = true)
    {
        $errorsAttributes = $model->getErrors();

        foreach ($errorsAttributes as $errorsAttribute) {
            foreach ($errorsAttribute as $error) {
                $this->addNotifierError($error);

                if ($first) {
                    return;
                }
            }
        }
    }
}

<?php
namespace common\components\grid;

use yii\grid\DataColumn;

/**
 * Class IdColumn
 * @package common\components\grid
 */
class IdColumn extends DataColumn
{
    /** @var string */
    public $attribute = 'id';

    /** @var string */
    public $label = '#';

    /**
     * @var array
     */
    public $headerOptions = ['class' => 'th-record-id'];

    /**
     * @var array
     */
    public $contentOptions = ['class' => 'td-record-id'];
}

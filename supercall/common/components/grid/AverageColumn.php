<?php
    
namespace common\components\grid;


use common\modules\order\models\Order;
use Yii;
use yii\grid\DataColumn;

class AverageColumn extends DataColumn
{
    private $_total = 0;
    private $_average = 0;
    
    
    public function getDataCellValue($model, $key, $index)
    {
        // UN-50 Средний чек считать только по апрувам
        if ($model[Order::STATUS_APPROVED] != 0) {
            $this->_total += $model[Order::STATUS_APPROVED];
            $this->_average += $model[$this->attribute] * $model[Order::STATUS_APPROVED];
    
            return parent::getDataCellValue($model, $key, $index);
        }
        
        // по умолчанию
        return 0;
    }
    
    protected function renderFooterCellContent()
    {
        $avg = $this->_total != 0
            ? ($this->_average / $this->_total)
            : 0;
        
        return $this->grid->formatter->format($avg, ['decimal', 2]);
    }
}
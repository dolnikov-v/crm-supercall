<?php
    
namespace common\components\grid;


use Yii;
use yii\grid\DataColumn;

/**
 * Class RatioColumn
 *
 * Рассчитывает процент (долю),
 *
 * н-р, % новых заказов по отношению к общему кол-ву заказов за выбранный период
 * @package common\components\grid
 */
class RatioColumn extends DataColumn
{
    private $_total = 0;
    private $_indexValue = 0;
    
    public function getDataCellValue($model, $key, $index)
    {
        // Общее кол-во считаем отдельно
        $this->_total += $model['total'];
        // Найдем ID столбца
        $idArray = explode('-percent', $this->attribute);
        $id = $idArray[0];
        $this->_indexValue += $model[$id];
        
        $value = parent::getDataCellValue($model, $key, $index);
        return $value;
    }
    
    protected function renderFooterCellContent()
    {
        $avg = $this->_total !== 0
            ? $this->_indexValue / $this->_total
            : 0;
        return $this->grid->formatter->format($avg, 'percent');
    }
}
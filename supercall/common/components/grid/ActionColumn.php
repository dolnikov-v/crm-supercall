<?php
namespace common\components\grid;

use common\widgets\base\DropdownActions;
use yii\base\Model;
use yii\grid\Column;

/**
 * Class ActionColumn
 * @package common\components\grid
 */
class ActionColumn extends Column
{
    /** @var array */
    public $headerOptions = ['class' => 'th-actions'];

    /** @var array */
    public $contentOptions = ['class' => 'td-actions'];

    /** @var boolean */
    public $divider = false;

    /** @var array */
    public $items = [];

    /**
     * @param Model $model
     * @param integer $key
     * @param integer $index
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $items = [];

        foreach ($this->items as $key => $item) {
            if ($item['can']($model)) {
                if (!isset($item['label'])) {
                    $items[$key] = '';
                } else {
                    $items[$key] = [
                        'label' => $item['label'],
                        'url' => $item['url']($model),
                        'attributes' => (isset($item['attributes'])) ? $item['attributes']($model) : [],
                        'style' => (isset($item['style'])) ? $item['style'] : '',
                    ];
                }
            }
        }

        return DropdownActions::widget([
            'links' => $items
        ]);
    }
}

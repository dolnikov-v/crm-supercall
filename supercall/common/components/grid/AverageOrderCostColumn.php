<?php
    
namespace common\components\grid;


use common\modules\order\models\Order;
use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

class AverageOrderCostColumn extends DataColumn
{
    private $_total_cost = 0;
    private $_total_quantity = 0;
    public $countColumn = Order::STATUS_APPROVED;

    public function getDataCellValue($model, $key, $index)
    {
        $cost = parent::getDataCellValue($model, $key, $index);
        $quantity = ArrayHelper::getValue($model, $this->countColumn);
        $this->_total_cost += $cost*$quantity;
        $this->_total_quantity+= $quantity;

        return $this->grid->formatter->format($cost, ['decimal', 2]);
    }
    /*
    public function getDataCellValue($model, $key, $index)
    {
        $quantity = 0;
        $cost = 0;

        foreach ((array)Json::decode($model['price_data']) as $item) {
            foreach ($item as $price => $ids) {
                $this->total_quantity += count($ids);
                $this->total_cost += $price * count($ids);

                $quantity += count($ids);
                $cost += $price * count($ids);
            }
        }

        return $quantity ? $this->grid->formatter->format($cost / $quantity, ['decimal', 2]) : 0;
    }
    */
    protected function renderFooterCellContent()
    {
        $average = $this->_total_quantity != 0
            ? ($this->_total_cost / $this->_total_quantity)
            : 0;
        
        return $this->grid->formatter->format($average, ['decimal', 2]);//." ({$this->total_quantity}, {$this->total_cost})";
    }
}
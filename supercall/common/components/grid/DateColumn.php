<?php
namespace common\components\grid;

use common\components\i18n\Formatter;
use Yii;
use yii\grid\DataColumn;
use yii\helpers\Html;

/**
 * Class DateColumn
 * @package common\widgets\grid
 */
class DateColumn extends DataColumn
{
    /** @var boolean */
    public $withRelativeTime = false;

    /** @var string */
    public $formatType = 'Datetime';

    /** @var array */
    public $headerOptions = ['class' => 'th-date text-center'];

    /** @var array */
    public $contentOptions = ['class' => 'text-center'];

    /** @var string|null */
    public $timezone = null;

    /**
     * @param mixed $model
     * @param mixed $key
     * @param integer $index
     * @return string
     */
    public function renderDataCellContent($model, $key, $index)
    {
        $time = $this->getDataCellValue($model, $key, $index);

        if (is_null($time)) {
            return '—';
        } else {
            if (!in_array($this->formatType, ['Date', 'Datetime', 'FullTime', 'FullDatetime', 'RelativeTime', 'Time', 'Timestamp'])) {
                $this->formatType = 'Datetime';
            }

            $relativeTime = $this->withRelativeTime ? Html::tag('div', Yii::$app->formatter->asRelativeTime($time), ['class' => 'font-size-11 text-gray']) : '';

            /** @var Formatter $formatter */
            $formatter = Yii::$app->formatter;
            if ($this->timezone){

                if (is_callable($this->timezone)){
                    $timezone = call_user_func($this->timezone, $model);
                }
                else{
                    $timezone = $this->timezone;
                }

                $default_timezone = $formatter->timeZone;
                $formatter->timeZone = $timezone;
                $content = Html::tag('div', $formatter->{"as{$this->formatType}"}($time)) . $relativeTime;
                $formatter->timeZone = $default_timezone;
            }
            else {
                $content = Html::tag('div', $formatter->{"as{$this->formatType}"}($time)) . $relativeTime;
            }

            return $content;
        }
    }
}

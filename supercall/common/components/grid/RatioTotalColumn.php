<?php

namespace common\components\grid;

use common\modules\order\models\Order;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * Class RatioTotalColumn
 * @package common\components\grid
 */
class RatioTotalColumn extends DataColumn
{
    private $_total = 0;
    private $_approve = 0;
    private $_indexValue = 0;
    /** @var ActiveDataProvider|ArrayDataProvider */
    public $dataProviderWithoutPaginate;

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return string
     */
    public function getDataCellValue($model, $key, $index)
    {
        $total = $this->getTotal($this->dataProviderWithoutPaginate, 'total');
        $approve = $this->getTotal($this->dataProviderWithoutPaginate, 'status_quantity_' . Order::STATUS_APPROVED);
        $column = $this->getTotal($this->dataProviderWithoutPaginate, $this->attribute);

        // Общее кол-во считаем отдельно
        $this->_total = $total > 0 ? ($column > 0 ? ($column / $total) : 0) : 0;
        $this->_approve = $approve > 0 ? ($column > 0 ? ($column / $approve) : 0) : 0;


        // Найдем ID столбца
        $idArray = explode('-percent', $this->attribute);
        $id = $idArray[0];
        $this->_indexValue += $model[$id];

        $value = parent::getDataCellValue($model, $key, $index);
        return $value;
    }

    /**
     * @return string
     */
    protected function renderFooterCellContent()
    {
        if($this->attribute == 'buyout'){
            return yii::$app->formatter->asPercent($this->_approve);
        }
        return yii::$app->formatter->asPercent($this->_total);
    }
}
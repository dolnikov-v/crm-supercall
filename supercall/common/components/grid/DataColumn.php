<?php

namespace common\components\grid;

use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * Class DataColumn
 * @package common\components\grid
 */
class DataColumn extends \yii\grid\DataColumn
{
    /**
     * @param ActiveDataProvider|ArrayDataProvider $provider
     * @param $columnName
     * @return int
     */
    public function getTotal($provider, $columnName)
    {
        $total = 0;
        foreach ($provider->getModels() as $item) {
            $total += $item[$columnName];
        }
        return $total;
    }

    /**
     * @param ActiveDataProvider|ArrayDataProvider $provider
     * @param $columnName
     * @param $countColumn
     * @return int
     */
    public function getSumOrderCost($provider, $columnName, $countColumn)
    {
        $total = 0;
        foreach ($provider->getModels() as $item) {
            $total += $item[$countColumn] * $item[$columnName];
        }
        return $total;
    }
}
<?php

namespace common\components\grid;

use common\modules\order\models\Order;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class AverageTotalOrderCostColumn extends DataColumn
{
    private $_total_cost = 0;
    private $_total_quantity = 0;
    public $countColumn = Order::STATUS_APPROVED;
    /** @var ActiveDataProvider|ArrayDataProvider */
    public $dataProviderWithoutPaginate;

    public function getDataCellValue($model, $key, $index)
    {
        $cost = parent::getDataCellValue($model, $key, $index);
        $this->_total_quantity = $this->getTotal($this->dataProviderWithoutPaginate, $this->countColumn);
        $this->_total_cost = $this->getSumOrderCost($this->dataProviderWithoutPaginate, $this->attribute, $this->countColumn);

        return $this->grid->formatter->format($cost, ['decimal', 2]);
    }

    protected function renderFooterCellContent()
    {
        $average = $this->_total_quantity != 0
            ? ($this->_total_cost / $this->_total_quantity)
            : 0;

        return $this->grid->formatter->format($average, ['decimal', 2]);
    }
}
<?php
namespace common\components\grid;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class GridView
 * @package common\widgets\grid
 */
class GridView extends \yii\grid\GridView
{
    /**
     * @var array
     */
    public $tableOptions = ['class' => 'table table-striped table-hover table-sortable table-responsive'];
    /**
     * @var array
     */
    public $rowOptions = ['class' => 'tr-vertical-align-middle'];

    /**
     * @var array
     */
    public $options = ['tag' => false];

    /**
     * @var string
     */
    public $layout = '{items}';

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        $config['tableOptions'] = isset($config['tableOptions']) ? ArrayHelper::merge($this->tableOptions, $config['tableOptions']) : $this->tableOptions;
        $this->emptyText = Yii::t('common', 'Данные отсутствуют');

        parent::__construct($config);
    }

    public function renderItems()
    {
        return Html::tag('div', parent::renderItems(), ['class' => 'table-responsive']);
    }
}

<?php

namespace common\components\grid;

use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * Class NumberTotalColumn
 * @package common\components\grid
 */
class TimeTotalColumn extends DataColumn
{
    private $_total = 0;
    /** @var ActiveDataProvider|ArrayDataProvider */
    public $dataProviderWithoutPaginate;

    public function getDataCellValue($model, $key, $index)
    {
        $value = parent::getDataCellValue($model, $key, $index);
        $this->_total = $this->getTotal($this->dataProviderWithoutPaginate, $this->attribute);
        //todo заменить на задаваемый формат
        return date("H:i", mktime(0, 0, $value));
    }

    protected function renderFooterCellContent()
    {
        return $this->grid->formatter->format(date("H:i", mktime(0, 0, $this->_total)), $this->format);
    }
}
<?php
    
namespace common\components\grid;


use yii\grid\DataColumn;

class TotalColumn extends DataColumn
{
    private $_total = 0;

    public $totalColumn = 'total';
    
    /**
     * UN-54 Общее кол-во попыток считаем перемножением на кол-во заказов
     *
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return int|string
     */
    public function getDataCellValue($model, $key, $index)
    {
        $this->_total += $model[$this->attribute] * intval($model[$this->totalColumn]);
        
        return parent::getDataCellValue($model, $key, $index);
    }
    
    protected function renderFooterCellContent()
    {
        return $this->_total;
    }
}
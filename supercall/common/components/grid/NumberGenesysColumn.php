<?php
    
namespace common\components\grid;


use Yii;
use yii\grid\DataColumn;

class NumberGenesysColumn extends DataColumn
{
    private $_total = 0;
    private $_total_available = 0;
    
    public function getDataCellValue($model, $key, $index)
    {
        $value = parent::getDataCellValue($model, $key, $index);
        $this->_total += $value;
        $this->_total_available += $model['orders_available'];
        return $value;
    }
    
    protected function renderFooterCellContent()
    {
        return $this->_total. ' (' .$this->_total_available. ')';
    }
}
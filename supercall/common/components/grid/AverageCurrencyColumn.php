<?php
    
namespace common\components\grid;


use common\models\Cbr;
use common\models\Country;
use common\modules\order\models\Order;
use Yii;
use yii\base\ErrorException;
use yii\grid\DataColumn;
use yii\helpers\Html;

/**
 * Расчитывает средний чек только c конвертацией в ЕВРО
 * используется толко при группировке по странам
 *
 * Class AverageCurrencyColumn
 * @package common\components\grid
 */
class AverageCurrencyColumn extends DataColumn
{
    private $_total = 0;
    private $_euro = 0;
    
    
    public function getDataCellValue($model, $key, $index)
    {
        // UN-50 Средний чек считать только по апрувам
        if ($model[Order::STATUS_APPROVED] != 0) {
            $this->_total += $model[Order::STATUS_APPROVED];
            $value = parent::getDataCellValue($model, $key, $index);
            
            // проверка, если Статистика сгруппирована по странам
            $countries = Country::getAllowCountries();
            if (in_array($model['time'], $countries)) {
    
                $currencyQuery = Country::findOne(array_search($model['time'], $countries))->currency;
                // если валюта для страны указана
                if ($currencyQuery) {
                    $currency = $currencyQuery->char_code;
                    return parent::getDataCellValue($model, $key, $index) . ' ' .$currency. ' ' . $this->getCount($currency, $value);
                }
                
            }
            
            return $value;
        }
        
        // по умолчанию
        return 0;
    }
    
    /**
     * Подсчет в итоговоую строку таблицы
     * @return string
     */
    protected function renderFooterCellContent()
    {
        $avg = $this->_total != 0
            ? ($this->_euro / $this->_total)
            : 0;
        
        return $this->grid->formatter->format($avg, ['decimal', 2]). html_entity_decode(' &euro;', ENT_QUOTES, "UTF-8");
    }
    
    /**
     * Пересчитывается текущая валюта в ЕВРО
     *
     * @param $currency
     * @return string
     */
    private function getCount($currency, $value)
    {
        $cbr = new Cbr();
        if ($cbr->load()) {
            
            $currentCurr = $cbr->get($currency);
            // Конвертация именно в ЕВРО
            $euro = $cbr->get('EUR');
            
            // чтобы избежать выброса исключений при отсутствии курса валют
            try {
                $rate = (($euro['value'] * $currentCurr['nominal']) / $currentCurr['value']) / $euro['nominal'];
                
            } catch (ErrorException $Exception) {
                $rate = 0;
            } finally {
                $countEuro = $value / $rate;
                $this->_euro += $countEuro;
                $count = \Yii::$app->formatter->asDecimal($countEuro, 2);
                
                if ($currency != 'EUR')
                    return html_entity_decode('&nbsp;&asymp;&nbsp;' . $count . ' &euro;', ENT_QUOTES, "UTF-8");
            }
        }
        
    }
}
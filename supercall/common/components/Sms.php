<?php

namespace common\components;

use yii\base\Component;
use yii\helpers\FileHelper;

/**
 * Компонент для отправки смс-сообщений
 * @package common\components
 */
class Sms extends Component
{
    /**
     * @param $to
     * @param $text
     */
    public function send($to, $text)
    {
        FileHelper::createDirectory(\Yii::getAlias('@runtime/sms'));
        file_put_contents(\Yii::getAlias('@runtime/sms/'.$to.'-'.uniqid().'.txt'), $text);
    }
}
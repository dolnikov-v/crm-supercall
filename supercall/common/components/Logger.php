<?php

namespace common\components;

use yii\base\Component;
use Yii;
use yii\httpclient\Exception;

/**
 * Class Logger
 * @package common\components
 *
 *  $log = yii::$app->get('logger');
 *  $log->route = Yii::$app->urlManager->parseRequest(Yii::$app->request);
 *  $log->action = 'move order to error_lead';
 *  $log->type = 'sms';
 *  $log->error = '.....';
 *  $log->add(['phone_to' => 322223, 'phone_from' => 1234567, 'trigger' => 'send.buyout.week'])
 *  $log->save();
 */
class Logger extends Component
{
    const LOG_WRONG_TAGS = 'теги переданны не массивом';
    /**
     * @var string в какую таблицу пишем
     */
    public $table_log_class;
    public $table_tag_log_class;

    public $table_log;
    public $table_tag_log;

    /**
     * @var string действие|route, которое логируем
     */
    public $route = null;


    /**
     * @var string действие, которое логируем
     */
    public $action = null;

    /**
     * @var string
     */
    public $type = null;

    /**
     * @var null
     */
    public $error = null;
    
    /**
     * @var array
     */
    public $old_tags = [];
    
    /**
     * @var null
     */
    public $model = null;
    
    /**
     * @var integer
     */
    public $user_id = null;

    /**
     * @var array
     */
    public $tags = [];

    public function initialize()
    {

        $logger = yii::$app->get('logger');

        $this->table_log = new $logger->table_log_class;
        $this->table_tag_log = new $logger->table_tag_log_class;
    }

    /**
     * @param $array
     */
    public function addTags($array)
    {
        if (!is_array($array)) {
            $tags = [
                'errorLog' => yii::t('common', self::LOG_WRONG_TAGS)
            ];
            $this->tags = $tags;
        } else {
            $this->tags = $array;
        }
    }

    public function save()
    {
        $this->initialize();
        $transaction = yii::$app->db->beginTransaction();

        try {
            $log = clone $this->table_log;
            $log->route = $this->route;
            $log->action = $this->action;
            $log->type = $this->type;
            $log->error = $this->error;
            $log->tags = json_encode($this->tags, JSON_UNESCAPED_UNICODE);
            $log->old_tags = json_encode($this->old_tags, JSON_UNESCAPED_UNICODE);
            $log->model = $this->model;
            $log->user_id = $this->user_id;
            $log->created_at = time();

            if ($log->save()) {
                $log_id = yii::$app->db->getLastInsertID();

                if (!empty($this->tags)) {

                    foreach ($this->tags as $tag => $value) {
                        if (!is_array($value)) {
                            $logTag = clone $this->table_tag_log;
                            $logTag->log_id = $log_id;
                            $logTag->tag = (string)$tag;
                            $logTag->value = (string)$value;
                        
                            $logTag->save();
                        }
                    }
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }

    }


}
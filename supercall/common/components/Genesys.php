<?php
namespace common\components;

use common\modules\order\models\genesys\Call;
use common\modules\order\models\Order;
use common\modules\order\models\OrderGenesys;
use common\modules\order\models\OrderProduct;
use common\modules\partner\models\PartnerSettings;
use Yii;
use yii\base\Component;
use yii\base\InvalidParamException;
use yii\db\Connection;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class Genesys
 * @package api\components
 */
class Genesys extends Component
{
    /**
     * @var string
     */
    public $token = '';

    /**
     * @var string
     */
    public $secretKey = '';

    /**
     * @var Connection
     */
    public $db;

    /**
     * @var Connection
     */
    public $db2;
    
    const STATUS_APPROVED = 34; // Одобрен
    const STATUS_REJECTED = 35; // Отклонен
    const STATUS_TRASH    = 37; // Треш
    const STATUS_DOUBLE   = 36; // Дубль
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->secretKey)) {
            throw new InvalidParamException(Yii::t('common', 'Ошибка инициализации Genesys.'));
        }

        $this->initDatabase();
    }

    /**
     * @return Connection
     */
    public function getDb()
    {
        return $this->db;
    }
    
    /**
     * Сопоставляет статус заказа из CRM соответствующим ему значением из ORACLE
     * по дефолту вернет весь массив финальных статусов
     *
     * @param null $orderStatus
     * @return array|mixed
     */
    private function getFinallyStatus($orderStatus = null)
    {
        $genesysOrderStatus = [
            Order::STATUS_APPROVED => self::STATUS_APPROVED,
            Order::STATUS_REJECTED => self::STATUS_REJECTED,
            Order::STATUS_TRASH    => self::STATUS_TRASH,
            Order::STATUS_DOUBLE   => self::STATUS_DOUBLE,
        ];
        
        if ($orderStatus && array_key_exists($orderStatus, $genesysOrderStatus)) {
            return $genesysOrderStatus[$orderStatus];
        }
        
        // дефолту вернуть весь массив статусов
        return $genesysOrderStatus;
    }

    /**
     * Init database
     */
    protected function initDatabase()
    {
        $class = isset($this->db['class']) ? $this->db['class'] : 'yii\db\Connection';
        unset($this->db['class']);

        $this->db = new $class($this->db);

        $class = isset($this->db2['class']) ? $this->db2['class'] : 'yii\db\Connection';
        unset($this->db2['class']);

        $this->db2 = new $class($this->db2);
        if ($this->db2->dsn){
            try {
                $this->db2->createCommand("alter session set nls_date_format='yyyy-mm-dd hh24:mi:ss'")->execute();
            } catch (Exception $e) {
                Yii::$app->notifier->addNotifierError(Yii::t('common', 'Genesys недоступен'));
                return false;
            } finally {
                return true;
            }
        }
    }

    /**
     * Добавление заказа в БД Genesys
     *
     * @param Order $order
     * @param bool $allowedUpdate - параметр, который разрешает сделать update записи в Genesys
     * @throws \Exception
     */
    public function syncOrder(Order $order, $allowedUpdate = false)
    {
        $table = 'CALLINGLIST_'.$order->getLanguageCode();
    
        $settings = PartnerSettings::find()
            ->select(['daily_from', 'daily_till'])
            ->where([
                'partner_id' => $order->partner_id,
                'country_id' => $order->country_id,
                'type_id'    => $order->type_id,
            ])->one();
        
        
        $daily_from = $settings->daily_from;
        $daily_till = $settings->daily_till;

        //первичный ключ таблицы прозвона CHAIN_ID + CHAIN_N
        //где CHAIN_ID должен быть равен RECORD_ID
        //CHAIN_N - номер в цепочке телефонов. если 3 телефона по клиенту, то будет 3 записи по 1 заказу
        $attrs = [
            'CONTACT_INFO' => null,
            'CONTACT_INFO_TYPE' => '0',
            'RECORD_TYPE' => '2',
            'RECORD_STATUS' => '1',
            'CALL_RESULT' => '28',
            'ATTEMPT' => '0',
            'DIAL_SCHED_TIME' => null,
            'CALL_TIME' => null,
            'DAILY_FROM' => !empty($daily_from) ? $daily_from : 32400, //c 9 утра
            'DAILY_TILL' => !empty($daily_till) ? $daily_till : 75600, //по 21 вечера
            'TZ_DBID' => ArrayHelper::getValue($order, 'country.timezone.genesys_id'),
            'CAMPAIGN_ID' => null,
            'AGENT_ID' => null,
            'CHAIN_ID' => null,
            'CHAIN_N' => 1,
            'GROUP_ID' => null,
            'APP_ID' => null,
            'TREATMENTS' => null,
            'MEDIA_REF' => null,
            'SWITCH_ID' => null,
            'PRODUCT_ID' => $order->id,
            'PRODUCT_LANGUAGE' => $order->languageName,
            'PRODUCT_TIMESTAMP' => !empty($order->ordered_at) ? $order->ordered_at : $order->created_at,
            'CUSTOMER_NAME' => $order->customerFullName,
            'PRODUCT_NAME' => implode(', ', array_map(function(OrderProduct $op){
                return $op->product->name;
            }, $order->orderProducts)),
            'PRODUCT_COUNT' => array_sum(array_map(function(OrderProduct $op){
                return $op->quantity;
            }, $order->orderProducts)),
            'PRODUCT_TYPE' => $order->type->id,
            'CRM_CREATE' => $order->created_at
        ];

        /** @var OrderGenesys[] $records имеющиеся записи об экспорте */
        $records = OrderGenesys::find()->where(['order_id' => $order->id])->orderBy(['index' => SORT_ASC])->all();

        if (count($records) > 0) {
            $record = $records[0];
        }
        else {
            $record = new OrderGenesys();
            $record->order_id = $order->id;
            $record->index = 1;
            $record->phone = $order->customer_phone;
            $record->genesys_id = $this->getNewId($table);
        }

        $attrs['RECORD_ID'] = $record->genesys_id;
        $attrs['CONTACT_INFO'] = $order->customer_phone;
        $attrs['CHAIN_ID'] = $record->genesys_id;
        $attrs['CHAIN_N'] = 1;
        $chain_id = $record->genesys_id;

        $existing = (new Query())
            ->select('RECORD_ID')
            ->from($table)
            ->where(['RECORD_ID' => $record->genesys_id])
            ->scalar($this->getDb());

        if ($existing){
            if ($allowedUpdate) {
                $this->getDb()->createCommand()
                    ->update($table, $attrs, ['RECORD_ID' => $record->genesys_id])
                    ->execute();
            }
        }
        else{
            $this->getDb()->createCommand()
                ->insert($table, $attrs)
                ->execute();
        }

        $record->save(false);


        foreach ($order->phones as $i => $phone) {
            $index = $i+1;

            $record = ArrayHelper::getValue($records, $index);
            if (!$record){
                $record = new OrderGenesys();
                $record->order_id = $order->id;
                $record->genesys_id = $chain_id+$index;
            }
            $record->index = $index+1;
            $record->phone = $phone;

            $attrs['RECORD_ID'] = $record->genesys_id;
            $attrs['CONTACT_INFO'] = $phone;
            $attrs['CHAIN_ID'] = $chain_id;
            $attrs['CHAIN_N'] = $index+1;

            if ($attrs['RECORD_ID'] != $record->genesys_id){
                throw new \Exception();
            }

            $existing = (new Query())
                ->select('RECORD_ID')
                ->from($table)
                ->where(['RECORD_ID' => $record->genesys_id])
                ->one($this->getDb());

            if ($existing) {
                // Только если разрешено обновление данных в Genesys
                if ($allowedUpdate) {
                    $this->getDb()->createCommand()
                        ->update($table, $attrs, ['RECORD_ID' => $record->genesys_id])
                        ->execute();
                }
            }
            else{
                $this->getDb()->createCommand()
                    ->insert($table, $attrs)
                    ->execute();
            }

            $record->save(false);
        }
    }
    
    /**
     * Функция пишет в табл ORACLE "GEN_OCS.PREDIAL" данные заказа,
     * который получил финальный статус
     *
     * @param Order $order
     * @return bool
     */
    public function syncPredial(Order $order)
    {
        // финальный статус из CRM переведем в финальный статус ORACLE
        $status = $this->getFinallyStatus($order->status);
    
        $db = \Yii::$app->genesys->db;
        $result = false; // по умолчанию записи в табл.PREDIAL
        $transaction = $db->beginTransaction();
        try {
            $db->createCommand()->insert('PREDIAL', [
                    'ORDER_ID'        => $order->id,
                    'ORDER_STATUS'    => $status,
                    'MAKE_CALL'       => 'false',
                    'ORDER_TIMESTAMP' => $order->ordered_at,
                ])
                ->execute();
        
            $transaction->commit();
            $result = true;
            
        } catch(\Throwable $e) {
            $transaction->rollBack();
            $result = false;
            
        } finally {
            // вернем результат записи в событие Order
            return $result;
        }
        
    }
    
    public function updateOrder(Order $order)
    {
        /** @var Call $last_call */
        $last_call = Call::find()
            ->where(['PRODUCT_ID' => $order->id])
            ->orderBy(['CALL_START' => SORT_DESC])
            ->one();
        if ($last_call && $last_call->LAST_ATTEMPT == 'true'){
            $order->type_id = 7;//тестирование аудита автоотклона
            $order->beginSave();
            $order->save(false);
            $order->completeSave();
            if (!$order->partner->getSettingsByOrder($order)->genesys_sync_on_save) {
                $order->addToQueue();
            }
            return true;
        }
        return false;
    }

    /**
     * Возвращает ID для новой записи
     * @param $table
     * @param string $pk
     * @return false|null|string
     */
    public function getNewId($table, $pk = 'RECORD_ID')
    {
        return (int)$this->getDb()->createCommand(
            'SELECT MAX(' . $pk . ') AS LAST_ID FROM ' . $table
            )->queryScalar() + 1;
    }

}

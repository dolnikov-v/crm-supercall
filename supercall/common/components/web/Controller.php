<?php

namespace common\components\web;

use Yii;
use yii\helpers\Url;
use yii\web\Controller as YiiController;

/**
 * Class Controller
 * @package common\components\web
 */
class Controller extends YiiController
{
    /**
     * Функция готовит данные для фильтра из сессии, либо обновляет данные в сессии для данного фильтра
     *
     * @return mixed
     */
    protected function getFilterBySession($filterSessId, &$query)
    {
        // необходимо для сохранения фильтра в сессии
        $session = Yii::$app->session;
        $query = Yii::$app->request->queryParams;

        // Сбросить сессию для фильтра
        if (array_key_exists('reset', $query)) {
            Yii::$app->session->remove($filterSessId);
            return $this->redirect(Url::canonical());

        } else {
            // Если есть в сессии, а страница открыта с пустым фильтром
            if (!$session->has($filterSessId) || !empty($query)) {
                $session->set($filterSessId, $query);
            }

            $query = $session->get($filterSessId);
        }
    }

    /**
     * Запоминает в сессии свернута панела или развернута
     *
     * $keySession - это url формата "stats/log/list"
     * $value - это значение, фильтр должен быть открыт или закрыт, true - открыт, false - закрыт
     */
    public function actionVisibleBlock()
    {
        $keySession = Yii::$app->request->post('keySession');
        $value = (bool)Yii::$app->request->post('value');

        Yii::$app->session->set($keySession . '.isVisible', $value);
        return Yii::$app->session->get($keySession . '.isVisible');
    }

    public function init()
    {

        if (\Yii::$app->user->isGuest) {
            Yii::$app->user->language->id = 2;
            Yii::$app->language = 'en-US';
        } else {
            $language = Yii::$app->user->getLanguage();
            Yii::$app->language = $language->locale;
            Yii::$app->formatter->setLocale($language->locale);
        }

        parent::init(); // TODO: Change the autogenerated stub

    }
}

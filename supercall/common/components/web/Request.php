<?php
namespace common\components\web;

use Yii;
use yii\base\InvalidConfigException;

/**
 * Class Request
 * @package common\components\web
 */
class Request extends \yii\web\Request
{
    /**
     * @var string
     */
    private $countrySlug;
    private $timezoneId;

    /**
     * @var array
     */
    private $ignoredModules = [
        'auth',
    ];

    /**
     * @return string
     * @throws InvalidConfigException
     */
    protected function resolvePathInfo()
    {
        // $this->setCountrySlug();
        // для таймзоны
        $this->setTimezoneId();

        $url = $this->getUrl();

        if ($this->timezoneId) {
            $url = mb_substr($url, mb_strlen($this->timezoneId) + 1);
        }

        $this->setUrl($url);

        return parent::resolvePathInfo();
    }

    /**
     * @return string
     */
    private function setCountrySlug()
    {
        $url = $this->getUrl();

        $partialsUrl = explode('/', $url);
        $slug = isset($partialsUrl[1]) ? $partialsUrl[1] : null;

        Yii::$app->user->setCountry($slug);

        if (Yii::$app->user->getCountry() && $slug == Yii::$app->user->getCountry()->slug) {
            $this->countrySlug = $slug;
        }
    }
    
    /**
     * метод устанавливает Часовой пояс
     */
    private function setTimezoneId()
    {
        $url = $this->getUrl();

        $partialsUrl = explode('/', $url);
        $timezoneId = isset($partialsUrl[1]) ? $partialsUrl[1] : null;

        Yii::$app->user->setTimezone($timezoneId);

        if (Yii::$app->user->getTimezone() && $timezoneId == Yii::$app->user->getTimezone()->id) {
            $this->timezoneId = $timezoneId;
        }
    }
}

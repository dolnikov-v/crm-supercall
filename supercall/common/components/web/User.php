<?php
namespace common\components\web;

use backend\modules\operator\models\ProductOperator;
use common\models\Country;
use common\models\Language;
use common\models\Timezone;
use common\models\User as UserModel;
use common\models\UserCountry;
use common\modules\partner\models\Partner;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * Class User
 * @package common\components\web
 * @property UserModel|null $identity
 * @property boolean $isSuperadmin
 * @property boolean $isReady
 * @property boolean $checkUnready
 * @property Language $language
 * @property Country $country
 * @property Timezone $timezone
 * @property integer $last_activity
 * @property string $ip
 *
 */
class User extends \yii\web\User
{
    const ACCESS_TYPE_USER = 'user';
    const ACCESS_TYPE_PARTNER = 'partner';
    const ACCESS_TYPE_GENESYS = 'genesys';
    const ACCESS_TYPE_PAY = '2wtrade-pay';
    const ACCESS_TYPE_DINA = 'dina';
    const ACCESS_TYPE_ASTERISK = 'asterisk';
    const ACCESS_TYPE_PLATINUM = 'platinum';
    const ACCESS_TYPE_CC = 'crm';

    /**
     * Пермишены собранные не по экшену, а тупо для ограничений доступа
     * @var array
     */
    public $exceptionPermissions = [
        'order.index.viewcontacts'
    ];

    /** @var string */
    public $accessType = self::ACCESS_TYPE_USER;

    /** @var array */
    private $roles = [];
    // включен ли статус Готов?
    private $isReady;
    // переводить в статус Неготов?
    private $checkUnready;

    /** @var array */
    private $permissions;

    /** @var boolean */
    private $isSuperadmin;

    /** @var Language */
    private $language;

    /** @var Country */
    private $country;

    // Доступ пользователю к списку опр.стран, в соответствии с правами
    private $countries;

    /** @var Timezone */
    private $timezone;

    protected $userModel;

    /** @var boolean */
    public $webrtc;

    /** @var  integer */
    public $last_activity;

    /** @var  boolean */
    public $use_messenger;

    /** @var  string */
    public $ip;

    /** @var  Partner[] */
    public $partners;

    /**
     * @param string $permissionName
     * @param array $params
     * @param boolean $allowCaching
     * @return boolean
     */
    public function can($permissionName, $params = [], $allowCaching = true)
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        if (is_null($this->roles)) {
            $this->initRoles();
        }

        if ($this->getIsSuperadmin()) {
            return true;
        }

        if (is_null($this->permissions)) {
            $this->initPermissions();
        }

        $part = explode('.', $permissionName);
        // Если разрешен доступ ко всем методам Контроллера
        // Т.е. указан "модуль"."контроллер", н-р, partner.vat
        if (count($part) == 2) {
            return array_key_exists($part[0] . '.' . $part[1], $this->permissions);
        }

        // или разрешен доступ только для "partner.vat.index",
        // но данной роли разрешен доступ ко всем методам "partner.vat"

        //$part[0] == 'partner' &&
        if (count($part) >= 2 && array_key_exists($part[0] . '.' . $part[1], $this->permissions) && !in_array($permissionName, $this->exceptionPermissions)) {
            return true;
        }

        return array_key_exists($permissionName, $this->permissions);
    }

    /**
     * @param null $slug
     * @throws HttpException
     */
    public function setCountry($slug = null)
    {
        if (!$this->isGuest) {
            $country = null;

            if ($slug) {
                $query = Country::find()->active();

                if (!$this->getIsSuperadmin()) {
                    $query->joinWith(['userCountry'])
                        ->where([UserCountry::tableName() . '.user_id' => $this->id]);
                }

                $country = $query->bySlug($slug)->one();
            }

            if (!$country && Yii::$app->session->get('user.country.slug')) {
                $this->country = Country::find()
                    ->active()
                    ->bySlug(Yii::$app->session->get('user.country.slug'))->one();
            } else {
                $this->country = $country;
            }

            if (is_null($this->country)) {
                $this->country = Country::getDefault();
            }

            if ($this->country) {
                Yii::$app->session->set('user.country.slug', $this->country->slug);
            } else {
                throw new HttpException(400, Yii::t('common', 'У вас не назначена страна.'));
            }
        }
    }

    /**
     * @return boolean
     */
    public function getIsSuperadmin()
    {
        if ($this->isGuest) {
            return false;
        }

        if (is_null($this->isSuperadmin)) {
            $this->isSuperadmin = $this->identity->isSuperadmin;

            if (is_null($this->isSuperadmin)) {
                $this->isSuperadmin = false;
            }
        }

        return $this->isSuperadmin;
    }

    /**
     * @return Language|null
     * @throws HttpException
     */
    public function getLanguage()
    {
        if (is_null($this->language)) {
            /** @var Language $language */
            $language = Language::findOne(Yii::$app->session->get('user.language.id'));

            if ($language && $language->active) {
                $this->language = $language;
            } else {
                $this->language = Language::getDefault();
            }
        }

        if (is_null($this->language)) {
            throw new HttpException(400, Yii::t('common', 'Ошибка инициализации языка интерфейса.'));
        }

        return $this->language;
    }

    /**
     * @return Country
     * @throws HttpException
     */
    public function getCountry()
    {
        if (is_null($this->country)) {
            $this->country = Country::find()->where(['slug' => Yii::$app->session->get('user.country.slug')])->one();

            if (is_null($this->country)) {
                $this->country = Country::getDefault();
            }
        }

        if (is_null($this->country)) {
            throw new HttpException(400, Yii::t('common', 'У вас не назначена страна.'));
        }

        return $this->country;
    }

    /**
     * @return Timezone
     * @throws HttpException
     */
    public function getTimezone()
    {
        if (is_null($this->timezone)) {
            if ($this->isGuest) {
                $this->timezone = Timezone::getDefault();
            } else {
                if (is_null($this->identity->timezone_id)) {
                    if ($this->getCountry()->timezone_id) {
                        $this->timezone = Timezone::findOne($this->getCountry()->timezone_id);
                    } else {
                        $this->timezone = Timezone::getDefault();
                    }
                } else {
                    $this->timezone = Timezone::findOne($this->identity->timezone_id);
                }
            }
        }

        if (is_null($this->timezone)) {
            throw new HttpException(400, Yii::t('common', 'У вас не назначена временная зона.'));
        }

        return $this->timezone;
    }

    /**
     * Устанавливает часовой пояс
     *
     * @param null $id
     * @throws HttpException
     */
    public function setTimezone($id = null)
    {
        if (!$this->isGuest) {
            $timezone = null;

            // ID таймзоны числовой
            if ((int)$id) {
                $query = Timezone::find();
                $timezone = $query->byId($id)->one();
            }

            if (!$timezone && Yii::$app->session->get('user.timezone.id')) {
                $this->timezone = Timezone::find()
                    ->byId(Yii::$app->session->get('user.timezone.id'))->one();
            } else {
                $this->timezone = $timezone;
            }

            if (is_null($this->timezone)) {
                $this->timezone = Timezone::getDefault();
            }

            if ($this->timezone) {
                Yii::$app->session->set('user.timezone.id', $this->timezone->id);
            } else {
                // @todo Может устанавливать дефолтный Часовой пояс
                throw new HttpException(400, Yii::t('common', 'У вас не определен часовой пояс.'));
            }
        }
    }

    /**
     * Инициализация ролей
     */
    private function initRoles()
    {
        $this->roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id);
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Инициализация прав
     */
    private function initPermissions()
    {
        $this->permissions = Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->identity->id);
    }

    /**
     * Функция возвратит список доступных стран для Пользователя
     * Для пользователя с правами Superadmin будут доступны все страны
     * @return \common\models\Country[]
     */
    public function getCountries()
    {
        if (is_null($this->isSuperadmin)) {
            $this->getIsSuperadmin();
        }

        if (Yii::$app->user->isSuperadmin) {
            return Country::find()
                ->active()
                ->orderBy(['name' => SORT_ASC])
                ->all();
        }

        return Yii::$app->user->identity->countries;
    }


    /**
     * Определяет в онлайне пользователь или нет
     * @return boolean
     */
    public function getIsReady()
    {
        if ($this->isGuest) {
            return false;
        }

        if (is_null($this->isReady)) {
            $this->isReady = $this->identity->isReady;

            if (is_null($this->isReady)) {
                $this->isReady = false;
            }
        }

        return $this->isReady;
    }

    /**
     * Определяет Переводить Оператора в статус "Неготов" после сохранения лида или игнорироваь
     * @return boolean
     */
    public function getCheckUnready()
    {
        if ($this->isGuest) {
            return false;
        }

        if (is_null($this->checkUnready)) {
            $this->checkUnready = $this->identity->checkUnready;

            if (is_null($this->checkUnready)) {
                $this->checkUnready = false;
            }
        }

        return $this->checkUnready;
    }

    public function getUserModel()
    {
        if (is_null($this->userModel)) {
            $this->userModel = UserModel::findOne(['id' => $this->id]);
        }

        return $this->userModel;
    }

    public function getUserProduct()
    {
        return ArrayHelper::getColumn(ProductOperator::findAll(['user_id' => $this->id]), 'product_id');
    }

    public function getPartners()
    {
        if(Yii::$app->user->isSuperadmin){
            return Partner::find()->active()->all();
        }else {
            return yii::$app->user->identity->partners;
        }
    }
}

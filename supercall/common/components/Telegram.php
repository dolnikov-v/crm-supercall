<?php
/**
 * Created by PhpStorm.
 * User: PCWORK
 * Date: 26.10.2017
 * Time: 13:00
 */

namespace common\components;
use yii\base\Component;
use yii\httpclient\Client;
use yii;

/**
 * Class Telegram
 * @package common\components
 */
class Telegram extends Component
{
    const METHOD_GET = 'get';
    const METHOD_POST = 'post';
    const LOGGER_TYPE = 'telegram';
    const LOGGER_ACTION_POST = 'post request';
    const LOGGER_ACTION_GET = 'get request';

    public $urlApi;
    public $token;
    public $urlTelegram;
    public $bot;
    public $name;

    public function setPost($url, $headers, $data, $route = null)
    {
        $params = [
            'method' => self::METHOD_POST,
            'url' => $url,
            'data' => $data,
            'headers' => $headers
        ];

        return $this->sendRequest($params, $route);
    }

    public function setGet($url, $data, $route = null)
    {
        $params = [
            'method' => self::METHOD_POST,
            'url' => $url,
            'data' => $data,
        ];

        return $this->sendRequest($params, $route);
    }

    /**
     * @param $params
     * @return array
     */
    public function sendRequest($params, $route = null){
        $method = isset($params['method']) ? $params['method'] : self::METHOD_POST;

        if(!isset($params['url'])){
            $url = null;
            return [
                'success' => 'false',
                'error' => yii::t('common', 'Не указан url для запроса в API Telegram')
            ];
        }else{
            $url = $params['url'];
        }

        $data = isset($params['data']) ? $params['data'] : [];
        $headers = isset($params['headers']) ? $params['headers'] : [];

        $client = new Client();

        $request = $client->createRequest()
            ->setMethod($method)
            ->setUrl($url);

        if(!empty($data)){
            $request->setData($data);
        }

        if(!empty($headers)){
            $request->setHeaders($headers);
        }

        $response = $request->send();

        if(!$response->isOk){
            $result = [
                'success' => false,
                'error' => $response->content
            ];
        }else{
            $result = [
                'success' => true,
                'content' => $response->content
            ];
        }

        $tags = !empty($headers) ? array_merge($data, $headers) : $data;

        $logger = yii::$app->get('logger');
        $logger->route = $route;
        $logger->action = $method == self::METHOD_POST ? self::LOGGER_ACTION_POST : self::LOGGER_ACTION_GET;
        $logger->type = self::LOGGER_TYPE;
        $logger->tags = $tags;
        $logger->save();

        return $result;
    }

    /**
     * @param $methodName
     * @param bool $slash
     * @return string
     */
    public function buildUrl($methodName, $slash = true)
    {
        return $this->urlApi.$this->token.($slash ? '/' : '').$methodName;
    }

}
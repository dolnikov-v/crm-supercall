<?php

namespace common\components;

use backend\models\TaskQueue;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

class Asterisk2 extends Component
{
    const LOGGER_TYPE = 'Asterisk2';

    public $url;
    public $token;

    /**
     * @return Client
     */
    public function getClient()
    {
        return new Client();
    }

    /**
     * @param TaskQueue $task
     * @return array
     */
    public function getCallRecords($task)
    {
        $result = [
            'success' => false,
            'message' => 'wrong request'
        ];

        $response = $this->getClient()->createRequest()
            ->setMethod('post')
            ->setUrl($this->url)
            ->setData([
                'task_queue_id' => $task->id,
                'unique_ids' => ArrayHelper::map($task->taskQueueOrders, 'uniqueid', 'order_id'),
                'token' => $this->token
            ])
            ->send();

        if($response->isOk){
            if($result =json_decode($response->content, true)) {

                if ($result['success']) {
                    $result = [
                        'success' => true,
                        'message' => $result['message']
                    ];
                } else {
                    $result = [
                        'success' => false,
                        'message' => $result['message']
                    ];
                }
            }else{
                $result = [
                    'success' => false,
                    'message' => $response->content
                ];
            }
        }

        $result['task_queue_id'] = $task->id;

        return $result;
    }
}
<?php
namespace common\components;

use backend\modules\administration\models\BaseLogger;
use common\components\web\User;
use common\models\LiveMessage;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Class ChatApiCom
 * @package common\components
 */
class ChatApiCom extends Component
{
    const LOGGER_TYPE = 'chatApiCom';
    const TYPE_PHONE = 'customer_phone';
    const TYPE_MOBILE = 'customer_mobile';
    const END = '@c.us';

    /** @var string */
    public $urlSent;
    /** @var string */
    public $urlGet;

    /** @var string */
    public $urlStatus;

    /** @var string */
    public $token;

    /** @var  string */
    public $nodeJSserver;

    /** @var  string */
    public $nodeJSToken;

    /** @var  string */
    public $openOrder;

    /** @var  string */
    public $toQueue;

    /** @var  string */
    public $deleteOrderFromQueue;

    /** @var bool */
    public $debug = false;

    /**
     * @param $phone
     * @param $message
     * @param Order $order
     * @param string $beforeMessage
     * @param string $afterMessage
     * @return string
     */
    public function sentMessage($phone, $message, $order, $beforeMessage = '', $afterMessage = '')
    {
        $data = [
            'phone' => $phone,
            'body' => $beforeMessage . ' ' . $message . ' ' . $afterMessage,
            'order_id' => $order->id
        ];

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl($order->country->whatsapp_url . '/message?token=' . $order->country->whatsapp_token)
            ->addHeaders([
                'Authorization' => 'Content-Type: application/json'])
            ->setData($data)
            ->send();

        return $response->content;
    }

    /**
     * @param $number
     * @param $url
     * @param $token
     * @return string
     */
    public function getMessages($number, $url, $token)
    {
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url . '/messages?token=' . $token . '&last=' . $number)
            ->setData([
                'last' => $number
            ])
            ->addHeaders([
                'Authorization' => 'Content-Type: application/json'])
            ->send();

        return $response->content;
    }

    /**
     * @return string
     */
    public function checkStatus()
    {
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($this->urlStatus . $this->token)
            ->addHeaders([
                'Content-Type: application/json'])
            ->send();

        return $response->content;
    }

    /**
     * @return array
     */
    public static function mapCrmFields($type)
    {
        return [
            'order_id' => 'OrderID',
            'order_products' => 'Product',
            'customer_full_name' => 'ClientName',
            $type => 'Phone',
            'char_code' => 'Geo',
            'content' => 'MessageToClient'
        ];
    }

    /**
     * @param $field
     * @return bool|mixed
     */
    public static function mapFieldToPlatinum($field, $type)
    {
        $fields = self::mapCrmFields($type);

        return isset($fields[$field]) ? $fields[$field] : false;
    }

    public function createMessageFromOrder($order)
    {
        $dataFromOrder = [
            'order_id' => $order->id,
            'customer_full_name' => $order->customer_full_name,
            'order_products' => implode(', ', ArrayHelper::getColumn(
                OrderProduct::find()
                    ->joinWith(['product'])
                    ->where(['order_id' => $order->id])
                    ->andWhere(['<>', 'price', 0])
                    ->all(),
                'product.name')),
            'customer_phone' => $order->customer_phone,
            'customer_mobile' => $order->customer_mobile,
            'char_code' => $order->country->char_code,
            'content' => null
        ];


        return $this->combineData($dataFromOrder);
    }

    /**
     * @param $attributes
     * @return array
     */
    public function combineData($attributes)
    {
        //если оба номера указаны в заказе - будет соображать на пару чатов
        $types_phone = [self::TYPE_PHONE, self::TYPE_MOBILE];

        $data = [];

        foreach ($types_phone as $type_phone) {

            foreach ($attributes as $field => $value) {

                $mappedField = self::mapFieldToPlatinum($field, $type_phone);

                if ($mappedField) {
                    $data[$type_phone][$mappedField] = $value;
                }
            }
        }

        sort($data);

        return $data;
    }

    /**
     * @param LiveMessage $message
     * @return array
     */
    public function prepareData($message)
    {
        //формат уже подобран
        if (is_array($message)) {
            return $message;
        }

        $attributes = [
            'char_code' => $message->order->country->char_code,
            'customer_mobile' => $message->order->customer_mobile,
            'customer_phone' => $message->order->customer_phone,
            'customer_full_name' => $message->order->customer_full_name,
            'order_products' => implode(', ', ArrayHelper::getColumn(
                OrderProduct::find()
                    ->joinWith(['product'])
                    ->where(['order_id' => $message->order->id])
                    ->andWhere(['<>', 'price', 0])
                    ->asArray()->all(),
                'product.name'))
        ];

        $compared_attributes = array_merge($message->getAttributes(), $attributes);

        return $this->combineData($compared_attributes);
    }

    /**
     * @param $message
     * @param $order
     * @return array
     */
    public function initChat($message, $order, $toSent = true)
    {
        $data = $this->prepareData($message);
        $tpl = $order->country->whatsapp_message_template;
        $results = [];

        foreach ($data as $k => $params) {
            if (!empty($params['Phone'])) {
                $string_message = $this->applyTpl($tpl, $params);

                if($toSent) {
                    $sent = $this->sentMessage($params['Phone'], $string_message, $order);
                    $results[$k] = [
                        'message' => $string_message,
                        'phone' => $params['Phone'],
                        'result' => Json::decode($sent),
                    ];
                }else{
                    $results[$k] = [
                        'message' => $string_message,
                        'phone' => $params['Phone'],
                    ];
                }
            }
        }

        return $results;
    }

    /**
     * @param $tpl
     * @param array $message
     * @return string
     */
    public function applyTpl($tpl, $message)
    {
        return yii::t('tpl', $tpl, $message);
    }

    /**
     * @param $phone
     * @param $message
     * @param $order
     * @return string
     */
    public function sendToChat($phone, $message, $order)
    {
        return $this->sentMessage($phone, $message, $order);
    }

    /**
     * Отправка свежих сообщений на NodeJS
     * @param $data
     * @return string
     */
    public function sentToNodeJS($url, $data)
    {
        $data = array_merge($data, [
            'token' => $this->nodeJSToken,
            'room' => $data['order_id']
        ]);
        $json_data = json_encode($data, 256);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data))
        );

        $res = curl_exec($ch) . "\n";
        curl_close($ch);

        return $res;
    }

    /**
     * @param $data
     * @return array
     */
    public function setAnswer($data)
    {
        /** @var BaseLogger $logger */
        $logger = yii::$app->get('logger');
        $logger->action = __METHOD__;
        $logger->type = LiveMessage::LOGGER_TYPE;
        $logger->route = __CLASS__;

        if (!isset($data['type'])) {
            $error = 'Required parameter [type] not passed. Allowable types: ' . implode(', ', LiveMessage::getTypesCollection());
        } elseif (!in_array($data['type'], LiveMessage::getTypesCollection())) {
            $error = 'Incorrect parameter [type]. Allowable types: ' . implode(', ', LiveMessage::getTypesCollection());
        } elseif (!isset($data['order_id'])) {
            $error = 'Required parameter [order_id] not passed';
        } elseif (!isset($data['content'])) {
            $error = 'Required parameter [content] not passed';
        } elseif (empty($data['content'])) {
            $error = 'Parameter [content] value  can not be empty';
        }

        if (isset($error)) {
            $result = [
                'success' => false,
                'message' => $error
            ];
        } else {

            $model = new LiveMessage();
            $model->order_id = $data['order_id'];
            $model->type = $data['type'];
            $model->content = $data['content'];
            $model->user_id = null;
            $model->is_new = true;
            $model->queue_number = null;
            $model->phone = $data['phone'];
            $model->chat_id = $data['chat_id'];
            $model->number = $data['number'];
            $model->created_at = time();
            $model->updated_at = time();

            if (!$model->save()) {
                $result = [
                    'success' => false,
                    'message' => $model->getErrors()
                ];

                $logger->tags = $model->getAttributes();
                $logger->error = json_encode($model->getErrors(), 256);
                $logger->save();

            } else {
                $attributes = $model->getAttributes();
                $attributes['created_at'] = yii::$app->formatter->asDate($attributes['created_at'], 'php:d/m/Y H:i:s');
                $attributes['sender'] = $model->order->customer_full_name;

                $result = [
                    'success' => true,
                    'message' => $attributes
                ];
            }
        }

        return $result;
    }
}
<?php

namespace common\components\notification;


use common\components\notification\transport\Transport;
use Yii;
use yii\base\InvalidParamException;

/**
 * Class Queue
 * @package common\components\notification
 */
abstract class Queue
{
    /** @var  Transport */
    public $transport;

    /**
     * @return Transport
     */
    public function getTransport()
    {
        if (is_null($this->transport)) {
            throw new InvalidParamException(Yii::t('common', 'Необходима реализация Transport.'));
        }
    }

    /**
     * @param $message
     * @return bool
     */
    public static abstract function add($message);

    /**
     * @param null $limit
     * @return bool
     */
    public abstract function sendNotifications($limit = null);
}
<?php

namespace common\components\notification\transport;


use common\components\notification\transport\Message;
use yii\base\Component;

/**
 * Class Transport
 * @package common\components\notification\transport
 */
abstract class Transport extends Component
{
    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';

    /**
     * @var Message
     */
    protected $message;

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        if (is_null($this->message)) {
            $this->message = new Message();
        }
        return $this->message;
    }

    /**
     * @param Message $message
     * @return bool
     */
    public abstract function send(Message $message);
}
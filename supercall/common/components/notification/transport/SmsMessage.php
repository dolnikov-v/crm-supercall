<?php

namespace common\components\notification\transport;

/**
 * Class SmsMessage
 * @package common\components\notification\transport
 */
class SmsMessage extends Message
{
    public $country_char_code = null;
}
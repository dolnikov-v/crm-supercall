<?php

namespace common\components\notification\transport;


use yii\base\Component;

/**
 * Class Message
 * @package common\components\notification\transport
 */
class Message extends Component
{
    /**
     * @var string
     */
    public $text;

    public $to;

    public $from;
}
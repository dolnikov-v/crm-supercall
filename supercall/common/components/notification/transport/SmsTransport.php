<?php

namespace common\components\notification\transport;

use common\components\notification\service\sms\SmsService;
use Yii;

/**
 * Class SmsTransport
 * @package common\components\notification\transport
 */
class SmsTransport extends Transport
{
    /**
     * @var SmsMessage
     */
    protected $message;

    public $serviceName = 'SmsService';

    /**
     * @return SmsMessage|Message
     */
    public function getMessage(): Message
    {
        if (is_null($this->message)) {
            $this->message = new SmsMessage();
        }
        return $this->message;
    }

    /**
     * @param SmsMessage|Message $message
     * @return array
     */
    public function send(Message $message)
    {
        $response = [
            'status' => self::STATUS_FAIL,
            'response' => null
        ];
        $smsService = Yii::$app->get($this->serviceName);
        $results = $smsService->sendMessage($message->to, $message->from, $message->text, [
            'char_code' => ($message instanceof SmsMessage) && $message->country_char_code ? $message->country_char_code : null
        ]);

        if ($results['status'] == SmsService::STATUS_SUCCESS) {
            $response['status'] = self::STATUS_SUCCESS;
        }

        $response['response'] = $results;

        return $response;
    }
}
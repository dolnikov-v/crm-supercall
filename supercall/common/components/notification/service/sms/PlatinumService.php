<?php

namespace common\components\notification\service\sms;

/**
 * Class PlatinumService
 * @package app\components
 */
class PlatinumService extends SmsService
{
    /**
     * @var string
     */
    public $login;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $url;
    /**
     * @var string
     */
    public $sender;
    /**
     * @var string
     */
    public $smsSender;
    /**
     * @var string
     */
    public $localSmsSender;
    /**
     * @var string
     */
    public $countrySupportSmsSender;

    const STATUS_FAIL = 'fail';
    const STATUS_SUCCESS = 'success';
    const STATUS_SEND = 'send';

    /**
     * @param string $phoneTo
     * @param string $phoneFrom
     * @param string $message
     * @param array $options
     * @return array
     */
    public function sendMessage($phoneTo, $phoneFrom, $message, $options = [])
    {
        $max_size = 300;

        $result['status'] = self::STATUS_SUCCESS;
        $result['message'] = '';
        $result['responses'] = [];
        $response['code'] = 0;

        unset($module);

        $size = mb_strlen($message, 'utf-8');

        if ($size > $max_size) {
            preg_match_all('#.{' . $max_size . '}#uis', $message, $parts);

            if (isset($parts[0])) {
                foreach ($parts[0] as $num => $part) {
                    if (!empty($part)) {
                        $result['responses'][] = $this->sendToParts($phoneTo, $phoneFrom, $part, $options, $num);
                    }
                }
                //остатки
                $last_part = mb_substr($message, count($parts[0]) * $max_size, 1000);
                $result['responses'][] = $this->sendToParts($phoneTo, $phoneFrom, $last_part, $options, count($parts[0]) + 1);
            }

        } else {
            $result['responses'][] = $this->sendToParts($phoneTo, $phoneFrom, $message, $options, 1);
        }

        //всегда массив с результатами
        return $result;
    }

    //смски по 300 символов
    public function sendToParts($phoneTo, $phoneFrom, $message, $options, $num)
    {
        $customSmsSender = $this->smsSender;

        if (empty($phoneFrom)) {
            $phoneFrom = $customSmsSender;
        }

        //для операторов РФ
        //app\components\notification\transports\SmsTransport.php метод send()
        if (isset($options['local']) && $options['local'] === true) {
            $phoneFrom = $this->localSmsSender;
        }

        $bodyXml = '<?xml version="1.0" encoding="utf-8" ?>
                        <request>
                            <message type="sms">
                                <sender>' . $phoneFrom . '</sender>
                                <text>' . $message . '</text>
                                <abonent phone="' . $phoneTo . '" number_sms="' . $num . '"/>
                            </message>
                            <security>
                                <login value="' . $this->login . '" />
                                <password value="' . $this->password . '" />
                            </security>
                        </request>';


        $bodyXml = strtr($bodyXml, [PHP_EOL => "\n", '  ' => ' ', '  ' => ' ', "\n" => ' ', "\t" => '']);

        $optionsCurl = [
            CURLOPT_HTTPHEADER => ['Content-type: text/xml; charset=utf-8'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CRLF => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $bodyXml,
            CURLOPT_URL => $this->url
        ];


        $response = self::send($optionsCurl);

        $result['to'] = $phoneTo;
        $result['from'] = $phoneFrom;
        $result['request'] = $bodyXml;
        $result['response'] = $response;

        return $result;

    }

    /**
     * @param string $foreign_id
     * @return array
     */
    public function getMessageStatus($foreign_id)
    {
        $bodyXml = '<?xml version="1.0" encoding="utf-8" ?>
                <request>
                    <security>
                        <login value="' . $this->login . '" />
                        <password value="' . $this->password . '" />
                    </security>
                    <get_state>
                        <id_sms>' . $foreign_id . '</id_sms>
                    </get_state>
                </request>';

        $bodyXml = strtr($bodyXml, [PHP_EOL => "\n"]);

        $options = [
            CURLOPT_HTTPHEADER => ['Content-type: text/xml; charset=utf-8'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CRLF => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $bodyXml,
            CURLOPT_URL => $this->url
        ];

        $response = self::send($options);

        //невалидный XML
        if (isset($response->error)) {
            $result['message'] = $response->error;
        } else {
            //Статус отправки
            $result['message'] = $response->state[0];
        }

        return $result;
    }

    /**
     * @param $options []
     * @return $result object
     */
    public function send($options)
    {

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        if (curl_exec($ch) === false) {
            $result = 'error curl:  ' . curl_error($ch);
            curl_close($ch);
        } else {
            $resultXml = curl_exec($ch);
            curl_close($ch);

            $result = (simplexml_load_string($resultXml) === false) ? $resultXml : simplexml_load_string($resultXml);
        }

        return $result;
    }
}

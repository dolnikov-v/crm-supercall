<?php

namespace common\components\notification\service\sms;


use yii\base\Component;

/**
 * Class SmsService
 * @package common\components\notification\service\sms
 */
abstract class SmsService extends Component
{
    /**
     * Урл сервиса
     * @var string
     */
    public $url;

    /**
     * Отправитель сообщения
     * @var string
     */
    public $sender;

    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';

    /**
     * Отправка сообщения
     * должен возвращать массив вида
     * [
     *      'status' => 'success',
     *      'messageId' => '{id}',
     *      'error' => 'text',
     * ]
     * @param string $phoneTo
     * @param string $phoneFrom
     * @param string $message
     * @param array $options
     * @return array
     */
    public abstract function sendMessage($phoneTo, $phoneFrom, $message, $options = []);

    /**
     * Получение информации о статусе сообщения
     * должен возвращать массив вида:
     * [
     *      'status' => 'success',
     *      'message_status' => '{status of message}'
     * ]
     * @param $messageId
     * @return array
     */
    public abstract function getMessageStatus($messageId);
}
<?php

namespace common\components\notification\service\sms;


use yii\httpclient\Client;
use yii\httpclient\CurlTransport;

/**
 * Class ZorroService
 * @package common\components\notification\service\sms
 */
class ZorroService extends SmsService
{
    public $apiKey;

    /**
     * @var string
     */
    public $localSender;

    /**
     * @param string $phoneTo
     * @param string $phoneFrom
     * @param string $message
     * @param array $options
     * @return array
     */
    public function sendMessage($phoneTo, $phoneFrom, $message, $options = [])
    {
        $result = [
            'status' => self::STATUS_FAIL,
            'messageId' => null,
            'error' => null,
            'response' => null,
        ];

        $sender = $this->sender;
        if (isset($options['local']) && $options['local'] == true) {
            $sender = $this->localSender;
        }

        $data = [
            'API_KEY' => $this->apiKey,
            'JSON' => json_encode([
                'target' => 'mailings',
                'method' => 'sendSms',
                'params' => [
                    'recipient' => ltrim(trim($phoneTo), '+'),
                    'sender' => ltrim(trim($phoneFrom), '+') ?: ltrim(trim($sender), '+'),
                    'message' => $message,
                ]
            ], JSON_UNESCAPED_UNICODE),
        ];


        $client = new Client();
        $client->setTransport(CurlTransport::className());
        $response = $client->createRequest()
            ->setUrl($this->url)
            ->setData($data)
            ->setMethod('POST')
            ->setOptions(['sslVerifyPeer' => false])
            ->send();
        if ($response->isOk) {
            $decodedResponse = json_decode($response->content, true);
            $result['response'] = $decodedResponse;
            if (is_array($decodedResponse)) {
                if (isset($decodedResponse['status']) && strtolower($decodedResponse['status']) == 'ok') {
                    $result['status'] = self::STATUS_SUCCESS;
                }
            } else {
                $result['error'] = $response->content;
            }
        } else {
            $result['error'] = $response->content;
        }

        return $result;
    }

    /**
     * @param string $messageId
     * @return array
     */
    public function getMessageStatus($messageId)
    {
        return ['status' => self::STATUS_FAIL, 'message_status' => null];
    }
}
<?php

use common\components\Amqp;
use common\components\Platinum;
use snapsuzun\yii2logger\logstash\async\LoggerAsync;
use snapsuzun\yii2logger\logstash\LogstashInterface;

return [
    'timezone' => 'UTC',
    'bootstrap' => [],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'ratchet' => require(__DIR__ . DIRECTORY_SEPARATOR . 'ratchet.php'),
        'user' => [
            'class' => 'common\components\web\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => [
                'auth/login',
            ],
        ],
        'formatter' => [
            'class' => 'common\components\i18n\Formatter',
            'nullDisplay' => '-',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'sourceLanguage' => 'ru-RU',
                    'sourceMessageTable' => '{{%i18n_source_message}}',
                    'messageTable' => '{{%i18n_message}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => true,
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
        'cache' => [
            'class' => 'yii\caching\MemCache',
            'useMemcached' => true,
            'servers' => [
                [
                    'host' => '127.0.0.1',
                    'port' => 11211,
                    'weight' => 100,
                ],

            ]
        ],
        'logger' => [
            'class' => 'common\components\Logger',
            'table_log_class' => 'common\models\Logger',
            'table_tag_log_class' => 'common\models\LoggerTag'
        ],
        'telegram' => [
            'class' => 'common\components\Telegram',
            'urlApi' => 'https://api.telegram.org/bot',
            'token' => '373344284:AAFE-LtGvtqqCHk5lp70aYXe1rgdW9jSN1A',
            'urlTelegram' => 't.me/Wwcall_bot',
            'bot' => 'Wwcall_bot',
            'name' => '2Wcall'
        ],
        'notifier' => [
            'class' => 'common\components\Notifier',
        ],
        'sms' => [
            'class' => 'common\components\Sms'
        ],

        'SmsService' => [
            'class' => 'common\components\notification\service\sms\PlatinumService',
            'login' => 'zavolokin',
            'password' => 'jsOGVwUE',
            'url' => '185.22.233.163/xml/',
            'smsSender' => '2WTRADE',
            'localSmsSender' => 'Dostavim',
            'countrySupportSmsSender' => require(__DIR__ . '/country_support_sms_sender.php'),
        ],
        'NoAnswerSmsService' => [
            'class' => \common\components\notification\service\sms\ZorroService::className(),
            'url' => 'https://my.zorra.com/api',
            'sender' => 'PHILOMELA',
            'localSender' => 'Dostavim',
            'apiKey' => '2443c776faac6e1a07c76e5e71913432b52e9b7d',

        ],
        'amqp' => [
            'class' => 'common\components\Amqp',
            'host' => 'rabbitmq',
            'port' => 5672,
            'user' => 'admin',
            'password' => 'stNPuLMaoIxdU',
            'vhost' => '/',
        ],
        'queue' => [
            'class' => AMQPQueue::class,
            'host' => 'rabbitmq',
            'port' => 5672,
            'user' => 'admin',
            'password' => 'stNPuLMaoIxdU',
            'queueName' => Amqp::QUEUE_LOGS,
        ],
        'genesys' => [
            'class' => 'common\components\Genesys',
            'db' => [
                'class' => 'yii\db\Connection',
            ],
        ],
        'asterisk' => [
            'class' => 'common\components\Asterisk',
        ],
        'aster' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=78.46.23.228;dbname=asterisk',
            'username' => 'mexico',
            'password' => 'PQdqMQN7fy6Dk',
            'charset' => 'utf8',
        ],
        'ArrayValueIntegerValidator' => [
            'class' => 'common\components\validator\ArrayValueIntegerValidator',
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LfHoVoUAAAAAF5du2bluCMFeNS22pqfls6v5SiN',
            'secret' => '6LfHoVoUAAAAADQt5gPNpjfmh6Wa0NOfwyJfgXA5',
        ],
        Platinum::NAME => [
            'class' => 'common\components\Platinum',
            'debug' => false,
            'params' => [
                'WhatsappApi' => [
                    'url_create' => 'http://174.138.1.171/watsappAPI/sendMessage',
                    'nodeJSserver' => 'http://socket.2wcall.com:',
                    'openOrder' => 'http://socket.2wcall.com/open-order',
                    'toQueue' => 'http://socket.2wcall.com/to-queue',
                    'deleteOrderFromQueue' => 'http://socket.2wcall.com/delete-order-from-queue'
                    //'nodeJSserver' => 'http://socket2wcall.local:53763',
                    //'openOrder' => 'http://socket2wcall.local:53763/open-order',
                    //'toQueue' => 'http://socket2wcall.local:53763/to-queue',
                    //'deleteOrderFromQueue' => 'http://socket2wcall.local:53763/delete-order-from-queue'
                ]
            ]
        ],
        'ChatApiCom' => [
            'class' => 'common\components\ChatApiCom',
            'debug' => false,
            'urlSent' => 'https://eu26.chat-api.com/instance28262/message?token=',
            'urlGet' => 'https://eu26.chat-api.com/instance28262/messages?token=',
            'urlStatus' => 'https://eu26.chat-api.com/instance28262/status?token=',
            'token' => 'lbebez0lv61regio',
            'nodeJSserver' => 'http://socket.2wcall.com:44377',
            'nodeJSToken' => 'YzFhYjk5MjRlY2RhMWJlYWY4YmJhYTFlYjgyMzhiODNlMGVkOGM2Mw==',
            'openOrder' => 'http://socket.2wcall.com:44377/open-order',
            'toQueue' => 'http://socket.2wcall.com:44377/to-queue',
            'deleteOrderFromQueue' => 'http://socket.2wcall.com:44377/delete-order-from-queue'
        ],
        'ZorraPhoneValidator' => [
            'class' => 'common\components\ZorraHLR',
            'url' => 'http://lu.zorra.com/api/v1/lookup',
            'api_key' => '347f88dzfb746h57v4c12992bdt56v5b',
            'batch_size_to_check' => 1,
            'console' => true,
            'test' => false,
            'global_off' => false
        ],
//        'Georgelogger' => [
//            'class' => \snapsuzun\yii2logger\logstash\Logger::class,
//            'host' => '88.208.41.237',
//            'port' => 12201,
//            'logIndex' => 'crm', // Index of created logs in ElasticSearch
//            'user' => 'wtrade', // Username and password for authenticate on logstash server if it need authentication for create log
//            'password' => 'mzI$qq.234Psh',
//            'transportType' => LogstashInterface::TRANSPORT_HTTP, // Maybe TRANSPORT_HTTP or TRANSPORT_SOCKET
//        ],
        'logstash' => [
            'class' => \common\components\Logstash::class,
            'queue' => 'amqp',
            'host' => '88.208.41.237',
            'port' => 12201,
            'logIndex' => 'crm', // Index of created logs in ElasticSearch
            'user' => 'wtrade', // Username and password for authenticate on logstash server if it need authentication for create log
            'password' => 'mzI$qq.234Psh',
            'transportType' => LogstashInterface::TRANSPORT_HTTP, // Maybe TRANSPORT_HTTP or TRANSPORT_SOCKET
        ],

    ],
    //for export order list
    'controllerMap' => [
        'export' => 'phpnt\exportFile\controllers\ExportController'
    ],
    'modules' => [
        'partner' => [
            'class' => 'common\modules\partner\Module',
        ],
        'call' => [
            'class' => 'common\modules\call\Module'
        ],
        'smsnotification' => [
            'class' => 'common\modules\smsnotification\Module',
            'params' => [
                'countrySupportSmsSender' => require(__DIR__ . '/country_support_sms_sender.php'),
                'smsSender' => '2WTRADE',
            ],
        ],
    ]
];

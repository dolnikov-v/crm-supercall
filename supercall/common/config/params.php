<?php
return [
    'urlFrontend' => 'http://2wcall.com',
    'urlBackend' => 'http://panel.2wcall.com',
    'urlApi' => 'http://api.2wcall.com',
    'slogan' => '2WTRADE - Worldwide Express Trade',
    'asterisk_sipml5_disabled_logs' => 'true',
];

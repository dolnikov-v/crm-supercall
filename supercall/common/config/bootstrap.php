<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@console-import', dirname(dirname(__DIR__)) . '/console/import');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@additional-price-condition', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR .'common'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'partner'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'form'.DIRECTORY_SEPARATOR.'conditions');

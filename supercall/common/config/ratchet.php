<?php

return [
    'class' => 'common\components\RatchetService',
    'server_port' => 9095,
    'single_access' => [
        'http://dev.2wcall.com',
        'http://2wcall.com',
        'http://supercall.local'
    ]
];
<?php

namespace common\helpers;

/**
 * Class Utils
 * @package common\helpers
 */
class Utils
{
    /**
     * @param null|string $val
     * @return string
     */
    public static function uid($val = null)
    {
        if ($val) {
            return sha1($val);
        }
        return sha1(uniqid('', true) . mt_rand());
    }
}
<?php
namespace common\helpers\i18n;

use Yii;

/**
 * Class Moment
 * @package common\helpers\i18n
 */
class Moment extends Base
{
    /**
     * @var array
     */
    private static $dateFormats = [
        'ru-RU' => 'DD.MM.YYYY',
        'en-US' => 'MM/DD/YYYY',
        'de-DE' => 'DD.MM.YYYY',
    ];

    /**
     * @var array
     */
    private static $timeFormats = [
        'ru-RU' => 'HH:mm',
        'en-US' => 'HH:mm',
        'de-DE' => 'HH:mm',
    ];

    /**
     * @var array
     */
    private static $fullTimeFormats = [
        'ru-RU' => 'HH:mm:ss',
        'en-US' => 'HH:mm:ss',
        'de-DE' => 'HH:mm:ss',
    ];

    /**
     * @return string
     */
    public static function getDateFormat()
    {
        return self::getFormat(self::$dateFormats);
    }

    /**
     * @return string
     */
    public static function getTimeFormat()
    {
        return self::getFormat(self::$timeFormats);
    }

    /**
     * @return string
     */
    public static function getFullTimeFormat()
    {
        return self::getFormat(self::$fullTimeFormats);
    }

    /**
     * @return string
     */
    public static function getDatetimeFormat()
    {
        return self::getFormat(self::$dateFormats) . ' ' . self::getFormat(self::$timeFormats);
    }

    /**
     * @return string
     */
    public static function getDateFullTimeFormat()
    {
        return self::getFormat(self::$dateFormats) . ' ' . self::getFormat(self::$fullTimeFormats);
    }
}

<?php
namespace common\helpers\i18n;

use Yii;

/**
 * Class Base
 * @package common\helpers\i18n
 */
class Base
{
    /**
     * @param array $formats
     * @return string
     */
    protected static function getFormat($formats)
    {
        $result = '';

        if (isset($formats[Yii::$app->language])) {
            $result = $formats[Yii::$app->language];
        }

        if (!$result) {
            $result = array_shift($formats);
        }

        return $result;
    }
}

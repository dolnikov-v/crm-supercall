<?php
namespace common\helpers\i18n;

use Yii;

/**
 * Class DateTimePicker
 * @package common\helpers\i18n
 */
class DateTimePicker extends Base
{
    /**
     * @var array
     */
    private static $localeFormats = [
        'ru-RU' => 'ru',
        'en-US' => 'en',
        'de-DE' => 'de',
    ];

    /**
     * @return string
     */
    public static function getDateFormat()
    {
        return Moment::getDateFormat();
    }

    /**
     * @return string
     */
    public static function getTimeFormat()
    {
        return Moment::getTimeFormat();
    }

    /**
     * @return string
     */
    public static function getFullTimeFormat()
    {
        return Moment::getFullTimeFormat();
    }

    /**
     * @return string
     */
    public static function getDatetimeFormat()
    {
        return Moment::getDatetimeFormat();
    }

    /**
     * @return string
     */
    public static function getDateFullTimeFormat()
    {
        return Moment::getDateFullTimeFormat();
    }

    /**
     * @return string
     */
    public static function getLocale()
    {
        return self::getFormat(self::$localeFormats);
    }
}

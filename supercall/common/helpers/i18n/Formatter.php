<?php
namespace common\helpers\i18n;

use Yii;

/**
 * Class Formatter
 * @package common\helpers\i18n
 */
class Formatter extends Base
{
    /**
     * @var array
     */
    private static $dateFormats = [
        'ru-RU' => 'php:d.m.Y',
        'en-US' => 'php:m/d/Y',
        'de-DE' => 'php:d.m.Y',
    ];

    /**
     * @var array
     */
    private static $timeFormats = [
        'ru-RU' => 'php:H:i',
        'en-US' => 'php:H:i',
        'de-DE' => 'php:H:i',
    ];

    /**
     * @var array
     */
    private static $fullTimeFormats = [
        'ru-RU' => 'php:H:i:s',
        'en-US' => 'php:H:i:s',
        'de-DE' => 'php:H:i:s',
    ];

    /**
     * @return string
     */
    public static function getDateFormat()
    {
        return self::getFormat(self::$dateFormats);
    }

    /**
     * @return string
     */
    public static function getTimeFormat()
    {
        return self::getFormat(self::$timeFormats);
    }

    /**
     * @return string
     */
    public static function getFullTimeFormat()
    {
        return self::getFormat(self::$fullTimeFormats);
    }

    /**
     * @return string
     */
    public static function getDatetimeFormat()
    {
        $timeFormat = str_replace('php:', '', self::getFormat(self::$timeFormats));

        return self::getFormat(self::$dateFormats) . ' ' . $timeFormat;
    }

    /**
     * @return string
     */
    public static function getDateFullTimeFormat()
    {
        $fullTimeFormat = str_replace('php:', '', self::getFormat(self::$fullTimeFormats));

        return self::getFormat(self::$dateFormats) . ' ' . $fullTimeFormat;
    }
}

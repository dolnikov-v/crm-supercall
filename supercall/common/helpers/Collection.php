<?php
namespace common\helpers;

use Yii;

/**
 * Class Collection
 * @package common\helpers
 */
class Collection
{
    const ACTIVE = 1;
    const NO_ACTIVE = 0;

    /**
     * @return array
     */
    public static function getActiveStatuses()
    {
        return [
            self::ACTIVE => Yii::t('common', 'Да'),
            self::NO_ACTIVE => Yii::t('common', 'Нет')
        ];
    }
}

<?php
namespace common\helpers\fonts;

use yii\base\Object;

/**
 * Class PeIcon
 * @package common\helpers\fonts
 */
class PeIcon extends Object
{
    const ALBUMS = 'pe-7s-albums';
    const BOOKMARKS = 'pe-7s-bookmarks';
    const CLOSE = 'pe-7s-close';
    const HOME = 'pe-7s-home';
    const KEY = 'pe-7s-key';
    const REFRESH = 'pe-7s-refresh';
    const REPEAT = 'pe-7s-repeat';
    const SEARCH = 'pe-7s-search';
    const TOOLS = 'pe-7s-tools';
    const TRASH = 'pe-7s-trash';
    const WORLD = 'pe-7s-world';
    const PARTNER = 'pe-7s-users';
    const WIKI = 'pe-7s-study';
    const STAT = 'pe-7s-graph3';
    const OPERATORS = 'pe-7s-id';
    const BOOK = 'pe-7s-notebook';
    const BOOK_COLLAPSED = 'pe-7s-bookmarks';
}

<?php
namespace common\helpers\fonts;

use yii\base\Object;

/**
 * Class FontAwesome
 * @package common\helpers\fonts
 */
class FontAwesome extends Object
{
    const CAR = 'fa fa-car';
    const PHONE = 'fa fa-phone';
    const PHONE_SQUARE = 'fa fa-phone-square';
    const DATABASE = 'fa fa-database';
    const CLOUD = 'fa fa-cloud';
    const GIFT = 'fa fa-gift';
    const PAPER_PLANE_O = 'fa fa-paper-plane-o';
    const TRUCK = 'fa fa-truck';
    const GLOBE = 'fa fa-globe';
    const LANGUAGE = 'fa fa-language';
    const TASKS = 'fa fa-tasks';
    const WRENCH = 'fa fa-wrench';
    const COG = 'fa fa-cog';
    const FILE_EXCEL_O = 'fa fa-file-excel-o';
    const FILE_WORD_O = '	fa fa-file-word-o';
    const FILE_PDF_O = 'fa fa-file-pdf-o';
    const CODE = 'fa fa-file-code-o';
    const FILE_IMAGE_O = 'fa fa-file-image-o';
    const FILE_ARCHIVE_O = 'fa fa-file-archive-o';
    const FILE_TEXT = 'fa fa-file-text';
    const EYE = 'fa fa-eye';
    const RANDOM = 'fa fa-random';
    const EMAIL = 'fa fa-envelope-o';
    const FILE_AUDIO = 'fa fa-file-audio-o';

}

<?php
namespace common\helpers\grid;

use Yii;
use yii\data\DataProviderInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class DataProvider
 * @package common\helpers\grid
 */
class DataProvider
{
    /**
     * @param DataProviderInterface $dataProvider
     * @param array $options
     * @return string
     */
    public static function renderSummary($dataProvider, $options = ['class' => 'summary'])
    {
        $count = $dataProvider->getCount();

        if ($count <= 0) {
            return '';
        }

        $tag = ArrayHelper::remove($options, 'tag', 'div');

        if (($pagination = $dataProvider->getPagination()) !== false) {
            $totalCount = $dataProvider->getTotalCount();
            $begin = $pagination->getPage() * $pagination->pageSize + 1;
            $end = $begin + $count - 1;

            if ($begin > $end) {
                $begin = $end;
            }

            $page = $pagination->getPage() + 1;
            $pageCount = $pagination->pageCount;

            return Html::tag($tag, Yii::t('yii', 'Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}.', [
                'begin' => $begin,
                'end' => $end,
                'count' => $count,
                'totalCount' => $totalCount,
                'page' => $page,
                'pageCount' => $pageCount,
            ]), $options);
        }

        return '';
    }

    /**
     * @param \yii\data\ActiveDataProvider|\yii\data\ArrayDataProvider $dataProvider
     * @param string $attribute
     * @param string $label
     * @param array $options
     * @return string
     */
    public static function renderSortingTh($dataProvider, $attribute, $label, $options = [])
    {
        $thSorting = 'sorting';

        $sort = Yii::$app->request->get('sort');

        if ($sort == $attribute) {
            $thSorting = 'sorting-desc';
        } elseif ($sort == '-' . $attribute) {
            $thSorting = 'sorting-asc';
        }

        if (isset($options['th-class'])) {
            $thSorting .= ' ' . $options['th-class'];
        }

        $html = '<th class="' . $thSorting . '">';
        $html .= $dataProvider->getSort()->link($attribute, ['label' => $label]);
        $html .= '</th>';

        return $html;
    }
}

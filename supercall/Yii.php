<?php

/**
 * Class Yii
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}

/**
 * Class BaseApplication
 */
abstract class BaseApplication extends yii\base\Application
{

}

/**
 * Class WebApplication
 * @property \common\components\web\User $user
 * @property \common\components\i18n\Formatter $formatter
 * @property \common\components\Notifier $notifier
 *
 * @property \api\components\Partner $partner
 * @property \common\components\Genesys $genesys
 *
 * @property \yii\caching\FileCache $frontendCache
 * @property \yii\web\AssetManager $frontendAssetManager
 * @property \yii\caching\FileCache $apiCache
 * @property \yii\web\AssetManager $apiAssetManager
 *
 * @property common\components\Amqp $amqp
 */
class WebApplication extends yii\web\Application
{

}

/**
 * Class ConsoleApplication
 */
class ConsoleApplication extends yii\console\Application
{

}

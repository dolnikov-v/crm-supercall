<?php
namespace api\models\genesys;

use api\components\db\ActiveRecord;
use api\models\genesys\query\CallingListQuery;
use Yii;

/**
 * Class CallingList
 * @package api\models\genesys
 * @property integer $record_id
 * @property string $contact_info
 * @property integer $call_result
 * @property integer $attempt
 * @property integer $tz_dbid
 * @property string $language
 * @property string $lead_type
 * @property integer $timestamp
 */
class CallingList extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%calling_list}}';
    }

    /**
     * @return CallingListQuery
     */
    public static function find()
    {
        return new CallingListQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [];
    }

}

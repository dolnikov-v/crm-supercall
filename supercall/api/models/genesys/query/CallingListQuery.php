<?php
namespace api\models\genesys\query;

use api\components\db\ActiveQuery;
use api\models\genesys\CallingList;

/**
 * Class CallingListQuery
 * @package api\models\genesys\query
 * @method CallingList one($db = null)
 * @method CallingList[] all($db = null)
 */
class CallingListQuery extends ActiveQuery
{

}

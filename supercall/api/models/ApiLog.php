<?php

namespace api\models;

use common\components\db\ActiveRecord;
use common\modules\order\models\Order;

/**
 * @property integer $id
 * @property string $url
 * @property string $auth
 * @property string $request
 * @property string $response
 * @property integer $created_at
 * @property integer $order_id
 * @property string $operator
 */
class ApiLog extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%api_log}}';
    }

    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
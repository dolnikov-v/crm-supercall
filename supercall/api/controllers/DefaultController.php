<?php
namespace api\controllers;

use api\components\rest\Controller;

/**
 * Class DefaultController
 * @package api\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return array
     */
    public function actionIndex()
    {
        $this->addMessage('Welcome!');

        return $this->success();
    }
}

<?php
use api\assets\base\ThemeAsset;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var string $content */

ThemeAsset::register($this);
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: <?= Yii::$app->name ?></title>
    <link rel="icon" href="<?= Yii::$app->params['urlApi'] ?>/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?= Yii::$app->params['urlApi'] ?>/favicon.ico" type="image/x-icon">
    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>
<?= $content ?>
<div style="display: none" id="document_domain"><?=yii::$app->params['document.domain'];?></div>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>

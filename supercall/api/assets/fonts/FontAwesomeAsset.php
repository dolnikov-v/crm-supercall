<?php
namespace api\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class FontAwesomeAsset
 * @package backend\assets\fonts
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@api/web/resources/fonts/font-awesome';

    public $css = [
        'font-awesome.css',
        'font-awesome.custom.css',
    ];
}

<?php
namespace api\assets\base;

use yii\web\AssetBundle;

/**
 * Class ThemeAsset
 * @package api\assets\base
 */
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@api/web/resources/base/theme';

    public $js = [
        'theme.js',
    ];

    public $css = [
        'theme.css',
    ];

    public $depends = [
        'common\assets\fonts\FontAwesomeAsset',
        'common\assets\fonts\PeIconAsset',
        'common\assets\vendor\ToastrAsset',
    ];

}

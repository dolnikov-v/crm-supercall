<?php
namespace api\modules\order\controllers;

use api\models\UrlRateLimiter;
use common\modules\order\models\Order;
use Yii;
use yii\filters\RateLimiter;
use yii\web\NotFoundHttpException;

/**
 * Class ChangeController
 * @package api\modules\order\controllers
 */
class ChangeController extends \common\modules\order\controllers\ChangeController
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (!in_array($action->id, ['index', 'buffered'])){
            $this->viewPath = '@common/modules/order/views/change';
        }
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    protected function responseAfterOrderSaved(Order $order)
    {
        if(yii::$app->request->get('quality-control') == 1){
            return true;
        }else {
            echo 'Please wait: we are looking for a fresh order for you.';
            exit;
        }
    }

    /**
     * @return string
     */
    public function actionBuffered()
    {
        return $this->render('buffered');
    }

    /**
     * @param $id
     * @param bool $quality_control
     * @return bool|Order
     * @throws NotFoundHttpException
     */
    protected function findOrder($id, $quality_control = false)
    {
        $order = Order::findByUid($id);
        if (!$order) {
            throw new NotFoundHttpException('Order not found.');
        }
        if ($order->final && $quality_control === false){
            echo 'Order is blocked';
            exit;
        }
        return $order;
    }
}

<?php
namespace api\modules\order\controllers;

use common\modules\order\models\Order;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Class IndexController
 * @package api\modules\order\controllers
 */
class IndexController extends \common\modules\order\controllers\IndexController
{
    public function actionIndex($id = '', $operator = '')
    {
        // предварительно данные из ро.класса
        $data = parent::beforeActionIndex();
        $modelSearch = $data['modelSearch'];

        // урл iframe разобрать на части
        $parts = explode('-', $id);
        if (count($parts) === 3) {
            $country = (int) $parts[0];
            $phone = (int) $parts[1];
            // проверка корректности url iframe
            if ((new Order())->generateIframeUidList($country, $phone) != $id) {
                throw new NotFoundHttpException();
            }
    
            $modelSearch->country = $country;
            $modelSearch->customerPhone = $phone;
            
            if (Yii::$app->request->isGet) {
                if ($customerPhone = Yii::$app->request->get('OrderSearch')['customerPhone']) {
                    $customerPhone = preg_replace('/[^0-9]/', '', $customerPhone);
                    $modelSearch->customerPhone = trim($customerPhone);
                }
            }
            
            
            $querySearch = $modelSearch->search();
            // Найдем текущий урл, чтобы работал фильтр
            $url = Url::to();
            $partUrl = explode('?', $url);
            
            // передавать в урл operator
            $operator = Yii::$app->request->get('operator')
                ? Yii::$app->request->get('operator')
                : '';
    
            return $this->render('index', [
                'modelSearch'          => $data['modelSearch'],
                'contentVisible'       => true,
                'showButtons'          => false,
                'dataProvider'         => $querySearch,
                'statuses'             => $data['statuses'],
                'partners'             => $data['partners'],
                'products'             => $data['products'],
                'visiblePartner'       => false,
                'visiblePartnerNumber' => false,
                'filterUrl'            => Url::toRoute([$partUrl[0], 'operator' => $operator]),
            ]);
        }
    
        return false;
    }
    
}

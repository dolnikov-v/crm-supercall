<?php
namespace api\modules\order;

use api\components\base\Module as ApiModule;
use api\models\ApiLog;
use Yii;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * Class Module
 * @package api\modules\order
 */
class Module extends ApiModule
{
    public $controllerNamespace = 'api\modules\order\controllers';
    
    /**
     * @var ApiLog
     */
    protected $log;
    
    /**
     * @return array
     */
    public function behaviors()
    {
        return [];
    }
    
    public function init()
    {
        parent::init();
    
        $request = Yii::$app->request;
        $this->log = new ApiLog();
        // для url необходимо отбросить все доп.параметры
        $partUrl = explode('?', $request->url);
        $this->log->url = $partUrl[0];
        $this->log->operator = Yii::$app->request->get('operator');
        $this->log->auth = $request->getHeaders()->get('Authorization');
        $this->log->request = Json::encode(Yii::$app->request->queryParams);

        $response = Yii::$app->response;
        $respData = [
            'status' => $response->statusCode,
            'headers' => $response->headers,
        ];

        $this->log->response = Json::encode($respData);
        $this->log->ip = $request->userIP;
        
        if (Yii::$app->request->get('id')) {
            // Опеределим order_id
            $this->log->order_id = $this->_getOrderId( Yii::$app->request->get('id') );
        }
        
        if (!$this->log->save()) {
            Yii::error('Не удалось записать в логи действия Оператора при работе с iframe.\n' . VarDumper::dumpAsString([
                    'url'      => $partUrl[0],
                    'operator' => Yii::$app->request->get('operator'),
                    'auth'     => $request->getHeaders()->get('Authorization'),
                    'request'  => Json::encode(Yii::$app->request->queryParams),
                    'ip'       => $request->userIP,
                ]));
        }
        
    }
    
    /**
     * При работе с iframe по GET-параметру определим order_id
     *
     * @param $id
     * @return int
     */
    private function _getOrderId($id)
    {
        $idArray = explode('-', $id);
        
        return (int)$idArray[0];
    }
    
    
}

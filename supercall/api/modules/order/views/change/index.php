<?php

use common\modules\order\assets\ChangeAsset;
use common\assets\widgets\PhoneIcon;
use common\modules\order\models\Order;
use common\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use common\modules\order\controllers\ChangeController;
use common\modules\call\assets\CallHistoryAsset;
use common\modules\order\assets\ScriptsOperatorsAsset;
use yii\helpers\Url;
use common\modules\order\assets\PromoProductsOrderAsset;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var bool $editable */
/** @var array $types */
/** @var array $products */
/** @var array $shippings */
/** @var array $vat_list */
/** @var array $shipping_options */
/** @var string $operator */
/** @var array $packageList */
/** @var string $packageData */
/** @var string $productPrice */
/** @var string $partner_created_at */
/** @var string $partner_created_at */
/** @var array $operators */
/** @var bool $hide_status_buttons */
/** @var bool $disabled_status_buttons */
/** @var string $partnerFormAdditionalPrice */

$this->assetManager->bundles[ChangeAsset::className()] = [
    'order' => $order
];
$this->assetManager->bundles[PromoProductsOrderAsset::className()] = [
    'order' => $order
];

ChangeAsset::register($this);
PhoneIcon::register($this);
CallHistoryAsset::register($this);
PromoProductsOrderAsset::register($this);
//ScriptsOperatorsAsset::register($this);
//Берем первый товар
$productIdForScript = false;
foreach ($order->orderProducts as $i => $product) {
    if (!$product->gift) {
        $productIdForScript = $product->product_id;
        break;
    }
}

$new_live_messages = 0;

if ($order->liveMessages) {
    foreach ($order->liveMessages as $message) {
        if ($message->is_new) {
            $new_live_messages++;
        }
    }
}

$old_live_messages = count($order->liveMessages) - $new_live_messages;
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-order',
    'method' => 'post',
    'action' => '',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validateOnType' => false,
    'validateOnSubmit' => true,
    'options' => [
        'data' => [
            'editable' => $editable,
            'id_param' => $order->generateIframeUid(),
            'id' => $order->id,
            'partner_id' => $order->partner_id,
            'country' => $order->country->char_code,
            'country_id' => $order->country_id,
            'vat_list' => $vat_list,
            'status' => $order->status,
            'extra-price' => ArrayHelper::getValue($order, 'form.extra_price', 0),
            'vat' => ArrayHelper::getValue($order, 'form.vat.value', 0.0),
            'product_id' => $productIdForScript,
            'order_type_id' => $order->type_id
        ],
    ]
]); ?>
<div class="panel">
    <p>
        <?= Yii::t('common', 'Номер заказа') ?>:
        <strong><?= Html::a($order->id, yii::$app->params['urlBackend'] . '/order/change/' . $order->id, ['target' => '_blank']); ?></strong>,
        <?= Yii::t('common', 'Дата создания') ?>:
        <strong><?= Yii::$app->formatter->asDate($order->ordered_at) ?></strong>
    </p>
    <?php
    $similar_count = $order->getSimilar()->count();

    $changeController = new ChangeController('change', 'order');
    echo Tabs::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => Yii::t('common', 'Форма заказа'),
                'content' => $this->render('@common/modules/order/views/change/_tab-order', [
                    'order' => $order,
                    'editable' => $editable,
                    'types' => $types,
                    'products' => $products,
                    'shippings' => $shippings,
                    'countries' => $countries,
                    'is_default_shipping' => $is_default_shipping,
                    'shipping_options' => $shipping_options,
                    'products_price_data' => $products_price_data,
                    'operator' => $operator,
                    'form' => $form,
                    'formAttributes' => $changeController->getFormAttribute($order->country_id, $order->partner_id, $order->form_id),
                    'packageList' => $packageList,
                    'packageData' => $packageData,
                    'productPrice' => $productPrice,
                    'priceShipping' => $priceShipping,
                    'disabled_status_buttons' => $disabled_status_buttons,
                    'masked_phones' => true,
                    'partner_created_at' => $partner_created_at,
                    'time_now_with_timezone' => $time_now_with_timezone,
                    'operators' => $operators,
                    'hide_status_buttons' => $hide_status_buttons,
                    'partnerFormAdditionalPrice' => $partnerFormAdditionalPrice
                ])
            ],
            [
                'label' => Yii::t('common', 'Сообщения ') . Html::tag('span', $similar_count, ['class' => 'label label-success new-messages'], [
                        'old' => $old_live_messages,
                        'new' => $new_live_messages
                    ]),
                'content' => $this->render('@common/modules/order/views/change/_tab-live-message', [
                    'order' => $order,
                    'form' => $form
                ]),
                'linkOptions' => [
                    'id' => 'live-messages-tab'
                ],
                /** Только при наличии переписки */
                'visible' => count($order->liveMessages) > 0
            ],
            [
                'label' => Yii::t('common', 'История заказа'),
                'content' => $this->render('@common/modules/order/views/change/_tab-history', [
                    'order' => $order,
                    'form' => $form
                ])
            ],
            [
                'label' => Yii::t('common', 'Похожие заказы')
                    . ($similar_count > 0
                        ? ' ' . Html::tag('span', $similar_count, ['class' => 'label label-warning'])
                        : ''
                    ),
                'content' => $this->render('@common/modules/order/views/change/_tab-similar', [
                    'order' => $order,
                    'form' => $form
                ])
            ],
        ]
    ]);
    ?>
</div>
<?php ActiveForm::end(); ?>


<?php
    use common\components\grid\ActionColumn;
    use common\components\grid\DateColumn;
    use common\components\grid\GridView;
    use common\components\grid\IdColumn;
    use common\helpers\grid\DataProvider;
    use common\modules\order\models\Order;
    use common\widgets\base\Panel;
    use yii\widgets\LinkPager;
?>
<div class="panel">
<?= Panel::widget([
    'title'          => Yii::t('common', 'Фильтр поиска'),
    'showButtons'    => $showButtons,
    'contentVisible' => $contentVisible,
    'content'        => $this->render('_view-filter', [
        'modelSearch'          => $modelSearch,
        'statuses'             => $statuses,
        'products'             => $products,
        'partners'             => $partners,
        'visiblePartner'       => $visiblePartner,
        'visiblePartnerNumber' => $visiblePartnerNumber,
        'filterUrl'            => $filterUrl,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заказами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'partner.name',
                'label' => Yii::t('common', 'Партнер'),
            ],
            [
                'attribute' => 'foreign_id',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'enableSorting' => false,
                'content' => function ($model) {
                    return Order::getStatusesCollection()[$model->status];
                }
            ],
            [
                'attribute' => 'customer_full_name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'customer_phone',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Открыть'),
                        'url' => function ($model) {
                            return $model->generateIframeUrl(Yii::$app->user->identity);
                        },
                        'can' => function () {
                            return true;
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
</div>
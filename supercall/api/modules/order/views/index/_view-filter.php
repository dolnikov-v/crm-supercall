<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

/** @var \common\models\search\ProductSearch $modelSearch */
/** @var array $statuses */
/** @var array $products */
/** @var array $partners */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute($filterUrl),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    
    <?php if ($visiblePartner): ?>
        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= $form->field($modelSearch, 'partner_id')->select2List($partners, [
                'prompt' => '—'
            ]) ?>
        </div>
    <?php endif; ?>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'product')->select2List($products, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'status')->select2List($statuses, [
            'prompt' => '—'
        ]) ?>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'createdRange')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'pluginOptions' => [
                        'useWithAddon'        => true,
                        'timePicker'          => false,
                        'locale'              => [
                                'format' => 'DD/MM/YYYY',
                        ],
                ],
        ])->label('Дата создания'); ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'updatedRange')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'pluginOptions' => [
                        'useWithAddon'        => true,
                        'timePicker'          => false,
                        'locale'              => [
                                'format' => 'DD/MM/YYYY',
                        ],
                ],
        ])->label('Дата изменения'); ?>
    </div>


    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'id')->textInput()->label('ID заказа') ?>
    </div>
    
    <?php if ($visiblePartnerNumber): ?>
        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= $form->field($modelSearch, 'foreign_id')->textInput() ?>
        </div>
    <?php endif; ?>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'customer_full_name')->textInput() ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'customerPhone')->textInput()->label( Yii::t('common', 'Телефон') ) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'customerMobile')->textInput()->label( Yii::t('common', 'Мобильный') ) ?>
    </div>
</div>
    
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute($filterUrl)); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

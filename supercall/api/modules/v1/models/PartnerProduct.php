<?php
namespace api\modules\v1\models;

use common\models\Country;
use common\models\Product;
use common\modules\partner\models\PartnerProduct as PartnerProductModel;
use common\modules\partner\models\PartnerProductPrice;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class PartnerProduct
 * @package api\modules\v1\models
 */
class PartnerProduct extends Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_UPDATE_PRICE = 'update_price';

    public $partner_id;
    public $partner_product_id;
    public $product_id;
    public $active;
    public $country;
    public $price_data;

    protected $model;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['partner_product_id', 'validatePartnerProductId'],
            ['partner_product_id', 'required', 'on' => self::SCENARIO_CREATE],
            ['product_id', 'validateProductId', 'on' => [self::SCENARIO_CREATE]],
            ['active', 'integer', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
            [['country'], 'required', 'on' => self::SCENARIO_UPDATE_PRICE],
            [['country'], 'exist', 'targetClass' => Country::className(), 'targetAttribute' => 'char_code', 'on' => self::SCENARIO_UPDATE_PRICE],
            [['price_data'], 'validatePriceData', 'on' => self::SCENARIO_UPDATE_PRICE],
        ];
    }

    /**
     * @param string $scenario
     * @param integer $partner_id
     * @param integer|null $partner_product_id
     * @param array $config
     * @throws \Exception
     */
    public function __construct($scenario, $partner_id, $partner_product_id = null, array $config = [])
    {
        $this->scenario = $scenario;
        $this->partner_id = $partner_id;
        if ($partner_product_id){
            $this->model = PartnerProductModel::find()->byPartner($partner_id)->byPartnerProduct($partner_product_id)->one();
            if (!$this->model && in_array($this->scenario, [self::SCENARIO_UPDATE, self::SCENARIO_UPDATE_PRICE])){
                throw new \Exception(Yii::t('common', 'Товар с ID={id} не найден', [
                    'id' => $partner_product_id
                ]));
            }
        }
        else{
            $this->model = new PartnerProductModel();
        }
        parent::__construct($config);
    }

    /**
     * Валидатор внутреннего ида товара
     */
    public function validatePartnerProductId()
    {
        $model = PartnerProductModel::find()->byPartner($this->partner_id)->byPartnerProduct($this->partner_product_id)->one();
        if ($this->scenario == self::SCENARIO_CREATE || ($this->scenario == self::SCENARIO_UPDATE && $model->id != $this->model->id)){
            if ($model){
                $this->addError('partner_product_id', Yii::t('common', 'Товар с ID={id} уже существует', [
                    'id' => $this->partner_product_id
                ]));
            }
        }
    }

    /**
     * Валидатор ида товара
     */
    public function validateProductId()
    {
        $partner_product = PartnerProductModel::find()->byPartner($this->partner_id)->where(['product_id' => $this->product_id])->one();

        if ($partner_product){
            if ($partner_product->id != $this->model->id){
                $this->addError('partner_product_id', Yii::t('common', 'Товар с ID={id} уже сущеaствует', [
                    'id' => $this->product_id
                ]));
            }
        }
        else{
            $product = Product::find()->where(['id' => $this->product_id])->one();
            if (!$product){
                $this->addError('product_id', Yii::t('common', 'Товар с ID={id} не найден', [
                    'id' => $this->product_id
                ]));
            }
        }
    }

    /**
     * Валидатор данных по цене
     */
    public function validatePriceData()
    {
        if (!is_array($this->price_data)) {
            $this->addError('price_data', Yii::t('common', 'price_data должен быть массивом'));
            return;
        }

        if (!($type = ArrayHelper::getValue($this->price_data, 'type'))) {
            $this->addError('price_data', Yii::t('common', 'не указано поле type'));
            return;
        }

        $types = [
            PartnerProductModel::PRICE_DATA_TYPE_TABLE,
            PartnerProductModel::PRICE_DATA_TYPE_STATIC,
        ];

        if (!in_array($type, $types)) {
            $this->addError('price_data', Yii::t(
                'common',
                'type может принимать следующие значения: {types}',
                ['types' => implode(', ', $types)]
            ));
        }

        if ($type == PartnerProductModel::PRICE_DATA_TYPE_TABLE) {
            if (!($table = ArrayHelper::getValue($this->price_data, 'table'))) {
                $this->addError('price_data', Yii::t('common', 'не указана таблица цен (поле table)'));
            } else {
                $i = 0;
                foreach ($table as $q => $p) {
                    if (!is_numeric($q)) {
                        $this->addError('price_data', Yii::t(
                            'common',
                            'в строке {i} table указано неверно количество',
                            ['i' =>$i]
                        ));
                    }
                    if (empty($p)) {
                        $this->addError('price_data', Yii::t(
                            'common',
                            'в строке {i} table не указана цена',
                            ['i' =>$i]
                        ));
                    }
                    $i++;
                }
            }
        }
        if ($type == PartnerProductModel::PRICE_DATA_TYPE_STATIC) {
            if (!($price = ArrayHelper::getValue($this->price_data, 'price'))) {
                $this->addError('price_data', Yii::t('common', 'не указана цена (поле price)'));
            }
        }
    }

    /**
     * @return boolean|integer
     */
    public function save()
    {
        if ($this->validate()) {
            if ($this->scenario == self::SCENARIO_UPDATE_PRICE) {
                $country = Country::find()->where(['char_code' => $this->country])->one();
                $product_price = PartnerProductModel::find()->where([
                    'partner_product_id' => $this->model->id,
                    'country_id' => $country->id,
                ])->one();
                if (!$product_price){
                    $product_price = new PartnerProductModel();
                    $product_price->partner_product_id = $this->model->id;
                    $product_price->country_id = $country->id;
                }
                $product_price->price_data = $this->price_data;
                $product_price->save(false);
            }
            if ($this->scenario == self::SCENARIO_CREATE){
                $this->model = new PartnerProductModel();
                $this->model->partner_id = $this->partner_id;
                $this->model->product_id = $this->product_id;
                $this->model->partner_product_id = $this->partner_product_id;
                $this->model->active = $this->active;
                $this->model->save(false);
            }
            if ($this->scenario == self::SCENARIO_UPDATE){
                $this->model->partner_product_id = $this->partner_product_id;
                $this->model->active = $this->active;
                $this->model->save(false);
            }
            return true;
        }
        return false;
    }
}

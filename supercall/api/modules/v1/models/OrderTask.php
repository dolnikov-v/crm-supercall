<?php
namespace api\modules\v1\models;

use backend\modules\order\models\OrderTask as  OrderTaskModel;
use Yii;

/**
 * Class OrderTask
 * @package api\modules\v1\models
 */
class OrderTask extends OrderTaskModel
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'status', 'curator', 'curator_username'], 'required'],
            [['id', 'status','curator'], 'integer'],
            ['curator_username', 'string'],
            ['status', 'in', 'range' =>  array_keys($this->getTaskStatuses())]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'curator' => 'ID куратора',
            'curator_username' => 'Имя куратора',
            'status' => 'Статус'
        ];
    }
}
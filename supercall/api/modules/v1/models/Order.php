<?php

namespace api\modules\v1\models;

use backend\modules\queue\models\Queue;
use codeonyii\yii2validators\AtLeastValidator;
use common\models\Country;
use common\models\CountryPhoneCode;
use common\models\Language;
use common\models\Product;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order as OrderModel;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderProductLog;
use common\modules\order\models\OrderType;
use common\modules\partner\helpers\PartnerProduct;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerProduct as PartnerProductModel;
use common\modules\partner\models\PartnerForm;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

/**
 * Class Order
 * @package api\modules\v1\models
 */
class Order extends Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_STATUS = 'status';
    const SCENARIO_OPERS = 'opers';
    const SCENARIO_PROD_CALLS = 'prod-calls';

    public $type;
    public $country;
    public $language;
    public $id;
    public $internal_id;
    public $first_name;
    public $last_name;
    public $middle_name;
    public $phone;
    public $mobile;
    public $address;
    public $ip;
    public $source_uri;
    public $customer_components;
    public $approve_components;
    public $ordered_at;
    public $shipping_price;
    public $unicall_order_id; // для дубля заказа
    public $customer_email;
    public $customer_full_name;
    public $partner_id;
    public $order_comment;
    public $queue_type;
    public $call_from;
    public $call_till;
    public $call_date;

    public $products;

    public $uid;

    /** @var OrderModel */
    protected $order;

    /** @var Partner */
    protected $partner;

    /** @var OrderType */
    protected $typeModel;

    /** @var Country */
    protected $countryModel;

    /** @var integer */
    protected $languageId;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country', 'phone', 'products', 'customer_full_name'], 'required', 'on' => self::SCENARIO_CREATE],
            [['id', 'unicall_order_id'], 'integer', 'on' => self::SCENARIO_CREATE],
            [['country', 'language', 'first_name', 'last_name', 'middle_name'], 'string', 'on' => self::SCENARIO_CREATE],
            [['mobile', 'address', 'ip', 'products', 'ordered_at', 'partner_id', 'customer_email'], 'safe', 'on' => self::SCENARIO_CREATE],
            ['type', 'validateType', 'on' => self::SCENARIO_CREATE],
            ['country', 'validateCountry', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_PROD_CALLS]],
            ['language', 'validateLanguage', 'on' => self::SCENARIO_CREATE],
            ['products', 'validateProducts', 'on' => self::SCENARIO_CREATE],
            [['shipping_price'], 'number'],
            [['phone', 'address', 'mobile', 'country', 'ip', 'source_uri'], 'trim'],
            ['id', AtLeastValidator::className(), 'in' => ['id', 'internal_id'], 'on' => [self::SCENARIO_CREATE, self::SCENARIO_STATUS]],
            ['id', 'validateStatusId', 'on' => self::SCENARIO_STATUS],
            ['internal_id', 'validateStatusId', 'on' => self::SCENARIO_STATUS],
            ['id', 'validateStatusId', 'on' => self::SCENARIO_OPERS],
            ['internal_id', 'validateStatusId', 'on' => self::SCENARIO_OPERS],
            [['call_from', 'call_till'], 'integer', 'on' => self::SCENARIO_OPERS],
            ['id', AtLeastValidator::className(), 'in' => ['id', 'internal_id', 'call_from', 'call_till'], 'on' => [self::SCENARIO_OPERS]],
            ['call_date', 'integer', 'on' => self::SCENARIO_PROD_CALLS],
            ['call_date', 'required', 'on' => self::SCENARIO_PROD_CALLS],
            [['source_uri', 'queue_type'], 'safe'],//'url'
            [['customer_components', 'approve_components'], 'safe'],
            [['mobile', 'phone'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type' => Yii::t('common', 'Type'),
            'id' => Yii::t('common', 'ID'),
            'internal_id' => Yii::t('common', 'Id in call center'),
            'country' => Yii::t('common', 'Country'),
            'language' => Yii::t('common', 'Language'),
            'customer_full_name' => Yii::t('common', 'Full name client'),
            'phone' => Yii::t('common', 'Phone'),
            'mobile' => Yii::t('common', 'Mobile'),
            'address' => Yii::t('common', 'Address'),
            'ip' => Yii::t('common', 'ip'),
            'products' => Yii::t('common', 'Products'),
            'created_at' => Yii::t('common', 'Date create'),
            'shipping_price' => Yii::t('common', 'Shipping price'),
            'source_uri' => Yii::t('common', 'Source uri'),
            'call_from' => Yii::t('common', 'Call from'),
            'call_till' => Yii::t('common', 'Call till'),
            'call_date' => Yii::t('common', 'Call date'),
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateType($attribute, $params)
    {
        $type = OrderType::find()
            ->where(['id' => $this->type])
            ->one();

        if ($type) {
            $this->typeModel = $type;
        } else {
            $this->addError($attribute, Yii::t('common', 'Unknown order type "{code}".', [
                'code' => $this->type,
            ]));
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validateId($attribute, $params)
    {
        $this->partner = Yii::$app->partner;

        $query = OrderModel::find()
            ->byPartnerId($this->partner->id)
            ->byForeignId($this->id)
            ->limit(1)
            ->orderBy(['created_at' => SORT_DESC]);

        // Дубли можно слать в систему, если присутствует  параметр "unicall_order_id"
        // $this->unicall_order_id - это ID заказа в unicall
        if ($query->exists()) {
            // Если партнер прислал дубль
            if ($this->unicall_order_id) {
                $order = $query->one();
                // проверяем, что запрос соответствует ID заказу
                if ($order->id == $this->unicall_order_id) {
                    // Разрешаем валидацию
                    return true;
                }
            }

            $this->addError($attribute, Yii::t('common', 'Заказ уже существует.'));
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateLanguage($attribute, $params)
    {
        if (empty($this->language)) {
            return;
        }

        $language = Language::find()
            ->byLocale($this->language)
            ->one();

        if ($language) {
            $this->languageId = $language->id;
        } else {
            $this->addError($attribute, Yii::t('common', 'Unknown language code "{code}".', [
                'code' => $this->language,
            ]));
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateCountry($attribute, $params)
    {
        $country = Country::find()
            ->byCharCode(strtoupper($this->country))
            ->one();

        if ($country) {
            $this->countryModel = $country;
        } else {
            $this->addError($attribute, Yii::t('common', 'Неизвестный код страны "{code}".', [
                'code' => $this->country,
            ]));
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateProducts($attribute, $params)
    {
        if (!is_array($this->products)) {
            $this->addError($attribute, Yii::t('common', 'Необходимо указать товары.'));
        } else {
            foreach ($this->products as $product) {
                if (empty($product['id'])) {
                    $this->addError($attribute, Yii::t('common', 'Необходимо указать номер товара.'));
                }

                if (empty($product['quantity'])) {
                    $this->addError($attribute, Yii::t('common', 'Необходимо указать количество товара.'));
                }
            }
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateStatusId($attribute, $params)
    {
        $this->$attribute = explode(',', $this->$attribute);
        if (!is_array($this->$attribute)) {
            $this->addError($attribute, Yii::t('common', 'Необходимо передать номера заказов.'));
            return;
        }
        if (count($this->id) > 500) {
            $this->addError($attribute, Yii::t('common', 'Можно передать максимум 500 номеров.'));
            return;
        }
    }

    /**
     * @return boolean|integer
     */
    public function create($orderExist = false)
    {
        echo "START CREATE METHOD" . PHP_EOL;
        if ($this->validate()) {
            //если пришли с по API
            if (!$this->partner_id) {
                $partner = Partner::getPartnerByToken(sha1(yii::$app->user->accessType));
                if ($partner) {
                    $this->partner_id = $partner->id;
                } else {
                    $this->addError('common', Yii::t('common', 'Partner is not found'));
                    $this->consoleError('Partner is not found');
                    return false;
                }
            }

            echo "CHECK PRODUCT" . PHP_EOL;
            //Проверям на существование продукта
            foreach ($this->products as $product) {
                echo "PARTNER ID: " . $this->partner_id . " PRODUCT ID: " . $product['id'] . 'country_id: ' . $this->countryModel->id . PHP_EOL;

                //Т.к. пока не понял в чем причина проблемы в PartnerProduct::get - используем просто запрос
                $checkPartnerProduct = PartnerProductModel::find()
                    ->where([
                        'country_id' => $this->countryModel->id,
                        'partner_id' => $this->partner_id,
                        'active' => 1,
                        'partner_product_id' => $product['id']
                    ])
                    ->asArray()
                    ->all();

                if (empty($checkPartnerProduct)) {
                    if (!$this->countryModel->id) {
                        $this->addError('country', Yii::t('common', 'Country ' . $this->country . ' not found.'));
                        $this->consoleError('Country ' . $this->country . ' not found.');
                        return false;
                    }
                    echo "ADD PRODUCT TO PARTNER" . PHP_EOL;
                    $product_id = $product['id'];
                    $price = 0;
                    if ($product['price'] > 0) {
                        $price = $product['price'];// / $product['quantity'];
                    }
                    if (!$product['id'] = $this->addProductFromPay($this->partner_id, $product_id, $this->countryModel->id, $price)) {
                        echo "ERROR ADD PRODUCT" . PHP_EOL;
                        $this->addError('products', Yii::t('common', 'Product not added.'));
                        $this->consoleError('Product not added.');
                        $this->addError('products', Yii::t('common', 'Order Info ID: ' . $this->id . 'partner_id ' . $this->partner_id . ' product_id = ' . $product_id . ' country_id = ' . $this->countryModel->id));
                        $this->consoleError('Order Info ID: ' . $this->id . 'partner_id ' . $this->partner_id . ' product_id = ' . $product_id . ' country_id = ' . $this->countryModel->id);
                        return false;
                    }
                } else {
                    echo "PARTNER HAVE THIS PRODUCT" . PHP_EOL;
                }
            }

            $this->order = new OrderModel();

            echo 'FOREIGN ID ' . $this->id . PHP_EOL;
            //Если заказ не нашли
            if ($orderExist) {
                echo 'ORDER FIND FOREIGN ID ' . $this->id . PHP_EOL;
                //Параметр updateFromAPI исключает валидацию
                $this->order->updateFromAPI = true;
            }

            //Выставляем статус новый (т.е. не обработанный заказ)
            $this->order->status = OrderModel::STATUS_NEW;

            $this->order->beginSave();

            $this->order->partner_id = $this->partner_id;

            $this->order->country_id = $this->countryModel->id;
            $this->order->language_id = $this->languageId ? $this->languageId : $this->countryModel->language_id;
            $this->order->type_id = $this->type;

            $this->order->foreign_id = $this->id;

            if ($this->customer_components) {
                $this->order->customer_components = $this->customer_components;
            }

            if ($this->approve_components) {
                $this->order->approve_components = $this->approve_components;
            }

            if ($this->shipping_price) {
                $this->order->shipping_price = $this->shipping_price;
            }

            if (empty($this->customer_full_name)) {
                $this->order->customer_full_name = $this->customer_first_name . ' ' . $this->customer_last_name . ' ' . $this->customer_midlle_name;
            } else {
                $this->order->customer_full_name = $this->customer_full_name;
            }

            $this->order->customer_phone = CountryPhoneCode::checkPhone($this->phone, $this->countryModel->id);//$this->phone;
            $this->order->customer_mobile = CountryPhoneCode::checkPhone($this->mobile, $this->countryModel->id);//$this->mobile;

            $this->order->customer_address = !empty($this->order->customer_address) ? $this->order->customer_address : $this->address;
            $this->order->customer_ip = $this->ip;
            $this->order->source_uri = $this->source_uri;
            $this->order->order_comment = $this->order_comment;
            $this->order->comment = $this->order_comment;
            //$this->order->queue_type = $this->queue_type;
            $this->order->customer_email = $this->customer_email;

            if (isset($this->ordered_at) && $this->ordered_at > 0) {
                $this->order->partner_created_at = $this->ordered_at;
            }

            if ($this->ordered_at) {
                if (!is_numeric($this->ordered_at)) {
                    $this->ordered_at = (new \DateTime(
                        $this->ordered_at,
                        new \DateTimeZone($this->countryModel->timezone->timezone_id))
                    )->getTimestamp();
                } else {
                    $date = new \DateTime();
                    $date->setTimestamp($this->ordered_at);
                    $date->setTimezone(new \DateTimeZone($this->countryModel->timezone->timezone_id));
                    $this->ordered_at = $date->getTimestamp();
                }
                $this->order->ordered_at = $this->ordered_at;
            }

            $this->order->init_price = 0;
            $this->order->final_price = 0;

            //Проверка существование очереди на данный тип заказа
            $queueId = false;
            /*if (!Queue::checkExistsQueueByType($this->order->country_id, $this->order->type_id, $this->order->partner_id)) {
                //$orderTypeName = OrderType::findOne(['id' => OrderType::getMappedType($this->queue_type)]);
                //$this->addError('common', Yii::t('common', 'No queue was found for the order type "'.$this->queue_type.'". Please create primary queue with order type "'.$orderTypeName->name.'"'));
                $this->consoleError('QUEUE FOR TYPE: ' . OrderType::getMappedType($this->queue_type) . ' NOT FOUND');
                //return false;

                if (!$queueId = Queue::createDefaultQueue($this->order->partner_id, $this->order->country_id, $this->order->type_id)) {
                    $orderTypeName = OrderType::findOne(['id' => OrderType::getMappedType($this->queue_type)]);
                    $this->addError('common', Yii::t('common', 'No queue was found for the order type "' . $this->queue_type . '". Please create primary queue with order type "' . $orderTypeName->name . '"'));
                    return false;
                }
            }*/

            if ($this->prepareProducts() && $this->order->save()) {
                $this->saveProducts();
                $this->order->addToQueue($queueId);

                //$this->order->addOrderIdToQueue();
                $this->order->completeSave();

                return $this->order->id;
            } else {
                $this->consoleError(' PREPARE PRODUCT ' . print_r($this->getErrors(), 1));
            }

            $this->order->cancelSave($this->getErrors());
        }

        return false;
    }

    public function consoleError($message = '')
    {
        echo 'ERROR: ' . $message . PHP_EOL;
    }

    /**
     * Добавляем новый продукт в таблицу продукты и добавляем новый продукт в продукты партнера
     * По умолчанию всем новым товарам делаем фиксированную ценну
     * @param int $partner_id
     * @param int $product_id
     * @param int $county_id
     * @return bool|int
     */
    public function addProductFromPay($partner_id, $product_id, $county_id, $price = 0)
    {
        //Вычисляем партнера по ID =)
        $partner = Partner::find()->where(['id' => $partner_id])->asArray()->one();
        if (!$partner || !$partner['api_key'])
            return false;

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($partner['url_product'])
            ->addHeaders(['Authorization' => 'Bearer ' . base64_encode($partner['api_key'])])
            ->send();

        if (!$response->isOk) {
            $this->addError('products', Yii::t('common', 'Error getting list product. Status code: ' . $response->statusCode));
            return false;
        }

        if (isset($response->data['data'][$product_id])) {
            $product = new Product();

            $findProduct = Product::findOne(['name' => $response->data['data'][$product_id]['name']]);

            $productId = false;

            if (!$findProduct && !isset($findProduct->id)) {
                $transaction = $product->getDb()->beginTransaction();
                $product->load(['name' => $response->data['data'][$product_id]['name']], '');
                if (!$product->save()) {
                    $this->addError('products', Yii::t('common', 'Error add product.' . json_encode($product->getErrors())));
                    $transaction->rollBack();
                    return false;
                } else {
                    $transaction->commit();
                    $productId = $product->id;
                }
            } else {
                $productId = $findProduct->id;
            }

            if ($productId) {
                $partnerProduct = new \common\modules\partner\models\PartnerProduct();
                $partnerProduct->product_id = $productId;//$product->id;
                $partnerProduct->partner_id = $partner_id;
                $partnerProduct->partner_product_id = $product_id;
                $partnerProduct->country_id = $county_id;
                $partnerProduct->price_data = ['type' => 2, 'price' => $price];
                $transaction = $partnerProduct->getDb()->beginTransaction();
                if ($partnerProduct->save()) {
                    $transaction->commit();
                    Yii::$app->cache->delete(PartnerProduct::getCacheKey($partner_id));
                    return $partnerProduct->id;
                } else {
                    $this->addError('products', Yii::t('common', 'Error add partner product.' . json_encode($product->getErrors())));
                    $transaction->rollBack();
                    return false;
                }
            } else {
                $this->addError('products', Yii::t('common', 'Error save product.' . json_encode($product->getErrors())));
                return false;
            }
        }
    }

    /**
     * @return boolean
     */
    protected function prepareProducts()
    {
        $products = [];

        // Если Цена доставки передана в API параметром "shipping_price"
        if ($this->shipping_price) {
            $this->order->final_price += $this->shipping_price;
        }

        $productPackage = [];
        foreach ($this->products as $product) {
            //$productId = PartnerProduct::get($this->partner_id, $product['id']);
            $checkProduct = PartnerProductModel::find()
                ->where([
                    'partner_id' => $this->partner_id,
                    'partner_product_id' => $product['id'],
                    'country_id' => $this->countryModel->id,
                    'active' => 1
                ])
                ->asArray()
                ->one();

            if (!empty($checkProduct)) {
                $productId = $checkProduct['product_id'];
                $orderProduct = new OrderProduct();
                $orderProduct->product_id = $productId;
                $orderProduct->price = ArrayHelper::getValue($product, 'price', null);
                // @todo подумать, как для фикс.цены перестроить логику
                // $orderProduct->cost = $orderProduct->price ? $orderProduct->price * $product['quantity'] : null;
                $orderProduct->fixed_price = $orderProduct->price ? true : false;
                $orderProduct->quantity = $product['quantity'];
                $orderProduct->gift = $product['gift'];

                //акции не обязательна
                $productPackage[$productId]['paid_amount'] = 0;
                $productPackage[$productId]['free_amount'] = 0;

                //считаем кол-во платный и бесплатных продуктов
                //$productPackage[$productId]['product_name'] = $product['name'];
                if (!$product['gift']) {
                    $productPackage[$productId]['paid_amount'] = $product['quantity'];
                } else {
                    $productPackage[$productId]['free_amount'] = $product['quantity'];
                }

                $this->order->init_price += $orderProduct->price * $orderProduct->quantity;
                $this->order->final_price += $orderProduct->price * $orderProduct->quantity;

                if (!$orderProduct->validate(array_diff($orderProduct->attributes(), ['order_id']))) {
                    $this->consoleError('validate product');
                    $this->addErrors($orderProduct->getErrors());
                    return false;
                }

            } else {
                $this->addError('products', Yii::t('common', 'Product not found or unavailable.'));
                return false;
            }

            $products[] = $orderProduct;
        }

        if (!isset($this->countryModel->id) && is_string($this->country)) {
            $country = Country::findOne(['char_code' => $this->country]);
        } else {
            $country = $this->countryModel;
        }

        /*if(isset($country->id)){
            foreach ($productPackage as $product_id => $product) {
                if($product['free_amount'] > 0) {
                    $checkPackage = Package::find()
                        ->where(['partner_id' => $this->partner_id])
                        ->andWhere(['country_id' => $country->id])
                        ->andWhere(['product_id' => $product_id])
                        ->andWhere(['paid_amount' => $product['paid_amount']])
                        ->andWhere(['free_amount' => $product['free_amount']])
                        ->andWhere(['active' => 1])
                        ->one();

                    if (!$checkPackage) {
                        //Проверяем подходит ли под акции АдКомбо
                        if (!Package::checkPackageAdCombo($product['paid_amount'], $product['free_amount'])) {
                            $this->addErrors(['No find package. Paid amount: ' . $product['paid_amount'] . ' Free amount: ' . $product['free_amount']]);
                            return false;
                        } else {
                            $packageModel = new Package();
                            if (!$packageModel->create($this->partner_id, $country->id, $product_id, $product['paid_amount'], $product['free_amount'])) {
                                $this->addErrors(['Error create package. PartnerId:' . $this->partner_id . ' CountryId:' . $country->id . ' CharCode: ' . $this->country . ' ProductId: ' . $product_id . ' PaidAmount: ' . $product['paid_amount'] . ' FreeAmount: ' . $product['free_amount']]);
                                return false;
                            }
                        }
                    }
                }
            }
        }*/

        $this->order->populateRelation('orderProducts', $products);
        return true;
    }

    protected function saveProducts()
    {
        if (is_numeric($this->order->id)) {
            foreach ($this->order->orderProducts as $product) {
                $product->order_id = $this->order->id;
                $product->save(false);
            }
            return true;
        } else {
            $this->addError('Order not have ID');
        }

        return false;
    }

    /**
     * @return boolean|array
     */
    public function status()
    {
        $response = false;

        if ($this->validate()) {
            $query = OrderModel::find()
                ->with([
                    'orderProducts',
                ])
                ->joinWith([
                    'country',
                ]);

            if (!empty($this->internal_id)) {
                $query->byInternalIds($this->internal_id);
            } else {
                $query->byForeignIds($this->id);
            }


            $orders = $query->byPartnerId(Yii::$app->partner->id)
                ->orderBy(['foreign_id' => SORT_ASC])
                ->all();

            $orders = array_filter($orders, function (OrderModel $order) {
                return !$order->buffered;
            });

            if ($orders) {
                $response = [];

                foreach ($orders as $order) {
                    // партнерам нужен только последний комментарий
                    $comment = $this->getComment($order);

                    $info = [
                        'id' => $order->foreign_id,
                        'internal_id' => $order->id,
                        'country' => ArrayHelper::getValue($order, 'country.char_code'),
                        'status' => $order->status,
                        'sub_status' => $order->sub_status,
                        'email' => $order->customer_email,
                        'phone' => array_filter(array_merge([$order->customer_phone, $order->customer_mobile], $order->phones)),
                        'order_components' => array_filter($order->getGeneralModel()->attributes, function ($key) {
                            return !in_array($key, PartnerForm::configurableAttributes());
                        }, ARRAY_FILTER_USE_KEY),
                        'address' => $order->getAddressModel()->attributes,
                        'products' => [],
                        'final_price' => $order->final_price,
                        'shipping_price' => $order->shipping_price,
                        'comments' => $comment
                    ];
                    if ($order->status == OrderModel::STATUS_RECALL) {
                        $info['recall_date'] = $order->recall_date;
                    }

                    foreach ($order->orderProducts as $orderProduct) {
                        $partnerProductId = PartnerProduct::getForeign(Yii::$app->partner->id, $orderProduct->product_id, null);

                        $info['products'][] = [
                            'id' => $partnerProductId,
                            'cost' => $orderProduct->cost,
                            'quantity' => $orderProduct->quantity,
                        ];
                    }

                    $response[$order->foreign_id] = $info;
                }
            } else {
                $this->addError('common', Yii::t('common', 'Заказы не найдены.'));
            }
        }

        return $response;
    }

    /**
     * Возвращает посл.коммент, необходимый партнеру
     *
     * @param OrderModel $order
     * @return mixed|null|string
     */
    private function getComment(OrderModel $order)
    {
        $comment = null;
        $sub_comment = null;

        // Если есть субстатус, то необходимо учесть Причину
        // также особый коммент при автоотклоне

        // Все субстатусы с комментариями
        $sub_comment_merge = $order::getRejectReasonCollection() + $order::getTrashReasonCollection();
        if ($order->sub_status && array_key_exists($order->sub_status, $sub_comment_merge)) {

            // Если автом.отклонение, то "Не удалось связаться с покупателем"
            if ($order->sub_status == OrderModel::SUB_STATUS_REJECT_REASON_AUTO) {
                $comment = Yii::t('api', 'Не удалось связаться с покупателем');
            } // иначе, просто укажем причину в скобках
            else {
                $sub_comment = ' (' . $sub_comment_merge[$order->sub_status] . ')';
            }
        } // если не автоотклон, то проверить на наличие коммента
        else {
            if (!empty($order->comments)) {
                $comments = $order->comments;
                $comment = end($comments)['text'];
            }
        }

        return $comment . $sub_comment;
    }

    /**
     * Добавить ID заказа в очереди
     * Проверить заказ на соответствие первичной очереди
     */
    private function addToQueue()
    {
        $order = $this->order;
        // поле "primary_components" является jsonb
        $queryQueue = Yii::$app->db->createCommand('SELECT * FROM "queue"
                      WHERE primary_components @>:QUERY
                      ORDER BY priority ASC
                ')
            ->bindValue(':QUERY', Json::encode([
                'primary_country' => (string)$order->country_id,
                'primary_partner' => (string)$order->partner_id,
                'primary_type' => (string)$order->type_id,
            ]))
            ->queryOne();

        if ($queryQueue) {
            if (OrderModel::updateByParams(['last_queue_id' => $queryQueue['id']], ['id' => $order->id])) {
                $amqp = Yii::$app->amqp;
                $connection = new AMQPStreamConnection($amqp->host, $amqp->port, $amqp->user, $amqp->password, $amqp->vhost);
                $channel = $connection->channel();

                $msg = new AMQPMessage($order->id, [
                    'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
                ]);
                $channel->basic_publish($msg, Queue::EXCHANGE, $queryQueue['name']);
            }
        }
    }

    /**
     * @return boolean|array
     */
    public function opers()
    {
        $response = false;

        if ($this->validate()) {

            $query = OrderModel::find()
                ->select(new Expression("array_to_string(array_agg(" . OrderModel::tableName() . ".id),',') as ids"));

            if (!empty($this->internal_id)) {
                $query->byInternalIds($this->internal_id);
            }

            if (!empty($this->id)) {
                $query->byForeignIds($this->id);
            }

            if (!empty($this->call_from) && (empty($this->call_till) && empty($this->id) && empty($this->internal_id))) {
                $this->call_till = strtotime('tomorrow', $this->call_from) - 1;
                $this->call_from = strtotime('today', $this->call_from);
            }

            if (!empty($this->call_till) && (empty($this->call_from) && empty($this->id) && empty($this->internal_id))) {
                $this->call_from = strtotime('today', $this->call_till);
                $this->call_till = strtotime('tomorrow', $this->call_till) - 1;
            }

            if (!empty($this->call_from)) {
                $query->byDateFrom($this->call_from);
            }

            if (!empty($this->call_till)) {
                $query->byDateTill($this->call_till);
            }

            $orders = $query->byPartnerId(Yii::$app->partner->id)
                ->asArray()
                ->one();

            if (!empty($orders['ids'])) {
                $ordersIds = explode(',', $orders['ids']);


                $response = [];

                $opersHistory = CallHistory::find()
                    ->select([CallHistory::tableName() . '.order_id',
                            OrderModel::tableName() . '.foreign_id',
                            CallHistory::tableName() . '.user_id',
                            new Expression("count(" . CallHistory::tableName() . ".id) as calls")]
                    )
                    ->leftJoin(OrderModel::tableName(), OrderModel::tableName() . '.id = ' . CallHistory::tableName() . '.order_id')
                    ->where(['in', CallHistory::tableName() . '.order_id', $ordersIds])
                    ->groupBy([CallHistory::tableName() . '.order_id', OrderModel::tableName() . '.foreign_id', CallHistory::tableName() . '.user_id'])
                    ->asArray()
                    ->all();


                foreach ($opersHistory as $row) {
                    $response[$row['foreign_id']][$row['user_id']] = $row['calls'];
                }
            } else {
                $this->addError('common', Yii::t('common', 'Заказы не найдены.'));
            }
        }

        return $response;
    }

    public function prodCalls()
    {
        $response = false;
        if ($this->validate()) {
            $response = [];
            $begin_day = strtotime('today', $this->call_date);
            $end_day = strtotime('tomorrow', $this->call_date) - 1;

            $callHistoryTable = CallHistory::tableName();
            $orderTable = OrderModel::tableName();
            $partnerProductTable = PartnerProductModel::tableName();
            $orderProductLogTable = OrderProductLog::tableName();
            $partnerId = Yii::$app->partner->id;
            $countryTable = Country::tableName();

            $operatorHistory = CallHistory::find()
                ->select([
                    $countryTable . '.char_code as country',
                    $partnerProductTable . '.partner_product_id',
                    new Expression("count(DISTINCT $callHistoryTable.id) as calls")
                ])
                ->innerJoin($orderTable, "$orderTable.id = $callHistoryTable.order_id AND $orderTable.partner_id = $partnerId")
                ->innerJoin($orderProductLogTable, "$orderProductLogTable.order_id = $orderTable.id AND $orderProductLogTable.action = 'create'")
                ->leftJoin($partnerProductTable, "$partnerProductTable.product_id = $orderProductLogTable.product_id AND $orderTable.partner_id = $partnerId AND $partnerProductTable.active = 1")
                ->leftJoin($countryTable, "$countryTable.id = $orderTable.country_id")
                ->where(['between', $callHistoryTable . '.start_time', $begin_day, $end_day])
                ->andWhere($partnerProductTable . '.partner_product_id NOTNULL');

            if (!empty($this->country)) {
                $countryId = Country::findOne(['char_code' => $this->country])->id;
                $operatorHistory->andWhere([$orderTable . '.country_id' => $countryId]);
            }

            $operatorHistory->groupBy([$countryTable . '.char_code', $orderProductLogTable . '.product_id', $partnerProductTable . '.partner_product_id']);
            $results = $operatorHistory->asArray()->all();

            foreach ($results as $row) {
                $response[$row['country']][$row['partner_product_id']] = $row['calls'];
            }

        }
        return $response;
    }
}
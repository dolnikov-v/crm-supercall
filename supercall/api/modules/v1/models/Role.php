<?php
namespace api\modules\v1\models;

use yii\base\Model;
use yii\db\Query;

class Role extends Model
{
    const SOURCE = '2wcall';

    /**
     * @return array
     */
    public static function getRolesList()
    {
        return (new Query())->from('{{%auth_item}}')->where(['type' => 1])->all();
    }
}
<?php

namespace api\modules\v1\models;

use common\models\Country;
use common\models\UserCountry;
use common\models\UserPartner;
use common\models\UserReady;
use common\models\User as UserModel;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use yii\base\Model;
use yii\db\Expression;
use backend\modules\access\models\AuthAssignment;
use Yii;
use yii\helpers\ArrayHelper;

class User extends Model
{
    /**
     * @param $date_from
     * @param $date_to
     * @return array|\yii\db\ActiveRecord[]
     * todo переделать на нормльный вид дополнительные параметры для поиска
     */
    public static function getOpersActivityData($date_from, $date_to, $country_id = false, $user_login = false)
    {
        $result = UserModel::find()
            ->select([
                'user_id' => UserModel::tableName() . '.id',
                'login' => UserModel::tableName() . '.username',
                'date' => new Expression("date_trunc('day', to_timestamp(start_time))"),
                'work_time' => new Expression("COALESCE(sum(diff), 0)"),
                'country' => Country::tableName() . '.char_code',
                'first_action_utc' => new Expression("date_trunc('second', to_timestamp(COALESCE(min(start_time), 0)))"),
                'last_action_utc' => new Expression("date_trunc('second', to_timestamp(COALESCE(max(stop_time), 0)))"),
                'user_created' => new Expression("date_trunc('second', to_timestamp(COALESCE(max(" . UserModel::tableName() . '.created_at' . "), 0)))")
            ])
            ->leftJoin(UserReady::tableName(), UserReady::tableName() . '.user_id=' . UserModel::tableName() . '.id')
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id=' . UserModel::tableName() . '.id')
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName() . '.user_id=' . UserModel::tableName() . '.id')
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . UserCountry::tableName() . '.country_id')
            ->leftJoin(UserPartner::tableName(), UserPartner::tableName() . '.user_id=' . UserModel::tableName() . '.id')
            ->where(['or', ['between', UserReady::tableName() . '.start_time',
                    new Expression(strtotime($date_from . "00:00:00")),
                    new Expression(strtotime($date_to . "23:59:59"))],
                    ['between', UserModel::tableName() . '.created_at',
                        new Expression(strtotime($date_from . "00:00:00")),
                        new Expression(strtotime($date_to . "23:59:59"))]
                ]
            )
            /*->where(['>=', UserReady::tableName() . '.start_time', new Expression(strtotime($date_from . "00:00:00"))])
            ->andWhere(['<=', UserReady::tableName() . '.stop_time', new Expression(strtotime($date_to . "23:59:59"))])
            ->orWhere()*/
            //->andWhere(['is not', UserReady::tableName() . '.user_id', null])
            //->andWhere([AuthAssignment::tableName() . '.item_name' => UserModel::ROLE_OPERATOR])
            ->andWhere([UserPartner::tableName() . '.partner_id' => 1]);//TODO исправить на опеределение партнера по токену

        if ($country_id) {
            $result->andWhere([Country::tableName() . '.id' => $country_id]);
        }
        if($user_login){
            $result->andWhere([UserModel::tableName() . '.username' => $user_login]);
        }

        return $result->groupBy("user.id, username, char_code, date_trunc('day', to_timestamp(start_time))")
            ->asArray()
            ->all();
    }

    /**
     * @param $date_from
     * @param $date_to
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCallStat($date_from, $date_to)
    {
        return CallHistory::find()
            ->select([
                'user_id' => CallHistory::tableName() . '.user_id',
                'login' => UserModel::tableName() . '.username',
                'date' => new Expression("date_trunc('day', to_timestamp(start_time))"),
                'amount_calls' => new Expression("count(*)")
            ])
            ->leftJoin(UserModel::tableName(), UserModel::tableName() . '.id=' . CallHistory::tableName() . '.user_id')
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id=' . UserModel::tableName() . '.id')
            ->leftJoin(Order::tableName(), Order::tableName() . '.id=' . CallHistory::tableName() . '.order_id')
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . Order::tableName() . '.country_id')
            ->where(['>=', CallHistory::tableName() . '.start_time', new Expression(strtotime($date_from . "00:00:00"))])
            ->andWhere(['<=', CallHistory::tableName() . '.end_time', new Expression(strtotime($date_to . "23:59:59"))])
            ->andWhere(['is not', CallHistory::tableName() . '.user_id', null])
            ->andWhere([AuthAssignment::tableName() . '.item_name' => UserModel::ROLE_OPERATOR])
            ->andWhere([UserModel::tableName() . '.status' => UserModel::STATUS_DEFAULT])
            ->groupBy("date_trunc('day', to_timestamp(start_time)), call_history.user_id, username")
            ->asArray()
            ->all();
    }

    /**
     * @return UserCountry[]
     */
    public static function getUsers()
    {
        $users = UserModel::find()
            ->select([
                'user_id' => UserModel::tableName() . '.id',
                'user_login' => UserModel::tableName() . '.username',
                'user_name' => "CONCAT(" . UserModel::tableName() . ".first_name" . ",' '," . UserModel::tableName() . ".last_name)",
                'user_role' => AuthAssignment::tableName() . '.item_name'
            ])
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id=' . UserModel::tableName() . '.id')
            ->leftJoin(UserPartner::tableName(), UserPartner::tableName() . '.user_id=' . UserModel::tableName() . '.id')
            ->andWhere([UserModel::tableName() . '.status' => UserModel::STATUS_DEFAULT])
            ->andWhere([UserPartner::tableName() . '.partner_id' => 1])//TODO исправить на опеределение партнера по токену
            ->asArray()
            ->all();

        foreach ($users as $key => $user) {
            $countries = UserCountry::find()
                ->select(Country::tableName() . '.char_code')
                ->leftJoin(Country::tableName(), Country::tableName() . '.id = ' . UserCountry::tableName() . '.country_id')
                ->where([UserCountry::tableName() . '.user_id' => $user['user_id']])->asArray()->all();
            $users[$key]['countries'] = ArrayHelper::getColumn($countries, 'char_code');
        }

        return $users;
    }
}
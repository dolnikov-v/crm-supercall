<?php

namespace api\modules\v1\controllers;


use api\components\filters\VerbFilter;
use api\components\rest\Controller;
use api\models\ApiLog;
use backend\models\TaskQueue;
use common\components\web\User;
use common\models\IncomingCall;
use common\modules\call\models\CallHistory;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class AsteriskController
 * @package api\modules\v1\controllers
 */
class AsteriskController extends Controller
{
    /**
     * @var array
     */
    protected $accessTypes = [
        'call-status' => [
            User::ACCESS_TYPE_ASTERISK,
        ],
        'incoming-call' => [
            User::ACCESS_TYPE_ASTERISK,
        ],
        'update-data-zip-task-queue' => [
            User::ACCESS_TYPE_ASTERISK,
        ],
        'update-call-history' => [
            User::ACCESS_TYPE_ASTERISK,
        ],
        'callback' => [
            User::ACCESS_TYPE_ASTERISK,
        ],
        'save-log' => [
            User::ACCESS_TYPE_ASTERISK,
        ]
    ];

    /**
     * Восстановление/Обновление call_history с сервера Asterisk
     * в post получаем массив вида
     *
     * При первой попытке - находим запись по uniqueid и обновляем ей данные
     * При второй попытке - находим запись по phone+sip - и если она единственная  - то обновляем, если нет - то мы не можем определить нужную call_history
     *
     * Array
     * (
     * [success] => 1
     * [message] => Array
     * (
     * [0] => Array
     * (
     * [calldate] => 2018-10-12 07:23:52
     * [utc_call_date] => 2018-10-12 04:23:52
     * [start_time] => 1539318232
     * [end_time] => 1539318289
     * [billsec] => 31
     * [sip] => 2wcall1295
     * [disposition] => ANSWERED
     * [customer_phone] => 94768944646
     * [recordingfile] => 2018-10-12/07/94768944646-2wcall1295-07:23.wav
     * [uniqueid] => 1539318232.2307682
     * )
     * )
     * )
     */
    public function actionUpdateCallHistory()
    {
        $post = yii::$app->request->post();

        $this->log = new ApiLog();
        $this->log->url = $this->getRoute();
        $this->log->request = Json::encode($post);

        if (!isset($post['message'])) {
            $this->log->response = 'Incorrect data from asterisk';
        } else {
            //для POSTMAN
            //$post['message'] = Json::decode($post['message']);

            //попробуем найти по Uniqueid
            //сформируем массив uniqueid из данных записей Asterisk
            $uniqueids = ArrayHelper::getColumn($post['message'], 'uniqueid');

            //сделать это размумней так - т.е. получить список найденных call_history по uniqueid - чем каждый раз в цикле тревожить базу
            $found_from_uniqueid = ArrayHelper::map(CallHistory::find()->byUniqueid($uniqueids)->all(), 'id', 'uniqueid');

            foreach ($post['message'] as $call_history_asterisk) {
                $this->log->request = Json::encode($call_history_asterisk);

                //по uniqueid нашли call_history
                if (in_array($call_history_asterisk['uniqueid'], $found_from_uniqueid)) {
                    //необходимо найти по номеру и user_sip - но очень важно присутсвие group_id
                    $byPhoneSip = CallHistory::findOne(['id' => $found_from_uniqueid[$call_history_asterisk['uniqueid']]]);
                    //если у call_history есть recordingfile - нас он не интересует
                    /** @var CallHistory $byPhoneSip */
                    if (!$byPhoneSip->recordingfile) {
                        $byPhoneSip->start_time = $call_history_asterisk['start_time'];
                        $byPhoneSip->end_time = $call_history_asterisk['end_time'];
                        $byPhoneSip->duration = $call_history_asterisk['duration'];
                        $byPhoneSip->updated_at = yii::$app->formatter->asTimestamp(time());
                        $byPhoneSip->recordingfile = $call_history_asterisk['recordingfile'];
                        $byPhoneSip->disposition = $call_history_asterisk['disposition'];
                        $byPhoneSip->billsec = $call_history_asterisk['billsec'];

                        //обновили данные
                        if (!$byPhoneSip->save(false)) {
                            $this->log->response = 'Error update: ' . Json::encode($byPhoneSip->getErrors());
                            $this->log->save();
                        }
                    }
                } else {
                    //если у call_history нет group id - связка sip + phone может существовать несколько раз -
                    // и тут не возможно определить заказ - только какая-либо магия с вычислением времени - но мы не маги
                    $byPhoneSips = CallHistory::find()->byPhone($call_history_asterisk['customer_phone'])->bySip($call_history_asterisk['sip'])->all();

                    //тупик - завязать запись разговора - практические не возможно
                    if (count($byPhoneSips) > 1) {
                        $this->log->response = 'Has many: ' . Json::encode(ArrayHelper::getColumn($byPhoneSips, 'id'));
                        $this->log->save();
                    } elseif (count($byPhoneSips) == 1) {
                        //если у call_history есть recordingfile - нас он не интересует
                        /** @var CallHistory $byPhoneSip */
                        if (!$byPhoneSips[0]->recordingfile) {
                            // но если такая связка сушествует только один раз - то это и есть нужный заказ


                            $byPhoneSip[0]->start_time = $call_history_asterisk['start_time'];
                            $byPhoneSip[0]->end_time = $call_history_asterisk['end_time'];
                            $byPhoneSip[0]->duration = $call_history_asterisk['duration'];
                            $byPhoneSip[0]->updated_at = yii::$app->formatter->asTimestamp(time());
                            $byPhoneSip[0]->recordingfile = $call_history_asterisk['recordingfile'];
                            $byPhoneSip[0]->disposition = $call_history_asterisk['disposition'];
                            $byPhoneSip[0]->billsec = $call_history_asterisk['billsec'];

                            //обновили данные
                            if (!$byPhoneSip->save(false)) {
                                $this->log->response = 'Error update: ' . Json::encode($byPhoneSip->getErrors());
                                $this->log->save();
                            }
                        }
                    }

                    $this->log->response = 'Not found by uniqueid: ' . $call_history_asterisk['uniqueid'];
                    $this->log->save();
                }

            }

        }
    }

    /**
     * Обновление данных о звонке из Астера
     * @return array
     */
    public function actionCallStatus()
    {
        $request = \Yii::$app->request->get();

        if (!isset($request['phone']) || !isset($request['user_sip'])) {
            $this->addMessage('Phone or user_sip is not found.');

            $this->log = new ApiLog();
            $this->log->url = $this->getRoute();
            $this->log->request = Json::encode($request);
            $this->log->response = 'Phone or user_sip is not found.';
            $this->log->save();

            return $this->fail();
        }

        $prePhone = explode('|', $request['phone']);
        $phone = $prePhone[0];
        $callHistoryId = false;
        if (isset($prePhone[1])) {
            $callHistoryId = $prePhone[1];
        }

        $callHistoryModel = CallHistory::find();

        if ($callHistoryId) {
            $callHistoryModel->andWhere(['id' => $callHistoryId]);
        } else {
            $callHistoryModel->where([
                'user_sip' => $request['user_sip'],
                'phone' => $request['phone'],
                'uniqueid' => null
            ])
                ->orderBy(['id' => SORT_ASC]);
        }

        $callHistoryModel->one();

        if (!$callHistoryModel) {
            $this->addMessage('Call history with this parameters not found.');

            $this->log = new ApiLog();
            $this->log->url = $this->getRoute();
            $this->log->request = Json::encode($request);
            $this->log->response = 'Call history with this parameters not found.';
            $this->log->save();

            return $this->fail();
        }

        if ($callHistoryModel->load($request, '')) {
            if (!$callHistoryModel->save()) {
                $this->addMessage($callHistoryModel->getFirstErrorAsString());

                $this->log = new ApiLog();
                $this->log->url = $this->getRoute();
                $this->log->request = Json::encode($request);
                $this->log->order_id = $callHistoryModel->order_id;
                $this->log->response = $callHistoryModel->getFirstErrorAsString();

                return $this->fail();
            } else {
                return $this->success();
            }
        }

        return $this->fail();
    }

    /**
     * @return array
     */
    public function actionIncomingCall()
    {
        $request = \Yii::$app->request->get();

        if (!isset($request['phone'])) {
            $this->addMessage('Phone is not found.');
            return $this->fail();
        }

        $model = null;
        if (isset($request['user_sip']) && !empty($request['user_sip'])) {
            $model = IncomingCall::find()->where(['phone' => $request['phone']])->andWhere([
                'or',
                ['is', 'user_sip', null],
                ['user_sip' => '']
            ])->orderBy(['created_at' => SORT_DESC])->one();
        }

        if (!$model) {
            $model = new IncomingCall();
        }

        if ($model->load($request, '')) {
            if (!$model->save()) {
                $this->addMessage($model->getFirstErrorAsString());
            } else {
                return $this->success();
            }
        }


        return $this->fail();
    }

    /**
     * Получение информации с сервера астериска по таблице task_queue - при подготовке архивов с записями разговоров
     * @return array
     */
    public function actionUpdateDataZipTaskQueue()
    {
        $post = yii::$app->request->post();

        $task_queue = TaskQueue::findOne(['id' => $post['task_queue_id']]);

        if ($task_queue) {
            if (!in_array($post['status'], [TaskQueue::STATUS_FAIL, TaskQueue::STATUS_COMPLETED])) {
                $error = 'incorrect status task_queue : ' . $post['status'];
            } else {
                $task_queue->status = $post['status'];
                $task_queue->updated_at = time();

                switch ($post['status']) {
                    case TaskQueue::STATUS_FAIL:
                        $task_queue->error = $post['error'];
                        break;
                    case  TaskQueue::STATUS_COMPLETED:
                        $task_queue->path = $post['path'];
                        break;
                }

                if (!$task_queue->save(false)) {
                    $error = Json::encode($task_queue);
                }
            }

        } else {
            $this->addData(['message' => 'Task #' . $post['task_queue_id'] . ' not found']);
            return $this->fail();
        }

        if (isset($error)) {
            $this->log = new ApiLog();
            $this->log->url = Yii::$app->request->url;
            $this->log->operator = Yii::$app->user->id;
            $this->log->request = Json::encode($post);
            $this->log->response = Json::encode(['error' => $error]);
            $this->log->save();

            $this->addData(['error' => $error]);

            return $this->fail();
        }

        return $this->success();
    }

    /**
     * сохранять логи в очереди раббита, для последующей передачи в Kibana
     */
    public function actionSaveLog()
    {
        $log = yii::$app->request->post('log');
        $logQueue = yii::$app->request->post('queue');

        Yii::$app->amqp->saveLog($log, $logQueue);
    }
}
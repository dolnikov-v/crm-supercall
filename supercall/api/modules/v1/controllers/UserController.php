<?php
namespace api\modules\v1\controllers;

use api\components\rest\Controller;
use common\components\web\User;
use api\modules\v1\models\User as UserApi;
use common\models\Country;
use Yii;

class UserController extends Controller
{
    public $log;
    /**
     * @var array
     */
    protected $accessTypes = [
        'get-opers-activity-data' => [
            User::ACCESS_TYPE_PAY
        ],
        'get-call-stat' => [
            User::ACCESS_TYPE_PAY
        ],
        'get-users' => [
            User::ACCESS_TYPE_PAY
        ],
    ];

    /**
     * @return array
     */
    public function actionGetOpersActivityData()
    {
        $post = Yii::$app->request->get();

        if (empty($post)) {
            $post = Yii::$app->request->post();
        }

        $date_from = isset($post['date_from']) ? $post['date_from'] : null;
        $date_to = isset($post['date_to']) ? $post['date_to'] : null;
        //$country = Country::findOne(['char_code' => strtolower($post['char_code'])]);

        if (empty($date_from) || empty($date_to) || is_null($date_from) || is_null($date_to)) {
            $this->addMessage('Incorrect date period');
            return $this->successFalse();
        /*} elseif (!isset($country->id)) {
            $this->addMessage('Country not fined');
            return $this->successFalse();*/
        } else {
            $country = false;
            if(!empty($post['country'])){
                $country = Country::findOne(['char_code' => strtoupper($post['country'])]);

                if(!$country){
                    $this->addMessage('Country not fined');
                    return $this->successFalse();
                }

                $country = $country->id;
            }

            $user_login = empty($post['login']) ? false : $post['login'];


            $response = UserApi::getOpersActivityData($date_from, $date_to, $country, $user_login);//$country->id

            $clearData = [];

            foreach ($response as $data) {
                $data['id'] = $data['user_id'];
                unset($data['user_id']);
                $clearData[] = $data;
            }

            $this->addForPayData($clearData, 'response');
            unset($this->responseData['data']);

            return $this->successTrue();
        }
    }

    /**
     * @return array
     */
    public function actionGetCallStat()
    {
        $post = Yii::$app->request->post();

        $date_from = isset($post['date_from']) ? $post['date_from'] : null;
        $date_to = isset($post['date_to']) ? $post['date_to'] : null;

        if (empty($date_from) || empty($date_to) || is_null($date_from) || is_null($date_to)) {
            $this->addMessage('Incorrect date period');

            return $this->successFalse();
        } else {
            $response = UserApi::getCallStat($date_from, $date_to);
            $this->addForPayData($response, 'response');
            unset($this->responseData['data']);

            return $this->successTrue();
        }
    }

    /**
     * @return array
     */
    public function actionGetUsers()
    {

        $response = UserApi::getUsers();
        $this->addForPayData(['users' => $response], 'response');
        unset($this->responseData['data']);

        return $this->successTrue();

    }

    /**
     * @param mixed $value
     */
    protected function addForPayData($value, $key = 'data')
    {
        $this->responseData[$key] = $value;
    }
}
<?php
namespace api\modules\v1\controllers;

use api\components\filters\VerbFilter;
use api\components\rest\Controller;
use common\components\web\User;
use common\models\Timezone;
use common\modules\order\models\asterisk\Records;
use Yii;

/**
 * Class RecordController
 * @package api\modules\v1\controllers
 */
class RecordController extends Controller
{
    /**
     * @var array
     */
    protected $accessTypes = [
        'create' => [
            User::ACCESS_TYPE_ASTERISK,
        ],
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create'      => ['get'],
                ],
            ],
        ];
    }

    /**
     * Запись данных разговора в бд Unicall
     * @param $path  - путь к файлу, включает телефон и путь
     * @return array
     */
    public function actionCreate($path)
    {
        // хак от лишних символов в конце пути
        $path = str_replace(',b', '', $path);

        $parts = explode('/', $path);
        $pathPhone = explode('-', $parts[2]);
        // телефон находится в подстроке
        $phone = $pathPhone[0];
        $phone = preg_replace('/[^0-9]/', '', $phone);
        
        if (!empty($phone) && !empty($path)) {

            $currentTime = (new \DateTime())
                ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
                ->getTimestamp();

            // проверить на наличие записи в бд со статусом SAVED=FALSE
            $is_record = Records::find()->where(['phone' => $phone, 'path' => $path]);

            if (!$is_record->exists()) {
                $record = new Records();
                $record->phone = $phone;
                $record->path = $path;
                $record->datetime = $currentTime;

                if ($record->save()) {
                    $this->setDataValue('id', $record->id);
                    $this->setDataValue('phone', $record->phone);

                    return $this->success();
                } else {
                    $this->addMessagesByModel($record);
                }
            } else
                $this->addMessage('Запись уже добавлена в таблицу `records`');

        }

        return $this->fail();
    }

}

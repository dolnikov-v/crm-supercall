<?php
namespace api\modules\v1\controllers;

use api\components\filters\VerbFilter;
use api\components\rest\Controller;
use common\components\web\User;
use common\modules\order\models\Order as OrderModel;
use Yii;

/**
 * Class SmsController
 * @package api\modules\v1\controllers
 */
class SmsController extends Controller
{
    /**
     * @var array
     */
    protected $accessTypes = [
        'send' => [
            User::ACCESS_TYPE_GENESYS,
        ],
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'send'      => ['post'],
                ],
            ],
        ];
    }
    
    /**
     * Отправка сообщения клиенту
     * @return array
     */
    public function actionSend()
    {
        $id = (int) Yii::$app->request->post('id');
        $phone = Yii::$app->request->post('phone');
        
        $phone = preg_replace('/[^0-9]/', '', $phone);
        $model = OrderModel::findOne(['foreign_id' => $id]);
        
        if ($model) {
            // складываем в очередь
            $queueName = 'sms';
            Yii::$app->amqp->declareExchange('router', $type = 'direct', $passive = false, $durable = true, $auto_delete = false);
            // здесь публикуем сообщение
            Yii::$app->amqp->declareQueue($queueName);
            Yii::$app->amqp->bindQueueExchanger($queueName, 'router', $routingKey = $queueName);
            Yii::$app->amqp->publish_message($phone, 'router', $routingKey = $queueName);
            
            // @todo Интеграция с sms-агрегатором или ...
            // передать значение $phone
            return $this->success();
        }
        else {
            $this->addMessage('Заказ не найден.');
        }
        return $this->fail();
        
    }


}

<?php

namespace api\modules\v1\controllers;

use api\components\filters\VerbFilter;
use api\components\rest\Controller;
use api\modules\v1\models\Role;
use common\components\web\User;
use yii\helpers\ArrayHelper;

/**
 * Class RoleController
 * @package api\modules\v1\controllers
 */
class RoleController extends Controller
{
    /**
     * @var array
     */
    protected $accessTypes = [
        'get-list' => [
            User::ACCESS_TYPE_PAY,
            User::ACCESS_TYPE_DINA
        ],
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-list' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionGetList()
    {
        $roles = Role::getRolesList();

        if (!$roles) {
            $listRoles = [];
        } else {
            $listRoles = ArrayHelper::map($roles, 'name', 'description');
        }

        return json_encode([
            'source' => Role::SOURCE,
            'roles' => $listRoles
        ], JSON_UNESCAPED_UNICODE);
    }
}
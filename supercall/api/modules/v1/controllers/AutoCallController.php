<?php
namespace api\modules\v1\controllers;

use api\components\filters\VerbFilter;
use api\components\rest\Controller;

use common\components\Amqp;
use common\models\AutoCallLog;
use common\models\LeadSQS;
use common\models\QueueOrder;
use common\models\Timezone;
use common\modules\order\models\Order;
use common\modules\call\models\CallHistory;
use common\models\AutoCall;
use common\models\User;

use Yii;
use yii\di\Instance;
use yii\helpers\Json;

/**
 * Class AaotCallController
 * @package api\modules\v1\controllers
 */
class AutoCallController extends Controller
{
    /**
     * @var Amqp
     */
    public $amqp;
    /**
     * @var array
     */
    protected $accessTypes = [
        'call-status' => [
            \common\components\web\User::ACCESS_TYPE_PARTNER,
            \common\components\web\User::ACCESS_TYPE_ASTERISK
        ],
    ];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->amqp = Instance::ensure($this->amqp, Amqp::class);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'call-status' => ['get', 'post'],
                ],
            ],
        ];
    }

    /**
     * @param $phone
     * @param $call_status
     * @param string $cdr_id
     * @return array
     */
    public function actionCallStatus($phone, $call_status, $cdr_id = '')
    {
        /* пример входящих данных
        {
        //"http://asterweb.2wcall.com/index.php?r=webhook/auto-call/callback&phone=$phone&exten=$exten&action=lost"

          "phone":"79133869812","call_status":"ANSWERED","action":"search-order"
        }
        {
          "phone":"79133869812","call_status":"ANSWERED","cdr_id":"1556167123.23149116","user_sip":"79133869812","recordingfile":"2019-04-25/07/79133869812-8117313-79133869812-07:38.wav",
         "duration":"7","billsec":"0","action":"hangup"
        }

        {
          "user_sip":"2wcall860","queue":"cc_1","phone":"79133869812","call_status":"ANSWERED","action":"open-form"
        }
         */

        $requestData = Yii::$app->request->get();
        $this->addData(['requestData' => $requestData]);

        //с aterweb.2wcall.com modules/webhook/controllers/AutoCallController.php проходят все звонки с астера, но для call_history они идут на отдельное апи, но сюда они тоже попадают.
        $autoCall = AutoCall::find()->where(['order_phone' => $phone])->orderBy(['id' => SORT_DESC])->one();

        if (!$autoCall) {
            if ($requestData['action'] != AutoCall::ACTION_LOST) {
                $autoCall = AutoCallLog::find()->where(['order_phone' => $phone])->andWhere(['is', 'user_sip', null])->orderBy(['id' => SORT_DESC])->one();
            } else {
                $autoCall = AutoCallLog::find()->where(['order_phone' => $phone])->andWhere(['call_status' => CallHistory::DISPOSITION_ANSWERED])->orderBy(['id' => SORT_DESC])->one();
            }
        }

        if (!is_null($autoCall)) {
            /** @var AutoCallLog $autoCall */
            $autoCallLog = AutoCallLog::findOne(['id' => $autoCall->id]);

            //open-form
            if ($requestData['action'] == AutoCall::ACTION_OPEN_FORM) {
                $autoCall->user_sip = $requestData['user_sip'];
                if ($autoCall->call_status != CallHistory::DISPOSITION_LOST) {
                    $autoCall->call_status = $requestData['call_status'];
                }
                $autoCallLog->user_sip = $requestData['user_sip'];
                if ($autoCallLog->call_status != CallHistory::DISPOSITION_LOST) {
                    $autoCallLog->call_status = $requestData['call_status'];
                }

                $status = implode(',', [User::STATUS_BLOCKED, User::STATUS_DELETED]);
                $sql = 'SELECT * FROM "user" WHERE sip::JSON->>\'sip_login\' = \'' . $requestData['user_sip'] . '\' AND status NOT IN (' . $status . ')';
                $user = User::findBySql($sql)->one();

                //оператор, который ответил - перехвачен
                //проверить - есть ли блокировка для этого заказа и этого оператора - если нет - то создать
                //т.к. далее отработает стандартный get-order-form
                $blocked = QueueOrder::findOne(['user_id' => $user->id, 'order_id' => $autoCall->order_id]);

                if (!$blocked) {
                    $queueOrder = new QueueOrder([
                        'user_id' => $user->id,
                        'order_id' => $autoCall->order_id,
                        'blocked_to' => time() + 1800
                    ]);

                    $queueOrder->save();
                }

                /** @var CallHistory $callHistory */
                $callHistory = CallHistory::find()
                    ->where(['order_id' => $autoCall->order_id])
                    ->andWhere([
                        'or',
                        ['user_sip' => $autoCall->order_phone],
                        ['is', 'user_sip', null]
                    ])
                    ->orderBy(['id' => SORT_DESC])
                    ->one();

                if ($callHistory) {
                    $callHistory->user_sip = $requestData['user_sip'];
                    $callHistory->user_id = $user->id;
                    $callHistory->disposition = $requestData['call_status'];
                    $callHistory->save(false);
                }

                // Конец разговора
            } else if ($requestData['action'] == AutoCall::ACTION_HANGUP) {


                $callHistory = CallHistory::find()
                    ->where(['order_id' => $autoCall->order_id])
                    ->andWhere(['is', 'recordingfile', null])
                    ->one();

                $this->addData(['$requestData' => $requestData]);
                $this->addData(['$callHistory' => is_null($callHistory) ? 'null' : $callHistory->id]);

                if ($callHistory) {
                    $callHistory->user_sip = $requestData['user_sip']; // == phone
                    $callHistory->uniqueid = $requestData['cdr_id'];

                    if ($callHistory->disposition != CallHistory::DISPOSITION_LOST) {
                        $callHistory->disposition = $requestData['call_status'];
                    }

                    $callHistory->recordingfile = $requestData['recordingfile'];
                    $callHistory->end_time = $callHistory->start_time + $requestData['duration'];
                    $callHistory->duration = $requestData['duration'];
                    $callHistory->billsec = $requestData['billsec'];

                    $callHistory->save(false);
                }

                $autoCallLog->cdr_id = $callHistory->uniqueid;
                $autoCallLog->save(false);

                //удалить запись из autoCall - остаётся толькв autoCallLog
                if ($autoCall instanceof AutoCall) {
                    $autoCall->delete();
                } else {
                    $realAutoCall = AutoCall::findOne(['order_id' => $autoCall->order->id]);

                    if ($realAutoCall) {
                        $realAutoCall->delete();
                    }
                }

                //Не нашлось оператора, который ответил клиенту
            } elseif ($requestData['action'] == AutoCall::ACTION_LOST) {
                if ($autoCall) {
                    $autoCall->call_status = CallHistory::DISPOSITION_LOST;
                }
                if ($autoCallLog) {
                    $autoCallLog->call_status = CallHistory::DISPOSITION_LOST;
                }

                $callHistory = CallHistory::find()
                    ->where(['order_id' => $autoCall->order_id])
                    ->orderBy(['id' => SORT_DESC])
                    ->one();

                if ($callHistory) {
                    $callHistory->disposition = CallHistory::DISPOSITION_LOST;
                    $callHistory->save(false);
                }
            }

            if ($autoCall->save()) {
                $autoCallLog->save();

                if (isset($requestData['call_status'])) {
                    if ($requestData['call_status'] == AutoCall::DISPOSITION_FAILED) {
                        $this->trashOrder($autoCall->order_id);
                    } elseif ($requestData['call_status'] != AutoCall::DISPOSITION_ANSWERED) {
                        $this->uncall($autoCall->order_id);
                    }
                }
                $result = $this->success();
            } else {
                $this->addData(['error' => 'autoCall not saved: ' . Json::encode($autoCall->getErrors())]);
                $result = $this->fail();
            }
        } else {
            $result = $this->fail();
        }

        //обновление данных callhistory
        if (isset($requestData['action'], $requestData['phone'], $requestData['user_sip'])) {
            if ($requestData['action'] == AutoCall::ACTION_HANGUP && $requestData['phone'] == $requestData['user_sip']) {
                /** @var CallHistory $callHistoryForUpdate */
                $callHistoryForUpdateQuery = CallHistory::find()->where([
                        'phone' => $requestData['phone'],
                    ]
                );

                if ($callHistoryForUpdateQuery->exists()) {
                    $callHistoryForUpdate = $callHistoryForUpdateQuery->orderBy(['id' => SORT_DESC])->one();

                    $callHistoryForUpdate->uniqueid = $requestData['cdr_id'];
                    $callHistoryForUpdate->duration = $requestData['duration'];
                    $callHistoryForUpdate->billsec = $requestData['billsec'];
                    $callHistoryForUpdate->recordingfile = $requestData['recordingfile'];

                    if ($autoCall->call_status != CallHistory::DISPOSITION_LOST) {
                        $autoCall->call_status = $requestData['call_status'];
                    }

                    $autoCall->cdr_id = $requestData['cdr_id'];

                    $autoCall->save(false);

                    $autoCallLog = AutoCallLog::findOne(['id' => $autoCall->id]);

                    if ($autoCallLog) {
                        if ($autoCallLog->call_status != CallHistory::DISPOSITION_LOST) {
                            $autoCallLog->call_status = $requestData['call_status'];
                        }
                        $autoCallLog->cdr_id = $requestData['cdr_id'];
                        $autoCallLog->save(false);
                    }

                    if ($callHistoryForUpdate->save()) {
                        $this->addData(['update billsec success ' => $callHistoryForUpdateQuery->createCommand()->getRawSql()]);
                        $result = $this->success();
                    } else {
                        $this->addData(['update billsec error ' => $callHistoryForUpdate->getErrors()]);
                        $result = $this->fail();
                    }
                } else {
                    $this->addData(['update billsec not found by ' => $callHistoryForUpdateQuery->createCommand()->getRawSql()]);
                    $result = $this->fail();
                }

            }
        }

        return $result;
    }

    /**
     * @param $id
     * @throws \yii\db\Exception
     */
    public function uncall($id)
    {
        $order = Order::findOne($id);
        $order->status = $order::STATUS_FAIL;

        $LeadSQS = new LeadSQS();
        $order->attempts++;

        if ($order->useStrategyByQueue()) {
            $params = $order->dirtyAttributes;
            if (key_exists('status', $params)) {
                $order->saveLogsByOrder();
            }

            if ($order->saveWithoutLogs()) {
                if ($LeadSQS->sendLeadStatus($order)) {
                    if ($order->id) {
                        Yii::$app->db->createCommand()->update('{{%order}}', ['sent_to_sqs' => true], ['id' => $order->id])->execute();
                    }
                }
            }

            Order::updateByParams($params, ['id' => $order->id]);
        }

        // Удалить из сессии ID заказа, чтобы, чтоб освободить "место" для нового заказа
        Yii::$app->db->createCommand()->delete('{{%queue_order}}', [
            'order_id' => $order->id
        ])->execute();
    }

    /**
     * затрешить заказ если звонок не удался
     * @param $id
     * @throws \yii\db\Exception
     */
    public function trashOrder($id)
    {
        $order = Order::findOne($id);
        $order->status = $order::STATUS_TRASH;

        $LeadSQS = new LeadSQS();
        $order->attempts++;

        if ($order->useStrategyByQueue()) {
            $params = $order->dirtyAttributes;
            if (key_exists('status', $params)) {
                $order->saveLogsByOrder();
            }

            if ($order->saveWithoutLogs()) {
                if ($LeadSQS->sendLeadStatus($order)) {
                    if ($order->id) {
                        Yii::$app->db->createCommand()->update('{{%order}}', ['sent_to_sqs' => true], ['id' => $order->id])->execute();
                    }
                }
            }

            Order::updateByParams($params, ['id' => $order->id]);
        }

        // Удалить из сессии ID заказа, чтобы, чтоб освободить "место" для нового заказа
        Yii::$app->db->createCommand()->delete('{{%queue_order}}', [
            'order_id' => $order->id
        ])->execute();

    }
}

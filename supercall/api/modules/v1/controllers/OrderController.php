<?php

namespace api\modules\v1\controllers;

use api\components\filters\VerbFilter;
use api\components\rest\Controller;
use api\models\ApiLog;
use api\modules\v1\models\Order;
use common\components\web\User;
use common\models\Country;
use common\modules\order\models\Order as OrderModel;
use common\modules\order\models\search\OrderSearch;
use common\modules\partner\models\Partner;
use Yii;
use yii\helpers\Json;
use yii\validators\StringValidator;

/**
 * Class OrderController
 * @package api\modules\v1\controllers
 */
class OrderController extends Controller
{
    public $log;
    /**
     * @var array
     */
    protected $accessTypes = [
        'create' => [
            User::ACCESS_TYPE_PARTNER,
            User::ACCESS_TYPE_PAY,
            User::ACCESS_TYPE_DINA,
        ],
        'set-foreign-id-from-pay' => [
            User::ACCESS_TYPE_PARTNER,
            User::ACCESS_TYPE_PAY,
            User::ACCESS_TYPE_DINA,
        ],
        'status' => [
            User::ACCESS_TYPE_PARTNER,
        ],
        'approve' => [
            User::ACCESS_TYPE_GENESYS,
        ],
        'iframe' => [
            User::ACCESS_TYPE_GENESYS,
        ],
        'iframe-list' => [
            User::ACCESS_TYPE_GENESYS,
        ],
        'opers' => [
            User::ACCESS_TYPE_PARTNER,
            User::ACCESS_TYPE_PAY,
        ],
        'prod-calls' => [
            User::ACCESS_TYPE_PARTNER,
            User::ACCESS_TYPE_PAY,
        ],
        'get-approve' => [
            User::ACCESS_TYPE_PAY,
        ],
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create' => ['post'],
                    'status' => ['post'],
                    'set-foreign-id-from-pay' => ['post'],
                    'approve' => ['get'],
                    'iframe' => ['get'],
                    'iframe-list' => ['get'],
                    'opers' => ['post'],
                    'prod-calls' => ['post'],
                    'get-approve' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionGetApprove()
    {
        $from = yii::$app->request->get('from');
        $to = yii::$app->request->get('to');
        $partner = yii::$app->request->get('partner');

        if (!$from) {
            $from = yii::$app->formatter->asTimestamp(time() - 3600);
        }

        if (!$to) {
            $to = yii::$app->formatter->asTimestamp(time());
        }

        $partnerData = Partner::findOne(['name' => $partner]);

        if (!$partnerData) {
            $this->addMessage('Партнёр "' . $partner . '" не найден.');
            return $this->fail();
        }

        $updatedOrders = OrderModel::find()
            ->select([
                'order.id',
                'foreign_id',
                'status',
                'country.char_code',
                'partner_created_at',
                'order.created_at',
                'order.updated_at'
            ])
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . OrderModel::tableName() . '.country_id')
            ->where(['status' => OrderModel::STATUS_APPROVED])
            ->andWhere(['between', OrderModel::tableName() . '.updated_at', $from, $to])
            ->asArray()
            ->all();

        $this->addData($updatedOrders);

        return $this->success();
    }

    /**
     * @return array
     */
    public function actionCreate()
    {
        $model = new Order();
        $model->scenario = Order::SCENARIO_CREATE;
        $model->load(Yii::$app->request->post(), '');

        if ($id = $model->create()) {
            $this->setDataValue('id', $id);
            return $this->success();
        } else {
            $this->addMessagesByModel($model);
        }

        return $this->fail();
    }

    /**
     *  После отправки заказа с изменённым статусом - если заказа не было на пее - он пришлёт foreign_id обратно в КЦ
     */
    public function actionSetForeignIdFromPay()
    {
        $post = Yii::$app->request->post();

        if (isset($post['id'])) {
            $order = OrderModel::findOne(['id' => $post['id']]);
            $order->foreign_id = $post['foreign_id'];

            //foreign_id приняли, но не смогли обновить
            if (!$order->save()) {
                $this->log = new ApiLog();
                $this->log->url = Yii::$app->request->url;
                $this->log->order_id = $post['id'];
                $this->log->request = json_encode(['foreign_id from pay save error' => $order->getErrors()], JSON_UNESCAPED_UNICODE);
                $this->log->save();

                return $this->log->request;
            }

            return true;
        }
    }

    /**
     * @return array
     */
    public function actionStatus()
    {
        $model = new Order();
        $model->scenario = Order::SCENARIO_STATUS;
        $model->load(Yii::$app->request->post(), '');

        if ($response = $model->status()) {
            $this->addData($response);

            return $this->success();
        } else {
            $this->addMessagesByModel($model);
        }

        return $this->fail();
    }

    /**
     * @return array
     */
    public function actionOpers()
    {
        $model = new Order();
        $model->scenario = Order::SCENARIO_OPERS;
        $model->load(Yii::$app->request->post(), '');

        if ($response = $model->opers()) {
            $this->addData($response);

            return $this->success();
        } else {
            $this->addMessagesByModel($model);
        }

        return $this->fail();
    }

    /**
     * @return array
     */
    public function actionProdCalls()
    {
        $model = new Order();
        $model->scenario = Order::SCENARIO_PROD_CALLS;
        $model->load(Yii::$app->request->post(), '');

        if ($response = $model->prodCalls()) {
            $this->addData($response);

            return $this->success();
        } else {
            $this->addMessagesByModel($model);
        }

        return $this->fail();
    }

    /**
     * @param integer $id
     * @param string $operator
     * @return array
     */
    public function actionIframe($id, $identity)
    {
        $model = OrderModel::findOne($id);

        if ($model && !$model->buffered) {
            $this->setDataValue('url', $model->generateIframeUrl($identity));

            return $this->success();
        } else {
            $this->addMessage('Заказ не найден.');
        }

        return $this->fail();
    }

    /**
     * Сформируем ссылку на iframe для списка заказов
     *
     * @param $country - приходит в ISO формате
     * @param $phone
     * @param string $operator
     * @return array
     */
    public function actionIframeList($country, $phone, $operator = '')
    {
        $modelSearch = new OrderSearch();

        $queryCountry = Country::find()->select(['id'])->where(['char_code' => trim($country)]);
        $phone = preg_replace('/[^0-9]/', '', $phone);

        if ($queryCountry->exists() && !empty($phone)) {
            $countryISO = $queryCountry->scalar();
            $modelSearch->country = $countryISO;

            $modelSearch->customerPhone = $phone;

            $url = (new OrderModel)->generateIframeUrlList($modelSearch->country, $modelSearch->customerPhone, trim($operator));
            $this->setDataValue('url', $url);

            return $this->success();
        } else {
            $this->addMessage('Заказы не найден.');

            return $this->fail();
        }
    }

    /**
     * Функция по номеру телефона ищет в списке заказов последний апрув
     *
     * GET
     * Params: (string) "phone"
     *
     * Response: json
     * {
     *      "status": "success",
     *      "data": {
     *          "code": 1,
     *          "id": 433,
     *          "tracker": 45645ghgfjh7,
     *      }
     * }
     *
     *  "code" - Коды статуса:
     *  0 - не  найден
     *  1 - заказ в процессе оформления // продолжаем их дергать
     *  2 - в доставке // продолжаем их дергать
     *  3 - доставлен
     *  4 - недоставлен
     *  5 - отказ,дубль,треш (косяк) // с таким статусом отобразить в админке
     *  "id" - ID заказа;
     *  "tracker" - трекинг номер;
     */
    public function actionApprove($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        // проверить не старше 2-х месяцев
        $datetime = new \DateTime('-2 month');
        $timeAgo = Yii::$app->formatter->asTimestamp($datetime->format('Y-m-d'));

        $query = OrderModel::find()
            ->select(['id', 'approve_components', 'created_at'])
            ->where(['like', 'customer_phone', $phone])
            ->andWhere(['status' => OrderModel::STATUS_APPROVED])
            ->andWhere(['>', 'created_at', $timeAgo])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1);

        if ($query->exists()) {
            $model = $query->one();

            // получим массив
            $array = Json::decode($model->approve_components);
            $array['id'] = $model->id;
            $this->addData($array);

            return $this->success();
        } else {
            $this->addMessage('Заказ не найден.');
        }

        return $this->fail();
    }

    /**
     * Проверка статуса заказа в пее
     *
     */
    public function actionCheckStatus()
    {

    }

    protected function oldFieldsMap($array = [])
    {

    }


}

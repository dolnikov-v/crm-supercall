<?php
namespace api\modules\v1\controllers;

use common\modules\order\models\Order as OrderModel;
use api\modules\v1\models\Order;
use api\modules\v1\models\OrderTask;
use api\components\rest\Controller;
use common\modules\order\models\OrderProduct;
use Yii;
use api\models\ApiLog;
use common\components\web\User;
use yii\base\Exception;

/**
 * Class OrderTaskController
 * @package api\modules\v1\controllers
 */
class OrderTaskController extends Controller
{
    /**
     * @var integer
     */
    public $order_id;
    /**
     * @var array
     */
    protected $accessTypes = [
        'set-status' => [
            User::ACCESS_TYPE_PAY,
        ],
        'get-tasks-by-order-id' => [
            User::ACCESS_TYPE_PAY,
        ],
    ];

    /**
     * @return array
     */
    public function actionSetStatus()
    {
        $id = yii::$app->request->post('id');
        $status = yii::$app->request->post('status');
        $curator = yii::$app->request->post('curator');
        $curator_username = yii::$app->request->post('curator_username');

        $orderTask = OrderTask::find()->where(['id' => $id])->one();

        $this->log = new ApiLog();
        $this->log->order_id = $orderTask->order_id;
        $this->log->url = '/v1/order-task/set-status';
        $this->log->request = json_encode([$id, $status, $curator, $curator_username], JSON_UNESCAPED_UNICODE);

        try {
            if (!is_numeric($curator)) {
                $messageError = yii::t('common', '$curator должно быть целым цислом');

                $this->log->response = json_encode(['error' > $messageError], JSON_UNESCAPED_UNICODE);
                $this->log->save();

                $this->addMessage($messageError);
                return $this->fail();
            }

            if (!is_string($curator_username)) {
                $messageError = yii::t('common', '$curator_username должно быть строкой');

                $this->log->response = json_encode(['error' > $messageError], JSON_UNESCAPED_UNICODE);
                $this->log->save();

                $this->addMessage($messageError);
                return $this->fail();
            }



            if (!in_array($status, [OrderTask::TASK_STATUS_REJECTED, OrderTask::TASK_STATUS_APPROVED])) {

                $messageError = yii::t('common', 'Неизвестный статус. Доступны 3 - апрув, 2 - отказ');

                $this->log->response = json_encode(['error' > $messageError], JSON_UNESCAPED_UNICODE);
                $this->log->save();

                $this->addMessage($messageError);
                return $this->fail();
            }

            if (!is_null($orderTask)) {
                $orderTask->status = $status;
                $orderTask->curator = $curator;
                $orderTask->curator_username = $curator_username;
                $orderTask->updated_at = time();

                if (!$orderTask->save()) {
                    $messageError = yii::t('common', 'Ошибка смены статуса');
                    $this->addMessage($messageError);
                    $this->log->order_id = $orderTask->order_id;
                    $this->log->response = json_encode([$messageError, $orderTask->getErrors()], JSON_UNESCAPED_UNICODE);
                    $this->log->save();

                    return $this->fail();
                }

            } else {
                $messageNotFound = yii::t('common', 'Таск не найден');
                $this->addMessage($messageNotFound);

                $this->log->response = $messageNotFound;
                $this->log->save();

                return $this->fail();
            }

            $this->response = json_encode([$id, $status, $curator, $curator_username], JSON_UNESCAPED_UNICODE);
            $this->log->save();

            //обновление самого заказа
            if ($status == OrderTask::TASK_STATUS_APPROVED) {
                $order_id = $orderTask->order_id;
                $order = OrderModel::find()->where(['id' => $order_id])->one();

                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                if (!is_null($order)) {
                    $orderProducts = OrderProduct::findAll(['order_id' => $order_id]);

                    $this->order_id = $order_id;

                    foreach ($orderProducts as $orderProduct) {
                        if (!$orderProduct->delete()) {
                            $transaction->rollBack();

                            $this->log = new ApiLog();
                            $this->log->order_id = $this->order_id;
                            $this->log->url = '/v1/order-task/set-status';
                            $this->log->request = json_encode(['errors on delete orderProducts', $id, $status, $curator, $curator_username], JSON_UNESCAPED_UNICODE);
                            $this->log->response = json_encode($orderProduct->getErrors(), JSON_UNESCAPED_UNICODE);
                            $this->log->save();

                            return $this->fail();
                        }
                    }

                    $orderTask = OrderTask::find()->where(['id' => $id])->one();
                    $order_new = json_decode($orderTask->order_new);
                    $phones = is_array($order_new->order_phones) ? $order_new->order_phones: [];
                    $order->order_phones = !empty($phones) ? implode(', ', $phones) : '';

                    $order->customer_phone = isset($order_new->customer_phone) ? $order_new->customer_phone : '';
                    $order->customer_mobile = isset($order_new->customer_mobile) ? $order_new->customer_mobile : '';
                    $order->customer_email = isset($order_new->customer_email) ? $order_new->customer_email : '';
                    $order->customer_address = isset($order_new->customer_address) ? $order_new->customer_address : '';
                    $order->customer_first_name = isset($order_new->customer_first_name) ? $order_new->customer_first_name : '';
                    $order->customer_last_name = isset($order_new->customer_last_name) ? $order_new->customer_last_name : '';
                    $order->customer_middle_name = isset($order_new->customer_middle_name) ? $order_new->customer_middle_name : '';

                    $order->customer_full_name = isset($order_new->customer_full_name) ? $order_new->customer_full_name : '';

                    $order->customer_components = $order_new->customer_components;
                    $order->init_price = $order_new->init_price;
                    $order->final_price = $order_new->final_price;
                    $order->shipping_price = $order_new->shipping_price;
                    $order->updated_at = time();

                    //order new data update
                    if (!$order->save()) {
                        $transaction->rollBack();

                        $this->log = new ApiLog();
                        $this->log->order_id = $this->order_id;
                        $this->log->url = '/v1/order-task/set-status';
                        $this->log->request = json_encode(['errors on update order', $id, $status, $curator, $curator_username], JSON_UNESCAPED_UNICODE);
                        $this->log->response = json_encode(['order_id' => $order_id, 'errors' => $order->getErrors()], JSON_UNESCAPED_UNICODE);
                        $this->log->save();

                        $this->addMessage(['update order fail', 'order_id' => $order_id, 'order-task' => $id, 'errors' => implode(', ', $order->getErrors())]);

                        return $this->fail();
                    } //orderProduct update
                    else {
                        $products_new = json_decode($orderTask->product_new);

                        foreach ($products_new as $product) {
                            $orderProduct = new OrderProduct();
                            $orderProduct->order_id = $order_id;
                            $orderProduct->product_id = $product->product_id;
                            $orderProduct->quantity = $product->quantity;
                            $orderProduct->cost = $product->cost;
                            $orderProduct->price = $product->price;
                            $orderProduct->gift = $product->gift;

                            if (!$orderProduct->save()) {
                                $transaction->rollBack();

                                $this->log = new ApiLog();
                                $this->log->order_id = $this->order_id;
                                $this->log->url = '/v1/order-task/set-status';
                                $this->log->request = json_encode(['errors on insert order_product', $product], JSON_UNESCAPED_UNICODE);
                                $this->log->response = json_encode(['order_id' => $this->order_id, 'errors' => $order->getErrors()], JSON_UNESCAPED_UNICODE);
                                $this->log->save();

                                $this->addMessage(['add order_product fail', 'order_id' => $this->order_id, 'order-task' => $id, 'product_id' => $product['id']]);

                                return $this->fail();
                            }
                        }

                        $this->log = new ApiLog();
                        $this->log->order_id = $this->order_id;
                        $this->log->url = '/v1/order-task/set-status';
                        $this->log->request = json_encode(['update order & order_product', 'order_id' => $order_id], JSON_UNESCAPED_UNICODE);
                        $this->log->response = json_encode($order->getErrors(), JSON_UNESCAPED_UNICODE);
                        $this->log->save();

                        $orderTask->status = $status;

                        if (!$orderTask->save()) {
                            $transaction->rollBack();

                            $this->log = new ApiLog();
                            $this->log->order_id = $this->order_id;
                            $this->log->url = '/v1/order-task/set-status';
                            $this->log->request = json_encode(['update status on order_task', 'order_id' => $order_id], JSON_UNESCAPED_UNICODE);
                            $this->log->response = json_encode($order->getErrors(), JSON_UNESCAPED_UNICODE);
                            $this->log->save();

                            $this->addMessage(['update status order_task fail', 'order_id' => $this->order_id, 'order-task' => $id]);
                            return $this->fail();
                        }
                        $this->addMessage(['update order from order_task success', 'order_id' => $this->order_id, 'order-task' => $id]);
                        $transaction->commit();
                        return $this->success();

                    }
                }
            }
        }catch (\Exception $e){
            $this->log = new ApiLog();
            $this->log->order_id = $this->order_id;
            $this->log->url = '/v1/order-task/set-status';
            $this->log->request = json_encode(['errors on update order', $id, $status, $curator, $curator_username], JSON_UNESCAPED_UNICODE);
            $this->log->response = json_encode(['order_id' => $this->order_id, 'errors' => $e->getMessage()], JSON_UNESCAPED_UNICODE);
            $this->log->save();
        }

        return $this->success();
    }

    /**
     * @param $order_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetTasksByOrderId($order_id)
    {
        $orderTasks = OrderTask::find()
            ->where(['order_id' => $order_id])
            ->all();

        return $orderTasks;
    }
}
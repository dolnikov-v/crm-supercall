<?php

namespace api\modules\v1\controllers;

use api\components\rest\Controller;
use api\models\ApiLog;
use common\components\web\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use DateTime;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\Json;

class SkypeBotController extends Controller
{
    protected $log;
    /**
     * @var array
     */
    protected $accessTypes = [
        'time-lead-to-call' => [
            User::ACCESS_TYPE_PAY,
        ],
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'time-lead-to-call' => ['get'],
                ],
            ],
        ];
    }

    /**
     * https://2wtrade-tasks.atlassian.net/browse/WCALL-422
     * Среднее время от лида до звонка за период
     * @param $to
     * @param $from
     * @return array
     */
    public function actionTimeLeadToCall($to, $from)
    {

        $request = Yii::$app->request;

        $this->log = new ApiLog();
        $this->log->auth = $request->getHeaders()->get('Authorization');
        $partUrl = explode('?', $request->url);
        $this->log->url = $partUrl[0];
        $this->log->request = Json::encode(Yii::$app->request->queryParams);

        if (!DateTime::createFromFormat('Y-m-d H:i:s', $to) || !DateTime::createFromFormat('Y-m-d H:i:s', $from)) {
            $this->addMessage('to: ' . $to);
            $this->addMessage('from: ' . $from);
            $this->addMessage('Incorrect format period. Use this format http://url?to=Y-m-d H:i:s&from=Y-m-d H:i:s');

            /** @var ApiLog */
            $this->log->response = Json::encode($this->fail());
            $this->log->save();

            return $this->fail();
        } else {
            $to = yii::$app->formatter->asTimestamp($to);
            $from = yii::$app->formatter->asTimestamp($from);

            $subQuery = (new Query())->from(OrderLog::tableName())
                ->select([
                    'id' => new Expression('MAX(id)'),
                    'created_at',
                    'order_id'
                ])
                ->where(['field' => 'status'])
                ->andWhere(['is not', 'created_at', null])
                ->groupBy(['order_id', 'created_at']);

            $data = Order::find()
                ->leftJoin(['ol' => $subQuery], 'ol.order_id=' . Order::tableName() . '.id')
                ->select([
                    'avg_time' => new Expression('AVG(ol.created_at - partner_created_at)')
                ])
                ->where(['is not', 'partner_created_at', null])
                ->andWhere(['>=', 'ol.created_at', $to])
                ->andWhere(['<=', 'ol.created_at', $from]);

            $this->addMessage($data->one()->avg_time);

            /** @var ApiLog */
            $this->log->response = Json::encode($this->success());
            $this->log->save();

            return $this->success();
        }

    }
}
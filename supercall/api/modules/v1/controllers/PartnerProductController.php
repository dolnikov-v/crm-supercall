<?php
namespace api\modules\v1\controllers;

use api\components\filters\VerbFilter;
use api\components\rest\Controller;
use api\modules\v1\models\PartnerProduct as PartnerProductForm;
use common\components\web\User;
use common\models\Product;
use common\modules\partner\helpers\PartnerProduct;
use Yii;

/**
 * Class PartnerProductController
 * @package api\modules\v1\controllers
 */
class PartnerProductController extends Controller
{
    /**
     * @var array
     */
    protected $accessTypes = [
        'available' => [
            User::ACCESS_TYPE_PARTNER
        ],
        'index' => [
            User::ACCESS_TYPE_PARTNER
        ],
        'create' => [
            User::ACCESS_TYPE_PARTNER
        ],
        'update' => [
            User::ACCESS_TYPE_PARTNER
        ],
        'update-price' => [
            User::ACCESS_TYPE_PARTNER,
        ],
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'available' => ['get'],
                    'index' => ['get'],
                    'create' => ['post'],
                    'update' => ['post'],
                    'update-price' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionAvailable()
    {
        $this->addData(Product::find()->select(['id', 'name'])->all());
        return $this->success();
    }

    /**
     * @return array
     */
    public function actionIndex()
    {
        $this->addData(PartnerProduct::getMapping(Yii::$app->partner->id));
        return $this->success();
    }

    /**
     * Метод создания сущности партнер-товар
     * @return array
     */
    public function actionCreate()
    {
        $model = new PartnerProductForm(PartnerProductForm::SCENARIO_CREATE, Yii::$app->partner->id);
        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            return $this->success();
        }

        $this->addMessagesByModel($model);
        return $this->fail();
    }

    /**
     * Метод изменения сущности партнер-товар
     * @param integer $product_id
     * @return array
     */
    public function actionUpdate($product_id)
    {
        $model = new PartnerProductForm(PartnerProductForm::SCENARIO_UPDATE, Yii::$app->partner->id, $product_id);
        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            return $this->success();
        }

        $this->addMessagesByModel($model);
        return $this->fail();
    }

    /**
     * Метод сохранения расчета цены для сущности партнер-товар
     * @param integer $product_id
     * @return array
     */
    public function actionUpdatePrice($product_id)
    {
        $model = new PartnerProductForm(PartnerProductForm::SCENARIO_UPDATE_PRICE, Yii::$app->partner->id, $product_id);
        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            // возвращать продукт и страну
            $this->addData([
                'product_id' => $product_id,
                'country' => Yii::$app->request->post('country'),
            ]);
            return $this->success();
        }

        $this->addMessagesByModel($model);
        return $this->fail();
    }
}

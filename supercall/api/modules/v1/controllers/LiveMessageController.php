<?php

namespace api\modules\v1\controllers;

use api\components\rest\Controller;
use backend\modules\queue\models\Queue;
use backend\modules\queue\models\QueueUser;
use common\components\ChatApiCom;
use common\components\Platinum;
use common\components\web\User;
use api\components\filters\VerbFilter;
use common\models\LiveMessage;
use common\models\QueueOrder;
use common\models\Timezone;
use common\models\UserReady;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class LiveMessageController
 * @package api\modules\v1\controllers
 */
class LiveMessageController extends Controller
{
    /**
     * @var array
     */
    protected $accessTypes = [
        'create' => [
            User::ACCESS_TYPE_CC,
        ],
    ];


    public $template = [
        'order_id',
        'content',
        'type'
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function mapping()
    {
        return [
            'text' => 'content',
            'order' => 'order_id'
        ];
    }

    /**
     * @param $field
     * @return bool|mixed
     */
    public function mapField($field)
    {
        $mapping = $this->mapping();

        if (isset($mapping[$field])) {
            return $mapping[$field];
        } else {
            return false;
        }
    }

    /**
     * @param $message
     * @return array|bool
     */
    public function mapMessage($message)
    {
        $mappedMessage = [];

        foreach ($message as $field => $value) {
            if ($this->mapField($field)) {
                $mappedMessage[$this->mapField($field)] = $value;
            }
        }

        $mappedMessage['type'] = LiveMessage::TYPE_WHATSAPP;

        if (!empty(array_diff($this->template, array_keys($mappedMessage)))) {
            $logger = yii::$app->get('logger');
            $logger->action = basename(__METHOD__);
            $logger->type = ChatApiCom::LOGGER_TYPE;
            $logger->tags = [
                'before_map' => $message,
                'after_map' => $mappedMessage
            ];
            $logger->error = 'incorrect format message';
            $logger->save();

            return false;
        }

        return $mappedMessage;
    }

    /**
     * @return array|bool
     */
    public function actionCreate()
    {
        $messages = yii::$app->request->post('messages');
        $decoded_messages = json_decode((string)$messages, 1);

        $last_message = array_pop($decoded_messages);
        $mappedMessage = $this->mapMessage($last_message);

        if (!$mappedMessage) {
            $template = $this->template;
            unset($template['type']);

            $this->addMessage('Incorrect format data, expected fields: '
                . implode(',', array_keys($this->mapping())) . ', received: '
                . implode(', ', array_keys($last_message)));
            return $this->fail();
        }

        /** @var ChatApiCom $chatApiCom */
        $chatApiCom = yii::$app->get('ChatApiCom');
        $result = $chatApiCom->setAnswer($mappedMessage);

        if (!$result['success']) {
            $this->addMessage($result['message']);
            return $this->fail();
        } else {
            $this->addData(['live_message_id' => $result['message']]);

            $sendToNodeJS = [];

            //оператор уже работает с заказом
            if (QueueOrder::find()->where(['order_id' => $mappedMessage['order_id']])->exists()) {
                $sendToNodeJS = $chatApiCom->sentToNodeJS($chatApiCom->nodeJSserver, $result['message']);
            } else {
                //поиск оператора
                //$operator_id = 26;
                if ($operator_id = $this->getOperatorForOrder($mappedMessage['order_id'])) {
                    //отправка оператору заказа
                    $sendToNodeJS = $this->sentOrderToOperator($operator_id, $mappedMessage['order_id']);
                    $this->setBlockToOrder($operator_id, $mappedMessage['order_id']);
                } else {
                    //отправить в очередь на NODE JS
                    $toQeueuNodeJS = $this->sentOrderToQueueNodeJS($mappedMessage['order_id']);
                }
            }

            return $this->success();
        }
    }

    /**
     * @param $operator_id
     * @param $order_id
     */
    public function sentOrderToOperator($operator_id, $order_id)
    {
        /** @var ChatApiCom $chatApiCom */
        $chatApiCom = yii::$app->get('ChatApiCom');
        $json_data = json_encode([
            'order_id' => $order_id,
            'operator_id' => $operator_id,
            'roomForOrder' => $operator_id
        ]);

        $ch = curl_init($chatApiCom->openOrder);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data))
        );


        $res = curl_exec($ch) . "\n";
        curl_close($ch);
    }

    /**
     * Подобрать оператора на заказ
     * @return mixed
     */
    public function getOperatorForOrder($order_id)
    {
        $optimalOperator = false;

        $operators = UserReady::getUsersOnline(['use_messenger' => true]);

        if (!empty($operators)) {
            $optimalOperator = $this->getOptimalOperator($operators, $order_id);
        }

        return $optimalOperator;
    }

    /**
     * Подобрать оператора по очереди заказа
     * @param $noBusyOperators
     * @return bool|int
     */
    public function getOptimalOperator($noBusyOperators, $order_id)
    {
        $order = Order::findOne(['id' => $order_id]);
        $queue = Queue::findOne(['id' => $order->prevent_queue_id]);
        $accessOperators = ArrayHelper::getColumn(QueueUser::find()->where(['queue_id' => $queue->id])->asArray()->all(), 'user_id');
        $operators = array_intersect($accessOperators, $noBusyOperators);
        $sortOperators = OrderLog::find()->select(['user_id'])->where(['user_id' => $operators])->orderBy(['id' => SORT_DESC])->one();

        if(is_null($sortOperators)){
            $operatorId = array_shift($operators);
        } else {
            $operatorId = $sortOperators->user_id;
        }


        return $operatorId;
    }



    /**
     * @param $operator_id
     * @param $order_id
     * @return bool
     */
    public function setBlockToOrder($operator_id, $order_id)
    {
        $result = QueueOrder::setBlockOrder($operator_id, $order_id);

        if (!$result['success']) {
            $logger = yii::$app->get('logger');
            $logger->action = basename(__METHOD__);
            $logger->type = ChatApiCom::LOGGER_TYPE;
            $logger->tags = [
                'order_id' => $order_id,
                'user_id' => $operator_id,
            ];
            $logger->error = json_encode($result['message'], 256);
            $logger->save();

            return false;
        }

        return true;
    }

    /**
     * @param $order_id
     */
    public function sentOrderToQueueNodeJS($order_id)
    {
        /** @var ChatApiCom $chatApiCom */
        $chatApiCom = yii::$app->get('chatApiCom');
        $json_data = json_encode([
            'order_id' => $order_id,
        ]);

        $ch = curl_init($chatApiCom->toQueue);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data))
        );

        $res = curl_exec($ch) . "\n";
        curl_close($ch);

    }
}
<?php

namespace api\modules\v1\controllers;


use api\components\rest\Controller;
use common\components\web\User;
use common\models\Country;
use common\models\CountryPhoneCode;
use Yii;

class ValidatorController extends Controller
{
    protected $accessTypes = [
        'phone' => [
            User::ACCESS_TYPE_PAY
        ],
    ];

    public function actionPhone()
    {
        $this->addData(Yii::$app->request->get());

        $phone = Yii::$app->request->get('phone');
        $charCode = Yii::$app->request->get('char_code');

        if (!$phone || !$charCode) {
            $message = !$phone ? 'Phone not setting. ' : '';
            $message .= !$charCode ? 'char_code not setting. ' : '';

            $this->addData($message, 'Message');
            return $this->successFalse();
        }

        $country = Country::findOne(['char_code' => strtoupper($charCode)]);

        if (is_null($country)) {
            $this->addData('Country not found', 'Message');
            return $this->successFalse();
        }

        if(!CountryPhoneCode::validatePhone($phone,$country->id)){
            $this->addData('Not valid phone', 'Message');
            return $this->successFalse();
        }

        $newPhone = CountryPhoneCode::checkPhone($phone, $country->id);

        $this->addForPayData($newPhone, 'phone');

        return $this->successTrue();
    }
}
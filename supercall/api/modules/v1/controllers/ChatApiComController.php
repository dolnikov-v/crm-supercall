<?php
namespace api\modules\v1\controllers;

use api\components\filters\VerbFilter;
use api\components\rest\Controller;
use common\components\ChatApiCom;
use common\components\web\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class ChatApiComController
 * @package api\modules\v1\controllers
 */
class ChatApiComController extends Controller
{
    const STATUS_INIT = 'init';
    const STATUS_LOADING = 'loading';
    const STATUS_QR_CODE = 'got qr code';
    const STATUS_SUCCESS = 'authenticated';

    /**
     * @var array
     */
    protected $accessTypes = [
        'send-message' => [
            User::ACCESS_TYPE_CC,
        ],
        'get-messages' => [
            User::ACCESS_TYPE_CC,
        ],
        'check-status' => [
            User::ACCESS_TYPE_CC,
        ],
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'send-message' => ['post'],
                    'get-messages' => ['post'],
                    'check-status' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @return ChatApiCom
     */
    public function getChatApiCom()
    {
        return yii::$app->get('ChatApiCom');
    }


    /**
     * метод отправки сообщения
     * @return string
     */
    public function actionSendMessage()
    {
        $phone = yii::$app->request->post('phone');
        $message = yii::$app->request->post('message');
        $order_id = yii::$app->request->post('order_id');

        $response = $this->getChatApiCom()->sentMessage($phone, $message);

        $decoded = Json::decode($response);

        if (isset($decoded['sent']) && $decoded['sent']) {
            $result = [
                'success' => true,
                'order_id' => $order_id,
                'message' => $decoded['message']
            ];
        } else {
            $result = [
                'success' => false,
                'order_id' => $order_id,
                'message' => $response
            ];
        }

        return Json::encode($result);
    }

    /**
     * метод получения истории сообщений
     * @return string
     */
    public function actionGetMessages()
    {
        $countryPhone = yii::$app->request->get('country-phone');
        $phone = yii::$app->request->get('phone');
        $url = yii::$app->request->get('url');
        $token = yii::$app->request->get('token');

        $number = 1000;
        $response = $this->getChatApiCom()->getMessages($number, $url, $token);
        $result = Json::decode($response);
        $messages = [];

        if (isset($result['messages'])) {
            foreach ($result['messages'] as $message) {
                if ($message['chatId'] == $phone . ChatApiCom::END) {

                    if ($message['author'] == $phone . ChatApiCom::END) {
                        $messages[] = [
                            'direction' => 'response',
                            'order' => null,
                            'text' => $message
                        ];
                    } elseif ($message['author'] == $countryPhone . ChatApiCom::END) {
                        $messages[] = [
                            'direction' => 'request',
                            'order' => null,
                            'text' => $message
                        ];
                    }
                }
            }
        }

        return Json::encode($messages);
    }

    /**
     * метод проверки чата (активности вебя whatsapp)
     * @return string
     */
    public function actionCheckStatus()
    {
        return $this->getChatApiCom()->checkStatus();
    }
}
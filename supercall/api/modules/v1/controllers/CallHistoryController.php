<?php
namespace api\modules\v1\controllers;

use api\components\filters\VerbFilter;
use api\components\rest\Controller;
use common\components\Logstash;
use common\components\web\User;
use common\modules\call\models\CallHistory;
use yii\base\Exception;
use yii\di\Instance;

/**
 * Class CallHistoryController
 * @package api\modules\v1\controllers
 */
class CallHistoryController extends Controller
{
    const METHOD = __METHOD__;
    const EVENT_NOT_FOUND = 'not_found';
    const EVENT_ERROR_SAVE = 'error_save';
    const EVENT_UPDATED = 'updated';
    const EVENT_EXCEPTION = 'exception';
    const EVENT_UPDATE_BILLSEC= 'update_billsec';

    /**
     * @var Logstash
     */
    public $logstash;
    /**
     * @var array
     */
    protected $accessTypes = [
        'hangup-call-status' => [
            User::ACCESS_TYPE_ASTERISK,
        ],
    ];

    public function init()
    {
        parent::init();
        $this->logstash = Instance::ensure($this->logstash, Logstash::class);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'hangup-call-status' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Самый актуальный метод для подтягивания данных о записях разговоров
     *  данные шлёт asterweb.2wcall.com \app\modules\webhook\controllers\AutoCallController::actionCallback
     * @return array
     */
    public function actionHangupCallStatus()
    {
        //        "phone":"62081344668475",
        //        "user_sip":"2wcall608",
        //        "uniqueid":"1555913527.22719387",
        //        "disposition":"ANSWERED",
        //        "recordingfile":"2019-04-22/09/7689739-62081344668475-2wcall608-09:12.wav",
        //        "billsec":"2",
        //        "duration":"17",
        //        "action":"hangup"

        $phone = \Yii::$app->request->post('phone');
        $user_sip = \Yii::$app->request->post('user_sip');
        $uniqueid = \Yii::$app->request->post('uniqueid');
        $disposition = \Yii::$app->request->post('call_status');
        $recordingfile = \Yii::$app->request->post('recordingfile');
        $billsec = \Yii::$app->request->post('billsec');
        $duration = \Yii::$app->request->post('duration');

        $update = false;

        try {
            $callHistoryQuery = CallHistory::find()
                ->where([
                    'or',
                    ['phone' => $phone],
                    ['user_sip' => $user_sip]
                ])
                ->andWhere(['disposition' => CallHistory::DISPOSITION_WAITING])
                //->andWhere(['is', 'uniqueid', null])
                //->andWhere(['is', 'recordingfile', null])
                ->orderBy(['id' => SORT_DESC]);

            if (!$callHistoryQuery->exists()) {
                $this->logstash->saveLog([
                    'method' => self::METHOD,
                    'event' => self::EVENT_NOT_FOUND,
                    'sql' => $callHistoryQuery->createCommand()->getRawSql(),
                    'data' => \Yii::$app->request->post()
                ]);

                $this->addData([
                    'event' => self::EVENT_NOT_FOUND,
                    'sql' => $callHistoryQuery->createCommand()->getRawSql(),
                    'data' => \Yii::$app->request->post()
                ]);

            } else {
                /** @var CallHistory $callHistory */
                $callHistory = $callHistoryQuery->one();

                if (!$callHistory->user_id) {
                    $userQuery = \common\models\User::find();
                    $userQuery->andFilterWhere(['=', "sip::jsonb->>'sip_login'", $user_sip]);

                    if ($userQuery->exists()) {
                        $user = $userQuery->one();
                        $callHistory->user_id = $user->id;
                    }
                }

                $callHistory->uniqueid = $uniqueid;
                $callHistory->disposition = $disposition;
                $callHistory->recordingfile = $recordingfile;
                $callHistory->billsec = $billsec;
                $callHistory->duration = $duration;
                $callHistory->user_sip = $user_sip;

                if (!$callHistory->save(false)) {
                    $this->logstash->saveLog([
                        'method' => self::METHOD,
                        'event' => self::EVENT_ERROR_SAVE,
                        'sql' => $callHistory->getErrors(),
                        'data' => \Yii::$app->request->post()
                    ]);

                    $this->addData([
                        'event' => self::EVENT_ERROR_SAVE,
                        'errors' => $callHistory->getErrors()
                    ]);
                } else {
                    $this->addData([
                        'event' => self::EVENT_UPDATED,
                        'callHistory' => $callHistory->getAttributes()
                    ]);

                    $update = true;
                }
            }
        } catch (Exception $e) {
            $this->logstash->saveLog([
                'method' => self::METHOD,
                'event' => self::EVENT_EXCEPTION,
                'data' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
        }

        return $update ? $this->success() : $this->fail();
    }
}
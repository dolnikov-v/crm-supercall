**Список доступных товаров**
----

**Уровень доступа**

    `partner`

**URL**

    /v1/partner-product/available

**Метод**

    `GET`
    
**Параметры**

    -

**Success Ответ**

    ```
    {
        "status": "success",
        "data": {
            {
                "id": 1,
                "name": "Goji cream"
            }
        }
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": {"error : {список ошибок}}
        }
    }
    ```


**Список внутренних кодов товаров**
----

**Уровень доступа**

    `partner`

**URL**

    /v1/partner-product/index

**Метод**

    `GET`

**Параметры**

    -

**Success Ответ**

    ```
    {
        "status": "success",
        "data": {
            "1": {
                "product_id": 10,
                "active": 1
            },
        }
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": {"error : {список ошибок}}
        }
    }
    ```


**Создание товара партнера**
----

**Уровень доступа**

    `partner`

**URL**

    /v1/partner-product/create

**Метод**

    `POST`

**Параметры**

    `product_id` - Integer, ид товара (из списка /v1/partner-product/available)
    `partner_product_id` - Integer, номер продукта у партнера (partner_product.partner_product_id)
    `active` - Integer, статус товара, 0 - неактивен, 1 - активен

**Success Ответ**

    ```
    {
        "status": "success",
        "data": []
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": {"error : {список ошибок}}
        }
    }
    ```
    
    
**Изменение товара партнера**
----

**Уровень доступа**

    `partner`

**URL**

    /v1/partner-product/update/:product_id

**Метод**

    `POST`

**Параметры**

    `partner_product_id` - Integer, номер продукта у партнера (partner_product.partner_product_id)
    `active` - Integer, статус товара, 0 - неактивен, 1 - активен

**Success Ответ**

    ```
    {
        "status": "success",
        "data": []
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": {"error : {список ошибок}}
        }
    }
    ```
    
    
**Изменение товара партнера**
----

**Уровень доступа**

    `partner`

**URL**

    /v1/partner-product/update/:product_id

**Метод**

    `POST`

**Параметры**

    `partner_product_id` - Integer, номер продукта у партнера (partner_product.partner_product_id)
    `active` - Integer, статус товара, 0 - неактивен, 1 - активен

**Success Ответ**

    ```
    {
        "status": "success",
        "data": []
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": {"error : {список ошибок}}
        }
    }
    ```

**Установка цены товара партнера**
----

**Уровень доступа**

    `partner`

**URL**

    /v1/partner-product/update-price/:product_id

**Метод**

    `POST`

**Параметры**

    `product_id` - Integer, номер продукта у партнера (partner_product.partner_product_id)
    `country` - String, код страны
    `price_data` - json массив, если `type` == 1 указывается массив со значениями колличество товара => цена в поле table
       если `type` == 2, указывается статичная цена в поле price

**Success Ответ**

    ```
    {
        "status": "success",
        "data": {
            "product_id": "10",
            "country": "IN"
        }
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": {"error : {список ошибок}}
        }
    }
    ```

**Инициировать отправку SMS**
----

**Уровень доступа**

    `genesys`

**URL**

    /v1/sms/send
    
**Метод**

    `POST`
    
**Заголовки**

    `Authorization` - Токен авторизации.

**Параметры**

    `id` - Integer, Номер заказа в системе партнера.
    `phone` - String, Телефон заказчика.

**Success Ответ**

    ```
    {
        "status": "success"
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": [
                "Неверно указан номер",
            ]
        }
    }
    ```
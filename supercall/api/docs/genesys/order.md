**Получение ссылки для Iframe**
----

**Уровень доступа**

    `genesys`

**URL**

    /v1/order/iframe/:id?:operator

**Метод**

    `GET`

**Заголовки**

    `Authorization` - Токен авторизации.
    
**Параметры**

    `id` - Integer, Номер заказа.
    `operator` - String, Оператор.

**Success Ответ**

    ```
    {
        "status": "success",
        "data": {
            "url": "http://api.unicall.in/order/change/080451-11264be210662ebaeedbc6103e759cb770a1e0f8.html?operator=op"
        }
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": [
                "Заказ не найден."
            ]
        }
    }
    ```



**Получение ссылки для Iframe Списка заказов**
----

**Уровень доступа**

    `genesys`

**URL**

    /v1/order/iframe-list

**Метод**

    `GET`

**Заголовки**

    `Authorization` - Токен авторизации.
    
**Параметры**

    `country` - String, код страны.
    `phone` - String, номер телефона.
    `operator` - String, Оператор.

**Success Ответ**

    ```
    {
        "status": "success",
        "data": {
            "url": "http://api.unicall.in/order/index/000001-11264be210662ebaeedbc6103e759cb770a1e0f8.html?operator=op"
        }
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": [
                "Заказы не найдены."
            ]
        }
    }
    ```
    
    

**Получение информации об Approve-заказе**
----

**Уровень доступа**

    `genesys`

**URL**

    /v1/order/approve

**Метод**

    `GET`
    
**Заголовки**

    `Authorization` - Токен авторизации.
    
**Параметры**

    `phone` - String, Номер телефона.
    
**Success Ответ**

    ```
    {
        "status": "success",
        "data": {
            "code": 1,
            "id": 433,
            "tracker": 45645ghgfjh7,
    	
        }
    }
    ```
    
    где
        "code" - Коды статуса:
        1 - заказ в процессе оформления
        2 - в доставке
        3 - доставлен
        4 - недоставлен
        
        "id" - ID заказа;
        "tracker" - трекинг номер;

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": [
                "Заказ не найден."
            ]
        }
    }
    ```
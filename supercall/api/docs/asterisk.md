**Запись разговора**
----

**Уровень доступа**

    `asterisk`

**URL**

    /v1/record/load

**Метод**

    `POST`
    
**Параметры**

    `path` - String, Путь к файлу 

**Success Ответ**

    ```
    {
        "status": "success",
        "data": {
            {
                "id": 1,
                "phone": "40728148058"
            }
        }
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": {"error : {список ошибок}}
        }
    }
    ```


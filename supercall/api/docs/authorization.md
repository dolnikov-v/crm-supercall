**Авторизация по токену**
----

Для авторизации, необходимо передать токен в заголовках HTTP вместе с запросом.

Токен должен быть закодирован base64().

**Пример**

    ```
    {
        "Authorization": "Basic nTc3X2Y0MjFlGOW&DJxIgDH4215lOTJmM2Y2NTIxNGU5OWJlYh=="
    }
    ```

**Error Ответ**

    ```
    {
        "status": "fail",
        "data": {
            "messages": [
                "Неверный токен аутентификации."
            ]
        }
    }
    ```

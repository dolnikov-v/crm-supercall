<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'genesys',
    ],
    'controllerNamespace' => 'api\controllers',
    'defaultRoute' => 'default/index',
    'name' => '2WCall',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-api',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'assetManager' => [
            'linkAssets' => YII_ENV_DEV ? true : false,
            'bundles' => [
                'yii\web\YiiAsset' => [
                    'depends' => [
                        'common\assets\vendor\BootstrapAsset',
                    ],
                ],
                'yii\web\JqueryAsset' => [
                    'css' => [],
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'css' => [],
                    'js' => [],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                // [\d]+[-]+[\w]+ для iframe заказа
                // [\d]+[-]+[\d]+[-]+[\w]+ для iframe списка заказов
                '<module:[\w-]+>/<controller:[\w-]+>/<id:([\d]+[-]+[\w]+)|([\d]+[-]+[\d]+[-]+[\w]+)>.html' => '<module>/<controller>/index',

                '<module:[\w-]+>/<folder:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<folder>/<controller>/<action>',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',

                '<module:[\w-]+>/<folder:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<folder>/<controller>/<action>',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
            ],
        ],
        'partner' => [
            'class' => 'api\components\Partner'
        ],
    ],
    'modules' => [
        'order' => [
            'class' => 'api\modules\order\Module',
        ],
        'v1' => [
            'class' => 'api\modules\v1\Module',
            'controllerMap' => [
                'auto-call' => [
                    'class' => 'api\modules\v1\controllers\AutoCallController',
                    'amqp' => 'amqp'
                ],
                'call-history' => [
                    'class' => 'api\modules\v1\controllers\CallHistoryController',
                    'logstash' => 'logstash'
                ]
            ]
        ],
    ],
    'params' => $params,
];

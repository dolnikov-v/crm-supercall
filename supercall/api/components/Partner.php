<?php
namespace api\components;

use yii\base\Component;

/**
 * Class Partner
 * @package app\components
 * @property \common\modules\partner\models\Partner $identity
 * @property integer $id
 */
class Partner extends Component
{
    /** @var \common\modules\partner\models\Partner */
    protected $identity;

    /**
     * @param $value
     */
    public function setIdentity($value)
    {
        $this->identity = $value;
    }

    /**
     * @return \common\modules\partner\models\Partner
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->identity->id;
    }
}

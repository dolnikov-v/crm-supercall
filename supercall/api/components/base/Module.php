<?php
namespace api\components\base;

use api\components\filters\auth\HttpBasicAuth;
use common\components\base\Module as CommonModule;
use Yii;

/**
 * Class Module
 * @package api\components\base
 */
class Module extends CommonModule
{
    /**
     * @return array
     */
    public function behaviors()
    {
        Yii::$app->user->enableSession = false;

        $behaviors = [];

        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
        ];

        return $behaviors;
    }
}

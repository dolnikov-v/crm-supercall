<?php
namespace api\components\rest;

use api\models\ApiLog;
use common\components\rest\Controller as CommonController;
use Yii;
use yii\helpers\Json;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class Controller
 * @package api\components\rest
 */
class Controller extends CommonController
{
    const RESPONSE_STATUS_SUCCESS = 'success';
    const RESPONSE_STATUS_FAIL = 'fail';
    const VERSION_1 = 'v1';

    /**
     * Разрешенные доступы
     * @var array
     */
    protected $accessTypes = [];

    /**
     * @var string Формат ответа
     */
    protected $responseFormat = Response::FORMAT_JSON;

    /**
     * @var array Данные для ответа
     */
    protected $responseData = [
        'status' => self::RESPONSE_STATUS_FAIL,
        'data' => [],
    ];
    
    /**
     * Можем указать controller и action, для сохранения orderId
     * @var array
     */
    private $deny_action = [
        'order.create',
    ];

    /**
     * @var ApiLog
     */
    protected $log;

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($this->id != 'default') {
            if (array_key_exists($action->id, $this->accessTypes)) {
                $accessTypes = $this->accessTypes[$action->id];

                if (!in_array(Yii::$app->user->accessType, $accessTypes)) {
                    throw new ForbiddenHttpException('Доступ запрещен.');
                }
            } else {
                throw new ForbiddenHttpException('Доступ запрещен.');
            }
        }

        $request = Yii::$app->request;

        $this->log = new ApiLog();
        $this->log->url = $request->url;
        $this->log->auth = $request->getHeaders()->get('Authorization');
        $this->log->request = Json::encode(Yii::$app->request->post());
        $this->log->ip = $request->userIP;
        // Опеределим order_id
        if (Yii::$app->request->get('id')) {
            $this->log->order_id = Yii::$app->request->get('id');
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function runAction($id, $params = [])
    {
        try {
            return parent::runAction($id, $params);
        } catch (\Exception $e) {
            $this->addMessage($e->getMessage());
            $statusCode = isset($e->statusCode) ? $e->statusCode : 400;

            return $this->fail($statusCode);
        }
    }

    /**
     * @return array
     */
    protected function respond()
    {
        Yii::$app->response->format = $this->responseFormat;

        if ($this->log){
            $this->log->response = Json::encode($this->responseData);
    
            // проверить, можно ли логировать экшн или нет
            // для /v1/order/create необходимо сохранять orderId
            if ($this->responseData['status'] === 'success') {
                if (in_array($this->id.'.'.$this->action->id, $this->deny_action)) {
                    $this->log->order_id = $this->responseData['data']['id'];
                }
            }
            
            $this->log->save(false);
        }

        return $this->responseData;
    }

    /**
     * Успешный ответ
     */
    protected function success()
    {
        Yii::$app->response->statusCode = 200;
        $this->responseData['status'] = self::RESPONSE_STATUS_SUCCESS;

        return $this->respond();
    }

    /**
     * @return array
     */
    public function successTrue()
    {
        Yii::$app->response->statusCode = 200;
        $this->responseData['success'] = true;

        return $this->respondResult();
    }

    /**
     * @param null $statusCode
     * @return array
     */
    public function successFalse($statusCode = null)
    {
        if (is_null($statusCode)) {
            $statusCode = 400;
        }

        Yii::$app->response->statusCode = $statusCode;
        $this->responseData['success'] = false;

        return $this->respondResult();
    }

    /**
     * @return array
     */
    protected function respondResult()
    {
        Yii::$app->response->format = $this->responseFormat;

        if ($this->log){
            $this->log->response = Json::encode($this->responseData);

            // проверить, можно ли логировать экшн или нет
            // для /v1/order/create необходимо сохранять orderId
            if ($this->responseData['success'] === true) {
                if (in_array($this->id.'.'.$this->action->id, $this->deny_action)) {
                    $this->log->order_id = $this->responseData['data']['id'];
                }
            }

            $this->log->save(false);
        }
        unset($this->responseData['status']);
        return array_reverse($this->responseData);
    }

    /**
     * @param null $statusCode
     * @return array
     */
    protected function fail($statusCode = null)
    {
        if (is_null($statusCode)) {
            $statusCode = 400;
        }

        Yii::$app->response->statusCode = $statusCode;
        $this->responseData['status'] = self::RESPONSE_STATUS_FAIL;

        return $this->respond();
    }

    /**
     * @param $message
     */
    protected function addMessage($message)
    {
        if (!isset($this->responseData['data']['messages'])) {
            $this->responseData['data']['messages'] = [];
        }

        $this->responseData['data']['messages'][] = $message;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    protected function setDataValue($key, $value)
    {
        $this->responseData['data'][$key] = $value;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    protected function addDataValue($key, $value)
    {
        if (!array_key_exists($key, $this->responseData['data'])) {
            $this->responseData['data'][$key] = [];
            $this->responseData['data'][$key][] = $value;
        } else {
            if (is_array($this->responseData['data'][$key])) {
                $this->responseData['data'][$key][] = $value;
            }
        }
    }

    /**
     * @param mixed $value
     */
    protected function addData($value, $key = 'data')
    {
        $this->responseData[$key][] = $value;
    }

    /**
     * @param mixed $value
     */
    protected function addForPayData($value, $key = 'data')
    {
        $this->responseData[$key] = $value;
    }

    /**
     * @param \yii\base\Model $model
     * @param boolean $first
     */
    protected function addMessagesByModel($model, $first = false)
    {
        $errorsAttributes = $model->getErrors();

        foreach ($errorsAttributes as $errorsAttribute) {
            foreach ($errorsAttribute as $error) {
                $this->addMessage($error);

                if ($first) {
                    return;
                }
            }
        }
    }
}

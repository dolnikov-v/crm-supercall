<?php

namespace api\components\filters\auth;

use common\components\web\User;
use common\modules\partner\models\Partner;
use Yii;
use yii\filters\auth\AuthMethod;
use yii\httpclient\Client;
use yii\web\UnauthorizedHttpException;

/**
 * Class HttpBasicAuth
 * @package api\components\filters\auth
 */
class HttpBasicAuth extends AuthMethod
{
    /**
     * @var string
     */
    public $realm = 'api';

    public $allowedGetMethods = [
        'v1/user/get-users',
        'v1/user/get-call-stat',
        'v1/user/get-opers-activity-data',
        'v1/asterisk/call-status',
        'v1/asterisk/incoming-call',
    ];

    /**
     * @param $url
     * @return string
     */
    public function authenticateWithGet($url)
    {
        $params = yii::$app->request->get();

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(Yii::$app->getRequest()->hostInfo . $url)
            ->addHeaders(['Authorization' => 'Basic ' . yii::$app->request->get('token')])
            ->setData($params)
            ->send();

        return $response->content;
    }

    /**
     * @param User $user
     * @param \yii\web\Request $request
     * @param \yii\web\Response $response
     * @return boolean|null
     */
    public function authenticate($user, $request, $response)
    {
        //для Пея - по КЦ - работа с GET
        if (Yii::$app->request->method == 'GET' && in_array($request->pathInfo, $this->allowedGetMethods)) {
            echo $this->authenticateWithGet($request->url);
            die();
        }

        $authHeader = $request->getHeaders()->get('Authorization');

        if ($authHeader !== null && preg_match('/^Basic\s+(.*?)$/', $authHeader, $matches)) {
            $accessToken = base64_decode($matches[1]);

            if (is_string($accessToken)) {
                if ($accessToken == User::ACCESS_TYPE_PAY) {
                    $user->accessType = User::ACCESS_TYPE_PAY;
                    return true;
                } else if ($accessToken == User::ACCESS_TYPE_DINA) {
                    $user->accessType = User::ACCESS_TYPE_DINA;
                    return true;
                }else if ($accessToken == User::ACCESS_TYPE_CC) {
                    $user->accessType = User::ACCESS_TYPE_CC;
                    return true;
                }elseif (Yii::$app->genesys->token === $accessToken) {
                    $user->accessType = User::ACCESS_TYPE_GENESYS;
                    return true;
                } elseif ($accessToken === $accessToken) {
                    $user->accessType = User::ACCESS_TYPE_ASTERISK;
                    return true;
                } elseif ($accessToken == User::ACCESS_TYPE_PLATINUM) {
                    $user->accessType = User::ACCESS_TYPE_PLATINUM;
                    return true;
                }else if ($accessToken == User::ACCESS_TYPE_CC) {
                    $user->accessType = User::ACCESS_TYPE_CC;
                    return true;
                }

                else {
                    $partner = Partner::find()
                        ->byApiKey($accessToken)
                        ->active()
                        ->one();

                    if ($partner) {
                        Yii::$app->partner->identity = $partner;
                        $user->accessType = User::ACCESS_TYPE_PARTNER;;

                        return true;
                    }
                }
            }
        }

        return null;
    }

    /**
     * @param \yii\web\Response $response
     * @throws UnauthorizedHttpException
     */
    public function challenge($response)
    {
        $response->getHeaders()->set('WWW-Authenticate', 'Basic realm="' . $this->realm . '"');

        throw new UnauthorizedHttpException('Неверный токен аутентификации.');
    }
}

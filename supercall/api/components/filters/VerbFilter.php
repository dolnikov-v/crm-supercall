<?php
namespace api\components\filters;

use Yii;
use yii\filters\VerbFilter as YiiVerbFilter;
use yii\web\MethodNotAllowedHttpException;

/**
 * Class VerbFilter
 * @package api\components\filters
 */
class VerbFilter extends YiiVerbFilter
{
    /**
     * @inheritdoc
     */
    public function beforeAction($event)
    {
        try {
            parent::beforeAction($event);
        } catch (MethodNotAllowedHttpException $e) {
            throw new MethodNotAllowedHttpException(Yii::t('common', 'Метод не разрешен.'));
        }
    }
}

<?php
namespace api\components\web;

use common\components\web\Controller as CommonController;
use Yii;

/**
 * Class Controller
 * @package api\components\web
 */
class Controller extends CommonController
{
    public $layout = '@api/views/layouts/main';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $language = Yii::$app->user->getLanguage();

        Yii::$app->language = $language->locale;
        Yii::$app->formatter->setLocale($language->locale);
    }
}

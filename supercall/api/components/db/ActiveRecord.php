<?php
namespace api\components\db;

use Yii;

/**
 * Class ActiveRecord
 * @package api\components\db
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * @return \yii\db\Connection
     */
    public static function getDb()
    {
        return Yii::$app->genesys->getDb();
    }

}

<?php
namespace backend\helpers;

/**
 * Class Formatter
 * @package backend\helpers
 */
class Formatter extends Base
{
    /**
     * @var array
     */
    private static $dateFormats = [
        'ru-RU' => 'php:d.m.Y',
        'en-US' => 'php:m/d/Y',
        'es-ES' => 'php:d/m/Y',
        'tr-TR' => 'php:d.m.Y',
    ];

    /**
     * @var array
     */
    private static $timeFormats = [
        'ru-RU' => 'php:H:i',
        'en-US' => 'php:H:i',
        'es-ES' => 'php:H:i',
        'tr-TR' => 'php:H:i',
    ];

    /**
     * @var array
     */
    private static $datetimeFormats = [
        'ru-RU' => 'php:d.m.Y H:i',
        'en-US' => 'php:m/d/Y H:i',
        'es-ES' => 'php:d/m/Y H:i',
        'tr-TR' => 'php:d.m.Y H:i',
    ];

    /**
     * @var array
     */
    private static $fullTimeFormats = [
        'ru-RU' => 'php:H:i:s',
        'en-US' => 'php:H:i:s',
        'es-ES' => 'php:H:i:s',
        'tr-TR' => 'php:H:i:s',
    ];

    /**
     * @var array
     */
    private static $mysqlDateFormats = [
        'ru-RU' => '%d.%m.%Y',
        'en-US' => '%m/%d/%Y',
        'es-ES' => '%d/%m/%Y',
        'tr-TR' => '%d.%m.%Y',
    ];

    /**
     * @var array
     */
    private static $mysqlMonthFormats = [
        'ru-RU' => '%m.%Y',
        'en-US' => '%m/%Y',
        'es-ES' => '%m/%Y',
        'tr-TR' => '%m.%Y',
    ];

    /**
     * @return string
     */
    public static function getDateFormat()
    {
        return self::getFormat(self::$dateFormats);
    }

    /**
     * @return string
     */
    public static function getTimeFormat()
    {
        return self::getFormat(self::$timeFormats);
    }

    /**
     * @return string
     */
    public static function getDatetimeFormat()
    {
        return self::getFormat(self::$datetimeFormats);
    }

    /**
     * @return string
     */
    public static function getFullTimeFormat()
    {
        return self::getFormat(self::$fullTimeFormats);
    }

    /**
     * @return string
     */
    public static function getMysqlDateFormat()
    {
        return self::getFormat(self::$mysqlDateFormats);
    }

    /**
     * @return string
     */
    public static function getMysqlMonthFormat()
    {
        return self::getFormat(self::$mysqlMonthFormats);
    }
}

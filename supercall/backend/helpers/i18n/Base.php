<?php
namespace backend\helpers;

use Yii;

/**
 * Class Base
 * @package backend\helpers
 */
class Base
{
    /**
     * @param array $formats
     * @return string
     */
    protected static function getFormat($formats)
    {
        $result = '';

        if (isset($formats[Yii::$app->language])) {
            $result = $formats[Yii::$app->language];
        }

        if (!$result) {
            $result = array_shift($formats);
        }

        return $result;
    }
}

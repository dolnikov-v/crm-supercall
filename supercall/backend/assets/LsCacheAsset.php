<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class LsCacheAsset
 * @package app\assets\vendor
 */
class LsCacheAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/theme/lscache';

    public $js = [
        'lscache.min.js'
    ];
}

<?php

namespace backend\assets;

use yii\web\AssetBundle;

class SourceAsset extends AssetBundle
{
    public $sourcePath = '@app/web/resources/modules/catalog';

    public $js = [
        'source.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

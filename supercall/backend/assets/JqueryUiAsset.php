<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class GridStackAsset
 * @package doindo\assets
 */
class JqueryUiAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/theme/jquery-ui';

    public $js = [
        'jquery-ui.min.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

<?php

namespace backend\assets\base;

use yii\web\AssetBundle;

class ThemeSloganAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/base/theme-slogan';

    public $js = [
        'theme-slogan.js',
    ];

    public $css = [
        'theme-slogan.css',
    ];

    public $depends = [
    ];
}
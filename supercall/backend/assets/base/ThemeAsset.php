<?php
namespace backend\assets\base;

use yii\web\AssetBundle;

/**
 * Class ThemeAsset
 * @package backend\assets\base
 */
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/base/theme';

    public $js = [
        'theme.js',
        'theme.custom.js',
    ];

    public $css = [
        'theme.css',
        'theme.custom.css',
    ];

    public $depends = [
        'common\assets\base\ComponentsAsset',
        'common\assets\fonts\FlagIconAsset',
        'common\assets\fonts\FontAwesomeAsset',
        'common\assets\fonts\GlyphiconsAsset',
        'common\assets\fonts\OpenSansAsset',
        'common\assets\fonts\PeIconAsset',
        'common\assets\vendor\AsScrollableAsset',
        'common\assets\vendor\BootstrapAsset',
        'common\assets\vendor\MetisMenuAsset',
        'common\assets\vendor\ToastrAsset',
    ];
}

<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class JiraIssueCollectorsAsset
 * @package backend\assets
 */
class JiraIssueCollectorsAsset extends AssetBundle
{
    public $sourcePath = '@app/web/resources/base/jira-issue-collectors';

    public $js = [
        'jira-issue-collectors.js',
    ];

    public $css = [
        'jira-issue-collectors.css',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
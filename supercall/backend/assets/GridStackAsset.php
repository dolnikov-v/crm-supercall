<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class GridStackAsset
 * @package doindo\assets
 */
class GridStackAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/theme/gridstack';

    public $css = [
        'gridstack.min.css',
        'gridstack-extra.min.css'
    ];

    public $js = [
        'gridstack.min.js',
        'component/gridstack.min.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'backend\assets\JqueryUiAsset',
        'backend\assets\LoDashAsset',
        'backend\assets\LsCacheAsset',
        'common\assets\vendor\BootstrapAsset',
        'common\assets\base\ComponentsAsset'
    ];
}

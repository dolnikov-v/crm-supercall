<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class SiteBeginAsset
 * @package backend\assets
 */
class SiteAsset extends AssetBundle
{
    public $sourcePath = '@app/web/theme/site';

    public $css = [
        'main.css',
    ];

    public $js = [
        'core.min.js',
        'site.min.js',
        'main.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

// @todo: Перенести стили bootstrap в папку vendor/bootstrap
// @todo: Добавить apple-touch-icon и shortcut icon

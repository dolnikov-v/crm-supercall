<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class LoDashAsset
 * @package app\assets\vendor
 */
class LoDashAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/theme/lodash';

    public $js = [
        'lodash.min.js'
    ];

    public $depends = [
        'backend\assets\JqueryUiAsset',
    ];
}

<?php
namespace backend\assets\modules\queue;

use yii\web\AssetBundle;

/**
 * Class Queue Asset
 * @package backend\assets\base
 */
class QueueAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/queue/';

    public $js = [
        'form.js',
    ];

    public $css = [
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\BootstrapAsset',
    ];
}

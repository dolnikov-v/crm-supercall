<?php
namespace backend\assets\modules\presets;

use yii\web\AssetBundle;

/**
 * Class Presets Asset
 * @package backend\assets\base
 */
class PresetsAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/presets/';

    public $js = [
        'form.js',
    ];

    public $css = [
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\BootstrapAsset',
    ];
}

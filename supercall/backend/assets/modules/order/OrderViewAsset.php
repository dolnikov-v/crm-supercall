<?php
namespace backend\assets\modules\order;

use yii\web\AssetBundle;

/**
 * Class OrderView Asset
 * @package backend\assets\base
 */
class OrderViewAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/order-view/';

    public $js = [
        'order.view.js',
    ];

    public $css = [
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\BootstrapAsset',
    ];
}

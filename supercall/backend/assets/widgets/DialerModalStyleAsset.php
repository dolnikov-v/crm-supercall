<?php
namespace backend\assets\widgets;

use yii\web\AssetBundle;

class DialerModalStyleAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/sipml5-slide-style';
    
    public $css = [
        'sipml5-modal.css',
    ];
    
    public $js = [
        'sipml5-api-beatufy.js',
        'sipml5-core.js',
        'sipml5-2wcall.js',
        'sipml5-2wcall-modal.js'
    ];
}

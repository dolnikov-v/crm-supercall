<?php
namespace backend\assets\widgets\navbar;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class TimezoneSwitcherAsset
 * @package backend\assets\widgets\navbar
 */
class TimezoneSwitcherAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/widgets/navbar/timezone-switcher';

    public $js = [
        'timezone-switcher.js',
    ];

    public $css = [
        'timezone-switcher.css',
    ];
    
    public $depends = [
        'backend\assets\base\ThemeAsset',
    ];

}

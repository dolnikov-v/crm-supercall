<?php
namespace backend\assets\widgets\navbar;

use yii\web\AssetBundle;

/**
 * Class ClockAsset
 * @package backend\assets\widgets\navbar
 */
class ClockAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/widgets/navbar/clock';

    public $js = [
        'clock.js',
    ];

    public $css = [
        'clock.css',
    ];

    public $depends = [
        'common\assets\vendor\MomentAsset',
        'common\assets\vendor\ATimerAsset',
        'backend\assets\base\ThemeAsset',
    ];
}

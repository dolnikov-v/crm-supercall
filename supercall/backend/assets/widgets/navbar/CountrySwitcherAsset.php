<?php
namespace backend\assets\widgets\navbar;

use yii\web\AssetBundle;

/**
 * Class CountrySwitcherAsset
 * @package backend\assets\widgets\navbar
 */
class CountrySwitcherAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/widgets/navbar/country-switcher';

    public $js = [
        'country-switcher.js',
    ];

    public $css = [
        'country-switcher.css',
    ];

    public $depends = [
        'backend\assets\base\ThemeAsset',
    ];
}

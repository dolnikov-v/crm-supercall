<?php
namespace backend\assets\widgets\navbar;

use yii\web\AssetBundle;

/**
 * Class ClockAsset
 * @package backend\assets\widgets\navbar
 */
class MaskAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/widgets/navbar/mask';

    public $js = [
        'jquery.mask.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

<?php
namespace backend\modules\i18n\controllers;

use backend\components\web\Controller;
use common\models\search\SourceMessageSearch;
use common\models\SourceMessage;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class PhraseController
 * @package backend\modules\i18n\controllers
 */
class PhraseController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new SourceMessageSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new SourceMessage();
            $model->category = SourceMessage::CATEGORY_COMMON;
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Фраза успешно добавлена.') : Yii::t('common', 'Фраза успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Фраза успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return SourceMessage
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = SourceMessage::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Фраза не найдена.'));
        }

        return $model;
    }
}

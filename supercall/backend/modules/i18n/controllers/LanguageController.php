<?php
namespace backend\modules\i18n\controllers;

use backend\components\web\Controller;
use common\models\Language;
use common\models\search\LanguageSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class LanguageController
 * @package backend\modules\i18n\controllers
 */
class LanguageController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new LanguageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = Language::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Язык не найден.'));
        }

        $model->active = 1;

        if ($model->save()) {
            Yii::$app->notifier->addNotifierError(Yii::t('common', 'Язык успешно активирован.'));
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDeactivate($id)
    {
        $model = Language::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Язык не найден.'));
        }

        if (!$model->source_language) {
            $model->active = 0;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier(Yii::t('common', 'Язык успешно деактивирован.'), 'success');
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        } else {
            Yii::$app->notifier->addNotifierError(Yii::t('common', 'Нельзя деактивировать основной язык.'));
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param string $locale
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionChange($locale = '')
    {
        $language = Language::find()
            ->active()
            ->byLocale($locale)
            ->one();

        if (!$language) {
            throw new HttpException(400, Yii::t('common', 'Не удалось изменить язык интерфейса.'));
        }

        Yii::$app->session->set('user.language.id', $language->id);

        return $this->redirect(Yii::$app->request->referrer);
    }
    
    /**
     * Функция открывает страницу для редактирования языка
     * и сохраняет изменения после редактирования
     *
     * @param integer|null $id
     * @param string|null $returnUrl
     * @return string|\yii\web\Response
     */
    public function actionEdit($id = null, $returnUrl = null)
    {
        if ($id) {
            $model = Language::findOne($id);
        } else {
            $model = new Language();
        }
        
        if ( $model->load(Yii::$app->request->post()) ) {
            
            if ($model->save()) {
                Yii::$app->notifier->addNotifier($model->isNewRecord
                    ? Yii::t('common', 'Язык успешно добавлен.')
                    : Yii::t('common', 'Язык успешно отредактирован.'), 'success');
                return $this->redirect( $returnUrl ? $returnUrl : Url::toRoute('index') );
            }
            else
                Yii::$app->notifier->addNotifierErrorByModel($model);
            
        }
        $genesysNameList = Language::getLanguageList();
    
        return $this->render('edit', [
            'model'           => $model,
            'genesysNameList' => $genesysNameList,
        ]);
    }
}

<?php
namespace backend\modules\i18n\controllers;

use backend\components\web\Controller;
use common\models\Language;
use common\models\Message;
use common\models\search\MessageSearch;
use common\models\SourceMessage;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class TranslationController
 * @package backend\modules\i18n\controllers
 */
class TranslationController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new MessageSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $tmpLanguages = Language::find()->collection();

        $languages = [];

        foreach ($tmpLanguages as $tmpLanguageKey => $tmpLanguage) {
            $languages[$tmpLanguageKey] = $tmpLanguage . ($tmpLanguageKey ? ' (' . $tmpLanguageKey . ')' : '');
        }

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'languages' => $languages,
            'languagesAll' => Language::find()->collectionAll(),
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit()
    {
        $source = (int)Yii::$app->request->get('source');
        $language = Yii::$app->request->get('language');

        $data['sourceMessage'] = SourceMessage::findOne($source);

        if (!$data['sourceMessage']) {
            throw new HttpException(400, Yii::t('common', 'Переводимая фраза не найдена.'));
        }

        if ($language) {
            $model = Message::find()
                ->where(['id' => $source])
                ->andWhere(['language' => $language])
                ->one();

            if (!$model) {
                throw new HttpException(404, Yii::t('common', 'Перевод не найден.'));
            }

            $data['translationForm'] = [
                'action' => Url::toRoute(['edit', 'source' => $source, 'language' => $language]),
            ];
        } else {
            $model = new Message();
            $model->id = $source;

            $data['translationForm'] = [
                'action' => Url::toRoute(['edit', 'source' => $source]),
            ];
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Перевод успешно добавлен.') : Yii::t('common', 'Перевод успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $data['model'] = $model;
        $languages = Language::find()->collection();

        $data['languagesAll'] = Language::find()->collectionAll();

        $languagesKeys = array_keys($languages);

        $data['translations'] = new ActiveDataProvider([
            'query' => Message::find()
                ->with('sourceMessage')
                ->where(['id' => $source])
                ->andWhere(['in', 'language', $languagesKeys])
        ]);

        foreach ($data['translations']->getModels() as $translation) {
            /** @var Message $translation */
            if (!$model->isNewRecord && $model->language == $translation->language) {
                continue;
            }

            unset($languages[$translation->language]);
        }

        $data['languages'] = [];

        foreach ($languages as $languageKey => $language) {
            $data['languages'][$languageKey] = $language . ' (' . $languageKey . ')';
        }

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        /** @var Message $model */
        $model = Message::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Перевод не найден.'));
        }

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Перевод успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

}

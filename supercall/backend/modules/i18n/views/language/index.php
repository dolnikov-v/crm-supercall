<?php
use common\components\grid\{
    ActionColumn, DateColumn, GridView
};
use common\helpers\grid\DataProvider;
use common\widgets\base\{
    Panel, ButtonLink
};
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Таблица языков');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Интернационализация'), 'url' => '/'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список языков')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица языков'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ((!$model->active) ? 'text-muted' : ''),
            ];
        },
        'columns' => [
            [
                'attribute' => 'id',
                'label' => '#',
                'headerOptions' => ['class' => 'width-100'],
            ],
            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'locale',
                'headerOptions' => ['class' => 'width-100 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'genesys_name',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'created_at',
                'enableSorting' => false,
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.language.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['deactivate', 'id' => $data->id]);
                        },
                        'can' => function ($data) {
                            return Yii::$app->user->can('i18n.language.deactivate') && $data->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['activate', 'id' => $data->id]);
                        },
                        'can' => function ($data) {
                            return Yii::$app->user->can('i18n.language.activate') && !$data->active;
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('i18n.language.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить язык'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>


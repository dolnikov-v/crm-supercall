<?php

use common\widgets\ActiveForm;
use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin(['action' => Url::current()]); ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'locale')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'icon')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'genesys_name')->dropDownList($genesysNameList) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить язык') : Yii::t('common', 'Сохранить язык')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
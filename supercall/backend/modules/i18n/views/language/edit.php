<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\Partner $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление языка') : Yii::t('common', 'Редактирование языка');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список языков'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/i18n/language/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Язык'),
    'content' => $this->render('_edit-form', [
        'model'           => $model,
        'genesysNameList' => $genesysNameList,
    ]),
]) ?>

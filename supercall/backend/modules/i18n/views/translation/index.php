<?php
use common\components\grid\ActionColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \common\models\search\MessageSearch $modelSearch */
/** @var \common\models\Language[] $languages */
/** @var \common\models\Language[] $languagesAll */

$this->title = Yii::t('common', 'Список переводов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Интернационализация'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Переводы')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с переводами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => IdColumn::className()],
            [
                'attribute' => 'language',
                'contentOptions' => ['class' => 'width-200'],
                'content' => function ($data) use ($languagesAll) {
                    $name = isset($languagesAll[$data->language]) ? $languagesAll[$data->language] : '';
                    return $name . ' (' . $data->language . ')';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'sourceMessage.message',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'translation',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'source' => $data->id, 'language' => $data->language]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Добавить еще перевод'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'source' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.edit');
                        }
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.delete');
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>

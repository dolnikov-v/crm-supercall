<?php
use common\components\grid\ActionColumn;
use common\components\grid\GridView;
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\Message $model */
/** @var array $translationForm */
/** @var \common\models\Language[] $languages */
/** @var \common\models\SourceMessage $sourceMessage */
/** @var \common\models\Language $languagesAll */
/** @var \common\models\Message $translations */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление перевода') : Yii::t('common', 'Редактирование перевода');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Интернационализация'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список переводов'), 'url' => Url::toRoute('/i18n/translation/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление перевода') : Yii::t('common', 'Редактирование перевода')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Перевод'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'translationForm' => $translationForm,
        'languages' => $languages,
        'sourceMessage' => $sourceMessage,
    ])
]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Существующие переводы'),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $translations,
        'columns' => [
            [
                'attribute' => 'language',
                'contentOptions' => ['class' => 'width-200'],
                'content' => function ($data) use ($languagesAll) {
                    $name = isset($languagesAll[$data->language]) ? $languagesAll[$data->language] : '';

                    return $name . ' (' . $data->language . ')';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'translation',
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'source' => $data->id, 'language' => $data->language]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.edit');
                        }
                    ],
                ]
            ]
        ],
    ])
]); ?>

<?php
use common\components\grid\ActionColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var \common\models\search\SourceMessageSearch $modelSearch */

$this->title = Yii::t('common', 'Список фраз');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Интернационализация'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с фразами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'category',
                'headerOptions' => ['class' => 'width-150'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'message',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'is_fully_translation',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($data) {
                    return Label::widget([
                        'label' => Yii::t('common', $data->is_fully_translation ? 'Да' : 'Нет'),
                        'style' => $data->is_fully_translation ? Label::STYLE_SUCCESS : Label::STYLE_DANGER,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.phrase.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Добавить перевод'),
                        'url' => function ($data) {
                            return Url::toRoute(['translation/edit', 'source' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.translation.edit');
                        }
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('i18n.phrase.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-target' => "#modal_confirm_delete",
                                'data-toggle' => "modal",
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('i18n.phrase.delete');
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('i18n.phrase.edit') ?
            ButtonLink::widget([
                    'label' => Yii::t('common', 'Добавить фразу'),
                    'url' => Url::toRoute('edit'),
                    'style' => ButtonLink::STYLE_SUCCESS,
                    'size' => ButtonLink::SIZE_SMALL,
                ]
            ) : '') . LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget(); ?>

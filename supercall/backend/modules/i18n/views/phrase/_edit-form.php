<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\models\SourceMessage $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'category')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'message')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить фразу') : Yii::t('common', 'Сохранить фразу')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php

namespace backend\modules\queue;


use backend\components\base\Module as BackendModule;
/**
 * queue module definition class
 */
class Module extends BackendModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\queue\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

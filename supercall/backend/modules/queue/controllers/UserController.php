<?php

namespace backend\modules\queue\controllers;

use backend\modules\administration\models\QueueUserLog;
use backend\modules\queue\models\Queue;
use common\models\Asterisk;
use common\models\Teams;
use common\models\Timezone;
use common\models\User;
use Yii;
use backend\modules\queue\models\QueueUser;
use backend\modules\queue\models\search\QueueUserSearch;
use common\components\web\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use backend\modules\administration\models\ApiLog;

/**
 * UserController implements the CRUD actions for QueueUser model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all QueueUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QueueUserSearch();

        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);
        $dataProvider = $searchModel->search($query);

        $users = User::find()
            ->byIdentityUser()
            ->byStatus(User::STATUS_DEFAULT)
            ->byRole([User::ROLE_APPLICANT, User::ROLE_OPERATOR, User::ROLE_SENIOR_OPERATOR])
            ->all();

        $queues = Queue::find()
            ->byIdentityUser()
            ->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'contentVisible' => Yii::$app->session->get($filterSessId.'.isVisible', true),
            'dataProvider' => $dataProvider,
            'users' => ArrayHelper::map($users, 'id', 'username'),
            'teams' => Teams::find()->active()->collection(),
            'queues' => ArrayHelper::map($queues, 'id', 'name')
        ]);
    }

    public function actionEdit(int $user_id = null)
    {
        if ($user_id) {
            $model = QueueUser::find()
                ->select([
                    new Expression("array_to_json(array_agg(concat(queue_id))) as queue_ids"),
                    'user_id',
                ])
                ->where(['user_id' => $user_id])
                ->groupBy(['user_id'])
                ->one();
        }

        if(!isset($model) || !$model){
            $model = new QueueUser();
            $model->queue_ids = [];
        }
        
        if (Yii::$app->request->isPost) {
            $postQueueUser = Yii::$app->request->post('QueueUser');

            if (!isset($postQueueUser['user_id']) || empty($postQueueUser['user_id'])) {
                Yii::$app->notifier->addNotifierError('Не указан Оператор');

                return $this->redirect(Url::toRoute('index'));
            }

            $currentTime = (new \DateTime())
                ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
                ->getTimestamp();

            // Если очереди заданы
            if (key_exists('queue_ids', $postQueueUser)) {
                // Вытащим доступы для данного оператора из БД
                $dbQueueUser = QueueUser::find()
                    ->select(['queue_id'])
                    ->where([
                        'user_id' => $postQueueUser['user_id']
                    ]);

                $queue_ids = $postQueueUser['queue_ids'];
                $arrayDelete = []; // в этот массив попадут очереди, которые необходимо удалить
                if ($dbQueueUser->exists()) {
                    foreach ($dbQueueUser->each() as $queue) {
                        if (!in_array($queue->queue_id, $queue_ids)) {
                            $arrayDelete[] = $queue->queue_id;
                        }
                        // Если есть в БД и в POST массиве, то не трогаем
                        else {
                            // нужен ключ
                            $key = array_search($queue->queue_id, $queue_ids);
                            unset($queue_ids[$key]);
                        }
                    }

                    // удалим те очереди, что уже не используются
                    QueueUser::deleteAll(['and', [
                        'user_id' => $postQueueUser['user_id']
                    ], [
                        'in', 'queue_id', $arrayDelete
                    ]]);

                    if ($arrayDelete) {
                        QueueUserLog::saveQueue($arrayDelete, $postQueueUser['user_id'], QueueUserLog::ACTION_DELETE);
                    }


                }
                // этой новый пользователь, т.к. у него еще нет доступа в БД
                //else {
                    //
                //}

                if (!empty($queue_ids)) {
                    foreach ($queue_ids as $queueId) {
                        $rows[] = [$queueId, $postQueueUser['user_id'], $currentTime, $currentTime];
                    }
                    // Если есть новые очереди => добавим
                    \Yii::$app->db->createCommand()->batchInsert(QueueUser::tableName(), [
                        'queue_id', 'user_id', 'created_at', 'updated_at'
                    ], $rows)->execute();

                    QueueUserLog::saveQueue($queue_ids, $postQueueUser['user_id'], QueueUserLog::ACTION_INSERT);
                }
                //отключил, т.к. на астере операторы распределяются на подочереди в php yii auto-call/generate
                //Queue::updateAsteriskMemberQueues($postQueueUser['user_id'], $postQueueUser['queue_ids']);

            }
            // удалить все связки Доступа данного Оператора с очередями
            else {
                $dbQueueUser = QueueUser::find()
                    ->select(['queue_id'])
                    ->where([
                        'user_id' => $postQueueUser['user_id']
                    ]);

                if ($dbQueueUser->exists()) {
                    $queueIds = [];

                    foreach ($dbQueueUser->each() as $queue) {
                        $queueIds[] = $queue->queue_id;
                    }

                    QueueUserLog::saveQueue($queueIds, $postQueueUser['user_id'], QueueUserLog::ACTION_DELETE);
                }

                QueueUser::deleteAll(['user_id' => $postQueueUser['user_id']]);
                //$this->deleteAsteriskMemberQueues($postQueueUser['user_id']);
            }

            Yii::$app->notifier->addNotifier(Yii::t('common', 'Права на очередь отредактированы.'), 'success');
            return $this->redirect(Url::toRoute('index'));
        }

        return $this->render('edit', [
            'model' => $model,
            'users' => ArrayHelper::map(User::find()->asArray()->all(), 'id', 'username'),
            'user_id' => $user_id
        ]);
    }

    /**
     * Deletes an existing QueueUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_DELETED;

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Доступ удален.'), 'success');
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * Finds the QueueUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QueueUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QueueUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //метод не используется и требует пересмотра удаления на сервере астериска
    public function deleteAsteriskMemberQueues($user_id) {
        $user = User::findOne($user_id);
        
        if(yii::$app->params['is_dev']){
          $userSip = yii::$app->params['AsteriskSp_dev_prefix'].$user->id;
        }else{
          $userSip = yii::$app->params['AsteriskSp_prefix'].$user->id;
        }
        
        $result = Asterisk::sendRequest(['action'=> 'deleteMemberQueue', 'user_name' => $userSip]);
        if (!$result) {
          $log = new ApiLog();
          $log->request = 'delete user queues on Asterisk2 (user id = '.$user->id.')';
          $log->response = $user->id;
          $log->save();
        }
    }
}

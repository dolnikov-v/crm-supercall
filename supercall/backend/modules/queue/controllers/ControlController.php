<?php

namespace backend\modules\queue\controllers;

use backend\modules\queue\models\query\QueueQuery;
use backend\modules\queue\models\QueueUser;
use backend\modules\queue\widgets\Strategy;
use common\models\Country;
use common\models\Product;
use common\models\QueueOrder;
use common\models\Timezone;
use common\modules\order\models\Order;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use kartik\grid\EditableColumnAction;
use Yii;
use backend\modules\queue\models\Queue;
use backend\modules\queue\models\search\QueueSearch;
use backend\components\web\Controller;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use backend\modules\access\models\AuthAssignment;
use common\models\User;
use common\models\UserCountry;
use yii2mod\editable\EditableAction;

/**
 * Control Controller implements the CRUD actions for Queue model.
 */
class ControlController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'get-strategy' => ['POST'],
                    'change-queue' => ['POST'],
                    'get-sub-statuses-by-status' => ['POST'],
                    'get-queue-by-country' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['get-queues-by-params', 'get-queue-by-country'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actions()
    {

        return ArrayHelper::merge(parent::actions(), [
            'change-priority' => [
                'class' => EditableAction::className(),
                'modelClass' => Queue::className(),
            ],
        ]);
    }

    public function actionSorting()
    {
        $sorting = Yii::$app->request->post('sorting');

        foreach ($sorting as $key => $sort) {
            Yii::$app->db->createCommand()->update('{{%queue}}',
                ['display_position' => ($key + 1)],
                ['id' => $sort])->execute();
        }

        return true;
    }

    /**
     * @return string
     */
    public function actionGetQueueByCountry()
    {
        $country_id = yii::$app->request->post('country_id');

        if (empty($country_id)) {
            $result = Queue::find()->all();
        } else {
            $result = Queue::find()
                ->byCountry($country_id, false)
                ->all();
        }

        $result = ArrayHelper::map($result, 'id', 'name');

        return Json::encode($result);
    }

    /**
     * Lists all Queue models.
     * @return mixed
     */
    public function actionIndex()
    {
        $modelSearch = new QueueSearch();
        $request = Yii::$app->request->queryParams;

        /** @var ActiveDataProvider $dataProvider */

        if (empty($request['QueueSearch']['name']) && empty($request['QueueSearch']['country_id']) && empty($request['QueueSearch']['partner_id'])) {
            $capacity = ['now' => [], 'hour' => []];
            $query = Queue::find()->where('1=2');
            $dataProvider = (new ActiveDataProvider(['query' => $query]));
        } else {
            $capacity = Queue::getCapacity();
            $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
        }

        $user_countries = ArrayHelper::map(yii::$app->user->getCountries(), 'id', 'name');
        $countries = [];

        foreach ($user_countries as $id => $country) {
            $countries[$id] = yii::t('common', $country);
        }

        $earliestOrders = Order::find()->select([
            'partner_created_at' => new Expression('min(partner_created_at)'),
            'last_queue_id'
        ])
            ->groupBy('last_queue_id')
            ->asArray()
            ->all();

        $newOrders = Order::find()
            ->select([
                'count_new' => new Expression("count(*)"),
                'last_queue_id'
            ])
            ->where(['is not', 'last_queue_id', null])
            ->andWhere(['status' => Order::STATUS_NEW])
            ->andWhere([
                    'or',
                    ['<', Order::tableName() . '.blocked_to', yii::$app->formatter->asTimestamp(time())],
                    ['is', Order::tableName() . '.blocked_to', null]
                ]
            )
            ->groupBy('last_queue_id')
            ->asArray()
            ->all();

        $earliestOrders = ArrayHelper::map($earliestOrders, 'last_queue_id', 'partner_created_at');
        $newOrders = ArrayHelper::map($newOrders, 'last_queue_id', 'count_new');

        return $this->render('index', [
            'cloneQueryForActive' => clone $dataProvider->query,
            'cloneQueryForNotActive' => clone $dataProvider->query,
            'capacity' => $capacity,
            'earliestOrders' => $earliestOrders,
            'newOrders' => $newOrders,
            'modelSearch' => $modelSearch,
            'countries' => $countries,
            'order_types' => ArrayHelper::map(OrderType::find()->all(), 'id', 'name'),
            'partners' => ArrayHelper::map(yii::$app->user->identity->partners, 'id', 'name')
        ]);
    }


    /**
     * Finds the Queue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Queue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Queue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Очередь успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Очередь успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new Queue();
        }

        if (Yii::$app->request->isPost && Yii::$app->user->can('queue.edit.configuration')) {
            // Если не выбран ни один товар для очереди
            if (!key_exists('primary_product', Yii::$app->request->post('Queue'))) {
                $model->primary_product = '';
            }

            $post = Yii::$app->request->post();

            if ($post['Queue']['is_primary'] == 1) {
                $post['Queue']['country_id'] = null;
            }


            if ($model->load($post)) {
                //для вторичных очередей страна обязательна
                if ($post['Queue']['is_primary'] == 0 && empty($post['Queue']['country_id'])) {
                    yii::$app->notifier->addNotifier(yii::t('common', 'Укажите страну'));
                } elseif (isset($post['Strategy']['is_count']) && isset($post['Strategy']) && isset($post['Strategy'][0]) && isset($post['Strategy'][0]['blockTime']) && $post['Strategy'][0]['blockTime'] < 30) {
                    Yii::$app->notifier->addNotifier(Yii::t('common', 'Блокировка между попытками должна быть больше 30 минут'));
                } else {
                    $isNewRecord = $model->isNewRecord;

                    // обработать стратегию в json
                    $strategy = Queue::transformStrategy(Yii::$app->request->post('Strategy'));
                    $model->strategy = $strategy;

                    $model->primary_components = [
                        'primary_type' => $model->primary_type,
                        'primary_country' => $model->primary_country,
                        'primary_partner' => $model->primary_partner,
                        'primary_product' => $model->primary_product,
                    ];

                    $postEvent = Yii::$app->request->post('Events');

                    //добавлены тригеры
                    if (key_exists('is_event', is_array($postEvent) ? $postEvent : [])) {
                        $event = json_encode([
                            'eventType' => $postEvent['eventType'],
                            'eventCount' => $postEvent['eventCount'],
                            'changeStatus' => $postEvent['eventChangeStatus'],
                            'changeSubStatus' => $postEvent['eventChangeSubStatus'],
                            'changeQueue' => isset($postEvent['eventChangeQueue']) ? $postEvent['eventChangeQueue'] : '',
                        ], JSON_UNESCAPED_UNICODE);


                        if (!empty($postEvent['eventChangeStatus'])) {
                            $model->scenario = Queue::EVENT_SCENARIO_CHANGE_STATUS;
                            $model->eventChangeStatus = $postEvent['eventChangeStatus'];
                            $model->eventChangeSubStatus = $postEvent['eventChangeSubStatus'];
                        } elseif (!empty($postEvent['eventChangeQueue'])) {
                            $model->scenario = Queue::EVENT_SCENARIO_CHANGE_QUEUE;
                            $model->eventChangeQueue = $postEvent['eventChangeQueue'];
                        } else {
                            $model->scenario = Queue::EVENT_SCENARIO_CHANGE_STATUS;
                            $model->eventChangeStatus = $postEvent['eventChangeStatus'];
                            $model->eventChangeSubStatus = $postEvent['eventChangeSubStatus'];
                        }

                        $model->eventType = $postEvent['eventType'];
                        $model->eventCount = $postEvent['eventCount'];

                    } else {
                        $event = '';
                    }

                    $model->event = $event;

                    if ($model->save()) {
                        Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Очередь успешно добавлена.') : Yii::t('common', 'Очередь успешно отредактирована.'), 'success');

                        return $this->redirect(Url::toRoute('index'));
                    } else {
                        Yii::$app->notifier->addNotifierErrorByModel($model);
                    }
                }
            }
        } elseif (Yii::$app->request->isPost && !Yii::$app->user->can('queue.edit.configuration')) {
            return $this->redirect(Url::toRoute('index'));
        }

        $dataOperators = $this->actionGetUsersForQueue($model->id, $model->primary_country, $model->country_id);
        $primary_components = json_decode($model->primary_components, 1);
        $partner_id = isset($primary_components['primary_partner']) ? $primary_components['primary_partner'] : null;
        $country_id = isset($primary_components['primary_country']) ? json_decode($primary_components['primary_country'], 1) : null;

        if (!$model->isNewRecord && !is_null($partner_id) && !is_null($country_id)) {
            $queues = Queue::getQueuesByPartnerOrCountry($partner_id, $country_id);
            $select2items = ArrayHelper::map($queues, 'id', 'name');
        }

        $countries = [];

        if (is_array(Yii::$app->user->countries)) {
            foreach (Yii::$app->user->countries as $k => $country) {
                $countries[$country->id] = yii::t('common', $country->name);
            }
        }

        $editable = Yii::$app->user->can('queue.edit.configuration');

        return $this->render('edit', [
            'model' => $model,
            'operators' => isset($dataOperators['operators']) ? $dataOperators['operators'] : [],
            'attachedOperators' => isset($dataOperators['attachedOperators']) ? $dataOperators['attachedOperators'] : [],
            'queue_id' => $model->id,
            'queue_list' => isset($select2items) ? $select2items : [],
            'countries' => $countries,
            'primary' => [
                'type' => OrderType::find()->collection('common'),
                'country' => Country::find()->collection('common'),
                'partner' => ArrayHelper::map(yii::$app->user->identity->partners, 'id', function ($val) {
                    return yii::t('common', $val->name);
                }),//Partner::find()->orderBy(['active' => SORT_DESC, 'id' => SORT_ASC])->collection(),
                'product' => Product::getActiveListProducts(),
            ],
            'editable' => $editable
        ]);
    }

    /**
     * Метод одновременно используется как action так и внутренний метод
     * Выводит всех доступных операторов по стране и операторов уже прикреплённых к очереди
     * @param null|Queue $queue_id
     * @param null|[] $country_ids
     * @return []|string
     */
    public function actionGetUsersForQueue($queue_id = null, $country_ids = null, $secondary_country_id = null)
    {
        $post = yii::$app->request->post();
        $queue_id = is_null($queue_id) ? yii::$app->request->post('queue_id') : $queue_id;
        $country_ids = is_null($country_ids) ? yii::$app->request->post('country_ids') : $country_ids;

        $is_primary = isset($post['is_primary']) ? (int)$post['is_primary'] : 0;
        $secondary_country_id = isset($post['secondary_country_id']) ? $post['secondary_country_id'] : $secondary_country_id;

        //if (!$country_ids || !$queue_id) {
        //    return !isset($post['json']) ? [] : json_encode([], JSON_UNESCAPED_UNICODE);
        //} else {

        $operators = User::find()
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id::integer=' . User::tableName() . '.id')
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName() . '.user_id=' . User::tableName() . '.id')
            ->where(['=', AuthAssignment::tableName() . '.item_name', User::ROLE_OPERATOR])
            ->orderBy(['user.id' => SORT_ASC]);

        if (is_array($country_ids) && !empty($country_ids)) {
            $operators->andWhere(['in', UserCountry::tableName() . '.country_id', $country_ids]);
        }

        if ($is_primary == 0) {
            $operators->andWhere([UserCountry::tableName() . '.country_id' => is_numeric($secondary_country_id) ? $secondary_country_id : null]);
        }

        $attachedOperators = [];

        if (isset($queue_id) && is_numeric($queue_id)) {
            $attachedOperators = QueueUser::findAll(['queue_id' => $queue_id]);
        }

        $data = [
            'operators' => ArrayHelper::map($operators->all(), 'id', 'username'),
            'attachedOperators' => ArrayHelper::getColumn($attachedOperators, 'user_id')
        ];

        return !isset($post['json']) ? $data : json_encode($data, JSON_UNESCAPED_UNICODE);
        //}
    }

    /**
     * @return string
     */
    public function actionGetQueuesByParams()
    {
        $post = Yii::$app->request->post();

        $is_primary = isset($post['is_primary']) ? (int)$post['is_primary'] : 0;
        $secondary_country_id = isset($post['secondary_country_id']) ? $post['secondary_country_id'] : null;

        $country_id = ($is_primary == 0) ? $secondary_country_id : $post['country_id'];
        $queues = Queue::getQueuesByPartnerOrCountry($is_primary == 1 ? $post['partner_id'] : null, $country_id);

        $queuesData = ArrayHelper::map($queues, 'id', 'name');

        $select2items = [];
        $select2items = [0 => ['id' => '', 'text' => '-']];

        foreach ($queuesData as $k => $v) {
            $select2items[] = [
                'id' => $k,
                'text' => $v
            ];
        }

        return json_encode($select2items, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Функция вернет готовый html для формирования json-стратегии
     * @return \yii\console\Response|Response
     */
    public function actionGetStrategy()
    {
        $numb = (int)Yii::$app->request->post('numb');

        $widget = Strategy::widget([
            'numb' => $numb,
        ]);

        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'result' => 'ok',
            'data' => $widget
        ];

        return $response;

    }

    /**
     * @param $id
     * @return Response
     */
    public function actionClear($id)
    {
        $model = $this->findModel($id);
        Yii::$app->amqp->queuePurge($model->name);
        Yii::$app->notifier->addNotifier(Yii::t('common', 'Очередь успешно очищена.'), 'success');

        return $this->redirect(Url::toRoute('index'));
    }


    /**
     * @param $queue
     * @return array
     */
    public function getSimilarQueues($queue)
    {
        return Queue::getSimilarQueues($queue);
    }

    /**
     * Обновить очередь у указанных заказов
     * @param array Order $orders
     * @param $queue_id
     * @return array
     */
    public function updateQueueOnOrder($orders, $queue_id)
    {
        foreach ($orders as $order) {
            $order->last_queue_id = $queue_id;

            if (!$order->save(false)) {
                return [
                    'success' => false,
                    'message' => json_encode($order->getErrors, JSON_UNESCAPED_UNICODE)
                ];
            }
        }

        return [
            'success' => true
        ];
    }


    /**
     * Найти все заказы  в очереди
     * @param $id
     * @return Order[]
     */
    public function getOrderByQueueId($id)
    {
        $blockedOrders = (new Query())->select('order_id')->from('{{%queue_order}}')->all();
        $blockedOrders = ArrayHelper::getColumn($blockedOrders, 'order_id');

        $orders = Order::find()
            ->where(['last_queue_id' => $id])
            ->andWhere(['not in', Order::tableName() . '.id', $blockedOrders])
            ->all();

        return $orders;
    }

    /**
     * Сменить очередь у пользователей
     * @param $before_queue_id
     * @param $after_queue_id
     * @return array
     */
    public function changeQueueUser($before_queue_id, $after_queue_id)
    {
        $queue_users = QueueUser::findAll(['queue_id' => $before_queue_id]);

        foreach ($queue_users as $user) {
            $user->queue_id = $after_queue_id;

            if (!$user->save(false)) {
                return [
                    'success' => false,
                    'message' => json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE)
                ];
            }
        }

        return [
            'success' => true
        ];
    }

    /**
     * Удалить очередь из RabbitMQ
     * @param $queue_name
     * @return bool
     */
    public function deleteRabbitQueue($queue_name)
    {
        $amqp = yii::$app->amqp;

        if (!$amqp->queueDelete($queue_name)) {
            return false;
        }

        return true;
    }

    /**
     * Создание очереди в RabbitMQ
     * @param $queue
     */
    public function createQueueRabbit($queue)
    {
        $queueName = $queue->name;
        $messages = Queue::build($queue);

        Yii::$app->amqp->declareQueue($queueName);
        Yii::$app->amqp->bindQueueExchanger($queueName, Queue::EXCHANGE, $routingKey = $queueName);
        Yii::$app->amqp->queuePurge($queueName);

        $count = count($messages);

        if ($count > 0) {
            $item = 0;
            foreach ($messages as $orderId) {
                $item++;
                Queue::publisher($queue, $orderId);
            }
        }
    }

    /**
     * Поиск подходящих на замену очередей (первичных и вторичных)
     * @return string
     */
    public function actionGetSimilarQueues()
    {
        $targetQueue = Queue::findOne(['id' => yii::$app->request->post('id')]);

        if (!is_null($targetQueue)) {
            $queues = Queue::getSimilarQueues($targetQueue, Queue::SIMILAR_TYPE_ALL);

            $result = [
                'success' => true,
                'message' => $queues
            ];
        } else {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Исходная очередь №{id} не найдена', ['id' => yii::$app->request->post('id')])
            ];
        }
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Метод смены очереди, с удалением исходной
     * По сути - происходит следующие
     * 1. все заказы переносятся в указанную очередь путём смены last_queue_id (блокированные заказы не трогаются)
     * @return string
     */
    public function actionChangeQueue()
    {
        $queue_id = yii::$app->request->post('queue_id');
        $target_queue_id = yii::$app->request->post('target_queue_id');

        //Если есть на что сменить
        if (is_numeric($queue_id)) {
            $this->moveQueueOrders($target_queue_id, $queue_id);
        }

        $targetQueue = Queue::find()->where(['id' => $target_queue_id])->one();

        if ($targetQueue) {
            $delete = $this->removeQueue($targetQueue);


            if (!$delete['success']) {
                $result = [
                    'success' => false,
                    'message' => $delete['message']
                ];
            } else {
                $result = [
                    'success' => true,
                    'message' => yii::t('common', 'Очередь удалена, заказы перемещены. Обновление списка очередей ...')
                ];
            }
        } else {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Очередь #{id} не найдена', ['id' => $target_queue_id])
            ];
        }


        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param Queue $targetQueue
     * @return array
     */
    public function removeQueue($targetQueue)
    {
        //удалить саму очередь
        if ($targetQueue->delete()) {
            $result = [
                'success' => true,
                'message' => yii::t('common', 'Очередь №{id} удалена', ['id' => $targetQueue->id]),
                'queue_name' => $targetQueue->name
            ];
        } else {
            $result = [
                'success' => true,
                'message' => yii::t('common', 'Очередь №{id} не удалена: {error}', [
                    'id' => $targetQueue->id,
                    'error' => $targetQueue->getErrors()
                ]),
            ];
        }


        //очищаем очередь
        if ($this->deleteRabbitQueue($result['queue_name'])) {
            $this->createQueueRabbit($targetQueue);
        }

        $result = [
            'success' => true,
            'message' => yii::t('common', 'Очередь удалена. Обновление списка очередей ...')
        ];

        return $result;

    }

    public function moveQueueOrders($target_queue_id, $queue_id)
    {
        $transaction = yii::$app->db->beginTransaction();
        $orders = $this->getOrderByQueueId($target_queue_id);
        $updateLastQueue = $this->updateQueueOnOrder($orders, $queue_id);
        $result = [
            'success' => false
        ];
        //обновить очередь у не заблокированных заказов
        if (!$updateLastQueue['success']) {
            $result = [
                'success' => false,
                'message' => $updateLastQueue['message']
            ];
        } else {
            //обновить очередь у пользователей
            $changeQueueUser = $this->changeQueueUser($target_queue_id, $queue_id);
            if (!$changeQueueUser['success']) {
                $result = [
                    'success' => false,
                    'message' => $changeQueueUser['message']
                ];
            }
        }

        if ($result['success']) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
            return json_encode($result, JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * @return array
     */
    public function actionGetSubStatusesByStatus()
    {
        $status_id = yii::$app->request->post('status_id');

        switch ($status_id) {
            case Order::STATUS_REJECTED:
                $sub_statuses = Order::getRejectReasonCollection();
                break;
            case Order::STATUS_TRASH:
                $sub_statuses = Order::getTrashReasonCollection();
                break;

            default:
                $sub_statuses = [];
                break;
        }

        return json_encode($sub_statuses, 256);
    }

}

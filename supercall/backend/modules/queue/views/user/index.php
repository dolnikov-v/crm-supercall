<?php

use backend\modules\queue\models\Queue;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;


/* @var $this yii\web\View */
/* @var $searchModel backend\modules\queue\models\search\QueueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Настройка доступа к очереди');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Настройка доступа к очереди'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $searchModel,
        'users' => $users,
        'teams' => $teams,
        'queues' => $queues,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с доступами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ',
            ];
        },
        'columns' => [
            [
                'class' => \yii\grid\SerialColumn::className(),
            ],
            [
                'attribute' => 'user_id',
                'content' => function ($model) {
                    if ($model->user)
                        return $model->user->username;
                },
                'enableSorting' => true,
            ],
            [
                'attribute' => 'queue_ids',
                'content' => function ($model) {
                    if ($model->queue_ids && !empty($model->queue_ids[0]))
                        return $model->getQueuesList();
                },
                'enableSorting' => true,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'user_id' => $data->user_id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('queue.user.edit');
                        }
                    ],
//                    [
//                        'can' => function () {
//                            return (Yii::$app->user->can('queue.user.edit')
//                                 && Yii::$app->user->can('queue.user.delete'));
//                        },
//                    ],
//                    [
//                        'label' => Yii::t('common', 'Удалить доступ'),
//                        'url' => function () {
//                            return '#';
//                        },
//                        'style' => 'confirm-delete-link',
//                        'attributes' => function ($model) {
//                            return [
//                                'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
//                            ];
//                        },
//                        'can' => function () {
//                            return Yii::$app->user->can('queue.user.delete');
//                        },
//                    ],
                ]
            ],

            

        ],
    ]),
    'footer' => (Yii::$app->user->can('queue.user.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить доступ'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

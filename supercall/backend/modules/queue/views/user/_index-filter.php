<?php
use common\helpers\Collection;
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\modules\partner\models\search\PartnerProductSearch $modelSearch */
/** @var array $partners */
/** @var array $products */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute('index'), 'method' => 'get']); ?>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'team_id')->select2List($teams, [
                'multiple' => true
            ])->label( Yii::t('common', 'Команда') ); ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'user_id')->select2List($users, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'queue_id')->select2List($queues, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php $this->registerCss("
    .select2-search--hide {
        display: block !important; 
    }
") ?>
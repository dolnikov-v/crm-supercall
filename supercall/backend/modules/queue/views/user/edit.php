<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\Partner $model */
/** @var array @$sers */
/** @var integer $user_id */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление доступа Пользователя к Очереди') : Yii::t('common', 'Редактирование доступа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Очереди'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/queue/user/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Доступы'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'users' => $users,
        'user_id' => $user_id
    ])
]) ?>

<?php

use backend\modules\queue\models\Queue;
use common\models\User;
use common\widgets\ActiveForm;
use yii\helpers\{
    ArrayHelper, Url
};

/** @var array @$sers */
/** @var integer $user_id */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'user_id')->select2List($users, [
                'prompt' => '—',
                'disabled' => true,
                'value' => $user_id
            ]); ?>

            <?= $form->field($model, 'user_id')->hiddenInput(['value' => $user_id])->label(false); ?>
        </div>
        <div class="col-lg-8">
            <?= $form->field($model, 'queue_ids')->select2List(Queue::find()->active()->collection(), [
                'multiple' => true,

            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить доступ') : Yii::t('common', 'Сохранить доступ')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php $this->registerCss("
    .select2-search--hide {
        display: block !important; 
    }
") ?>
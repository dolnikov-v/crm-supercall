<?php
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/** @var \backend\modules\queue\models\search\QueueSearch $modelSearch */
/** @var array $partners */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-4 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'name')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'partner_id')->select2List($partners, [
            'prompt' => '-'
        ]); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'country_id')->select2List($countries, [
            'prompt' => '-'
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php $this->registerCss("
    .select2-search--hide {
        display: block !important; 
    }
") ?>



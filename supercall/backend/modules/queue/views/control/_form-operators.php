<?php
use yii\helpers\Html;
use backend\modules\queue\assets\QueueUsersAsset;

/** @var [] $operators */
/** @var [] $attachedOperators */
/** @var integer $queue_id */

QueueUsersAsset::register($this);
?>

<div class="row">
    <?= Html::input('hidden','queue_id', $queue_id);?>
    <div class="col-lg-1"><b><?= yii::t('common', 'Прикрепление'); ?></b></div>
    <div class="col-lg-1"><b><?= yii::t('common', 'Идентификатор'); ?></b></div>
    <div class="col-lg-10"><b><?= yii::t('common', 'Имя пользователя'); ?></b></div>
</div>
<br/>
<div id="attached_users">
    <?php if(empty($operators)) :?>
        <?=yii::t('common', 'Нет доступных операторов');?>
    <?php else:?>

    <?php foreach ($operators as $id => $username): ?>
        <div class="row">

            <div class="col-lg-1"><?= Html::checkbox('attach[' . $id . ']', in_array($id, $attachedOperators), [
                    'label' => '',
                    'data-queue_id' => $queue_id,
                    'data-user_id' => $id
                ]); ?></div>
            <div class="col-lg-1"><?= $id; ?></div>
            <div class="col-lg-10"><?= $username; ?></div>

        </div>
    <?php endforeach; ?>

    <?php endif;?>
</div>

<?php

use backend\assets\modules\queue\QueueAsset;
use backend\modules\queue\assets\QueuePartnerAsset;
use backend\modules\queue\models\Queue;
use backend\modules\queue\widgets\Strategy;
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\modules\order\models\Order;
use backend\modules\queue\assets\QueueEventsAsset;

/** @var Queue $model */
/** @var [] $operators */
/** @var [] $attachedOperators */
/** @var integer $queue_id */
/** @var [] $queue_list */

QueueAsset::register($this);
QueueEventsAsset::register($this);
QueuePartnerAsset::register($this);

$subStatusesEvent = [];

if (isset($model->event['changeStatus']) && in_array($model->event['changeStatus'], [Order::STATUS_TRASH, Order::STATUS_REJECTED])) {
    switch ($model->event['changeStatus']) {
        case Order::STATUS_TRASH:
            $subStatusesEvent = Order::getTrashReasonCollection();
            break;
        case Order::STATUS_REJECTED;
            $subStatusesEvent = Order::getRejectReasonCollection();
            break;
        default:
            $subStatusesEvent = [];
            break;
    }
}

?>

<br/>
<fieldset <?= $editable ? '' : 'disabled'; ?>>
    <div class="row">
        <div class="col-lg-12">

            <?= $form->field($model, 'is_primary')->checkbox(['checked' => $model->is_primary, 'value' => 1, 'unchecked_value' => 0]); ?>
            <div id="box-is_primary" class="row" style="display: <?= $model->is_primary ? '' : 'none' ?>">
                <div class="col-lg-12">
                    <div class="alert alert-warning" style="margin-bottom: 20px">
                        <div class="row">
                            <div class="col-lg-3">
                                <?= $form->field($model, 'primary_partner')->select2List($primary['partner']); ?>
                            </div>
                            <div class="col-lg-3">
                                <?= $form->field($model, 'primary_country')->select2List($primary['country'], [
                                    'multiple' => true
                                ]); ?>
                            </div>
                            <div class="col-lg-3">
                                <?= $form->field($model, 'primary_type')->select2List($primary['type']); ?>
                            </div>
                            <div class="col-lg-3">
                                <?= $form->field($model, 'primary_product')->select2List($primary['product'], [
                                    'multiple' => true
                                ]); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'priority')->textInput(['placeholder' => Yii::t('common', 'Укажите цифрой')]) ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'priority_selection')->select2List(Queue::getPrioritySelection(), ['prompt' => '—']); ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'partner_id')->select2List(ArrayHelper::map(yii::$app->user->identity->partners, 'id', 'name'), ['prompt' => '—']); ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'country_id')->select2List($countries, ['prompt' => '—']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 hidden">
            <?= $form->field($model, 'mode_auto')->checkbox(['checked' => $model->mode_auto, 'value' => 1, 'unchecked_value' => 0]); ?>
        </div>
        <div class="col-lg-8">
            <?= $form->field($model, 'description')->textarea() ?>
        </div>
    </div>

    <div id="block-strategy" class="alert alert-warning">
        <div class="original-strategy">
            <?php
            // для новых и если стратегия у очереди не заполнена
            if ($model->isNewRecord || empty($model->strategy)):
                echo Strategy::widget([
                    'numb' => 0,
                    'label' => true,
                ]);
            endif; ?>
        </div>
        <div class="copy-strategy">
            <?php
            // для старых и тех, у кого Стратегия непустая
            if (!$model->isNewRecord && !empty($model->strategy)):
                $item = 0;

                //TODO проблема в том, что в $model->strategy разные типы данных JSON а второй сразу array
                $strategy = is_array($model->strategy) ? $model->strategy : json_decode($model->strategy, 1);

                foreach ($strategy as $key => $value):
                    echo Strategy::widget([
                        'key' => $key,
                        'value' => $value,
                        'numb' => $item,
                        'label' => ($item == 0) ? true : false,
                    ]);
                    $item++;
                endforeach;
            endif; ?>
        </div>

        <div class="row" style="display:none">
            <div class="col-lg-4">
                <?php
                // Данную кнопку добавить для возможности настройки работы очереди с несколькими статусами заказа
                // echo Html::a('+ Добавить', null, ['class' => 'btn-add-strategy btn btn-default btn-sm', 'onclick' => 'addStrategy(1)']) ?>
            </div>
        </div>
    </div>

    <br/>
    <div class="row">
        <div class="col-lg-4">
            <?= Html::checkbox('Events[is_event]', isset($model->event['eventType']) ? 1 : 0, ['unchecked_value' => 0, 'id' => 'is_event']); ?>
            <?= Html::label(Yii::t('common', 'Использовать триггеры'), 'is_event') ?>
            <?= $form->field($model, 'event')->select2List(Queue::getEventsCollection(), [
                'prompt' => '-',
                'name' => 'Events[eventType]',
                'disabled' => isset($model->event['eventType']) ? false : true
            ])->label(false); ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'eventCount')->textInput([
                'placeholder' => Yii::t('common', 'Кол-во срабатываний'),
                'value' => isset($model->event['eventCount']) ? $model->event['eventCount'] : '',
                'name' => 'Events[eventCount]',
                'disabled' => isset($model->event['eventType']) ? false : true
            ])->label(Yii::t('common', 'Кол-во срабатываний')) ?>
        </div>
    </div>
    <hr/>

    <div class="row">
        <div class="col-lg-3">
            <?= Html::label(Yii::t('common', 'Сменить статус')); ?>
            <?= $form->field($model, 'event')->select2List(Order::getFinalStatusesForQueueStrategyCollection(), [
                'prompt' => '-',
                'id' => 'event_change_status',
                'name' => 'Events[eventChangeStatus]',
                'value' => isset($model->event['changeStatus']) ? $model->event['changeStatus'] : '',
                'disabled' => isset($model->event['eventType']) ? false : true
            ])->label(false); ?>
        </div>
        <div class="col-lg-3">
            <?= Html::label(Yii::t('common', 'Sub status')); ?>
            <?= $form->field($model, 'event')->select2List($subStatusesEvent, [
                'prompt' => '-',
                'id' => 'event_change_sub_status',
                'name' => 'Events[eventChangeSubStatus]',
                'value' => isset($model->event['changeSubStatus']) ? $model->event['changeSubStatus'] : '',
                'disabled' => isset($model->event['eventType']) ? false : true
            ])->label(false); ?>
        </div>
        <div class="col-lg-1"><br/>- <?= Yii::t('common', 'или'); ?> -</div>
        <div class="col-lg-5">
            <?= Html::label(Yii::t('common', 'Сменить очередь')); ?>
            <?= Html::hiddenInput('queue_event', isset($model->event['changeQueue']) ? $model->event['changeQueue'] : null); ?>
            <?= $form->field($model, 'event')->select2List($queue_list, [
                'prompt' => '-',
                'name' => 'Events[eventChangeQueue]',
                'id' => 'event_change_queue',
                'value' => isset($model->event['changeQueue']) ? $model->event['changeQueue'] : '',
            ])->label(false); ?>
        </div>
    </div>
</fieldset>

<hr/>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить очередь') : Yii::t('common', 'Сохранить очередь')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<hr/>

<div class="row <?= $model->isNewRecord ? 'hidden' : '' ?>">
    <div class="col-lg-12">
        <fieldset>
            <legend><h4><?= Yii::t('common', 'Назначенные операторы') ?></h4></legend>
            <?= $this->render('_form-operators', [
                'operators' => $operators,
                'attachedOperators' => $attachedOperators,
                'queue_id' => $queue_id
            ]); ?>
        </fieldset>
    </div>
</div>


<?php $this->registerJs("
    $(function () {
        initSelect2()
    });

"); ?>

<?php $this->registerCss("
    .select2-search--hide {
        display: block !important; 
    }
") ?>

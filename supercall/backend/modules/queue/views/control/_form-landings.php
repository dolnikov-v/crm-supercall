<?php
/** @var \backend\modules\queue\models\Queue $model */

use common\widgets\base\Button;
use common\widgets\base\GlyphIcon;
use backend\modules\queue\assets\QueueLandingAsset;
use yii\helpers\Url;

QueueLandingAsset::register($this);

/** @var \common\widgets\ActiveForm */
?>

<br/>

<div class="landings">
    <fieldset <?= $editable ? '' : 'disabled'; ?>>
        <?php if (count($model->queueLandings) === 0) : ?>


            <div class="row landing">
                <div class="col-lg-4">
                    <?= $form->field($model, 'url_landing')->textInput([
                        'name' => 'Queue[url_landing][]'
                    ])->label(false) ?>
                </div>
                <div class="col-lg-3 padding-40">
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_SUCCESS . ' add-landing dropdown-toggle',
                            'icon' => GlyphIcon::PLUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>
                    </div>
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_DANGER . ' remove-landing dropdown-toggle',
                            'icon' => GlyphIcon::MINUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>
                    </div>
                </div>
            </div>


        <?php else: ?>

            <?php foreach ($model->queueLandings as $landing) : ?>

                <div class="row landing">
                    <div class="col-lg-4">
                        <?= $form->field($model, 'url_landing')->textInput([
                            'name' => 'Queue[url_landing][]',
                            'value' => $landing->url_landing
                        ])->label(false) ?>
                    </div>
                    <div class="col-lg-3 padding-40">
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_SUCCESS . ' add-landing dropdown-toggle',
                                'icon' => GlyphIcon::PLUS,
                                'attributes' => ['data-toggle' => 'dropdown']
                            ]) ?>
                        </div>
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_DANGER . ' remove-landing dropdown-toggle',
                                'icon' => GlyphIcon::MINUS,
                                'attributes' => ['data-toggle' => 'dropdown']
                            ]) ?>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>

        <?php endif; ?>
    </fieldset>
</div>
<hr/>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить очередь') : Yii::t('common', 'Сохранить очередь')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>

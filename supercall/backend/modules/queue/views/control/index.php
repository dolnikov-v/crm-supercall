<?php

use backend\modules\queue\models\Queue;
use backend\modules\queue\models\search\QueueSearch;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use common\widgets\base\Select2;
use yii\bootstrap\Modal;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use backend\modules\queue\assets\DeleteQueueAsset;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var \backend\modules\queue\models\query\QueueQuery $query */
/* @var array $capacity */
/* @var array $newOrders */
/* @var array $earliestOrders */
/* @var  QueueSearch $modelSearch */
/** @var array $order_types */
/** @var array $partners */

$this->title = Yii::t('common', 'Очереди');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Очереди'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

DeleteQueueAsset::register($this);

/** @var \backend\modules\queue\models\query\QueueQuery $cloneQueryForActive */
/** @var \backend\modules\queue\models\query\QueueQuery $cloneQueryForNotActive */
?>


<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'countries' => $countries,
        'modelSearch' => $modelSearch,
        'partners' => $partners
    ])
]) ?>

<?php Pjax::begin() ?>

<?= Panel::widget([
    'title' => Yii::t('common', ''),
    'content' => Tabs::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => Yii::t('common', 'Активные ({count})', ['count' => $cloneQueryForActive->active()->count()]),
                'content' => $this->render('_tab-active', [
                    'dataProvider' => new \yii\data\ActiveDataProvider([
                        'query' => $cloneQueryForActive->active(),
                        'pagination' => [
                            'pageSize' => 20
                        ]
                    ]),
                    'order_types' => $order_types,
                    'partners' => $partners,
                    'countries' => $countries,
                    'earliestOrders' => $earliestOrders,
                    'capacity' => $capacity,
                    'newOrders' => $newOrders
                ])
            ],
            [
                'label' => Yii::t('common', 'Не активные ({count})', ['count' => $cloneQueryForNotActive->notActive()->count()]),
                'content' => $this->render('_tab-not-active', [
                    'dataProvider' => new \yii\data\ActiveDataProvider([
                        'query' => $cloneQueryForNotActive->notActive(),
                        'pagination' => [
                            'pageSize' => 20
                        ]
                    ]),
                    'order_types' => $order_types,
                    'partners' => $partners,
                    'countries' => $countries,
                    'earliestOrders' => $earliestOrders,
                    'capacity' => $capacity,
                    'newOrders' => $newOrders
                ])
            ]
        ]
    ])
]); ?>

<?php Pjax::end() ?>

<?= ModalConfirmDelete::widget() ?>

<?php Modal::begin(['id' => 'modal_similar_queues',
    'header' => '<h4>' . yii::t('common', 'Смена очереди') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Ок'), ['class' => 'btn btn-success', 'id' => 'change_queue'])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-danger', 'data-dismiss' => 'modal'])]); ?>

<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend>
                <small><?= yii::t('common', 'Первичные очереди') ?></small>
            </legend>
            <?= Select2::widget([
                'id' => 'primary_queues',
                'name' => 'primary_queues',
                'items' => [],]); ?>
        </fieldset>
    </div>
    <p>&nbsp;</p>
    <div class="col-lg-12">
        <fieldset>
            <legend>
                <small><?= yii::t('common', 'Вторичные очереди') ?></small>
            </legend>
            <?= Select2::widget([
                'id' => 'secondary_queues',
                'name' => 'secondary_queues',
                'items' => [],]); ?>
        </fieldset>
    </div>
</div>

<?php Modal::end(); ?>

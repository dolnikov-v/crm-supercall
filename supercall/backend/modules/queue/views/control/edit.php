<?php

use common\widgets\ActiveForm;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\bootstrap\Tabs;
use backend\widgets\HistoryWidget;

/** @var \yii\web\View $this */
/** @var \common\modules\partner\models\Partner $model */
/** @var [] $operators */
/** @var [] $attachedOperators */
/** @var integer $queue_id */
/** @var [] $queue_list */
/** @var bool $editable  */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление очереди') : Yii::t('common', 'Редактирование очереди');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Очереди'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/queue/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Edit queue'),
    'content' =>
        Tabs::widget([
            'items' => [
                [
                    'label' => Yii::t('common', 'Основные настройки'),
                    'content' => $this->render('_edit-form', [
                        'model' => $model,
                        'primary' => $primary,
                        'operators' => $operators,
                        'attachedOperators' => $attachedOperators,
                        'queue_id' => $queue_id,
                        'queue_list' => $queue_list,
                        'countries' => $countries,
                        'form' => $form,
                        'editable' => $editable,
                    ]),
                    'active' => true
                ],
                [
                    'label' => Yii::t('common', 'Лендинги ({count})', ['count' => count($model->queueLandings)]),
                    'content' => $this->render('_form-landings', [
                        'model' => $model,
                        'queue_id' => $queue_id,
                        'form' => $form,
                        'editable' => $editable
                    ]),
                    'active' => false
                ],
                //отключил - тяжёлые логи. включу повле переноса логов
//                [
//                    'label' => Yii::t('common', 'View edit history'),
//                    'content' => Yii::$app->user->can('queue.control.viewhistory') ? HistoryWidget::widget(['route' => Yii::$app->request->getUrl()]) : '',
//                    'active' => false,
//                    'visible' =>  Yii::$app->user->can('queue.control.viewhistory'),
//                ],
            ]
        ])
])
?>

<?php ActiveForm::end(); ?>

<?php
use backend\modules\queue\models\Queue;
use backend\modules\queue\models\search\QueueSearch;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use common\widgets\base\Select2;
use yii\bootstrap\Modal;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use backend\modules\queue\assets\DeleteQueueAsset;

/** @var array $partners */
/** @var array $order_types */
/** @var array $earliestOrders */
/** @var array $capacity */
/** @var array $newOrders */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с очередями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'data' => [
                'sortable-widget' => 1,
                'sortable-url' => \yii\helpers\Url::toRoute(['sorting']),
            ]
        ],
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
                'data-sortable-id' => $model->id,
            ];
        },
        'columns' => [
            [
                'class' => \beatep\sortable\grid\Column::className(),
            ],
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Название'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'url_landing',
                'visible' => false,
                'contentOptions' => [
                    'class' => ['width-50 text-center']
                ],
                'label' => Yii::t('common', 'Лендинги'),
                'enableSorting' => false,
                'content' => function ($model) {
                    return count($model->queueLandings);
                }
            ],
            [
                'attribute' => 'description',
                'content' => function ($model) {
                    return Html::encode($model->description);
                },
            ],
            [
                'attribute' => 'is_primary',
                'label' => Yii::t('common', 'Первичная очередь'),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->is_primary ? Queue::getStatus($model->is_primary) : '',
                        'style' => $model->is_primary ? Label::STYLE_SUCCESS : 'label',
                    ]);
                },
                'enableSorting' => true,
            ],
            [
                'attribute' => 'primary_type',
                'content' => function ($model) use ($order_types) {
                    if ($model->is_primary) {
                        $primary_components = Json::decode($model->primary_components);
                        return yii::t('common', $order_types[$primary_components['primary_type']] ?? '');
                    } else {
                        return '';
                    }
                },
            ],
            [
                'attribute' => 'partner_id',
                'label' => yii::t('common', 'Партнёр'),
                'content' => function ($model) use ($partners) {
                    if ($model->is_primary) {
                        $primary_components = Json::decode($model->primary_components);

                        return isset($partners[$primary_components['primary_partner']]) ? $partners[$primary_components['primary_partner']] : '-';
                    } else {
                        return $model->partner ? $model->partner->name : '';
                    }
                },
            ],
            [
                'attribute' => 'country_id',
                'label' => Yii::t('common', 'Страна'),
                'content' => function ($model) use ($countries) {
                    $content = '';
                    if ($model->is_primary) {
                        $primary_components = json_decode($model->primary_components, 1);
                        $primary_country = isset($primary_components['primary_country']) ? json_decode($primary_components['primary_country'], 1) : [];

                        if (is_array($primary_country) && !empty($primary_country)) {
                            $data = [];
                            foreach ($primary_country as $country) {
                                $data[] = yii::t('common', isset($countries[$country]) ? $countries[$country] : '');
                            }

                            $content = implode(', ', $data);
                        } else if (is_numeric($primary_country)) {
                            $content = yii::t('common', isset($countries[$primary_country]) ? $countries[$primary_country] : '');
                        }
                    } else {
                        $content = yii::t('common', isset($countries[$model->country_id]) ? $countries[$model->country_id] : '');
                    }

                    return $content;
                },
            ],
            [
                'attribute' => 'priority',
                'class' => \yii2mod\editable\EditableColumn::className(),
                'url' => ['change-priority'],
                'contentOptions' => function ($model) {
                    return [
                        'class' => 'text-center'
                    ];
                }
            ],
            [
                'attribute' => 'priority_selection',
                'label' => Yii::t('common', 'Приоритет выборки'),
                'content' => function ($model) {
                    return $model->priority_selection ? Queue::getPrioritySelection($model->priority_selection) : '';
                },
                'enableSorting' => true,
            ],
            [
                'attribute' => 'earliest_lead',
                'contentOptions' => function ($model) {
                    return ['class' => 'text-center'];
                },
                /** @var Queue $model */
                'content' => function ($model) use ($earliestOrders) {
                    if (isset($earliestOrders[$model->id])) {
                        return $model->getDatetimeByTimezone($earliestOrders[$model->id]);
                    }

                    return '';
                },
                'enableSorting' => true,
            ],
            [
                'attribute' => 'count',
                'label' => Yii::t('common', 'Емкость'),
                'content' => function ($model) use ($capacity) {
                    $now = !empty($capacity['now'][$model->id]) ? $capacity['now'][$model->id] : 0;
                    $hour = !empty($capacity['hour'][$model->id]) ? $capacity['hour'][$model->id] : 0;
                    return $now . ' -> ' . $hour;
                },
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => yii::t('common', 'Доступны сейчас -> доступны через 1 час'),
                ],
                'enableSorting' => true,
            ],
            [
                'attribute' => 'count',
                'label' => Yii::t('common', 'Заказы'),
                'content' => function ($model) use ($capacity, $newOrders) {
                    $all = !empty($capacity['all'][$model->id]) ? $capacity['all'][$model->id] : 0;
                    $new = isset($newOrders[$model->id]) ? $newOrders[$model->id] : 0;

                    return $all . ' | ' . $new;
                },
                'contentOptions' => function ($model) {
                    return [
                        'data-count-id' => $model->id
                    ];
                },
                'headerOptions' => [
                    'data-container' => 'body',
                    'data-toggle' => 'tooltip',
                    'title' => yii::t('common', 'Всего заказов | Необработанных заказов'),
                ],
                'enableSorting' => true,
            ],
            [
                'attribute' => 'active',
                'label' => Yii::t('common', 'Активна'),
                'headerOptions' => ['class' => 'width-50 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => Queue::getStatus($model->active),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Очистить'),
                        'url' => function ($model) {
                            return Url::toRoute(['/queue/control/clear', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('queue.control.clear');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('queue.control.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/queue/control/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('queue.control.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/queue/control/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('queue.control.deactivate') && $model->active;
                        }
                    ],
//                    [
//                        'label' => Yii::t('common', 'Удалить'),
//                        'url' => function ($model) {
//                            return Url::toRoute(['#']);
//                        },
//                        'can' => function ($model) {
//                            return Yii::$app->user->can('queue.control.delete');
//                        },
//                        'attributes' => function ($model) {
//                            return [
//                                'data-action' => 'delete',
//                                'data-id' => $model->id
//                            ];
//                        }
//                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('queue.control.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить Очередь'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

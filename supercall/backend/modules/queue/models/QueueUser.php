<?php

namespace backend\modules\queue\models;

use Yii;
use common\models\User;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%queue_user}}".
 *
 * @property integer $id
 * @property integer $queue_id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Queue $queue
 * @property User $user
 */
class QueueUser extends \yii\db\ActiveRecord
{
    public $queue_ids;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%queue_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['queue_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['queue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Queue::className(), 'targetAttribute' => ['queue_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['queue_ids'], 'safe'],
            [['team_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'queue_id'   => Yii::t('common', 'Очередь'),
            'queue_ids'  => Yii::t('common', 'Очереди'),
            'user_id'    => Yii::t('common', 'Пользователь'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата редактирования'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\queue\models\query\QueueUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\queue\models\query\QueueUserQuery(get_called_class());
    }

    public function afterFind()
    {
        parent::afterFind();
        if ($this->queue_ids) {
            $this->queue_ids = Json::decode($this->queue_ids);
        }
    }

    /**
     * выведет список очередей, для конкретного доступа
     * @return string
     */
    public function getQueuesList()
    {
        $queuesQuery = Queue::find()
            ->select(['name'])
            ->where(['in', 'id', $this->queue_ids]);

        if ($queuesQuery->exists()) {
            return implode(', ', $queuesQuery->column());
        }

        return '';
    }

}

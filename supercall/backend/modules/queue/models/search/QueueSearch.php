<?php

namespace backend\modules\queue\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\queue\models\Queue;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * QueueSearch represents the model behind the search form about `backend\modules\queue\models\Queue`.
 */
class QueueSearch extends Queue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'priority', 'priority_selection', 'active', 'created_at', 'updated_at', 'display_position'], 'integer'],
            [['name', 'description', 'strategy', 'country_id', 'partner_id'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $sort = [];
        if (empty($params['dp-1-sort'])) {
            //$sort['id'] = SORT_DESC;
            $sort['display_position'] = SORT_ASC;
            $sort['priority'] = SORT_ASC;
        }

        $query = Queue::find()->priority()
            ->select([
                Queue::tableName() . '.*',
            ])->from('(SELECT "queue".*, primary_components::jsonb->>\'primary_country\' AS "primary_country" from "queue" ORDER BY "priority") as queue');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'priority' => $this->priority,
            'priority_selection' => $this->priority_selection,
            'active' => $this->active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', new Expression('lower(name)'), mb_strtolower($this->name)])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'strategy', $this->strategy])
            ->orderBy($sort);

        if (!$this->partner_id) {
            if (!yii::$app->user->isSuperadmin) {
                //только по дрступным для юзера партнёрам

                $query->byPartner(ArrayHelper::getColumn(yii::$app->user->identity->partners, 'id'));

            }
        }

        if ($this->country_id) {
            $query->byCountry($this->country_id, false);
        }

        if ($this->partner_id) {
            $query->byPartner($this->partner_id);
        }

        $query->indexBy('id');

        return $dataProvider;
    }
}

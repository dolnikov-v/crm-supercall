<?php

namespace backend\modules\queue\models\search;

use common\models\Teams;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\queue\models\QueueUser;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * QueueUserSearch represents the model behind the search form about `backend\modules\queue\models\QueueUser`.
 */
class QueueUserSearch extends QueueUser
{
    public $team_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'queue_id', 'user_id', 'team_id', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = QueueUser::find()
            ->select([
                new Expression("array_to_json(array_agg(concat(".QueueUser::tableName().".queue_id))) as queue_ids"),
                QueueUser::tableName().'.user_id',
            ])
            ->joinWith('user')
            ->groupBy([QueueUser::tableName().'.user_id'])
            ->indexBy('user_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if($this->team_id) {
            $users = Teams::getUsers($this->team_id);

            if(is_array($users)){
                $user_ids = ArrayHelper::getColumn($users, 'id');
                $query->andFilterWhere(['user.id' => $user_ids]);
            }
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'queue_id' => $this->queue_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);


        return $dataProvider;
    }
}

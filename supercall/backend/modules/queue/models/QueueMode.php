<?php

namespace backend\modules\queue\models;
use yii\base\ErrorException;

/**
 * Class QueueMode
 * Подготовит заказы для очереди, у которой выставлен Режим=АВТО
 *
 * @package backend\modules\queue\models
 */
class QueueMode extends Queue
{
    public $queue;
    
    public static function run($queue)
    {
        $objectQueue = new QueueMode();
        $objectQueue->queue = $queue;
        
        // запускает метод для выгрузки заказов в систему обзвона
        return $objectQueue->get();
        
    }
    
    /**
     * Заглушка, здесь будет метод, который будет выгружать заказы для обзвона в систему обзвона
     * @return array
     */
    private function get()
    {
        // Какой-то функционал
//        try {
//              ....
//        } catch(\Throwable $e) {
//            $result = false;
//
//        } finally {
//            return $result;
//        }
        
        return true;
    }
    
}

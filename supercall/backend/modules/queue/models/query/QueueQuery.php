<?php

namespace backend\modules\queue\models\query;


use backend\modules\queue\models\Queue;
use common\components\db\ActiveQuery;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[\backend\modules\queue\models\Queue]].
 *
 * @see \backend\modules\queue\models\Queue
 */
class QueueQuery extends ActiveQuery
{
    /**
     * @param $type_id
     * @return $this
     */
    public function byType($type_id)
    {
        $fieldPrimaryType = new Expression("\"primary_components\"->>'primary_type'");

        return $this->andWhere(['=', $fieldPrimaryType, (string)$type_id]);
    }

    /**
     * @param $partner_id
     * @return QueueQuery
     */
    public function byPartner($partner_id)
    {
        $partner_id = is_array($partner_id) ? $partner_id : [$partner_id];

        $fieldPrimaryPartner = new Expression("\"primary_components\"->>'primary_partner'");

        $query = $this;
        $param = [];
        $param[] = 'or';

        foreach($partner_id as $partner){
            $param[] = ['partner_id' => $partner];
            $param[] = ['=', $fieldPrimaryPartner, (string)$partner];
        }

        if(count($param) > 1){
            $query->andWhere($param);
        }

        return $query;
    }

    /**
     * @param integer|array $partner_id
     * @return $this
     */
    /*public function byPartner($partner_id)
    {
        $partner_id = is_array($partner_id) ? $partner_id : [$partner_id];

        $fieldPrimaryPartner = new Expression("\"primary_components\"->>'primary_partner'");

        $query = $this;

        foreach($partner_id as $partner){
            $query
                ->andWhere(['=', $fieldPrimaryPartner, (string)$partner])->orWhere(['partner_id' => $partner])
                ->orWhere(['partner_id' => $partner]);
        }

        return $query;
    }*/

    /**
     * @param $country_id
     * @param bool $only_primary
     * @return $this|QueueQuery
     */
    public function byCountry($country_id, $only_primary = true)
    {
        $country_id = is_array($country_id) ? $country_id : [$country_id];

        $fieldPrimaryCountry = new Expression("\"primary_components\"->>'primary_country'");

        $query = $this;
        $param = [];
        $param[] = 'or';

        foreach ($country_id as $country) {
            $param[] = ['in', $fieldPrimaryCountry, [(string)$country, '["' . $country . '"]']];
        }

        if (!$only_primary) {
            foreach ($country_id as $country) {
                $param[] = ['country_id' => $country];
            }
        }

        $query->andWhere($param);

        return $query;
    }

    /**
     * Используем только активные очереди
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['active' => Queue::STATUS_ACTIVE]);
    }

    public function notActive()
    {
        return $this->andWhere(['active' => Queue::STATUS_NOT_ACTIVE]);
    }

    public function notPrimary()
    {
        return $this->andWhere(['[[is_primary]]' => false]);
    }

    public function primary()
    {
        return $this->andWhere(['[[is_primary]]' => true]);
    }

    /**
     * Фильтрует только очередит с авто Режимом
     * @return $this
     */
    public function mode()
    {
        return $this->andWhere(['[[mode_auto]]' => true]);
    }

    /**
     * Фильтрует только очереди с ручным Режимом
     * @return $this
     */
    public function notMode()
    {
        return $this->andWhere(['[[mode_auto]]' => false]);
    }

    /**
     * Определяет приоритет очереди
     * @return $this
     */
    public function priority()
    {
        return $this->orderBy([
            'priority' => SORT_ASC,
        ]);
    }

    /**
     * @return $this
     */
    public function byIdentityUser()
    {
        if(yii::$app->user->isSuperadmin) {
            return $this;

        }
        return $this
            ->byCountry(ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id'), false)
            ->byPartner(ArrayHelper::getColumn(yii::$app->user->getPartners(), 'id'));

    }

    /**
     * @inheritdoc
     * @return \backend\modules\queue\models\Queue[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\queue\models\Queue|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

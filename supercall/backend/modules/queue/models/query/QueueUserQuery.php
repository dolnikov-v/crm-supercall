<?php

namespace backend\modules\queue\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\queue\models\QueueUser]].
 *
 * @see \backend\modules\queue\models\QueueUser
 */
class QueueUserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\queue\models\QueueUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\queue\models\QueueUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

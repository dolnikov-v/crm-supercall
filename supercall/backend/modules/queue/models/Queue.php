<?php

namespace backend\modules\queue\models;

use common\models\Asterisk;
use common\models\AutoCall;
use common\models\Country;
use common\models\Timezone;
use common\models\User;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use Yii;
use common\components\db\ActiveRecord;
use common\modules\order\models\Order;
use common\modules\partner\models\PartnerSettings;
use yii\base\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use backend\modules\administration\models\ApiLog;

/**
 * This is the model class for table "{{%queue}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $priority
 * @property integer $priority_selection
 * @property integer $active
 * @property string $strategy
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $partner_id
 * @property integer $country_id
 * @property boolean $is_primary
 * @property boolean $mode_auto
 * @property jsonb $primary_components
 *
 * @property QueueUser[] $queueUsers
 */
class Queue extends ActiveRecord
{
    const STATUS_ACTIVE = 1; //Активен
    const STATUS_NOT_ACTIVE = 0; //Не активен

    const EXCHANGE = 'router';
    const MAX_LIMIT = 500;

    const MODE_AUTO = 'авто';
    const MODE_HAND = 'вручную';

    const EVENT_SCENARIO_CHANGE_STATUS = 'event_change_status';
    const EVENT_SCENARIO_CHANGE_QUEUE = 'event_change_queue';

    //events для настройки очереди при работе с заказами
    const EVENT_FAIL = Order::STATUS_FAIL;
    const EVENT_RECALL = Order::STATUS_RECALL;
    const EVENT_RECALL_FAIL = Order::STATUS_RECALL . '_' . Order::STATUS_FAIL;

    //тип при поиске похожей очереди
    const SIMILAR_TYPE_ALL = 0;
    const SIMILAR_TYPE_PRIMARY = 1;
    const SIMILAR_TYPE_SECONDARY = 2;

    const LOGGER_TYPE = 'queue_create';
    const LOGGET_TYPE_AUTOMATIC = 'queue_auto_create';

    //events - триггеры в стратегии
    public $eventType;
    public $eventCount;
    public $eventChangeStatus;
    public $eventChangeQueue;

    public $strategyStatus;
    public $eventChangeSubStatus;
    public $strategyCount;
    public $strategyPreset;
    public $blockTime;
    public $changeStatus;
    public $transferToQueue;

    // Перменные для первичной очереди
    public $primary_type;
    public $primary_country;
    public $primary_partner;
    public $primary_product;

    protected $diff;

    public $log_history = true;

    public $log;

    public $url_landing;

    /** @var  integer */
    public $earliest_lead;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%queue}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['eventChangeStatus'], 'required', 'on' => self::EVENT_SCENARIO_CHANGE_STATUS],
            [['eventChangeQueue'], 'required', 'on' => self::EVENT_SCENARIO_CHANGE_QUEUE],
            [['eventCount'], 'required', 'on' => [self::EVENT_SCENARIO_CHANGE_STATUS, self::EVENT_SCENARIO_CHANGE_QUEUE]],
            [['description', 'strategy'], 'string'],
            [['strategyStatus', 'strategyCount', 'strategyPreset', 'blockTime', 'changeStatus', 'transferToQueue'], 'integer'],
            [['priority', 'priority_selection', 'active', 'created_at', 'updated_at', 'primary_type', 'primary_partner', 'display_position'], 'integer'],
            [['is_primary', 'mode_auto'], 'boolean'],
            [['primary_components'], 'string'],
            [['country_id', 'partner_id'], 'integer'],
            [['event'], 'string'],
            [['eventCount', 'eventChangeQueue', 'eventChangeStatus'], 'integer'],
            [['primary_product', 'primary_country', 'event'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Название'),
            'description' => Yii::t('common', 'Описание'),
            'priority' => Yii::t('common', 'Приоритет'),
            'priority_selection' => Yii::t('common', 'Приоритет выборки'),
            'active' => Yii::t('common', 'Активность'),
            'strategy' => Yii::t('common', 'Стратегия'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
            'country_id' => Yii::t('common', 'Страна'),
            'partner_id' => Yii::t('common', 'Партнёр'),

            'strategyStatus' => Yii::t('common', 'Статус заказа'),
            'strategyCount' => Yii::t('common', 'Количество попыток'),
            'strategyPreset' => Yii::t('common', 'Пресет'),
            'blockTime' => Yii::t('common', 'Блокировка между попытками, в мин.'),
            'changeStatus' => Yii::t('common', 'Завершить обработку'),
            'transferToQueue' => Yii::t('common', 'Изменить очередь'),
            'is_primary' => Yii::t('common', 'Первичная очередь'),
            'primary_components' => Yii::t('common', 'Тип-партнер-страна'),
            'primary_type' => Yii::t('common', 'Тип заказа'),
            'primary_country' => Yii::t('common', 'Страны'),
            'primary_partner' => Yii::t('common', 'Партнер'),
            'primary_product' => Yii::t('common', 'Товары'),
            'mode_auto' => Yii::t('common', 'Режим авто'),
            'events' => Yii::t('common', 'Триггер'),
            'eventsCount' => Yii::t('common', 'Кол-во срабатываний триггера'),
            'earliest_lead' => Yii::t('common', 'Дата самого раннего лида')
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->is_primary) {
            $this->partner_id = null;
            $this->country_id = null;
        }

        return parent::beforeSave($insert);
    }

    public static function getEventsCollection()
    {
        return [
            self::EVENT_FAIL => Yii::t('common', 'Недозвон'),
            self::EVENT_RECALL => Yii::t('common', 'Перезвон'),
            self::EVENT_RECALL_FAIL => Yii::t('common', 'Недозвон/Перезвон'),
        ];
    }

    /**
     * Манипуляции с данными перед валидацией
     * @return bool
     */
    public function beforeValidate()
    {
        if (!empty($this->strategy))
            $this->strategy = is_array($this->strategy) ? Json::encode($this->strategy) : $this->strategy;

        if (!empty($this->is_primary)) {
            $this->primary_components = Json::encode([
                'primary_type' => $this->primary_type,
                'primary_partner' => $this->primary_partner,
                'primary_country' => is_array($this->primary_country)
                    ? Json::encode($this->primary_country, true)
                    : $this->primary_country,
                'primary_product' => is_array($this->primary_product)
                    ? Json::encode($this->primary_product, true)
                    : '',
            ]);
        } else {
            $this->primary_components = null;
        }

        return parent::beforeValidate();
    }

    /**
     * Манипуляции с данными перед выводом
     */
    public function afterFind()
    {
        if (!empty($this->event)) {
            $this->event = Json::decode($this->event);
        }

        if (!empty($this->strategy))
            $this->strategy = Json::decode($this->strategy);

        if (!empty($this->primary_components)) {
            $primary_components = is_array($this->primary_components) ? $this->primary_components : Json::decode($this->primary_components, true);
            $this->primary_type = $primary_components['primary_type'];
            $this->primary_country = $primary_components['primary_country'];
            $this->primary_partner = $primary_components['primary_partner'];
            $this->primary_product = $primary_components['primary_product'];
        }

        if (!empty($this->primary_country)) {
            $this->primary_country = Json::decode($this->primary_country, true);
        }
        if (!empty($this->primary_product)) {
            $this->primary_product = Json::decode($this->primary_product, true);
        }

        parent::afterFind();
    }

    /**
     * Функция принимает данные Стратегии POST и формирует массив перед сохранением в БД
     * @param $post
     * @return array
     */
    public static function transformStrategy($post)
    {
        if (empty($post))
            return null;

        $strategy = [];
        foreach ($post as $value) {
            // сейчас только для недозвона
            $status = Order::STATUS_FAIL;

            // Если выбрано числовое значение кол-ва попыток
            if (!empty($value['strategyCount']))
                $strategy[$status]['strategyCount'] = (int)$value['strategyCount'];

            // Если выбран пресет
            if (!empty($value['strategyPreset']))
                $strategy[$status]['strategyPreset'] = (int)$value['strategyPreset'];

            if (!empty($value['blockTime']))
                $strategy[$status]['blockTime'] = (int)$value['blockTime'];

            if (!empty($value['changeStatus']))
                $strategy[$status]['changeStatus'] = (int)$value['changeStatus'];

            if (!empty($value['changeSubStatusFromStrategy']))
                $strategy[$status]['changeSubStatusFromStrategy'] = (int)$value['changeSubStatusFromStrategy'];

            if (!empty($value['transferToQueue']))
                $strategy[$status]['transferToQueue'] = (int)$value['transferToQueue'];

        }

        if (empty($strategy))
            return null;

        return $strategy;
    }

    /**
     * Список статусов Активности, либо
     * если передан $key, то конкретный статус
     * @param type $key
     * @return string
     */
    public static function getStatus($key = null)
    {
        $status = [
            self::STATUS_ACTIVE => 'Да',
            self::STATUS_NOT_ACTIVE => 'Нет',
        ];

        if (!is_null($key) && isset($status[$key])) {
            return $status[$key];
        }

        return $status;
    }

    /**
     * Возвращает массив с количеством доступных для обработки заказов сейчас
     * и спустя один час сгруппированый по очередям
     * @return array
     */
    public static function getCapacity()
    {
        // тек.время по дефолт.таймзоне с временем блокировки
        $current_time = (new \DateTime())
            ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
            ->getTimestamp();

        // Время через час
        $current_time_hour = $current_time + 60 * 60;

        $queues = Yii::$app->db->createCommand('
          SELECT count(*) AS count,last_queue_id AS queue FROM "order"
          WHERE last_queue_id  NOTNULL
          AND (blocked_to <= :time OR blocked_to ISNULL)
          AND STATUS IN (1,2,3)
          GROUP BY queue');

        //COUNT((SELECT COUNT(*) FROM "order" where status IN (1,2,3) and "order".last_queue_id = last_queue_id)) as all_count

        $queues_now = $queues->bindValue(':time', $current_time)->queryAll();
        $queues_hour = $queues->bindValue(':time', $current_time_hour)->queryAll();
        $queues_all = $queues->bindValue(':time', 2147483647)->queryAll(); //max integer value

        $capacity = ['now' => [], 'hour' => []];

        foreach ($queues_now as $queue) {
            $capacity['now'][$queue['queue']] = $queue['count'];
        }
        foreach ($queues_hour as $queue) {
            $capacity['hour'][$queue['queue']] = $queue['count'];
        }
        foreach ($queues_all as $queue) {
            $capacity['all'][$queue['queue']] = $queue['count'];
        }


        return $capacity;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQueueUsers()
    {
        return $this->hasMany(QueueUser::className(), ['queue_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnerSettings()
    {
        return $this->hasMany(PartnerSettings::className(), ['queue_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\queue\models\query\QueueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\queue\models\query\QueueQuery(get_called_class());
    }

    public static function getPrioritySelection($key = null)
    {
        $priority = [
            1 => 'свежие заказы',
            2 => 'старые заказы',
            3 => 'по прозвону',
        ];

        if (!is_null($priority) && isset($priority[$key])) {
            return $priority[$key];
        }

        return $priority;
    }

    /**
     * Функция публикует сообщение в очереди
     *
     * предварительно проверяет OrderLog и Стратегию текущей очереди
     * @param Queue $queue
     * @param $orderId
     */
    public static function publisher(self $queue, $orderId)
    {
        $queueName = $queue->id;
        // обновить поле last_queue_id в таблице Order
        if (Order::updateByParams(['last_queue_id' => $queue->id], ['id' => $orderId])) {
            // здаесь уже публикуем сообщение
            Yii::$app->amqp->publish_message($orderId, self::EXCHANGE, $routingKey = $queueName);
        } // пишем логи
        else
            Yii::error("Not updated 'last_queue_id' in the Order for id=$orderId and queueId=" . $queue->id, __METHOD__);

    }

    /**
     * Функция выдает заказ по приоритету из очереди
     * @param $id
     * @return $this
     */
    public static function getOrderByOperator($id)
    {
        $queue = QueueUser::find()
            ->select('queue_id')
            ->joinWith([
                'queue',
            ])
            ->where(['user_id' => $id, 'active' => 1])
            ->orderBy(['priority' => SORT_ASC, 'priority_selection' => SORT_ASC]);

        return $queue;
    }

    /**
     * Функция вернет значение режима
     * авто или вручную
     *
     * @param $value
     * @return string
     */
    public static function getMode($value)
    {
        if ($value)
            return self::MODE_AUTO;
        else
            return self::MODE_HAND;

    }

    /**
     * Функция возвращает ID заказов в массиве
     *
     * @param $queue
     * @return array
     */
    public static function build($queue)
    {
        $strategy = $queue->strategy;
        // Только для недозвонов
        if (!empty($strategy) && key_exists(Order::STATUS_FAIL, $strategy)) {

            // тек.время по дефолт.таймзоне с временем блокировки
            $current_time = (new \DateTime())
                ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
                ->getTimestamp();

            $orders = Order::find()
                ->select(['id'])
                ->where([
                    'last_queue_id' => $queue->id,
                ])
                ->andWhere(['in', 'status', array_keys(Order::getNotFinalStatuses())])
                ->andWhere(['not in', 'status', array_keys(Order::getFinalStatusesCollection())])
                // либо blocked_to is null, либо blocked_to <= $current_time
                ->andWhere([
                    'or',
                    ['is', 'blocked_to', null],
                    ['and',
                        ['is not', 'blocked_to', null],
                        ['<=', 'blocked_to', $current_time],
                    ]
                ])
                ->limit(self::MAX_LIMIT);

            if ($queue->priority_selection) {
                $orders->byPrioritySelection($queue->priority_selection);
            }

            return $orders->column();
        }

        return [];
    }

    public static function getQueuesByPartnerOrCountry($partner_id = null, $country_id)
    {
        //Ставим по умолчанию первого парнера, в нашем случае 2WTrade
        if (is_null($partner_id)) {
            $partner_id = 1;
        }

        if ($country_id) {
            $query = Queue::find()
                ->byCountry($country_id, false)
                ->byPartner($partner_id)
                ->all();
        } else {
            $query = [];
        }

        return $query;
    }

    /**
     * @param integer $partner_id
     * @param [] $country_id
     * @return mixed
     */
    /*public static function getQueuesByPartnerOrCountry($partner_id = null, $country_id)
    {
        //Ставим по умолчанию первого парнера, в нашем случае 2WTrade
        if (is_null($partner_id)) {
            $partner_id = 1;
        }

        if($country_id) {
            $query = Queue::find()
                ->byCountry($country_id, false)
                ->byPartner($partner_id)
                ->all();
        }else{
            $query = [];
        }

        //баг у createCommand или у prepare - если в массиве 1 элемент валит с ошибкой yii\db\Exception' with message 'Undefined offset: 1
        /*if(count($country_id) == 1){
            $country_id[1] = 0;
        }*/

    /*$secondary_country_id = $country_id;

    //$secondary_country_id = array_map('intval',$secondary_country_id);
    //primary_components->>'primary_country' a.k.a "["1","7","13","11"]"
    $country_id = array_map(function ($v) {
        return '"' . (int)$v . '"';
    }, (is_array($country_id) ? $country_id : [$country_id]));

    $queues = Yii::$app->db->createCommand("SELECT id, name FROM queue WHERE active = :active AND (((primary_components @> :partner_id) OR primary_components ISNULL) AND ( (primary_components->>'primary_country' ~  :country_id "
        //если очередь вторичная - у неё страна задаётся в country_id таблицы queue
        . (') OR country_id = :secondary_country_id ) )'))
        ->bindValues([
            ':partner_id' => json_encode(['primary_partner' => (string)$partner_id]),
            ':country_id' => '(' . implode("|", $country_id) . ')',
            ':secondary_country_id' => $secondary_country_id,
            ':active' => Queue::STATUS_ACTIVE
        ]);

    $sql = $queues->getRawSql();
    $queues = Yii::$app->db->createCommand($sql);

    try {
        return $queues->queryAll();
    } catch (\Exception $e) {
        return false;
    }

}*/

    /**
     * Метод для возврата списка подходящих на замену очередей
     * Выполнен изящно ввиду того - что билдер yii не способен составлять запросы с json полями
     * @param $queue
     * @param int $type
     * @return array
     */
    public static function getSimilarQueues($queue, $type = Queue::SIMILAR_TYPE_ALL)
    {
        $queues = [
            'primary' => [],
            'secondary' => []
        ];

        $listQueues = Queue::find()->where(['<>', 'id', $queue->id])->active()->all();

        //необходимо выяснить - является ли входящая очередь первичной или вторичной
        $targetQueuePrimary = $queue->is_primary;

        $targetPartner = null;
        $targetType = null;
        $targetProduct = [];

        //если входящая очередь является первичной
        if ($targetQueuePrimary) {
            $primaryComponents = json_decode($queue->primary_components, 1);

            $targetCountries = json_decode($primaryComponents['primary_country'], 1);
            $targetPartner = $primaryComponents['primary_partner'];
            $targetType = $primaryComponents['primary_type'];
            $targetProduct = json_decode($primaryComponents['primary_product'], 1);

            if (!is_array($targetCountries)) {
                $targetCountries = [];
            }

            if (is_null($targetProduct)) {
                $targetProduct = [];
            }
        } else {
            $targetCountries = $queue->country_id;
        }

        //todo проблема вочередях, сохранятся либо как массив, либо как просто integer
        if (!is_array($targetCountries)) {
            $targetCountries = [$targetCountries];
        }

        //будем искать подходящую по парметрам очередь
        foreach ($listQueues as $q) {
            //входящая очередь первичная
            if ($targetQueuePrimary) {
                //сравнение первичных очередей
                if ($q->is_primary) {
                    $compareComponents = json_decode($q->primary_components, 1);

                    $compareCountries = json_decode($compareComponents['primary_country'], 1);
                    $comparePartner = $compareComponents['primary_partner'];
                    $compareType = $compareComponents['primary_type'];
                    $compareProduct = json_decode($compareComponents['primary_product'], 1);

                    if (!is_array($compareCountries)) {
                        $compareCountries = [];
                    }

                    if (!is_array($compareProduct)) {
                        $compareProduct = [];
                    }

                    //сравнение по типу
                    if ($targetType == $compareType) {
                        //по партнёру
                        if ($targetPartner == $comparePartner) {
                            //по странам
                            if (empty(array_diff($compareCountries, $targetCountries))) {
                                //по товарам
                                if (empty(array_diff($targetProduct, $compareProduct))) {
                                    $queues['primary'][] = ['id' => $q->id, 'name' => $q->name];
                                }
                            }
                        }
                    }
                } else {
                    //проверяемая очередь вторичная
                    if (in_array($q->country_id, $targetCountries)) {
                        $queues['secondary'][] = ['id' => $q->id, 'name' => $q->name];
                    }
                }

            } else {
                //входящая очередь яляется вторичной
                //подбираемая - первичной
                if ($q->is_primary) {
                    $compareComponents = json_decode($q->primary_components, 1);
                    $compareCountries = json_decode($compareComponents['primary_country'], 1);

                    if (is_null($compareCountries)) {
                        $compareCountries = [];
                    }

                    if (!is_array($compareCountries)) {
                        $compareCountries = [$compareCountries];
                    }


                    if (in_array($targetCountries, $compareCountries)) {
                        $queues['primary'][] = ['id' => $q->id, 'name' => $q->name];
                    }
                } else {
                    //обе очереди вторичные
                    if ($targetCountries == $q->country_id) {
                        $queues['secondary'][] = ['id' => $q->id, 'name' => $q->name];
                    }
                }
            }
        }
        if ($type == Queue::SIMILAR_TYPE_PRIMARY) {
            return $queues['primary'];
        } elseif ($type == Queue::SIMILAR_TYPE_SECONDARY) {
            return $queues['secondary'];
        } else {
            return $queues;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $createQueue = $this->editOnAsterisk($this->id);
        }

        if (method_exists(yii::$app->request, 'post')) {
            $post = yii::$app->request->post('Queue');

            $langings = $post['url_landing'] ? $post['url_landing'] : [];

            $this->saveLandings($langings);
        }
    }

    /**
     * @param array $landings
     */
    public function saveLandings($landings)
    {
        if ($landings) {
            $transction = yii::$app->db->beginTransaction();
            $error = false;

            QueueLanding::deleteAll(['queue_id' => $this->id]);

            foreach ($landings as $landing) {
                if (!empty($landing)) {
                    $model = new QueueLanding([
                        'queue_id' => $this->id,
                        'url_landing' => $landing,
                    ]);

                    if (!$model->save()) {
                        $error = true;
                        $transction->rollBack();
                        yii::$app->notifier->addNotifierErrorByModel($this);
                        break;
                    }
                }
            }

            if (!$error) {
                $transction->commit();
            }

        }
    }

    /**
     * создание extentions + queue_table на сервере астериска
     * @param $id
     * @return \yii\httpclient\Response'
     */
    public function editOnAsterisk($id)
    {
        $queue_name = 'cc_' . $id;

        $client = new Client();

        $response =  $client->createRequest()
            ->setMethod('post')
            ->setUrl(AutoCall::URL_CREATE_QUEUE)
            ->setHeaders(['Authorization' => 'Basic ' .  base64_encode(\common\components\web\User::ACCESS_TYPE_CC)])
            ->setData([
                'queue_name' => $queue_name,
            ])
            ->send();

        return $response->content;

//        $params = ['action' => 'addQueue', 'queue_name' => $queue_name];
//        $answer = Asterisk::sendRequest($params);
//
//        if (!$answer || (isset($answer['status']) && $answer['status'] != 'success')) {
//            $this->log = new ApiLog();
//            $this->log->request = 'Edit queue data on Asterisk2 (id = ' . $id . ') . ' . json_encode($params, JSON_UNESCAPED_UNICODE);
//            $this->log->response = $answer;
//            $this->log->save();
//        }

    }

    //Функция не используется, операторы распределяются по подочередям в php yii auto-call/generate
    //если функционал пройдет тестирование - вообще упрать это все + использование
    public static function updateAsteriskMemberQueues($user_id, $queue_ids = [], $type = null)
    {
        if (empty($queue_ids))
            return false;

//        if (yii::$app->params['is_dev']) {
//            $prefix = yii::$app->params['AsteriskSp_dev_prefix'];
//        } else {
        $prefix = yii::$app->params['AsteriskSp_prefix'];
//        }

        $queues = [];

        foreach ($queue_ids as $queue) {
            $queues[] = $prefix . '_queue_' . $queue;
            $queueCreate = new Queue();
            $queueCreate->editOnAsterisk($queue);
        }

        $user = User::findOne(['id' => $user_id]);

        if ($user) {
            $sipData = $user->sipArray;
            $sip = $sipData['sip_login'];

            $sendData = ['action' => 'addMemberQueues', 'user_name' => $sip, 'queues' => $queues];

            if (!is_null($type)) {
                if ($type == 'unset') {
                    $sendData['action'] = 'deleteMemberQueue';
                }
                $sendData['deleteBeforeAdd'] = false;
            }

            $result = Asterisk::sendRequest($sendData);

            if (!$result) {
                $log = new ApiLog();
                $log->request = 'Update user queues on Asterisk2 (user id = ' . $user_id . ')';
                $log->response = $user_id;
                $log->save();
            }
        } else {
            $result = [
                'success' => false,
                'message' => 'user ' . $user_id . ' not found'
            ];
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQueueLandings()
    {
        return $this->hasMany(QueueLanding::className(), ['queue_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['last_queue_id' => 'id']);
    }

    /**
     * @return array|static[]
     */
    public function getCountries()
    {
        return $this->primary_country ? Country::findAll(['id' => $this->primary_country]) : [];
    }

    /* Проверка на существоваание очереди в стране по типу заказа
    * @param $country_id
    * @param $type_id
    * @param $partner_id
    * @return bool
    * @throws \yii\db\Exception
    */
    public static function checkExistsQueueByType($country_id, $type_id, $partner_id)
    {
        return Queue::find()
            ->primary()
            ->active()
            ->byCountry($country_id)
            ->byPartner($partner_id)
            ->byType($type_id)
            ->orderBy(['priority' => SORT_ASC])
            ->one();
    }

    /**
     * @param int $partner_id
     * @param int $country_id
     * @param string $type_id
     * @return bool|int
     */
    public static function createDefaultQueue(int $partner_id, int $country_id, string $type_id)
    {
        $country = Country::findOne(['id' => $country_id]);
        $typeName = OrderType::getReMappedType($type_id);

        if (!$typeName) {
            return false;
        }

        $model = new Queue();
        $model->name = 'AUTO ' . yii::t('common', $country->name, [], 'en-US') . ' ' . $typeName;
        $model->is_primary = true;
        $model->priority = 1;
        $model->description = 'The queue was created automatically';
        $model->priority_selection = 0;
        $model->active = self::STATUS_ACTIVE;
        $model->mode_auto = false;
        $model->strategy = Json::encode([3 => ['strategyCount' => 2, 'blockTime' => 60]]);
        $model->primary_type = $type_id;
        $model->primary_country = Json::encode([(string)$country_id]);
        $model->primary_product = [];
        $model->primary_partner = $partner_id;
        $model->event = '';

        $logger = yii::$app->get('logger');
        $logger->action = basename(__METHOD__);
        $logger->type = self::LOGGET_TYPE_AUTOMATIC;
        $logger->tags = [
            'partner_id' => $partner_id,
            'country_id' => $country_id,
            'type_id' => $type_id
        ];

        if (!$model->save()) {
            $logger->error = Json::encode($model->getErrors());
        } else {
            return $model->id;
        }

        $logger->save();

        return false;
    }

}

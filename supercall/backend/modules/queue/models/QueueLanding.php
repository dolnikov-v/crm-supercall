<?php

namespace backend\modules\queue\models;

use common\components\db\ActiveRecord;
use Yii;

/**
 * @property integer $id
 * @property integer $queue_id
 * @property string $url_landing
 * @property Queue $queue
 */

/**
 * Class QueueLanding
 * @package backend\modules\queue\models
 */
class QueueLanding extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%queue_landing}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['queue_id', 'url_landing'], 'required'],
            [['queue_id', 'created_at', 'updated_at'], 'integer'],
            ['url_landing', 'string']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'queue_id' => Yii::t('common', 'Очередь'),
            'url_landing' => Yii::t('common', 'URL лендинга'),
            'created_at' => Yii::t('common', 'Добавлен'),
            'updated_at' => Yii::t('common', 'Обновлен'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_id']);
    }
}
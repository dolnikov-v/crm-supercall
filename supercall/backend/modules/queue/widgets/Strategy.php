<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 2/16/17
 * Time: 12:14 AM
 */

namespace backend\modules\queue\widgets;


use backend\modules\presets\models\Presets;
use backend\modules\queue\models\Queue;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class Strategy extends Widget
{
    public $numb;
    public $key = 0; // ключ - является статусом для сохр. стратегии
    public $value = []; // массив с параметрами - для сохр. стратегии
    public $label = false;
    
    public function run()
    {
        $queue = new Queue();
        $presetsModel = Presets::find()->select(['id', 'name'])->all();
    
        return $this->render('strategy', [
            'numb'    => $this->numb,
            'key'     => $this->key,
            'value'   => $this->value,
            'label'   => $this->label,
            'queue'   => $queue,
            'presets' => ArrayHelper::map($presetsModel, 'id', 'name'),
        ]);
    }
}
<?php

use yii\helpers\Html;
use backend\modules\queue\models\Queue;
use common\modules\order\models\Order;

/** @var array $value */
$subStatusesStrategy = [];

if (isset($value['changeStatus']) && in_array($value['changeStatus'], [Order::STATUS_TRASH, Order::STATUS_REJECTED])) {
    switch ($value['changeStatus']) {
        case Order::STATUS_TRASH:
            $subStatusesStrategy = Order::getTrashReasonCollection();
            break;
        case Order::STATUS_REJECTED;
            $subStatusesStrategy = Order::getRejectReasonCollection();
            break;
        default: $subStatusesStrategy = []; break;
    }
}

//echo '<pre>' . print_r($value,1) . '</pre>'; exit;

?>

<div class="strategy-raw--<?= $numb ?>">
    <div class="row">
        <?php
        // временно закомментировать
        // по умолчанию только дя сатуса Недозвон
        if (false): ?>
            <div class="col-lg-2">
                <div class="form-group">
                    <?= $label ? Html::label($queue->getAttributeLabel('strategyStatus')) : null ?>
                    <?= Html::dropDownList("Strategy[$numb][strategyStatus]", $key ? $key : null, Order::getStatusesCollection(), [
                        'prompt' => '—',
                        'onchange' => 'strategyViewStatus()',
                        'class' => 'select2-form',
                        'style' => 'width: 100%',
                    ]); ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="col-lg-12">
            <div class="form-group">
                <!-- count -->
                <?= $label ? Html::label($queue->getAttributeLabel('strategyCount')) : null ?>
                <div class="alert alert-info alert-block-time">
                    <?= yii::t('common', 'Минимальное количество минут для поля "Блокировка между попытками, в мин." - 30'); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-5 hidden">
                        <?= Html::checkbox('Strategy[is_preset]', key_exists('strategyPreset', $value), ['unchecked_value' => 0, 'id' => 'strategy-is_preset']); ?>
                        <?= Html::label(Yii::t('common', 'Использовать пресеты'), 'strategy-is_preset') ?>
                        <?= Html::dropDownList("Strategy[$numb][strategyPreset]", key_exists('strategyPreset', $value) ? $value['strategyPreset'] : null, $presets, [
                            'class' => 'select2-form',
                            'id' => 'strategy-preset',
                            'style' => 'width: 100%',
                            'disabled' => !key_exists('strategyPreset', $value),
                        ]); ?>
                    </div>

                    <div class="col-lg-2 hidden"><br/>- <?= yii::t('common', 'или'); ?> -</div>
                    <div class="col-lg-12">

                        <?= Html::checkbox('Strategy[is_count]', key_exists('strategyCount', $value), ['unchecked_value' => 0, 'id' => 'strategy-is_count']); ?>
                        <?= Html::label(Yii::t('common', 'Указать кол-во вручную'), 'strategy-is_count') ?>
                        <?= Html::textInput("Strategy[$numb][strategyCount]", key_exists('strategyCount', $value) ? $value['strategyCount'] : null, [
                            'class' => 'form-control  ',
                            'id' => 'strategy-count',
                            'type' => 'number',
                            'disabled' => !key_exists('strategyCount', $value),
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <!-- blockTime -->
                <?= $label ? Html::label($queue->getAttributeLabel('blockTime')) : null ?>
                <?= Html::textInput("Strategy[$numb][blockTime]", key_exists('blockTime', $value) ? $value['blockTime'] : null, [
                    'class' => 'form-control  ',
                    'type' => 'number',
                ]); ?>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-5">
                    <div class="form-group">
                        <!-- changeStatus OR transferToQueue -->
                        <?= $label ? Html::label($queue->getAttributeLabel('changeStatus')) : null ?>
                        <?= Html::dropDownList("Strategy[$numb][changeStatus]", key_exists('changeStatus', $value) ? $value['changeStatus'] : null,
                            Order::getFinalStatusesForQueueStrategyCollection(), [
                                'prompt' => '—',
                                'class' => 'select2-form  form-control',
                                'id' => 'select-changeStatus',
                                'onchange' => 'strategyChangeFinal(\'changeStatus\', \'transferToQueue\')',
                                'style' => 'width: 100%',
                                'disabled' => key_exists('transferToQueue', $value),
                            ]); ?>
                    </div>
                </div>
                <!--
                <div class="col-lg-3">
                    <div class="form-group">
                        <?= $label ? Html::label($queue->getAttributeLabel('changeSubStatus')) : null ?>
                        <?= Html::dropDownList("Strategy[$numb][changeSubStatusFromStrategy]", key_exists('changeSubStatusFromStrategy'
                            , $value) ? $value['changeSubStatusFromStrategy'] : null,
                            $subStatusesStrategy, [
                                'prompt' => '—',
                                'class' => 'select2-form form-control',
                                'id' => 'select-changeSubStatusFromStrategy',
                                'style' => 'width: 100%',
                                'disabled' => key_exists('transferToQueue', $value),
                            ]); ?>
                    </div>
                </div>
                -->
                <div class="col-lg-2"><br/>- <?= yii::t('common', 'или'); ?> -</div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <?= Html::hiddenInput('queue_trigger', key_exists('transferToQueue', $value) ? $value['transferToQueue'] : null); ?>
                        <?= $label ? Html::label($queue->getAttributeLabel('transferToQueue')) : null; ?>
                        <?= Html::dropDownList("Strategy[$numb][transferToQueue]", key_exists('transferToQueue', $value) ? $value['transferToQueue'] : null,
                            Queue::find()->active()->notPrimary()->collection(), [
                                'prompt' => '—',
                                'class' => 'select2-form',
                                'id' => 'select-transferToQueue',
                                'onchange' => 'strategyChangeFinal(\'transferToQueue\', \'changeStatus\')',
                                'style' => 'width: 100%',
                                'disabled' => key_exists('changeStatus', $value)
                            ]); ?>
                    </div>
                </div>
                <!--
                <div class="col-lg-1">
                    <a href="javascript:void(0)" onclick="removeRawStrategy('<?php // echo $numb ?>')">
                        <i class="pe-7s-close" style="font-size: 28px"></i>
                    </a>
                </div>
                -->
            </div>
        </div>
    </div>

</div>
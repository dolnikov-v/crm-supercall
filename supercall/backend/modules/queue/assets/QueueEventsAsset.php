<?php
namespace backend\modules\queue\assets;
use yii\web\AssetBundle;

class QueueEventsAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/queue/';

    public $js = [
        'queue-events.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

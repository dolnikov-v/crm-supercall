<?php
namespace backend\modules\queue\assets;

use yii\web\AssetBundle;

/**
 * Class DeleteQueueAsset
 * @package backend\modules\queue\assets
 */
class DeleteQueueAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/queue/';

    public $js = [
        'delete-queue.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
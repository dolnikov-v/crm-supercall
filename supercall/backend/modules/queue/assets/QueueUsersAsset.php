<?php
namespace backend\modules\queue\assets;

use yii\web\AssetBundle;

class QueueUsersAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/queue/';

    public $js = [
        'queue-users.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
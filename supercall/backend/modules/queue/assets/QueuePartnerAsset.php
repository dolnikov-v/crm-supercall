<?php

namespace backend\modules\queue\assets;

use yii\web\AssetBundle;

/**
 * Class QueuePartnerAsset
 * @package backend\modules\queue\assets
 */
class QueuePartnerAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/queue/';

    public $js = [
        'queue-partner.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
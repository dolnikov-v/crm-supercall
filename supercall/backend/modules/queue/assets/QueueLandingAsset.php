<?php

namespace backend\modules\queue\assets;

use yii\web\AssetBundle;

/**
 * Class QueueLandingAsset
 * @package backend\modules\queue\assets
 */
class QueueLandingAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/queue/';

    public $js = [
        'queue-landing.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
<?php

use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var array $languages */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление статьи') : Yii::t('common', 'Редактирование статьи');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статьи'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/wiki/article/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статья'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'categoryList' => $categoryList,
        'languages' => $languages
    ])
]) ?>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\wiki\models\WikiArticle */

?>
<div class="wiki-article-view">

    <h1><?= Html::encode(yii::t('common', $this->title)) ?></h1>

    <p>
        <?= Html::a('<i class="pe-7s-back"></i>&nbsp;' . Yii::t('common', 'Список статей'), ['index'], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a(Yii::t('common', 'Изменить'), ['edit', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
    </p>
    <p>
        <?= $model->text ?>
    </p>

</div>

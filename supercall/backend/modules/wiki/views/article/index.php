<?php

use backend\modules\wiki\models\WikiArticle;
use common\components\grid\GridView;
use common\models\User;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use common\components\grid\DateColumn;
use common\components\grid\ActionColumn;
use yii\widgets\LinkPager;
use common\helpers\grid\DataProvider;
use yii\helpers\Url;
use yii\helpers\Html;
use common\widgets\base\ButtonLink;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\wiki\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var array $userRoles */

$this->title = Yii::t('common', 'Wiki статьи');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статьи'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

\common\assets\vendor\JqueryAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со статьями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) use ($userRoles) {
            $classText = 'text-dark';

            if($model->status == WikiArticle::STATUS_DELETED){
                $classText = 'text-danger';
            }elseif($model->status == WikiArticle::STATUS_DRAFT){
                $classText = 'text-muted';
            }

            return [
                'class' => (yii::$app->user->language->id != $model->language->id) && !yii::$app->user->isSuperadmin && !in_array(User::ROLE_SUPERVISOR, $userRoles)? 'hidden' : $classText,
                'data-sortable-id' => $model->id,
            ];
        },
        'columns' => [
            [
                'class' => \beatep\sortable\grid\Column::className(),
            ],
            [
                'attribute' => 'title',
                'content' => function ($model) {
                    return Html::a($model->title, Url::toRoute(['view', 'id' => $model->id]));
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'language_id',
                'content' => function ($model) {
                    $icon = '<span class="flag-icon ' . $model->language->icon . '"></span> ';
                    return $icon . yii::t('common', $model->language->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_by',
                'content' => function ($model) {
                    return $model->createdBy->username;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_by',
                'content' => function ($model) {
                    return $model->updatedBy->username;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотр'),
                        'url' => function ($model) {
                            return Url::toRoute(['view', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('wiki.article.index');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('wiki.article.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'В черновики'),
                        'url' => function ($model) {
                            return Url::toRoute(['draft', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('wiki.article.edit') && $model->status != WikiArticle::STATUS_DRAFT;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Опубликовать'),
                        'url' => function ($model) {
                            return Url::toRoute(['publish', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('wiki.article.edit')  && $model->status != WikiArticle::STATUS_PUBLISH  && $model->status != WikiArticle::STATUS_DELETED;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id])
                            ];
                        },
                        'url' => function($model){
                            return '#';
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('wiki.article.delete') && $model->status != WikiArticle::STATUS_DELETED;
                        }
                    ],
                ]
            ]
        ],
        'options' => [
            'data' => [
                'sortable-widget' => 1,
                'sortable-url' => Url::toRoute(['sorting']),
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('wiki.article.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить статью'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>


<?= ModalConfirmDelete::widget() ?>
    
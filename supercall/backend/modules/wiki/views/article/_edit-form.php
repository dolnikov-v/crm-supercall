<?php
use backend\modules\wiki\models\WikiArticle;
use common\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget as Redactor;

/** @var $languages */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'title')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'category_id')->select2List($categoryList, [
                'prompt' => '—',
            ]); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'language_id')->select2List($languages, [
                'prompt' => '-'
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'text')->widget(Redactor::className(), [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 200,
                    'plugins' => [
                        'fullscreen'
                    ],
                    'imageUpload' => Url::toRoute(['image-upload'])
                ]
            ]); ?>
        </div>
    </div>

    <hr/>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'status')->checkbox([
                'label' => yii::t('common', 'Сохранить как черновик'),
                'checked' => $model->status == WikiArticle::STATUS_DRAFT,
                'value' => WikiArticle::STATUS_DRAFT,
                'unchecked_value' => 1]);
            ?>
        </div>

        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить статью') : Yii::t('common', 'Сохранить статью')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
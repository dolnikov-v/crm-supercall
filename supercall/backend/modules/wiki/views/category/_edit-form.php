<?php
use common\widgets\ActiveForm;
use common\widgets\base\Button;
use common\widgets\base\GlyphIcon;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\modules\wiki\assets\WikiCategoryLanguageAsset;

/** @var  array $role */
/** @var  array $roles */
/** @var $languages */
/** @var array $translate_list */


WikiCategoryLanguageAsset::register($this);
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'title')->textInput()->label(yii::t('common', 'Заголовок (на английском языке)')) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'role')->select2List($role, [
                'value' => $roles,
                'multiple' => true,
                'placeholder' => yii::t('common', 'Все'),

            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'description')->textarea() ?>
        </div>
    </div>
    <div class="translate_messages">
        <div class="row">
            <div class="col-lg-12"><h4><?=yii::t('common', 'Добавить перевод категории');?></h4>
                <hr/>
            </div>
        </div>


        <?php if (!empty($translate_list)) : ?>

            <?php foreach ($translate_list as $language => $message): ?>

                <div class="row translate_message">

                    <div class="col-lg-6">
                        <?= $form->field($model, 'translate_message')->textInput([
                            'name' => 'translate_message[]',
                            'value' => $message
                        ])->label(false) ?>
                    </div>
                    <div class="col-lg-3">
                        <?= $form->field($model, 'translate_language')->select2List(ArrayHelper::map($languages, 'locale', 'name'), [
                            'prompt' => '-',
                            'value' => $language,
                            'name' => 'translate_language[]'
                        ])->label(false); ?>
                    </div>
                    <div class="col-lg-3">
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_SUCCESS . ' add-translate dropdown-toggle',
                                'icon' => GlyphIcon::PLUS,
                                'attributes' => ['data-toggle' => 'dropdown']
                            ]) ?>
                        </div>
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_DANGER . ' remove-translate dropdown-toggle',
                                'icon' => GlyphIcon::MINUS,
                                'attributes' => ['data-toggle' => 'dropdown']
                            ]) ?>
                        </div>

                    </div>

                </div>
            <?php endforeach; ?>

        <?php else: ?>

            <div class="row translate_message">

                <div class="col-lg-6">
                    <?= $form->field($model, 'translate_message')->textInput([
                        'name' => 'translate_message[]'
                    ])->label(false) ?>
                </div>
                <div class="col-lg-3">
                    <?= $form->field($model, 'translate_language')->select2List(ArrayHelper::map($languages, 'locale', 'name'), [
                        'prompt' => '-',
                        'name' => 'translate_language[]'
                    ])->label(false); ?>
                </div>
                <div class="col-lg-3">
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_SUCCESS . ' add-translate dropdown-toggle',
                            'icon' => GlyphIcon::PLUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>
                    </div>
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_DANGER . ' remove-translate dropdown-toggle',
                            'icon' => GlyphIcon::MINUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>
                    </div>

                </div>
            </div>
        <?php endif; ?>

    </div>

    <hr/>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить категорию') : Yii::t('common', 'Сохранить категорию')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php

use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var  array $role */
/** @var  array $roles */
/** @var array $translate_list */
/** @var array $languages */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление категории') : Yii::t('common', 'Редактирование категории');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Категории'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/wiki/category/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Категория'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'role' => $role,
        'roles' => $roles,
        'languages' => $languages,
        'translate_list' => $translate_list
    ])
]) ?>

<?php

use common\components\grid\GridView;
use common\models\User;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use common\components\grid\DateColumn;
use common\components\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;
use common\helpers\grid\DataProvider;
use yii\helpers\Url;
use common\widgets\base\ButtonLink;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\wiki\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var  array $rolesByUser */
/* @var array $rolesCollection */

$this->title = Yii::t('common', 'Wiki категории');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Категории'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

\common\assets\vendor\JqueryAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с категориями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) use ($rolesByUser) {
            $categoryRoles = ArrayHelper::getColumn($model->getRoles()->all(), 'auth_item_name');
            $class = 'hidden';

            if ($model->isOpen()) {
                $class = '';
            }else{
                foreach ($rolesByUser as $role) {
                    if (in_array($role, $categoryRoles)) {
                        $class = '';
                        break;
                    }
                }
            }

            return [
                'class' => $class,
                'data-sortable-id' => $model->id
            ];
        },
        'columns' => [
            [
                'class' => \beatep\sortable\grid\Column::className(),
            ],
            [
                'attribute' => 'title',
                'content' => function ($model) {
                    return yii::t('common', $model->title);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'role',
                'content' => function ($model) use ($rolesCollection) {
                    $roles = ArrayHelper::getColumn($model->getRoles()->all(), 'auth_item_name');
                    $content = [];

                    foreach ($roles as $role) {
                        $content[] = yii::t('common', $rolesCollection[$role]);
                    }

                    return implode(', ', $content);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_by',
                'content' => function ($model) {
                    return $model->createdBy->username;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_by',
                'content' => function ($model) {
                    return $model->updatedBy->username;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('wiki.category.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id])
                            ];
                        },
                        'url' => function ($model) {
                            return '#';
                        },
                        'can' => function () {
                            return Yii::$app->user->can('wiki.category.delete');
                        }
                    ],
                ]
            ]
        ],
        'options' => [
            'data' => [
                'sortable-widget' => 1,
                'sortable-url' => Url::toRoute(['sorting']),
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('wiki.category.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить категорию'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>


<?= ModalConfirmDelete::widget() ?>
    
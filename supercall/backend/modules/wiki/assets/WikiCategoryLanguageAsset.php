<?php
namespace backend\modules\wiki\assets;

use yii\web\AssetBundle;

class WikiCategoryLanguageAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/wiki/wiki-category-language';

    public $js = [
        'wiki-category-language.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
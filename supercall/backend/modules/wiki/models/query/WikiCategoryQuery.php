<?php

namespace backend\modules\wiki\models\query;
use backend\modules\wiki\models\WikiCategory;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[\backend\modules\wiki\models\WikiCategory]].
 *
 * @see \backend\modules\wiki\models\WikiCategory
 */
class WikiCategoryQuery extends \yii\db\ActiveQuery
{
    public $collectionKey = 'id';
    public $collectionValue = 'title';

    public function active()
    {
        return $this->andWhere('[[status]]=1');
    }

    /**
     * @inheritdoc
     * @return \backend\modules\wiki\models\WikiCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\wiki\models\WikiCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param null $translateCategory
     * @param bool $active false - all
     * @return array
     */
    public function collection($translateCategory = null, $active = true)
    {
        $all = (!$active) ? $this->all() : $this->where(['status' => WikiCategory::STATUS_ACTIVE])->all();
        $items = ArrayHelper::map($all, $this->collectionKey, $this->collectionValue);

        if (!is_null($translateCategory)) {
            foreach ($items as $key => $item) {
                if(empty($item)){
                    $items[$key] = $item;
                }else {
                    $items[$key] = Yii::t($translateCategory, $item);
                }
            }
        }

        return $items;
    }

}

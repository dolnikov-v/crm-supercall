<?php

namespace backend\modules\wiki\models\query;

/**
 * This is the ActiveQuery class for [[\backend\modules\wiki\models\WikiArticle]].
 *
 * @see \backend\modules\wiki\models\WikiArticle
 */
class WikiArticleQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    /**
     * @inheritdoc
     * @return \backend\modules\wiki\models\WikiArticle[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\wiki\models\WikiArticle|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

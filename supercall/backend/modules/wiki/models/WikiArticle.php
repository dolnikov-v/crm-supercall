<?php

namespace backend\modules\wiki\models;

use common\models\Language;
use common\models\User;
use common\components\db\ActiveRecord;
use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%wiki_article}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $text
 * @property integer $category_id
 * @property integer $sort
 * @property integer $active
 * @property integer $status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property WikiCategory $category
 */
class WikiArticle extends ActiveRecord
{
    const DEFAULT_LANGUAGE_ARTICLE = 2; //english

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    const STATUS_PUBLISH = 1;
    const STATUS_DRAFT = 2;
    const STATUS_DELETED = 3;

    /**
 * @inheritdoc
 */
    public static function tableName()
    {
        return '{{%wiki_article}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title','language_id'], 'required'],
            [['text'], 'string'],
            [['category_id', 'sort', 'active', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'title'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['title'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => WikiCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            ['language_id', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('common', 'ID'),
            'name'        => Yii::t('common', 'Name'),
            'title'       => Yii::t('common', 'Название'),
            'text'        => Yii::t('common', 'Текст статьи'),
            'category_id' => Yii::t('common', 'Категория'),
            'sort'        => Yii::t('common', 'Сортировка'),
            'active'      => Yii::t('common', 'Active'),
            'status'      => Yii::t('common', 'Статус'),
            'created_at'  => Yii::t('common', 'Время создания'),
            'created_by'  => Yii::t('common', 'Создал'),
            'updated_at'  => Yii::t('common', 'Время изменения'),
            'updated_by'  => Yii::t('common', 'Редактировал'),
            'language_id'  => Yii::t('common', 'Язык'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(WikiCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\wiki\models\query\WikiArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\wiki\models\query\WikiArticleQuery(get_called_class());
    }
    
    
    public function beforeValidate()
    {
        $this->name = Inflector::slug($this->title);
        
        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
            $this->created_by = Yii::$app->user->id;
        
        $this->updated_by = Yii::$app->user->id;
        
        return parent::beforeSave($insert);
    }
}

<?php
namespace backend\modules\wiki\models;

use common\components\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/** @var  array $role */

class WikiCategoryRole extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%wiki_category_role}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wiki_category_id', 'auth_item_name'], 'required'],
            ['wiki_category_id', 'integer'],
            ['auth_item_name', 'string'],
            [['created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWikiCategory()
    {
        return $this->hasMany(WikiCategory::className(), ['wiki_category_id' => 'id']);
    }
}
<?php

namespace backend\modules\wiki\models;

use common\models\User;
use common\components\db\ActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%wiki_category}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property integer $sort
 * @property integer $status
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property WikiArticle[] $wikiArticles
 * @property User $createdBy
 * @property User $updatedBy
 */
class WikiCategory extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    public $translate_message;
    public $translate_language;

    public function afterSave($insert, $changedAttributes)
    {

        $id = $this->id;
        $post = yii::$app->request->post();
        $roles = isset($post['WikiCategory']['role']) ? $post['WikiCategory']['role'] : [];

        if (!$insert) {
            WikiCategoryRole::deleteAll(['wiki_category_id' => $id]);
        }

        foreach ($roles as $role) {
            $model = new WikiCategoryRole();
            $model->load(['wiki_category_id' => $id, 'auth_item_name' => $role], '');
            $model->save();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /** @var  array */
    public $role;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%wiki_category}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title'], 'required'],
            [['description'], 'string'],
            [['sort', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name', 'title'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['title'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'title' => Yii::t('common', 'Заголовок'),
            'description' => Yii::t('common', 'Описание'),
            'sort' => Yii::t('common', 'Сортировка'),
            'status' => Yii::t('common', 'Статус'),
            'created_at' => Yii::t('common', 'Время создания'),
            'created_by' => Yii::t('common', 'Создал'),
            'updated_at' => Yii::t('common', 'Время изменения'),
            'updated_by' => Yii::t('common', 'Редактировал'),
            'role' => Yii::t('common', 'Роли')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWikiArticles()
    {
        return $this->hasMany(WikiArticle::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\wiki\models\query\WikiCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\wiki\models\query\WikiCategoryQuery(get_called_class());
    }

    public function beforeValidate()
    {
        $this->name = Inflector::slug($this->title);

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
            $this->created_by = Yii::$app->user->id;

        $this->updated_by = Yii::$app->user->id;

        return parent::beforeSave($insert);
    }

    public function getRoles()
    {
        return $this->hasMany(WikiCategoryRole::className(), ['wiki_category_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function isOpen()
    {
        //для суперадмина все - god mode
        $is_superadmin = yii::$app->user->isSuperadmin;
        //у статьи нет ограничений по роли
        $empty_category_roles = empty($this->getRoles()->all());
        //для супервизора все - редактор
        $is_supervisor = in_array(User::ROLE_SUPERVISOR, yii::$app->user->getRoles());

        return $empty_category_roles ? $empty_category_roles : ($is_superadmin || $is_supervisor);
    }
}

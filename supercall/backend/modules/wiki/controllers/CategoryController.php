<?php

namespace backend\modules\wiki\controllers;

use backend\models\i18n_message;
use backend\models\i18n_source_message;
use common\components\Notifier;
use common\models\Language;
use common\models\User;
use Yii;
use backend\modules\wiki\models\WikiCategory;
use backend\modules\wiki\models\search\CategorySearch;
use backend\components\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for WikiCategory model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                    'sorting' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WikiCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(yii::$app->user->identity->id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'rolesCollection' => ArrayHelper::map(User::getRoleCollection(), 'name', 'description'),
            'rolesByUser' => ($user) ? $user->getRolesNames(true) : []
        ]);
    }

    /**
     * Updates an existing WikiCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new WikiCategory();
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                $translate_messages = Yii::$app->request->post('translate_message');
                $translate_language = Yii::$app->request->post('translate_language');
                $data = array_combine($translate_language, $translate_messages);

                //add/update translate title category
                $addTranslate = $this->addTranslateCategory($model->title, $data);

                if (!$addTranslate['success']) {
                    Yii::$app->notifier->addNotifier(yii::t('common', 'Произошла ошибка при добавлении перевода категории: {error}', ['error' => $addTranslate['message']]), Notifier::TYPE_ERROR);
                }

                Yii::$app->notifier->addNotifier($model->isNewRecord ? Yii::t('common', 'Категория успешно добавлена.') : Yii::t('common', 'Категория успешно отредактирована.'), 'success');
                return $this->redirect(Url::toRoute('index'));
            } else
                Yii::$app->notifier->addNotifierErrorByModel($model);

        }

        $rolesCollection = User::getRoleCollection();
        $role = [];

        foreach ($rolesCollection as $k => $v) {
            $role[$v['name']] = yii::t('common', $v['description']);
        }

        $languagesList = Language::find()->where(['<>', 'locale', 'en-US'])->active()->all();
        $languages = [];

        foreach ($languagesList as $lng) {
            $languages[$lng->id] = [
                'id' => $lng->id,
                'locale' => $lng->locale,
                'name' => yii::t('common', $lng->name),
                'icon' => $lng->icon
            ];
        }

        $translateQuery = i18n_source_message::find()->where(['message' => $model->title]);

        if ($translateQuery->exists()) {
            $source = $translateQuery->one();
            $translates = i18n_message::findAll(['id' => $source->id]);
            $translate_list = ArrayHelper::map($translates, 'language', 'translation');
        } else {
            $translate_list = [];
        }

        return $this->render('edit', [
            'model' => $model,
            'role' => $role,
            'roles' => ArrayHelper::getColumn($model->getRoles()->all(), 'auth_item_name'),
            'languages' => $languages,
            'translate_list' => $translate_list
        ]);
    }


    public function addTranslateCategory($phrase, $data)
    {
        //по дефолту категория в eng, нужно найти фразу или добавить её для перевода
        $source = i18n_source_message::find()->where(['message' => $phrase]);
        //фраза не найдена - добавить
        if (!$source->exists()) {
            $model = new i18n_source_message();
            $model->category = 'common';
            $model->message = $phrase;
            $model->created_at = time();
            $model->updated_at = time();

            //сохранили новую фразу для перевода
            if (!$model->validate() || !$model->save()) {
                return [
                    'success' => false,
                    'message' => json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE)
                ];
            }
        } else {
            $model = $source->one();
        }

        //перевод есть
        $translateQuery = i18n_message::find()
            ->andWhere(['id' => $model->id]);

        if ($translateQuery->exists()) {
            i18n_message::deleteAll(['id' => $model->id]);
        }

        //добавить варианты переводов - если они указаны
        foreach ($data as $language => $message) {
            if(empty($language) || empty($message)){
                continue;
            }

            $translate = new i18n_message();
            $translate->id = $model->id;
            $translate->language = $language;
            $translate->translation = $message;
            $translate->created_at = time();
            $translate->updated_at = time();

            if (!$translate->validate() || !$translate->save()) {
                return [
                    'success' => false,
                    'message' => json_encode($translate->getErrors(), JSON_UNESCAPED_UNICODE)
                ];
            }
        }

        return [
            'success' => true
        ];
    }

    /**
     * Deletes an existing WikiCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = 0;

        if ($model->save(true, ['status'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Категория удалена'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the WikiCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WikiCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WikiCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Задается сортировка Категорий
     * @return bool
     */
    public function actionSorting()
    {
        // POST приходит новая сортировка
        $sorting = Yii::$app->request->post('sorting');

        foreach ($sorting as $key => $sort) {
            Yii::$app->db->createCommand()->update(WikiCategory::tableName(),
                ['sort' => $key],
                ['id' => $sort])->execute();
        }

        // @todo Добавить возможность возвращать Yii::$app->notifier->addNotifier асинхронно
        return true;

    }
}

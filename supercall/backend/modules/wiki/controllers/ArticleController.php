<?php

namespace backend\modules\wiki\controllers;

use backend\modules\wiki\models\WikiCategory;
use common\models\Language;
use common\models\User;
use vova07\imperavi\actions\UploadAction;
use Yii;
use backend\modules\wiki\models\WikiArticle;
use backend\modules\wiki\models\search\ArticleSearch;
use backend\components\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleController implements the CRUD actions for WikiArticle model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                    'sorting' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [
            /**
             * Функция для обработки изображений
             */
            'image-upload' => [
                'class' => UploadAction::className(),
                'url' => Yii::$app->getUrlManager()->createAbsoluteUrl('/resources/modules/wiki/img'),
                'path' => Yii::getAlias('@backend/web/resources/modules/wiki/img')
            ],
        ];
    }

    /**
     * Lists all WikiArticle models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(yii::$app->user->identity->id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'userRoles' => ($user) ? $user->getRolesNames(true) : []
        ]);
    }
    
    
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new WikiArticle();
        }
        
        if ( $model->load(Yii::$app->request->post()) ) {
            
            if ($model->save()) {
                Yii::$app->notifier->addNotifier($model->isNewRecord
                    ? Yii::t('common', 'Статья успешно добавлена.')
                    : Yii::t('common', 'Статья успешно отредактирована.'), 'success');
                return $this->redirect( Url::toRoute('index') );
            }
            else
                Yii::$app->notifier->addNotifierErrorByModel($model);
            
        }

        $languagesList = Language::find()->active()->all();
        $languages = [];

        foreach ($languagesList as $lng){
            $languages[$lng->id] = [
                'id' => $lng->id,
                'name' => yii::t('common', $lng->name),
                'icon' => $lng->icon
            ];
        }
        
        return $this->render('edit', [
            'model' => $model,
            'categoryList' =>WikiCategory::find()->collection('common'),
            'languages' => ArrayHelper::map($languages, 'id', 'name')
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model){
            $model->status = WikiArticle::STATUS_DELETED;

            if(!$model->save(false)){
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }

        }else{
            throw new NotFoundHttpException('Статья не найдена');
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDraft($id)
    {
        $model = $this->findModel($id);

        if($model){
            $model->status = WikiArticle::STATUS_DRAFT;

            if(!$model->save(false)){
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }

        }else{
            throw new NotFoundHttpException('Статья не найдена');
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionPublish($id)
    {
        $model = $this->findModel($id);

        if($model){
            $model->status = WikiArticle::STATUS_PUBLISH;

            if(!$model->save(false)){
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }

        }else{
            throw new NotFoundHttpException('Статья не найдена');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the WikiArticle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WikiArticle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WikiArticle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Задается сортировка Статей
     * @return bool
     */
    public function actionSorting()
    {
        // POST приходит новая сортировка
        $sorting = Yii::$app->request->post('sorting');
        
        foreach($sorting as $key => $sort) {
            Yii::$app->db->createCommand()->update( WikiArticle::tableName() ,
                ['sort' => $key],
                ['id' => $sort])->execute();
        }
        
        // @todo Добавить возможность возвращать Yii::$app->notifier->addNotifier асинхронно
        return true;
        
    }
    
    /**
     * Просмотр статьи
     *
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $this->view->title = $model->title;
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
}

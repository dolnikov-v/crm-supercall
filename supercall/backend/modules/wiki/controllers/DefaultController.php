<?php

namespace backend\modules\wiki\controllers;

use yii\web\Controller;

/**
 * Default controller for the `wiki` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->view->title = 'Wiki 2WCall';
        return $this->render('index');
    }
}

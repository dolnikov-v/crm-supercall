<?php

namespace backend\modules\widget\models;

use common\models\Country;
use backend\modules\widget\models\query\WidgetCacheQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "widget_cache".
 *
 * @property integer $id
 * @property integer $widgetuser_id
 * @property integer $country_id
 * @property string $data
 * @property integer $cached_at
 *
 * @property WidgetUser $widgetuser
 * @property Country $country
 */
class WidgetCache extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widget_cache';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['widgetuser_id', 'cached_at', 'country_id'], 'integer'],
            [['data'], 'string'],
            [['widgetuser_id'], 'exist', 'skipOnError' => true, 'targetClass' => WidgetUser::className(), 'targetAttribute' => ['widgetuser_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'widgetuser_id' => 'Widgetuser ID',
            'country_id' => 'Country ID',
            'data' => 'Data',
            'cached_at' => 'Cached At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgetuser()
    {
        return $this->hasOne(WidgetUser::className(), ['id' => 'widgetuser_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return WidgetCacheQuery
     */
    public static function find()
    {
        return new WidgetCacheQuery(get_called_class());
    }

    /**
     * @param array $data
     * @return string
     */
    public function saveData($data)
    {
        return json_encode($data);
    }

    /**
     * @return array
     */
    public function readData()
    {
        return json_decode($this->data, true);
    }


    /**
     * Прочитать закешированные данные по идентификатору  и возможно стране
     *
     * @param integer $widgetUserId
     * @param integer $countryId
     * @return array
     */
    static function readCachedData($widgetUserId, $countryId = null)
    {
        $data = [];
        $query = self::find()->byWidgetUser($widgetUserId);
        if ($countryId) {
            $query->byCountry(yii::$app->user->country->id);
        }
        $widgetCache = $query->one();
        if ($widgetCache) {
            $data = $widgetCache->readData();
        }
        return $data;
    }
}

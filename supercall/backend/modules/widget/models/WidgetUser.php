<?php

namespace backend\modules\widget\models;

use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\components\db\ActiveRecord;

/**
 * Class WidgetUser
 * @package backend\modules\widget\models
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property integer $type_id
 * @property string $options
 * @property WidgetType $type
 * @property WidgetCache $cache
 * @property User $user
 */
class WidgetUser extends ActiveRecord
{

    /**
     * @var array $_options
     */
    protected $_options;

    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_user}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'type_id', 'user_id'], 'integer'],
            [['options'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = Json::decode($this->options);
        }
        return $this->_options;
    }

    public function getOption($name, $def = 0)
    {
        return ArrayHelper::getValue($this->getOptions(), $name, $def);
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $fields = ['x', 'y', 'height', 'width'];

        foreach ($fields as $field) {
            $this->_options[$field] = intval(ArrayHelper::getValue($options, $field, ''));
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     */

    public function setOption($name, $value)
    {
        $this->_options[$name] = $value;
    }

    public function setDefaultOptions()
    {
        $this->_options = [
            'x' => 0,
            'y' => 200,
            'height' => 3,
            'width' => 5,
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->options = Json::encode($this->_options);

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(WidgetType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCache()
    {
        return $this->hasOne(WidgetCache::className(), ['widgetuser_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}

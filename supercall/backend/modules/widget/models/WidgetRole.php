<?php

namespace backend\modules\widget\models;

use common\components\db\ActiveRecord;

/**
 * Class WidgetRole
 * @package backend\modules\widget\models
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $role
 */
class WidgetRole extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_role}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['type_id'], 'integer'],
            [['role'], 'string'],
        ];
    }


}

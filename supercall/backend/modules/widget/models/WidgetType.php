<?php

namespace backend\modules\widget\models;
use common\components\db\ActiveRecord;
use Yii;

/**
 * Class WidgetType
 * @package backend\modules\widget\models
 *
 * @property string $code
 * @property string $name
 * @property integer $id
 * @property integer $status
 * @property integer $by_country
 * @property integer $no_cache
 * @property WidgetRole[] $roles
 */
class WidgetType extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const BY_COUNTRY = 1;
    const BY_ALL_COUNTRIES = null;
    const NO_CACHE = 1;

    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_type}}';
    }

    public static function getStatuses() {
        return [
            static::STATUS_INACTIVE => Yii::t('common', 'неактивный'),
            static::STATUS_ACTIVE => Yii::t('common', 'активный'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'status', 'by_country', 'no_cache'], 'integer'],
            [['name', 'code'], 'string'],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'code' => Yii::t('common', 'Код'),
            'name' => Yii::t('common', 'Название'),
            'status' => Yii::t('common', 'Статус'),
            'by_country' => Yii::t('common', 'Данные зависят от страны'),
            'no_cache' => Yii::t('common', 'Не кешируется'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles() {
        return $this->hasMany(WidgetRole::className(), ['type_id' => 'id']);
    }

}

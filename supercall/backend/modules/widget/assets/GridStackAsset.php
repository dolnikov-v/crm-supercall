<?php
namespace backend\modules\widget\assets;

use yii\web\AssetBundle;

/**
 * Class GridStackAsset
 * @package backend\modules\access\assets
 */
class GridStackAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/widgets/widget/grid-stack';

    public $js = [
        'panel.js',
    ];

    public $css = [
        'panel.css',
    ];


    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\BootstrapAsset',
    ];
}

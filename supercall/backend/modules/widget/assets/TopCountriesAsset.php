<?php
namespace backend\modules\widget\assets;

use yii\web\AssetBundle;

/**
 * Class TopCountriesAsset
 * @package backend\modules\access\assets
 */
class TopCountriesAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/modules/widget';

    public $js = [
        'top-countries.js',
    ];

    public $depends = [
        'backend\assets\SiteAsset',
    ];
}
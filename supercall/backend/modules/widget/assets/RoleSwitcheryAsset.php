<?php
namespace backend\modules\widget\assets;

use yii\web\AssetBundle;

/**
 * Class CountrySwitcheryAsset
 * @package backend\modules\access\assets
 */
class RoleSwitcheryAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/widgets/widget/role-switchery';

    public $js = [
        'role-switchery.js',
    ];

    public $depends = [
        'backend\assets\vendor\SwitcheryAsset',
    ];
}

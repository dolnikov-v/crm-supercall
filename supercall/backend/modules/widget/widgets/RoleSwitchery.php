<?php
namespace backend\modules\widget\widgets;

use common\widgets\base\Switchery;

/**
 * Class RoleSwitchery
 * @package app\modules\widget\widgets
 */
class RoleSwitchery extends Switchery
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        return $this->render('role-switchery', [
            'widget' => Switchery::widget([
                'checked' => $this->checked,
                'disabled' => $this->disabled,
                'attributes' => [
                    'data-widget' => 'role-switchery',
                ],
            ]),
        ]);
    }
}

<?php

use common\components\grid\GridView;

/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var \common\modules\order\models\Order $model */
/** @var []  $queues */
/** @var []  $products */
?>

<!-- панель фильтров рендерить не нужно, чтоб не делать лишние движения с установкой фильтров -->
<?php if (is_null(yii::$app->request->post('filter'))): ?>

    <?= $this->render('_filter-panel', [
        'model' => $model,
        'queues' => $queues,
        'products' => $products,
    ]); ?>

<?php endif; ?>

<div class="innerbody">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-striped'],
        'rowOptions' => function ($model, $key, $index, $column) {
        },
        'showHeader' => true,
        'columns' => [
            [
                'header' => Yii::t('common', 'Тип'),
                'attribute' => 'type',
                'headerOptions' => ['style' => 'text-align:center;'],
                'content' => function ($data) {
                    return yii::t('common', $data['type']);
                }
            ],
            [
                'header' => Yii::t('common', 'Количество'),
                'contentOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
                'attribute' => 'count',
                'headerOptions' => ['style' => 'text-align:center;'],
            ],
            [
                'header' => Yii::t('common', 'Процент'),
                'contentOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
                'attribute' => 'percent',
                'headerOptions' => ['style' => 'text-align:center;'],
                'content' => function ($data) {
                    return Yii::$app->formatter->format($data['percent'], ['percent', 0]);
                }
            ],
        ]
    ]); ?>

</div>

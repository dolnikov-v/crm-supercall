<?php
namespace backend\modules\widget\widgets\mainorders;

use backend\modules\queue\models\Queue;
use backend\modules\widget\widgets\CacheableWidget;
use common\models\Product;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProduct;
use yii\db\Query;
use yii\data\ArrayDataProvider;
use yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\User;

/**
 * Class MainOrders
 * @package backend\modules\widget\widgets\mainorders
 */
class MainOrders extends CacheableWidget
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    public static function typeMap()
    {
        return [
            'all' => yii::t('common', 'Все'),
            'new' => yii::t('common', 'Новые'),
            'approved' => yii::t('common', 'Аппрув'),
            'fail' => yii::t('common', 'Недозвон'),
            'recall' => yii::t('common', 'Перезвон'),
            'reject' => yii::t('common', 'Отказ'),
            'other' => yii::t('common', 'Другие'),
        ];
    }

    public function getReportOrdersData()
    {
        $orders = Order::find()
            ->select([
                'all' => new Expression('count(' . Order::tableName() . '.id)'),
                'new' => new Expression('count('.Order::tableName().'.status in(' . Order::STATUS_NEW . ') or null)'),
                'approved' => new Expression('count('.Order::tableName().'.status in(' . Order::STATUS_APPROVED . ') or null)'),
                'fail' => new Expression('count('.Order::tableName().'.status in(' . Order::STATUS_FAIL . ') or null)'),
                'recall' => new Expression('count('.Order::tableName().'.status in(' . Order::STATUS_RECALL . ') or null)'),
                'reject' => new Expression('count('.Order::tableName().'.status in(' . Order::STATUS_REJECTED . ') or null)'),
                'other' => new Expression('count('.Order::tableName().'.status not in(' . implode(",", [
                        Order::STATUS_NEW,
                        Order::STATUS_APPROVED,
                        Order::STATUS_FAIL,
                        Order::STATUS_REJECTED,
                        Order::STATUS_RECALL
                    ]) . ') or null)'),
            ]);

        //дефолтовый текущий период
        $begin_date = Yii::$app->formatter->asTimestamp(strtotime(date('Y-m-d 00:00:00')));
        $end_date = Yii::$app->formatter->asTimestamp(strtotime(date('Y-m-d 23:59:59')));

        //фильтр виджета
        if ($this->filter) {
            parse_str($this->filter, $params);

            $created_at = explode(" - ", $params['Filter']['created_at']);
            $begin_date = Yii::$app->formatter->asTimestamp($created_at[0] . ' 00:00:00');
            $end_date = Yii::$app->formatter->asTimestamp($created_at[1] . ' 23:59:59');

            $queue_id = $params['Filter']['queue_id'];
            if (!empty($queue_id)) {
                $orders->andFilterWhere(['=', Order::tableName() . '.last_queue_id', $queue_id]);
            }

            $product_id = $params['Filter']['product_id'];
            if (!empty($product_id)) {
                $orders->andFilterWhere([
                    'in',
                    Order::tableName() . '.id',
                    (new Query())
                        ->select([OrderProduct::tableName() . '.order_id'])
                        ->from(OrderProduct::tableName())
                        ->leftJoin(Order::tableName(), Order::tableName() . '.id=' . OrderProduct::tableName() . '.order_id')
                        ->where(['>=', Order::tableName() . '.created_at', $begin_date])
                        ->andWhere(['<=', Order::tableName() . '.created_at', $end_date])
                        ->andWhere(['=', OrderProduct::tableName() . '.product_id', $product_id])
                        ->groupBy(OrderProduct::tableName() . '.order_id')
                ]);
            }

        }

        $orders->andWhere(['>=', Order::tableName() . '.created_at', $begin_date]);
        $orders->andWhere(['<=', Order::tableName() . '.created_at', $end_date]);
        $orders->andWhere(['in', Order::tableName().'.country_id', ArrayHelper::getColumn(User::getAllowCountries()->all(), 'country_id')]);

        $orders->leftJoin(OrderLog::tableName(), OrderLog::tableName().'.order_id='.Order::tableName().'.id');
        $orders->leftJoin(User::tableName(), User::tableName().'.id='.OrderLog::tableName().'.user_id');

        $userModel = yii::$app->user->getUserModel();
        $users = $userModel->getCoTeamsUsers();
        if(!empty($users)){
            $user_ids = ArrayHelper::getColumn($users, 'id');
            $orders->andFilterWhere([User::tableName().'.id' => $user_ids]);
        }


        return $orders->asArray()->all();
    }

    public function buildDataProvider()
    {
        $data = $this->getReportOrdersData();

        $all = isset($data[0]['all']) ? $data[0]['all'] : 0;

        $datas = [];

        $typeMap = self::typeMap();

        foreach ($data[0] as $type => $count) {
            if ($type != 'all') {
                $datas[] = [
                    'type' => $typeMap[$type],
                    'count' => $count,
                    'percent' => $all > 0 ? $count / $all : 0
                ];
            }
        }

        return new ArrayDataProvider([
            'allModels' => $datas
        ]);
    }

    /**
     * @return string
     */
    public function run()
    {
        $listQueues = Queue::find()
            ->where(['active' => Queue::STATUS_ACTIVE])
            ->asArray()
            ->all();

        $listProducts = Product::find()
            ->asArray()
            ->all();

        return $this->render('main-orders', [
            'dataProvider' => $this->buildDataProvider(),
            'model' => new Order(),
            'queues' => ArrayHelper::map($listQueues, 'id', 'name'),
            'products' => ArrayHelper::map($listProducts, 'id', 'name')
        ]);
    }

    public function runData()
    {
        return $this->getReportOrdersData();
    }
}
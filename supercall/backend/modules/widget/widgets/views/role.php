<?php
use backend\modules\widget\widgets\RoleSwitchery;

/** @var \common\models\User $model */
/** @var array $rolesByWidget */
/** @var array $roleList */
?>
<table class="table table-striped table-hover" data-type-id="<?= $model->id ?>">
    <?php if (!empty($roleList)) : ?>
        <thead>
        <tr>
            <th><?= Yii::t('common', 'Роли пользователей') ?></th>
            <th class="text-center width-150"><?= Yii::t('common', 'Вкл/Выкл') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($roleList as $role) : ?>
            <tr>
                <td><?= Yii::t('common', $role->description) ?></td>
                <td class="text-center" data-role="<?= $role->name ?>">
                    <?= RoleSwitchery::widget([
                        'checked' => /*$model->isSuperadmin ? true :*/ in_array($role->name, $rolesByWidget),
//                        'disabled' => $model->isSuperadmin,
                    ]); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php else: ?>
        <tr>
            <td class="text-center"><?= Yii::t('common', 'Роли отсутствуют') ?></td>
        </tr>
    <?php endif ?>
</table>

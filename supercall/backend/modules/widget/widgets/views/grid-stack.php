<?php

use backend\widgets\GridStack;
use backend\modules\widget\components\Widget;
use common\widgets\base\Button;
use common\widgets\base\GlyphIcon;
use yii\helpers\Url;
use yii\helpers\Html;
//use common\widgets\base\Panel;
use backend\widgets\PanelWidget;

use backend\modules\widget\assets\GridStackAsset;

/** @var \backend\modules\widget\models\WidgetUser[] $widgets */
/** @var \backend\modules\widget\models\WidgetType[] $widgetTypes */

GridStackAsset::register($this);


$items = [];

foreach ((array)$widgets as $widget) {

    $obj = Widget::createWidgetObj($widget);

    $items[] = [
        'x' => $widget->getOption('x', 0),
        'y' => $widget->getOption('y', 0),
        'width' => $widget->getOption('width', 5),
        'height' => $widget->getOption('height', 5),
        'content' => Html::tag('div',
            PanelWidget::widget([
                'id' => $widget->type->code,
                'title' => Yii::t('common', $widget->type->name),
                'content' => Widget::widget($widget),
                'actions' => '',
                'close' => true,
                'refresh' => true,
                'export' => is_null($obj) ? null : $obj->export,
                'footer' => (($widget->cache && $widget->cache->cached_at) ? Yii::t('common', 'Данные от') . ' '
                    . Yii::$app->formatter->asDatetime($widget->cache->cached_at) : Yii::t('common', '')),
            ]),
            [
                'class' => 'panel-wrap-data',
                'data-id' => $widget->id,
                'data-code' => $widget->type->code,
            ]
        )
    ];
}
?>
<br/>
<br/>
<div id="widget-stack">

    <?php if ($widgetTypes) { ?>
        <div class="row">

            <div class="col-md-12">

                <div id="widget-add-box" class="pull-right">
                    <div class="btn-group">

                        <?= Button::widget([
                            'id' => 'widget_add_btn',
                            'style' => Button::STYLE_SUCCESS . ' add-selectable-filters dropdown-toggle',
                            'icon' => GlyphIcon::PLUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>

                        <ul id="widget_add_dropdown" class="dropdown-menu dropdown-menu-right">
                            <?php foreach ($widgetTypes as $widgetType) { ?>
                                <li>
                                    <a href="<?= Url::to(['/widget/index/add', 'id' => $widgetType->id]); ?>" tabindex="-1"><?= Yii::t('common', $widgetType->name); ?></a>
                                </li>
                            <?php } ?>
                        </ul>

                    </div>
                </div>

            </div>
        </div>
    <?php } ?>

    <?=GridStack::widget(['items' => $items]); ?>
</div>

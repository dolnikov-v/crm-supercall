<?php

namespace backend\modules\widget\widgets;

use yii\base\Widget;

/**
 * Class GridStack
 * @package backend\modules\widget\widgets
 */

class GridStack extends Widget
{

    /**
     * @var
     */
    public $widgets;
    public $items;

    /**
     * @var
     */
    public $widgetTypes;


    /**
     * @return string
     */
    public function run()
    {
        return $this->render('grid-stack', [
            'widgets' => $this->widgets,
            'widgetTypes' => $this->widgetTypes,
        ]);
    }

}
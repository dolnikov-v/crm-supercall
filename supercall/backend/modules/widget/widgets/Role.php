<?php
namespace backend\modules\widget\widgets;

use yii\base\Widget;

/**
 * Class Role
 * @package backend\modules\widget\widgets
 */
class Role extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    public $roleList;

    /**
     * @var
     */
    public $rolesByWidget;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('role', [
            'model' => $this->model,
            'roleList' => $this->roleList,
            'rolesByWidget' => $this->rolesByWidget,
        ]);
    }
}

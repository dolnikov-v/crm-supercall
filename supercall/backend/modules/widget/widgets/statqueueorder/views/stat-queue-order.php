<?php

use common\components\grid\GridView;
use common\modules\order\models\Order;

/** @var \yii\data\ArrayDataProvider $dataProvider */

$statuses = Order::getStatusesCollection();

$columns = [
    [
        'header' => Yii::t('common', 'Очередь/Страна'),
        'attribute' => 'queue_name',
        'contentOptions' => ['style' => 'text-left;', 'class' => 'width-150'],
        'headerOptions' => ['style' => 'text-align:center;'],
        'content' => function($data){
            return $data['type'] == 'header' ? '<b>'.$data['queue_name'].'</b>' : $data['queue_name'];
        }
    ],
    [
        'header' => Yii::t('common', 'Всего'),
        'label' => Yii::t('common', 'Всего'),
        'attribute' => 'total',
        'contentOptions' => ['class' => 'width-150 text-center'],
        'headerOptions' => ['class' => 'text-center;'],
        'content' => function($data){
            return $data['type'] == 'data' ? '<b>'.$data['total'].'</b>' : $data['total'];
        }
    ],
];

foreach($statuses as $status=>$name){
    $columns[] = [
        'header' => Yii::t('common', $name),
        'label' => $statuses[$status],
        'attribute' => 'status_'.$status,
        'contentOptions' => function ($model, $key, $index, $column) use ($status){
            return [
                'class' => 'width-'.($status == Order::STATUS_DOUBLE ? 200 : 150).' text-center ',
            ];
        },
        'headerOptions' => ['class' => 'text-center;'],
    ];
}
?>


<div class="innerbody">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-striped'],
        'rowOptions' => function ($model, $key, $index, $column) {
            return [
                    'class' => ($model['type'] == 'header') ? 'success' : ''
            ];
        },
        'showHeader' => false,
        'columns' => $columns
    ]); ?>

</div>
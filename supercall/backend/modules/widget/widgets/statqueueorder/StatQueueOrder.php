<?php
namespace backend\modules\widget\widgets\statqueueorder;

use backend\modules\queue\models\Queue;
use backend\modules\queue\models\QueueUser;
use backend\modules\widget\widgets\CacheableWidget;
use common\models\Country;
use common\models\UserCountry;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\User;
use yii\data\ArrayDataProvider;
use common\modules\order\models\Order;
use Yii;

class StatQueueOrder extends CacheableWidget
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    public function getStatQueueOrder()
    {
        $stat = Order::find()
            ->leftJoin(Country::tableName(), Country::tableName().'.id='.Order::tableName().'.country_id')
            ->leftJoin(Queue::tableName(), Queue::tableName().'.id='.Order::tableName().'.last_queue_id')
            ->where(['in', 'last_queue_id', Queue::find()->select('id')->where(['active'=>Queue::STATUS_ACTIVE])])
            ->andWhere([Order::tableName() . '.country_id' => ArrayHelper::getColumn(User::getAllowCountries()->all(), 'country_id')])
            ->groupBy([Country::tableName().'.id', Queue::tableName().'.id'])
            ->orderBy([Country::tableName().'.id' => SORT_DESC]);

        $stat->select([
            'country_name' => Country::tableName().'.name',
            'queue_name' => Queue::tableName().'.name',
            'total' => new Expression("count(".Order::tableName().'.id'.")")
        ]);

        foreach(Order::getStatusesCollection() as $status=>$name){
            $stat->addSelect([
                'status_'.$status => new Expression("sum(case when status in (".$status.") then 1 else 0 end)")
            ]);
        }

        $stat->leftJoin(QueueUser::tableName(), QueueUser::tableName().'.queue_id='.Queue::tableName().'.id');

        $userModel = yii::$app->user->getUserModel();
        $users = $userModel->getCoTeamsUsers();
        if(!empty($users)){
            $user_ids = ArrayHelper::getColumn($users, 'id');
            $stat->andFilterWhere([QueueUser::tableName().'.user_id' => $user_ids]);
        }

        return $stat->asArray()->all();
    }

    function buildDataProvider()
    {
        $statuses = Order::getStatusesCollection();

        $stats = $this->getStatQueueOrder();
        $widgetData = [];



        $countries = ArrayHelper::getColumn($stats, 'country_name');

        foreach($statuses as $status=>$name) {
            $mapKeys[] = 'status_' . $status;
        }

        $labels = array_combine($mapKeys, array_values($statuses));

        foreach($stats as $k => $value){
            if($k == 0){
               $widgetData[] = ['type' => 'header', 'queue_name' => yii::t('common', $countries[$k]), 'total' => yii::t('common', 'Всего')] + $labels;
            }

           if($k>0 && $value['country_name'] != $countries[$k-1] && $k%2 == 0) {
                $widgetData[] = ['type' => 'header', 'queue_name' => yii::t('common', $countries[$k]), 'total' => yii::t('common', 'Всего')] + $labels;
           }

           unset($value['country_name']);
           $widgetData[] = ['type' => 'data', ] + $value;
        }

        return new ArrayDataProvider([
            'allModels' => $widgetData
        ]);
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('stat-queue-order', [
            'dataProvider' => $this->buildDataProvider()
        ]);
    }

    public function runData()
    {
        return $this->getStatQueueOrder();
    }
}
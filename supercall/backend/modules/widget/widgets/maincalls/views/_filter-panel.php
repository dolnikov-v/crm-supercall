<?php
use common\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use common\widgets\base\Button;

/** @var \common\modules\call\models\CallHistory $model */
/** @var [] $queues */
/** @var [] $products */
?>

<?php
/** @var ActiveForm $form */
$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'id' => 'main_calls_'.time(),
    'action' => ''
]); ?>

<div class="panel-filter" data-toggle="panel-filter">
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'created_at')->widget(DateRangePicker::className(), [
                'convertFormat'=>true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'Y/m/d',
                    ]
                ],
                'convertFormat' => true,
                'options' => [
                    'class' => 'form-control',
                    'name' => 'Filter[created_at]',
                    'value' => date('Y/m/d') . ' - ' . date('Y/m/d'),
                ]
            ])->label(yii::t('common', 'Период создания')); ?>
        </div>
        <div class="col-lg-3">

            <?= $form->field($model, 'queue_id')->select2List($queues, [
                'prompt' => '—',
                'name' => 'Filter[queue_id]',
            ])->label(yii::t('common', 'Очередь')) ?>

        </div>
        <div class="col-lg-3">

            <?= $form->field($model, 'product_id')->select2List($products, [
                'prompt' => '—',
                'name' => 'Filter[product_id]',
            ])->label(yii::t('common', 'Товары')) ?>

        </div>
        <div class="col-md-2 filter-submit">
            <div class="control-label"><label for="">&nbsp;</label></div>
            <?= Button::widget([
                'label' => Yii::t('common', 'Применить'),
                'style' => Button::STYLE_SUCCESS,
                'size' => Button::SIZE_SMALL,
            ]) ?>

        </div>
    </div>
</div>


<?php ActiveForm::end(); ?>

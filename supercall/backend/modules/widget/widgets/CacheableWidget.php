<?php
namespace backend\modules\widget\widgets;

use backend\modules\widget\models\WidgetCache;
use backend\modules\widget\models\WidgetUser;
use yii\base\Widget;
use Yii;

/**
 * Class CacheableWidget
 * @package backend\modules\widget\widgets
 */
class CacheableWidget extends Widget
{
    /** @var  serialized string for widget filter */
    public $filter;

    //кол-во дней от текущей даты (в запросах везде "-", т.е. от текущей даты назад)
    const PERIOD = 31;

    /**
     * @var bool
     */
    public $export = false;

    /**
     * @var string
     * value: 'string', 'array'
     */
    public $refreshDataType = 'string';

    /**
     * @var string
     */

    public $refreshScenario = '';

    /**
     * @var bool
     */

    public $onlineLoad = false;
    /**
     * @var WidgetUser
     */
    public $model;

    /**
     * @var array
     */
    public $data;

    /**
     * @var integer selected country
     */
    public $countryId = null;

    /**
     * @var array all users countries
     */
    public $countryIds = [];

    /**
     * @var integer
     */
    public $timeOffset = null;

    /**
     * Выборка данных из базы по параметрам
     *
     * @param array $params
     * @return array
     */
    private function getData($params)
    {
        return [];
    }

    /**
     * Выборка данных из базы по параметрам
     *
     * @param array $params
     * @return array
     */
    private function findAll()
    {
        return [];
    }

    /**
     * Выполнение виджета для получаения данных без подключения views и assets
     * @return array
     */
    private function runData()
    {
        return [];
    }

    /**
     * Типовое выполнение виджета, но с учетом выбора данных из кеша
     */
    public function run()
    {
        $params = [];
        $data = [];
        //чтение данных из кеша
        $widgetCache = WidgetCache::find()
            ->byCountry(Yii::$app->user->country->id)
            ->byWidgetUser($this->model->id)
            ->one();
        if ($widgetCache) {
            $data = $widgetCache->readData();
        }
        if (!$data) {
            $data = $this->getData($params);
        }

        //далее оперируем с $data и выводим виджет обычным образом
        //return $this->render(''...
    }

    public function getRefreshData($filter = null) {
        return null;
    }

}

<?php

use common\components\grid\GridView;

/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var \common\models\User $model */
?>


<div class="innerbody">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-bordered table-striped'],
        'rowOptions' => function ($model, $key, $index, $column) {
        },
        'showHeader' => false,
        'columns' => [
            [
                'header' => Yii::t('common', 'Страна'),
                'attribute' => 'country',
                'headerOptions' => ['style' => 'text-align:center;'],
                'content' => function($data){
                    return yii::t('common', $data['country']);
                }
            ],
            [
                'header' => Yii::t('common', 'Количество онлайн'),
                'contentOptions' => ['style' => 'text-align:center;', 'class' => 'width-150'],
                'attribute' => 'count',
                'headerOptions' => ['style' => 'text-align:center;'],
            ],
        ]
    ]); ?>

</div>
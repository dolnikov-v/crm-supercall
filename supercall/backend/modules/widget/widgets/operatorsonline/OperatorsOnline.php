<?php
namespace backend\modules\widget\widgets\operatorsonline;

use backend\modules\widget\widgets\CacheableWidget;
use common\models\Country;
use common\models\UserCountry;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\models\User;
use yii\data\ArrayDataProvider;

class OperatorsOnline extends CacheableWidget
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    public function getOperatorsOnline()
    {
        $listOperators = User::find()
            ->select([
                'country' => Country::tableName() . '.name',
                'count' => new Expression('count(' . User::tableName() . '.id)')
            ])
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName() . '.user_id=' . User::tableName() . '.id')
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . UserCountry::tableName() . '.country_id')
            ->where(['is_ready' => true])
            ->andWhere([UserCountry::tableName() . '.country_id' => ArrayHelper::getColumn(User::getAllowCountries()->all(), 'country_id')])
            ->groupBy([Country::tableName() . '.id']);

        $userModel = yii::$app->user->getUserModel();
        $users = $userModel->getCoTeamsUsers();
        if(!empty($users)){
            $user_ids = ArrayHelper::getColumn($users, 'id');
            $listOperators->andFilterWhere([User::tableName().'.id' => $user_ids]);
        }

        return $listOperators->asArray()->all();
    }

    function buildDataProvider()
    {
        $operators = $this->getOperatorsOnline();

        return new ArrayDataProvider([
            'allModels' => $operators
        ]);
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('operators-online', [
            'dataProvider' => $this->buildDataProvider(),
            'model' => new User(),
        ]);
    }

    public function runData()
    {
        return $this->getOperatorsOnline();
    }
}
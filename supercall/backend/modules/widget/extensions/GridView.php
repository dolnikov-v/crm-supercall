<?php
namespace backend\modules\widget\extensions;

use kartik\grid\GridExportAsset;
use Yii;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\Json;

/**
 * Class GridView
 * @package backend\modules\report\extensions
 */
class GridView extends \kartik\grid\GridView
{
    /**
     * @var array
     */
    public $tableOptions = [
        'class' => 'table table-report table-striped table-hover table-bordered tl-fixed',
    ];

    /**
     * @var array|boolean
     */
    public $export = [
        'showConfirmAlert' => false,
        'fontAwesome' => true,
        'target' => self::TARGET_SELF,
        'header' => false,
    ];

    /**
     * @var array
     */
    public $exportConfig = [
        GridView::EXCEL => [],
        GridView::PDF => [],
        GridView::CSV => [],
        GridView::JSON => [],
    ];

    /**
     * @var array|boolean
     */
    public $pageSummaryRowOptions = [
        'class' => 'report-summary',
    ];

    /**
     * @var string
     */
    public $layout = '{items}<div class="report_export_box">{toolbar}</div>';

    /**
     * @var string
     */
    public $toolbar = '{export}';

    /**
     * @var string
     */
    public $exportFilename = '';

    /**
     * @var bool
     */

    public $exportMoveDropDown = true;

    /**
     * @inheritdoc
     */
    public function init()
    {

        if ($this->export !== false) {
            $this->export['label'] = Yii::t('common', 'Экспорт');
            $this->export['options']['class'] = 'btn btn-default btn-sm';

            $this->exportConfig = [
                GridView::EXCEL => ['label' => Yii::t('common', 'Excel')],
                GridView::PDF => ['label' => Yii::t('common', 'PDF')],
                GridView::CSV => ['label' => Yii::t('common', 'CSV')],
                GridView::JSON => ['label' => Yii::t('common', 'JSON')],
            ];
            if ($this->exportFilename) {
                $filename = $this->exportFilename;
                foreach ($this->exportConfig as &$config) {
                    $config['filename'] = $filename;
                } unset($config);
            }
        }

        if ($this->showPageSummary === false) {
            $this->pageSummaryRowOptions = false;
        }

        parent::init();
    }

    public function renderExport()
    {
        if ($this->export === false || !is_array($this->export) ||
            empty($this->exportConfig) || !is_array($this->exportConfig)
        ) {
            return '';
        }
        $menuOptions = $this->export['menuOptions'];
        $iconPrefix = $this->export['fontAwesome'] ? 'fa fa-' : 'glyphicon glyphicon-';
        $action = $this->_module->downloadAction;
        $encoding = ArrayHelper::getValue($this->export, 'encoding', 'utf-8');
        $bom = ArrayHelper::getValue($this->export, 'bom', true);
        $target = ArrayHelper::getValue($this->export, 'target', self::TARGET_POPUP);
        $form = Html::beginForm(
                is_array($action) ? $action : [$action],
                'post',
                [
                    'class' => 'kv-export-form',
                    'style' => 'display:none',
                    'target' => ($target == self::TARGET_POPUP) ? 'kvDownloadDialog' : $target,
                ]
            ) . "\n" .
            Html::hiddenInput('export_hash') . "\n" .
            Html::hiddenInput('export_filetype') . "\n" .
            Html::hiddenInput('export_filename') . "\n" .
            Html::hiddenInput('export_mime') . "\n" .
            Html::hiddenInput('export_config') . "\n" .
            Html::hiddenInput('export_encoding', $encoding) . "\n" .
            Html::hiddenInput('export_bom', $bom) . "\n" .
            Html::textarea('export_content') . "\n" .
            "</form>";


        $items = empty($this->export['header']) ? [] : [$this->export['header']];
        foreach ($this->exportConfig as $format => $setting) {
            $iconOptions = ArrayHelper::getValue($setting, 'iconOptions', []);
            Html::addCssClass($iconOptions, $iconPrefix . $setting['icon']);
            $label = (empty($setting['icon']) || $setting['icon'] == '') ? $setting['label'] :
                Html::tag('i', '', $iconOptions) . ' ' . $setting['label'];

            unset($setting['options']['title']);

            $mime = ArrayHelper::getValue($setting, 'mime', 'text/plain');
            $config = ArrayHelper::getValue($setting, 'config', []);
            if ($format === self::JSON) {
                unset($config['jsonReplacer']);
            }
            $dataToHash = $setting['filename'] . $mime . $encoding . $bom . Json::encode($config);
            $hash = Yii::$app->security->hashData($dataToHash, $this->_module->exportEncryptSalt);
            $items[] = [
                'label' => $label,
                'url' => '#',
                'linkOptions' => [
                    'class' => 'export-' . $format,
                    'data-mime' => $mime,
                    'data-hash' => $hash,
                ],
                'options' => $setting['options'],
            ];
        }
        $itemsBefore = ArrayHelper::getValue($this->export, 'itemsBefore', []);
        $itemsAfter = ArrayHelper::getValue($this->export, 'itemsAfter', []);
        $items = ArrayHelper::merge($itemsBefore, $items, $itemsAfter);
        return Dropdown::widget([
                'items' => $items,
                'encodeLabels' => false,
                'options' => $menuOptions
            ]) . $form;
    }

    /**
     * @inheritdoc
     */
    protected function registerAssets()
    {
        if ($export = $this->export) {

            /* Блокируем поведение export по умолчанию */

            $this->export = false;
            parent::registerAssets();
            $this->export = $export;

            /* Определяем новое поведение export*/

            $view = $this->getView();
            $gridId = $this->options['id'];
            $function = $this->_gridClientFunc . '__export';
            $script = '';

            GridExportAsset::register($view);
            $target = ArrayHelper::getValue($this->export, 'target', self::TARGET_POPUP);
            $gridOpts = Json::encode(
                [
                    'gridId' => $gridId,
                    'target' => $target,
                    'messages' => $this->export['messages'],
                    'exportConversions' => $this->exportConversions,
                    'showConfirmAlert' => ArrayHelper::getValue($this->export, 'showConfirmAlert', true),
                ]
            );
            $gridOptsVar = 'kvGridExp_' . hash('crc32', $gridOpts);
            $view->registerJs("var {$gridOptsVar}={$gridOpts};", View::POS_HEAD);
            foreach ($this->exportConfig as $format => $setting) {

                $id = "$('#{$gridId} .export-{$format}')";
                $genOpts = Json::encode(
                    [
                        'filename' => $setting['filename'],
                        'showHeader' => $setting['showHeader'],
                        'showPageSummary' => $setting['showPageSummary'],
                        'showFooter' => $setting['showFooter'],
                    ]
                );
                $genOptsVar = 'kvGridExp_' . hash('crc32', $genOpts);
                $view->registerJs("var {$genOptsVar}={$genOpts};", View::POS_HEAD);
                $expOpts = Json::encode(
                    [
                        'dialogLib' => ArrayHelper::getValue($this->krajeeDialogSettings, 'libName', 'krajeeDialog'),
                        'gridOpts' => new JsExpression($gridOptsVar),
                        'genOpts' => new JsExpression($genOptsVar),
                        'alertMsg' => ArrayHelper::getValue($setting, 'alertMsg', false),
                        'config' => ArrayHelper::getValue($setting, 'config', []),
                    ]
                );
                $expOptsVar = 'kvGridExp_' . hash('crc32', $expOpts);
                $view->registerJs("var {$expOptsVar}={$expOpts};", View::POS_HEAD);
                $script .= "{$id}.gridexport({$expOptsVar});";
            }

            $view->registerJs("var {$function}=function(){\n{$script}\n};", View::POS_HEAD);
            $view->registerJs("{$function}();", View::POS_READY);
            if ($this->exportMoveDropDown) {
                $view->registerJs("$('#{$gridId} .report_export_box').children('ul').appendTo($('#{$gridId} .report_export_box').closest('.panel').find('.panel-action-export'));", View::POS_READY);
            }
        }
    }
}

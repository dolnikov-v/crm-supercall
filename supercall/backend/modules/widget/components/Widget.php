<?php

namespace backend\modules\widget\components;

use backend\modules\widget\models\WidgetUser;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class Widget
 * @package backend\modules\widget\components
 */
class Widget
{
    public static $filter;
    /**
     * @return array
     */
    public static function getClasses()
    {
        return [
            'main-orders' => 'mainorders\MainOrders',
            'main-calls' => 'maincalls\MainCalls',
            'operators-online' => 'operatorsonline\OperatorsOnline',
            'stat-queue-order' => 'statqueueorder\StatQueueOrder',
        ];
    }

    /**
     * @param $code
     * @return string
     */
    public static function getClass($code)
    {
        $class = ArrayHelper::getValue(static::getClasses(), $code);
        return ($class) ? 'backend\modules\widget\widgets\\' . $class : '';
    }

    /**
     * @param WidgetUser
     * @param array
     * @return \backend\modules\widget\widgets\CacheableWidget | null
     */

    public static function createWidgetObj($widget, $config = [])
    {
        $class = static::getClass($widget->type->code);

        if ($class && class_exists($class)) {
            /** @var \yii\base\Widget $widgetClass */

            $config['model'] = $widget;
            return new $class($config);
        }

        return null;
    }

    /**
     * @param WidgetUser
     * @param array
     * @return string
     */

    public static function widget($widget, $config = [])
    {
        $content = '';

        $class = static::getClass($widget->type->code);
        if ($class && class_exists($class)) {
            /** @var \yii\base\Widget $class */

            $config['model'] = $widget;
            $content = $class::widget($config);
        }
        return $content;
    }

    /**
     * Получить данные виджета для его кеширования
     *
     * @param WidgetUser $widget
     * @param integer $countryId
     * @param array $countryIds
     * @param integer $timeOffset
     * @return array
     */

    public static function getData($widget, $countryId = null, $countryIds = null, $timeOffset = null)
    {
        $data = [];

        $class = static::getClass($widget->type->code);
        if ($class && class_exists($class)) {
            /** @var \yii\base\Widget $widgetClass */

            $widgetObject = new $class;

            if (method_exists($widgetObject, 'runData')) {
                $widgetObject->model = $widget;
                $widgetObject->countryId = $countryId;
                $widgetObject->countryIds = $countryIds;
                $widgetObject->timeOffset = $timeOffset;

                $data = $widgetObject->runData();
            } else {
                throw new InvalidParamException(Yii::t('common', 'class method does not exists ' . $class . '::runData'));
            }

        }
        return $data;
    }

    /**
     * @param $widget
     * @param $config array
     * @return null|string
     */

    public static function getRefreshData($widget, $config = [])
    {

        $data = static::widget($widget, $config);

        if ($widgetObject = static::createWidgetObj($widget, $config)) {
            $widgetObject->filter = $config['filter'];
            if ($widgetObject->refreshDataType == 'string') {
                $data = static::widget($widget, $config);
            } elseif ($widgetObject->refreshDataType == 'array') {
                $data = $widgetObject->getRefreshData(isset($config['filter']) ? $config['filter'] : null);
                $data['scenario'] = $widgetObject->refreshScenario;
            }
        }
        return $data;
    }

}


<?php
namespace backend\modules\widget\controllers;

use common\components\base\AjaxFilter;
use backend\components\web\Controller;
use backend\modules\widget\components\Widget;
use backend\modules\widget\models\WidgetCache;
use backend\modules\widget\models\WidgetUser;
use backend\modules\widget\models\WidgetType;
use yii\helpers\ArrayHelper;
use common\models\User;
use Yii;

/**
 * Class IndexController
 * @package backend\modules\widget\controllers
 */
class IndexController extends Controller
{

    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['change-place', 'delete', 'refresh'],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $widgets = WidgetUser::find()
            ->joinWith(['type'])
            ->where([
                'user_id' => Yii::$app->user->id,
                WidgetType::tableName() . '.status' => WidgetType::STATUS_ACTIVE,
            ])
            ->all();

        $query = WidgetType::find()
            ->andWhere(['status' => WidgetType::STATUS_ACTIVE]);

        if ($list = ArrayHelper::getColumn($widgets, 'type_id')) {
            $query->andWhere(['not in', 'id', $list]);
        }

        $widgetTypes = $query->all();

        return $this->render('index', [
            'widgets' => $widgets,
            'widgetTypes' => $widgetTypes,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */

    public function actionAdd($id)
    {
        $result = false;

        $type = WidgetType::find()
            ->with('roles')
            ->where([
                'id' => intval($id),
                'status' => WidgetType::STATUS_ACTIVE,
            ])
            ->one();

        if ($type) {
            $model = new WidgetUser();
            $model->user_id = Yii::$app->user->id;
            $model->type_id = $type->id;
            $model->setDefaultOptions();
            $result = $model->save();
        }

        if ($result) {
            $message = Yii::t('common', 'Виджет успешно добавлен.');
            $type = 'success';
        } else {
            $message = Yii::t('common', 'Не удалось добавить виджет.');
            $type = 'danger';
        }

        Yii::$app->notifier->addNotifier($message, $type);

        return $this->redirect('/?done');
    }


    /**
     * @return array
     */

    public function actionDelete()
    {
        $model = WidgetUser::findOne(
            ['id' => Yii::$app->request->post('id')]
        );

        if ($model->user_id == Yii::$app->user->id) {

            if ($model && $model->delete()) {
                $message = Yii::t('common', 'Виджет успешно удален.');
                $type = 'success';
            } else {
                $message = Yii::t('common', 'Не удалось удалить виджет.');
                $type = 'danger';
            }

            //эта нотификация после перезагрузки страницы будет показана, она сейчас не нужна, т.к у нас удаление без перезагрузки страницы
            //Yii::$app->notifier->addNotifier($message, $type);
        }

        return [
            'status' => 1,
        ];
    }

    /**
     * @return array
     */

    public function actionChangePlace()
    {

        if ($items = Yii::$app->request->post('items')) {

            foreach ($items as $item) {

                if ($model = WidgetUser::findOne(['id' => $item['id']])) {
                    if ($model->user_id == Yii::$app->user->id) {
                        $model->setOptions($item['options']);
                        $model->save();
                    }
                }
            }
        }

        return [
            'status' => 1,
        ];
    }

    /**
     * @return string
     */

    public function actionLoad()
    {
        $widget = WidgetUser::find()
            ->joinWith(['type'])
            ->where([
                WidgetUser::tableName() . '.id' => Yii::$app->request->get('id'),
                WidgetUser::tableName() . '.user_id' => Yii::$app->user->id,
                WidgetType::tableName() . '.status' => WidgetType::STATUS_ACTIVE,
            ])
            ->one();

        return ($widget) ? $this->renderPartial('load', ['widget' => $widget]) : '';
    }

    /**
     * @return array
     */

    public function actionRefresh()
    {
        $model = WidgetUser::find()
            ->joinWith(['type'])
            ->where([
                WidgetUser::tableName() . '.id' => Yii::$app->request->post('id')
            ])
            ->one();



        $data = [];
        $time = null;
        if ($model && $model->user_id == Yii::$app->user->id) {
            /** @var \backend\modules\widget\models\WidgetUser $model */

            //виджет не кешируется
            if ($model->type->no_cache == WidgetType::NO_CACHE) {
                $data = Widget::getRefreshData($model, ['filter' => Yii::$app->request->post('filter')]);
            }
            else {

                //виджет может быть закеширован
                $widgetCache = WidgetCache::find()
                    ->byWidgetUser($model->id)
                    ->one();
                if (!$widgetCache) {
                    $widgetCache = new WidgetCache();
                    $widgetCache->widgetuser_id = $model->id;
                }
                $cachedData = [];
                if ($model->type->by_country == WidgetType::BY_ALL_COUNTRIES) {
                    $cachedData = Widget::getData($model, null, array_keys(User::getAllowCountries()));
                }
                if ($model->type->by_country == WidgetType::BY_COUNTRY) {
                    $cachedData = Widget::getData($model, Yii::$app->user->country->id);
                    $widgetCache->country_id = Yii::$app->user->country->id;
                }
                $widgetCache->data = $widgetCache->saveData($cachedData);
                $widgetCache->cached_at = time();

                // сохранение данных для кеширования
                if ($widgetCache->save()) {
                    //обернутые данные для виджета вернем
                    $data = Widget::getRefreshData($model, ['data' => $cachedData]);
                    $time = Yii::t('common', 'Данные от') . ' ' . Yii::$app->formatter->asDatetime($widgetCache->cached_at);
                }
            }
        }
        return [
            'status' => 1,
            'data' => $data,
            'time' => $time
        ];
    }
}

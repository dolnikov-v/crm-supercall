<?php

namespace backend\modules\widget\controllers;

use common\components\base\AjaxFilter;
use backend\components\web\Controller;
use backend\modules\widget\models\WidgetType;
use backend\modules\widget\models\WidgetRole;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use Yii;

/**
 * Class TypeController
 * @package backend\modules\widget\controllers
 */
class TypeController extends Controller
{

    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['set-role'],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $query = WidgetType::find()
            ->orderBy(['name' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param null $id
     * @return string
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                $message = Yii::t('common', 'Виджет успешно отредактирован.');
                Yii::$app->notifier->addNotification($message, 'success');

                return $this->redirect(Url::toRoute('index'));
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'roles' => Yii::$app->authManager->getRoles(),
            'rolesByWidget' => ArrayHelper::getColumn($model->roles, 'role'),
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);
        $model->status = $model::STATUS_ACTIVE;

        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Виджет успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось активировать виджет.'), 'danger');
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);
        $model->status = $model::STATUS_INACTIVE;

        if ($model->save()) {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Виджет успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotification(Yii::t('common', 'Не удалось деактивировать виджет.'), 'danger');
        }

        return $this->redirect(['index']);
    }

    /**
     * @throws \yii\web\HttpException
     */
    public function actionSetRole()
    {
        $request = Yii::$app->request;
        $value = $request->post('value');

        if ($value == 'off') {

            $model = WidgetRole::find()
                ->where([
                    'role' => $request->post('role'),
                    'type_id' => $request->post('type_id'),
                ])->one();

            $status = ($model && $model->delete());

        } elseif ($value == 'on') {

            $model = new WidgetRole();
            $model->load($request->post(), '');
            $status = $model->save();

        }

        if (empty($status)) {
            throw new BadRequestHttpException(Yii::t('common', 'Обращение к несуществующему виждету или роли пользователю.'));
        }

        return [
            'success' => true,
        ];
    }

    /**
     * @param $id
     * @return WidgetType
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = WidgetType::findOne($id);

        if ($model) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Виджет не найден.'));
        }
    }

}
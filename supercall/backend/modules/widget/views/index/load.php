<?php

use backend\modules\widget\components\Widget;

/** @var \backend\modules\widget\models\WidgetUser $widget */
?>

<?= Widget::widget($widget); ?>

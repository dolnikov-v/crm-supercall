<?php

use backend\modules\widget\widgets\GridStack;

/** @var \yii\web\View $this */
/** @var \backend\modules\widget\models\WidgetUser[] $widgets */
/** @var \backend\modules\widget\models\WidgetType[] $widgetTypes */

?>

<?= GridStack::widget([
    'widgets' => $widgets,
    'widgetTypes' => $widgetTypes,
]); ?>


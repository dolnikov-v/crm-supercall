<?php
use common\components\grid\ActionColumn;
use backend\modules\widget\extensions\GridView;
use common\helpers\grid\DataProvider;
use backend\widgets\assets\custom\ModalConfirmDeleteAsset;
use common\widgets\base\Panel;
use common\widgets\base\Label;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider */

ModalConfirmDeleteAsset::register($this);

$this->title = Yii::t('common', 'Список виджетов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Виджеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с виджетами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'withBody' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Название'),
                'headerOptions' => ['class' => 'width-200'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'description',
                'label' => Yii::t('common', 'Статус'),
                'content' => function ($data) {

                    /** @var \backend\modules\widget\models\WidgetType $data */

                    return Label::widget([
                        'label' => ArrayHelper::getValue($data::getStatuses(), $data->status),
                        'style' => $data->status == $data::STATUS_ACTIVE ? Label::STYLE_SUCCESS : Label::STYLE_DANGER,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('widget.type.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('widget.type.activate') && $model->status == $model::STATUS_INACTIVE;
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('widget.type.deactivate') && $model->status == $model::STATUS_ACTIVE;
                        },
                    ],
                ]
            ],
        ]
    ]),
]) ?>

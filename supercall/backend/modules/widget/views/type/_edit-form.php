<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \backend\modules\widget\models\WidgetType $model */
?>

<?php
/** @var ActiveForm $form */
$form = ActiveForm::begin([
    'id' => 'user_form',
    'action' => $model->isNewRecord ? Url::toRoute('edit') : Url::toRoute(['edit', 'id' => $model->id])
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'status')->select2List($model::getStatuses()) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Сохранить виждет')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
use backend\modules\widget\widgets\Role;
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var backend\modules\widget\models\WidgetType $model */
/** @var backend\modules\widget\models\WidgetRole[] $roles */
/** @var array $rolesByWidget */

$this->title = Yii::t('common', 'Редактирование виджета');

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Виджеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список виджетов'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные виджета'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список ролей'),
    'collapse' => true,
    'withBody' => false,
    'content' => Role::widget([
        'model' => $model,
        'roleList' => $roles,
        'rolesByWidget' => $rolesByWidget,
    ]),
]) ?>


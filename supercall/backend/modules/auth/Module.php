<?php
namespace backend\modules\auth;

use backend\components\base\Module as BackendModule;
use Yii;

/**
 * Class Module
 * @package backend\modules\auth
 */
class Module extends BackendModule
{
    public $controllerNamespace = 'backend\modules\auth\controllers';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

}

<?php

namespace backend\modules\auth\controllers;

use backend\components\web\Controller;
use backend\modules\auth\models\LoginForm;
use common\models\UserReady;
use Yii;
use yii\filters\AccessControl;
use common\models\User;
use common\models\Asterisk;
use yii\helpers\VarDumper;

/**
 * Class LoginController
 * @package backend\modules\auth\controllers
 */
class LoginController extends Controller
{
    const LOGGER_TYPE = 'last-activity';

    public $layout = 'login';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $model = new LoginForm();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if ($model->login()) {
                $model->resetAttempts();
                $model->addToLog(LoginForm::LOGGER_TYPE_SUCCESS, Yii::$app->request->post('LoginForm'));
                // При логоне зафиксировать статус Готов для Оператора
                UserReady::change(Yii::$app->user->id, [
                    'param' => 'is_ready',
                    'value' => true,
                ]);

                $setActivity = User::setLastActivity();

                if (!$setActivity['success']) {
                    $logger = yii::$app->get('logger');
                    $logger->action = __METHOD__;
                    $logger->route = '/auth/login/index';
                    $logger->type = self::LOGGER_TYPE;
                    $logger->tags = [
                        'user_id' => Yii::$app->user->id,
                    ];
                    $logger->error = $setActivity['message'];
                    $logger->save();
                }

                /** @var User $userData */
                $userData = $model->getUser();
                $post = yii::$app->request->post();

                if(isset($post['LoginForm']) && $post['LoginForm']['ip'] && filter_var($post['LoginForm']['ip'], FILTER_VALIDATE_IP)) {
                    $userData->ip = $post['LoginForm']['ip'];
                    $userData->outer_ip = Yii::$app->request->getRemoteIP();

                }else{
                    $userData->ip = null;
                    $userData->outer_ip = null;
                }

                $userData->save(false, ['ip','outer_ip']);

                if ($userData['auto_call'])
                    Asterisk::setUserPaused(0);

                return $this->goBack();

            } else {
                if ($model->isLimitAttempts()) {
                    $model->setScenario(LoginForm::CAPTCHA_SCENARIO);
                    $model->addToLog(LoginForm::LOGGER_TYPE_CAPTCHA);
                }else{
                    $model->addToLog(LoginForm::LOGGER_TYPE_FAILED);
                }

                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}

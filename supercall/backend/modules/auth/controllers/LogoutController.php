<?php
namespace backend\modules\auth\controllers;

use backend\components\web\Controller;
use common\models\AutoCall;
use common\models\UserReady;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\Asterisk;

/**
 * Class LogoutController
 * @package app\controllers\auth
 */
class LogoutController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return \yii\web\Response
     */
    public function actionIndex()
    {
        // При разлогоне завершить фиксацию онлайн-режима в табл. user_ready
        UserReady::change(Yii::$app->user->id, [
            'param' => 'is_ready',
            'value' => false
        ]);

        $userData = Yii::$app->getUser()->getIdentity();

        if ($userData['auto_call']) {
            Asterisk::setUserPaused(AutoCall::PAUSEON, $userData->sipArray['sip_login']);
        }

        Yii::$app->user->logout();

        return $this->redirect(Url::toRoute('/auth/login'));
    }
}

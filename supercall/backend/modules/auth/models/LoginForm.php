<?php

namespace backend\modules\auth\models;

use common\models\User;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;
use yii\web\Cookie;

/**
 * Class LoginForm
 * @package backend\modules\auth\models
 */
class LoginForm extends Model
{
    //кол-во ошибок авторизации
    const COUNT_ATTEMPTS_LOGIN_ERROR = 3;
    const NAME_LIMIT_AUTH_ATTEMPTS_SESSION = 'LimitAuthAttempts';
    const LOGGER_TYPE_FAILED = 'AUTORIZATION FAILED';
    const LOGGER_TYPE_CAPTCHA = 'AUTORIZATION CAPTCHA';
    const LOGGER_TYPE_SUCCESS = 'AUTORIZATION SUCCESS';
    const CAPTCHA_AUTH_MOMENT_EMAIL = 'security@2wtrade.com';
    const SUBJECT_EMAIL = 'Using Captcha on 2wcall.com when trying to authenticate';

    const CAPTCHA_SCENARIO = 'captcha';
    const DEFAULT_SCENARIO = 'default';

    public $username;
    public $password;
    public $ip;
    public $rememberMe = false;

    /**
     * @var boolean|User
     */
    private $user = false;

    public function scenarios()
    {
        return [
            self::CAPTCHA_SCENARIO => ['username', 'password', 'reCaptcha'],
            self::DEFAULT_SCENARIO => ['username', 'password'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password'], 'filter', 'filter' => 'trim'],
            ['rememberMe', 'boolean'],
            ['ip', 'string'],
            ['password', 'validatePassword'],
            [[], ReCaptchaValidator::className()],
            [['username', 'password', 'reCaptcha'], 'required', 'on' => self::CAPTCHA_SCENARIO]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('common', 'Имя пользователя'),
            'password' => Yii::t('common', 'Пароль'),
            'ip' => Yii::t('common', 'Локальный IP адрес'),
            'rememberMe' => Yii::t('common', 'Запомнить'),
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('common', 'Неверное имя пользователя или пароль.'));
            }
        }
    }

    /**
     * @return null|User
     */
    public function getUser()
    {
        if ($this->user === false) {
            $byUser = User::findByUsername($this->username);
            $this->user = is_null($byUser) ? User::findByEmail($this->username) : $byUser;
        }

        return $this->user;
    }

    /**
     * @return bool
     */
    public function login($validate = true)
    {
        if (($validate ? $this->validate() : !$validate) && $this->canAccess($this->getUser())) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }

    /**
     *  //проверка на удаленную учетку или блокированную
     * @param User $user
     * @return bool
     */
    public function canAccess($user)
    {
        return !in_array($user->status, User::blockedStatusCollection());
    }

    /**
     * Подсчёт кол-ва попыток авторизации
     * @return mixed
     */
    public function checkLimitAttempts()
    {
        $name = self::NAME_LIMIT_AUTH_ATTEMPTS_SESSION;
        $session = yii::$app->getSession();
        $session->getIsActive();

        if (!$session->has($name)) {
            $session->set($name, 1);
        } else {
            $session->set($name, ($session->get($name) + 1));

        }
        //какой - то магией это все срабатывает 2 раза. В коде - метод вызывается 1 раз
        return $session->get($name) / 2;
    }

    /**
     * Возвращает кол-во попыток
     * @return mixed
     */
    public function isLimitAttempts()
    {
        return $this->checkLimitAttempts() > LoginForm::COUNT_ATTEMPTS_LOGIN_ERROR;
    }

    /**
     * Сброс кол-ва попыток неудачной авторизации
     */
    public function resetAttempts()
    {
        $name = self::NAME_LIMIT_AUTH_ATTEMPTS_SESSION;
        $session = yii::$app->getSession();
        $session->getIsActive();

        if ($session->has($name)) {
            $session->remove($name);
            $this->setScenario(LoginForm::DEFAULT_SCENARIO);
        }
    }

    /**
     * @param $type
     * @param array $data
     */
    public function addToLog($type, $data = [])
    {
        $post = Yii::$app->request->post();
        $logger = yii::$app->get('logger');
        $logger->action = __METHOD__;
        $logger->route = '/auth/login/index';
        $logger->type = $type;
        $logger->tags = [
            'post' => json_encode((isset($post['LoginForm']) ? $post['LoginForm'] : $post), 256),
            'get' => json_encode(Yii::$app->request->get(), 256),
            'headers' => json_encode(Yii::$app->request->headers, 256),
            'Yii IP' => Yii::$app->request->userIP,
        ];

        if (!empty($data)) {
            $logger->tags = array_merge($data, $logger->tags);
        }

        $logger->error = 'Not autorized';

        $logger->save();

        if ($type == self::LOGGER_TYPE_CAPTCHA) {
            $this->sendEmail($logger->tags);
        }

    }

    public function sendEmail($data)
    {
        Yii::$app->mailer
            ->compose('@frontend/modules/auth/mail/captcha-auth-moment', ['data' => $data])
            ->setFrom('no-reply@2wcall.com')
            ->setTo(self::CAPTCHA_AUTH_MOMENT_EMAIL)
            ->setSubject(Yii::t('common', self::SUBJECT_EMAIL))
            ->send();
    }
}

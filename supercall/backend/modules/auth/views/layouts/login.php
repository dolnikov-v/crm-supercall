<?php
use backend\assets\base\ThemeAsset;
use common\assets\fonts\OpenSansAsset;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var string $content */

OpenSansAsset::register($this);
ThemeAsset::register($this);
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: <?= Yii::$app->name ?></title>
    <?php $this->head(); ?>
</head>
<body class="blank">
<?php $this->beginBody(); ?>

<div class="color-line"></div>
<div class="login-container">
    <?= $content ?>
</div>

<?php $this->endBody(); ?>
<?= $this->render('@backend/views/common/_notifiers') ?>
</body>
</html>
<?php $this->endPage(); ?>

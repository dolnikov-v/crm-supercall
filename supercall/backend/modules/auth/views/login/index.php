<?php

use common\assets\base\CustomCommonAsset;
use common\widgets\ActiveForm;
use common\widgets\base\Checkbox;
use himiklab\yii2\recaptcha\ReCaptcha;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \backend\modules\auth\models\LoginForm $model */

CustomCommonAsset::register($this);

$this->title = Yii::t('common', 'Авторизация');
?>

<div class="row">
    <div class="col-md-12">
        <div class="text-center m-b-md">
            <h3><?= Yii::t('common', 'Авторизация') ?></h3>
            <small><?= Yii::t('common', 'Панель управления') ?></small>
        </div>
        <div class="hpanel">
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['action' => '/auth/login']); ?>
                <div class="form-group">
                    <label class="control-label"
                           for="<?= Html::getInputId($model, 'username') ?>"><?= Yii::t('common', 'Имя пользователя') . ' / ' . Yii::t('common', 'e@mail') ?></label>

                    <?= Html::input('text', Html::getInputName($model, 'username'), $model->username, [
                        'id' => Html::getInputId($model, 'username'),
                        'class' => 'form-control',
                        'placeholder' => Yii::t('common', 'Имя пользователя'),
                    ]) ?>

                    <span
                            class="help-block small"><?= Yii::t('common', 'Введите ваше имя пользователя') ?></span>
                </div>
                <div class="form-group">
                    <label class="control-label"
                           for="<?= Html::getInputId($model, 'password') ?>"><?= Yii::t('common', 'Пароль') ?></label>

                    <?= Html::input('password', Html::getInputName($model, 'password'), '', [
                        'id' => Html::getInputId($model, 'password'),
                        'class' => 'form-control',
                        'placeholder' => Yii::t('common', 'Пароль'),
                    ]) ?>

                    <?= Html::input('hidden', Html::getInputName($model, 'ip'), $model->ip, [
                        'id' => 'local_ip'
                    ]); ?>

                    <span class="help-block small"><?= Yii::t('common', 'Введите ваш пароль') ?></span>
                </div>

                <?php if ($model->isLimitAttempts()) : ?>

                    <div class="form-group">
                        <?= $form->field($model, 'reCaptcha')->widget(ReCaptcha::className())->label(false) ?>
                    </div>

                <?php endif; ?>

                <div class="form-group">
                    <?= Checkbox::widget([
                        'id' => Html::getInputId($model, 'rememberMe'),
                        'name' => Html::getInputName($model, 'rememberMe'),
                        'value' => 1,
                        'label' => Yii::t('common', 'Запомнить меня'),
                        'checked' => $model->rememberMe,
                    ]) ?>
                </div>
                <button class="btn btn-success btn-block"><?= Yii::t('common', 'Войти') ?></button>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <strong><?= strtoupper(Yii::$app->params['companyName']); ?></strong>
        - <?= Yii::t('common', 'Панель управления') ?> <br/>
        © <?= date('Y') ?> <?= Yii::t('common', 'Все права защищены') ?>
    </div>
</div>

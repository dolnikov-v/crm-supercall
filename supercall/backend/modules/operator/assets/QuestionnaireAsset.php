<?php
namespace backend\modules\operator\assets;

use yii\web\AssetBundle;

/**
 * Class QuestionnaireAsset
 * @package backend\modules\operator\assets
 */
class QuestionnaireAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/operator/questionnaire';

    public $js = [
        'questionnaire.js',
    ];

    public $css = [
        'questionnaire.css',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

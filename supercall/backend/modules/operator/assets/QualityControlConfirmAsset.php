<?php

namespace backend\modules\operator\assets;

use kartik\base\AssetBundle;

class QualityControlConfirmAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/operator/quality-control/';

    public $js = [
        'quality-control-confirm.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
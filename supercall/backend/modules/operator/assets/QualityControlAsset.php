<?php
/**
 * Created by PhpStorm.
 * User: PCWORK
 * Date: 13.12.2017
 * Time: 13:46
 */

namespace backend\modules\operator\assets;
use yii\web\AssetBundle;

class QualityControlAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/operator/quality-control';

    public $js = [
        'quality-control.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
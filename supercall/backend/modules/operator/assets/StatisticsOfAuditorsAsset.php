<?php
namespace backend\modules\operator\assets;

use yii\web\AssetBundle;

/**
 * Class StatisticsOfAuditorsAsset
 * @package backend\modules\operator\assets
 */
class StatisticsOfAuditorsAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/operator/statistics-of-auditors';

    public $js = [
        'statistics-of-auditors.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
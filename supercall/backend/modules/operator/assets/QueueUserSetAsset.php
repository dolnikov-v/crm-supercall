<?php
/**
 * Created by PhpStorm.
 * User: PCWORK
 * Date: 08.09.2017
 * Time: 12:39
 */

namespace backend\modules\operator\assets;

use yii\web\AssetBundle;

class QueueUserSetAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/operator/queue-user-set';

    public $js = [
        'queue-user-set.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
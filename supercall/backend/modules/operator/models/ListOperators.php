<?php
namespace backend\modules\operator\models;


use backend\modules\access\models\AuthAssignment;
use backend\modules\operator\models\query\ListOperatorsQuery;
use common\components\db\ActiveRecord;
use common\models\User;
use common\models\UserCountry;

class ListOperators extends ActiveRecord
{
    public $countries;
    public $teams;
    public $queues;
    public $auto_call;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function rules()
    {
        return [['username', 'country_id'], 'safe'];
    }

    /**
     * @return ListOperatorsQuery
     */
    public static function find()
    {
        return new ListOperatorsQuery(get_called_class());
    }


    /**
     * @param mixed int|array $country_ids
     * @param bool $active   false = all
     * @return UserCountry[]
     */
    public static function getOperators($country_ids, $active = true)
    {
        $country_ids = is_array($country_ids) ? $country_ids : [$country_ids];

        $query = User::find()
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName() . '.user_id=' . User::tableName() . '.id')
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id=' . User::tableName() . '.id')
            ->where([
                AuthAssignment::tableName() . '.item_name' => [
                    User::ROLE_APPLICANT,
                    User::ROLE_OPERATOR,
                    User::ROLE_SENIOR_OPERATOR
                ]
            ]);

        if ($active) {
            $query->where(['not in', User::tableName() . '.status', [User::STATUS_DELETED, User::STATUS_BLOCKED]]);
        }

        $query->andWhere([UserCountry::tableName().'.country_id' => $country_ids]);

        return $query->all();
    }
}
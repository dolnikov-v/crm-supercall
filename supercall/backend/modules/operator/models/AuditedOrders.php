<?php

namespace backend\modules\operator\models;

use common\models\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProductLog;
use Yii;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class AuditedOrders
 * @package backend\modules\operator\models
 */
class AuditedOrders extends Order
{

    const TYPE_JSON = 'json';
    const TYPE_STRING = 'string';

    const FIELD_CUSTOMER_COMPONENTS = 'customer_components';

    const FIELD_IGNORE_CONFIRM_ADDRESS = 'confirm_address';
    const FIELD_IGNORE_ATTEMPTS = 'attempts';
    const FIELD_IGNORE_BLOCKED_TO = 'blocked_to';
    const FIELD_IGNORE_ZIP = 'zip';
    const FIELD_IGNORE_FULL_NAME_CLIENT = 'customer_full_name';
    const FIELD_IGNORE_CUSTOMER_ADDRESS = 'customer_address';
    const FIELD_IGNORE_CUSTOMER_EMAIL = 'customer_email';
    const FIELD_IGNORE_CUSTOMER_MOBILE = 'customer_mobile';

    const ACTION_PRODUCT_CREATE = 'create';
    const ACTION_PRODUCT_DELETE = 'delete';
    const ACTION_PRODUCT_UPDATE = 'update';

    const MODIFICATION_TYPE_ADDRESS = 'address';
    const MODIFICATION_TYPE_SHIPPING_INFO = 'shipping_info';
    const MODIFICATION_TYPE_GOODS = 'goods';
    const MODIFICATION_TYPE_STATUS = 'status';

    /** @var  integer */
    public $controller_id;
    public $operator_id;
    public $order_id;

    /** @var  string */
    public $date_range;

    /** @var  string */
    public $modification_type;

    /**
     * @return array
     */
    public static function modificationTypesCollection()
    {
        return [
            self::MODIFICATION_TYPE_ADDRESS => yii::t('common', 'Адрес'),
            self::MODIFICATION_TYPE_SHIPPING_INFO => yii::t('common', 'Информация о доставке'),
            self::MODIFICATION_TYPE_GOODS => yii::t('common', 'Товары'),
            self::MODIFICATION_TYPE_STATUS => yii::t('common', 'Статус')
        ];
    }

    /**
     * @param $code
     * @return bool|mixed
     */
    public static function getModificationTypesByCode($code)
    {
        $typesCollection = self::modificationTypesCollection();

        return isset($typesCollection[$code]) ? $typesCollection[$code] : false;
    }

    /**
     * @return array
     */
    public static function fieldJSONCollection()
    {
        return [
            self::FIELD_CUSTOMER_COMPONENTS
        ];
    }

    public static function fieldIgnoreCollection()
    {
        return [
            self::FIELD_IGNORE_ATTEMPTS,
            self::FIELD_IGNORE_BLOCKED_TO,
            self::FIELD_IGNORE_CONFIRM_ADDRESS,
            self::FIELD_IGNORE_ZIP,
            self::FIELD_IGNORE_FULL_NAME_CLIENT,
            self::FIELD_IGNORE_CUSTOMER_ADDRESS,
            self::FIELD_IGNORE_CUSTOMER_EMAIL,
            self::FIELD_IGNORE_CUSTOMER_MOBILE
        ];
    }

    public static function ActionCollection()
    {
        return [
            self::ACTION_PRODUCT_CREATE,
            self::ACTION_PRODUCT_DELETE,
            self::ACTION_PRODUCT_UPDATE,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQualityControl()
    {
        return $this->hasOne(QualityControl::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getController()
    {
        return $this->hasOne(User::className(), ['id' => 'controller_id']);
    }

    /**
     * @param $order_id
     * @param $controller_id
     * @return Query
     */
    public static function getOrderLogsByAuditor($order_id, $controller_id)
    {
        return OrderLog::find()
            ->where([
                'order_id' => $order_id,
                'user_id' => $controller_id
            ]);
    }

    /**
     * @param $order_id
     * @param $fields
     * @return Query
     */
    public static function getOrderLogsByOperators($order_id, $controller_id, $fields)
    {
        return OrderLog::find()
            ->where([
                'order_id' => $order_id,
                'field' => $fields
            ])
            ->andWhere(['is not', 'user_id', null])
            ->andWhere(['<>', 'user_id', $controller_id])
            ->orderBy(['id' => SORT_ASC]);
    }

    /**
     * @param $order_id
     * @param $controller_id
     * @return Query
     */
    public static function getOrderProductLogsByAuditor($order_id, $controller_id)
    {
        return OrderProductLog::find()
            ->joinWith(['product'])
            ->where([
                'order_id' => $order_id,
                'user_id' => $controller_id
            ])->orderBy(['id' => SORT_ASC]);
    }

    /**
     * @param $modification_type
     * @return array
     */
    public static function getFieldByModificationType($modification_type)
    {
        switch ($modification_type) {
            case self::MODIFICATION_TYPE_STATUS:
                $fields = [
                    'status',
                    'sub_status',
                ];
                break;
            case  self::MODIFICATION_TYPE_GOODS:
                $fields = ['action'];
                break;
            case self::MODIFICATION_TYPE_ADDRESS:
                $fields = [
                    'customer_address',
                    'customer_components'
                ];
                break;
            case self::MODIFICATION_TYPE_SHIPPING_INFO:
                $fields = [
                    'shipping_price',
                    'shipping_id',
                    'delivery_from',
                    'delivery_to',
                ];
                break;
            default:
                $fields = [];
                break;
        }

        return $fields;
    }

    /**
     * Данные по типам изменений заказов
     * Array
     *   (
     *     [shipping_info] => 0
     *     [status] => 2
     *     [address] => 2
     *     [goods] => 3
     *   )
     * @param $order_id
     * @return array
     */
    public static function getModificationTypes($order_id)
    {
        $modificationsQuery = QualityControl::find()
            ->select([
                self::MODIFICATION_TYPE_SHIPPING_INFO => new Expression("case when " . OrderLog::tableName() . ".field::text in (" . implode(", ", array_map(function ($v) {
                        return "'" . $v . "'";
                    }, self::getFieldByModificationType(self::MODIFICATION_TYPE_SHIPPING_INFO))) . ") then 1 else 0 end"),
                self::MODIFICATION_TYPE_STATUS => "case when " . OrderLog::tableName() . ".field::text in (" . implode(", ", array_map(function ($v) {
                        return "'" . $v . "'";
                    }, self::getFieldByModificationType(self::MODIFICATION_TYPE_STATUS))) . ") then 1 else 0 end",
                self::MODIFICATION_TYPE_ADDRESS => "case when " . OrderLog::tableName() . ".field::text in (" . implode(", ", array_map(function ($v) {
                        return "'" . $v . "'";
                    }, self::getFieldByModificationType(self::MODIFICATION_TYPE_ADDRESS))) . ") then 1 else 0 end",
                self::MODIFICATION_TYPE_GOODS => new Expression("case when " . OrderProductLog::tableName() . ".action='" . AuditedOrders::ACTION_PRODUCT_UPDATE . "' and  ".OrderProductLog::tableName().".user_id = ".QualityControl::tableName().".controller_id  then 1 else 0 end")
            ])
            ->leftJoin(Order::tableName(), Order::tableName() . '.id=' . QualityControl::tableName() . '.order_id')
            ->leftJoin(OrderLog::tableName(), OrderLog::tableName() . '.order_id=' . QualityControl::tableName() . '.order_id')
            ->leftJoin(OrderProductLog::tableName(), OrderProductLog::tableName() . '.order_id=' . QualityControl::tableName() . '.order_id')
            ->where([
                QualityControl::tableName() . '.edit' => true,
                OrderLog::tableName() . '.user_id' => new Expression(QualityControl::tableName() . '.controller_id'),
                QualityControl::tableName() . '.order_id' => $order_id
            ])
            ->andWhere(['<>', OrderLog::tableName() . '.old', new Expression(OrderLog::tableName() . '.new')])
            ->andWhere(['is not', OrderLog::tableName() . '.user_id', null])
            ->groupBy([
                QualityControl::tableName() . '.order_id',
                OrderLog::tableName() . '.field',
                OrderProductLog::tableName() . '.action',
                OrderProductLog::tableName().'.user_id',
                QualityControl::tableName().'.controller_id'])
            ->asArray()
            ->all();

        $modifications = [
            self::MODIFICATION_TYPE_SHIPPING_INFO => 0,
            self::MODIFICATION_TYPE_STATUS => 0,
            self::MODIFICATION_TYPE_ADDRESS => 0,
            self::MODIFICATION_TYPE_GOODS => 0
        ];

        foreach ($modificationsQuery as $k => $group) {
            foreach ($group as $type => $value) {
                $modifications[$type] += $value;
            }
        }

        return $modifications;
    }
}
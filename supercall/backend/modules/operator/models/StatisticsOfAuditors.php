<?php

namespace backend\modules\operator\models;

use yii;
use yii\helpers\Url;

/**
 * Class StatisticsOfAuditors
 * @package backend\modules\operator\models
 */
class StatisticsOfAuditors extends QualityControl
{
    const GROUP_BY_HOUR = 'Час';
    const GROUP_BY_DAY = 'День';
    const GROUP_BY_MONTH = 'Месяц';
    const GROUP_BY_COUNTRY = 'Страна';

    /** @var  array */
    public $date_range;

    /** @var  integer */
    public $country_id;

    /** @var  string */
    public $group;

    /**
     * @return array
     */
    public static function getGroupCollection()
    {
        return [
            self::GROUP_BY_HOUR => yii::t('common', 'Час'),
            self::GROUP_BY_DAY => yii::t('common', 'День'),
            self::GROUP_BY_MONTH => yii::t('common', 'Месяц'),
            self::GROUP_BY_COUNTRY => yii::t('common', 'Страна'),
        ];
    }

    /**
     * @return string
     */
    public function getAggregatedGroup()
    {
        $group_hour = 'extract(epoch from date_trunc(\'hour\', to_timestamp(' . QualityControl::tableName() . '.created_at) at time zone \'UTC\'))';
        $group_day = 'extract(epoch from date_trunc(\'day\', to_timestamp(' . QualityControl::tableName() . '.created_at) at time zone \'UTC\'))';
        $group_month = 'extract(epoch from date_trunc(\'month\', to_timestamp(' . QualityControl::tableName() . '.created_at) at time zone \'UTC\'))';

        switch ($this->group) {
            case self::GROUP_BY_HOUR :
                $group = $group_hour;
                break;
            case self::GROUP_BY_DAY :
                $group = $group_day;
                break;
            case self::GROUP_BY_MONTH :
                $group = $group_month;
                break;
            case self::GROUP_BY_COUNTRY :
                $group = 'country.name';
                break;
            default :
                $group = 'controller_id';
                break;
        }

        return $group;
    }

    /**
     * @param $row
     * @return false|string
     */
    public function formatGroupView($row)
    {
        switch ($this->group) {
            case self::GROUP_BY_HOUR:
                return date('d/m/Y H:00', $row['group']);
                break;
            case self::GROUP_BY_DAY:
                return date('d/m/Y', $row['group']);
                break;
            case static::GROUP_BY_MONTH:
                return date('F Y', $row['group']);
                break;
            default:
                return yii::t('common', $row['group']);
                break;
        }
    }

    /**
     * @param $model
     * @param $modelSearch
     * @param $status
     * @return null|string
     */
    public static function generateUrl($model,$modelSearch, $status)
    {
        $cell = 'status_quantity_' . $status;
        return $model->$cell > 0
            ? Url::toRoute([
                '/operator/audited-orders',
                'AuditedOrdersSearch' => [
                    'controller_id' => $model->controller_id,
                    'operator_id' => $modelSearch->operator_id,
                    'status' => $status,
                    'date_range' => $modelSearch->date_range,
                    'country_id' => $modelSearch->country_id,
                ]
            ]) : null;
    }
}
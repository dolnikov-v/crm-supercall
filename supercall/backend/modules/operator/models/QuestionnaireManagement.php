<?php

namespace backend\modules\operator\models;

use common\components\db\ActiveRecord;
use common\modules\order\models\Order;
use backend\modules\operator\models\query\QuestionnaireManagementQuery;
use Yii;

class QuestionnaireManagement extends ActiveRecord
{
    const TYPE_TEXT = 'text';
    const TYPE_OPTIONS = 'options';

    const ANSWER_1_3 = '1_3';
    const ANSWER_YES_NO = 'yes_no';
    const ANSWER_YES_NO_NA = 'yes_no_na';
    const ANSWER_OTHER = 'other';

    /**
     * @var []
     */
    public $answers_other;

    /** @var  string */
    public $type_answers;

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $post = yii::$app->request->post('QuestionnaireManagement');
        $mappedAnswers = self::mappingAnswers();
        $answerCollections = self::getAnswersCollection();

        if ($post['type_answers'] == self::TYPE_TEXT) {
            $this->answers = $mappedAnswers[self::TYPE_TEXT];
        } elseif ($post['type_answers'] == self::TYPE_OPTIONS) {
            if ($post['answers'] != self::ANSWER_OTHER &&  in_array($post['answers'], array_keys($answerCollections))) {
                $this->answers = $mappedAnswers[$post['answers']];
            } else {
                $this->answers = json_encode(['type' => self::TYPE_OPTIONS, 'mask' => false, 'answers' => $post['answers_other']], JSON_UNESCAPED_UNICODE);
            }
        }

        $this->required = isset($this->required) ? 1 : 0;
        $this->order_status = array_map('intval', $this->order_status);
        $this->order_status = json_encode($this->order_status);

        return parent::beforeSave($insert);
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%questionnaire}}';
    }

    /**
     * @return QuestionnaireManagementQuery
     */
    public static function find()
    {
        return new QuestionnaireManagementQuery(get_called_class());
    }

    /**
     * @param $attribute
     */
    public function validateOrderStatuses($attribute)
    {
        foreach ($this->$attribute as $order_status) {
            if (!in_array($order_status, array_keys(self::getOrderStatusesCollection()))) {
                $this->addError($attribute, yii::t('common', 'Неверное значение статуса заказа'));
                break;
            }
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                [
                    'question_text',
                    'order_status'
                ],
                'required'
            ],
            ['question_text', 'string'],
            ['order_status', 'validateOrderStatuses'],
            ['answers', 'string'],
            [
                [
                    'required'
                ],
                'in',
                'range' => [
                    0,
                    1
                ]
            ],
            [
                [
                    'answers_other'
                ],
                'each',
                'rule' => ['string']
            ],
            [
                [
                    'sort',
                    'created_at',
                    'updated_at'
                ],
                'integer'
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'question_text' => Yii::t('common', 'Вопрос'),
            'answers' => Yii::t('common', 'Варианты ответoв'),
            'order_status' => Yii::t('common', 'Статус заказа'),
            'required' => Yii::t('common', 'Ответ обязателен'),
            'sort' => Yii::t('common', 'Порядок сортировки'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен'),
            'type_answers' => Yii::t('common', 'Тип ответа на вопрос'),
            'answers_other' => Yii::t('common', 'Введите варианты ответов, разделяя их запятыми или нажатием на клавишу ENTER')
        ];
    }

    /**
     * @return array
     */
    public static function getTypeAnswersCollection()
    {
        return [
            self::TYPE_TEXT => Yii::t('common', 'Произвольный ответ'),
            self::TYPE_OPTIONS => Yii::t('common', 'Варианты ответов'),
        ];
    }

    /**
     * @return array
     */
    public static function getAnswersCollection()
    {
        return [
            self::ANSWER_1_3 => "1 / 2 / 3",
            self::ANSWER_YES_NO => yii::t("common", "Да / Нет"),
            self::ANSWER_YES_NO_NA => yii::t("common", "Да / Нет / Не определено"),
            self::ANSWER_OTHER => yii::t("common", "Свой вариант")
        ];
    }

    /**
     * @return array
     */
    public static function getOrderStatusesCollection()
    {
        return Order::getQuestionnaireStatuses();
    }

    /**
     * @return array
     */
    function mappingAnswers()
    {
        $modelAnswers = QuestionnaireManagement::getAnswersCollection();

        return [
            self::ANSWER_1_3 => json_encode([
                'type' => self::TYPE_OPTIONS,
                'mask' => self::ANSWER_1_3,
                'answers' => range(1, 3)
            ]),
            self::ANSWER_YES_NO => json_encode([
                'type' => self::TYPE_OPTIONS,
                'mask' => self::ANSWER_YES_NO,
                'answers' => explode('/', $modelAnswers[QuestionnaireManagement::ANSWER_YES_NO])
            ], JSON_UNESCAPED_UNICODE),
            self::ANSWER_YES_NO_NA => json_encode([
                'type' => self::TYPE_OPTIONS,
                'mask' => self::ANSWER_YES_NO_NA,
                'answers' => explode('/', $modelAnswers[QuestionnaireManagement::ANSWER_YES_NO_NA])
            ], JSON_UNESCAPED_UNICODE),
            self::TYPE_TEXT => json_encode([
                'type' => QuestionnaireManagement::TYPE_TEXT,
                'mask' => false,
            ], JSON_UNESCAPED_UNICODE)
        ];
    }


}
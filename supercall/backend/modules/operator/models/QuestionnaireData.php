<?php
namespace backend\modules\operator\models;

use common\modules\order\models\Order;
use common\components\db\ActiveRecord;
use backend\modules\operator\models\QuestionnaireManagement;
use backend\modules\operator\models\query\QuestionnaireDataQuery;
use Yii;

/**
 * Class QuestionnaireData
 * @package backend\modules\operator\models
 * @property integer $id
 * @property integer $questionnaire_order_id
 * @property integer $questionnaire_id
 * @property integer $questionnaire_answer
 * @property integer $created_at
 * @property integer $updated_at
 *
 */
class QuestionnaireData extends ActiveRecord
{
    const ORDER_FINISHED = 1;
    const ORDER_NOT_FINISHED = 0;

    /**
     * @inheritdocs
     */
    public static function tableName()
    {
        return '{{%questionnaire_data}}';
    }

    /**
     * @return QuestionnaireDataQuery
     */
    public static function find()
    {
        return new QuestionnaireDataQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionnaire_order_id', 'questionnaire_id', 'question_text', 'created_at'], 'required'],
            [['questionnaire_order_id', 'questionnaire_id', 'created_at', 'created_at', 'updated_at'], 'integer'],
            [['answer_text', 'question_text'], 'string'],
            [['questionnaire_order_id'], 'in', 'range' => array_values(QuestionnaireOrder::find()->collection())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'questionnaire_order_id' => Yii::t('common', 'Номер в плане оценки'),
            'questionnaire_id' => Yii::t('common', 'Номер вопроса в справочнике'),
            'question_text' => Yii::t('common', 'Вопрос'),
            'answer_text' => Yii::t('common', 'Ответ'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionnaire()
    {
        return $this->hasOne(QuestionnaireManagement::className(), ['id' => 'questionnaire_id']);
    }

}

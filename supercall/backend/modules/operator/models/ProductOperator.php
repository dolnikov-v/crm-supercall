<?php
namespace backend\modules\operator\models;

use common\components\db\ActiveRecord;
use backend\modules\access\models\query\ProductOperatorQuery;
use common\components\web\User;
use common\models\Product;
use common\models\UserCountry;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ProductOperator
 * @package backend\modules\operator\models
 * @property integer $user_id
 * @property integer $product_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class ProductOperator extends ActiveRecord
{
    /**
     * @var []
     */
    public $product_ids;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_operator}}';
    }

    /**
     * @return ProductOperatorQuery
     */
    public static function find()
    {
        return new ProductOperatorQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['product_id'], 'integer'],
            ['product_ids', 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'product_ids' => Yii::t('common', 'Товары'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public static function getProductsOnOrder($order)
    {

    }

    /**
     * @param $product_id
     * @return bool|integer
     */
    public static function getOperatorByProduct($product_id)
    {
        $operator = ProductOperator::find()
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName().'.user_id='.ProductOperator::tableName().'.user_id')
            ->where(['=', 'product_id', $product_id])
            ->andWhere([UserCountry::tableName().'.country_id' => yii::$app->user->country->id])
            ->one();

        return is_null($operator) ? false : $operator->user_id;
    }
}

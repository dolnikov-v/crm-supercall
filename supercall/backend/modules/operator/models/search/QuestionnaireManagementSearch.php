<?php
namespace backend\modules\operator\models\search;

use backend\modules\operator\models\QuestionnaireManagement;
use yii\data\ActiveDataProvider;
use Yii;
use yii\helpers\Json;

/**
 * Class QuestionnaireManagement
 * @package backend\modules\catalog\models\search
 */
class QuestionnaireManagementSearch extends QuestionnaireManagement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_text', 'order_status'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = QuestionnaireManagement::find()->orderBy($sort);

        $formName = 'QuestionnaireManagementSearch';

        if(isset($params[$formName])) {

            $order_status = $params[$formName]['order_status'];
            $question_text = $params[$formName]['question_text'];

            if (!empty($order_status)) {
                $sql = "select * from \"questionnaire\" where order_status::jsonb @> '[{$order_status}]'::jsonb";

                if (!empty($question_text)) {
                    $sql .= " and question_text like '%{$question_text}%'";
                }

                $query = QuestionnaireManagement::findBySql($sql);
            }
        }
        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params, 'QuestionnaireManagementSearch');

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'question_text', $this->question_text]);

        return $dataProvider;
    }
}

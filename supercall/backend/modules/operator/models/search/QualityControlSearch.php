<?php

namespace backend\modules\operator\models\search;

use backend\modules\operator\models\OrderAudit;
use backend\modules\operator\models\QualityControl;
use common\models\Timezone;
use common\modules\call\models\CallHistory;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderType;
use Yii;
use yii\data\ActiveDataProvider;
use common\modules\order\models\Order;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class QualityControlSearch extends QualityControl
{
    public $orderNotByRand;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['countries', 'status', 'id', 'date_range', 'operator_id', 'partner_id'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     * @throws HttpException
     */
    public function search($params = [])
    {

        $this->clearOldAudits();

        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $this->orderNotByRand = true;

        $get = yii::$app->request->get('QualityControlSearch');

        if (isset($get['id'])) {
            $query = QualityControl::getAudit($get['id']);

            if (!$query) {
                throw new HttpException(500, Yii::t('common', 'При попытке получить заказ по аудиту произошла ошибка'));
            }

        } else {
            $query = QualityControl::getOrderForAudit($this->orderNotByRand);
        }

        //нельзя взять и так просто без лимита залезть в логи заказов и заджойнить таблицу заказов в довес.
        $query->limit(1000);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!empty($params)) {
            if (!$this->load($params, '') || !$this->validate(['status', 'countries'], false)) {
                return $dataProvider;
            }
        }

        if($this->partner_id){
            $query->andWhere([Order::tableName().'.partner_id' => $this->partner_id]);
        }

        //Список проверенных заказов
        //в таблице аудита нужно отсеять все аудиты - с другими контроллерами и те, у которых аудит уже проведён
        $list = QualityControl::find()
            ->where(['<>', 'controller_id', yii::$app->user->identity->id])
            ->orWhere(['is not', 'confirm', null])
            ->all();

        $orders_audit = ArrayHelper::getColumn($list, 'order_id');

        if (!$this->id) {
            $query->andWhere(['not in', Order::tableName() . '.id', $orders_audit]);
        }

        if ($this->status && !empty($this->status)) {
            $query->andWhere(['status' => $this->status]);
            $query->andWhere(['<>', 'type_id', OrderType::TYPE_CHECK_ADDRESS]);
        }

        if (!$this->status || empty($this->status)) {
            if (!isset($get['id'])) {
                $query->orderBy(new Expression('CASE WHEN type_id = ' . OrderType::TYPE_CHECK_ADDRESS . ' THEN 1 else 2 end ASC, CASE WHEN status = ' . Order::STATUS_APPROVED . ' THEN 1 else 2 end ASC, "order".updated_at DESC'));
            }else{
                $query->leftJoin(Order::tableName(), Order::tableName().'.id='.QualityControl::tableName().'.order_id');
            }
        }

        if ($this->order_id) {
            $query->andWhere(['order_id' => $this->order_id]);
            $query->orderBy([Order::tableName() . '.updated_at' => SORT_DESC]);
        }

        if ($this->operator_id) {
            $subQuery = OrderLog::find()
                ->select([
                    'id' => new Expression('max(id)'),
                    'order_id',
                    'user_id'
                ])
                ->where(['field' => 'status'])
                ->andWhere(['is not', 'user_id', null])
                ->groupBy(['id', 'order_id'])
                ->orderBy(['order_log.id' => SORT_DESC]);

            $query->leftJoin(['order_log' => $subQuery], 'order_log.order_id=' . Order::tableName() . '.id');
            $query->andWhere(['order_log.user_id' => $this->operator_id]);
        }

        if ($this->controller_id) {
            $query->andWhere(['controller_id' => $this->controller_id]);
        }

        if ($this->edit) {
            $query->andWhere(['edit' => ($this->edit == 1 ? true : false)]);
        }

        if ($this->countries) {
            $query->andWhere(['order.country_id' => $this->countries]);
        }


        if (!$this->date_range) {
            $this->date_range = date('d/m/Y 00:00') . ' - ' . date('d/m/Y 23:59');
        }

        if (!isset($get['id'])) {
            $date = Yii::$app->formatter->asConvertToFrom($this->date_range);

            $query->andWhere(['>=', Order::tableName() . '.updated_at', $date->from->getTimestamp()]);
            $query->andWhere(['<=', Order::tableName() . '.updated_at', $date->to->getTimestamp()]);
        }

        $query->andWhere([Order::tableName().'.partner_id' => ArrayHelper::getColumn(yii::$app->user->identity->partners, 'id')]);

        return $dataProvider;
    }

    /**
     * Если аудит провесел более 1 часа - удалить.
     */
    public function clearOldAudits()
    {
        $current_time = (new \DateTime())->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))->getTimestamp();
        $limit = $current_time - 3600;

        $oldAudits = QualityControl::find()
            ->where(['<=', 'updated_at', $limit])
            ->andWhere(['is', 'confirm', null])
            ->all();

        if ($oldAudits) {
            foreach ($oldAudits as $audit) {
                $audit->delete();
            }
        }

        //так же - если при поиске заказов есть заказы с confirm=null и controller_id = аудитору
        //для данных заказов - данные в queue_order тоже нужно удалить - чтобы отобразить эти заказы для данного аудитора
        $user_id = yii::$app->user->identity->id;
        $listActiveAudit = QualityControl::find()
            ->where(['controller_id' => $user_id])
            ->andWhere(['is', 'confirm', null]);

        if ($listActiveAudit->exists()) {
            $blocked_orders = implode(', ', ArrayHelper::getColumn($listActiveAudit->all(), 'order_id'));
            $delete = yii::$app->db->createCommand('delete from queue_order where order_id in (' . $blocked_orders . ') and user_id = ' . $user_id)->execute();
        }
    }
}
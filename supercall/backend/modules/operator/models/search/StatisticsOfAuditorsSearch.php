<?php
namespace backend\modules\operator\models\search;

use backend\modules\operator\models\QualityControl;
use backend\modules\operator\models\StatisticsOfAuditors;
use common\models\Country;
use common\models\User;
use common\modules\order\models\Order;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * Class StatisticsOfAuditorsSearch
 * @package backend\modules\operator\models\search
 */
class StatisticsOfAuditorsSearch extends StatisticsOfAuditors
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['controller_id', 'operator_id', 'country_id', 'date_range', 'group'], 'safe']
        ];
    }


    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = QualityControl::find()
            ->joinWith(['order', 'controller'])
            ->where(['confirm' => true])
            ->groupBy(['group', 'user.username', 'user.id']);

        $query->leftJoin(Country::tableName(), Country::tableName().'.id="order".country_id');

        $select = [];

        foreach (Order::getStatusesCollection() as $id => $name) {
            $statusField = 'CASE WHEN operator_status::integer = ' . $id . ' THEN 1 ELSE 0 END';
            $select[] = new Expression('SUM(' . $statusField . ') as status_quantity_' . $id);
        }

        $query->select($select);

        $config = [
            'query' => $query
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        $query->addSelect([
            'group' => $this->getAggregatedGroup(),
            'total' => new Expression('COUNT(*)'),
            'username' => 'user.username',
            'controller_id' => 'user.id'
        ]);

        if($this->group == StatisticsOfAuditors::GROUP_BY_COUNTRY){
            $query->addGroupBy('country.name');
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->country_id){
            $query->andWhere([Country::tableName().'.id' => $this->country_id]);
        }

        if($this->controller_id){
            $query->andWhere(['controller_id' => $this->controller_id]);
        }

        if($this->operator_id){
            $query->andWhere(['operator_id' => $this->operator_id]);
        }

        if (empty($this->date_range)) {
            $this->date_range = date('d/m/Y 00:00') . ' - ' . date('d/m/Y 23:59');
        }

        $date = Yii::$app->formatter->asConvertToFrom($this->date_range);

        $query->andWhere(['>=', QualityControl::tableName().'.updated_at', $date->from->getTimestamp()]);
        $query->andWhere(['<=', QualityControl::tableName().'.updated_at', $date->to->getTimestamp()]);

        return $dataProvider;
    }

}
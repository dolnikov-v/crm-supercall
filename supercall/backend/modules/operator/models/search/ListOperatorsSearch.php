<?php

namespace backend\modules\operator\models\search;

use backend\modules\operator\models\ProductOperator;
use backend\modules\queue\models\QueueUser;
use common\models\Product;
use common\models\UserCountry;
use common\models\UserPartner;
use common\models\UserTeam;
use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\operator\models\ListOperators;
use backend\modules\access\models\AuthAssignment;
use common\models\User;
use yii\helpers\ArrayHelper;

class ListOperatorsSearch extends ListOperators
{
    public $product;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'id', 'countries', 'teams', 'queues', 'auto_call', 'product'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $partner = UserPartner::findOne(['user_id' => Yii::$app->user->id]);

        $query = User::find()
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id::integer=' . User::tableName() . '.id')
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName() . '.user_id=' . User::tableName() . '.id')
            ->leftJoin(UserPartner::tableName(),UserPartner::tableName().'.user_id=' . User::tableName() . '.id')
            ->where(['in', AuthAssignment::tableName() . '.item_name', [User::ROLE_OPERATOR, User::ROLE_APPLICANT]])
            ->andWhere(['in', UserCountry::tableName() . '.country_id', ArrayHelper::getColumn(User::getAllowCountries()->all(), 'country_id')])
            ->groupBy('user.id')
            ->orderBy($sort);

        if($partner){
            $query->andWhere([UserPartner::tableName().'.partner_id' => $partner->partner_id]);
        }

        $config = [
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ]
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        if($this->username){
            $query->andWhere(['like', 'username', $this->username]);
        }

        if($this->countries){
            $query->andWhere(['user_country.country_id' => $this->countries]);
        }

        //Т.к. postgres принимает только integer
        if($this->id){
            $query->andWhere(['user.id' => (int)preg_replace('/[^0-9]/', '', $this->id)]);
        }

        if($this->teams){
            $query->leftJoin(UserTeam::tableName(), UserTeam::tableName().'.user_id='.User::tableName().'.id');
            $query->andWhere(['user_team.id' => $this->teams]);
        }

        if($this->queues){
            $query->leftJoin(QueueUser::tableName(), QueueUser::tableName().'.user_id='.User::tableName().'.id');
            $query->andWhere(['queue_user.queue_id' => $this->queues]);
        }
        
        if ($this->auto_call > -1){
            $query->andWhere(['user.auto_call' => $this->auto_call]);
        }


        if($this->product){
            $query->leftJoin(ProductOperator::tableName(), ProductOperator::tableName().'.user_id='.User::tableName().'.id');
            $query->andWhere([ProductOperator::tableName().'.product_id' => $this->product]);
        }

        return $dataProvider;
    }

    public function getProduct()
    {
        return Product::find()->where(['active' => 1])->asArray()->all();
    }
}
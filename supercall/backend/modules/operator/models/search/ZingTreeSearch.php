<?php

namespace backend\modules\operator\models\search;


use backend\modules\operator\models\ZingTree;
use common\models\Country;
use common\models\Product;
use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class ZingTreeSearch extends ZingTree
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'country_id', 'product_id', 'order_type_id', 'active'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = ZingTree::find()
            ->joinWith(['country', 'product', 'orderTypes'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        if ($this->name) {
            $query->andWhere(['like', ZingTree::tableName().'.name', $this->name]);
        }

        if ($this->country_id) {
            $query->andWhere(['country_id' => $this->country_id]);
        }

        if ($this->product_id) {
            $query->andWhere(['product_id' => $this->product_id]);
        }

        if ($this->order_type_id) {
            $query->andWhere(['order_type_id' => $this->order_type_id]);
        }

        if (in_array($this->active, [ZingTree::ACTIVE, ZingTree::NOT_ACTIVE]) && $this->active != '') {
            $query->andWhere([ZingTree::tableName() . '.active' => $this->active]);
        }
        
        return $dataProvider;
    }
}
<?php
namespace backend\modules\operator\models\search;

use backend\modules\access\models\AuthItem;
use backend\modules\operator\models\Request;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use backend\modules\access\models\AuthAssignment;
use common\models\UserCountry;
use common\models\Media;
use yii\db\Expression;

/**
 * Class RequestSearch
 * @package backend\modules\operator\models\search
 */
class RequestSearch extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['phone', 'email', 'role', 'status', 'created_at'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort[User::tableName() . '.id'] = SORT_DESC;
        }

        $query = User::find()
            ->select([
                User::tableName().'.*',
                'role' => AuthItem::tableName() . '.description'
            ])
            ->where(
                [
                    'status' => User::STATUS_APPLICANT,
                    AuthAssignment::tableName() . '.item_name' => User::ROLE_APPLICANT
                ]
            )
            ->orWhere(
                [
                    'status' => User::STATUS_BLOCKED,
                    AuthAssignment::tableName() . '.item_name' => User::ROLE_APPLICANT
                ]
            )
            ->orWhere(
                [
                    'status' => User::STATUS_BLOCKED,
                    AuthAssignment::tableName() . '.item_name' => User::ROLE_OPERATOR
                ]
            )
            ->orWhere(
                [
                    'status' => User::STATUS_APPLICANT,
                    AuthAssignment::tableName() . '.item_name' => User::ROLE_OPERATOR
                ]
            )
            ->andWhere(['country_id' => ArrayHelper::getColumn(User::getAllowCountries()->all(), 'country_id')])
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id::integer=' . User::tableName() . '.id')
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName() . '.user_id=' . User::tableName() . '.id')
            ->leftJoin(AuthItem::tableName(), AuthItem::tableName().'.name='.AuthAssignment::tableName() . '.item_name')
            ->andWhere(['in', UserCountry::tableName() . '.country_id', ArrayHelper::getColumn(User::getAllowCountries()->all(), 'country_id')])
            ->orderBy($sort);


        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->created_at){
            $period = explode(' - ', $this->created_at);

            $query->andFilterWhere(['>=', User::tableName().'.created_at', strtotime(date($period[0]))]);
            $query->andFilterWhere(['<=', USer::tableName().'.created_at', strtotime(date($period[1]))]);
        }

        if($this->role){
            $query->andFilterWhere([AuthAssignment::tableName() . '.item_name' => $this->role]);
        }

        if($this->status){
            $query->andFilterWhere([User::tableName() . '.status' => $this->status]);
        }

        if($this->email){
            $query->andFilterWhere(['like', User::tableName() . '.email',  $this->email]);
        }

        if($this->phone){
            $query->andFilterWhere(['like', User::tableName() . '.phone', $this->phone]);
        }

        return $dataProvider;
    }
}
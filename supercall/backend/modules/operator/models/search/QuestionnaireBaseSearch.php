<?php
namespace backend\modules\operator\models\search;

use backend\modules\operator\models\QuestionnaireBase;
use yii\data\ActiveDataProvider;
use yii;

/**
 * Class QuestionnaireBaseSearch
 * @package backend\modules\operator\models\search
 */
class QuestionnaireBaseSearch extends QuestionnaireBase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = QuestionnaireBase::find()
            ->with(['controller', 'operator', 'country'])
            ->where(['controller_id' => yii::$app->user->id])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

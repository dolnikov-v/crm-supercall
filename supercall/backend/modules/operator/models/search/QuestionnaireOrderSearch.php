<?php
namespace backend\modules\operator\models\search;

use backend\modules\operator\models\QuestionnaireOrder;
use yii\data\ActiveDataProvider;

/**
 * Class QuestionnaireOrderSearch
 * @package backend\modules\operator\models\search
 */
class QuestionnaireOrderSearch extends QuestionnaireOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionnaire_base_id'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = QuestionnaireOrder::find()
            ->joinWith('order')
            ->orderBy($sort);


        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if(isset($params['id'])) {
            $query->andWhere([QuestionnaireOrder::tableName() . '.questionnaire_base_id' => $params['id']]);
        }

        return $dataProvider;
    }
}

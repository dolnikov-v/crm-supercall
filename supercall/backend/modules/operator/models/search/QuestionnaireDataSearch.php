<?php
namespace backend\modules\operator\models\search;

use backend\modules\operator\models\QuestionnaireData;
use yii\data\ActiveDataProvider;

/**
 * Class QuestionnaireDataSearch
 * @package backend\modules\operator\models\search
 */
class QuestionnaireDataSearch extends QuestionnaireData
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = QuestionnaireData::find()
            ->joinWith('order')
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

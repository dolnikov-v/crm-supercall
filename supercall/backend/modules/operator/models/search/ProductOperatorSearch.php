<?php
namespace backend\modules\operator\models\search;

use backend\modules\operator\models\ProductOperator;
use yii\data\ActiveDataProvider;

/**
 * Class ProductOperatorSearch
 * @package backend\modules\operator\models\search
 */
class ProductOperatorSearch extends ProductOperator
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = ProductOperator::find()
            ->with(['product'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

<?php

namespace backend\modules\operator\models\search;

use backend\modules\operator\models\AuditedOrders;
use backend\modules\operator\models\QualityControl;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProductLog;
use common\modules\partner\models\Partner;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class AuditedOrdersSearch
 * @package backend\modules\operator\models\search
 */
class AuditedOrdersSearch extends AuditedOrders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['controller_id', 'operator_id', 'period', 'status', 'country_id', 'order_id', 'foreign_id', 'date_range', 'modification_type', 'partner_id'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = QualityControl::find()
            ->joinWith(['order', 'controller'])
            ->where([Order::tableName() . '.country_id' => ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id')])
            ->andWhere(['confirm' => true])
            ->orderBy($sort);

        $config = [
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ]
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        if ($this->controller_id) {
            $query->andWhere(['controller_id' => $this->controller_id]);
        }

        if ($this->operator_id) {
            $query->andWhere(['operator_id' => $this->operator_id]);
        }


        if ($this->status) {
            $query->andWhere(['operator_status' => $this->status]);
        }

        if ($this->country_id) {
            $query->andWhere([Order::tableName() . '.country_id' => $this->country_id]);
        }

        if ($this->order_id) {
            $query->andWhere([Order::tableName() . '.id' => $this->order_id]);
        }

        if ($this->foreign_id) {
            $query->andWhere([Order::tableName() . '.foreign_id' => $this->foreign_id]);
        }

        if ($this->date_range) {
            $dateCreatedRange = Yii::$app->formatter->asConvertToFrom($this->date_range);

            $dateCreatedRange->from = is_object($dateCreatedRange->from) ? $dateCreatedRange->from->format('Y-m-d H:i:s') : $dateCreatedRange->from;
            $dateCreatedRange->to = is_object($dateCreatedRange->to) ? $dateCreatedRange->to->format('Y-m-d H:i:s') : $dateCreatedRange->to;

            $date_from = new \DateTime($dateCreatedRange->from, new \DateTimeZone(yii::$app->user->timezone->timezone_id));
            $from = $date_from->getTimestamp();

            $date_to = new \DateTime($dateCreatedRange->to, new \DateTimeZone(yii::$app->user->timezone->timezone_id));
            $to = $date_to->getTimestamp();

            $query->andWhere(['between', QualityControl::tableName() . '.updated_at', $from, $to]);
        }

        if ($this->modification_type) {
            $modified_fields = self::getFieldByModificationType($this->modification_type);

            if (!empty($modified_fields)) {
                $query->distinct();
                $query->leftJoin(OrderLog::tableName(), OrderLog::tableName() . '.order_id=' . QualityControl::tableName() . '.order_id');
                $query->andWhere([OrderLog::tableName() . '.user_id' => new Expression(QualityControl::tableName() . '.controller_id')]);
                $query->andWhere(['<>', OrderLog::tableName() . '.old', new Expression(OrderLog::tableName() . '.new')]);
                $query->andWhere(['is not', OrderLog::tableName() . '.user_id', null]);


                $subQuery = (new Query())->from(QualityControl::tableName());
                $subQuery->select([
                    QualityControl::tableName() . '.order_id'
                ]);

                $subQuery->andWhere([QualityControl::tableName() . '.edit' => true]);

                if ($this->modification_type == self::MODIFICATION_TYPE_GOODS) {
                    $subQuery->leftJoin(OrderProductLog::tableName(), OrderProductLog::tableName() . '.order_id=' . Order::tableName() . '.id');
                    foreach ($modified_fields as $k => $modified_field) {
                        if ($modified_field == 'action') {
                            $subQuery->andWhere([OrderProductLog::tableName() . '.' . $modified_field => 'update']);
                        }
                        $subQuery->andWhere([OrderProductLog::tableName() . '.user_id' => new Expression(QualityControl::tableName() . '.controller_id')]);
                    }
                } else {
                    $subQuery->leftJoin(OrderLog::tableName(), OrderLog::tableName() . '.order_id=' . QualityControl::tableName() . '.order_id');
                    $subQuery->andWhere([OrderLog::tableName() . '.field' => $modified_fields]);
                    $subQuery->andWhere([OrderLog::tableName() . '.user_id' => new Expression(QualityControl::tableName() . '.controller_id')]);
                    $subQuery->andWhere(['<>', OrderLog::tableName() . '.old', new Expression(OrderLog::tableName() . '.new')]);
                    $subQuery->andWhere(['is not', OrderLog::tableName() . '.user_id', null]);
                }


                $subQuery->groupBy(QualityControl::tableName() . '.order_id');


                if (isset($from, $to)) {
                    $subQuery->andWhere(['between', QualityControl::tableName() . '.updated_at', $from, $to]);
                }

                if ($this->controller_id) {
                    $subQuery->andWhere(['controller_id' => $this->controller_id]);
                }

                if ($this->operator_id) {
                    $subQuery->andWhere(['operator_id' => $this->operator_id]);
                }

                if ($this->status || $this->country_id || $this->foreign_id) {
                    $subQuery->leftJoin(Order::tableName(), Order::tableName() . '.id=' . QualityControl::tableName() . '.order_id');
                }

                if ($this->status) {
                    $subQuery->andWhere(['operator_status' => $this->status]);
                }

                if ($this->country_id) {
                    $subQuery->andWhere([Order::tableName() . '.country_id' => $this->country_id]);
                }

                if ($this->order_id) {
                    $subQuery->andWhere([QualityControl::tableName() . '.order_id' => $this->order_id]);
                }

                if ($this->foreign_id) {
                    $subQuery->andWhere([Order::tableName() . '.foreign_id' => $this->foreign_id]);
                }

                $query->andWhere([QualityControl::tableName() . '.order_id' => $subQuery]);
            }
        }

        $query->andWhere([Order::tableName().'.partner_id' => ArrayHelper::getColumn(yii::$app->user->identity->partners, 'id')]);


        if ($this->partner_id) {
            $query->andWhere([Order::tableName() . '.partner_id' => $this->partner_id]);
        }

        return $dataProvider;
    }
}
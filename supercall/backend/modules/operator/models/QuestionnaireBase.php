<?php
namespace backend\modules\operator\models;

use backend\modules\access\models\AuthAssignment;
use common\components\db\ActiveRecord;
use common\models\User;
use common\models\Country;
use backend\modules\operator\models\query\QuestionnaireBaseQuery;
use common\models\UserCountry;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class QuestionnaireBase
 * @package @package backend\modules\operator\models
 * @property integer $id
 * @property integer $country_id
 * @property integer $controller_id
 * @property integer $operator_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class QuestionnaireBase extends ActiveRecord
{
    const BASE_FINISHED = 1;
    const BASE_NOT_FINISHED = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%questionnaire_base}}';
    }

    /**
     * @return QuestionnaireBaseQuery
     */
    public static function find()
    {
        return new QuestionnaireBaseQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'controller_id', 'operator_id', 'created_at'], 'required'],
            [['id', 'country_id', 'controller_id', 'operator_id', 'is_finished', 'created_at'], 'integer'],
            [['operator_id'], 'in', 'range' => array_keys($this->getOperators())],
            [['country_id'], 'in', 'range' => array_keys(Country::getAllowCountries())]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'country_id' => Yii::t('common', 'Страна'),
            'controller_id' => Yii::t('common', 'Контролер'),
            'operator_id' => Yii::t('common', 'Оператор'),
            'is_finished' => Yii::t('common', 'Закончен'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен')
        ];
    }


    public static function getOperators()
    {
        $operators = User::find()
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName().'.user_id::integer='.User::tableName().'.id')
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName().'.user_id='.User::tableName().'.id')
            ->where(['in', UserCountry::tableName().'.country_id', array_keys(self::getAllowedCountries())])
            ->andFilterWhere([AuthAssignment::tableName().'.item_name' => 'operator'])
            ->asArray()
            ->all();

        return ArrayHelper::map($operators, 'id', 'username');
    }

    /**
     * @return array
     */
    public static function getAllowedCountries()
    {
        return Country::getAllowCountries();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getController()
    {
        return $this->hasOne(User::className(), ['id' => 'controller_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperator()
    {
        return $this->hasOne(User::className(), ['id' => 'operator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

}

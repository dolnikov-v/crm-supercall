<?php

namespace backend\modules\operator\models;

use backend\modules\operator\models\query\QualityControlQuery;
use backend\modules\order\models\AddressVerification;
use common\components\db\ActiveRecord;
use common\models\User;
use common\modules\call\models\CallHistory;
use common\modules\order\models\asterisk\Records;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderType;
use common\modules\partner\models\PartnerFormAttribute;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * Class QualityControlController
 * @package backend\modules\operator\controllers
 * @property integer $id
 * @property integer $order_id
 * @property integer $operator_id
 * @property integer $controller_id
 * @property boolean $edit
 * @property boolean $confirm
 * @property integer $created_at
 * @property integer $updated_at
 */
class QualityControl extends ActiveRecord
{

    const ORDER_CONFIRM = true;
    const ORDER_EDIT = true;

    /** @var  array */
    public $countries;

    /** @var  integer */
    public $status;
    public $sup_status;
    public $foreign_id;
    public $country_id;

    /** @var  string */
    public $group;
    public $username;
    public $country_name;
    public $date_range;
    public $country;
    /** @var  integer */
    public $partner_id;

    /** @var  integer */
    public $total;
    public $status_quantity_2;
    public $status_quantity_3;
    public $status_quantity_4;
    public $status_quantity_5;
    public $status_quantity_6;
    public $status_quantity_7;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_quality_control}}';
    }

    /**
     * @return QualityControlQuery
     */
    public static function find()
    {
        return new QualityControlQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'controller_id'], 'required'],
            [['controller_id', 'operator_id', 'created_at', 'updated_at', 'operator_status', 'auditor_status'], 'integer'],
            [['edit', 'confirm'], 'boolean']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('common', 'Заказ'),
            'controller_id' => Yii::t('common', 'Контролёр'),
            'edit' => Yii::t('common', 'Отредактирован'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
            'operator_status' => Yii::t('common', 'Статус оператора'),
            'auditor_status' => Yii::t('common', 'Статус аудитора'),
        ];
    }

    /**
     * @param bool $idOnly
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAuditOrders($idOnly = true)
    {
        $auditOrders = QualityControl::find()->all();

        if ($idOnly) {
            return ArrayHelper::getColumn($auditOrders, 'order_id');
        } else {
            return $auditOrders;
        }
    }

    /**
     * @param bool $withoutOrderBy
     * @return Query
     */
    public static function getOrderForAudit($withoutOrderBy = false)
    {
        $list_blocked_orders = (new Query())->from('{{%queue_order}}')->all();

        $blocked_orders_ids = ArrayHelper::getColumn($list_blocked_orders, 'order_id');

        $data = Order::find()
            //новые не нужны
            ->where(['not in', 'status', Order::STATUS_NEW])
            //адрес должен быть подтверждён
            ->andWhere(['<>', 'confirm_address', AddressVerification::ADDRESS_NEED_CONFIRM ])
            ->orderBy(['updated_at' => SORT_DESC]);

        if(!empty($blocked_orders_ids)){
            //Заказы могут быть "в работе"
            $data->andWhere(['not in', Order::tableName().'.id', $blocked_orders_ids]);
        }

        if (!$withoutOrderBy) {
            return $data->orderBy(new Expression('random()'));
        } else {
            return $data;
        }

    }

    /**
     * @param $id
     * @return mixed Order|bool
     * @throws HttpException
     */
    public static function getAudit($id){
        $audit = QualityControl::find()
            ->select([
                'order_id' => QualityControl::tableName().'.order_id'
            ])
            ->where([QualityControl::tableName().'.id' => $id]);

        if(!$audit->exists()){
            throw new HttpException(404, Yii::t('common', 'Аудит не найден'));
        }else{
            $get = yii::$app->request->get('QualityControlSearch');

            if(!isset($get['id'])) {
                $order = Order::find()->where(['id' => $audit->one()]);

                if (!$order->exists()) {
                    throw new HttpException(404, Yii::t('common', 'Заказ не найден'));
                }

                return $order;
            }else{
                return $audit;
            }
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getController()
    {
        return $this->hasOne(User::className(), ['id' => 'controller_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
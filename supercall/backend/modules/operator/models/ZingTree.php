<?php

namespace backend\modules\operator\models;

use backend\modules\operator\models\query\ZingTreeQuery;
use common\components\db\ActiveRecord;
use common\models\Country;
use common\models\Product;
use common\modules\order\models\OrderType;
use Yii;
use yii\base\ExitException;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\web\HttpException;

class ZingTree extends ActiveRecord
{
    const IFRAME_ZING_TREE_URL = 'https://zingtree.com/deploy/tree.php?z=embed&tree_id=';
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    /** @var  array */
    public $country_id;

    /** @var  array */
    public $product_id;

    /** @var  array */
    public $order_type_id;


    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $id = yii::$app->db->getLastInsertID();
        } else {
            $id = $this->id;
        }

        $connection = yii::$app->db;
        $transaction = $connection->beginTransaction();

        $this->load(yii::$app->request->post());

        try {
            $countries = $this->country_id ? $this->country_id : [];
            $this->addCountries($id, $countries);
            $products = $this->product_id ? $this->product_id : [];
            $this->addProducts($id, $products);
            $order_types = $this->order_type_id ? $this->order_type_id : [];
            $this->addOrderType($id, $order_types);
        } catch (ExitException $e) {
            $transaction->rollBack();
        }

        $transaction->commit();
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param $zing_tree_id
     * @param $countries
     */
    public function addCountries($zing_tree_id, $countries)
    {
        if (empty($countries)) {
            return;
        }

        yii::$app->db->createCommand("delete from {{zing_tree_country}} where zing_tree_id = :id")->bindValue(':id', $zing_tree_id)->execute();

        foreach ($countries as $country_id) {
            yii::$app->db->createCommand("insert into {{%zing_tree_country}} (zing_tree_id, country_id) values (:id, :country_id)")
                ->bindValues([
                    ':id' => $zing_tree_id,
                    ':country_id' => $country_id
                ])->execute();
        }
    }

    /**
     * @param $zing_tree_id
     * @param $products
     */
    public function addProducts($zing_tree_id, $products)
    {
        if (empty($products)) {
            return;
        }

        yii::$app->db->createCommand("delete from {{zing_tree_product}} where zing_tree_id = :id")->bindValue(':id', $zing_tree_id)->execute();

        foreach ($products as $product_id) {
            yii::$app->db->createCommand("insert into {{%zing_tree_product}} (zing_tree_id, product_id) values (:id, :product_id)")
                ->bindValues([
                    ':id' => $zing_tree_id,
                    ':product_id' => $product_id
                ])->execute();
        }
    }

    /**
     * @param $zing_tree_id
     * @param $order_types
     * @throws Exception
     */
    public function addOrderType($zing_tree_id, $order_types)
    {
        ZingTreeOrderType::deleteAll([
            'zing_tree_id' => $zing_tree_id
        ]);

        if (empty($order_types)) {
            $order_types = [
                OrderType::TYPE_NEW
            ];
        }
        $error = false;
        $modelError = null;

        foreach ($order_types as $order_type) {
            $model = new ZingTreeOrderType();
            $model->zing_tree_id = $zing_tree_id;
            $model->order_type_id = $order_type;
            $model->created_at = time();
            $model->updated_at = time();
            if (!$model->save()) {
                $error = true;
                $modelError = $model;
            }
        }

        if ($error) {
            throw new Exception($modelError->getFirstErrorAsString());
        }
    }


    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%zing_tree}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['name', 'url'], 'string'],
            [['updated_at'], 'integer'],
            [['created_at'], 'integer'],
            [['active'], 'integer'],
            [['country_id', 'product_id', 'order_type_id'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Наименование'),
            'country_id' => Yii::t('common', 'Страна'),
            'product_id' => Yii::t('common', 'Товар'),
            'order_type_id' => Yii::t('common', 'Тип заказа'),
            'url' => Yii::t('common', 'Идентификатор ZingTree'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено')
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasMany(Country::className(), ['id' => 'country_id'])
            ->viaTable('zing_tree_country', ['zing_tree_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable('zing_tree_product', ['zing_tree_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrderTypes()
    {
        return $this->hasMany(OrderType::className(), ['id' => 'order_type_id'])
            ->viaTable(ZingTreeOrderType::tableName(), ['zing_tree_id' => 'id']);
    }

    /**
     * @param $country_id
     * @return ActiveQuery
     */
    public static function getByCountry($country_id)
    {

        return ZingTree::find()
            ->byCountry($country_id)
            ->getActive()
            ->orderBy([
                'zing_tree.id' => SORT_DESC
            ]);
    }

    /**
     * @param $country_id
     * @param $product_id
     * @return ActiveQuery
     */
    public static function getByCountryProductOrderType($country_id, $product_id, $order_type_id)
    {
        return ZingTree::find()
            ->byCountry($country_id)
            ->byProduct($product_id)
            ->byOrderType($order_type_id)
            ->getActive()
            ->orderBy([
                'zing_tree.id' => SORT_DESC
            ]);
    }

    /**
     * @param $order_type_id
     * @return ActiveQuery
     */
    public static function getByOrderType($order_type_id)
    {

        return ZingTree::find()
            ->byOrderType($order_type_id)
            ->getActive()
            ->orderBy([
                'zing_tree.id' => SORT_DESC
            ]);
    }

    /**
     * @return ZingTreeQuery
     */
    public static function find()
    {
        return new ZingTreeQuery(get_called_class());
    }
}
<?php

namespace backend\modules\operator\models;

use common\components\db\ActiveRecord;
use common\modules\order\models\OrderType;
use Yii;
/**
 * This is the model class for table "zing_tree_order_type".
 *
 * @property integer $id
 * @property integer $order_type_id
 * @property integer $zing_tree_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property OrderType $orderType
 * @property ZingTree $zingTree
 */


/**
 * Class ZingTreeOrderType
 * @package backend\modules\operator\models
 */
class ZingTreeOrderType extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%zing_tree_order_type}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_type_id', 'zing_tree_id'], 'required'],
            [['updated_at', 'created_at', 'order_type_id', 'zing_tree_id'], 'integer']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'order_type_id' => Yii::t('common', 'Тип заказа'),
            'zing_tree_id' => Yii::t('common', 'Zing Tree ID'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderType()
    {
        return $this->hasOne(OrderType::className(), ['id' => 'order_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZingTree()
    {
        return $this->hasOne(ZingTree::className(), ['id' => 'zing_tree_id']);
    }
}
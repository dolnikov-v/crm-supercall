<?php

namespace backend\modules\operator\models;

use common\models\User;
use common\models\Media;
use yii\db\Query;

class Request extends User
{
    /**
     * @param $user_id
     * @return Query
     */
    public static function getScunDocuments($user_id)
    {
        return Media::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['active' => Media::ACTIVE]);
    }

}
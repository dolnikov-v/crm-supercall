<?php
namespace backend\modules\operator\models;

use common\modules\order\models\Order;
use common\components\db\ActiveRecord;
use backend\modules\operator\models\QuestionnaireManagement;
use backend\modules\operator\models\query\QuestionnaireOrderQuery;
use Yii;

/**
 * Class QuestionnaireOrder
 * @package @package backend\modules\operator\models
 * @property integer $id
 * @property integer $questionnaire_base_id
 * @property integer $order_id
 * @property integer $order_status
 * @property integer $duration_time
 * @property integer $is_finished
 * @property string $audio_file
 * @property integer $created_at
 * @property integer $updated_at
 *
 */
class QuestionnaireOrder extends ActiveRecord
{
    const ORDER_FINISHED = 1;
    const ORDER_NOT_FINISHED = 0;

    public $operator;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%questionnaire_order}}';
    }

    /**
     * @return QuestionnaireOrderQuery
     */
    public static function find()
    {
        return new QuestionnaireOrderQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'questionnaire_base_id', 'order_status', 'created_at'], 'required'],
            [['id', 'order_id', 'order_status', 'questionnaire_base_id', 'duration_time', 'is_finished', 'created_at', 'updated_at'], 'integer'],
            [['order_status'], 'in', 'range' => array_keys(self::getOrderSatuses())]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'questionnaire_base_id' => Yii::t('common', 'План оценки'),
            'order_id' => Yii::t('common', 'Заказ'),
            'order_status' => Yii::t('common', 'Статус заказа'),
            'duration_time' => Yii::t('common', 'Продолжительность разговора'),
            'is_finished' => Yii::t('common', 'Закончено'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено')
        ];
    }

    /**
     * @return array
     */
    public static function getOrderSatuses()
    {
        return QuestionnaireManagement::getOrderStatusesCollection();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

}

<?php

namespace backend\modules\operator\models\query;

use backend\modules\operator\models\ZingTree;
use common\components\db\ActiveQuery;

class ZingTreeQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function getActive()
    {
        return $this->andWhere([ZingTree::tableName().'.active' => ZingTree::ACTIVE]);
    }

    /**
     * @return $this
     */
    public function getNotActive()
    {
        return $this->andWhere(['active' => ZingTree::NOT_ACTIVE]);
    }

    /**
     * @return $this
     */
    public function byCountry($country_id){
        $this->joinWith(['country', 'product', 'orderTypes']);
        return $this->andWhere(['country_id' => $country_id]);
    }

    /**
     * @return $this
     */
    public function byProduct($product_id){
        $this->joinWith(['product', 'country', 'orderTypes']);
        return $this->andWhere(['product_id' => $product_id]);
    }

    /**
     * @return $this
     */
    public function byOrderType($order_type_id){
        $this->joinWith(['country', 'product', 'orderTypes']);
        return $this->andWhere(['order_type_id' => $order_type_id]);
    }
}
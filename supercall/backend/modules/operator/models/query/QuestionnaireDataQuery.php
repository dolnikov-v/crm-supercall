<?php
namespace backend\modules\operator\models\query;

use common\components\db\ActiveQuery;
use backend\modules\operator\models\QuestionnaireData;

/**
 * Class QuestionnaireDataQuery
 * @package backend\modules\operator\models\query
 * @method QuestionnaireData one($db = null)
 * @method QuestionnaireData [] all($db = null)
 */
class QuestionnaireDataQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionValue = 'id';
}

<?php
namespace backend\modules\operator\models\query;

use common\components\db\ActiveQuery;
use backend\modules\operator\models\QuestionnaireOrder;

/**
 * Class QuestionnaireOrderQuery
 * @package backend\modules\operator\models\query
 * @method QuestionnaireOrder one($db = null)
 * @method QuestionnaireOrder [] all($db = null)
 */
class QuestionnaireOrderQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionValue = 'id';
}

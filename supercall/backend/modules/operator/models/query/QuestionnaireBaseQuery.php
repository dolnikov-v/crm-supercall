<?php
namespace backend\modules\operator\models\query;

use common\components\db\ActiveQuery;
use backend\modules\operator\models\QuestionnaireBase;

/**
 * Class QuestionnaireBaseQuery
 * @package backend\modules\operator\models\query
 * @method QuestionnaireBase one($db = null)
 * @method QuestionnaireBase[] all($db = null)
 */
class QuestionnaireBaseQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionValue = 'id';
}

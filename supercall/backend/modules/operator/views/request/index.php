<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\models\User;
use common\models\Media;
use backend\modules\operator\models\Request;
use hiqdev\assets\lightbox2\LightboxAsset;
use backend\assets\SiteAsset;
use common\components\web\View;

/** @var yii\web\View $this */
/** @var \backend\modules\operator\models\search\RequestSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список заявок');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Операторы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

SiteAsset::register($this);
LightboxAsset::register($this);

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заявками'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            /** @var User $model */
            return [
                'class' => 'tr-vertical-align-middle '
                . $model->authAssignment[0]->item_name == User::ROLE_APPLICANT && $model->status == User::STATUS_APPLICANT ? 'text-muted ' : ''
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'username',
                'enableSorting' => false,
                'content' => function ($data) {
                    return $data->username;
                }
            ],
            [
                'attribute' => 'role',
                'enableSorting' => false,
                'content' => function ($data) {
                    return $data->role;
                }
            ],
            [
                'attribute' => 'status',
                'enableSorting' => false,
                'content' => function ($data) {
                    return User::getTextStatus($data->status) . ' (' . $data->status . ')';
                }
            ],
            [
                'attribute' => 'country_id',
                'enableSorting' => false,
                'content' => function ($data) {
                    return $data->username;
                }
            ],

            [
                'attribute' => 'phone',
                'enableSorting' => false,
                'content' => function ($data) {
                    return $data->phone;
                }
            ],
            [
                'attribute' => 'email',
                'enableSorting' => false,
                'content' => function ($data) {
                    return $data->email;
                }
            ],
            [
                'attribute' => 'document',
                'enableSorting' => false,
                'content' => function ($data) {
                    $content = [];
                    $tpl = '
                        <a href="{pathtofile}" rel="lightbox" >
                        <img class="img-fluid" src="{pathtofile}" width="20px"/>
                        </a>
                        ';

                    foreach (Request::getScunDocuments($data->id)->all() as $k => $document) {
                        $content[] = strtr($tpl, [
                            '{pathtofile}' => Media::getAbsolutePathDocumentScun().'/'.$document->filename
                        ]);
                    }

                    return implode(" ", $content);
                }
            ],
            [
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center width-150'],
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Подтвердить'),
                        'url' => function ($model) {
                            return Url::toRoute(['/operator/request/confirm', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.request.confirm') && $model->authAssignment[0]->item_name == User::ROLE_APPLICANT;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Отклонить'),
                        'url' => function ($model) {
                            return Url::toRoute(['/operator/request/reject', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.request.reject') && $model->authAssignment[0]->item_name == User::ROLE_APPLICANT;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Заблокировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/operator/request/block', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.request.block') && $model->authAssignment[0]->item_name == User::ROLE_OPERATOR && $model->status == User::STATUS_APPLICANT;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Разблокировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/operator/request/unblock', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.request.unblock') && $model->authAssignment[0]->item_name == User::ROLE_APPLICANT && $model->status == User::STATUS_BLOCKED;
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use backend\assets\SiteAsset;
use common\models\User;

/** @var \backend\modules\operator\models\search\RequestSearch $modelSearch */
/** @var array $countries */

SiteAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'role')->select2List(User::getApplicantRolesCollection(), [
                'prompt' => '-'
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'status')->select2List(User::getApplicantStatusCollection(), [
                'prompt' => '-'
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'phone')->textInput(); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'email')->textInput(); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'created_at')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'pluginOptions' => [
                    'useWithAddon' => true,
                    'timePicker' => false,
                    'locale' => [
                        'format' => 'YYYY/MM/DD',
                    ],
                    'options' => [
                        'value' => date('Y/m/d') . ' - ' . date('Y/m/d')
                    ]
                ],
            ])->label(yii::t('common', 'Дата создания')); ?>

        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
<?php
use backend\modules\operator\models\StatisticsOfAuditors;
use backend\modules\stats\assets\StatsAsset;
use common\components\grid\GridView;
use common\components\grid\NumberColumn;
use common\components\grid\RatioColumn;
use common\modules\order\models\Order;
use common\widgets\base\Panel;
use backend\modules\operator\assets\StatisticsOfAuditorsAsset;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \backend\modules\operator\models\search\AuditedOrdersSearch $modelSearch */
/** @var array $countries */
/** @var array $controllers */
/** @var array $operators */
/** @var array $statuses */
/** @var array $group */

$this->title = Yii::t('common', 'Статистика по аудиторам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Аудит'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

StatsAsset::register($this);
StatisticsOfAuditorsAsset::register($this);

?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'dataProvider' => $dataProvider,
        'modelSearch' => $modelSearch,
        'countries' => $countries,
        'controllers' => $controllers,
        'operators' => $operators,
        'statuses' => $statuses,
        'group' => $group,
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статистика по аудиторам'),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'showFooter' => true,
        'footerRowOptions' => [
            'style' => 'font-weight:bold',
            'class' => 'success',
        ],
        'tableOptions' => [
            'data-id' => 'table-statistics',
            'data-locale' => 'en-US',
        ],
        'options' => [
            'class' => 'table-responsive',
        ],
        'columns' => [
            [
                'attribute' => 'group',
                'visible' => !empty($modelSearch->group),
                'enableSorting' => true,
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'group'],
                'value' => function ($model) use ($modelSearch) {
                    return yii::t('common', $modelSearch->formatGroupView($model));
                },
                'label' => yii::t('common', 'Группировка'),
                'footer' => \Yii::t('common', 'Всего:'),
            ],
            [
                'attribute' => 'controller',
                'headerOptions' => ['class' => 'width-100'],
                'content' => function ($model) {
                    return $model->username;
                },
                'enableSorting' => true,
                'label' => yii::t('common', 'Аудитор')
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => \Yii::t('common', 'Всего:'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_APPROVED,
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_APPROVED],
                'enableSorting' => true,
                'label' => $statuses[Order::STATUS_APPROVED],
                'contentOptions' => function ($model) use ($modelSearch) {
                    $url = StatisticsOfAuditors::generateUrl($model, $modelSearch, Order::STATUS_APPROVED);
                    return [
                        'class' => 'text-success',
                        'data-url' => $url
                    ];
                },
            ],
            [
                'class' => RatioColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_APPROVED . '-percent',
                'label' => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'value' => function ($row) {
                    $curr = (int)$row['status_quantity_' . Order::STATUS_APPROVED];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr / $total * 100, 2) : 0;
                },
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_APPROVED . '-percent'],
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_RECALL,
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_RECALL],
                'enableSorting' => true,
                'label' => $statuses[Order::STATUS_RECALL],
                'contentOptions' => function ($model) use ($modelSearch) {
                    $url = StatisticsOfAuditors::generateUrl($model, $modelSearch, Order::STATUS_RECALL);
                    return [
                        'class' => 'text-success',
                        'data-url' => $url
                    ];
                },
            ],
            [
                'class' => RatioColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_RECALL . '-percent',
                'label' => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'value' => function ($row) {
                    $curr = (int)$row['status_quantity_' . Order::STATUS_RECALL];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr / $total * 100, 2) : 0;
                },
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_RECALL . '-percent'],
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_FAIL,
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_FAIL],
                'enableSorting' => true,
                'label' => $statuses[Order::STATUS_FAIL],
                'contentOptions' => function ($model) use ($modelSearch) {
                    $url = StatisticsOfAuditors::generateUrl($model, $modelSearch, Order::STATUS_FAIL);
                    return [
                        'class' => 'text-success',
                        'data-url' => $url
                    ];
                },
            ],
            [
                'class' => RatioColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_FAIL . '-percent',
                'label' => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'value' => function ($row) {
                    $curr = (int)$row['status_quantity_' . Order::STATUS_FAIL];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr / $total * 100, 2) : 0;
                },
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_FAIL . '-percent'],
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_REJECTED,
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_REJECTED],
                'enableSorting' => true,
                'label' => $statuses[Order::STATUS_REJECTED],
                'contentOptions' => function ($model) use ($modelSearch) {
                    $url = StatisticsOfAuditors::generateUrl($model, $modelSearch, Order::STATUS_REJECTED);
                    return [
                        'class' => 'text-success',
                        'data-url' => $url
                    ];
                },
            ],
            [
                'class' => RatioColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_REJECTED . '-percent',
                'label' => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'value' => function ($row) {
                    $curr = (int)$row['status_quantity_' . Order::STATUS_REJECTED];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr / $total * 100, 2) : 0;
                },
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_REJECTED . '-percent'],
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_TRASH,
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_TRASH],
                'enableSorting' => true,
                'label' => $statuses[Order::STATUS_TRASH],
                'contentOptions' => function ($model) use ($modelSearch) {
                    $url = StatisticsOfAuditors::generateUrl($model, $modelSearch, Order::STATUS_TRASH);
                    return [
                        'class' => 'text-success width-150',
                        'data-url' => $url
                    ];
                },
            ],
            [
                'class' => RatioColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_TRASH . '-percent',
                'label' => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'value' => function ($row) {
                    $curr = (int)$row['status_quantity_' . Order::STATUS_TRASH];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr / $total * 100, 2) : 0;
                },
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_TRASH . '-percent'],
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_DOUBLE,
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_DOUBLE],
                'enableSorting' => true,
                'label' => $statuses[Order::STATUS_DOUBLE],
                'contentOptions' => function ($model) use ($modelSearch) {
                    $url = StatisticsOfAuditors::generateUrl($model, $modelSearch, Order::STATUS_DOUBLE);
                    return [
                        'class' => 'text-success width-150',
                        'data-url' => $url
                    ];
                },
            ],
            [
                'class' => RatioColumn::className(),
                'attribute' => 'status_quantity_' . Order::STATUS_DOUBLE . '-percent',
                'label' => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'value' => function ($row) {
                    $curr = (int)$row['status_quantity_' . Order::STATUS_DOUBLE];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr / $total * 100, 2) : 0;
                },
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'status_quantity_' . Order::STATUS_DOUBLE . '-percent'],
            ],
        ]
    ]),
    'footer' => '',
]);

$js = <<<JS
    var tableId = '[data-id="table-statistics"]';
    $(tableId).bootstrapTable();

    mvFooterToHead(tableId)
JS;

$this->registerJs($js, $this::POS_LOAD);

?>

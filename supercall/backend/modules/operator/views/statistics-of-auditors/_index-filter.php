<?php
use common\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use yii\helpers\Url;

/** @var \backend\modules\operator\models\search\StatisticsOfAuditorsSearch $modelSearch */
/** @var array $controllers */
/** @var array $operators */
/** @var array $countries */
/** @var array $statuses */
/** @var array $group */
?>

<?php
$form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">

        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'country_id')->select2List($countries, [
                'multiple' => true,
            ])->label(\Yii::t('common', 'Страна')) ?>
        </div>

        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'controller_id')->select2List($controllers, [
                'multiple' => false,
                'prompt' => '-',
            ])->label(\Yii::t('common', 'Аудитор')) ?>
        </div>

        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= $form->field($modelSearch, 'date_range')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'presetDropdown' => true,
                'hideInput' => true,
                'pluginOptions' => [
                    'autoUpdateOnInit' => true,
                    'autoUpdateInput' => false,
                    'timePicker' => true,
                    'timePicker24Hour' => true,
                    'timePickerIncrement' => 1,
                    'locale' => [
                        'format' => 'DD/MM/YYYY H:mm',
                    ],
                ],
                'pluginEvents' => [
                    'cancel.daterangepicker' => 'function(ev, picker) { $("input#ordersearch-updatedrange").val(""); $(this).find("span.range-value").text("") }',
                ],
            ])->label(yii::t('common', 'Дата')); ?>
        </div>

        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= $form->field($modelSearch, 'group')->select2List($group, ['prompt' => '-']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'operator_id')->select2List($operators, [
                'multiple' => false,
                'prompt' => '-',
            ])->label(\Yii::t('common', 'Оператор')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
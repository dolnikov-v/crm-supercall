<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use common\models\User;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \common\models\search\UserSearch $searchModel */
/** @var [] $products */
/** @var [] $queues */
/** @var [] $countries */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'countries' => $countries,
        'modelSearch' => $modelSearch,
        'user_country' => $user_country,
        'teams' => $teams,
        'queues_list' => $queues_list,
        'productsForFilter' => $productsForFilter
    ])
]) ?>


<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с пользователями'),
    //'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            $model = User::findOne(['id' => $model['id']]);
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->hasStatus(User::STATUS_BLOCKED) ? 'text-muted' : ''),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'username',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'rolesNames',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'productsList',
                'enableSorting' => false,
                'content' => function($model) use ($products){
                    return isset($products[$model['id']]) ? implode(', ', $products[$model['id']]) : '';
                }
            ],
            [
                'attribute' => 'countriesList',
                'enableSorting' => false,
                'content' => function($model) use ($countries){
                    return isset($countries[$model['id']]) ? implode(', ', $countries[$model['id']]) : '';
                }
            ],
            [
                'attribute' => 'queuesList',
                'enableSorting' => false,
                'content' => function($model) use ($queues){
                    return isset($queues[$model['id']]) ? implode(', ', $queues[$model['id']]) : '';
                }
            ],
            [
                'class' => DateColumn::className(),
                'withRelativeTime' => true,
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'withRelativeTime' => true,
                'attribute' => 'last_activity',
            ],
            [
                'attribute' => 'ip',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ]
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактирование'),
                        'url' => function ($model) {
                            return Url::toRoute(['/access/user/edit', 'id' => $model['id']]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('access.user.edit');
                        },
                        'attributes' => function () {
                            return [
                                'target' => '_blank',
                            ];
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Управление товарами'),
                        'url' => function ($model) {

                            return Url::toRoute(['set-product', 'id' => $model['id']]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.listoperators.setproduct');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Управление командами'),
                        'url' => function ($model) {

                            return Url::toRoute(['manage-team', 'id' => $model['id']]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.listoperators.manageteam');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Доступы к очередям'),
                        'url' => function ($model) {
                            return Url::toRoute(['/queue/user/edit', 'user_id' => $model['id']]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.listoperators.queue');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Оценить работу'),
                        'url' => function ($model) {
                            return Url::toRoute(['/operator/questionnaire/add-base/', 'user_id' => $model['id']]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.questionnaire.index') &&
                                !is_null(Yii::$app->authManager->getAssignment(User::ROLE_OPERATOR, $model['id']));
                        },

                    ],
                ]
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider
    ]),
])

?>

<?= ModalConfirmDelete::widget(); ?>
<?php

use yii\helpers\Url;

?>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Сохранить команды')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php

use common\widgets\ActiveForm;
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\User $user */
/** @var  string $username */
/** @var array $teams */
/** @var  array $user_teams */
/** @var  string $redirect */

$this->title = Yii::t('common', 'Редактирование команд пользователя "{username}"', ['username' => $user->username]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список пользователей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['manage-team', 'id' => $user->id]),
    'enableClientValidation' => false,
]); ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Управление командами пользователя'),
    'contentPadding' => 20,
    'content' => $this->render('_manage-team-form', [
        'form' => $form,
        'user' => $user,
        'teams' => $teams,
        'redirect' => $redirect,
        'user_teams' => $user_teams
    ]),

    'footer' => $this->render('_manage-team-footer', [
        'form' => $form
    ])
]); ?>

<?php ActiveForm::end(); ?>
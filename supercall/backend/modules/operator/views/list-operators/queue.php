<?php
/** @var \common\models\User $user */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var [] $listQueueUser */

use common\widgets\base\Panel;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use yii\helpers\Url;
use common\components\grid\DateColumn;
use yii\helpers\ArrayHelper;
use common\models\Country;
use common\models\Product;
use common\widgets\base\Checkbox;
use backend\modules\operator\assets\QueueUserSetAsset;

$this->title = Yii::t('common', 'Доступные очереди для пользователя "{username}"', ['username' => $user->username]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список пользователей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];

QueueUserSetAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Доступные очереди'),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'primary_country',
                'enableSorting' => false,
                'content' => function ($model) {
                    $primary_components = json_decode($model->primary_components);
                    $countries_id = isset($primary_components->primary_country) ? json_decode($primary_components->primary_country) : null;
                    $countries_id = is_array($countries_id) ? $countries_id : [$countries_id];
                    $countries = ArrayHelper::getColumn(Country::find()->where(['in', 'id', $countries_id])->all(), 'name');

                    return is_array($countries) ? implode(', ', $countries) : '';
                }
            ],
            [
                'attribute' => 'primary_product',
                'enableSorting' => false,
                'content' => function ($model) {
                    $primary_components = json_decode($model->primary_components);
                    $products_id = isset($primary_components->primary_product) ? json_decode($primary_components->primary_product) : null;
                    $products_id = is_array($products_id) ? $products_id : [$products_id];
                    $products = ArrayHelper::getColumn(Product::find()->where(['in', 'id', $products_id])->all(), 'name');

                    return is_array($products) ? implode(', ', $products) : '';
                }
            ],
            [
                'attribute' => 'active',
                'content' => function ($model) use ($listQueueUser, $user) {
                    return Checkbox::widget([
                        'name' => 'checkbox_activity',
                        'id' => $model->id.'-'.$user->id,
                        'value' => $model->id,
                        'style' => 'checkbox-inline',
                        'checked' => in_array($model->id, $listQueueUser),
                        'label' => '  ',
                    ]);
                }
            ],
            [
                'class' => DateColumn::className(),
                'withRelativeTime' => true,
                'attribute' => 'created_at',
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider
    ]),
])

?>
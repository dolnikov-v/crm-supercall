<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Product;

/** @var \yii\web\View $this */
/** @var \backend\modules\operator\models\search\ProductOperatorSearch $searchModel */
/** @var  integer $user_id */
/** @var  [] $productList */
/** @var  [] $productFilter */
/** @var  string $username */

$this->title = Yii::t('common', 'Редактирование товаров пользователя "{username}"', ['username' => $username]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список пользователей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['set-product', 'id' => $user_id]),
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($searchModel, 'product_ids')->select2List($productFilter, [
                'value' => $productList,
                'multiple' => true
            ]) ?>

            <?= $form->field($searchModel, 'user_id')->hiddenInput(['value' => $user_id])->label(false) ?>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit(Yii::t('common', 'Сохранить товары')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
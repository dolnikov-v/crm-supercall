<?php
/**
 * @var array $teams
 * @var \common\widgets\ActiveForm $form
 * @var array $user_teams
 * @var \common\models\User $user
 * @var string $redirect
 **/
use yii\helpers\Html;

?>

<br/>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($user, 'team_id')->select2List($teams, [
            'value' => $user_teams,
            'multiple' => true
        ])->label(false) ?>

        <?= $form->field($user, 'user_id')->hiddenInput(['value' => $user->id])->label(false) ?>
        <?= Html::hiddenInput('redirect', $redirect) ?>

    </div>
</div>


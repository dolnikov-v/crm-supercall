<?php
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use backend\assets\SiteAsset;
use common\models\User;

/** @var \backend\modules\operator\models\search\ListOperatorsSearch $modelSearch */
/** @var array $countries */

SiteAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'id')->textInput(); ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'username')->textInput(); ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'countries')->select2List($user_country, [
                'value' => $modelSearch->countries,
                'multiple' => true,
                'placeholder' => yii::t('common', 'Все'),
            ])->label(\Yii::t('common', 'Страна')) ?>
        </div>

        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'queues')->select2List($queues_list, [
                'value' => $modelSearch->queues,
                'multiple' => true,
                'placeholder' => yii::t('common', 'Все'),
            ])->label(\Yii::t('common', 'Очереди')) ?>
        </div>

        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'teams')->select2List($teams, [
                'value' => $modelSearch->teams,
                'prompt' => '-',
                'placeholder' => yii::t('common', 'Все'),
            ])->label(\Yii::t('common', 'Команда')) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'auto_call')->select2List(['-1' => 'All', 1 => 'Yes','0' => 'No'],[
                'value' => $modelSearch->auto_call,
                'placeholder' => yii::t('common','Все'),
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'product')->select2List($productsForFilter, [
                'value' => $modelSearch->product,
                'multiple' => true,
                'placeholder' => yii::t('common', 'Все'),
            ])->label(\Yii::t('common', 'Products'))
            ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
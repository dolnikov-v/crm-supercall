<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\widgets\base\Label;
use common\widgets\base\ModalConfirmDelete;
use backend\modules\operator\models\QuestionnaireManagement;

/** @var yii\web\View $this */
/** @var \backend\modules\operator\models\search\QuestionnaireManagementSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var  [] $order_status */

$this->title = Yii::t('common', 'Управление опросами операторов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'order_status' => $order_status
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с вопросами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'question_text',
                'headerOptions' => ['class' => 'width-400'],
                'content' => function ($model) {
                    return yii::t('common', $model->question_text);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'answers',
                'headerOptions' => ['class' => 'width-300'],
                'content' => function ($model) {
                    $data_answers = json_decode($model->answers, 1);
                    $list_answers = [];

                    if (isset($data_answers['answers']) && is_array($data_answers['answers'])) {
                        foreach ($data_answers['answers'] as $answer) {
                            $list_answers[] = yii::t("common", $answer);
                        }
                    }

                    return !empty($list_answers) ? implode("<br/>", $list_answers) : '';
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_status',
                'headerOptions' => ['class' => 'width-200'],
                'content' => function ($model) {
                    $order_statuses = QuestionnaireManagement::getOrderStatusesCollection();
                    $list_statuses = [];
                    $used_statuses = json_decode($model->order_status, 1);

                    foreach ($used_statuses as $status) {
                        $list_statuses[] = yii::t('common', $order_statuses[$status]);
                    }

                    return implode(", ", $list_statuses);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'required',
                'headerOptions' => ['class' => 'width-100 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->required > 0 ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->required > 0 ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);

                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'headerOptions' => ['class' => 'width-50'],
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'headerOptions' => ['class' => 'width-50'],
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.questionnaire.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/catalog/questionnaire/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.questionnaire.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' =>
        (Yii::$app->user->can('catalog.questionnaire.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить вопрос'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>
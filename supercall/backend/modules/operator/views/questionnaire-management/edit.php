<?php
use common\widgets\base\Panel;
use yii\helpers\Url;
use backend\modules\operator\assets\QuestionnaireAsset;

/** @var \yii\web\View $this */
/** @var \backend\modules\catalog\models\PriceShipping $model */
/** @var  [] $order_status */
/** @var  [] $type_answers */
/** @var  [] $answers */
/** @var  [] $answers_other */
/** @var  [] $answers_other_values */
/** @var string $mask */
/** @var  string $type_answer */

QuestionnaireAsset::register($this);

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление вопроса') : Yii::t('common', 'Редактирование вопроса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Операторы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление опросами операторов'), 'url' => Url::toRoute('/catalog/questionnaire/index/'.$model->id)];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Вопрос'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'order_status' => $order_status,
        'order_status_values' => $order_status_values,
        'type_answers' => $type_answers,
        'answers' => $answers,
        'type_answer' => $type_answer,
        'mask' => $mask,
        'answers_other' => $answers_other,
        'answers_other_values' => $answers_other_values,
    ])
]); ?>
<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \backend\modules\catalog\models\search\QuestionnaireSearch $modelSearch */
/** @var [] $order_status */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($modelSearch, 'question_text')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'order_status')->select2List($order_status, ['prompt' => yii::t('common', 'Все доступные')]); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

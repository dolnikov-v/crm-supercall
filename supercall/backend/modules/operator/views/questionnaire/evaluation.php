<?php
use yii\helpers\Url;
use common\components\grid\GridView;
use common\components\i18n\Formatter;
use common\modules\order\models\genesys\Call;
use common\modules\order\models\Order;
use common\widgets\base\Panel;
use yii\bootstrap\Tabs;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var $order */
/** @var backend\modules\operator\models\QuestionnaireOrder $modelQuestionnaireOrder */
/** @var $allModels [] Questionnaire */
/** @var backend\modules\operator\models\QuestionnaireManagement $modelQuestionnaire */
/** @var yii\web\View $this */
/** @var  $questionnaireData [] */


$this->title = Yii::t('common', 'Оценка по заказу #{order_id}', ['order_id' => $order->order_id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Планы оценки'), 'url' => Url::toRoute(['/operator/questionnaire', 'QuestionnaireOrderSearch[questionnaire_order_id]' => $order->questionnaire_base_id])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Перечень заказов'), 'url' => Url::toRoute(['orders', 'id' => $order->questionnaire_base_id])];
$this->params['breadcrumbs'][] = ['label' => $this->title];

$model = $order->order;
$components = json_decode($model->customer_components);

/** @var Formatter $formatter */
$formatter = Yii::$app->formatter;
$default_timezone = $formatter->timeZone;
$formatter->timeZone = Yii::$app->user->timezone->timezone_id;
$similar_count = $model->getSimilar()->count();
?>

<?php
echo Panel::widget([
    'title' => Yii::t('common', 'Просмотр заказа'),
    'content' => Tabs::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => Yii::t('common', 'Данные заказа'),
                'content' => DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'country.name',
                        'type.name',
                        [
                            'attribute' => 'status',
                            'value' => ArrayHelper::getValue(Order::getStatusesCollection(), $model->status)
                        ],
                        'sub_status',
                        'partner.name',
                        'foreign_id',
                        'form_id',
                        'customer_phone',
                        'customer_mobile',
                        'customer_address',
                        'customer_ip',
                        'source_uri',
                        [
                            'attribute' => 'customer_components',
                            'format' => 'raw',
                            'value' => Html::tag('p', Yii::t('common', 'Данные заказчика')) .
                                DetailView::widget([
                                    'model' => isset($components->general) ? $components->general : []
                                ]) .
                                Html::tag('p', Yii::t('common', 'Адрес')) .
                                DetailView::widget([
                                    'model' => isset($components->address) ? $components->address : []
                                ])
                        ],
                        'init_price',
                        'final_price',
                        'shipping_price',
                        'recall_date',
                        'customer_first_name',
                        'customer_last_name',
                        'customer_middle_name',
                        'shipping_id',
                        'customer_email',
                        [
                            'attribute' => 'order_phones',
                            'format' => 'raw',
                            'value' => GridView::widget([
                                'dataProvider' => new ArrayDataProvider([
                                    'allModels' => $model->phones
                                ])
                            ])
                        ],
                        'language.name',
                        'last_queue_id',

                        [
                            'attribute' => 'ordered_at',
                            'value' => $formatter->asDateFullTime($model->ordered_at),
                            'enableSorting' => false,
                        ],
                        [
                            'attribute' => 'created_at',
                            'value' => $formatter->asDateFullTime($model->created_at),
                            'enableSorting' => false,
                        ],
                        [
                            'attribute' => 'updated_at',
                            'value' => $formatter->asDateFullTime($model->updated_at),
                            'enableSorting' => false,
                        ],

                        [
                            'attribute' => 'orderProducts',
                            'format' => 'raw',
                            'value' => GridView::widget([
                                'dataProvider' => new ArrayDataProvider([
                                    'allModels' => ArrayHelper::merge($model->orderProducts, $model->orderPromoProducts),
                                ]),
                                'columns' => [
                                    'product.name',
                                    'quantity',
                                    'price',
                                    'cost'
                                ]
                            ])
                        ]
                    ]
                ])
            ], [
                'label' => Yii::t('common', 'История заказа'),
                'content' => $this->render('@common/modules/order/views/change/_tab-history', [
                    'order' => $model,
                    'form' => null
                ])
            ],
            [
                'label' => Yii::t('common', 'Записи разговоров'),
                'content' => GridView::widget([
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => array_filter($model->calls, function (Call $call) {
                            return $call->RECORD ? true : false;
                        }),
                    ]),
                    'columns' => [
                        [
                            'label' => '',
                            'format' => 'raw',
                            'value' => function ($row) {
                                return Html::tag('audio', '', ['src' => $row['RECORD'], 'controls' => true, 'preload' => 'none']);
                            }
                        ],
                        [
                            'label' => '',
                            'format' => 'url',
                            'attribute' => 'RECORD',
                        ]
                    ]
                ])
            ],
            [
                'label' => Yii::t('common', 'Комментарии'),
                'content' => $this->render('@common/modules/order/views/change/_tab-comment', [
                    'comments' => $model->comments
                ])
            ],
            [
                'label' => Yii::t('common', 'Оценка'),
                'content' => $this->render('_tab-evaluation', [
                    'allModels' => $allModels,
                    'modelQuestionnaire' => $modelQuestionnaire,
                    'questionnaireData' => $questionnaireData,
                    'order' => $order
                ]),
                'active' => true
            ],
            [
                'label' => Yii::t('common', 'Похожие заказы')
                    . ($similar_count > 0
                        ? ' ' . Html::tag('span', $similar_count, ['class' => 'label label-warning'])
                        : ''
                    ),
                'content' => $this->render('@common/modules/order/views/change/_tab-similar', [
                    'order' => $model,
                ]),
                'visible' => $similar_count
            ],
        ],
    ])
]);

$formatter->timeZone = $default_timezone;
?>

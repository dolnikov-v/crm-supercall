<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\widgets\base\Label;
use backend\modules\operator\models\QuestionnaireManagement;

/** @var yii\web\View $this */
/** @var \backend\modules\operator\models\search\QuestionnaireBaseSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Перечень заказов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Планы оценки'), 'url' => Url::toRoute(['/operator/questionnaire'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Планы оценки'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'order_id',
                'content' => function ($model) {
                    return yii::t('common', $model->order_id);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_status',
                'content' => function ($model) {
                    $order_statuses = QuestionnaireManagement::getOrderStatusesCollection();
                    return yii::t('common', $order_statuses[$model->order_status]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'operator',
                'content' => function ($model) {
                    return yii::$app->request->get('operator');
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'duration_time',
                'contentOptions' => ['class' => 'text-center width-300'],
                'content' => function ($model) {
                    return gmdate("H:i:s",$model->duration_time);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'is_finished',
                'contentOptions' => ['class' => 'text-center width-100'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->is_finished > 0 ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->is_finished > 0 ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Оценить'),
                        'url' => function ($model) {
                            return Url::toRoute(['evaluation', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('operator.questionnaire.evaluation');
                        }
                    ]
                ]
            ],
        ],
    ]),
    'footer' =>  LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

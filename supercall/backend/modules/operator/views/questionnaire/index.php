<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Label;

/** @var yii\web\View $this */
/** @var \backend\modules\operator\models\search\QuestionnaireBaseSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Оценка работы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Операторы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Планы оценки'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'country_id',
                'content' => function ($model) {
                    return yii::t('common', $model->country->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'controller_id',
                'content' => function ($model) {
                    return yii::t('common', $model->controller->username);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'operator_id',
                'content' => function ($model) {
                    return yii::t('common', $model->operator->username);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'is_finished',
                'contentOptions' => ['class' => 'text-center width-100'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->is_finished > 0 ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->is_finished > 0 ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Перейти'),
                        'url' => function ($model) {
                            return Url::toRoute(['orders', 'id' => $model->id, 'operator' => $model->operator->username]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('operator.questionnaire.orders');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Завершить'),
                        'url' => function ($model) {
                            return Url::toRoute(['finish-off', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.questionnaire.finishoff') && $model->is_finished < 1 ;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Отменить завершение'),
                        'url' => function ($model) {
                            return Url::toRoute(['cancel-completion', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.questionnaire.cancelcompletion') && $model->is_finished > 0 ;
                        }
                    ],
                ]
            ],
        ],
    ]),
    'footer' =>  LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>


<?= ModalConfirmDelete::widget() ?>

<?php
use common\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\time\TimePicker;
use yii\helpers\ArrayHelper;
use backend\modules\operator\models\QuestionnaireManagement;

/** @var backend\modules\operator\models\QuestionnaireData [] $allModels */
/** @var backend\modules\operator\models\QuestionnaireManagement $modelQuestionnaire */
/** @var yii\web\View $this */
/** @var backend\modules\operator\models\QuestionnaireOrder $order */
/** @var  $questionnaireData []  empty for new evaluation */

$post = yii::$app->request->post();
$allModels = empty($questionnaireData) ? $allModels : $questionnaireData;
?>

<p>&nbsp;</p>
<div class="alert alert-info" role="alert"><?= yii::t('common', '* - ответ на данный вопрос обязателен'); ?></div>

<?php $form = ActiveForm::begin(); ?>
<p>&nbsp;</p>

<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-2">
        <h5><?= yii::t('common', yii::t('common', 'Продолжительность разговора:')) ?></h5>
    </div>
    <div class="col-lg-2">
        <?= TimePicker::widget([
            'name' => 'duration_time',
            'value' => isset($post['duration_time']) ? $post['duration_time']
                : (isset($order->duration_time) ? gmdate("H:i:s", $order->duration_time) : '00:00:00'),
            'pluginOptions' => [
                'showSeconds' => true,
                'showMeridian' => false,
                'minuteStep' => 1,
                'secondStep' => 5,
            ]
        ]); ?>
    </div>
</div>
<?php foreach ($allModels as $k => $question) : ?>
    <p>&nbsp;</p>

    <?php
    if (empty($post)) {
        $require = true;
    } else {
        $required = isset($question->required) ? $question->required : $question->questionnaire->required;

        $require = $required < 1
            || ($required > 0 && '-' != ArrayHelper::getValue($post, 'answer_text.' . $k));
    }

    $answer_data = json_decode(isset($question->answers) ? yii::t('common', $question->answers) : yii::t('common', $question->questionnaire->answers));

    $modifySelect = [];
    $selectList = [];

    if (isset($answer_data->answers)) {
        $selectList = array_merge(['-'], $answer_data->answers);

        foreach ($selectList as $v) {
            $modifySelect[$v] = yii::t('common', $v);
        }
    }

    ?>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-2 width-100">
            <h5><?= yii::t('common', 'Вопрос:') ?></h5>
        </div>
        <div class="col-lg-9" style="color:<?= ($require === true) ? '' : 'red' ?>">
            <?= yii::t('common', $question->question_text); ?> <?= (((isset($question->required) ? $question->required : $question->questionnaire->required) > 0) ? '*' : ''); ?>
            <?= Html::hiddenInput('question_text[]', yii::t('common', $question->question_text)) ?>
            <?= Html::hiddenInput('questionnaire_order_id[]', $order->id) ?>
            <?= Html::hiddenInput('required[]', isset($question->required) ? $question->required : $question->questionnaire->required) ?>
            <?= Html::hiddenInput('questionnaire_id[]', isset($question->questionnaire_id) ? $question->questionnaire_id  : $question->id) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-2 width-100">
            <h5><?= yii::t('common', 'Ответ:') ?></h5>
        </div>

        <div class="col-lg-9">
            <?php if ($answer_data->type == 'text') : ?>
                <?= $form->field($modelQuestionnaire, 'answer_text')->textarea([
                    'name' => 'answer_text[]',
                    'value' => !is_null(ArrayHelper::getValue($post, 'answer_text.' . $k))
                        ? yii::t('common', ArrayHelper::getValue($post, 'answer_text.' . $k))
                        : ($question instanceof QuestionnaireManagement  ?  '' : yii::t('common', $question->answer_text)),
                    'placeholder' => yii::t('common', 'Ответ')
                ])->label(false); ?>

            <?php else: ?>

                <?= $form->field($modelQuestionnaire, 'answer_text')->select2List($modifySelect, [
                    'name' => 'answer_text[]',
                    'value' => !is_null(ArrayHelper::getValue($post, 'answer_text.' . $k))
                        ? yii::t('common', ArrayHelper::getValue($post, 'answer_text.' . $k))
                        : ($question instanceof QuestionnaireManagement  ?  '-' : yii::t('common', $question->answer_text))
                ])->label(false) ?>

            <?php endif; ?>
        </div>
    </div>

<?php endforeach; ?>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Сохранить оценку')); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>


<?php
use common\widgets\ActiveForm;
use backend\modules\operator\models\QuestionnaireManagement;
use yii\helpers\Url;
use common\widgets\base\GlyphIcon;
use common\widgets\base\Button;

/** @var backend\modules\operator\models\QuestionnaireManagement $model */
/** @var [] $order_status */
/** @var [] $type_answers */
/** @var [] $answers */
/** @var [] $order_status_values */
/** @var [] $answers_other */
/** @var [] $answers_other_values */
/** @var string $type_answer */
/** @var string $mask */

?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit'])]); ?>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'question_text')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'order_status')->select2List($order_status, [
                'value' => $order_status_values,
                'multiple' => true
            ]) ?>
        </div>
    </div>
<?php

?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'type_answers')->select2List($type_answers, [
                'value' => $type_answer,
            ]) ?>
        </div>
        <div class="col-lg-4">
            <div class="row">&nbsp;</div>
            <?= $form->field($model, 'required')->checkbox(['checked' => $model->isNewRecord ? true : $model->required]) ?>
        </div>
    </div>
    <div class="row answers" <?= in_array($mask, array_flip($answers)) && !empty($answers_other_values) ? 'style="display:block"' : ''; ?>>
        <div class="col-lg-4">
            <?= $form->field($model, 'answers')->select2List($answers, [
                'value' => $mask
            ]) ?>
        </div>
    </div>

    <div class="row answers_other" <?= $mask == QuestionnaireManagement::ANSWER_OTHER && !empty($answers_other_values) ? 'style="display:block"' : ''; ?>>
        <div class="col-lg-12">
            <?= $form->field($model, 'answers_other')->select2List($answers_other, [
                'multiple' => true,
                'tags' => true,
                'value' => $answers_other_values
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить') : Yii::t('common', 'Сохранить')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php
use backend\modules\operator\models\ZingTree;
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \backend\modules\operator\models\search\ZingTreeSearch $modelSearch */
/** @var array $countries */
/** @var array $products */
/** @var array $order_types */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'name')->textInput(); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'country_id')->select2List($countries, [
                'prompt' => '-',
            ]);?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'product_id')->select2List($products, [
                'prompt' => '-',
            ]);?>
        </div>

        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'active')->select2List([
                ZingTree::ACTIVE => yii::t('common', 'Активные'),
                ZingTree::NOT_ACTIVE => yii::t('common', 'Не активные'),
                '' => '-'
            ]);?>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'order_type_id')->select2List($order_types, [
                'prompt' => '-',
            ]);?>
        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>


<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var  \backend\modules\operator\models\ZingTree $model */
/** @var array $countries*/
/** @var array $products*/
/** @var array $order_types*/

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление ZingTree') : Yii::t('common', 'Редактирование ZingTree');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Операторы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'ZingTree'), 'url' => Url::toRoute('/operator/zing-tree/index/'.$model->id)];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'ZingTree'),
    'alertStyle' => Panel::ALERT_INFO,
    'alert' => yii::t('common', 'Если не будет указан не один тип заказа, значение тип заказа будет "Новый"'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'countries' => $countries,
        'products' => $products,
        'order_types' => $order_types,
    ])
]); ?>
<?php

/** @var yii\web\View $this */
/** @var \backend\modules\operator\models\search\ZingTreeSearch $modelSearch */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var array $countries */
/** @var array $products */
/** @var array $order_types */

use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use common\components\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = Yii::t('common', 'ZingTree');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Операторы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
        'countries' => $countries,
        'products' => $products,
        'order_types' => $order_types,
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с ZingTree'),
    'alertStyle' => Panel::ALERT_INFO,
    'alert' => yii::t('common', 'Для страны будет использоваться последний добавленный активный ZingTree'),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'headerOptions' => ['class' => 'width-400'],
                'content' => function ($model) {
                    return yii::t('common', $model->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'url',
                'headerOptions' => ['class' => 'width-400'],
                'content' => function ($model) {
                    return yii::t('common', $model->url);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country_id',
                'headerOptions' => ['class' => 'width-300'],
                'content' => function ($model) {
                    $countryList = ArrayHelper::getColumn($model->country, yii::t('common', 'name'));
                    $countries = [];

                    foreach($countryList as $country){
                        $countries[] = yii::t('common', $country);
                    }

                    return implode(", ", $countries);
                }
            ],
            [
                'attribute' => 'product_id',
                'headerOptions' => ['class' => 'width-300'],
                'content' => function ($model) {
                    $productList = ArrayHelper::getColumn($model->product, yii::t('common', 'name'));
                    $products = [];

                    foreach($productList as $product){
                        $products[] = yii::t('common', $product);
                    }

                    return implode(", ", $products);
                }
            ],
            [
                'attribute' => 'order_type_id',
                'headerOptions' => ['class' => 'width-300'],
                'content' => function ($model) {
                    $orderTypesList = ArrayHelper::getColumn($model->orderTypes, yii::t('common', 'name'));
                    $orderTypes = [];

                    foreach($orderTypesList as $orderType){
                        $orderTypes[] = yii::t('common', $orderType);
                    }

                    return implode(", ", $orderTypes);
                }
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-50'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                }
            ],
            [
                'attribute' => 'created_at',
                'headerOptions' => ['class' => 'width-50'],
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'headerOptions' => ['class' => 'width-50'],
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/operator/zing-tree/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.zingtree.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/operator/zing-tree/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.zingtree.deactivate') && $model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('operator.zingtree.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/operator/zing-tree/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('operator.zingtree.delete');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' =>
        (Yii::$app->user->can('operator.zingtree.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить ZingTree'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>

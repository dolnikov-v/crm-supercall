<?php

use common\modules\order\models\OrderType;
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var \backend\modules\operator\models\ZingTree $model */
/** @var array $countries */
/** @var array $products */
/** @var array $order_types */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'url')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'country_id')->select2List($countries, [
                'value' => ArrayHelper::getColumn($model->country, 'id'),
                'multiple' => true
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'product_id')->select2List($products, [
                'value' => ArrayHelper::getColumn($model->product, 'id'),
                'multiple' => true
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'order_type_id')->select2List($order_types, [
                'value' => $model->isNewRecord ? OrderType::TYPE_NEW : ArrayHelper::getColumn($model->orderTypes, 'id'),
                'multiple' => true
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить') : Yii::t('common', 'Сохранить')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
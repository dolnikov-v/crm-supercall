<?php

use backend\modules\operator\models\QualityControl;
use backend\widgets\callrecords\CallRecords;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\modules\order\models\asterisk\Records;
use backend\modules\operator\assets\QualityControlAsset;
use common\modules\order\models\Order;
use common\modules\order\models\OrderType;
use common\modules\partner\models\PartnerFormAttribute;
use common\widgets\base\Button;
use common\widgets\base\GlyphIcon;
use common\widgets\base\Panel;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\operator\assets\QualityControlConfirmAsset;

$this->title = Yii::t('common', 'Контроль качества');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Операторы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

/** @var array $operators */
/** @var array $partners */

QualityControlAsset::register($this);
QualityControlConfirmAsset::register($this);
?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
        'user_country' => $user_country,
        'partners' => $partners,
        'operators' => $operators
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', ''),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'order_id',
                'enableSorting' => false,
                'contentOptions' => function ($model) {
                    $phones = [];

                    if (!isset($model->customer_phone)) {
                        $customer_phone = $model->order->customer_phone;
                    } else {
                        $customer_phone = $model->customer_phone;
                    }

                    if (!isset($model->customer_mobile)) {
                        $customer_mobile = $model->order->customer_mobile;
                    } else {
                        $customer_mobile = $model->customer_mobile;
                    }

                    if ($customer_phone) {
                        $phones[] = $customer_phone;
                    }

                    if ($customer_mobile) {
                        $phones[] = $customer_mobile;
                    }

                    if (empty($phones)) {
                        $phones = null;
                    }

                    return [
                        'class' => ['width-100 order_id'],
                        'data-column' => 'records-' . $model->id,
                        'data-call_history' => (int)!empty($model->callHistory),
                        'data-phones' => implode(',', $phones)
                    ];
                },
                'content' => function ($model) {
                    return $model->id;
                },
                'label' => yii::t('common', 'Номер заказа')
            ],
            [
                'attribute' => 'country',
                'enableSorting' => false,
                'contentOptions' => ['class' => ['width-100']],
                'content' => function ($model) {
                    if (!isset($model->country)) {
                        return $model->order->country->name;
                    }
                    return $model->country->name;
                },
                'label' => yii::t('common', 'Страна')
            ],
            [
                'attribute' => 'status',
                'contentOptions' => ['class' => ['width-100']],
                'enableSorting' => false,
                'content' => function ($model) {
                    $status = Order::getStatusByNumber($model->status);

                    if (!isset($model->type_id)) {
                        $type_id = $model->order->type_id;
                    } else {
                        $type_id = $model->type_id;
                    }

                    if ($type_id == OrderType::TYPE_CHECK_ADDRESS) {
                        $status = yii::t('common', 'Некорректный адрес');
                    }
                    return yii::t('common', $status);
                },
                'label' => yii::t('common', 'Статус заказа')
            ],
            [
                'attribute' => 'sub_status',
                'contentOptions' => ['class' => ['width-100']],
                'enableSorting' => false,
                'content' => function ($model) {
                    $sub_statuses_collection = Order::getSubStatusesCollection();

                    if ($model instanceof QualityControl) {
                        $sub_status = $model->order->sub_status;
                    } else {
                        $sub_status = $model->sub_status;
                    }

                    $sub_status = isset($sub_statuses_collection[$sub_status]) ? $sub_statuses_collection[$sub_status] : '';
                    return yii::t('common', $sub_status);
                },
                'label' => yii::t('common', 'Sub status')
            ],
            /*
        [
            'attribute' => 'record',
            'contentOptions' => function ($model) {
                return [
                    'class' => ['width-400'],
                    'data-column' => 'records-' . $model->id,
                    'data-call_history' => empty($model->callHistory)
                ];
            },
            'enableSorting' => false,
            'content' => function ($model) {
                if (empty($model->callHistory)) {
                    $phones = [];

                    if (!isset($model->customer_phone)) {
                        $customer_phone = $model->order->customer_phone;
                    } else {
                        $customer_phone = $model->customer_phone;
                    }

                    if (!isset($model->customer_mobile)) {
                        $customer_mobile = $model->order->customer_mobile;
                    } else {
                        $customer_mobile = $model->customer_mobile;
                    }

                    if ($customer_phone) {
                        $phones[] = $customer_phone;
                    }

                    if ($customer_mobile) {
                        $phones[] = $customer_mobile;
                    }

                    if (empty($phones)) {
                        return '-';
                    }

                    $content = '<div style="margin-left:3px" class="row" id="record_place_' . (($model instanceof QualityControl) ? $model->order_id : $model->id) . '" data-phones="' . implode(",", $phones) . '">';

                    $content .= Button::widget([
                        'id' => 'get_' . (($model instanceof QualityControl) ? $model->order_id : $model->id),
                        'style' => Button::STYLE_SUCCESS . ' ' . Button::SIZE_TINY . ' get-record',
                        'icon' => GlyphIcon::PLUS,
                        'label' => yii::t('common', 'Получить запись')
                    ]);

                    $content .= '</div>';
                } else {
                    $content = CallRecords::widget([
                        'order' => Order::findOne(['id' => $model->id]),
                        'columns' => [
                            CallRecords::COLUMN_CALL_RECORD,
                            CallRecords::COLUMN_CALL_STATUS,
                            CallRecords::COLUMN_OPERATOR,
                            CallRecords::COLUMN_DATE_OF_CALL
                        ]
                    ]);
                }

                return $content;
            },
            'label' => yii::t('common', 'Запись разговора')
        ],*/
            [
                'attribute' => 'address',
                'enableSorting' => false,
                'contentOptions' => function ($model) {
                    return [
                        'class' => ['width-150'],
                        'data-column' => 'address-' . (($model instanceof QualityControl) ? $model->order_id : $model->id)
                    ];
                },
                'content' => function ($model) {
                    if (!isset($model->customer_components)) {
                        $customer_components = $model->order->customer_components;
                    } else {
                        $customer_components = $model->customer_components;
                    }

                    $customer_components = json_decode($customer_components, 1);
                    $address = $customer_components['address'];

                    $address_string = '';

                    if (!isset($model->form_id)) {
                        $form_id = $model->order->form_id;
                    } else {
                        $form_id = $model->form_id;
                    }

                    if (!empty($address)) {
                        foreach ($address as $key => $add) {
                            $attr = PartnerFormAttribute::findOne(['form_id' => $form_id, 'name' => $key]);
                            if (isset($attr->label)) {
                                $address_string .= '<b data-name="' . $attr->name . '">' . yii::t('common', $attr->label) . ':</b> ' . $add . '<br>';
                            }
                        }
                    }
                    return $address_string;
                },
                'label' => yii::t('common', 'Адрес')
            ],
            [
                'attribute' => 'confirm',
                'headerOptions' => ['class' => ['text-center']],
                'contentOptions' => function ($model) {
                    return [
                        'class' => ['text-center'],
                        'data' => [
                            'order_id' => $model->id
                        ]
                    ];
                },
                'enableSorting' => false,
                'content' => function ($model) {
                    return '';
                },
                'label' => yii::t('common', 'Audit completed')
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/']); ///order/change/' . (($model instanceof QualityControl) ? $model->order_id : $model->id)

                        },
                        'attributes' => function ($model) {
                            return [
                                'id' => 'action_audit_' . (($model instanceof QualityControl) ? $model->order_id : $model->id),
                                //'target' => '_blank'
                                'data-id' => (($model instanceof QualityControl) ? $model->order_id : $model->id),
                                'data-action' => 'edit',
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.quantitycontrol.audit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Завершить аудит'),
                        'url' => function ($model) {
                            return Url::toRoute('#');
                        },
                        'attributes' => function ($model) {
                            return [
                                'style' => 'display:none',
                                'id' => 'confirm_' . (($model instanceof QualityControl) ? $model->order_id : $model->id),
                                'data-action' => 'confirm',
                                'data-id' => (($model instanceof QualityControl) ? $model->order_id : $model->id),
                                'data-operator_id' => 0,
                                'data-edit' => 0
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.quantitycontrol.confirm');
                        },
                    ],
                ]
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider
    ]),
])

?>

<?php Modal::begin([
    'id' => 'denied_audit',
    'header' => yii::t('common', 'Предупреждение'),
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default', 'id' => 'denied_audit_close'])
]); ?>

<?= yii::t('common', 'Аудит заказа недоступен. Отсутствует запись разговора.'); ?>

<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'order_audit',
    'size' => Modal::SIZE_LARGE,
    'header' => '<h2>' . yii::t('common', 'Аудит заказа') . '</h2>',
    'options' => ['tabindex' => 2],
    'footer' => Html::button(yii::t('common', 'Сохранить'), ['class' => 'btn btn-success', 'id' => 'order_audit_save', 'disabled' => true])
        . Html::button(yii::t('common', 'Закрыть'), ['class' => 'btn btn-default', 'id' => 'order_audit_close', 'disabled' => true])
]); ?>

<div id="order_audit_form"></div>

<?php Modal::end(); ?>

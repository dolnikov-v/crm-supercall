<?php
use common\modules\order\models\Order;
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use backend\assets\SiteAsset;
use common\models\User;

/** @var \backend\modules\operator\models\search\ListOperatorsSearch $modelSearch */
/** @var array $countries */
/** @var array $operators */
/** @var array $partners */

SiteAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'countries')->select2List($user_country, [
                'value' => $modelSearch->countries,
                'multiple' => true,
                'placeholder' => yii::t('common', 'Все'),
            ])->label(\Yii::t('common', 'Страна')) ?>
        </div>

        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'status')->select2List(
                Order::getFinalStatusesCollection() + [Order::STATUS_RECALL => yii::t('common', Order::getStatusByNumber(Order::STATUS_RECALL))], [
                'prompt' => '-',
            ])->label(\Yii::t('common', 'Статус заказа')) ?>
        </div>

        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= $form->field($modelSearch, 'date_range')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'presetDropdown' => true,
                'hideInput' => true,
                'pluginOptions' => [
                    'autoUpdateOnInit' => true,
                    'autoUpdateInput' => false,
                    'timePicker' => true,
                    'timePicker24Hour' => true,
                    'timePickerIncrement' => 1,
                    'locale' => [
                        'format' => 'DD/MM/YYYY H:mm',
                    ],
                ],
                'pluginEvents' => [
                    'cancel.daterangepicker' => 'function(ev, picker) { $("input#ordersearch-updatedrange").val(""); $(this).find("span.range-value").text("") }',
                ],
            ])->label(yii::t('common', 'Дата')); ?>
        </div>

        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'operator_id')->select2List($operators, [
                'multiple' => false,
                'prompt' => '-',
            ])->label(\Yii::t('common', 'Оператор')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'partner_id')->select2List($partners, [
                'multiple' => false,
                'prompt' => '-',
            ])->label(\Yii::t('common', 'Партнёр')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
<?php
use yii\widgets\LinkPager;

/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<?php if ($dataProvider->getPagination()->pageCount > 1) { ?>
    <div class="box-footer clearfix">
        <?= LinkPager::widget(['pagination' => $dataProvider->getPagination()]) ?>
    </div>
<?php } ?>

<?php

use backend\modules\operator\models\AuditedOrders;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\helpers\grid\DataProvider;
use common\modules\order\models\Order;
use common\widgets\base\Label;
use yii\bootstrap\Modal;
use common\widgets\base\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\operator\assets\QualityControlConfirmAsset;

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \backend\modules\operator\models\search\AuditedOrdersSearch $modelSearch */
/** @var array $countries */
/** @var array $controllers */
/** @var array $operators */
/** @var array $statuses */
/** @var array $modificationTypesCollection */
/** @var array $partners */

$this->title = Yii::t('common', 'Проверенные заказы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Аудит'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

QualityControlConfirmAsset::register($this);
?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'dataProvider' => $dataProvider,
        'modelSearch' => $modelSearch,
        'countries' => $countries,
        'controllers' => $controllers,
        'operators' => $operators,
        'statuses' => $statuses,
        'partners' => $partners
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с аудитом'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {

            return [
                'class' => 'tr-vertical-align-middle ',
            ];
        },
        'columns' => [
            [
                'attribute' => 'order_id',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country',
                'headerOptions' => ['class' => 'width-200'],
                'content' => function ($model) {
                    return $model->order->country->name;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'partner_id',
                'headerOptions' => ['class' => 'width-200'],
                'content' => function ($model) {
                    return $model->order->partner->name;
                },
                'label' => yii::t('common', 'Партнёр'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'foreign_id',
                'headerOptions' => ['class' => 'width-150'],
                'content' => function ($model) {
                    return $model->order->foreign_id;
                },
                'label' => yii::t('common', 'Номер у партнёра'),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'operator_status',
                'headerOptions' => ['class' => 'width-150'],
                'content' => function ($model) use ($statuses) {
                    return empty($model->operator_status) ? '' : yii::t('common', Order::getStatusByNumber($model->operator_status));
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'auditor_status',
                'headerOptions' => ['class' => 'width-150'],
                'content' => function ($model) use ($statuses) {
                    return empty($model->auditor_status) ? '' : yii::t('common', Order::getStatusByNumber($model->auditor_status));
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'controller_id',
                'label' => yii::t('common', 'Аудитор'),
                'headerOptions' => ['class' => 'width-150'],
                'content' => function ($model) {
                    return $model->controller->username;
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'edit',
                'label' => yii::t('common', 'Результат проверки'),
                'headerOptions' => ['class' => 'width-150'],
                'content' => function ($model) use ($modificationTypesCollection) {
                    if (!$model->edit) {
                        return Label::widget([
                            'label' => yii::t('common', 'Подтверждён'),
                            'style' => Label::STYLE_SUCCESS,
                        ]);
                    } else {
                        $content = [];

                        foreach (AuditedOrders::getModificationTypes($model->order_id) as $type => $count) {
                            if ($count > 0) {
                                $content[] = $modificationTypesCollection[$type];
                            }
                        }

                        return implode('<br><br>', array_map(function ($v) {
                            return '<span class="label label-danger">' . $v . '</span>';
                        }, $content));
                    }
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'withRelativeTime' => true,
                'attribute' => 'updated_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотр изменений'),
                        'url' => function ($model) {
                            return Url::toRoute('#');
                        },
                        'attributes' => function ($model) {
                            return [
                                'data-action' => 'order-log',
                                'data-order_id' => $model->order_id,
                                'data-controller_id' => $model->controller_id,
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.auditedorders.orderlog') && $model->edit;
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Просмотр аудита'),
                        'url' => function ($model) {
                            return Url::toRoute(['/operator/quality-control/index', 'QualityControlSearch[id]' => $model->id]);

                        },
                        'attributes' => function ($model) {
                            return [
                                'target' => '_blank',
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.auditedorders.audit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Просмотр заказа'),
                        'url' => function ($model) {
                            return Url::toRoute(['/order/index/view', 'id' => $model->order_id]);

                        },
                        'attributes' => function ($model) {
                            return [
                                'target' => '_blank',
                            ];
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('order.index.view');
                        },
                    ],
                ]
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider
    ]),
])

?>

<?php Modal::begin([
    'id' => 'order_log_modal',
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4>' . yii::t('common', 'Просмотр изменений') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Закрыть'), ['class' => 'btn btn-default', 'id' => 'order_log_modal_close'])
]); ?>

<div id="order_log_modal_content"></div>

<?php Modal::end(); ?>

<?php
namespace backend\modules\operator;

use backend\components\base\Module as BackendModule;

/**
 * Class Module
 * @package backend\modules\operator
 */
class Module extends BackendModule
{
    public $controllerNamespace = 'backend\modules\operator\controllers';
}
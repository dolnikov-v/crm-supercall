<?php

namespace backend\modules\operator\controllers;

use backend\components\web\Controller;
use backend\modules\operator\models\search\ZingTreeSearch;
use backend\modules\operator\models\ZingTree;
use common\models\Country;
use common\models\Product;
use common\modules\order\models\OrderType;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\HttpException;

class ZingTreeController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ZingTreeSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'products' => Product::getActiveListProducts(),
            'countries' => Country::find()->collection('common'),
            'order_types' => OrderType::find()->collection('common'),
            'modelSearch' => $modelSearch,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new ZingTree();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'ZingTree успешно добавлен.') : Yii::t('common', 'ZingTree успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'products' => Product::getActiveListProducts(),
            'countries' => Country::find()->collection('common'),
            'order_types' => OrderType::find()->collection('common')
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'ZingTree успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = ZingTree::NOT_ACTIVE;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'ZingTree успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'ZingTree успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    public function getModel($id)
    {
        $model = ZingTree::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'ZingTree не найден.'));
        }

        return $model;
    }

    /**
     * @return string
     */
    public function actionGetZingTree()
    {

        $callback = yii::$app->request->get('callback');
        $country_id = yii::$app->request->get('country_id');
        $product_id = yii::$app->request->get('product_id');
        $order_type_id = yii::$app->request->get('order_type_id');
        $query = ZingTree::getByCountryProductOrderType($country_id, $product_id, $order_type_id);

        if($callback != 'zingtree'){
            return;
        }

        if (!$query->exists()) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'ZingTree не найдены для данной страны')
            ];
        } else {
            $result = [
                'success' => true,
                'url' => ZingTree::IFRAME_ZING_TREE_URL.$query->one()->url,
                'message' => $query->asArray()->one()
            ];
        }

        $resultJson = json_encode($result, JSON_UNESCAPED_UNICODE);

        if ($callback) {
            $resultJson = $callback . '(' . $resultJson . ')';
        }

        return $resultJson;
    }
}
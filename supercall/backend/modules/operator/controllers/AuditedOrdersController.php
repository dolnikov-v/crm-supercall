<?php

namespace backend\modules\operator\controllers;

use backend\components\web\Controller;
use backend\modules\operator\models\AuditedOrders;
use backend\modules\operator\models\search\AuditedOrdersSearch;
use backend\widgets\auditedorders\AuditorLogs;
use backend\widgets\auditedorders\OperatorLogs;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class AuditedOrdersController
 * @package backend\modules\operator\controllers
 */
class AuditedOrdersController extends Controller
{
    public function actionIndex()
    {
        $modelSearch = new AuditedOrdersSearch();

        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $countries = yii::$app->user->getCountries();
        $listCountries = [];

        foreach ($countries as $country) {
            $listCountries[$country->id] = yii::t('common', $country->name);
        }

        $controllers = ArrayHelper::map(User::getActiveByRole(array_values($countries), User::ROLE_AUDITOR), 'id', 'username');
        $operators = ArrayHelper::map(User::getActiveByRole(array_values($countries), [User::ROLE_OPERATOR, User::ROLE_OPERATOR, User::ROLE_APPLICANT]), 'id', 'username');

        $statuses = Order::getStatusesCollection();
        //new not needed
        unset($statuses[Order::STATUS_NEW]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'countries' => $listCountries,
            'controllers' => $controllers,
            'operators' => $operators,
            'statuses' => $statuses,
            'partners' => ArrayHelper::map(yii::$app->user->identity->partners, 'id', 'name'),
            'modificationTypesCollection' => AuditedOrders::modificationTypesCollection()
        ]);
    }

    /**
     * @param $notGroupedLogs
     * @return array
     */
    public function groupLogs($notGroupedLogs, $type = 'order')
    {
        $logs = [];

        if ($type == 'order') {

            foreach ($notGroupedLogs as $log) {
                if (in_array($log->field, AuditedOrders::fieldJSONCollection())) {
                    if ($log->field == AuditedOrders::FIELD_CUSTOMER_COMPONENTS) {
                        $old = json_decode($log->old, 1);
                        $new = json_decode($log->new, 1);

                        $logs[] = [
                            'field' => 'address',
                            'id' => $log['id'],
                            'old' => $old['address'],
                            'new' => $new['address'],
                            'updated_at' => $log['updated_at']
                        ];

                        $logs[] = [
                            'field' => 'general',
                            'id' => $log['id'],
                            'old' => $old['general'],
                            'new' => $new['general'],
                            'updated_at' => $log['updated_at']
                        ];
                    }
                } elseif (!in_array($log->field, AuditedOrders::fieldIgnoreCollection())) {
                    $logs[] = [
                        'id' => $log->id,
                        'field' => $log->field,
                        'old' => $log->old,
                        'new' => $log->new,
                        'updated_at' => $log['updated_at']
                    ];
                }
            }
        } elseif ($type == 'product') {
            return $notGroupedLogs;
        }

        return $logs;
    }

    public function diffData($logBefore, $logAfter)
    {
        $diff = [];

        if (in_array($logBefore['field'], ['address', 'general'])) {
            if (is_array($logBefore['old'])) {
                foreach ($logBefore['old'] as $name => $value) {
                    if (is_array($logAfter['new'])) {
                        foreach ($logAfter['new'] as $name_new => $value_new) {
                            if ($name == $name_new) {
                                if ($value != $value_new) {
                                    $diff[$name] = [
                                        'old' => $value,
                                        'new' => $value_new,
                                        'before_date' => $logBefore['updated_at'],
                                        'after_date' => $logAfter['updated_at'],
                                        'id' => $logAfter['id'],
                                    ];
                                }
                            }
                        }
                    } else {
                        $diff[$name] = [
                            'old' => $value,
                            'new' => '',
                            'before_date' => $logBefore['updated_at'],
                            'after_date' => $logAfter['updated_at'],
                            'id' => $logAfter['id'],
                        ];
                    }
                }
            } else {
                if ($logBefore['field'] == $logAfter['field'] && $logBefore['old'] != $logAfter['new']) {
                    $diff[$logBefore['field']] = [
                        'old' => $logBefore['old'],
                        'new' => $logAfter['new'],
                        'before_date' => $logBefore['updated_at'],
                        'after_date' => $logAfter['updated_at'],
                        'id' => $logAfter['id'],
                    ];
                }
            }
        } else {
            if ($logBefore['field'] == $logAfter['field'] && $logBefore['old'] != $logAfter['new']) {
                $diff[$logBefore['field']] = [
                    'old' => $logBefore['old'],
                    'new' => $logAfter['new'],
                    'before_date' => $logBefore['updated_at'],
                    'after_date' => $logAfter['updated_at'],
                    'id' => $logAfter['id'],
                ];
            }
        }

        return $diff;
    }

    /**
     * @return string
     */
    public function actionGetOrderLogs()
    {
        $order_id = yii::$app->request->post('order_id'); //115190;
        $controller_id = yii::$app->request->post('controller_id'); //1;
        $order = Order::find()->where(['id' => $order_id])->one();

        if (is_null($order)) {
            return json_encode([
                'success' => false,
                'message' => yii::t('common', 'Заказ не найден')
            ], JSON_UNESCAPED_UNICODE);
        } else {
            $attrCollection = PartnerFormAttribute::getLabelCollection($order);

            if (!$attrCollection) {
                return json_encode([
                    'success' => false,
                    'message' => yii::t('common', 'Форма заказа не определена')
                ], JSON_UNESCAPED_UNICODE);
            }

            $logsByAuditorQuery = AuditedOrders::getOrderLogsByAuditor($order_id, $controller_id);
            $logsProductByAuditorQuery = AuditedOrders::getOrderProductLogsByAuditor($order_id, $controller_id);


            if (!$logsByAuditorQuery->exists()) {
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Не найдено изменений данного заказа аудитором')
                ];
            } else {
                //order log compare
                $logsByAuditor = $this->groupLogs($logsByAuditorQuery->all());

                $diff = [];

                if (!empty($logsByAuditor)) {
                    foreach ($logsByAuditor as $idx => $logAuditor) {
                        $tmpBefore = $logAuditor;
                        unset($tmpBefore['new']);

                        $tmpAfter = $logAuditor;
                        unset($tmpAfter['old']);

                        $diffData = $this->diffData($tmpBefore, $tmpAfter);

                        if (!empty($diffData)) {
                            $diff[] = $diffData;
                        }
                    }
                }

                //order product log compare
                $logsProductByAuditor = [];

                if ($logsProductByAuditorQuery->exists()) {
                    $logsProductByAuditor = $this->groupLogs($logsProductByAuditorQuery->asArray()->all(), 'product');
                }

                $data = AuditorLogs::widget([
                    'log' => $diff,
                    'labels' => $attrCollection,
                    'product_log' => $logsProductByAuditor
                ]);


                $result = [
                    'success' => true,
                    'message' => $data
                ];

            }
        }
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
<?php

namespace backend\modules\operator\controllers;

use backend\components\web\Controller;
use backend\modules\operator\models\search\StatisticsOfAuditorsSearch;
use backend\modules\operator\models\StatisticsOfAuditors;
use common\models\User;
use common\modules\order\models\Order;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class StatisticsOfAuditorsController
 * @package backend\modules\operator\controllers
 */
class StatisticsOfAuditorsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new StatisticsOfAuditorsSearch();

        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);

        $modelSearch->load($query);

        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $countries = yii::$app->user->getCountries();
        $listCountries = [];

        foreach ($countries as $country) {
            $listCountries[$country->id] = yii::t('common', $country->name);
        }

        $controllers = ArrayHelper::map(User::getActiveByRole(array_values($countries), User::ROLE_AUDITOR), 'id', 'username');
        $operators = ArrayHelper::map(User::getActiveByRole(array_values($countries), [User::ROLE_OPERATOR, User::ROLE_OPERATOR, User::ROLE_APPLICANT]), 'id', 'username');

        $statuses = Order::getStatusesCollection();
        //new not needed
        unset($statuses[Order::STATUS_NEW]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'countries' => $listCountries,
            'controllers' => $controllers,
            'operators' => $operators,
            'statuses' => $statuses,
            'group' => StatisticsOfAuditors::getGroupCollection(),
        ]);
    }
}
<?php
namespace backend\modules\operator\controllers;

use backend\modules\catalog\models\search\QuestionnaireSearch;
use backend\modules\operator\models\QuestionnaireData;
use backend\modules\operator\models\search\QuestionnaireOrderSearch;
use common\modules\order\models\Order;
use backend\components\web\Controller;
use backend\modules\operator\models\QuestionnaireManagement;
use backend\modules\operator\models\QuestionnaireBase;
use backend\modules\operator\models\QuestionnaireOrder;
use backend\modules\operator\models\search\QuestionnaireBaseSearch;
use common\modules\order\models\OrderLog;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\data\ActiveDataProvider;

/**
 * Class QuestionnaireController
 * @package backend\modules\catalog\controllers
 */
class QuestionnaireController extends Controller
{
    /**
     * Список планов оценки текущего контролера
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new QuestionnaireBaseSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch
        ]);

    }

    /**
     * Создание нового плана оценки работы оператора и наполнение его заказами
     */
    public function actionAddBase()
    {
        $user_id = yii::$app->request->get('user_id');

        if (is_null($user_id)) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Произошла ошибка определения оператора'));
            $this->redirect(Url::to('/access/user'));
            return;
        } else {
            $statuses = array_keys(QuestionnaireManagement::getOrderStatusesCollection());

            $orders = [];

            foreach ($statuses as $k => $status) {
                $founded_orders = $this->getOrdersByStatus(intval($user_id), $status, 5);

                foreach ($founded_orders as $founded_order) {
                    $orders[] = $founded_order;
                }
            }

            if (empty($orders)) {
                $this->redirect(Url::to('/operator/list-operators'));
                Yii::$app->notifier->addNotifier(Yii::t('common', 'У данного оператора отсутствуют заказы'));
            } elseif (count($orders) < 15) {
                $this->redirect(Url::to('/operator/list-operators'));
                Yii::$app->notifier->addNotifier(Yii::t('common', 'У данного оператора не достаточно заказов'));
            } else {
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                $id_base = false;

                //создать план оценки оператора
                $modelBase = new QuestionnaireBase();
                $modelBase->country_id = yii::$app->user->country->id;
                $modelBase->controller_id = yii::$app->user->id;
                $modelBase->operator_id = $user_id;
                $modelBase->is_finished = 0;
                $modelBase->created_at = time();
                $modelBase->updated_at = time();

                if ($modelBase->validate()) {
                    if ($modelBase->save()) {
                        $id_base = $modelBase->id;
                    }
                }

                //план оценки сохранён, нужно сохранить заказы плана
                if ($id_base) {
                    $save_orders = true;

                    foreach ($orders as $order) {
                        $modelOrder = new QuestionnaireOrder();
                        $modelOrder->questionnaire_base_id = $id_base;
                        $modelOrder->order_id = $order->id;
                        $modelOrder->order_status = $order->status;
                        $modelOrder->is_finished = 0;
                        $modelOrder->created_at = time();
                        $modelOrder->updated_at = time();

                        if (!$modelOrder->validate()) {
                            Yii::$app->notifier->addNotifierErrorByModel($modelOrder);
                            $save_orders = false;
                            $transaction->rollBack();
                            break;
                        } else {
                            if (!$modelOrder->save()) {
                                Yii::$app->notifier->addNotifierErrorByModel($modelOrder);
                                $save_orders = false;
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    //если все ордера сохранились
                    if ($save_orders) {
                        $transaction->commit();
                        $this->redirect(Url::to([
                            '/operator/questionnaire/orders/', 'id' => $id_base
                        ]));
                    } else {
                        Yii::$app->notifier->addNotifier(Yii::t('common', 'Ошибка сохранения данных заказа плана оценки оператора'));
                        $this->redirect(Url::to('/operator/list-operators'));
                        return;
                    }
                } else {
                    Yii::$app->notifier->addNotifierErrorByModel($modelBase);
                    Yii::$app->notifier->addNotifier(Yii::t('common', 'Ошибка сохранения плана оценки оператора'));
                    $this->redirect(Url::to('/operator/list-operators'));
                    return;
                }
            }

        }
    }

    /**
     * Список order_id, которые учавствовали в оценке
     * @return array
     */
    public function getAllOrdersQuentionnaire()
    {
        $orders = QuestionnaireOrder::find()
            ->asArray()->all();

        return ArrayHelper::getColumn($orders, 'order_id');
    }

    /**
     * Поиск группы ордеров для плана оценки по конкретному статусу
     * @param $user_id
     * @param $status
     * @param $count
     * @return Order[]
     */
    public function getOrdersByStatus($user_id, $status, $count)
    {
        $orders = Order::find()
            ->leftJoin(OrderLog::tableName(), OrderLog::tableName() . '.order_id=' . Order::tableName() . '.id')
            ->where([Order::tableName() . '.status' => $status])
            ->andWhere([OrderLog::tableName() . '.user_id' => $user_id])
            //->andWhere([Order::tableName() . '.country_id' => yii::$app->user->country->id])
            ->orderBy('RANDOM()')
            ->limit($count);

        //заказы один раз учавствуют в оценке
        $ids = $this->getAllOrdersQuentionnaire();

        if (!empty($ids)) {
            $orders->andWhere(['not in', 'order_id', $ids]);
        }

        return $orders->all();
    }

    /**
     * Завершить план оценки
     * @param $id
     */
    public function actionFinishOff($id)
    {
        $modelBase = QuestionnaireBase::findOne(['id' => $id]);
        $modelBase->is_finished = 1;

        if (!$modelBase->save()) {
            Yii::$app->notifier->addNotifierErrorByModel($modelBase);
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'План оценки работы  оператора успешно завершён'), 'success');
        }

        $this->redirect(Url::to('/operator/questionnaire'));
        return;
    }

    /**
     * Отмена завершения планак оценки
     * @param $id
     */
    public function actionCancelCompletion($id)
    {
        $modelBase = QuestionnaireBase::findOne(['id' => $id]);
        $modelBase->is_finished = 0;

        if (!$modelBase->save()) {
            Yii::$app->notifier->addNotifierErrorByModel($modelBase);
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'План оценки работы запущен в работу'), 'success');
        }

        $this->redirect(Url::to('/operator/questionnaire'));
        return;
    }

    /**
     * Страница заказов плана оценки
     * @return string
     */
    public function actionOrders()
    {

        $modelSearch = new QuestionnaireOrderSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('orders', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch
        ]);
    }


    public function actionEvaluation()
    {
        $model = new QuestionnaireData();
        $questionnaireOrder = QuestionnaireOrder::findOne(Yii::$app->request->queryParams);


        $questionnaireData = QuestionnaireData::find()
            ->joinWith('questionnaire')
            ->where(['questionnaire_order_id' => $questionnaireOrder->id])
            ->all();

        $quiestionnsries = QuestionnaireManagement::findBySql('
            select * from "questionnaire" 
            where order_status::jsonb @> \'[' . $questionnaireOrder->order_status . ']\'::jsonb');

        $dataProvider = new ActiveDataProvider([
            'query' => $quiestionnsries,
        ]);

        if ($model->load(Yii::$app->request->post(), '')) {
            $errors = false;
            $post = Yii::$app->request->post();

            $preSavedData = [];

            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();

            $oldModels = QuestionnaireData::find()
                ->where([QuestionnaireData::tableName() . '.questionnaire_order_id' => $questionnaireOrder->id])->all();

            foreach ($oldModels as $oldModel) {
                $oldModel->delete();
            }


            foreach ($post as $name => $data) {
                if (is_array($data)) {
                    foreach ($data as $num => $value) {
                        $preSavedData[$num][$name] = $value;
                    }
                }
            }

            foreach ($preSavedData as $k => $v) {
                if ($v['required'] == 1 && '-' == ($v['answer_text'])) {
                    Yii::$app->notifier->addNotifier(Yii::t('common', 'Вы ответили не на все вопросы'));
                    $errors = true;
                    break;
                }

                //save data
                if (!$errors) {
                    /**
                     * question_text
                     * questionnaire_id
                     * questionnaire_order_id
                     * required
                     * answer_text
                     */
                    $modelData = new QuestionnaireData();
                    $modelData->question_text = $v['question_text'];
                    $modelData->questionnaire_id = $v['questionnaire_id'];
                    $modelData->questionnaire_order_id = $v['questionnaire_order_id'];
                    $modelData->answer_text = $v['answer_text'];
                    $modelData->created_at = time();
                    $modelData->updated_at = time();

                    if ($modelData->validate()) {
                        if (!$modelData->save()) {
                            Yii::$app->notifier->addNotifier(Yii::t('common', 'Ошибка сохранения ответа на вопрос "{question_text}"', ['question_text' => $v['question_text']]));
                            $errors = false;
                        }
                    } else {

                        Yii::$app->notifier->addNotifier(Yii::t('common', 'Некорректные данные в ответе вопроса "{question_text}"', ['question_text' => $v['question_text']]));
                        $errors = false;
                    }

                } else {
                    $transaction->rollBack();
                }
            }

            //template 00:00:00
            if (!preg_match("#\d{1,2}:\d{1,2}:\d{1,2}#", $post['duration_time'])) {

                Yii::$app->notifier->addNotifier(Yii::t('common', 'Неверное значение поля "Продолжительность разговора"'));
                $errors = true;
            } else {
                //saving parent model
                $path_time = explode(':', $post['duration_time']);
                ///timepeacker to seconds
                $questionnaireOrder->duration_time = $path_time[0] * 60 * 60 + $path_time[1] * 60 + $path_time[2];
                $questionnaireOrder->is_finished = 1;

                if (!$questionnaireOrder->save()) {
                    $errors = true;
                }

                if (!$errors) {
                    Yii::$app->notifier->addNotifier(Yii::t('common', 'Оценка успешно сохранена'), 'success');
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                    Yii::$app->notifier->addNotifier(Yii::t('common', 'Оценка не сохранена'));
                }
            }
        }


        return $this->render('evaluation', [
            'model' => $model,
            'allModels' => $dataProvider->getModels(),
            'modelQuestionnaire' => $model,
            'order' => $questionnaireOrder,
            'questionnaireData' => $questionnaireData
        ]);
    }


    /**
     * @param integer $id
     * @return QuestionnaireManagement
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = QuestionnaireManagement::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Оценка не найдена.'));
        }

        return $model;
    }
}

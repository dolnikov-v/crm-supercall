<?php
namespace backend\modules\operator\controllers;

use backend\components\web\Controller;
use backend\modules\administration\models\QueueUserLog;
use backend\modules\operator\models\ProductOperator;
use backend\modules\operator\models\search\ListOperatorsSearch;
use backend\modules\operator\models\search\ProductOperatorSearch;
use backend\modules\queue\models\Queue;
use backend\modules\queue\models\QueueUser;
use common\components\Notifier;
use common\models\Country;
use common\models\Product;
use common\models\Teams;
use common\models\TeamsCountry;
use common\models\User;
use common\models\UserCountry;
use common\models\UserPartner;
use common\models\UserTeam;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use backend\modules\administration\models\ApiLog;
use common\models\Asterisk;


/**
 * Class ListOperatorsController
 * @package backend\modules\operator\controllers
 */
class ListOperatorsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ListOperatorsSearch();

        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $user_ids = ArrayHelper::getColumn($dataProvider->getModels(), 'id');

        //товары закреплённые за оператором
        $products = ProductOperator::find()
            ->select([
                Product::tableName() . '.*',
                ProductOperator::tableName() . '.*',
            ])
            ->leftJoin(Product::tableName(), Product::tableName() . '.id=' . ProductOperator::tableName() . '.product_id')
            ->where([
                'in',
                'user_id',
                $user_ids
            ])
            ->orderBy([ProductOperator::tableName().'.user_id' => SORT_ASC])
            ->all();

        $listProductsByUser = [];
        foreach ($products as $model) {
            $listProductsByUser[$model->user_id][] = $model->product->name;
        }
        //очереди с которыми работает оператор
        $listQueuesByUser = [];
        foreach ($user_ids as $user_id) {
            $queues = QueueUser::find()
                ->select([
                    QueueUser::tableName() . '.*',
                    Queue::tableName() . '.*'
                ])
                ->leftJoin(Queue::tableName(), Queue::tableName() . '.id=' . QueueUser::tableName() . '.queue_id')
                ->where([
                    'in',
                    'user_id',
                    $user_id
                ])
                ->all();
            $listQueuesByUser[$user_id] = [];
            if (is_array($queues) && !empty($queues)) {
                $listQueuesByUser[$user_id] = ArrayHelper::getColumn($queues, 'queue.name');
            }
        }

        //страны доступные оператору
        $countries = UserCountry::find()
            ->select([
                UserCountry::tableName() . '.*',
                Country::tableName() . '.*'
            ])
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . UserCountry::tableName() . '.country_id')
            ->where([
                'in',
                'user_id',
                $user_ids
            ])
            ->all();

        $listCountriesByUser = [];

        foreach ($countries as $model) {
            $listCountriesByUser[$model->user_id][] = $model->country->name;
        }

        $user_country = [];
        $countries_list = ArrayHelper::map(yii::$app->user->getCountries(), 'id', 'name');

        foreach ($countries_list as $k => $country) {
            $user_country[$k] = yii::t('common', $country);
        }

        $teams = TeamsCountry::find()
            ->select([
                Teams::tableName() . '.*'
            ])
            ->leftJoin(Teams::tableName(), Teams::tableName() . '.id=' . TeamsCountry::tableName() . '.team_id')
            ->where(['in', 'country_id', ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id')])
            ->asArray()
            ->all();

        $partner_id = null;
        $partner = UserPartner::findOne(['user_id' => Yii::$app->user->id]);

        if($partner){
            $partner_id = $partner->partner_id;
        }

        $queues = [];

        if($queuesByCountries = Queue::getQueuesByPartnerOrCountry($partner_id, ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id'))){
            $queues = ArrayHelper::map($queuesByCountries, 'id', 'name');
        }

        $productsForFileter = Product::find()->where(['active' => 1])->asArray()->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'products' => $listProductsByUser,
            'queues' => $listQueuesByUser,
            'countries' => $listCountriesByUser,
            'modelSearch' => $modelSearch,
            'user_country' => $user_country,
            'teams' => ArrayHelper::map($teams, 'id', 'name'),
            'queues_list' => $queues,
            'productsForFilter' => ArrayHelper::map($productsForFileter,'id','name')
        ]);


    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionSetProduct()
    {
        $errors = false;

        $params = Yii::$app->request->queryParams;
        $user_id = isset($params['id']) ? $params['id'] : false;

        if (!$user_id) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Пользователь на найден'));
            return $this->redirect(Url::toRoute('index'));
        } else {
            $searchModel = new ProductOperatorSearch(['user_id' => $user_id]);

            if ($searchModel->load(Yii::$app->request->post())) {
                if ($searchModel->validate()) {
                    $connection = Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    /*
                     //очистить список продуктов и записать по новой, аккуратно
                    $transaction = $connection->beginTransaction();
                    $productOperatorModel = $searchModel->search(['user_id' => $user_id]);

                    foreach ($productOperatorModel->getModels() as $model) {
                        if (!$model->delete()) {
                            $errors = true;
                        }
                    }*/

                    //убрали все товары у пользователя
                    $products = ProductOperator::findAll(['user_id' => $user_id]);

                    foreach ($products as $model) {
                        if (!$model->delete()) {
                            $errors = true;
                            break;
                        }
                    }

                    $post = yii::$app->request->post('ProductOperatorSearch');

                    if (isset($post['product_ids'])) {
                        //записать по новой все выбранные товары
                        foreach ($post['product_ids'] as $product_id) {
                            $model = new ProductOperator();
                            $model->user_id = $user_id;
                            $model->product_id = $product_id;

                            if (!$model->save()) {
                                $errors = true;
                                break;
                            }
                        }
                    }

                    if (!$errors) {
                        Yii::$app->notifier->addNotifier(yii::t('common', 'Товары успешно сохранены'), Notifier::TYPE_SUCCESS);
                        $transaction->commit();
                    } else {
                        Yii::$app->notifier->addNotifier(yii::t('common', 'Ошибка сохранения товаров'));
                        $transaction->rollBack();
                    }

                } else {
                    Yii::$app->notifier->addNotifier(yii::t('common', 'Ошибка при валидации данных'));
                }

            }

            //все товары пользователя
            $products = ProductOperator::find()
                ->joinWith('product')
                ->where(['user_id' => $user_id])
                ->asArray()
                ->all();

            $productList = [];

            foreach ($products as $list) {
                $productList[] = $list['product']['id'];
            }

            $user = User::find()->where(['id' => $user_id])->asArray()->one();

            $productFilter = Product::getActiveListProducts();

            return $this->render('set-product', [
                'searchModel' => isset($searchModel) ? $searchModel : new ProductOperatorSearch(),
                'user_id' => $user_id,
                'productFilter' => $productFilter,
                'productList' => $productList,
                'username' => $user['username']
            ]);
        }
    }

    function actionQueue()
    {
        $params = Yii::$app->request->queryParams;
        $user_id = isset($params['id']) ? $params['id'] : false;
        $user = User::findOne(['id' => $user_id]);
        $queue_user = QueueUser::findAll(['user_id' => $user_id]);

        $listQueueUser = ArrayHelper::getColumn($queue_user, 'queue_id');

        if (!$user) {
            throw new HttpException(404, Yii::t('common', 'Оператор не найден'));
        }

        $countries = ArrayHelper::getColumn(UserCountry::findAll(['user_id' => $user_id]), 'country_id');
        $queryQueues = Queue::findAll(['active' => Queue::STATUS_ACTIVE]);

        $queuesLikeOperator = [];

        //необходимо подобрать оператору очереди по стране/странам и по товару/товарам
        //если у оператора нет закреплённых товаров - все по странам/стране
        foreach ($queryQueues as $queue) {
            $primary_components = json_decode($queue->primary_components);

            //интересуют очереди - только со странами
            if (isset($primary_components->primary_country)) {
                $queueCountries = json_decode($primary_components->primary_country);
                $queueProducts = json_decode($primary_components->primary_product);

                //очередь может быть закреплена только за одной страной
                if (is_numeric($queueCountries)) {
                    //оператор работает с данной страной
                    if (in_array($queueCountries, $countries)) {

                        if ($this->getProductOperator($user_id, $queueProducts)) {
                            $queuesLikeOperator[] = $queue;
                        }
                    }
                } elseif (is_array($queueCountries)) {
                    //очередь может принимать заказы с нескольких стран
                    $diff = array_diff($queueCountries, $countries);

                    //очередь содержит страны оператора, и "левых стран" в очереди нет, очередь подходит для оператора
                    if (empty($diff)) {
                        if ($this->getProductOperator($user_id, $queueProducts)) {
                            $queuesLikeOperator[] = $queue;
                        }
                    }
                }
            }

        }

        return $this->render('queue', [
            'model' => new Queue,
            'user' => $user,
            'listQueueUser' => $listQueueUser,
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $queuesLikeOperator,
            ])
        ]);
    }

    /**
     * Метод вернёт список product_id закреплённый за оператором
     * @param integer $user_id
     * @param [] $productsQueue
     * @return bool
     */
    public function getProductOperator($user_id, $productsQueue)
    {
        $operatorProducts = ArrayHelper::getColumn(ProductOperator::findAll(['user_id' => $user_id]), 'product_id');

        //у очереди не указаны товары и к оператору не привязаны товары
        if (is_null($productsQueue) && empty($operatorProducts)) {
            return true;
        } //если в очереди есть товары - то очередь под определённого оператора, значит у оператора должны быть товары
        elseif (is_null($productsQueue) || empty($operatorProducts)) {
            return false;
        } else {
            $diff = array_diff($productsQueue, $operatorProducts);
            return empty($diff);
        }
    }

    public function actionQueueSet()
    {
        $post = yii::$app->request->post();

        if (empty($post['queue_id']) || empty($post['user_id'])) {
            return json_encode([
                'success' => false,
                'message' => yii::t('common', 'Действие не выполнено')
            ], JSON_UNESCAPED_UNICODE);
        }

        $model = new QueueUser();
        $model->queue_id = $post['queue_id'];
        $model->user_id = $post['user_id'];
        $model->created_at = time();
        $model->updated_at = time();

        if ($model->save()) {
            //отключил, т.к. на астере операторы распределяются на подочереди в php yii auto-call/generate
            //Queue::updateAsteriskMemberQueues($post['user_id'], [$post['queue_id']], 'set');
            QueueUserLog::saveQueue($model->queue_id, $model->user_id, QueueUserLog::ACTION_INSERT);

            $result = [
                'success' => true,
                'message' => yii::t('common', 'Действие выполнено')
            ];
        } else {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Действие не выполнено')
            ];
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function actionQueueUnSet()
    {
        $post = yii::$app->request->post();

        if (empty($post['queue_id']) || empty($post['user_id'])) {
            return json_encode([
                'success' => false,
                'message' => yii::t('common', 'Действие не выполнено')
            ], JSON_UNESCAPED_UNICODE);
        }

        $model = QueueUser::find()
            ->where(['queue_id' => $post['queue_id']])
            ->andWhere(['user_id' => $post['user_id']])
            ->one();

        if ($model->delete()) {
            //отключил, т.к. на астере операторы распределяются на подочереди в php yii auto-call/generate
            //Queue::updateAsteriskMemberQueues($post['user_id'], [$post['queue_id']], 'unset');
            QueueUserLog::saveQueue($model->queue_id, $model->user_id, QueueUserLog::ACTION_DELETE);

            $result = [
                'success' => true,
                'message' => yii::t('common', 'Действие выполнено')
            ];
        } else {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Действие не выполнено')
            ];
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param $id
     */
    public function actionManageTeam($id)
    {
        if (yii::$app->request->isPost) {
            $post = yii::$app->request->post('User');
            $updateTeams = UserTeam::updateUserTeams($post['user_id'], $post['team_id']);

            if (!$updateTeams) {
                Yii::$app->notifier->addNotifier(Yii::t('common', 'Ошибка добавления пользователя в команды'));
            } else {
                Yii::$app->notifier->addNotifier(Yii::t('common', 'Команды пользователя успешно обновлены'), Notifier::TYPE_SUCCESS);

                $redirect = Yii::$app->request->post('redirect');

                if (!empty($redirect) && $redirect != Yii::$app->request->absoluteUrl) {
                    return $this->redirect($redirect);
                }
                return $this->redirect(Url::toRoute(['index']));

            }
        }

        $user = User::findOne(['id' => $id]);
        $user_teams = UserTeam::findAll(['user_id' => $user->id]);

        $teams = Teams::find()
            ->leftJoin(TeamsCountry::tableName(), 'team_id=' . Teams::tableName() . '.id')
            ->where([
                'active' => Teams::STATUS_ACTIVE,
                'country_id' => ArrayHelper::getColumn($user->countries, 'id')
            ])->all();

        return $this->render('manage-team', [
            'user' => $user,
            'redirect' => Yii::$app->request->referrer ?? '',
            'teams' => ArrayHelper::map($teams, 'id', 'name'),
            'user_teams' => ArrayHelper::getColumn($user_teams, 'team_id')
        ]);
    }
}

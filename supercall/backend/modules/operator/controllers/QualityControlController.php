<?php

namespace backend\modules\operator\controllers;

use backend\components\web\Controller;
use backend\modules\operator\models\QualityControl;
use backend\widgets\callrecords\CallRecords;
use common\models\Timezone;
use common\models\User;
use common\modules\order\models\asterisk\Records;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use console\controllers\WorkersController;
use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\operator\models\search\QualityControlSearch;
use common\models\UserCountry;
use common\models\Country;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class QualityControlController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new QualityControlSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $countries = UserCountry::find()
            ->select([
                UserCountry::tableName() . '.*',
                Country::tableName() . '.*'
            ])
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . UserCountry::tableName() . '.country_id')
            ->where(['user_id' => Yii::$app->user->id])
            ->all();

        $listCountriesByUser = [];

        foreach ($countries as $model) {
            $listCountriesByUser[$model->user_id][] = $model->country->name;
        }

        $user_country = [];
        $countries_list = ArrayHelper::map(yii::$app->user->getCountries(), 'id', 'name');

        foreach ($countries_list as $k => $country) {
            $user_country[$k] = yii::t('common', $country);
        }

        $operators = ArrayHelper::map(User::getActiveByRole(array_values($user_country), [User::ROLE_OPERATOR, User::ROLE_OPERATOR, User::ROLE_APPLICANT]), 'id', 'username');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'user_country' => $user_country,
            'status' => $modelSearch->status,
            'operators' => $operators,
            'partners' => ArrayHelper::map(yii::$app->user->identity->partners, 'id', 'name')
        ]);
    }

    /**
     * old function see actionGetRecords
     * @return string
     */
    public function actionGetRecord()
    {
        $order_id = yii::$app->request->post('order_id');

        $workersController = new WorkersController('workers', null);

        $order_id = explode('_', $order_id);

        if (count($order_id) == 2) {
            $order_id = $order_id[1];
        } else {
            $order_id = $order_id[0];
        }

        ob_start();
        $workersController->actionGetRecords('order_id', $order_id);
        ob_end_clean();

        $recordQuery = Records::find()->where(['order_id' => $order_id]);

        $id = 'audio_' . $order_id;

        $template = '<audio id="{id}" controls>
                            <source src="{record}" type="audio/mpeg">
                             Your browser does not support the audio element.
                            </audio>
                            <script>document.getElementById("{id}").currentTime = "{action_time}"</script>';

        if ($recordQuery->exists()) {
            $record = $recordQuery->one();
            $recordPath = Records::RECORD_FULL_PATH . DIRECTORY_SEPARATOR . $record->order_id . DIRECTORY_SEPARATOR . basename($record->path);

            return json_encode([
                'success' => true,
                'message' => strtr($template, [
                    '{id}' => $id,
                    '{record}' => $recordPath
                ])
            ], JSON_UNESCAPED_UNICODE);
        } else {

            return json_encode([
                'success' => false,
                'message' => '<span class="text-danger">' . yii::t('common', 'Запись не найдена') . '</span>',
            ], JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * @return string
     */
    public function actionGetRecords()
    {
        $phones = yii::$app->request->post('phones');
        $order_id = yii::$app->request->post('order_id');

        $records = Records::getFullDataFromAsterisk($phones);

        $id = 'audio_' . $order_id;

        $template = '<audio id="{id}" controls>
                            <source src="{record}" type="audio/mpeg">
                             Your browser does not support the audio element.
                            </audio>';

        if ($records === false) {
            return json_encode([
                'success' => false,
                'message' => '<span class="text-danger">' . yii::t('common', 'Ошибка запроса') . '</span>',
                'line_content' => '<span class="text-danger">' . yii::t('common', 'Ошибка запроса') . '</span>'
            ], JSON_UNESCAPED_UNICODE);
        } elseif (empty($records)) {
            $content = '<span class="text-info">' . yii::t('common', 'Записи не найдены') . '</span>';
            $line_content = '<span class="text-info">' . yii::t('common', 'Записи не найдены') . '</span>';
        } else {
            $content = '';
            $line_content = '';

            $listUsers = ArrayHelper::map(User::find()->All(), 'id', 'username');

            $records = array_reverse($records);

            foreach ($records as $record) {
                $user_id = strtr($record["src"], ['2wcall' => '']);

                $username = !isset($listUsers[$user_id]) ? yii::t('common', 'Не определён') : $listUsers[$user_id];

                $record_url = Records::ASTERISK_DOWNLOAD_URL . '/' . $record["recordingfile"];

                //для грида
                $content .= '<div class="row">
                             <div class="col-lg-2 record_operator"><b>' . $username . '</b></div>  
                             <div class="col-lg-4 record_date">' . $record["calldate"] . '</div>  
                             <div class="col-lg-6"></div>
                             </div>
                             <div class="row">
                             <div class="col-lg-12 record_player">  ' . strtr($template, ['{id}' => $id, '{record}' => $record_url]) . '</div>
                             </div><br/>';

                //для формы ордера
                $line_content .= '<div class="row">
                             <div class="col-lg-2 line_record_operator"><b>' . $username . '</b></div>  
                             <div class="col-lg-3 record_date">' . $record["calldate"] . '</div>  
                             <div class="col-lg-5">  ' . strtr($template, ['{id}' => $id, '{record}' => $record_url]) . '</div>
                             <div class="col-lg-2"></div>
                             </div>';
            }
        }

        return json_encode([
            'success' => true,
            'message' => $content,
            'line_content' => $line_content
        ], JSON_UNESCAPED_UNICODE);

    }

    /**
     * @return string
     */
    public function actionGetWidget()
    {
        $order_id = yii::$app->request->post('order_id');
        $order = Order::find()->where(['id' => $order_id])->one();

        if (is_null($order)) {
            $content = yii::t('common', 'Заказ не найден');
        } else {

            $content = CallRecords::widget([
                'order' => $order,
                'columns' => [
                    CallRecords::COLUMN_CALL_RECORD,
                    CallRecords::COLUMN_CALL_STATUS,
                    CallRecords::COLUMN_OPERATOR
                ]
            ]);
        }

        return json_encode([
            'success' => true,
            'message' => $content,
            'line_content' => $content
        ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * yii::$app->params['urlBackend'].'/operator/transfer-file?file='
     */
    public function actionTransferFile()
    {
        $file = yii::$app->request->get('file');

        header('Content-Type: audio/mp3');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        readfile( $file);
    }

    /**
     * @return string
     */
    public function actionConfirm()
    {

        $data = [
            'order_id' => (int)yii::$app->request->post('order_id'),
            'controller_id' => (int)Yii::$app->user->id,
            'operator_status' => (int)yii::$app->request->post('operator_status'),
            'auditor_status' => (int)yii::$app->request->post('auditor_status'),
            'edit' => (bool)yii::$app->request->post('edit'),
            'confirm' => true
        ];

        $dateTime = (new \DateTime('now', new \DateTimeZone(Timezone::DEFAULT_TIMEZONE)))->getTimestamp();
        //$data['created_at'] = $dateTime;
        $data['updated_at'] = $dateTime;

        $lastAudit = QualityControl::findOne(['order_id' => $data['order_id'], 'controller_id' => $data['controller_id']]);

        $model = is_null($lastAudit) ? new QualityControl() : $lastAudit;

        //снять блокировку
        $this->blockOrderToAudit(Order::findOne(['id' => $data['order_id']]), false);

        //поиск оператора, который последний поставил статус заказу
        $orderLogQuery = OrderLog::find()
            ->where(['order_id' => $data['order_id']])
            ->andWhere(['field' => 'status'])
            ->andWhere(['is not', 'user_id', null])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1);

        if ($orderLogQuery->exists()) {
            $order_log = $orderLogQuery->one();
            $data['operator_id'] = $order_log->user_id;
        } else {
            $data['operator_id'] = null;
        }

        if ($model->load($data, '') && $model->save()) {
            return json_encode([
                'success' => true,
            ], JSON_UNESCAPED_UNICODE);
        } else {
            return json_encode([
                'success' => false,
                'data' => $data,
                'error' => $model->getErrors()
            ], JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * @return array|string
     */
    public function actionGetEditForm()
    {
        $order_id = yii::$app->request->post('order_id');
        $order = Order::findOne(['id' => $order_id]);

        $openAuditOrder = QualityControl::findOne(['order_id' => $order_id, 'confirm' => null]);
        $queue_order = (new Query())->from('{{%queue_order}}')->where(['order_id' => $order_id]);

        //если заказ отурыл на аудит другой аудитор
        if ($openAuditOrder) {
            if ($openAuditOrder->controller_id != yii::$app->user->identity->id) {
                return [
                    'success' => false,
                    'message' => yii::t('common', 'Заказ №{order_id} открыт для аудита другим пользователем', ['order_id' => $order_id])
                ];
            } else {
                if ($queue_order->exists()) {
                    $model = $queue_order->one();

                    if ($model['user_id'] != yii::$app->user->identity->id) {
                        return json_encode([
                            'success' => false,
                            'message' => yii::t('common', 'Заказ №{order_id} открыт другим пользователем', ['order_id' => $order_id])
                        ], JSON_UNESCAPED_UNICODE);
                    }
                }
            }
        } else {
            $current_time = (new \DateTime())->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))->getTimestamp();

            $orderAudit = new QualityControl();
            $orderAudit->order_id = $order_id;
            $orderAudit->controller_id = yii::$app->user->identity->id;
            $orderAudit->created_at = $current_time;
            $orderAudit->updated_at = $current_time;

            if (!$orderAudit->validate() || !$orderAudit->save()) {
                return json_encode([
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка открытия заказ №{order_id} для аудита: {error}', [
                        'order_id' => $order_id,
                        'error' => json_encode($orderAudit->getErrors(), JSON_UNESCAPED_UNICODE)])
                ], JSON_UNESCAPED_UNICODE);
            }
        }

        if (!$order) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Заказ №{order_id} не найден', ['order_id' => $order_id])
            ];
        } else {
            //поставить блокировку
            $this->blockOrderToAudit($order);

            $url = $order->generateIframeUrl(yii::$app->user->identity, true);

            $result = [
                'success' => true,
                'message' => $url,
                'order_id' => $order_id
            ];
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param $order
     * @param bool $set_block
     */
    public function blockOrderToAudit($order, $set_block = true)
    {
        if ($order instanceof Order) {
            //ставим на блокировку
            if ($set_block) {
                $current_time = (new \DateTime())->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))->getTimestamp();
                $blocked_to = $current_time + 3600;

                //Order::updateByParams(['blocked_to' => $blocked_to], ['id' => $order->id]);
                Yii::$app->db->createCommand()->insert('{{%queue_order}}', [
                    'user_id' => yii::$app->user->identity->id,
                    'order_id' => $order->id,
                    'blocked_to' => $blocked_to
                ])->execute();
            } else {
                //Order::updateByParams(['blocked_to' => null], ['id' => $order->id]);
                $queueOrderQuery = (new Query())
                    ->from('{{%queue_order}}')
                    ->where([
                        'user_id' => yii::$app->user->identity->id,
                        'order_id' => $order->id
                    ]);

                if ($queueOrderQuery->exists()) {
                    $model = $queueOrderQuery->one();
                    yii::$app->db->createCommand('delete from queue_order where user_id = ' . $model['user_id'] . ' and order_id = ' . $model['order_id'])->execute();
                }
            }
        }
    }
}
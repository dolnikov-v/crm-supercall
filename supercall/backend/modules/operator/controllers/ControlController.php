<?php

namespace backend\modules\operator\controllers;

use backend\components\web\Controller;
use common\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;

class ControlController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-operators-by-countries' => ['POST']
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['get-operators-by-countries'])) {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionGetOperatorsByCountries()
    {
        $countries = yii::$app->request->post('countries');
        if ($countries) {
            $countries = array_map(function ($elem) {
                return ['id' => $elem];
            }, $countries);
        }
        $operators = User::getActiveByRole($countries, [User::ROLE_OPERATOR, User::ROLE_APPLICANT, User::ROLE_SENIOR_OPERATOR]);

        return Json::encode($operators);
    }
}

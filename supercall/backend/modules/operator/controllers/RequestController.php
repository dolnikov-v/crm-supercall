<?php
namespace backend\modules\operator\controllers;

use backend\components\web\Controller;
use backend\modules\access\models\AuthAssignment;
use backend\modules\access\models\AuthItem;
use backend\modules\operator\models\Request;
use backend\modules\operator\models\search\RequestSearch;
use Yii;
use common\models\User;
use yii\helpers\Url;

class RequestController extends Controller
{
    public function actionIndex()
    {
        $modelSearch = new RequestSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch
        ]);
    }

    public function actionConfirm($id)
    {
        $model = AuthAssignment::findOne(['user_id' => $id]);
        $user = User::findOne(['id' => $id]);
        
        $transaction = yii::$app->db->beginTransaction();

        $user->save();
        
        $model->delete();

        $role = AuthItem::findOne(['name' => User::ROLE_OPERATOR]);
        $authManager = yii::$app->get('authManager');

        if (!$authManager->assign($role, $id)) {
            $transaction->rollBack();
            Yii::$app->notifier->addNotifierErrorByModel($model);
        } else {
            //оповестить пользователя о акцтивации аккаунта
            Yii::$app->mailer
                ->compose('@frontend/modules/auth/mail/register', ['user' => $user, 'activatedmail' => true])
                ->setTo($user->email)
                ->setSubject(Yii::t('common', 'Активация аккаунта 2wcall.com'))
                ->send();

            $transaction->commit();
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Заявитель активирован'), 'success');
        }

        $this->redirect(Url::to('/operator/request'));
        return;
    }

    public function actionReject($id)
    {
        $model = User::findOne(['id' => $id]);
        $model->status = User::STATUS_BLOCKED;

        if (!$model->save()) {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Заявка отклонена'), 'success');
        }

        $this->redirect(Url::to('/operator/request'));
        return;
    }

    public function actionBlock($id)
    {
        $modelAuth = AuthAssignment::findOne(['user_id' => $id]);
        $modelAuth->delete();

        $model = User::findOne(['id' => $id]);
        $model->status = User::STATUS_BLOCKED;

        $role = AuthItem::findOne(['name' => User::ROLE_APPLICANT]);
        $authManager = yii::$app->get('authManager');

        if (!$authManager->assign($role, $id)) {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        if (!$model->save()) {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Оператор заблокирован'), 'success');
        }

        $this->redirect(Url::to('/operator/request'));
        return;
    }

    public function actionUnblock($id)
    {
        $model = User::findOne(['id' => $id]);
        $model->status = User::STATUS_APPLICANT;

        if (!$model->save()) {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Оператор заблокирован'), 'success');
        }

        $this->redirect(Url::to('/operator/request'));
        return;
    }
}
<?php
namespace backend\modules\operator\controllers;

use backend\components\web\Controller;
use common\modules\order\models\Order;
use backend\modules\operator\models\QuestionnaireManagement;
use backend\modules\operator\models\search\QuestionnaireManagementSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;


/**
 * Class QuestionnaireManagementController
 * @package backend\modules\catalog\controllers
 */
class QuestionnaireManagementController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new QuestionnaireManagementSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'order_status' => QuestionnaireManagement::getOrderStatusesCollection()
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new QuestionnaireManagement();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->validate()) {
                if ($model->save()) {
                    Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Вопрос успешно добавлен.') : Yii::t('common', 'Вопрос успешно отредактирован.'), 'success');

                    return $this->redirect(Url::toRoute('index'));
                } else {
                    Yii::$app->notifier->addNotifierErrorByModel($model);
                }
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        if (!$model->isNewRecord) {
            $answers = json_decode($model->answers);
            $order_statuses = json_decode($model->order_status);

            $data = [
                'model' => $model,
                'order_status' => QuestionnaireManagement::getOrderStatusesCollection(),
                'order_status_values' => array_values($order_statuses),
                'type_answers' => QuestionnaireManagement::getTypeAnswersCollection(),
                'answers' => QuestionnaireManagement::getAnswersCollection(),
                'type_answer' => !$answers->mask && !isset($answers->answers) ? QuestionnaireManagement::TYPE_TEXT : QuestionnaireManagement::TYPE_OPTIONS,
                'mask' => $answers->mask ? $answers->mask : QuestionnaireManagement::ANSWER_OTHER,
                'answers_other' => isset($answers->answers) ? $answers->answers : [],
                'answers_other_values' => isset($answers->answers) ? array_keys($answers->answers) : ''
            ];
        } else {
            $data = [
                'model' => $model,
                'order_status' => QuestionnaireManagement::getOrderStatusesCollection(),
                'order_status_values' => array_keys(QuestionnaireManagement::getOrderStatusesCollection()),
                'type_answers' => QuestionnaireManagement::getTypeAnswersCollection(),
                'answers' => QuestionnaireManagement::getAnswersCollection(),
                'type_answer' => QuestionnaireManagement::TYPE_TEXT,
                'mask' => '',
                'answers_other' => [],
                'answers_other_values' => ''
            ];
        }


        return $this->render('edit', $data);
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Вопрос успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));


    }

    /**
     * @param integer $id
     * @return QuestionnaireManagement
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = QuestionnaireManagement::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Вопрос не найден.'));
        }

        return $model;
    }

}
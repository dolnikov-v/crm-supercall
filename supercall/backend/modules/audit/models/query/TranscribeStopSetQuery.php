<?php

namespace backend\modules\audit\models\query;

use yii\db\ActiveQuery;

/**
 * Class TranscribeStopSetQuery
 * @package backend\modules\audit\models\query
 */
class TranscribeStopSetQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function bySettingId($id)
    {
        return $this->andWhere(['transcribe_setting_id' => $id]);
    }

    /**
     * @param $status
     * @return $this
     */
    public function byStatus($status)
    {
        return $this->andWhere(['status' => $status]);
    }

    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|\yii\db\ActiveRecord
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
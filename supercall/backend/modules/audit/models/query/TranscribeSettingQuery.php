<?php

namespace backend\modules\audit\models\query;

use backend\modules\audit\models\TranscribeSetting;
use yii\db\ActiveQuery;

/**
 * Class TranscribeSettingQuery
 * @package backend\modules\audit\models\query
 */
class TranscribeSettingQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function byCountry($id)
    {
        return $this->andWhere(['country_id' => $id]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['active' => TranscribeSetting::ACTIVE]);
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere(['active' => TranscribeSetting::NOT_ACTIVE]);
    }

    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|\yii\db\ActiveRecord
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
<?php

namespace backend\modules\audit\models;

use common\components\db\ActiveRecord;
use common\models\Country;
use common\models\Product;
use Yii;

/**
 * This is the model class for table "aggregate_word_stat".
 *
 * @property integer $id
 * @property integer $word_id
 * @property integer $country_id
 * @property integer $product_id
 * @property integer $total
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AggregateWordBase $word
 * @property Country $country
 * @property Product $product
 */

/**
 * Class AggregateWordStat
 * @package backend\modules\audit\models
 */
class AggregateWordStat extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%aggregate_word_stat}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['word_id', 'country_id', 'product_id'], 'required'],
            [['word_id', 'country_id', 'product_id', 'total', 'created_at', 'updated_at'], 'integer'],
            [['word_id'], 'exist', 'skipOnError' => true, 'targetClass' => AggregateWordBase::className(), 'targetAttribute' => ['word_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'word_id' => Yii::t('common', 'Слово'),
            'country_id' => Yii::t('common', 'Страна'),
            'product_id' => Yii::t('common', 'Товар'),
            'total' => Yii::t('common', 'Всего'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(AggregateWordBase::className(), ['id' => 'word_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
<?php

namespace backend\modules\audit\models;

use backend\modules\order\models\AddressVerification;
use common\modules\order\models\OrderLog;
use Yii;

/**
 * Class VerifiedAddress
 * @package backend\modules\audit\models
 */
class VerifiedAddress extends AddressVerification
{
    /** @var  integer */
    public $controller_id;

    /** @var  string */
    public $date_range;

    /** @var  string */
    public $username;


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'controller_id' => Yii::t('common', 'Верификатор'),
            'date_range' => Yii::t('common', 'Период'),
            'country_id' => Yii::t('common', 'Страна'),
            'partner_id' => Yii::t('common', 'Партнёр'),
            'id' => Yii::t('common', 'Номер'),
            'foreign_id' => Yii::t('common', 'Номер у партнёра'),
            'confirm_address' => Yii::t('common', 'Проверка адреса')
        ];
    }
}
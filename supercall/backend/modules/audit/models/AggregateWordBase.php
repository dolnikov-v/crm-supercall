<?php

namespace backend\modules\audit\models;

use common\components\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "aggregate_word_base".
 *
 * @property integer $id
 * @property string $word
 * @property integer $total
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AggregateWord[] $aggregateWords
 * @property AggregateWordStat[] $aggregateWordStats
 */

/**
 * Class AggregateWordBase
 * @package backend\modules\audit\models
 */
class AggregateWordBase extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%aggregate_word_base}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['word'], 'required'],
            [['total', 'created_at', 'updated_at'], 'integer'],
            [['word'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'word' => Yii::t('common', 'Слово'),
            'total' => Yii::t('common', 'Всего'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAggregateWords()
    {
        return $this->hasMany(AggregateWord::className(), ['word_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAggregateWordStats()
    {
        return $this->hasMany(AggregateWordStat::className(), ['word_id' => 'id']);
    }
}
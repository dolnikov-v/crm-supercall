<?php

namespace backend\modules\audit\models;

use backend\modules\audit\models\query\TranscribeStatusSetQuery;
use Yii;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "transcribe_status_set".
 *
 * @property integer $id
 * @property integer $transcribe_setting_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TranscribeSetting[] $transcribeSettings
 * @property TranscribeSetting $transcribeSetting
 */

/**
 * Class TranscribeStatusSet
 * @package backend\modules\audit\models
 */
class TranscribeStatusSet extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%transcribe_status_set}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['transcribe_setting_id', 'status'], 'required'],
            [['transcribe_setting_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['transcribe_setting_id'], 'exist', 'skipOnError' => true, 'targetClass' => TranscribeSetting::className(), 'targetAttribute' => ['transcribe_setting_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'transcribe_setting_id' => Yii::t('common', 'Transcribe Setting ID'),
            'status' => Yii::t('common', 'Статус'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranscribeSetting()
    {
        return $this->hasMany(TranscribeSetting::className(), ['id' => 'transcribe_setting_id']);
    }

    /**
     * @return TranscribeStatusSetQuery
     */
    public static function find()
    {
        return new TranscribeStatusSetQuery(get_called_class());
    }
}
<?php

namespace backend\modules\audit\models\search;

use backend\modules\audit\models\VerifiedAddress;
use backend\modules\order\models\AddressVerification;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class VerifiedAddressSearch
 * @package backend\modules\audit\models\search
 */
class VerifiedAddressSearch extends VerifiedAddress
{
    public $use_confirm_address_filter;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['controller_id', 'date_range', 'country_id', 'partner_id', 'id', 'foreign_id', 'confirm_address'], 'safe']
        ];
    }

    public function search($params = [])
    {
        $this->use_confirm_address_filter = true;

        $sort = [];

        if (empty($params['sort'])) {
            $sort[VerifiedAddress::tableName() . '.created_at'] = SORT_DESC;
        }

        $query = VerifiedAddress::find()
            ->select([
                'order.*',
                'username' => User::tableName() . '.username'
            ])
            ->leftJoin(OrderLog::tableName(), OrderLog::tableName() . '.order_id=' . AddressVerification::tableName() . '.id')
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . OrderLog::tableName() . '.user_id')
            ->joinWith(['partner', 'country'])
            ->andWhere([OrderLog::tableName() . '.field' => 'confirm_address'])
            ->andWhere([OrderLog::tableName() . '.new' => self::addressVerificationState()])
            ->andWhere([self::tableName() . '.confirm_address' => self::addressVerificationState()])
            ->groupBy([User::tableName() . '.username', Order::tableName() . '.id', 'order_id', 'user_id'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params)) {
            return $dataProvider;
        }

        $this->applyFilters($query);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function agregatedSearh($params = [])
    {
        $this->use_confirm_address_filter = false;

        $select = [
            'total' => new Expression('count(confirm_address in (' . implode(',', AddressVerification::addressVerificationState()) . '))'),
            'cell_need_confirm' => new Expression('count(confirm_address in (' . AddressVerification::ADDRESS_NEED_CONFIRM . ') or null)'),
            'cell_confirmed' => new Expression('count(confirm_address in (' . AddressVerification::ADDRESS_CONFIRMED . ') or null)'),
            'cell_not_confirmed' => new Expression('count(confirm_address in (' . AddressVerification::ADDRESS_NOT_CONFIRMED . ') or null)')
        ];

        $query = AddressVerification::find()
            ->select($select)
            ->leftJoin(OrderLog::tableName(), OrderLog::tableName() . '.order_id=' . AddressVerification::tableName() . '.id')
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . OrderLog::tableName() . '.user_id')
            ->joinWith(['partner', 'country'])
            ->andWhere([OrderLog::tableName() . '.field' => 'confirm_address'])
            ->andWhere([OrderLog::tableName() . '.new' => self::addressVerificationState()])
            ->andWhere([self::tableName() . '.confirm_address' => self::addressVerificationState()]);


        $config = [
            'query' => $query
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params)) {
            return $dataProvider;
        }

        $this->applyFilters($query);

        return $dataProvider;
    }

    /**
     * @param Query $query
     */
    public function applyFilters($query)
    {
        if ($this->controller_id) {
            $query->andWhere([OrderLog::tableName() . '.user_id' => $this->controller_id]);
        }

        if ($this->country_id) {
            $query->andWhere([Order::tableName() . '.country_id' => $this->country_id]);
        }

        if ($this->partner_id) {
            $query->andWhere([Order::tableName() . '.partner_id' => $this->partner_id]);
        }

        if ($this->id) {
            $query->andWhere([Order::tableName() . '.id' => $this->id]);
        }

        if ($this->foreign_id) {
            $query->andWhere([Order::tableName() . '.foreign_id' => $this->foreign_id]);
        }

        if ($this->confirm_address && $this->use_confirm_address_filter) {
            $query->andWhere([Order::tableName() . '.confirm_address' => $this->confirm_address]);
        }

        if (empty($this->date_range)) {
            $this->date_range = date('d/m/Y 00:00:00') . ' - ' . date('d/m/Y 23:59:59');
        }

        if ($this->date_range) {

            $dateCreatedRange = Yii::$app->formatter->asConvertToFrom($this->date_range);

            /** @var object $dateCreatedRange */
            $dateCreatedRange->from = is_object($dateCreatedRange->from) ? $dateCreatedRange->from->format('Y-m-d H:i:00') : $dateCreatedRange->from;
            $dateCreatedRange->to = is_object($dateCreatedRange->to) ? $dateCreatedRange->to->format('Y-m-d H:i:59') : $dateCreatedRange->to;

            $date_from = new \DateTime($dateCreatedRange->from, new \DateTimeZone(yii::$app->user->timezone->timezone_id));
            $from = $date_from->getTimestamp();

            $date_to = new \DateTime($dateCreatedRange->to, new \DateTimeZone(yii::$app->user->timezone->timezone_id));
            $to = $date_to->getTimestamp();


            $query->andWhere(['between', OrderLog::tableName() . '.created_at', $from, $to]);
        }
    }

}
<?php

namespace backend\modules\audit\models\search;

use backend\modules\audit\models\AggregateWord;
use backend\modules\audit\models\AggregateWordBase;
use backend\modules\audit\models\TranscribeExceptionSet;
use backend\modules\audit\models\TranscribeSetting;
use backend\modules\audit\models\TranscribeStopSet;
use common\models\User;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProductLog;
use Yii;
use yii\base\Object;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class AggregateWordSearch
 * @package backend\modules\audit\models\search
 */
class AggregateWordSearch extends AggregateWord
{
    const IGNORE_DATE_TYPE = 'ignore';
    const UPDATE_DATE_TYPE = 'updated_at';
    const PARTNER_CREATED_AT_DATE_TYPE = 'partner_created_at';

    /** @var  string */
    public $type_date;
    /** @var  string */
    public $date_range;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['date_range', 'date_type', 'product_id', 'user_id', 'country_id', 'withTranscribeStopSets', 'word', 'group', 'operator'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $labels = [
            'country_id' => yii::t('common', 'Страна'),
            'partner_id' => yii::t('common', 'Партнёр'),
            'product_id' => yii::t('common', 'Товар'),
            'order_id' => yii::t('common', 'Заказ'),
            'user_id' => yii::t('common', 'Оператор'),
            'date_type' => yii::t('common', 'Тип даты'),
            'date_range' => yii::t('common', 'Период'),
            'word' => yii::t('common', 'Слово'),
            'group' => yii::t('common', 'Группировка'),
            'operator' => yii::t('common', 'Оператор'),
        ];

        return array_merge($labels, parent::attributeLabels());
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];
        $select = array_merge($this->getStatusColumns());

        if (empty($params['sort'])) {
            $sort['total'] = SORT_DESC;
        }

        $query = AggregateWord::find()
            ->joinWith(['country', 'product', 'wordBase', 'user'])
            ->leftJoin(TranscribeSetting::tableName(), TranscribeSetting::tableName() . '.country_id=' . AggregateWord::tableName() . '.country_id')
            ->leftJoin(TranscribeExceptionSet::tableName(), TranscribeExceptionSet::tableName() . '.transcribe_setting_id=' . TranscribeSetting::tableName() . '.id')
            ->where(['word_id' => ArrayHelper::getColumn(AggregateWordBase::find()->orderBy(['total' => SORT_DESC])->all(), 'id')]);

        $defaultGroup = [
            AggregateWord::tableName() . '.word_id'
        ];

        $this->load($params);

        switch ($this->group) {
            case self::GROUP_WORD:
                $group = [AggregateWord::tableName() . '.word_id'];
                $selectFields = [
                    'word_id',
                    'total' => new Expression("SUM(aggregate_word.total)")
                ];
                break;
            case self::GROUP_COUNTRY:
                $group = [AggregateWord::tableName() . '.country_id', AggregateWord::tableName() . '.word_id'];
                $selectFields = [
                    'word_id',
                    'country_id' => AggregateWord::tableName() . '.country_id',
                    'total' => new Expression("SUM(aggregate_word.total)")
                ];
                break;
            case self::GROUP_PRODUCT:
                $group = [AggregateWord::tableName() . '.product_id', AggregateWord::tableName() . '.word_id'];
                $selectFields = [
                    'word_id',
                    'product_id',
                    'total' => new Expression("SUM(aggregate_word.total)")
                ];
                break;
            case self::GROUP_USER:
                $group = [AggregateWord::tableName() . '.word_id', 'operator'];
                $selectFields = [
                    'word_id',
                    'operator' => new Expression(User::tableName() . '.username'),
                    'total' => new Expression("SUM(aggregate_word.total)")
                ];
                $sort = [
                    'user.username' => SORT_ASC
                ];
                break;
            case self::GROUP_ONE_CALL:
                $group = [AggregateWord::tableName() . '.word_id', 'call_history_id'];
                $selectFields = [
                    'word_id',
                    'total' => new Expression("SUM(aggregate_word.total)")
                ];
                break;
            case self::GROUP_ALL_CALLS:
                $group = [AggregateWord::tableName() . '.word_id', 'order_id'];
                $selectFields = [
                    'word_id',
                    'total' => new Expression("SUM(aggregate_word.total)")
                ];
                break;
            default:
                $group = $defaultGroup;
                $selectFields = [
                    'word_id',
                    'total' => new Expression("SUM(aggregate_word.total)")
                ];
                break;
        }

        $select = array_merge($select, $selectFields);

        $query->select($select);
        $query->groupBy($group);
        $query->orderBy($sort);

        $config = [
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ];

        $dataProvider = new ActiveDataProvider($config);

        if ($this->country_id) {
            $query->andWhere([AggregateWord::tableName() . '.country_id' => $this->country_id]);
        }

        if ($this->withTranscribeStopSets == AggregateWord::SEARCH_WITH_STOP) {
            $query->leftJoin(TranscribeStopSet::tableName(), TranscribeStopSet::tableName() . '.transcribe_setting_id=' . TranscribeSetting::tableName() . '.id');
            $query->andWhere([TranscribeStopSet::tableName() . '.text' => new Expression(AggregateWordBase::tableName() . '.word')]);
            $query->andWhere([TranscribeStopSet::tableName() . '.status' => new Expression(AggregateWord::tableName() . '.status')]);
        }

        if ($this->date_type) {
            if ($this->date_type != self::IGNORE_DATE_TYPE) {
                if (empty($this->date_range)) {
                    $this->date_range = date('d/m/Y 00:00') . ' - ' . date('d/m/Y 23:59');
                }
                /** @var Object $date */
                $date = Yii::$app->formatter->asConvertToFrom($this->date_range);

                $query->leftJoin(Order::tableName(), Order::tableName() . '.id=' . AggregateWord::tableName() . '.order_id');
                $query->andWhere(['between', Order::tableName() . '.' . $this->date_type, $date->from->getTimestamp(), $date->to->getTimestamp()]);
            }
        }

        if ($this->word) {
            $query->andWhere(['=', AggregateWordBase::tableName() . '.word', $this->word]);
        }

        if ($this->operator != '') {
            $query->andWhere(['user_id' => $this->operator]);
        }

        return $dataProvider;
    }
}
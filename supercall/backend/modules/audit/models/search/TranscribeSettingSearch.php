<?php

namespace backend\modules\audit\models\search;

use backend\modules\audit\models\TranscribeSetting;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class TranscribeSettingSearch
 * @package backend\modules\audit\models\search
 */
class TranscribeSettingSearch extends TranscribeSetting
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id', 'active'], 'safe']
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['country.name'] = SORT_ASC;
        }

        $query = TranscribeSetting::find()
            ->joinWith('transcribeStatusSets')
            ->joinWith('transcribeStopSets')
            ->joinWith('transcribeExceptionSets')
            ->joinWith('country')
            ->where(['country_id' => ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id')])
            ->orderBy($sort);

        $config = [
            'query' => $query,
            'pagination' => false,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params)) {
            return $dataProvider;
        }

        if ($this->country_id) {
            $query->andWhere(['country_id' => $this->country_id]);
        }

        if ($this->active) {
            $query->andWhere([TranscribeSetting::tableName() . '.active' => $this->active == 'true']);
        }

        return $dataProvider;
    }
}
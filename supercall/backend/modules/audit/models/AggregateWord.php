<?php

namespace backend\modules\audit\models;

use common\components\db\ActiveRecord;
use common\components\grid\NumberColumn;
use common\components\grid\RatioColumn;
use common\models\Country;
use common\models\Product;
use common\models\User;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "aggregate_word".
 *
 * @property integer $id
 * @property string $word
 * @property integer $call_history_id
 * @property integer $order_id
 * @property integer $status
 * @property integer $country_id
 * @property integer $user_id
 * @property integer $product_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AggregateWordBase $word
 * @property CallHistory $callHistory
 * @property Country $country
 * @property Order $order
 * @property User $user
 */

/**
 * Class AggregateWord
 * @package backend\modules\audit\models
 */
class AggregateWord extends ActiveRecord
{
    const GROUP_DEFAULT = 'default';
    const GROUP_WORD = 'word';
    const GROUP_COUNTRY = 'country_id';
    const GROUP_PRODUCT = 'product_id';
    const GROUP_USER = 'user_id';
    const GROUP_ONE_CALL = 'one_call';
    const GROUP_ALL_CALLS = 'all_calls';

    const PASSIVE_STAT_LIMIT = 5;

    const DATE_TYPE_UPDATED = 'updated_at';
    const DATE_TYPE_CREATED_BY_PARTNER = 'created_by_partner';

    const SEARCH_WITH_STOP = 1;
    const SEARCH_ALL = 0;

    /** @var  string */
    public $group;

    /** @var  integer */
    public $quantity_2;
    /** @var  integer */
    public $quantity_3;
    /** @var  integer */
    public $quantity_4;
    /** @var  integer */
    public $quantity_5;
    /** @var  integer */
    public $quantity_6;
    /** @var  integer */
    public $quantity_7;
    /** @var  integer */
    public $quantity_8;

    /** @var  boolean */
    public $withTranscribeStopSets;

    /** @var  string */
    public $date_type;

    /** @var  string */
    public $word;

    /** @var  string */
    public $operator;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%aggregate_word}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['call_history_id', 'order_id', 'status', 'country_id', 'word_id'], 'required'],
            [['call_history_id', 'order_id', 'status', 'country_id', 'user_id', 'created_at', 'updated_at', 'total', 'word_id'], 'integer'],
            [['call_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => CallHistory::className(), 'targetAttribute' => ['call_history_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'word_id' => Yii::t('common', 'Слово'),
            'call_history_id' => Yii::t('common', 'Call History ID'),
            'order_id' => Yii::t('common', 'Заказ'),
            'status' => Yii::t('common', 'Статус'),
            'country_id' => Yii::t('common', 'Страна'),
            'user_id' => Yii::t('common', 'Оператор'),
            'product_id' => Yii::t('common', 'Товар'),
            'total' => Yii::t('common', 'Всего'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return array
     */
    public static function getGroupList()
    {
        return [
            self::GROUP_DEFAULT => yii::t('common', 'По умолчанию'),
            self::GROUP_WORD => yii::t('common', 'По слову'),
            self::GROUP_COUNTRY => yii::t('common', 'Страна'),
            self::GROUP_PRODUCT => yii::t('common', 'Товар'),
            self::GROUP_USER => yii::t('common', 'Оператор'),
            self::GROUP_ONE_CALL => yii::t('common', 'По звонку'),
            self::GROUP_ALL_CALLS => yii::t('common', 'По всем звонкам заказа'),
        ];
    }

    /**
     * @return array
     */
    public static function getDateTypeList()
    {
        return [
            self::DATE_TYPE_UPDATED => yii::t('common', 'Дата обновления'),
            self::DATE_TYPE_CREATED_BY_PARTNER => yii::t('common', 'Дата создания у партнёра'),
        ];
    }

    /**
     * @param bool $forGrid
     * @return array
     */
    public function getStatusColumns($forGrid = false)
    {
        $columns = [];
        $statusCollection = Order::getFinalStatusesCollection();

        foreach (array_keys($statusCollection) as $status) {
            if (in_array($status, [Order::STATUS_NEW])) {
                continue;
            }
            if (!$forGrid) {
                $columns['quantity_' . $status] = new Expression("SUM(CASE WHEN " . AggregateWord::tableName() . ".status = {$status} THEN aggregate_word.total ELSE NULL END)");
            } else {

                $attribute = 'quantity_' . $status;
                $statusName = isset($statusCollection[$status]) ? $statusCollection[$status] : '-';
                $columns[] = [
                    'class' => NumberColumn::className(),
                    'attribute' => $attribute,
                    'label' => yii::t('common', $statusName),
                    'headerOptions' => ['class' => 'text-center'],
                    'contentOptions' => ['class' => 'text-center text-success'],
                    'content' => function ($model) use ($attribute) {
                        return ($model->{$attribute} ? $model->{$attribute} : 0);
                    },
                    'enableSorting' => true,
                ];

                $columns[] = [
                    'class' => RatioColumn::className(),
                    'attribute' => $attribute . '_percent',
                    'label' => '%',
                    'headerOptions' => [
                        'data-sortable' => 'true',
                        'data-field' => $attribute . '_percent',
                        'class' => 'text-center text-danger'
                    ],
                    'contentOptions' => ['class' => 'text-center text-danger'],
                    'content' => function ($model) use ($attribute) {
                        return $model->total > 0 ? round($model->{$attribute} / $model->total * 100, 2) : 0;
                    },
                    'enableSorting' => true,
                ];
            }
        }

        return $columns;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        $list_columns = [
            'word_id' => [
                'attribute' => 'word_id',
                'content' => function ($model) {
                    return yii::t('common', $model->wordBase->word);
                },
                'enableSorting' => true,
            ],
            'country_id' => [
                'attribute' => 'country_id',
                'content' => function ($model) {
                    return yii::t('common', $model->country->name);
                },
                'enableSorting' => true,
            ],
            'product_id' => [
                'attribute' => 'product_id',
                'content' => function ($model) {
                    return yii::t('common', $model->product->name);
                },
                'enableSorting' => true,
            ],
            'operator' => [
                'attribute' => 'operator',
                'content' => function ($model) {
                    return yii::t('common', $model->operator);
                },
                'enableSorting' => true,
            ],
            'total' => [
                'attribute' => 'total',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return $model->total;
                },
                'enableSorting' => true,
            ],
        ];

        switch ($this->group) {
            case self::GROUP_COUNTRY:
                $static_columns = [
                    $list_columns['word_id'],
                    $list_columns['country_id'],
                    $list_columns['total']
                ];
                break;
            case self::GROUP_PRODUCT:
                $static_columns = [
                    $list_columns['word_id'],
                    $list_columns['product_id'],
                    $list_columns['total']
                ];
                break;
            case self::GROUP_USER:
                $static_columns = [
                    $list_columns['word_id'],
                    $list_columns['operator'],
                    $list_columns['total']
                ];
                break;
            case self::GROUP_WORD:
            case self::GROUP_ONE_CALL:
            case self::GROUP_ALL_CALLS:
                $static_columns = [
                    $list_columns['word_id'],
                    $list_columns['total']
                ];
                break;
            default:
                $static_columns = [
                    $list_columns['word_id'],
                    $list_columns['total']
                ];
                break;
        }

        return array_merge($static_columns, $this->getStatusColumns(true));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallHistory()
    {
        return $this->hasOne(CallHistory::className(), ['id' => 'call_history_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWordBase()
    {
        return $this->hasOne(AggregateWordBase::className(), ['id' => 'word_id']);
    }

}
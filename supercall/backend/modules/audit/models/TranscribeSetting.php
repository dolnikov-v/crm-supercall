<?php

namespace backend\modules\audit\models;

use backend\modules\audit\models\query\TranscribeSettingQuery;
use common\models\Country;
use \yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "transcribe_setting".
 *
 * @property integer $id
 * @property boolean $active
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TranscribeExceptionSet[] $transcribeExceptionSets
 * @property TranscribeStatusSet[] $transcribeStatusSets
 * @property TranscribeStopSet[] $transcribeStopSets
 */

/**
 * Class TranscribeSetting
 * @package backend\modules\audti\models
 */
class TranscribeSetting extends ActiveRecord
{
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    public static function tableName()
    {
        return '{{%transcribe_setting}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id'], 'required'],
            [['country_id', 'created_at', 'updated_at'], 'integer'],
            [['active'], 'boolean'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'country_id' => Yii::t('common', 'Страна'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranscribeStatusSets()
    {
        return $this->hasMany(TranscribeStatusSet::className(), ['transcribe_setting_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranscribeStopSets()
    {
        return $this->hasMany(TranscribeStopSet::className(), ['transcribe_setting_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranscribeExceptionSets()
    {
        return $this->hasMany(TranscribeExceptionSet::className(), ['transcribe_setting_id' => 'id']);
    }

    /**
     * @return TranscribeSettingQuery
     */
    public static function find()
    {
        return new TranscribeSettingQuery(get_called_class());
    }
}
<?php

namespace backend\modules\audit\models;

use backend\modules\audit\models\query\TranscribeExceptionSetQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "transcribe_exception_set".
 *
 * @property integer $id
 * @property integer $transcribe_setting_id
 * @property string $text
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TranscribeSetting $transcribeSetting
 * @property TranscribeSetting[] $transcribeSettings
 */

/**
 * Class TranscribeExceptionSet
 * @package backend\modules\audit\models
 */
class TranscribeExceptionSet extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%transcribe_exception_set}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['transcribe_setting_id'], 'required'],
            [['transcribe_setting_id', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string', 'max' => 255],
            [['transcribe_setting_id'], 'exist', 'skipOnError' => true, 'targetClass' => TranscribeSetting::className(), 'targetAttribute' => ['transcribe_setting_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'transcribe_setting_id' => Yii::t('common', 'Transcribe Setting ID'),
            'text' => Yii::t('common', 'Слова "Исключения"'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranscribeSetting()
    {
        return $this->hasOne(TranscribeSetting::className(), ['id' => 'transcribe_setting_id']);
    }

    /**
     * @return query\TranscribeExceptionSetQuery
     */
    public static function find()
    {
        return new TranscribeExceptionSetQuery(get_called_class());
    }
}
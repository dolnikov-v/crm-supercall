<?php

namespace backend\modules\audit\models;

use backend\modules\audit\models\query\TranscribeStopSetQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Transaction;

/**
 * This is the model class for table "transcribe_stop_set".
 *
 * @property integer $id
 * @property integer $transcribe_setting_id
 * @property integer $status
 * @property string $text
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TranscribeSetting[] $transcribeSettings
 * @property TranscribeSetting $transcribeSetting
 */

/**
 * Class TranscribeStopSet
 * @package backend\modules\audit\models
 */
class TranscribeStopSet extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%transcribe_stop_set}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['transcribe_setting_id', 'status'], 'required'],
            [['transcribe_setting_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string', 'max' => 255],
            [['transcribe_setting_id'], 'exist', 'skipOnError' => true, 'targetClass' => TranscribeSetting::className(), 'targetAttribute' => ['transcribe_setting_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'transcribe_setting_id' => Yii::t('common', 'Transcribe Setting ID'),
            'text' => Yii::t('common', '"Стоп"  слова'),
            'status' => Yii::t('common', 'Статус'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranscribeSetting()
    {
        return $this->hasOne(TranscribeSetting::className(), ['id' => 'transcribe_setting_id']);
    }

    /**
     * @return query\TranscribeStopSetQuery
     */
    public static function find()
    {
        return new TranscribeStopSetQuery(get_called_class());
    }
}
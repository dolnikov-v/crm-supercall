<?php

namespace backend\modules\audit\assets;

use yii\web\AssetBundle;

/**
 * Class VerifiedAddressAsset
 * @package backend\modules\audit\assets
 */
class VerifiedAddressAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/audit/';

    public $js = [
        'verified-address.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
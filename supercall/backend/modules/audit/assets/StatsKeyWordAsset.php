<?php

namespace backend\modules\audit\assets;

use yii\web\AssetBundle;

/**
 * Class StatsKeyWordAsset
 * @package backend\modules\audit\assets
 */
class StatsKeyWordAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/audit/stats-key-word/';

    public $css = [
        'stats-key-word.css',
    ];

    public $js = [
        'stats-key-word.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
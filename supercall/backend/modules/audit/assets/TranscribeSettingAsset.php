<?php

namespace backend\modules\audit\assets;

use yii\web\AssetBundle;

/**
 * Class TranscribeSettingAsset
 * @package backend\modules\audit\assets
 */
class TranscribeSettingAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/audit/';

    public $css = [
        'transcribe-setting.css',
    ];

    public $js = [
        'transcribe-setting.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
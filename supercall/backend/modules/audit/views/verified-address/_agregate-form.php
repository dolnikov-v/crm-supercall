<?php
use backend\modules\audit\models\VerifiedAddress;
use common\components\grid\GridView;
use common\components\grid\RatioTotalColumn;

/** @var \yii\data\ActiveDataProvider $agregatedDataProvider */
?>

<?=GridView::widget([
    'dataProvider' => $agregatedDataProvider,
    'columns' => [
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center width-50'],
            'content' => function ($model) {
                return yii::t('common', $model->total);
            },
            'label' => yii::t('common', 'Всего')
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center width-250'],
            'content' => function ($model) {
                return yii::t('common', $model->cell_need_confirm);
            },
            'label' => VerifiedAddress::getStateByCode(VerifiedAddress::ADDRESS_NEED_CONFIRM)
        ],
        [
            'class' => RatioTotalColumn::className(),
            'dataProviderWithoutPaginate' => $agregatedDataProvider,
            'attribute' => 'cell_need_confirm_percent',
            'label' => '%',
            'contentOptions' => ['class' => 'text-success'],
            'footerOptions' => ['class' => 'text-success'],
            'value' => function ($model) {
                $curr = (int)$model->cell_need_confirm;
                $total = (int)$model->total;
                return $total > 0 ? round($curr / $total * 100, 2) : 0;
            },
            'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'cell_need_confirm_percent'],
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center width-250'],
            'content' => function ($model) {
                return yii::t('common', $model->cell_confirmed);
            },
            'label' => VerifiedAddress::getStateByCode(VerifiedAddress::ADDRESS_CONFIRMED)
        ],
        [
            'class' => RatioTotalColumn::className(),
            'dataProviderWithoutPaginate' => $agregatedDataProvider,
            'attribute' => 'cell_confirmed_percent',
            'label' => '%',
            'contentOptions' => ['class' => 'text-success'],
            'footerOptions' => ['class' => 'text-success'],
            'value' => function ($model) {
                $curr = (int)$model->cell_confirmed;
                $total = (int)$model->total;
                return $total > 0 ? round($curr / $total * 100, 2) : 0;
            },
            'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'cell_confirmed_percent'],
        ],
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center width-250'],
            'content' => function ($model) {
                return yii::t('common', $model->cell_not_confirmed);
            },
            'label' => VerifiedAddress::getStateByCode(VerifiedAddress::ADDRESS_NOT_CONFIRMED)
        ],
        [
            'class' => RatioTotalColumn::className(),
            'dataProviderWithoutPaginate' => $agregatedDataProvider,
            'attribute' => 'cell_not_confirmed_percent',
            'label' => '%',
            'contentOptions' => ['class' => 'text-success'],
            'footerOptions' => ['class' => 'text-success'],
            'value' => function ($model) {
                $curr = (int)$model->cell_not_confirmed;
                $total = (int)$model->total;
                return $total > 0 ? round($curr / $total * 100, 2) : 0;
            },
            'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'cell_not_confirmed_percent'],
        ],
    ],
]);?>
<?php

use backend\modules\order\models\AddressVerification;
use common\models\Country;
use common\modules\partner\models\Partner;
use common\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var \backend\modules\audit\models\search\VerifiedAddressSearch $modelSearch */
/** @var array $controllers */
?>

<?php

$form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'controller_id')->select2List($controllers, ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'country_id')->select2List(Country::find()->collection('common'), ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'partner_id')->select2List(ArrayHelper::map(yii::$app->user->identity->partners, 'id', 'name'), ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'confirm_address')->select2List(AddressVerification::addressVerificationStateCollection(), [
                'prompt' => '—'
        ])->label(yii::t('common', 'Тип подтверждения')); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'id')->textInput(); ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'foreign_id')->textInput(); ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'date_range')->widget(DateRangePicker::className(), [
            'convertFormat' => false,
            'presetDropdown' => true,
            'hideInput' => true,
            'pluginOptions' => [
                'autoUpdateOnInit' => true,
                'autoUpdateInput' => true,
                'timePicker' => true,
                'timePicker24Hour' => true,
                'timePickerIncrement' => 1,
                'locale' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                ],
            ],
            'pluginEvents' => [
                'cancel.daterangepicker' => 'function(ev, picker) { $("input#orderreport-date_range").val(""); $(this).find("span.range-value").text("") }',
            ],
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php

use backend\modules\order\models\AddressVerification;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\LinkPager;
use common\widgets\base\Panel;
use backend\modules\audit\assets\VerifiedAddressAsset;

/** @var \backend\modules\audit\models\search\VerifiedAddressSearch $modelSearch */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var array $controllers */
/** @var \yii\data\ActiveDataProvider $agregatedDataProvider */

$this->title = Yii::t('common', 'Проверенные адреса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Аудит КЦ'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Проверенные адреса')];

VerifiedAddressAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
        'controllers' => $controllers
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Сводная статистика'),
    'contentPadding' => false,
    'content' => $this->render('_agregate-form', [
        'agregatedDataProvider' => $agregatedDataProvider
    ]),
    'footer' => '',
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с проверенными адресами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'country_id',
                'content' => function ($model) {
                    return yii::t('common', $model->country->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'foreign_id',
                'content' => function ($model) {
                    return yii::t('common', $model->foreign_id);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'confirm_address',
                'content' => function ($model) {
                    return AddressVerification::getStateByCode($model->confirm_address);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'controller_id',
                'content' => function ($model) {
                    return $model->confirm_address == AddressVerification::ADDRESS_CONFIRMED ? $model->username : '';
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'withRelativeTime' => true,
                'attribute' => 'updated_at',
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

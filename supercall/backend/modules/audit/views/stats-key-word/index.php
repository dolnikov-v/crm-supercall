<?php

use backend\modules\audit\models\AggregateWord;
use backend\modules\audit\models\TranscribeSetting;
use common\components\grid\GridView;
use common\helpers\grid\DataProvider;
use common\widgets\base\Panel;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;

$this->title = Yii::t('common', 'Статистика распознанных ключевых слов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Аудит'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика распознанных ключевых слов')];

/** @var \backend\modules\audit\models\search\AggregateWordSearch $modelSearch */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var array $settings */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статистика слов'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function($model) use ($settings){
            //Если настройки включены и есть слова исключения
            if(isset($settings[$model->country_id]) && $settings[$model->country_id]->active == TranscribeSetting::ACTIVE){
                return [
                    'class' => in_array($model->wordBase->word, ArrayHelper::getColumn($settings[$model->country_id]->transcribeExceptionSets, 'text')) ? 'hidden' : ''
                ];
            }

            return [];

        },
        'columns' => $modelSearch->getColumns(),
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

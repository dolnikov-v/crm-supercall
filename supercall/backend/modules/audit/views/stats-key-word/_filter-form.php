<?php

use backend\modules\audit\models\AggregateWord;
use common\models\Country;
use common\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use yii\helpers\Url;
use backend\modules\audit\assets\StatsKeyWordAsset;
use common\assets\base\ParseUrlAsset;
/** @var \backend\modules\audit\models\search\AggregateWordSearch $modelSearch */

StatsKeyWordAsset::register($this);
ParseUrlAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'country_id')->select2List(Country::find()->collection('common'), ['prompt' => '—']); ?>
    </div>

    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'word')->textInput();?>
    </div>

    <div class="col-lg-3 ">
        <?= $form->field($modelSearch, 'date_type')->select2List([
            $modelSearch::IGNORE_DATE_TYPE => yii::t('common', 'Игнорировать'),
            $modelSearch::UPDATE_DATE_TYPE => yii::t('common', 'Дата обновления'),
            $modelSearch::PARTNER_CREATED_AT_DATE_TYPE => yii::t('common', 'Дата создания у партнёра')
        ])->label(Yii::t('common', 'Тип даты')) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'date_range')->widget(DateRangePicker::className(), [
            'convertFormat' => false,
            'presetDropdown' => true,
            'hideInput' => true,
            'pluginOptions' => [
                'autoUpdateOnInit' => true,
                'autoUpdateInput' => true,
                'timePicker' => true,
                'timePicker24Hour' => true,
                'timePickerIncrement' => 1,
                'locale' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                ],
            ],
            'pluginEvents' => [
                'cancel.daterangepicker' => 'function(ev, picker) { $("input#orderreport-date_range").val(""); $(this).find("span.range-value").text("") }',
            ],
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'operator')->select2List([]); ?>
        <div class="loader operator-loader hidden"></div>
    </div>
    <div class="col-lg-3 ">
        <?= $form->field($modelSearch, 'group')->select2List($modelSearch::getGroupList()); ?>
    </div>
    <div class="col-lg-3 ">
        <?= $form->field($modelSearch, 'withTranscribeStopSets')->select2List([
            AggregateWord::SEARCH_ALL => yii::t('common', 'Все слова'),
            AggregateWord::SEARCH_WITH_STOP => yii::t('common', 'Только "Стоп" слова'),
        ])->label(Yii::t('common', 'Статистика по')) ?>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>


<?php ActiveForm::end(); ?>

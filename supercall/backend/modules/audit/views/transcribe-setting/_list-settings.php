<?php
/** @var array $models */

use backend\modules\audit\models\TranscribeSetting;
use common\widgets\ActiveForm;
use common\widgets\base\ButtonLink;
use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="container-settings">

        <?php if (empty($models)) : ?>

            <?= Yii::$app->controller->renderPartial('_edit-form', ['form' => $form, 'model' => new TranscribeSetting()]); ?>

        <?php else: ?>

            <?php foreach ($models as $model): ?>

                <?php /** @var TranscribeSetting $model */; ?>

                <?= Yii::$app->controller->renderPartial('_edit-form', ['form' => $form, 'model' => $model]); ?>

            <?php endforeach; ?>

            <?= Yii::$app->controller->renderPartial('_edit-form', ['form' => $form, 'model' => new TranscribeSetting()]); ?>

        <?php endif; ?>

    </div>

<?php ActiveForm::end(); ?>
    <br/>
    <br/>

<?= (Yii::$app->user->can('audit.transcribesetting.add') ? ButtonLink::widget([
    'label' => Yii::t('common', 'Добавить запись'),
    'url' => Url::toRoute('edit'),
    'style' => ButtonLink::STYLE_SUCCESS . ' transcribe-add',
    'size' => ButtonLink::SIZE_SMALL,
]) : ''); ?>
<?php
use common\widgets\base\Panel;
use backend\modules\audit\assets\TranscribeSettingAsset;

/** @var \backend\modules\audit\models\search\TranscribeSettingSearch $modelSearch */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Настройка транскрибации');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Аудит КЦ'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Настройка транскрибации')];

TranscribeSettingAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список настроек'),
    'content' => $this->render('_list-settings', [
        'models' => $dataProvider->getModels()
    ]),
]) ?>
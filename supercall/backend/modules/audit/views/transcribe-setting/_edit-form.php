<?php

use backend\modules\audit\models\TranscribeExceptionSet;
use backend\modules\audit\models\TranscribeSetting;
use backend\modules\audit\models\TranscribeStatusSet;
use backend\modules\audit\models\TranscribeStopSet;
use common\models\Country;
use common\modules\order\models\Order;
use common\widgets\base\Button;
use common\widgets\base\ButtonLink;
use common\widgets\base\GlyphIcon;
use yii\helpers\ArrayHelper;

/** @var TranscribeSetting $model */
/** @var common\widgets\ActiveForm $form */
/** @var array $usedCountries */
?>

<fieldset data-id="<?= $model->id; ?>"
          class="transcribe-setting-block <?= $model->active || is_null($model->active) ? 'not_muted_area' : 'muted_area'; ?>">
    <label class="country" data-text="<?= yii::t('common', 'Добавить') ?>">
        <?= $model->country ? yii::t('common', $model->country->name) : yii::t('common', 'Добавить'); ?>
    </label>
    <hr>
    <div class="">

        <div class="row block-setting">
            <div class="col-lg-2">
                <?= $form->field($model, 'country_id')->select2List(Country::find()->collection('common'), [
                    'prompt' => '-'
                ]); ?>
            </div>
        </div>

        <div class="stop-word-groups block-setting">

        <?php if($model->transcribeStopSets):?>

            <?php foreach ($model->transcribeStatusSets as $statusSet) : ?>

            <?php /** @var TranscribeStatusSet $statusSet */?>

            <div class="stop-word-group">
                <div class="row">
                    <?php $mark = uniqid()?>

                    <div class="col-lg-2">
                        <?= $form->field(new TranscribeStatusSet(), 'status')->select2List(Order::getFinalStatusesCollection(), [
                            'multiple' => false,
                            'value' => $statusSet->status,
                            'name' => 'status['.$mark.'][]'
                        ])->label(yii::t('common', 'Статус заказа')) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-11">
                        <?php $stops = $model->getTranscribeStopSets()->where(['status' => $statusSet->status])->all();?>

                        <?= $form->field(new TranscribeStopSet(), 'text')->select2List(ArrayHelper::map($stops, 'text', 'text'), [
                            'multiple' => true,
                            'value' => ArrayHelper::getColumn($stops, 'text'),
                            'tags' => true,
                            'name' => 'TranscribeStopSet['.$mark.']'
                        ])->label(yii::t('common', '"Стоп" слова')); ?>
                    </div>
                    <div class="col-lg-1 button-settings">
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_SUCCESS . ' add-stop-setting',
                                'icon' => GlyphIcon::PLUS,
                            ]) ?>
                        </div>
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_DANGER . ' remove-stop-setting',
                                'icon' => GlyphIcon::MINUS,
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <?php endforeach;?>

        <?php else: ?>

            <div class="stop-word-group">
                <div class="row">
                    <?php $mark = uniqid()?>

                    <div class="col-lg-2">
                        <?= $form->field(new TranscribeStatusSet(), 'status')->select2List(Order::getFinalStatusesCollection(), [
                            'multiple' => false,
                            'name' => 'status['.$mark.'][]'
                        ])->label(yii::t('common', 'Статус заказа')) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-11">
                        <?= $form->field(new TranscribeStopSet(), 'text')->select2List([], [
                            'multiple' => true,
                            'tags' => true,
                            'name' => 'TranscribeStopSet['.$mark.']'
                        ])->label(yii::t('common', '"Стоп" слова')); ?>
                    </div>
                    <div class="col-lg-1 button-settings">
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_SUCCESS . ' add-stop-setting',
                                'icon' => GlyphIcon::PLUS,
                            ]) ?>
                        </div>
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_DANGER . ' remove-stop-setting',
                                'icon' => GlyphIcon::MINUS,
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>

        <?php endif;?>

        </div>

        <div class="row block-setting">
            <div class="col-lg-12">
                <?= $form->field(new TranscribeExceptionSet(), 'text')->select2List(ArrayHelper::map($model->transcribeExceptionSets, 'text', 'text'), [
                    'multiple' => true,
                    'value' => ArrayHelper::getColumn($model->transcribeExceptionSets, 'text'),
                    'tags' => true
                ])->label(yii::t('common', 'Слова "Исключения"')); ?>
            </div>
        </div>



    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= (Yii::$app->user->can('audit.transcribesetting.edit') ? ButtonLink::widget([
                'label' => Yii::t('common', 'Сохранить'),
                'url' => '#',
                'style' => ButtonLink::STYLE_INFO . ' transcribe-save',
                'size' => ButtonLink::SIZE_SMALL,
            ]) : ''); ?>

            <?= (Yii::$app->user->can('audit.transcribesetting.manage') ? ButtonLink::widget([
                'label' => Yii::t('common', $model->active || is_null($model->active) ? 'Отключить' : 'Включить'),
                'url' => '#',
                'style' => ($model->active || is_null($model->active) ? ButtonLink::STYLE_DEFAULT : ButtonLink::STYLE_SUCCESS) . ' transcribe-manage' . ($model->id ? '' : ' hidden'),
                'size' => ButtonLink::SIZE_SMALL,
                'attributes' => [
                    'data-active' => is_null($model->active) ? TranscribeSetting::ACTIVE : $model->active,
                    'data-text-active' => Yii::t('common', 'Включить'),
                    'data-text-deactive' => Yii::t('common', 'Отключить')
                ]
            ]) : ''); ?>

            <?= (Yii::$app->user->can('audit.transcribesetting.delete') ? ButtonLink::widget([
                'label' => Yii::t('common', 'Удалить'),
                'url' => '#',
                'style' => ButtonLink::STYLE_DANGER . ' transcribe-delete' . ($model->id ? '' : ' hidden'),
                'size' => ButtonLink::SIZE_SMALL,
            ]) : ''); ?>

        </div>
    </div>
</fieldset>

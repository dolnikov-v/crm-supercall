<?php

use common\models\Country;
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \backend\modules\audit\models\search\TranscribeSettingSearch $modelSearch */
?>


<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-9">
        <?= $form->field($modelSearch, 'country_id')->select2List(Country::find()->collection('common'), [
            'multiple' => true
        ]); ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'active')->select2List([
            'true' => yii::t('common', 'Активные'),
            'false' => yii::t('common', 'Не активные'),
        ], [
            'prompt' => '-'
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php

namespace backend\modules\audit\controllers;

use api\components\filters\VerbFilter;
use backend\components\web\Controller;
use backend\modules\audit\models\search\TranscribeSettingSearch;
use backend\modules\audit\models\TranscribeExceptionSet;
use backend\modules\audit\models\TranscribeSetting;
use backend\modules\audit\models\TranscribeStatusSet;
use backend\modules\audit\models\TranscribeStopSet;
use common\modules\order\models\Order;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class TranscribeSettingController
 * @package backend\modules\audit\controllers
 */
class TranscribeSettingController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'edit' => ['post'],
                    'manage' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new TranscribeSettingSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return string
     */
    public function actionManage()
    {
        $id = yii::$app->request->post('id');

        /** @var TranscribeSetting $model */
        $model = TranscribeSetting::find()->byId($id)->one();

        if (!$model) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Настройка с идентификатором №{id} не найдены', ['id' => $id]),
                'id' => $id
            ];
        } else {
            $model->active = !$model->active;

            if (!$model->save(false, ['active'])) {
                $result = [
                    'success' => false,
                    'message' => Json::encode($model->getErrors()),
                    'id' => $model->id
                ];
            } else {
                $result = [
                    'success' => true,
                    'message' => yii::t('common', 'Настройка {action}', ['action' => yii::t('common', $model->active ? 'включены' : 'выключены')]),
                    'id' => $model->id
                ];
            }
        }

        return Json::encode($result);
    }

    /**
     * @return string
     */
    public function actionDelete()
    {
        $id = yii::$app->request->post('id');

        /** @var TranscribeSetting $model */
        $model = TranscribeSetting::find()->byId($id)->one();

        if (!$model) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Настройка с идентификатором №{id} не найдена', ['id' => $id]),
                'id' => $id
            ];
        } else {
            if (!$model->delete()) {
                $result = [
                    'success' => false,
                    'message' => Json::encode($model->getErrors()),
                    'id' => $model->id
                ];
            } else {
                $result = [
                    'success' => true,
                    'message' => yii::t('common', 'Настройка удалена'),
                    'id' => $model->id
                ];
            }
        }

        return Json::encode($result);
    }

    /**
     * @param $statuses
     * @param $stops
     * @return array
     */
    public function checkPairStatusStops($statuses, $stops)
    {
        $statusCollection = Order::getFinalStatusesCollection();

        $data_statuses = array_count_values($statuses);

        foreach ($data_statuses as $status => $count) {
            if (!isset($statusCollection[$status])) {
                return [
                    'success' => false,
                    'message' => yii::t('common', 'Указан неизвестный статус: {status}', ['status' => $status])
                ];
            }

            if ($count > 1) {
                return [
                    'success' => false,
                    'message' => yii::t('common', 'Для статуса: {status} "Стоп"-слова уже укзаны', ['status' => $statusCollection[$status]])
                ];
            }
        }

        $pairs = [];

        foreach ($statuses as $mark => $status) {
            if (!isset($stops[$mark]) || empty($stops[$mark])) {
                return [
                    'success' => false,
                    'message' => yii::t('common', 'Для статуса: {status} не указаны "Стоп"-слова', ['status' => $statusCollection[$status]])
                ];
            } else {
                if (!isset($pairs[$status])) {
                    $pairs[$status] = $stops[$mark];
                }

            }
        }

        return [
            'success' => true,
            'message' => $pairs
        ];
    }

    /**
     * @return string
     */
    public function actionEdit()
    {
        $id = yii::$app->request->post('id');
        $country_id = yii::$app->request->post('country_id');
        $statuses = yii::$app->request->post('statuses');
        $stops = yii::$app->request->post('stops');
        $exceptions = yii::$app->request->post('exceptions');
        $error = null;

        $checkPairStatusStops = $this->checkPairStatusStops($statuses, $stops);

        if (!$checkPairStatusStops['success']) {
            $result = $checkPairStatusStops;
        } else {
            $result = [
                'success' => true,
                'message' => yii::t('common', 'Настройка успешно сохранена'),
                'id' => $id
            ];

            if (!$country_id) {
                $error = yii::t('common', 'Необходимо указать страну');
            } elseif (empty($statuses)) {
                $error = yii::t('common', 'Необходимо указать статусы заказа');
            } elseif (!empty($res = array_diff($statuses, array_keys(Order::getFinalStatusesCollection())))) {
                $error = yii::t('common', 'Неизвестные статусы заказа: {status}', ['status' => implode(',', $res)]);
            } elseif (empty($stops)) {
                $error = yii::t('common', 'Необходимо указать "Стоп" слова');
            }

            if ($error) {
                $result = [
                    'success' => false,
                    'message' => $error,
                    'id' => $id
                ];
            } else {

                $pairsStatusStops = $checkPairStatusStops['message'];

                /** @var TranscribeSetting $setting */
                $setting = TranscribeSetting::find()->byCountry($country_id)->one();

                if ($setting) {
                    if (!$id) {
                        $error = yii::t('common', 'Для страны "{country}" уже существует настройка транскрибации', ['country' => yii::t('common', $setting->country->name)]);
                    }
                }

                $transaction = yii::$app->db->beginTransaction();

                if ($id) {
                    $model = TranscribeSetting::find()->byId($id)->one();
                    $model->country_id = $country_id;
                    $model->updated_at = time();
                } else {
                    $model = new TranscribeSetting();
                    $model->country_id = $country_id;
                    $model->created_at = time();
                }

                if (!$model->save()) {
                    $result = [
                        'success' => false,
                        'message' => Json::encode($model->getErrors()),
                        'id' => $model->id
                    ];
                } else {
                    $dataStatusInsert = [];
                    $dataStopsInsert = [];

                    foreach ($pairsStatusStops as $status => $stops) {
                        $stops = array_unique($stops);

                        $dataStatusInsert[] = [
                            'transcribe_setting_id' => $model->id,
                            'status' => $status,
                            'created_at' => time()
                        ];

                        foreach ($stops as $stop) {
                            $dataStopsInsert[] = [
                                'transcribe_setting_id' => $model->id,
                                'text' => $stop,
                                'status' => $status,
                                'created_at' => time()
                            ];
                        }
                    }

                    TranscribeStatusSet::deleteAll(['transcribe_setting_id' => $model->id]);

                    yii::$app->db->createCommand()->batchInsert(
                        TranscribeStatusSet::tableName(),
                        [
                            'transcribe_setting_id',
                            'status',
                            'created_at'
                        ],
                        $dataStatusInsert
                    )->execute();

                    TranscribeStopSet::deleteAll(['transcribe_setting_id' => $model->id]);

                    yii::$app->db->createCommand()->batchInsert(
                        TranscribeStopSet::tableName(),
                        [
                            'transcribe_setting_id',
                            'text',
                            'status',
                            'created_at'
                        ],
                        $dataStopsInsert
                    )->execute();

                    TranscribeExceptionSet::deleteAll(['transcribe_setting_id' => $model->id]);

                    if ($exceptions) {
                        $dataExceptions = [];

                        foreach ($exceptions as $exception) {
                            $dataExceptions[] = [
                                'transcribe_setting_id' => $model->id,
                                'text' => $exception,
                                'created_at' => time()
                            ];
                        }

                        yii::$app->db->createCommand()->batchInsert(TranscribeExceptionSet::tableName(), [
                            'transcribe_setting_id',
                            'text',
                            'created_at'
                        ], $dataExceptions)->execute();
                    }
                }


                if ($result['success']) {
                    $result['id'] = isset($model) ? $model->id : $id;
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            }
        }

        return Json::encode($result);

    }
}
<?php

namespace backend\modules\audit\controllers;

use backend\components\web\Controller;
use backend\modules\audit\models\search\VerifiedAddressSearch;
use common\components\base\AjaxFilter;
use common\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class VerifiedAddressController
 * @package backend\modules\audit\controllers
 */
class VerifiedAddressController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['get-controllers'],
            ]
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = ($action->id !== "get-controllers");
        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new VerifiedAddressSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
        $controllers = $this->getControllers(yii::$app->user->getCountries());

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'agregatedDataProvider' => $this->getAgregateData(),
            'controllers' => $controllers
        ]);
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function getAgregateData()
    {
        $modelSearch = new VerifiedAddressSearch();
        return $modelSearch->agregatedSearh(Yii::$app->request->queryParams);
    }

    /**
     * @param $country_ids
     * @return array
     */
    public function getControllers($country_ids)
    {
        return ArrayHelper::map(User::getActiveByRole($country_ids, User::ROLE_CONTROLLER), 'id', 'username');
    }

    /**
     * @return array
     */
    public function actionGetControllers()
    {
        $country_id = yii::$app->request->post('country_id');

        if (!$country_id) {
            return $this->getControllers(yii::$app->user->getCountries());
        } else {
            return $this->getControllers([['id' => $country_id]]);
        }
    }
}
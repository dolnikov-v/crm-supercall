<?php

namespace backend\modules\audit\controllers;

use api\components\filters\VerbFilter;
use backend\components\web\Controller;
use backend\modules\access\models\AuthAssignment;
use backend\modules\audit\models\search\AggregateWordSearch;
use backend\modules\audit\models\TranscribeSetting;
use common\models\User;
use common\models\UserCountry;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class StatsKeyWordController
 * @package backend\modules\audit\controllers
 */
class StatsKeyWordController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-operators' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if ($action->id == 'get-operators') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new AggregateWordSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $settings = TranscribeSetting::find()->all();

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'settings' => ArrayHelper::index($settings, 'country_id')
        ]);
    }

    /**
     * @return string
     */
    public function actionGetOperators()
    {
        $country_id = yii::$app->request->post('country_id');

        if(!$country_id){
            $country_id = ArrayHelper::getColumn(yii::$app->user->getCountries(), 'country_id');
        }

        $operators = User::find()
            ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id::integer=' . User::tableName() . '.id')
            ->leftJoin(UserCountry::tableName(), UserCountry::tableName() . '.user_id=' . User::tableName() . '.id')
            ->where([AuthAssignment::tableName() . '.item_name' => [
                User::ROLE_OPERATOR,
                User::ROLE_SENIOR_OPERATOR,
                User::ROLE_APPLICANT]
            ])
            ->andWhere(['country_id' => $country_id])
        ->all();

        return Json::encode(ArrayHelper::map($operators, 'id', 'username'));
    }
}
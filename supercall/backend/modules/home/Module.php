<?php
namespace backend\modules\home;

use backend\components\base\Module as BackendModule;
use Yii;

/**
 * Class Module
 * @package backend\modules\home
 */
class Module extends BackendModule
{
    public $controllerNamespace = 'backend\modules\home\controllers';

}

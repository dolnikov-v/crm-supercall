<?php

use backend\modules\widget\widgets\GridStack;

/** @var \yii\web\View $this */
/** @var \backend\modules\widget\models\WidgetUser[] $widgets */
/** @var \backend\modules\widget\models\WidgetType[] $widgetTypes */

$this->title = Yii::t('common', 'Домашняя страница');
$this->params['showPanelHeader'] = false;
?>

<div class="row">
    <div class="col-lg-12 text-center m-t-md">
        <h2>
            <?= Yii::t('common', 'Добро пожаловать в {name}', [
                'name' => Yii::$app->name,
            ]) ?>
        </h2>
    </div>
</div>

<?= GridStack::widget([
    'widgets' => $widgets,
    'widgetTypes' => $widgetTypes,
]); ?>

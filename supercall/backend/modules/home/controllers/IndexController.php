<?php
namespace backend\modules\home\controllers;

use backend\components\web\Controller;
use backend\modules\widget\models\WidgetUser;
use backend\modules\widget\models\WidgetType;
use yii\helpers\ArrayHelper;
use common\models\User;
use Yii;

/**
 * Class IndexController
 * @package backend\modules\home\controllers
 */
class IndexController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $roles = Yii::$app->getAuthManager()->getRolesByUser(Yii::$app->user->id);
        $roles = ArrayHelper::getColumn($roles, 'name');

        $widgets = WidgetUser::find()
            ->joinWith(['type'])
            ->joinWith(['cache'])
            ->where([WidgetType::tableName() . '.status' => WidgetType::STATUS_ACTIVE, 'user_id' => Yii::$app->user->id]);

        if (!in_array(User::ROLE_SUPERADMIN, $roles)) {
            $widgets->andWhere(['user_id' => Yii::$app->user->id]);
        }

        $usedTypes = ArrayHelper::getColumn($widgets->all(), 'type_id');

        $widgetTypes = WidgetType::find()
            ->joinWith(['roles'])
            ->where([
                'status' => WidgetType::STATUS_ACTIVE,
            ])
            ->andWhere(['not in', WidgetType::tableName() . '.id', $usedTypes])
            ->orderBy(['name' => SORT_ASC]);

        if (!in_array(User::ROLE_SUPERADMIN, $roles)) {
            $widgetTypes->andWhere(['widget_role.role' => $roles]);
        }

        return $this->render('index', [
            'widgets' => $widgets->all(),
            'widgetTypes' => $widgetTypes->all(),
        ]);

    }
}

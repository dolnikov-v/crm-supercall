<?php

namespace backend\modules\administration\models;
use common\models\Logger;
use common\models\LoggerTag;

use Yii;

class BaseLogger extends Logger
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogger_tag()
    {
        return $this->hasMany(LoggerTag::className(), ['log_id' => 'id']);
    }

}
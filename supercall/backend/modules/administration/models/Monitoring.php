<?php

namespace backend\modules\administration\models;

use common\components\db\ActiveRecord;
use Yii;

/**
 * Class Monitoring
 * @package backend\modules\administration\models
 */
class Monitoring extends ActiveRecord
{
    const TYPE_CALL_DEFAULT = 'default';
    const TYPE_CALL_AUTO = 'auto_call';
    const TYPE_CALL_INCOMING = 'incoming_call';

    const STATUS_ZOIPER_REGISTER = 'register';
    const STATUS_ZOIPER_REGISTERED = 'registered';
    const STATUS_ZOIPER_REGISTER_FAILED = 'register_failed';
    const STATUS_ZOIPER_UNREGISTER = 'unregister';

    const STATUS_CALL_WAITING = 'waiting';
    const STATUS_CALL_RING = 'ring';
    const STATUS_CALL_ACCEPT = 'accept';
    const STATUS_CALL_REJECT = 'reject';
    const STATUS_CALL_HANG = 'hang';
    const STATUS_CALL_INCOMING = 'incoming';

    const QUEUE_PAUSE_ON = 'pause_on';
    const QUEUE_PAUSE_OFF = 'pause_off';

    const UNKNOWN_VALUE = 'Unknown value';

    /** @var  integer */
    public $user_country;

    /** @var  integer */
    public $user_id;

    /** @var  string */
    public $type_call;

    /** @var  string */
    public $status_zoiper;

    /** @var  string */
    public $status_call;

    /** @var  string */
    public $queue_paused;

    /** @var  integer */
    public $order;

    /** @var  string */
    public $ip_address;

    /**
     * @return string
     */
    public static function tableName()
    {
        return parent::tableName();
    }

    /**
     * @return array
     */
    public static function stateQueuePauseCollection()
    {
        return [
            self::QUEUE_PAUSE_ON => yii::t('common', 'На паузе'),
            self::QUEUE_PAUSE_OFF => yii::t('common', 'Свободен'),
        ];
    }

    /**
     * @param $state
     * @return mixed|string
     */
    public function getStateQueuePause($state)
    {
        $collection = self::stateQueuePauseCollection();

        return $collection[$state] ?? self::UNKNOWN_VALUE;
    }

    /**
     * @return array
     */
    public static function typeCallCollection()
    {
        return [
            self::TYPE_CALL_DEFAULT => yii::t('common', 'Обычный режим'),
            self::TYPE_CALL_INCOMING => yii::t('common', 'Входящая линия'),
            self::TYPE_CALL_AUTO => yii::t('common', 'Auto Call'),
        ];
    }

    /**
     * @param $type
     * @return mixed|string
     */
    public static function getTypeCall($type)
    {
        $collection = self::typeCallCollection();

        return $collection[$type] ?? self::UNKNOWN_VALUE;
    }

    /**
     * @return array
     */
    public static function statusZoiperCollection()
    {
        return [
            self::STATUS_ZOIPER_REGISTER => yii::t('common', 'Регистрация'),
            self::STATUS_ZOIPER_REGISTERED => yii::t('common', 'Зарегистрирован'),
            self::STATUS_ZOIPER_REGISTER_FAILED => yii::t('common', 'Ошибка авторизации'),
            self::STATUS_ZOIPER_UNREGISTER => yii::t('common', 'Разрегистрирован'),
        ];
    }

    /**
     * @param $status
     * @return mixed|string
     */
    public static function getStatusZoiper($status)
    {
        $collection = self::statusZoiperCollection();

        return $collection[$status] ?? self::UNKNOWN_VALUE;
    }

    /**
     * @return array
     */
    public static function statusCallCollection()
    {
        return [
            self::STATUS_CALL_WAITING => yii::t('common', 'Ожидание'),
            self::STATUS_CALL_RING => yii::t('common', 'Звонок'),
            self::STATUS_CALL_ACCEPT => yii::t('common', 'Разговор'),
            self::STATUS_CALL_REJECT => yii::t('common', 'Клиент сбросил'),
            self::STATUS_CALL_HANG => yii::t('common', 'Разговор закончен'),
            self::STATUS_CALL_INCOMING => yii::t('common', 'Входящий звонок'),
        ];
    }

    /**
     * @param $status
     * @return mixed|string
     */
    public static function getStatusCall($status)
    {
        $collection = self::statusCallCollection();

        return $collection[$status] ?? self::UNKNOWN_VALUE;
    }
}
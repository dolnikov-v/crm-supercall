<?php
/**
 * Created by PhpStorm.
 * User: PCWORK
 * Date: 10.10.2017
 * Time: 11:26
 */

namespace backend\modules\administration\models;

use common\components\db\ActiveRecord;
use backend\modules\administration\models\query\ApiLogQuery;
use common\modules\order\models\Order;
use Yii;

/**
 * Class ApiLog
 * @package backend\modules\administration\models
 */
class ApiLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%api_log}}';
    }

    /**
     * @return ApiLogQuery
     */
    public static function find()
    {
        return new ApiLogQuery(get_called_class());
    }

    public function rules()
    {
        return [
            [['request'], 'required'],
            [['url', 'auth', 'request', 'response', 'operator', 'ip'], 'string'],
            [['created_at', 'updated_at', 'order_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'url' => Yii::t('common', 'Url'),
            'auth' => Yii::t('common', 'Авторизация'),
            'request' => Yii::t('common', 'Запрос'),
            'response' => Yii::t('common', 'Ответ'),
            'operator' => Yii::t('common', 'Оператор'),
            'order_id' => Yii::t('common', 'Заказ'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
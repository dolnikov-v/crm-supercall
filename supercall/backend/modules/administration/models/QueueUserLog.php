<?php

namespace backend\modules\administration\models;

use backend\modules\queue\models\Queue;
use common\models\User;
use Yii;

/**
 * Class QueueUserLog
 * @package backend\modules\administration\models
 *
 * @property integer $id
 * @property integer $queue_id
 * @property integer $operator_id
 * @property integer $user_id
 * @property integer $action
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Queue $queue
 * @property User $operator
 * @property User $user
 */
class QueueUserLog extends \common\components\db\ActiveRecord
{
    const ACTION_INSERT = 'insert';
    const ACTION_DELETE = 'delete';

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%queue_user_log}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['queue_id', 'operator_id', 'user_id', 'action', 'created_at', 'updated_at'], 'required'],
            [['queue_id', 'operator_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['action'], 'string'],
            [['queue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Queue::className(), 'targetAttribute' => ['queue_id' => 'id']],
            [['operator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['operator_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'queue_id' => Yii::t('common', 'Очередь'),
            'operator_id' => Yii::t('common', 'Оператор'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'action' => Yii::t('common', 'Действие'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата изменения'),
        ];
    }

    /**
     * @param array|int $queueIds
     * @param int|string $operatorId
     * @param string $action
     * @throws \yii\db\Exception
     */
    public static function saveQueue($queueIds, $operatorId, $action)
    {
        $queueIds = is_array($queueIds) ? $queueIds : (array)$queueIds;

        $rows = [];

        $currentTime = (new \DateTime())->getTimestamp();

        foreach ($queueIds as $queueId) {
            $rows[] = [$queueId, $operatorId, Yii::$app->getUser()->getId(), $action, $currentTime, $currentTime];
        }

        \Yii::$app->db->createCommand()->batchInsert(self::tableName(), [
            'queue_id', 'operator_id', 'user_id', 'action', 'created_at', 'updated_at'
        ], $rows)->execute();
    }

    /**
     * @return array|\yii\db\ActiveRecord|null
     */
    public function getQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_id'])->one();
    }

    /**
     * @return array|\yii\db\ActiveRecord|null
     */
    public function getOperator()
    {
        return $this->hasOne(User::className(), ['id' => 'operator_id'])->one();
    }

    /**
     * @return array|\yii\db\ActiveRecord|null
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->one();
    }
}

<?php
namespace backend\modules\administration\models;

use backend\modules\administration\models\query\JsLoggerQuery;
use common\components\db\ActiveRecord;
use common\models\User;
use common\modules\order\models\Order;
use Yii;

/**
 * Class JsLogger
 * @package backend\modules\administration\models
 */
class JsLogger extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%js_logger}}';
    }

    /**
     * @return JSLoggerQuery
     */
    public static function find()
    {
        return new JsLoggerQuery(get_called_class());
    }

    public function rules()
    {
        return [
            [['script'], 'required'],
            [['script', 'message', 'type', 'stack'], 'string'],
            [['created_at', 'updated_at', 'order_id', 'user_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'script' => Yii::t('common', 'Скрипт'),
            'type' => Yii::t('common', 'Тип'),
            'message' => Yii::t('common', 'Текст'),
            'stack' => Yii::t('common', 'Стек'),
            'user_id' => Yii::t('common', 'Оператор'),
            'order_id' => Yii::t('common', 'Заказ'),
            'created_at' => Yii::t('common', 'Создан'),
            'updated_at' => Yii::t('common', 'Обновлен')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


}
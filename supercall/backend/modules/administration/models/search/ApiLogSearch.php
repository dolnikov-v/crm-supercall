<?php
namespace backend\modules\administration\models\search;

use backend\modules\administration\models\ApiLog;
use yii\data\ActiveDataProvider;

/**
 * Class ApiLogSearch
 * @package backend\modules\administration\models\search
 */
class ApiLogSearch extends ApiLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'order_id', 'created_at'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort[ApiLog::tableName().'.id'] = SORT_DESC;
        }

        $query = ApiLog::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([ApiLog::tableName().'.order_id' => $this->order_id]);
        $query->andFilterWhere(['like', ApiLog::tableName().'.url',  $this->url]);

        if($this->created_at){
            $period = explode(' - ', $this->created_at);

            $query->andFilterWhere(['>=', ApiLog::tableName().'.created_at', strtotime(date($period[0]))]);
            $query->andFilterWhere(['<=', ApiLog::tableName().'.created_at', strtotime(date($period[1]))]);
        }



        return $dataProvider;
    }
}
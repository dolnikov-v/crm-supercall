<?php
namespace backend\modules\administration\models\search;

use backend\modules\administration\models\JsLogger;
use yii\data\ActiveDataProvider;

/**
 * Class JsLoggerSearch
 * @package backend\modules\administration\models\search
 */
class JsLoggerSearch extends JSLogger
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['script', 'order_id', 'created_at'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort[JsLogger::tableName().'.id'] = SORT_DESC;
        }

        $query = JsLogger::find()
            ->joinWith(['order', 'user'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([JsLogger::tableName().'.order_id' => $this->order_id]);
        $query->andFilterWhere(['like', JsLogger::tableName().'.script',  $this->script]);

        if($this->created_at){
            $period = explode(' - ', $this->created_at);

            $query->andFilterWhere(['>=', JsLogger::tableName().'.created_at', strtotime(date($period[0]))]);
            $query->andFilterWhere(['<=', JsLogger::tableName().'.created_at', strtotime(date($period[1]))]);
        }

        return $dataProvider;
    }
}
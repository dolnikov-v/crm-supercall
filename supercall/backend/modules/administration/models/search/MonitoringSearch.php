<?php

namespace backend\modules\administration\models\search;

use backend\modules\administration\models\Monitoring;
use Yii;

/**
 * Class MonitoringSearch
 * @package backend\modules\administration\models\search
 */
class MonitoringSearch extends Monitoring
{
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'user_country' => Yii::t('common', 'Страна'),
            'user_id' => Yii::t('common', 'Оператор'),
            'type_call' => Yii::t('common', 'Тип'),
            'status_zoiper' => Yii::t('common', 'Статус Zoiper'),
            'status_call' => Yii::t('common', 'Статус звонка'),
            'queue_paused' => Yii::t('common', 'Статус очереди'),
            'order' => Yii::t('common', 'Заказ'),
            'ip_address' => Yii::t('common', 'IP адрес'),
        ];
    }
}
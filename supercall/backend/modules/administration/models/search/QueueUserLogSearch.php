<?php

namespace backend\modules\administration\models\search;

use backend\modules\administration\models\QueueUserLog;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class QueueUserLogSearch
 * @package backend\modules\administration\models\search
 */
class QueueUserLogSearch extends QueueUserLog
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['queue_id', 'operator_id', 'user_id', 'action', 'created_at'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     * @throws \Exception
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = parent::find()->orderBy($sort);

        $config = [
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ]
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        if ($this->queue_id) {
            $query->andWhere(['=', 'queue_id', $this->queue_id]);
        }

        if ($this->operator_id) {
            $query->andWhere(['=', 'operator_id', $this->operator_id]);
        }

        if ($this->user_id) {
            $query->andWhere(['=', 'user_id', $this->user_id]);
        }

        if ($this->action) {
            $query->andWhere(['=', 'action', $this->action]);
        }

        if (!$this->created_at) {
            $this->created_at = date('d/m/Y 00:00:00') . ' - ' . date('d/m/Y 23:59:59');
        }

        /** @var Object $date */
        $date = Yii::$app->formatter->asConvertToFrom($this->created_at);

        $from = new \DateTime($date->from->format('Y-m-d H:i:s'), new \DateTimeZone(yii::$app->user->timezone->timezone_id));
        $to = new \DateTime($date->to->format('Y-m-d H:i:s'), new \DateTimeZone(yii::$app->user->timezone->timezone_id));

        $query->andWhere(['between', parent::tableName() . '.created_at' , $from->getTimestamp(), $to->getTimestamp()]);

        return $dataProvider;
    }
}

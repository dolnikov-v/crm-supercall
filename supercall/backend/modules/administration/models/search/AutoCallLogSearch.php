<?php

namespace backend\modules\administration\models\search;

use common\models\AutoCallLog;
use yii\data\ActiveDataProvider;


class AutoCallLogSearch extends AutoCallLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'order_phone', 'country_id', 'queue_name', 'call_status', 'user_sip'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }
        
        $query = AutoCallLog::find()->orderBy($sort);

        $config = [
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ]
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        if($this->order_id){
            $query->andWhere(['=', 'order_id', $this->order_id]);
        }
        
        if($this->order_phone){
            $query->andWhere(['like', 'order_phone', $this->order_phone]);
        }
    
        if($this->country_id){
            $query->andWhere(['=', 'country_id', $this->country_id]);
        }
    
        if($this->queue_name){
            $query->andWhere(['like', 'queue_name', $this->queue_name]);
        }
    
        if($this->call_status){
            $query->andWhere(['=', 'call_status', $this->call_status]);
        }
    
        if($this->user_sip){
            $query->andWhere(['like', 'user_sip', $this->user_sip]);
        }
    
        if($this->created_at){
            $period = explode(' - ', $this->created_at);
        
            $query->andFilterWhere(['>=', ApiLog::tableName().'.created_at', strtotime(date($period[0]))]);
            $query->andFilterWhere(['<=', ApiLog::tableName().'.created_at', strtotime(date($period[1]))]);
        }
        
        return $dataProvider;
    }
}
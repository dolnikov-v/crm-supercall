<?php

namespace backend\modules\administration\models\search;

use common\models\AutoCallCountry;
use yii\data\ActiveDataProvider;


class AutoCallCountrySearch extends AutoCallCountry
{
    public function rules()
    {
        return [
            [['country_id', 'count'], 'integer'],
            [['is_active'], 'boolean'],
        ];
    }


    public function search($params = [])
    {
        $sort = [];

        if (empty($params['dp-1-sort'])) {
            $sort['display_position'] = SORT_ASC;
        }

        $query = AutoCallCountry::find()->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        if ($this->country_id) {
            $query->andWhere(['=', 'country_id', $this->country_id]);
        }

        if ($this->is_active != "") {
            $query->andWhere(['=', 'is_active', $this->is_active]);
        }

        return $dataProvider;
    }
}
<?php

namespace backend\modules\administration\models\search;

use backend\modules\administration\models\BaseLogger;
use common\models\Logger;
use common\models\LoggerTag;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class BaseLoggerSearch
 * @package backend\modules\administration\models\search
 */
class BaseLoggerSearch extends BaseLogger
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['route', 'action', 'type', 'tag', 'value', 'created_at', 'old_tags', 'model'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $this->tag = yii::$app->request->get('tag');
        $this->value = yii::$app->request->get('value');

        $sort = [];

        if (empty($params['sort'])) {
            $sort[BaseLogger::tableName() . '.id'] = SORT_DESC;
        }

        $query = BaseLogger::find()
            ->orderBy($sort);

        if (empty($params)) {
            $query->limit(20);
        }

        $config = [
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->route) {
            $query->andWhere(['route' => $this->route]);
        }

        if ($this->action) {
            $query->andWhere(['action' => $this->action]);
        }

        if ($this->type) {
            $query->andWhere(['type' => $this->type]);
        }

        if ($this->created_at) {
            $period = explode(' - ', $this->created_at);

            $query->andFilterWhere(['>=', BaseLogger::tableName() . '.created_at', strtotime(date($period[0]))]);
            $query->andFilterWhere(['<=', BaseLogger::tableName() . '.created_at', strtotime(date($period[1]))]);
        }

        $foundedLogs = [];


        if ($this->tag) {
            foreach ($this->tag as $k => $tag) {
                if (!empty($tag) && isset($this->value[$k]) && !empty($this->value[$k])) {

                    $logsId = ArrayHelper::getColumn(LoggerTag::findAll(['tag' => $tag, 'value' => $this->value[$k]]), 'log_id');
                    $foundedLogs = array_merge($foundedLogs, $logsId);
                }
            }

            if (!empty($foundedLogs)) {
                $query->andWhere([Logger::tableName() . '.id' => array_values($foundedLogs)]);
            }
        }

        return $dataProvider;
    }
}
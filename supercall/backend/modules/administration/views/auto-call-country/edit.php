<?php

use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\Product $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление настройки автодозвона страны') : Yii::t('common', 'Редактирование настройки автодозвона страны');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Автодозвон стран'), 'url' => Url::toRoute('/administration/auto-call-country/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Настройка автодозвона страны'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>

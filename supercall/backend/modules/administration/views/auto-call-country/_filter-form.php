<?php

use backend\assets\SiteAsset;
use common\widgets\ActiveForm;
use yii\helpers\Url;

SiteAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">

        <div class="col-lg-3 col-sm-3 col-xs-3">
            <?= $form->field($modelSearch, 'country_id')->select2List($countries, [
                'value' => $modelSearch->country_id,
                'multiple' => false,
                'placeholder' => yii::t('common', 'Все'),
            ])->label(\Yii::t('common', 'Страна')) ?>
        </div>

        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'is_active')->select2List([
                true => yii::t('common', 'Да'),
                false => yii::t('common', 'Нет')
            ], ['prompt' => '—']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-sm-3 col-xs-3 ">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
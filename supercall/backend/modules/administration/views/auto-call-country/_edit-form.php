<?php

use common\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Country;

/** @var \common\models\AutoCallCountry $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <?php if ($model->isNewRecord): ?>
        <div class="col-lg-4">
            <?= $form->field($model, 'country_id')->select2List(Country::find()->collection('common'), [
                'prompt' => '—',
            ]); ?>
        </div>
    <?php endif ?>

    <div class="col-lg-4">
        <?= $form->field($model, 'count')->textInput([
            'type' => 'number'
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'is_active')->select2List([
            true => yii::t('common', 'Да'),
            false => yii::t('common', 'Нет')
        ], ['value' => [is_null($model->is_active) ? true : $model->is_active]]); ?>
    </div>

</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить настройку автодозвона') : Yii::t('common', 'Сохранить настройку автодозвона')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

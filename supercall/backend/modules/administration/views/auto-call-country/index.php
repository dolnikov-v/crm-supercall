<?php

use common\components\grid\ActionColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\models\Country;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\Panel;
use yii\helpers\Url;
use common\widgets\base\LinkPager;
use common\modules\order\assets\PagerSizeAsset;

PagerSizeAsset::register($this);

$this->title = Yii::t('common', 'Автодозвон стран');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Автодозвона стран')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
        'countries' => $countries,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с настройками автодозвона по странам'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'data' => [
                'sortable-widget' => 1,
                'sortable-url' => \yii\helpers\Url::toRoute(['sorting', 'page' => $page, 'pageSize' => $dataProvider->pagination->pageSize]),
            ]
        ],

        'rowOptions' => function ($model) {
            return [
                'data-sortable-id' => $model->id,
            ];
        },
        'columns' => [
            [
                'class' => \beatep\sortable\grid\Column::className(),
                'visible' => ($modelSearch->is_active || $modelSearch->country_id != 0 || $modelSearch->country_id) ? false : true
            ],
            [
                'class' => IdColumn::className(),
                'headerOptions' => ['class' => 'width-50'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country_id',
                'headerOptions' => ['class' => 'width-200'],
                'enableSorting' => false,
                'content' => function ($data) {
                    $countryData = Country::findOne(['id' => $data['country_id']]);
                    return $countryData->name;
                }
            ],
            [
                'attribute' => 'count',
                'enableSorting' => false,
                'headerOptions' => ['class' => 'width-300'],
            ],
            [
                'attribute' => 'is_active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->is_active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->is_active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['/administration/auto-call-country/edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('administration.autocallcountry.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/administration/auto-call-country/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('administration.autocallcountry.edit') && !$model->is_active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/administration/auto-call-country/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('administration.autocallcountry.edit') && $model->is_active;
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('administration.autocallcountry.edit')
            ? ButtonLink::widget([
                'label' => Yii::t('common', 'Добавить настройку автодозвона'),
                'url' => Url::toRoute('/administration/auto-call-country/edit'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL,
            ])
            : '') .
        LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
            'counterPosition' => LinkPager::COUNTER_POSITION_RIGHT,
        ]),
]) ?>

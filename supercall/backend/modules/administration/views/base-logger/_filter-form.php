<?php

use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use common\widgets\base\Button;
use common\widgets\base\GlyphIcon;
use backend\modules\administration\assets\BaseLoggerAsset;

/** @var  $modelSearch */
/** @var array $types */
/** @var array $actions */

BaseLoggerAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">

        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'route')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'type')->select2List(ArrayHelper::map($types, 'type', 'type'), [
                'prompt' => '-'
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'action')->select2List(ArrayHelper::map($actions, 'action', 'action'), [
                'prompt' => '-'
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'created_at')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'pluginOptions' => [
                    'useWithAddon' => true,
                    'timePicker' => false,
                    'locale' => [
                        'format' => 'YYYY/MM/DD',
                    ],
                    'options' => [
                        'value' => date('Y/m/d') . ' - ' . date('Y/m/d')
                    ]
                ],
            ])->label(yii::t('common', 'Дата изменения')); ?>

        </div>
    </div>

    <h4>Поиск по тегам</h4>
    <hr/>

    <div class="tags">

        <?php if (!yii::$app->request->get('tag')): ?>

            <div class="row">
                <div class="col-lg-3">
                    <?= $form->field($modelSearch, 'tag')->textInput([
                        'placeholder' => 'tag',
                        'name' => 'tag[]',
                        'value' => ''
                    ])->label(false) ?>
                </div>
                <div class="col-lg-3">
                    <?= $form->field($modelSearch, 'value')->textInput([
                        'placeholder' => 'value',
                        'name' => 'value[]',
                        'value' => ''
                    ])->label(false) ?>
                </div>
                <div class="col-lg-3">
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_SUCCESS . ' add-filters dropdown-toggle',
                            'icon' => GlyphIcon::PLUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>
                    </div>
                    <div class="btn-group">
                        <?= Button::widget([
                            'style' => Button::STYLE_DANGER . ' remove-filters dropdown-toggle',
                            'icon' => GlyphIcon::MINUS,
                            'attributes' => ['data-toggle' => 'dropdown']
                        ]) ?>
                    </div>
                </div>
            </div>

        <?php else: ?>

            <?php $values = yii::$app->request->get('value'); ?>

            <?php $unlicTags = []; ?>

            <?php foreach (yii::$app->request->get('tag') as $k => $tag) : ?>

                <?php if (in_array(md5($tag . (isset($values[$k]) ? $values[$k] : '')), $unlicTags)) : ?>

                    <?php continue; ?>

                <?php endif; ?>

                <?php $unlicTags[] = md5($tag . (isset($values[$k]) ? $values[$k] : '')); ?>

                <div class="row">
                    <div class="col-lg-3">
                        <?= $form->field($modelSearch, 'tag')->textInput([
                            'placeholder' => 'tag',
                            'name' => 'tag[]',
                            'value' => $tag
                        ])->label(false) ?>
                    </div>
                    <div class="col-lg-3">
                        <?= $form->field($modelSearch, 'value')->textInput([
                            'placeholder' => 'value',
                            'name' => 'value[]',
                            'value' => isset($values[$k]) ? $values[$k] : ''
                        ])->label(false) ?>
                    </div>
                    <div class="col-lg-3">
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_SUCCESS . ' add-filters dropdown-toggle',
                                'icon' => GlyphIcon::PLUS,
                                'attributes' => ['data-toggle' => 'dropdown']
                            ]) ?>
                        </div>
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_DANGER . ' remove-filters dropdown-toggle',
                                'icon' => GlyphIcon::MINUS,
                                'attributes' => ['data-toggle' => 'dropdown']
                            ]) ?>
                        </div>
                    </div>
                </div>


            <?php endforeach; ?>

        <?php endif; ?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>

    <br/>
    <br/>

    <div class="row">

        <div class="col-md-6">
            <fieldset>
                <legend><h5>Types collection</h5></legend>
                <?php foreach ($types as $log): ?>
                    <?php
                    $url = Url::toRoute([
                        '/administration/base-logger/index',
                        'BaseLoggerSearch' => [
                            'type' => $log->type
                        ]
                    ]);
                    ?>

                    <a href="<?= $url ?>">
                        <span class="label label-primary"><?= $log->type; ?></span>
                    </a>
                <?php endforeach; ?>
            </fieldset>
        </div>

        <div class="col-md-6">
            <fieldset>
                <legend><h5>Actions collection</h5></legend>
                <?php foreach ($actions as $log): ?>
                    <?php
                    $url = Url::toRoute([
                        '/administration/base-logger/index',
                        'BaseLoggerSearch' => [
                            'action' => $log->action
                        ]
                    ]);
                    ?>
                    <a href="<?= $url; ?>">
                        <span class="label label-warning"><?= $log->action; ?></span>
                    </a>
                <?php endforeach; ?>
            </fieldset>
        </div>

    </div>

<?php ActiveForm::end(); ?>
<?php
use common\widgets\base\Panel;
use common\helpers\grid\DataProvider;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use yii\helpers\Json;
use yii\helpers\Url;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use backend\modules\administration\models\BaseLogger;
use yii\widgets\LinkPager;
use yii\helpers\Html;

/** @var \backend\modules\administration\models\search\BaseLoggerSearch $modelSearch */
/** @var array $types */
/** @var array $actions */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Логгер');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Логгер')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
        'types' => $types,
        'actions' => $actions,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с логами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {

        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'width-50'],

            ],
            [
                'attribute' => 'action',
                'headerOptions' => ['class' => 'width-150'],
            ],
            [
                'attribute' => 'route',
                'headerOptions' => ['class' => 'width-150'],
            ],
            [
                'attribute' => 'tags',
                'headerOptions' => ['class' => 'width-150'],
                'contentOptions' => ['class' => 'width-200'],
                'content' => function ($data) {
                    $tags = json_decode($data->tags, 1);

                    if (is_array($tags)) {
                        $array_tags = [];
                        foreach ($tags as $tag => $value) {
                            $name = '<b>' . $tag . '</b> : <code>' . $value . '</code>';
                            $array_tags[] = $name;
                        }

                        return implode('<br/>', $array_tags);
                    } else {
                        return null;
                    }
                }
            ],
            [
                'attribute' => 'error',
                'content' => function ($data) {
                    return '<code>' . $data->error . '</code>';
                }
            ],
            [
                'attribute' => 'type',
                'headerOptions' => ['class' => 'width-150'],
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
                'headerOptions' => ['class' => 'width-150 text-center'],
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

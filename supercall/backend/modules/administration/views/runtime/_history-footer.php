<?php
use common\widgets\base\ButtonLink;
use yii\helpers\Url;

/** @var \yii\web\View $this */
?>

<?= ButtonLink::widget([
    'label' => Yii::t('common', 'Вернуться назад'),
    'url' => Url::to('index'),
]) ?>

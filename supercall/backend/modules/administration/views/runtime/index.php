<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use backend\modules\administration\components\runtime\Log;
use common\widgets\base\Panel;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Логирование');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Логирование')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список логов'),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'label' => Yii::t('common', 'Лог'),
                'enableSorting' => false,
                'content' => function (Log $model) {
                    return $model->name . ' - ' . Html::tag('span', $model->fileName, ['class' => 'text-gray']);
                },
            ],
            [
                'label' => Yii::t('common', 'Счетчики'),
                'enableSorting' => false,
                'content' => function (Log $model) {
                    return $this->render('_index-counter', ['log' => $model]);
                }
            ],
            [
                'attribute' => 'size',
                'label' => Yii::t('common', 'Размер'),
                'enableSorting' => false,
                'format' => 'shortSize',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'updatedAt',
                'label' => Yii::t('common', 'Дата изменения'),
                'enableSorting' => false,
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть'),
                        'url' => function (Log $model) {
                            return Url::toRoute(['view', 'name' => urlencode($model->name)]);
                        },
                        'can' => function (Log $model) {
                            return Yii::$app->user->can('administration.runtime.view');
                        },
                        'attributes' => function () {
                            return [
                                'target' => '_blank',
                            ];
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'История'),
                        'url' => function (Log $model) {
                            return Url::toRoute(['history', 'name' => urlencode($model->name)]);
                        },
                        'can' => function (Log $model) {
                            return Yii::$app->user->can('administration.runtime.history');
                        }
                    ],
                    [
                        'can' => function (Log $model) {
                            return (Yii::$app->user->can('administration.runtime.view') || Yii::$app->user->can('administration.runtime.history')) &&
                                Yii::$app->user->can('administration.runtime.archive');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Архивировать'),
                        'url' => function (Log $model) {
                            return Url::toRoute(['archive', 'name' => urlencode($model->name)]);
                        },
                        'can' => function (Log $model) {
                            return Yii::$app->user->can('administration.runtime.archive');
                        }
                    ],
                ]
            ]
        ],
    ]),
]) ?>

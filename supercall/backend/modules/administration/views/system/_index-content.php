<?php
use common\widgets\base\ButtonLink;
use yii\helpers\Url;
use common\helpers\fonts\PeIcon;
?>

<div class="row">
    <div class="col-lg-12">
        <?= ButtonLink::widget([
            'style' => ButtonLink::STYLE_DEFAULT . ' width-200',
            'url' => Url::toRoute(['flush-cache']),
            'icon' => '<i class="margin-right-3 icon ' . PeIcon::REFRESH . '"></i>',
            'label' => Yii::t('common', 'Очистить Cache'),
        ]) ?>
    </div>
</div>

<div class="row margin-top-10">
    <div class="col-lg-12">
        <?= ButtonLink::widget([
            'style' => ButtonLink::STYLE_DEFAULT . ' width-200',
            'url' => Url::toRoute(['clear-assets']),
            'icon' => '<i class="margin-right-3 icon ' . PeIcon::REFRESH . '"></i>',
            'label' => Yii::t('common', 'Очистить Assets'),
        ]) ?>
    </div>
</div>

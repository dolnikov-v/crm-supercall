<?php
use common\widgets\base\Panel;

/** @var \yii\web\View $this */

$this->title = Yii::t('common', 'Система');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Система')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список действий'),
    'content' => $this->render('_index-content'),
]) ?>

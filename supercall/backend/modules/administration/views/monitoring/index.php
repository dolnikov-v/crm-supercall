<?php

use backend\modules\administration\assets\MonitoringAsset;
use common\assets\base\SocketIoAsset;
use yii\bootstrap\Tabs;
use common\assets\base\CustomCommonAsset;

$this->title = Yii::t('common', 'Мониторинг');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Мониторинг')];

/** @var \backend\modules\administration\models\search\MonitoringSearch $modelSearch */
/** @var array $countries */
/** @var array $operators */

SocketIoAsset::register($this);
MonitoringAsset::register($this);
CustomCommonAsset::register($this);
?>

<?= Tabs::widget([
    'id' => 'monitoring-panel',
    'items' => [
        [
            'label' => Yii::t('common', 'Операторы онлайн'),
            'content' => $this->render('_users-online', [
                'modelSearch' => $modelSearch,
                'countries' => $countries,
                'operators' => $operators
            ]),
            'active' => true,
            'options' => [
                'id' => 'monitoring-users-online'
            ]
        ],
        [
            'label' => Yii::t('common', 'Инфографика'),
            'content' => $this->render('_charts', [
                'modelSearch' => $modelSearch
            ]),
            'active' => false,
            'options' => [
                'id' => 'monitoring-charts'
            ]
        ],
    ]
]); ?>
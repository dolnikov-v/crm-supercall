<?php
use common\widgets\base\Panel;

/** @var \backend\modules\administration\models\search\MonitoringSearch $modelSearch */
/** @var array $countries */
/** @var array $operators */

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
        'countries' => $countries,
        'operators' => $operators
    ]),
]) ?>

<?php $contentUsers = '<table class="monitoring-table table table-striped table-hover table-sortable table-responsive">
        <thead>
        <tr>
            <th scope="col">User_id</th>
            <th scope="col" class="text-center width-20">' . yii::t('common', 'Имя пользователя') . '</th>
            <th scope="col" class="text-center">' . yii::t('common', 'Sip логин') . '</th>
            <th scope="col" class="text-center">Ready</th>
            <th scope="col" class="text-center">' . yii::t('common', 'Тип') . '</th>
            <th scope="col" class="text-center width-20">' . yii::t('common', 'Мессенджер') . '</th>
            <th scope="col" class="width-200">' . yii::t('common', 'Статус Zoiper') . '</th>
            <th scope="col" class="text-center width-50">' . yii::t('common', 'Статус очереди') . '</th>
            <th scope="col" class="text-center width-200">' . yii::t('common', 'Статус звонка') . '</th>
            <th scope="col" class="text-center width-150">' . yii::t('common', 'Заказ') . '</th>
            <th scope="col" class="text-center width-100">' . yii::t('common', 'Страны пользователя') . '</th>
            <th scope="col" class="text-center width-50">' . yii::t('common', 'Action time') . '</th>
            <th scope="col" class="text-center width-50">' . yii::t('common', 'Последняя активность') . '</th>
            <th scope="col" class="text-center width-20">' . yii::t('common', 'IP') . '</th>
            <!--<th scope="col" class="text-center width-50">' . yii::t('common', 'Источник') . '</th>-->
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>'; ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с пользователями') . '<div class="socket_state"><span class="fa fa-user"></span>&nbsp;<span class="summary_users"><strong>0</strong></span>&nbsp;&nbsp;<span class="state_monitor fa fa-chain-broken text-danger"></span></div>',
    'contentPadding' => false,
    'content' => $contentUsers

]); ?>
<?php

use backend\modules\administration\models\Monitoring;
use common\models\Country;
use common\widgets\ActiveForm;
use common\widgets\base\Button;
use yii\helpers\Url;

/** @var \backend\modules\administration\models\search\MonitoringSearch $modelSearch */
/** @var array $countries */
/** @var array $operators */

?>

<?php

$form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-lg-2">
        <?= $form->field($modelSearch, 'user_country')->select2List($countries, [
            'prompt' => '-'
        ]); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($modelSearch, 'user_id')->select2List($operators, [
            'prompt' => '-'
        ]); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($modelSearch, 'type_call')->select2List(Monitoring::typeCallCollection(), [
            'prompt' => '-'
        ]); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($modelSearch, 'status_zoiper')->select2List(Monitoring::statusZoiperCollection(), [
            'prompt' => '-',
        ]); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($modelSearch, 'status_call')->select2List(Monitoring::statusCallCollection(), [
            'prompt' => '-',
        ]); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($modelSearch, 'queue_paused')->select2List(Monitoring::stateQueuePauseCollection(), [
            'prompt' => '-',
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($modelSearch, 'order')->textInput(); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($modelSearch, 'ip_address')->textInput(); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<hr/>

<div class="row">
    <div class="col-lg-12 pull-left">
        <?= Button::widget([
            'id' => 'reset-filter',
            'label' => Yii::t('common', 'Сбросить'),
            'style' => Button::STYLE_DEFAULT,
            'size' => Button::SIZE_SMALL,
        ]) ?>
    </div>
</div>
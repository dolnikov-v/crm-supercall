<?php
use common\helpers\fonts\PeIcon;
use common\widgets\base\ButtonLink;
use yii\helpers\Url;

?>

<div class="row">
    <div class="col-lg-12">

        <?php if ($in_progress): ?>
            <?= \yii\bootstrap\Alert::widget([
                    'options' => ['class' => 'alert-warning'],
                    'body' => Yii::t('common', 'Идет пересчет статистики'),
                    'closeButton' => false
            ]); ?>
        <?php else: ?>
            <?= ButtonLink::widget([
                'style' => ButtonLink::STYLE_DEFAULT,
                'url' => Url::toRoute(['recalc']),
                'icon' => '<i class="margin-right-3 icon ' . PeIcon::REFRESH . '"></i>',
                'label' => Yii::t('common', 'Пересчитать статистику'),
            ]) ?>
        <?php endif; ?>

    </div>
</div>
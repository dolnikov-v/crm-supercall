<?php
use common\widgets\base\Panel;

/** @var \yii\web\View $this */

$this->title = Yii::t('common', 'Статистика');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Пересчет статистики'),
    'content' => $this->render('_index-content', ['in_progress' => $in_progress]),
]) ?>

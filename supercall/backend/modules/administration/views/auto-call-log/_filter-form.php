<?php
    use common\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    use yii\helpers\Url;
    use kartik\daterange\DateRangePicker;
    use backend\assets\SiteAsset;
    
    SiteAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'order_id')->textInput(); ?>
        </div>
        
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'order_phone')->textInput(); ?>
        </div>
        
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'country_id')->select2List($countries, [
                'value' => $modelSearch->country_id,
                'multiple' => false,
                'placeholder' => yii::t('common', 'Все'),
            ])->label(\Yii::t('common', 'Страна')) ?>
        </div>
        
        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'user_sip')->textInput(); ?>
        </div>
        

        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'queue_name')->textInput(); ?>
        </div>

        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'call_status')->select2List(
                [
                    '0' => Yii::t('common', 'Все'),
                    'ANSWERED'  => 'ANSWERED',
                    'BUSY'      => 'BUSY',
                    'NO ANSWER' => 'NO ANSWER',
                    'FAILED'    => 'FAILED'
                ],
                [
                    'value'       => $modelSearch->call_status,
                    'prompt'      => '-',
                    'placeholder' => yii::t('common', 'Все'),
                ]) ?>
        </div>

        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'created_at')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'pluginOptions' => [
                    'useWithAddon' => true,
                    'timePicker'   => false,
                    'locale' => [
                        'format' => 'YYYY-MM-DD',
                    ],
                    'options' => [
                        'value' => date('Y/m/d') . ' - ' . date('Y/m/d')
                    ]
                ],
            ])->label(yii::t('common', 'Дата изменения')); ?>

        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
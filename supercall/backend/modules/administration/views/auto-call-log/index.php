<?php
use common\widgets\base\Panel;
use common\helpers\grid\DataProvider;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\components\grid\DateColumn;
use common\models\Country;
use yii\widgets\LinkPager;


$this->title = Yii::t('common', 'Логи автодозвона');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Логи автодозвона')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
        'countries' => $countries,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с логами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {

        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'order_id',
                'headerOptions' => ['class' => 'width-100'],
            ],
            [
                'attribute' => 'order_phone',
                'headerOptions' => ['class' => 'width-200'],
            ],
            [
                'attribute' => 'call_status',
                'headerOptions' => ['class' => 'width-150'],
            ],
            [
                'attribute' => 'country_id',
                'headerOptions' => ['class' => 'width-150'],
                'content' => function($data) {
                    $countryData = Country::findOne(['id' => $data['country_id']]);
                    return $countryData->name;
                }
            ],
            [
                'attribute' => 'user_sip',
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y-m-d H:i:s'],
                'enableSorting' => false,
                'headerOptions' => ['class' => 'width-150 text-center'],
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

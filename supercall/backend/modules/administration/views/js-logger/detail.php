<?php
    /** @var $jsLogger */
?>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'ID'); ?></b></div>
    <div class="col-xs-11"><?= $jsLogger->id; ?></div>
</div>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Заказ'); ?></b></div>
    <div class="col-xs-11"><?= $jsLogger->order_id; ?></div>
</div>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'script'); ?></b></div>
    <div class="col-xs-11"><?= $jsLogger->script; ?></div>
</div>
<br/>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Тип'); ?></b></div>
    <div class="col-xs-11" ><code><?= $jsLogger->type; ?></code><br/>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Ошибка'); ?></b></div>
    <div class="col-xs-11"><code><?= $jsLogger->message; ?></code><br/>
    </div>
</div>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Стек'); ?></b></div>
    <div class="col-xs-11"><code><?= $jsLogger->stack; ?></code><br/>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Дата создания'); ?></b></div>
    <div class="col-xs-11"><?= date('Y-m-d H:i:s', $jsLogger->created_at); ?></div>
</div>
<?php
use backend\modules\administration\models\ApiLog;
use common\widgets\base\Panel;

/** @var \backend\modules\administration\models\JSLogger $jsLogger */

$this->title = '#' . $jsLogger->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'JS логгирование'), 'url' => '/administration/js-logger'];
$this->params['breadcrumbs'][] = ['label' => '#' . $jsLogger->id];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Просмотр лога'),
    'content' => $this->render('detail', ['jsLogger' => $jsLogger]),
]) ?>



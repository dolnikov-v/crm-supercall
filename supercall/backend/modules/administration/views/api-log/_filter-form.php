<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

/** @var  $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">

        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'url')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'order_id')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'created_at')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'pluginOptions' => [
                    'useWithAddon' => true,
                    'timePicker' => false,
                    'locale' => [
                        'format' => 'YYYY/MM/DD',
                    ],
                    'options' => [
                        'value' => date('Y/m/d') . ' - ' . date('Y/m/d')
                    ]
                ],
            ])->label(yii::t('common', 'Дата изменения')); ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
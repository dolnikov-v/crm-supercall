<?php
    /** @var $apiLog */
?>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'ID'); ?></b></div>
    <div class="col-xs-11"><?= $apiLog->id; ?></div>
</div>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Заказ'); ?></b></div>
    <div class="col-xs-11"><?= $apiLog->order_id; ?></div>
</div>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Url'); ?></b></div>
    <div class="col-xs-11"><?= $apiLog->url; ?></div>
</div>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Дата создания'); ?></b></div>
    <div class="col-xs-11"><?= $apiLog->created_at; ?></div>
</div>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Авторизация'); ?></b></div>
    <div class="col-xs-11"><?= $apiLog->auth; ?></div>
</div>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Запрос'); ?></b></div>
    <div class="col-xs-11" ><code><?= $apiLog->request; ?></code><br/><br/>
        <pre><?= print_r(json_decode($apiLog->request, 1), 1); ?></pre>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-xs-1"><b><?= yii::t('common', 'Ответ'); ?></b></div>
    <div class="col-xs-11"><code><?= $apiLog->response; ?></code><br/><br/>
        <pre><?= print_r(json_decode($apiLog->response, 1), 1); ?></pre>
    </div>
</div>
<?php
use backend\modules\administration\models\ApiLog;
use common\widgets\base\Panel;

/** @var ApiLog $apiLog */

$this->title = '#' . $apiLog->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'API логи'), 'url' => '/administration/api-log'];
$this->params['breadcrumbs'][] = ['label' => '#' . $apiLog->id];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Просмотр лога'),
    'content' => $this->render('detail', ['apiLog' => $apiLog]),
]) ?>



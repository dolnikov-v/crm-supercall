<?php
use common\widgets\base\Panel;
use common\helpers\grid\DataProvider;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use yii\helpers\Url;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use backend\modules\administration\models\ApiLog;
use yii\widgets\LinkPager;
use backend\modules\administration\assets\ApiLogAsset;

/** @var  $modelSearch */
/** @var  $dataProvider */

ApiLogAsset::register($this);

$this->title = Yii::t('common', 'API логи');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'API логи')];
?>

<?=Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с логами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {

        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'order_id',
                'headerOptions' => ['class' => 'width-50'],

            ],
            [
                'attribute' => 'url',
                'headerOptions' => ['class' => 'width-150'],
            ],
            [
                'attribute' => 'auth',
                'headerOptions' => ['class' => 'width-150'],
            ],
            [
                'attribute' => 'request',
                'content' => function($data){

                    return mb_substr($data->request, 0, 50, 'utf-8').' ...';
                }
            ],
            [
                'attribute' => 'response',
                'content' => function($data){
                    return mb_substr($data->response, 0, 50, 'utf-8').' ...';
                }
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
                'headerOptions' => ['class' => 'width-150 text-center'],
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотр'),
                        'url' => function ($data) {
                            return Url::toRoute(['view', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('administrate.api-log.view');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' =>  LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>


<?php

use backend\modules\administration\models\QueueUserLog;
use backend\modules\administration\models\search\QueueUserLogSearch;
use common\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use backend\assets\SiteAsset;

/** @var QueueUserLogSearch $modelSearch */
/** @var array $queues */
/** @var array $users */

SiteAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'queue_id')->select2List($queues, [
                'value' => $modelSearch->queue_id,
                'multiple' => false,
                'placeholder' => yii::t('common', 'Все'),
            ])->label(\Yii::t('common', 'Очередь')) ?>
        </div>

        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'operator_id')->select2List($users, [
                'value' => $modelSearch->operator_id,
                'multiple' => false,
                'placeholder' => yii::t('common', 'Все'),
            ])->label(\Yii::t('common', 'Оператор')) ?>
        </div>

        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'user_id')->select2List($users, [
                'value' => $modelSearch->user_id,
                'multiple' => false,
                'placeholder' => yii::t('common', 'Все'),
            ])->label(\Yii::t('common', 'Пользователь')) ?>
        </div>

        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'action')->select2List(
                [
                    '0' => Yii::t('common', 'Все'),
                    QueueUserLog::ACTION_INSERT  => 'INSERT',
                    QueueUserLog::ACTION_DELETE  => 'DELETE',
                ],
                [
                    'value'       => $modelSearch->action,
                    'prompt'      => '-',
                    'placeholder' => yii::t('common', 'Все'),
                ]) ?>
        </div>

        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'created_at')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'presetDropdown' => true,
                'hideInput' => true,
                'pluginOptions' => [
                    'autoUpdateOnInit' => true,
                    'autoUpdateInput' => true,
                    'timePicker' => true,
                    'timePicker24Hour' => true,
                    'timePickerIncrement' => 1,
                    'options' => [
                        'value' => date('Y/m/d 00:00:00') . ' - ' . date('Y/m/d 23:59:59')
                    ],
                    'locale' => [
                        'format' => 'DD/MM/YYYY HH:mm',
                    ]
                ]
            ])->label(yii::t('common', 'Дата изменения')); ?>

        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

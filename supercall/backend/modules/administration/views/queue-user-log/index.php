<?php

use backend\modules\administration\models\QueueUserLog;
use backend\modules\administration\models\search\QueueUserLogSearch;
use backend\modules\queue\models\Queue;
use common\models\User;
use common\widgets\base\Label;
use common\widgets\base\Panel;
use common\helpers\grid\DataProvider;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var QueueUserLogSearch $modelSearch */
/** @var array $queues */
/** @var array $users */

$this->title = Yii::t('common', 'Логи очередей по операторам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Логи очередей по операторам')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
        'queues' => $queues,
        'users' => $users,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с логами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'queue_id',
                'headerOptions' => ['class' => 'width-200'],
                'enableSorting' => false,
                'content' => function($data) {
                    return $data->getQueue()->name;
                }
            ],
            [
                'attribute' => 'operator_id',
                'headerOptions' => ['class' => 'width-200'],
                'enableSorting' => false,
                'content' => function($data) use ($modelSearch) {
                    return $data->getOperator()->username;
                }
            ],
            [
                'attribute' => 'user_id',
                'headerOptions' => ['class' => 'width-150'],
                'enableSorting' => false,
                'content' => function($data) {
                    return $data->getUser()->username;
                }
            ],
            [
                'attribute' => 'action',
                'headerOptions' => ['class' => 'width-150'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->action,
                        'style' => $model->action === QueueUserLog::ACTION_INSERT ? Label::STYLE_SUCCESS : Label::STYLE_DANGER,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y-m-d H:i:s'],
                'enableSorting' => false,
                'headerOptions' => ['class' => 'width-150'],
            ],
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

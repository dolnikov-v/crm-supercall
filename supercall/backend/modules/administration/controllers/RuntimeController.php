<?php
namespace backend\modules\administration\controllers;

use backend\components\web\Controller;
use backend\modules\administration\components\runtime\Log;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class RuntimeController
 * @package backend\modules\administration\controllers
 */
class RuntimeController extends Controller
{
    /**
     * @var array
     */
    protected $aliases = [
        'Backend logs' => '@backend/runtime/logs/app.log',
        'Frontend logs' => '@frontend/runtime/logs/app.log',
        'Api logs' => '@api/runtime/logs/app.log',
        'Console logs' => '@console/runtime/logs/app.log',
    ];

    /**
     * @return string
     */
    public function actionIndex()
    {
        $logs = [];

        foreach ($this->aliases as $name => $alias) {
            $logs[] = new Log([
                'name' => $name,
                'alias' => $alias,
            ]);
        }

        $data['dataProvider'] = new ArrayDataProvider([
            'allModels' => $logs,
            'sort' => [
                'attributes' => [
                    'name',
                    'size' => ['default' => SORT_DESC],
                    'updatedAt' => ['default' => SORT_DESC],
                ],
            ],
            'pagination' => ['pageSize' => 0],
        ]);

        return $this->render('index', $data);
    }

    /**
     * @return string
     */
    public function actionView()
    {
        $name = urldecode(Yii::$app->request->get('name'));
        $stamp = urldecode(Yii::$app->request->get('stamp'));
        $log = $this->find($name, $stamp);

        if (file_exists($log->fileName)) {
            return Yii::$app->response->sendFile($log->fileName, Inflector::slug($log->fileName) . '.log', ['inline' => true]);
        } else {
            Yii::$app->notifier->addNotifierError(Yii::t('common', 'Лог не найден.'));

            return $this->redirect(Url::to('index'));
        }
    }

    /**
     * @return string
     */
    public function actionHistory()
    {
        $name = urldecode(Yii::$app->request->get('name'));
        $log = $this->find($name);

        $logs = [];

        foreach (glob(Log::extractFileName($log->alias, '*')) as $fileName) {
            $logs[] = new Log([
                'name' => $log->name,
                'alias' => $log->alias,
                'fileName' => $fileName,
            ]);
        }

        $logs = array_reverse($logs);

        return $this->render('history', [
            'name' => $log->name,
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $logs,
            ]),
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionArchive()
    {
        $name = urldecode(Yii::$app->request->get('name'));
        $log = $this->find($name);

        if (file_exists($log->fileName) && $log->archive(date('YmdHis'))) {
            return $this->redirect(['history', 'name' => urlencode($log->name)]);
        } else {
            Yii::$app->notifier->addNotifierError(Yii::t('common', 'Лог не найден.'));

            return $this->redirect(Url::to('index'));
        }
    }

    /**
     * @param string $fileName
     * @param null|string $stamp
     * @return Log
     * @throws NotFoundHttpException
     */
    protected function find($fileName, $stamp = null)
    {
        foreach ($this->aliases as $name => $alias) {
            if ($fileName === $name) {
                return new Log([
                    'name' => $name,
                    'alias' => $alias,
                    'fileName' => $stamp ? Log::extractFileName($alias, $stamp) : false,
                ]);
            }
        }

        throw new NotFoundHttpException(Yii::t('common', 'Лог не найден.'));
    }

}

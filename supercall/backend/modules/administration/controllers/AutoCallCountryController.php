<?php

namespace backend\modules\administration\controllers;

use backend\components\web\Controller;
use backend\modules\administration\models\search\AutoCallCountrySearch;
use common\models\AutoCallCountry;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

class AutoCallCountryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new AutoCallCountrySearch();

        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $countries = [];

        foreach (yii::$app->user->countries as $country) {
            $countries[$country->id] = Yii::t('common', $country->name);
        }

        $request = Yii::$app->request;
        $page = $request->get('page');


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'countries' => ['0' => Yii::t('common', 'Все')] + $countries,
            'page' => $page
        ]);
    }


    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new AutoCallCountry();
        }

        if ($model->load(Yii::$app->request->post())) {

            if (!$id && in_array($model->country_id, $model->getCountryId())) {
                yii::$app->notifier->addNotifier(yii::t('common', 'Выбранная страна уже добавлена'));
            } else {
                $isNewRecord = $model->isNewRecord;

                if ($model->save()) {
                    Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Настройка автодозвона страны успешно добавлены.') : Yii::t('common', 'Настройка автодозвона страны успешно отредактирована.'), 'success');

                    return $this->redirect(Url::toRoute('index'));
                } else {
                    Yii::$app->notifier->addNotifierErrorByModel($model);
                }
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }


    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->is_active = 1;

        if ($model->save(true, ['is_active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Настройка автодозвона успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->is_active = 0;

        if ($model->save(true, ['is_active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Настройка автодозвона успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }


    public function actionSorting()
    {
        $sorting = Yii::$app->request->post('sorting');

        $request = Yii::$app->request;
        $pageSize = $request->get('pageSize');
        $page = $request->get('page') ? $request->get('page') : 1;

        foreach ($sorting as $key => $sort) {
            //Устрановка display_position с учетом пегинации
            $next_display_position = ($key + 1) + ($page - 1) * $pageSize;

            Yii::$app->db->createCommand()->update('{{%auto_call_county}}',
                ['display_position' => $next_display_position],
                ['id' => $sort])->execute();
        }

        return true;
    }

    /**
     * @param $id
     * @return AutoCallCountry|null
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = AutoCallCountry::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Настройка автодозвона страны не найдена.'));
        }

        return $model;
    }

}
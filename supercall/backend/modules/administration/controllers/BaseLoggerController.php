<?php

namespace backend\modules\administration\controllers;

use backend\components\web\Controller;
use backend\modules\administration\models\BaseLogger;
use backend\modules\administration\models\search\BaseLoggerSearch;
use common\models\Logger;
use common\models\LoggerTag;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class BaseLoggerController
 * @package backend\modules\administration\controllers
 */
class BaseLoggerController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new BaseLoggerSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'types' => $modelSearch->getTypes(),
            'actions' => $modelSearch->getActions(),
        ]);
    }
}
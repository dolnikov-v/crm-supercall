<?php

namespace backend\modules\administration\controllers;

use backend\components\web\Controller;
use backend\modules\administration\models\search\QueueUserLogSearch;
use backend\modules\queue\models\Queue;
use common\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class QueueUserLogController
 * @package backend\modules\administration\controllers
 */
class QueueUserLogController extends Controller
{
    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $modelSearch = new QueueUserLogSearch();

        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'queues' => ['0' => Yii::t('common','Все')] + ArrayHelper::map(Queue::find()->all(), 'id', 'name'),
            'users' => ['0' => Yii::t('common','Все')] + ArrayHelper::map(User::find()->all(), 'id', 'username')
        ]);
    }
}

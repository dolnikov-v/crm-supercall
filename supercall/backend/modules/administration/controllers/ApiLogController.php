<?php
namespace backend\modules\administration\controllers;

use backend\components\web\Controller;
use backend\modules\administration\models\search\ApiLogSearch;
use backend\modules\administration\models\ApiLog;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Class ApiLogController
 * @package backend\modules\administration\controllers
 */
class ApiLogController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ApiLogSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch
        ]);
    }

    public function actionView()
    {
        $apiLog = ApiLog::find()->where(['id' => yii::$app->request->get('id')]);

        return $this->render('view', [
            'apiLog' => $apiLog->one()
        ]);
    }
}
<?php

namespace backend\modules\administration\controllers;

use backend\components\web\Controller;
use backend\modules\administration\models\search\AutoCallLogSearch;
use common\models\AutoCallLog;
use common\models\Country;
use yii\data\ActiveDataProvider;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class AutoCallLogController
 * @package backend\modules\administration\controllers
 */
class AutoCallLogController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new AutoCallLogSearch();
    
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
        //
        //$query = AutoCallLog::find();
        //
        //$provider = new ActiveDataProvider([
        //    'query' => $query,
        //    'pagination' => [
        //        'pageSize' => 10,
        //    ],
        //    'sort' => [
        //        'defaultOrder' => [
        //            'created_at' => SORT_DESC,
        //        ]
        //    ],
        //]);

        $countries = [];

        foreach (yii::$app->user->countries as $country) {
            $countries[$country->id] = Yii::t('common', $country->name);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'countries' => ['0' => Yii::t('common','Все')] + $countries
        ]);
    }
}
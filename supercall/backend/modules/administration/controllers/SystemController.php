<?php
namespace backend\modules\administration\controllers;

use backend\components\web\Controller;
use Yii;
use yii\helpers\Url;

/**
 * Class SystemController
 * @package backend\modules\administration\controllers
 */
class SystemController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return \yii\web\Response
     */
    public function actionFlushCache()
    {
        $caches = [
            'cache',
            'frontendCache',
            'apiCache',
        ];

        foreach ($caches as $cache) {
            Yii::$app->{$cache}->flush();
        }

        Yii::$app->notifier->addNotifierSuccess(Yii::t('common', 'Cache успешно очищен.'));

        return $this->redirect(Url::toRoute(['index']));
    }

    /**
     * @return \yii\web\Response
     */
    public function actionClearAssets()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            Yii::$app->notifier->addNotifierError(Yii::t('common', 'Пользователи Windows не могут чистить Assets.'));
        } else {
            $assetManagers = [
                'assetManager',
                'frontendAssetManager',
                'apiAssetManager',
            ];

            foreach ($assetManagers as $assetManager) {
                foreach (glob(Yii::$app->{$assetManager}->basePath . DIRECTORY_SEPARATOR . '*') as $asset) {
                    if (is_link($asset)) {
                        unlink($asset);
                    } elseif (is_dir($asset)) {
                        $this->deleteDir($asset);
                    } else {
                        unlink($asset);
                    }
                }
            }

            Yii::$app->notifier->addNotifierSuccess(Yii::t('common', 'Assets успешно очищены.'));
        }

        return $this->redirect(Url::toRoute(['index']));
    }

    /**
     * @param $directory
     * @return boolean
     */
    private function deleteDir($directory)
    {
        $iterator = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }

        return rmdir($directory);
    }
}

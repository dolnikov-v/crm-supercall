<?php
namespace backend\modules\administration\controllers;

use backend\components\web\Controller;
use backend\modules\administration\models\JsLogger;
use backend\modules\administration\models\search\JsLoggerSearch;
use Yii;

class JsLoggerController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new JsLoggerSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch
        ]);
    }

    /**
     * @return string
     */
    public function actionView()
    {
        $jsLogger = JsLogger::find()->where(['id' => yii::$app->request->get('id')]);

        return $this->render('view', [
            'jsLogger' => $jsLogger->one()
        ]);
    }

    /**
     * @return string
     */
    public function actionSaveLog()
    {
        $get = yii::$app->request->get();
        $get['created_at'] = time();
        $get['updated_at'] = time();

        $model = new JsLogger();

        $errors = [];

        if($model->load($get, '')){
            if($model->validate()){
                if(!$model->save()){
                    $errors = $model->getErrors();
                }
            }else{
                $errors = $model->getErrors();
            }
        }else{
            $errors = $model->getErrors();
        }
        /*
        if(!empty($errors)){
            return json_encode($errors, JSON_UNESCAPED_UNICODE);
        }

        return json_encode($get, JSON_UNESCAPED_UNICODE);
        */
    }
}
<?php
namespace backend\modules\administration\controllers;

use backend\components\web\Controller;
use common\components\Amqp;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class StatsController
 * @package backend\modules\administration\controllers
 */
class StatsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $in_progress = is_file(Yii::getAlias('@console/runtime/stats-olap.lock'));

        return $this->render('index', [
            'in_progress' => $in_progress
        ]);
    }

    public function actionRecalc()
    {
        $queue = ArrayHelper::getValue(\Yii::$app->params, 'statsRecalcQueueName', 'stats.recalc');
        /** @var Amqp $amqp */
        $amqp = \Yii::$app->amqp;
        $amqp->declareQueue($queue);
        $amqp->publish_message(json_encode(['qwe' => 'asd']), '', $queue);
        $amqp->closeConnection();
        return $this->redirect(['index']);
    }
}

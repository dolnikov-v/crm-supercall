<?php

namespace backend\modules\administration\controllers;

use backend\components\web\Controller;
use backend\modules\administration\models\search\MonitoringSearch;
use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class MonitoringController
 * @package backend\modules\administration\controllers
 */
class MonitoringController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new MonitoringSearch();
        $userCountry = $this->getUserCountry();
        asort($userCountry);

        return $this->render('index',[
            'modelSearch' => $modelSearch,
            'countries' => $userCountry,
            'operators' => ArrayHelper::map($this->getOperators(), 'id', function($item){
                $sip_data = Json::decode($item->sip);

                return $item->username . ' (' . $sip_data['sip_login'] . ')';
            })
        ]);
    }

    /**
     * @return array
     */
    public function getUserCountry()
    {
        $country = yii::$app->user->getCountries();

        return ArrayHelper::map($country, 'id', function($item){
           return yii::t('common', $item->name);
        });
    }

    /**
     * @return \common\models\UserCountry[]
     */
    public function getOperators()
    {
        $country = array_map(function($v){return ['id' => $v];}, array_keys($this->getUserCountry()));

        return User::getActiveByRole($country, [User::ROLE_SENIOR_OPERATOR, User::ROLE_OPERATOR, User::ROLE_APPLICANT]);
    }
}
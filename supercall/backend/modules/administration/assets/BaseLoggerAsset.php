<?php

namespace backend\modules\administration\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BaseLoggerAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/administration/base-logger';

    public $css = [

    ];

    public $js = [
        'base-logger.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];

    public function init() {
        $this->jsOptions['position'] = View::POS_HEAD;
        parent::init();
    }
}
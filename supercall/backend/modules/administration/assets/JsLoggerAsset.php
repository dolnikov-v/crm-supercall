<?php
namespace backend\modules\administration\assets;

use yii\web\AssetBundle;
use common\components\web\View;

class JsLoggerAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/administration/js-logger';

    public $css = [

    ];

    public $js = [
        'js-logger.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];

    public function init() {
        $this->jsOptions['position'] = View::POS_HEAD;
        parent::init();
    }
}
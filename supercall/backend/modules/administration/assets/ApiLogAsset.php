<?php

namespace backend\modules\administration\assets;

use yii\web\AssetBundle;

class ApiLogAsset extends AssetBundle
{
    public $sourcePath = '';

    public $css = [

    ];

    public $js = [

    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
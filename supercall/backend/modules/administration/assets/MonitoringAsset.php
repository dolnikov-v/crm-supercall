<?php

namespace backend\modules\administration\assets;

use yii\web\AssetBundle;

/**
 * Class MonitoringAsset
 * @package backend\modules\administration\assets
 */
class MonitoringAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/administration/monitoring';

    public $css = [
        'users-online.css'
    ];

    public $js = [
        'filter-users-online.js',
        'users-online.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
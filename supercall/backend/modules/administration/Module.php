<?php
namespace backend\modules\administration;

use backend\components\base\Module as BackendModule;

/**
 * Class Module
 * @package backend\modules\administration
 */
class Module extends BackendModule
{
    public $controllerNamespace = 'backend\modules\administration\controllers';
}

<?php
namespace backend\modules\order\assets;

use yii\web\AssetBundle;

class GroupOperationsAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/order/';

    public $js = [
        'group-operations.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
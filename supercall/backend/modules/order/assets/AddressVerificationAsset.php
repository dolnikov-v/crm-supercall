<?php
namespace backend\modules\order\assets;

use yii\web\AssetBundle;

class AddressVerificationAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/order/address-verification';

    public $js = [
        'address-verification.js',
    ];

    public $css = [
        'address-verification.css'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
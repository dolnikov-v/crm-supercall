<?php

namespace backend\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ExportCallRecordsAsset
 * @package backend\modules\order\assets
 */
class ExportCallRecordsAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/order/export-call-records';

    public $js = [
        'export-call-records.js',
    ];

    public $css = [
        'export-call-records.css',
    ];


    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
<?php

namespace backend\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ExportAsset
 * @package common\modules\order\assets
 */
class ExportAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/order/export';

    public $js = [
        'export.js',
    ];

    public $css = [
        'export.css',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
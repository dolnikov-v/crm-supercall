<?php
namespace backend\modules\order\widgets\groupoperations;

use yii\base\Widget;

class GroupOperationsWidget extends Widget
{
    public function run()
    {
        return $this->render('group-operations');
    }
}
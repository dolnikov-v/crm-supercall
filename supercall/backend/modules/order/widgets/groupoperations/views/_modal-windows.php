<?php
//Файл содержит описание модальных окон групповых операций
use common\modules\order\models\Order;
use common\widgets\base\Select2;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

?>

<?php Modal::begin([
    'id' => 'modal_change_status',
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4>' . yii::t('common', 'Смена статуса') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-success confirm-link', 'id' => 'modal_change_status_ok'])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
]); ?>

<?= Select2::widget([
    'id' => 'status',
    'name' => 'status',
    'items' => Order::getStatusesCollection(),
]); ?>

<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'modal_sent_to_sqs',
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4>' . yii::t('common', 'Отправка в очередь') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-success confirm-link', 'id' => 'modal_sent_to_sqs_ok'])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
]); ?>

<?php Modal::end(); ?>


    <!-- base modal-->

<?php Modal::begin([
    'id' => 'modal_confirm_group_operations',
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4>' . yii::t('common', 'Подтверждение') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true, 'style' => 'z-index:1051'],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-success', 'id' => 'modal_confirm_group_operations_ok'])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-danger', 'data-dismiss' => 'modal'])
]); ?>

    <div class="row">
        <div class="col-lg-12"><?= yii::t('common', 'Вы действительно собираетесь выполнить данную операцию ?') ?></div>
    </div>

<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'modal_warning_group_operations',
    'header' => '<h4>' . yii::t('common', 'Предупреждение') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Закрыть'), ['class' => 'btn btn-danger', 'data-dismiss' => 'modal'])
]); ?>

    <div class="row">
        <div class="col-lg-12"><?= yii::t('common', 'Заказы не выбраны') ?></div>
    </div>

<?php Modal::end(); ?>


<?php Modal::begin([
    'id' => 'modal_progress_group_operations',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4>' . yii::t('common', 'Прогресс') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Закрыть'), ['class' => 'btn btn-info', 'data-dismiss' => 'modal', 'id' => 'close_progress', 'disabled' => true])
]); ?>
    <div class="progress-success">
        <div class="progress-bar progress-bar-indicating  progress-bar-success" role="progressbar" style="width: 0%"
             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%
        </div>
    </div>
    <hr/>
    <div class="final-operaton-text text-center"></div>
    <div class="group_errors text-danger"></div>
    <br/>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'modal_change_queue',
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4>' . yii::t('common', 'Смена очереди') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-success confirm-link', 'id' => 'modal_change_queue_ok', 'disabled' => true])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
]); ?>

    <label for="country_id"><?= yii::t('common', 'Страна') ?></label>
<?= Select2::widget([
    'id' => 'cq_country_id',
    'name' => 'cq_country_id',
    'placeholder' => yii::t('common', 'Все'),
    'multiple' => true,
    'style' => 'flex',
    'items' => ArrayHelper::map(yii::$app->user->getCountries(), 'id', function ($item) {
        return yii::t('common', $item->name);
    }),
]); ?>
    <br/>
    <label for="cq_queue_id">New queue</label>
    <div class="loader"></div>
<?= Select2::widget([
    'id' => 'cq_queue_id',
    'name' => 'cq_queue_id',
    'items' => [],
    'disabled' => true,
    'style' => 'hidden'
]); ?>

<?php Modal::end(); ?>


<?php Modal::begin([
    'id' => 'modal_change_cost_products',
    'header' => '<h4>' . yii::t('common', 'Сменить стоимость товаров в заказе') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-success confirm-link', 'id' => 'modal_change_cost_products_ok'])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
]); ?>
    <div class="loader"></div>
    <div class="alert alert-info" role="alert">
        <?= yii::t('common', 'Чтобы не изменять стоимость товара - оставьте поле с ценой пустым.'); ?>
    </div>
    <div class="alert alert-danger error-no-price" role="alert" style="display: none;">
        <?= yii::t('common', 'Ни у одного товара не назначена стоимость'); ?>
    </div>
    <table class="table table-striped table-hover table-sortable table-responsive">
        <thead>
        <tbody class="product-table-body"></tbody>
    </table>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'modal_sent_to_check_demand',
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4>' . yii::t('common', 'Отправка в очередь Check demand') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-success confirm-link', 'id' => 'modal_sent_to_check_demand'])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
]); ?>

<?php Modal::end(); ?>


<?php Modal::begin([
    'id' => 'modal_resend_to_call',
    'header' => '<h4>' . yii::t('common', 'Отправить заказы на повторный обзвон') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-success confirm-link', 'id' => 'modal_resend_to_call_ok'])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
]); ?>
    <div class="row">
        <div class="alert alert-danger" role="alert">
            <?= yii::t('common', 'Внимание! При повторной отправки заказов на обзвон, данные заказы будут созданы в системе как новые, получат новые номера и будут отправлены в первичную очередь.'); ?>
        </div>
    </div>
    <label for="cq_queue_id"><?= yii::t('common', 'Выбор очереди');?></label>
    <div class="queue_error_message"></div>
    <div class="loader"></div>
    <?= Select2::widget([
        'id' => 'primary_queue_id',
        'name' => 'primary_queue_id',
        'items' => [],
        'disabled' => true,
        'style' => 'hidden'
    ]); ?>

<?php Modal::end(); ?>
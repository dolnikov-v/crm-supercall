<?php

use backend\modules\order\models\GroupOperations;
use common\widgets\base\Button;
use backend\modules\order\assets\GroupOperationsAsset;

GroupOperationsAsset::register($this);

?>

    <div class="row group-operations">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-3">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_INFO . ' width-200 ' . (!yii::$app->user->can('order.groupoperations.changestatus') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Сменить статус'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'change_status_button',
                            'data-action' => 'ChangeStatus',
                            'data-url' => GroupOperations::OPERATION_CHANGE_STATUS
                        ]
                    ]) ?>
                </div>
                <div class="col-lg-4">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_INFO . ' width-250 ' . (!yii::$app->user->can('order.groupoperations.changecostgoods') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Сменить стоимость товаров в заказе'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'change_cost_products_button',
                            'data-action' => 'ChangeCostProducts',
                            'data-url' => GroupOperations::OPERATION_CHANGE_COST_PRODUCTS
                        ]
                    ]) ?>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-3">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_INFO . ' width-200 ' . (!yii::$app->user->can('order.groupoperations.senttosqs') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Отправка в NPay'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'sent_to_sqs_button',
                            'data-action' => 'SentToSqs',
                            'data-url' => GroupOperations::OPERATION_SENT_TO_SQS
                        ]
                    ]) ?>
                </div>
                <div class="col-lg-4">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_WARNING . ' width-250 ' . (!yii::$app->user->can('order.groupoperations.senttocheckdemand') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Отправить в очередь Check demand'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'change_status_button',
                            'data-action' => 'sentToCheckDemand',
                            'data-url' => GroupOperations::OPERATION_SENT_TO_CHECK_DEMAND
                        ]
                    ]) ?>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-3">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_INFO . ' width-200 ' . (!yii::$app->user->can('order.groupoperations.changequeue') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Изменить очередь'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'change_queue_button',
                            'data-action' => 'ChangeQueue',
                            'data-url' => GroupOperations::OPERATION_CHANGE_QUEUE
                        ]
                    ]) ?>
                </div>
                <div class="col-lg-4">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_INFO . ' width-250 ' . (!yii::$app->user->can('order.groupoperations.resendtocall') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Отправить повторно на обзвон'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'resend_to_call_button',
                            'data-action' => 'resendToCall',
                            'data-url' => GroupOperations::OPERATION_RESEND_TO_CALL
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

<?= $this->render('_modal-windows'); ?>
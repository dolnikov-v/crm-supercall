<?php

namespace backend\modules\order\models\query;

use \yii\db\ActiveQuery;

class TranscribeQueueQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function complete()
    {
        return $this->andWhere(['status' => '']);
    }

    /**
     * @param null $db
     * @return array|\yii\db\ActiveRecord[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|null|\yii\db\ActiveRecord
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
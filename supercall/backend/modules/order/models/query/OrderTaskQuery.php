<?php
namespace backend\modules\order\models\query;


use common\components\db\ActiveQuery;
use backend\modules\order\models\OrderTask;

/**
 * class OrderTaskQuery
 * @package backend\modules\catalog\models\query
 */
class OrderTaskQuery extends ActiveQuery
{
    public function getNew()
    {
        return $this->andWhere([OrderTask::tableName() . '.status' => OrderTask::TASK_STATUS_NEW]);
    }

    public function getRejected()
    {
        return $this->andWhere([OrderTask::tableName() . '.status' => OrderTask::TASK_STATUS_REJECTED]);
    }

    public function getAprooved()
    {
        return $this->andWhere([OrderTask::tableName() . '.status' => OrderTask::TASK_STATUS_APPROVED]);
    }

    public function getPended()
    {
        return $this->andWhere([OrderTask::tableName() . '.status' => OrderTask::TASK_STATUS_PENDING]);
    }
}

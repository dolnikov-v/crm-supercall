<?php

namespace backend\modules\order\models;

use backend\modules\order\models\query\TranscribeQueueQuery;
use common\modules\call\models\CallHistory;
use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "transcribe_queue".
 *
 * @property integer $id
 * @property integer $call_history_id
 * @property string $media_file_temp
 * @property string $media_file_s3
 * @property string $job_name
 * @property string $job_params
 * @property string $transcribe_file_s3
 * @property string $transcribe_json
 * @property string $status
 * @property string $error
 *
 * @property CallHistory $callHistory
 */
class TranscribeQueue extends ActiveRecord
{
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_ASTERISK_QUERY_OK = 'asterisk_query_ok';
    const STATUS_ASTERISK_QUERY_ERROR = 'asterisk_query_error';
    const STATUS_SAVE_TEMP_OK = 'save_temp_ok';
    const STATUS_SAVE_TEMP_ERROR = 'save_temp_error';
    const STATUS_SENT_TO_S3_OK = 'sent_to_s3_ok';
    const STATUS_SENT_TO_S3_ERROR = 'sent_to_s3_error';
    const STATUS_CREATE_JOB_OK = 'create_job_ok';
    const STATUS_CREATE_JOB_ERROR = 'create_job_error';
    const STATUS_WAIT_COMPLETE_JOB = 'wait_complete_job';
    const STATUS_GET_TRANSCRIBE_ERROR = 'get_transcribe_error';
    const STATUS_GET_TRANSCRIBE_OK = 'get_transcribe_ok';

    const AWS_TRANSCRIBE_IN_PROGRESS = 'IN_PROGRESS';
    const AWS_TRANSCRIBE_FAIL = 'FAIL';
    const AWS_TRANSCRIBE_COMPLETED = 'COMPLETED';

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'transcribe_queue';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['call_history_id'], 'integer'],
            [['transcribe_json', 'error'], 'string'],
            ['status', 'in', 'range' => array_keys(self::getStatusesCollection())],
            [['media_file_temp', 'media_file_s3', 'job_name', 'job_params', 'transcribe_file_s3', 'status'], 'string', 'max' => 255],
            [['call_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => CallHistory::className(), 'targetAttribute' => ['call_history_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public static function getErrorStatuses()
    {
        return [
            self::STATUS_ASTERISK_QUERY_ERROR,
            self::STATUS_SAVE_TEMP_ERROR,
            self::STATUS_SENT_TO_S3_ERROR,
            self::STATUS_CREATE_JOB_ERROR,
            self::STATUS_GET_TRANSCRIBE_ERROR
        ];
    }

    /**
     * @return array
     */
    public static function getInProgressStatuses()
    {
        return [
            self::STATUS_IN_PROGRESS,
            self::STATUS_ASTERISK_QUERY_OK,
            self::STATUS_SAVE_TEMP_OK,
            self::STATUS_SENT_TO_S3_OK,
            self::STATUS_CREATE_JOB_OK,
            self::STATUS_WAIT_COMPLETE_JOB,

        ];
    }

    /**
     * @return array
     */
    public static function getFinalStatuses()
    {
        return [
            self::STATUS_GET_TRANSCRIBE_OK
        ];
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'call_history_id' => Yii::t('common', 'Call History ID'),
            'media_file_temp' => Yii::t('common', 'Запись разговора в папке Temp'),
            'media_file_s3' => Yii::t('common', 'Запись разговора на S3'),
            'job_name' => Yii::t('common', 'Имя задачи'),
            'job_params' => Yii::t('common', 'Параметры задачи'),
            'transcribe_file_s3' => Yii::t('common', 'Файл транскрибации на S3'),
            'transcribe_json' => Yii::t('common', 'Job json'),
            'status' => Yii::t('common', 'Статус'),
            'error' => Yii::t('common', 'Ошибка'),
        ];
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_IN_PROGRESS => yii::t('common', 'Начало обработки'),
            self::STATUS_ASTERISK_QUERY_OK => yii::t('common', 'Успешный запрос записи с сервера Asterisk'),
            self::STATUS_ASTERISK_QUERY_ERROR => yii::t('common', 'Ошибка запроса записи с сервера Asterisk'),
            self::STATUS_SAVE_TEMP_OK => yii::t('common', 'Сохранение записи на сервере выполнено'),
            self::STATUS_SAVE_TEMP_ERROR => yii::t('common', 'Ошибка сохранение записи на сервере'),
            self::STATUS_SENT_TO_S3_OK => yii::t('common', 'Отправка записи на S3 сервис выполнена'),
            self::STATUS_SENT_TO_S3_ERROR => yii::t('common', 'Ошибка отправка записи на S3 сервис'),
            self::STATUS_CREATE_JOB_OK => yii::t('common', 'Задача на транскрибацию успешно выполнена'),
            self::STATUS_CREATE_JOB_ERROR => yii::t('common', 'Ошибка создание задачи для транскрибации'),
            self::STATUS_WAIT_COMPLETE_JOB => yii::t('common', 'Ожидание готовности задачи'),
            self::STATUS_GET_TRANSCRIBE_OK => yii::t('common', 'Успешное получение транскрибации, идет обработка'),
            self::STATUS_GET_TRANSCRIBE_ERROR => yii::t('common', 'Ошибка получения транскрибации'),
        ];
    }

    /**
     * @param $status
     * @return mixed|string
     */
    public static function getNameStatus($status)
    {
        $collection = self::getStatusesCollection();

        return $collection[$status] ?? yii::t('common', 'Статус не определён');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallHistory()
    {
        return $this->hasOne(CallHistory::className(), ['id' => 'call_history_id']);
    }

    /**
     * @inheritdoc
     * @return TranscribeQueueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TranscribeQueueQuery(get_called_class());
    }

    /**
     * @param $id
     * @return null|TranscribeQueue
     */
    public static function findByCallHistory($id)
    {
        return self::findOne([
            'call_history_id' => $id
        ]);
    }
}
<?php

namespace backend\modules\order\models;

use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;


class OrderBackup extends Order
{
    const ACTION_USE_ORIGIN_ORDER = 'use_origin';
    const ACTION_DROP_BACKUP = 'drop_backup'; //экшн должен быть отправлен обратно до клиента - чтобы там очистить localStorage по данному ордеру
    const ACTION_USE_BACKUP = 'use_backup';

    const ORDER_BACKUP = 'OrderBackup';
    const ORDER_PRODUCT_BACKUP = 'OrderProductBackup';
    const ORDER_USER_BACKUP = 'OrderUserBackup';

    public $shipping_id;
    public $last_queue_id;
    public $attempts;
    public $finished;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_backup}}';
    }

    public function rules()
    {
        $fields = [
            [
                [
                    'order_id',
                    'user_id',
                    'form_id',
                    'status',
                ]
                , 'integer'
            ],
            [
                [
                    'customer_address',
                    'delivery_from',
                    'delivery_to',
                    'comment',
                    'customer_phone',
                    'customer_mobile',
                    'customer_full_name'
                ]
                , 'string'
            ],
            [['phones'], 'safe']
        ];
        return array_merge($fields, parent::rules());
    }

    /**
     * Не нужны не транзакции, ни проверки. Пользователю эта информация не нужна вообще.
     * @param OrderBackup $orderBackup
     */
    public static function dropBackup(OrderBackup $orderBackup, $user_id)
    {

        $orderProductBackup = OrderProductBackup::findAll([
            'order_id' => $orderBackup->order_id
        ]);

        if (is_array($orderProductBackup)) {
            foreach ($orderProductBackup as $k => $model) {
                $model->delete();
            }
        }

        $orderUserBackup = OrderUserBackup::findOne([
            'order_id' => $orderBackup->order_id
        ]);

        $orderUserBackup->delete();
        $orderBackup->delete();
    }

    /**
     * Если backup не смогли записать в бд, но в сессию положить успели
     */
    public function getFromSessionBackup($user_id)
    {
        $backups = Yii::$app->cache->get(self::getKeyUser_id($user_id));

        if (is_array($backups)) {
            foreach ($backups as $k => $json) {
                $backup = json_decode($json, 1);
                self::saveOrderDataBackup(true, $backup);
            }
        }

    }

    /**
     * Проверить наличие бекапа для данного пользователя
     *
     * данный метод используется в frontend/modules/order/controllers/ChangeController - useRequestByApiUrl()
     * - для определения url по которому должен грузиться заказ
     *
     * @param $user_id
     * @return array
     */
    public static function getOrderBackupData($user_id)
    {
        //если у текущего пользователя есть бекапы, которые не записаны в бд.
        self::getFromSessionBackup($user_id);

        //Наличие записи в order_backup с update более поздним, чем у order
        //(если в order есть этот заказ с более поздним update_at - значит найденный backup не нужен уже. удалить его отовсюду)
        //наличие в сессии
        //наличие в localStorage

        $orderBackupQuery = OrderBackup::find()->where([
            'user_id' => $user_id
        ]);

        //backup Найден для этого юзера
        if ($orderBackupQuery->exists()) {
            $orderBackup = $orderBackupQuery->one();

            //ищем ордер и сравниваем дату обновления
            $order = Order::findOne(['id' => $orderBackup->order_id]);
            //backup бал сделан позднее обновления оригинального заказа
            if ($orderBackup->updated_at > $order->updated_at) {
                return [
                    'success' => true,
                    'action' => OrderBackup::ACTION_USE_BACKUP,
                    'orderBackup' => $orderBackup
                ];
            } //ордер обновлён позднее чем backup - backup Больше не нужен по данному ордеру
            else {
                OrderBackup::dropBackup($orderBackup, yii::$app->user->id);

                self::dropFromSession($user_id, $orderBackup->order_id);

                return [
                    'success' => true,
                    'action' => OrderBackup::ACTION_DROP_BACKUP
                ];
            }
            //backup не найден - работает в обычном порядке
        } else {
            return [
                'success' => true,
                'action' => OrderBackup::ACTION_USE_ORIGIN_ORDER
            ];
        }

    }

    /**
     * Полный набор backup
     * @param $order_id
     * @param $user_id
     * @return array
     */
    public function getOldBackupData($order_id)
    {
        //нам нужно знать ids всех backup по данному заказу - в случае сохранения нового заказа - грохнуть все не актуальное
        $oldOrderBackup = OrderBackup::findAll(['order_id' => $order_id]);
        $oldOrderProductBackup = OrderProductBackup::findAll(['order_id' => $order_id]);
        $oldOrderUserBackup = OrderUserBackup::findAll(['order_id' => $order_id]);

        if (is_array($oldOrderBackup)) {
            $oldOrderBackupIds = ArrayHelper::getColumn($oldOrderBackup, 'id');
        }

        if (is_array($oldOrderProductBackup)) {
            $oldOrderProductBackup = ArrayHelper::getColumn($oldOrderProductBackup, 'id');
        }

        if (is_array($oldOrderUserBackup)) {
            $oldOrderUserBackup = ArrayHelper::getColumn($oldOrderUserBackup, 'id');
        }

        return [
            self::ORDER_BACKUP => isset($oldOrderBackupIds) ? $oldOrderBackupIds : [],
            self::ORDER_PRODUCT_BACKUP => isset($oldOrderProductBackup) ? $oldOrderProductBackup : [],
            self::ORDER_USER_BACKUP => isset($oldOrderUserBackup) ? $oldOrderUserBackup : []
        ];
    }

    /**
     * @return string
     */
    public function saveOrderDataBackup($fromSession = false, $sessionData = null)
    {
        if(!yii::$app->params['useOrderBackup']){
            return json_encode([
                'success' => true,
                'data' => null,
                'error' => null
            ], JSON_UNESCAPED_UNICODE);
        }

        if(!$fromSession) {

            $user_id = yii::$app->request->post('user_id');
            $order_id = yii::$app->request->post('order_id');

            $oldDataBackup = $this->getOldBackupData($order_id, $user_id);

            $backupData = [
                'order_id' => $order_id,
                'url' => yii::$app->request->post('url'),
                'data' => yii::$app->request->post('data'),
                'user_id' => $user_id,
            ];
        }else{
            $user_id = $sessionData['user_id'];
            $order_id = $sessionData['order_id'];



            $backupData = [
                'order_id' => $order_id,
                'url' => $sessionData['url'],
                'data' => $sessionData['data'],
                'user_id' => $user_id,
            ];
        }

        if (is_null($order_id)) {
            return json_encode(['success' => false, 'error' => 'empty order_id']);
        }

        //данные, которые вбил оператор, необходимо сохранить
        if(!$fromSession) {
            self::addToSession($backupData['user_id'], $backupData['order_id'], $backupData);
        }

        $orderModel = Order::findOne(['id' => $backupData['order_id']]);

        if (is_null($orderModel)) {
            return json_encode(['success' => false, 'error' => 'empty original order']);
        }

        $data = [];

        parse_str($backupData['data'], $data);

        $errors = false;

        if (!in_array(null, array_values($backupData))) {
            //Мы можем работать как с Order так и с OrderBackup и т.д
            $usedModelOrder = isset($data['OrderBackup']) ? 'OrderBackup' : 'Order';
            $usedModelOrderProduct = isset($data['OrderProductBackup']) ? 'OrderProductBackup' : 'OrderProduct';

            if (!isset($data[$usedModelOrder])) {
                return json_encode(['success' => false, 'error' => 'empty ' . $usedModelOrder]);
            }

            if (!isset($data[$usedModelOrderProduct])) {
                return json_encode(['success' => false, 'error' => 'empty ' . $usedModelOrderProduct]);
            }

            //Пробуем записать backup
            $transaction = yii::$app->db->beginTransaction();

            if($fromSession){
                $oldDataBackup = self::getOldBackupData($order_id);
            }
            else {
                $oldDataBackup = $this->getOldBackupData($order_id);
            }

            $orderBackup = new OrderBackup();
            $orderBackup->is_backup = true;
            $data[$usedModelOrder]['order_id'] = $backupData['order_id'];
            $data[$usedModelOrder]['country_id'] = $orderModel->country_id;
            $data[$usedModelOrder]['partner_id'] = $orderModel->partner_id;
            $data[$usedModelOrder]['foreign_id'] = $orderModel->foreign_id;
            $data[$usedModelOrder]['ordered_at'] = $orderModel->ordered_at;
            $data[$usedModelOrder]['source_uri'] = $orderModel->source_uri;
            $data[$usedModelOrder]['form_id'] = $orderModel->form_id;
            $data[$usedModelOrder]['user_id'] = $user_id;
            $data[$usedModelOrder]['status'] = (int)$orderModel->status;

            //если мы работаем с моделью бекапа - нужно все передать ей от шаблонной формы
            if ($usedModelOrder == 'OrderBackup') {
                foreach ($data['Order'] as $k => $val) {
                    $data[$usedModelOrder][$k] = $val;
                }

                if (isset($data[$usedModelOrder]['general'])) {
                    foreach ($data[$usedModelOrder]['general'] as $k => $v) {
                        $data[$usedModelOrder][$k] = $v;
                    }
                }
            }

            if (!isset($data[self::ORDER_BACKUP])) {
                $data[self::ORDER_BACKUP] = $data['Order'];
            }

            if ($orderBackup->load($data, self::ORDER_BACKUP)) {
                if ($orderBackup->save(false)) {
                    foreach ($data[$usedModelOrderProduct] as $k => $orderProduct) {
                        if (empty($orderProduct['product_id'])) {
                            continue;
                        }

                        $productsBackup = new OrderProductBackup();
                        $productsBackup->is_backup = true;
                        $orderProduct['order_id'] = $orderModel->id;

                        if (!$productsBackup->load($orderProduct, '')) {
                            $transaction->rollBack();
                            return json_encode(['success' => false, 'error' => 'not load order_product']);
                        } else {
                            if (!$productsBackup->save(false)) {
                                $transaction->rollBack();
                                return json_encode(['success' => false, 'error' => 'not save order_product']);
                            }
                        }
                    }
                } else {
                    $transaction->rollBack();
                    return json_encode(['success' => false, 'error' => 'not save order']);
                }
            } else {
                $transaction->rollBack();
                $orderBackup->validate();
                return json_encode(['success' => false, 'error' => 'not load order', 'message' => json_encode($orderBackup->getErrors(), JSON_UNESCAPED_UNICODE)]);
            }

            if (!$errors) {
                $orderUserBackup = new OrderUserBackup();
                $orderUserBackup->order_id = yii::$app->request->post('order_id');
                $orderUserBackup->user_id = yii::$app->request->post('user_id');
                $orderUserBackup->url = yii::$app->request->post('url');
                $orderUserBackup->created_at = time();
                $orderUserBackup->updated_at = time();

                if (!$orderUserBackup->save()) {
                    return json_encode(['success' => false, 'error' => 'not save order_user']);
                } else {
                    $transaction->commit();
                    //больше бекап заказа в сессии не нужен, он записан в таблицу бакапа
                    OrderBackup::dropFromSession($orderUserBackup->user_id, $orderBackup->order_id);
                    //удалим из базы все старые бекапы этого ордера у этого пользователя

                    foreach ($oldDataBackup as $table => $data) {
                        self::dropWithTableBackup($table, $data);
                    }
                }
            }

        }

        return json_encode([
            'success' => !$errors,
            'order_backup_id' => isset($orderBackup) ? $orderBackup->id : null,
            'data' => $backupData['data'],
            'oldDataBackup' => isset($oldDataBackup) ? $oldDataBackup : null,
            'error' => null
        ], JSON_UNESCAPED_UNICODE);
    }

    /**
     *  Удаление не нужного бекапа
     * @param $table
     * @param $data
     */
    public static function dropWithTableBackup($table, $data)
    {
        switch ($table) {
            case self::ORDER_BACKUP :
                OrderBackup::deleteAll(['in', 'id', $data]);
                break;
            case self::ORDER_PRODUCT_BACKUP :
                OrderProductBackup::deleteAll(['in', 'id', $data]);
                break;
            case self::ORDER_USER_BACKUP :
                OrderUserBackup::deleteAll(['in', 'id', $data]);
                break;
        }

    }

    /**
     * Заказ получен по ссылке в которой есть backup, но в процессе попыток сохранения - бекап может быть перезаписан
     *  поэтому - если бекап переписан и для его определения - нужны order_id и user_id
     * @param $order_backup_id
     * @param $user_id
     * @param $order_id
     * @return bool|Order
     */
    public static function getBackupById($order_backup_id, $user_id, $order_id)
    {
        //из урла id прикходит как 111934-956f8434695c8247017bb3a0279c665c8f3d7c86
        $order_temp = explode("-", $order_id);
        $order_id = $order_temp[0];

        $orderBackup = OrderBackup::find()
            ->where([
                OrderBackup::tableName() . '.id' => $order_backup_id
            ])->joinWith('order_product_backup');

        if ($orderBackup->exists()) {
            $order = $orderBackup->one();
            $order->orderProducts = $order->order_product_backup;
            //$order->id = $order->order_id;
            return $order;
        } else {
            $orderBackup = OrderBackup::find()
                ->where([
                    'order_id' => $order_id,
                    'user_id' => $user_id
                ])
                ->orderBy(['id' => SORT_DESC]);

            if ($orderBackup->exists()) {
                //$orderBackup->id = $order_id;
                return $orderBackup->one();
            }
        }

        return false;
    }

    public function getorder_product_backup()
    {
        return $this->hasMany(OrderProductBackup::className(), ['order_id' => 'order_id']);
    }

    public function beforeValidate()
    {
        return true;
    }

    public function load($data, $formName = null)
    {

        $result = parent::load($data, $formName);
        $this->status = (int)$this->status; // хак

        if ($this->delivery_from) {
            $timeZone = new \DateTimeZone(Yii::$app->user->timezone->timezone_id);
            $dateTime = new \DateTime($this->delivery_from, $timeZone);
            $this->delivery_from = $dateTime->getTimestamp();
        }

        if ($this->delivery_to) {
            $timeZone = new \DateTimeZone(Yii::$app->user->timezone->timezone_id);
            $dateTime = new \DateTime($this->delivery_to, $timeZone);
            $this->delivery_to = $dateTime->getTimestamp();
        }

        if ($this->recall_date) {
            $recall_date = \DateTime::createFromFormat('d/m/Y H:i:s', $data['OrderBackup']['recall_date']);
            if ($recall_date) {
                $this->recall_date = $recall_date->format('Y-m-d H:i:s');
            } else {
                $this->recall_date = null;
            }
        }

        $this->order_phones = implode(', ', $this->phones);

        if ($this->getGeneralModel()) {
            $this->getGeneralModel()->load(ArrayHelper::getValue($data, 'Order.general'), '');
            $general_attributes = $this->getGeneralModel()->attributes;


            unset($this->comment);

            //обновить комменты по необходимости
            if(isset($data['OrderBackup']['comment'])) {
                $this->comment = $data['OrderBackup']['comment'];

                $comments = [
                    [
                        'author' => $this->user_id,
                        'text' => $this->comment
                    ]
                ];

                $customer_components = json_decode($this->customer_components, 1);


                $old_comments = isset($customer_components['comments']) ? $customer_components['comments'] : null;

                if ($old_comments) {
                    $comments = array_merge($comments, $old_comments);
                }

                $customer_components['comments'] = $comments;

                $this->customer_components = json_encode($customer_components, JSON_UNESCAPED_UNICODE);
            }

            $this->customer_components = Json::encode(ArrayHelper::merge(
                Json::decode($this->customer_components, true),
                ['general' => $general_attributes]
            ));
            foreach (PartnerForm::configurableAttributes() as $name) {
                $this->{$name} = ArrayHelper::getValue($general_attributes, $name);
            }
        }
        $customer_address_delivery = [];
        if ($this->getAddressModel()) {
            $customer_address = ArrayHelper::getValue($data, 'Order.address');

            if (is_numeric($this->partner_id) && is_numeric($this->country_id)) {

                $partnerForm = PartnerForm::find()->where(['partner_id' => $this->partner_id, 'country_id' => $this->country_id])->asArray()->one();
                $form_id = ArrayHelper::getColumn($partnerForm, 'id');

                if (isset($partnerForm['id']) && !empty($customer_address)) {
                    $attributes = PartnerFormAttribute::getFormAttributes($form_id);

                    foreach ($attributes as $attribute) {
                        if ($attribute->is_coordinate == 1) {
                            foreach ($customer_address as $column => $value) {
                                if ($attribute->name == $column) {
                                    if (!empty($value) && !in_array($value, $customer_address_delivery)) {
                                        $customer_address_delivery[] = $value;
                                    }
                                }
                            }
                        }
                    }
                    //признака кородинаты у индекса нет, но в строке он нужен
                    if (isset($customer_address['zip'])) {
                        $customer_address_delivery[] = $customer_address['zip'];
                    }
                }
            }
            $this->getAddressModel()->load($customer_address, '');
            $this->customer_components = Json::encode(ArrayHelper::merge(
                Json::decode($this->customer_components, true),
                [
                    'address' => $this->getAddressModel()->attributes + ['customer_address' => implode(", ", $customer_address_delivery)]
                ]
            ));
        }

        return $result;
    }

    /**
     * Сессия принимает в качестве ключа integer, но апач отдаёт 500 статус. нонсенс.
     * @param $user_id
     * @return string
     */
    public static function getKeyUser_id($user_id)
    {
        return 'user_'.$user_id;
    }

    /**
     * По факту используется компонент CacheSession
     * @param $user_id
     * @param $order_id
     * @param $data
     */
    public static function addToSession($user_id, $order_id, $data)
    {
        $user_id = self::getKeyUser_id($user_id);

        if(!is_array(yii::$app->cache->get($user_id))){
            Yii::$app->cache->set($user_id, []);
        }

        Yii::$app->cache->set($user_id, array_merge( Yii::$app->cache->get($user_id), [$order_id => json_encode($data, JSON_UNESCAPED_UNICODE)]));
    }

    /**
     * @param $user_id
     * @param $order_id
     */
    public static function dropFromSession($user_id, $order_id)
    {
        yii::$app->cache->delete($user_id);
    }

    /**
     * @param $user_id
     * @param $order_id
     * @return bool|int|string
     */
    public static function checkFromSession($user_id, $order_id)
    {
        $sessionUserBackup = yii::$app->cache->get(self::getKeyUser_id($user_id));

        if (!$sessionUserBackup || !is_array($sessionUserBackup)) {
            return false;
        } else {
            foreach ($sessionUserBackup as $k => $json) {
                $data = json_decode($json, 1);

                if ($data['order_id'] == $order_id && $data['user_id'] == $user_id) {
                    return $k;
                }
            }
        }

        return false;
    }

    /**
     * Т.к. все формы заточены под модели Order и OrderProducts - метод портирования бекапа в ордер
     * @param $orderBackup
     * @return Order $order
     */
    public static function backupTransferToOrder($orderBackup)
    {
        $order = Order::findOne(['id' => $orderBackup->order_id]);
        $data = $orderBackup->getAttributes();

        $clearedData = [];

        foreach($data as $k=>$v){
            if(!is_null($v) && $k != 'id'){

                $clearedData[$k] = $v;
            }
        }

        $order->setAttributes($clearedData, false);
        $order->phones = array_filter(explode(', ', $order->order_phones));
        $orderProductBackup = $orderBackup->order_product_backup;
        $orderProducts = [];

        foreach($orderProductBackup as $k => $product){
            $productAttributes = $product->getAttributes();

            $orderProducts[] = new OrderProduct($productAttributes);
        }

        $order->orderProducts = $orderProducts;
        $order->setAttributes(['is_backup' => true]);
        return $order;
    }

    /**
     * При поиске заказа оператору в очередях - после того как найден определённый заказ - нужно проверить - есть ли у этого заказа бекап.
     * Если бекап есть - нам не важно что зав пользователь с ним работал, данный заказ уже определён для текущего пользователя
     * Обновим пользователя бекапа и вернём ему бекап.
     * Если бекапа нет - то отдадим сквозным способом - текущий ордер
     * @param Order $order
     * @return Order
     */
    public static function getBackupByOrder(Order $order)
    {
        if(yii::$app->params['useOrderBackup'] === true) {

            $orderBackupQuery = OrderBackup::find()->where(['order_id' => $order->id]);

            if ($orderBackupQuery->exists()) {
                $orderBackup = $orderBackupQuery->one();
                $orderBackup->user_id = yii::$app->user->id;
                $orderBackup->save(false,  ['user_id']);

                $orderUserBackupQuery = OrderUserBackup::find()->where(['order_id' => $order->id]);
                if ($orderUserBackupQuery->exists() && !is_null(yii::$app->user->id)) {
                    $orderUserBackup = $orderUserBackupQuery->one();
                    $orderUserBackup->user_id = yii::$app->user->id;
                    $orderUserBackup->save(false, ['user_id']);
                }
                return $orderBackup;
            }else{
                return $order;
            }
        }

        return $order;
    }

}
<?php
namespace backend\modules\order\models;

use common\components\base\FormModel;
use common\components\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use  backend\modules\order\controllers\OrderTaskController;
use yii;

/**
 * Class OrderTaskCustomer
 * @package backend\modules\order\model
 * @property string $customer_first_name
 * @property string $customer_last_name
 * @property string $customer_middle_name
 * @property string $customer_phone
 * @property string $customer_mobile
 * @property integer $form_id
 * @property PartnerForm $form
 */
class OrderTaskCustomer extends OrderTask
{
    public $order_id;
    public $customer_components;
    public $form_id;
    public $form;
    public $_extra_model;

    public $customer_first_name;
    public $customer_last_name;
    public $customer_middle_name;
    public $customer_phone;
    public $customer_mobile;
    public $customer_email;

    public function getOrderTask()
    {
        $OrderTaskController = new OrderTaskController('order-task', yii::$app->getModule('order'));
        return $OrderTaskController->getLastTask($this->order_id, array_keys(OrderTask::getTaskStatuses()));
    }

    /**
     * @return FormModel
     */
    public function getGeneralModel()
    {
        $orderTask = $this->getOrderTask();
        $orderTaskData = Json::decode($orderTask->order_new);
        $customer_components = Json::decode($orderTaskData['customer_components'], true);
        $general = ArrayHelper::getValue($customer_components, 'general', []);
        $form = PartnerForm::find()->where(['country_id' => $orderTaskData['country_id']])->one();

        if ($form) {
            $this->form = $form;
            $this->form_id = $form->id;
        }

        if (!$this->_extra_model) {
            $model = new FormModel();
            $model->name = $this->formName() . '[general]';
            if ($this->form_id) {
                /** @var PartnerFormAttribute $attr */
                foreach ($this->form->generalAttrs as $attr) {
                    $value = ArrayHelper::getValue($general, $attr->name);
                    $attr->addToModel($model, $value);
                }
            }
            $this->_extra_model = $model;
        }

        return $this->_extra_model;
    }
}
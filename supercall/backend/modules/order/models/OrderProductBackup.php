<?php

namespace backend\modules\order\models;
use common\modules\order\models\OrderProduct;

class OrderProductBackup extends OrderProduct
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_product_backup}}';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderBackup()
    {
        return $this->hasOne(OrderBackup::className(), ['order_id' => 'order_id']);
    }
}
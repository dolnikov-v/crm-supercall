<?php
namespace backend\modules\order\models;
use common\components\db\ActiveRecord;

class OrderUserBackup extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_user_backup}}';
    }

    public function rules()
    {
        return [
            [['order_id', 'user_id', 'url'], 'required'],
            [['url'], 'string'],
            [['order_id', 'user_id', 'created_at', 'updated_at'], 'integer']
        ];
    }
}
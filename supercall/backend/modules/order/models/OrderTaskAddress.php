<?php

namespace backend\modules\order\models;


use common\components\base\FormModel;
use common\components\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use  backend\modules\order\controllers\OrderTaskController;
use yii;

/**
 * Class OrderTaskAddress
 * @package backend\modules\order\model
 * @property integer $form_id
 * @property PartnerForm $form
 */
class OrderTaskAddress extends OrderTask
{
    public $order_id;
    public $customer_components;
    public $form_id;
    public $form;
    public $_address_model;

    public function getOrderTask()
    {
        $OrderTaskController = new OrderTaskController('order-task', yii::$app->getModule('order'));
        return $OrderTaskController->getLastTask($this->order_id, array_keys(OrderTask::getTaskStatuses()));
    }

    /**
     * @return FormModel
     */
    public function getAddressModel()
    {
        $orderTask = $this->getOrderTask();
        $orderTaskData = Json::decode($orderTask->order_new);
        $customer_components = Json::decode($orderTaskData['customer_components'], true);
        $address = ArrayHelper::getValue($customer_components, 'address', []);
        $form = PartnerForm::find()->where(['country_id' => $orderTaskData['country_id']])->one();

        if ($form) {
            $this->form = $form;
            $this->form_id = $form->id;
        }

        if (!$this->_address_model) {
            $model = new FormModel();
            $model->name = $this->formName() . '[address]';
            if ($this->form_id) {
                /** @var PartnerFormAttribute $attr */
                foreach ($this->form->addressAttrs as $attr) {
                    $value = ArrayHelper::getValue($address, $attr->name);
                    $attr->addToModel($model, $value);
                }
            }
            $this->_address_model = $model;
        }

        return $this->_address_model;
    }
}
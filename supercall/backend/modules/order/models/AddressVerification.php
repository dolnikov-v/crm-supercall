<?php

namespace backend\modules\order\models;

use backend\modules\catalog\models\KladerCountries;
use common\modules\order\models\Order;
use common\modules\partner\models\PartnerFormAttribute;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class AddressVerification
 * @package backend\modules\order\models
 */
class AddressVerification extends Order
{
    const ADDRESS_CONFIRM_DEFAULT = 0;
    const ADDRESS_NEED_CONFIRM = 1;
    const ADDRESS_CONFIRMED = 2;
    const ADDRESS_NOT_CONFIRMED = 3;

    const HOLD_TO = 3600;


    /**
     * @var integer
     */
    public $cell_need_confirm;
    public $cell_confirmed;
    public $cell_not_confirmed;
    public $total;
    public $cell_need_confirm_percent;
    public $cell_confirmed_percent;
    public $cell_not_confirmed_percent;

    public $status_quantity_1;
    public $status_quantity_2;
    public $status_quantity_3;
    public $status_quantity_4;
    public $status_quantity_5;
    public $status_quantity_6;
    public $status_quantity_7;
    public $status_quantity_8;


    /**
     * @return array
     */
    public static function addressVerificationState()
    {
        return [
            self::ADDRESS_NEED_CONFIRM,
            self::ADDRESS_CONFIRMED,
            self::ADDRESS_NOT_CONFIRMED
        ];
    }

    /**
     * @return array
     */
    public static function addressVerificationStateCollection()
    {
        return [
            self::ADDRESS_NEED_CONFIRM => yii::t('common', 'Нуждается в проверке'),
            self::ADDRESS_CONFIRMED => yii::t('common', 'Подтверждён'),
            self::ADDRESS_NOT_CONFIRMED => yii::t('common', 'Неподтверждён'),
        ];
    }

    /**
     * @param $code
     * @return mixed|string
     */
    public static function getStateByCode($code)
    {
        $stateCollection = self::addressVerificationStateCollection();

        return isset($stateCollection[$code]) ? $stateCollection[$code] : '';
    }
}
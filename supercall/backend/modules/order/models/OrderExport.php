<?php

namespace backend\modules\order\models;

use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\search\OrderSearch;
use Yii;
use juniq\helper\Time;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * https://packagist.org/packages/phpnt/yii2-export
 * Class OrderExport
 * @package backend\modules\order\models
 */
class OrderExport extends OrderSearch
{
    public static function getDinamicFields()
    {
        return [
            'date_first_call',
            'time_getting_order_crm'
        ];
    }

    /**
     * @return array
     */
    public static function columnMapNames()
    {
        return [
            'delivery_to' => yii::t('common', 'Доставка с'),
            'delivery_from' => yii::t('common', 'Доставка по')
        ];
    }


    /**
     * Отмеченные по дефолту - колонки списка заказов при экспорте
     * @return array
     */
    public static function getSelectedExportColumns()
    {
        return [
            'id',
            'status',
            'country_id'
        ];
    }

    /**
     * @param Order $model
     * @param $field
     * @return mixed
     */
    public static function getDinamicField($model, $field)
    {
        return is_array($model) ? $model[$field] : $model->$field;
    }

    /**
     * Settings for export Order-list
     * List exported columns
     * @return array
     */
    public static function getDefaultExportList()
    {
        return [
            'sub_status',
            'customer_full_name',
            'customer_phone',
            'customer_mobile',
            'customer_address',
            'delivery_from',
            'delivery_to',
            'customer_ip',
            'comment' => function ($model) {
                return Html::decode($model->getLastComment());
            },
            'init_price',
            'final_price',
            'source_uri',
            'attempts',
            'id',
            'type_id' => function ($model) {
                return $model->type->name;
            },
            'status' => function ($model) {
                $statuses = Order::getStatusesCollection();

                return isset($statuses[$model->status]) ? $statuses[$model->status] : 'unknown';
            },
            'partner_id' => function ($model) {
                return $model->partner->name;
            },
            'foreign_id',
            'country_id' => function ($model) {
                return $model->country->name;
            },
            'shipping_id' => function ($model) {
                return ($model->shipping) ? $model->shipping->name : '';
            },
            'created_at' => function ($model) {
                return $model->created_at ? yii::$app->formatter->asDateFullTime($model->created_at) : '';
            },
            'updated_at' => function ($model) {
                return $model->updated_at ? yii::$app->formatter->asDateFullTime($model->updated_at) : '';
            },
            'ordered_at' => function ($model) {
                return $model->ordered_at ? yii::$app->formatter->asDateFullTime($model->ordered_at) : '';
            },
            'recall_date' => function ($model) {
                return $model->recall_date ? yii::$app->formatter->asDateFullTime($model->recall_date) : '';
            },
            'last_queue_id' => function ($model) {
                return $model->last_queue_id ? $model->lastQueue->name : '';
            },
            'productString' => function ($model) {
                $products = [];

                /** @var OrderProduct $product */
                /** @var Order $model */
                foreach (ArrayHelper::merge($model->orderProducts, $model->orderPromoProducts) as $product) {
                    $products[] = $product->product->name . ' (' . $product->quantity . ') ' . $product->price;
                }

                return implode(PHP_EOL, $products);
            },
            'date_first_call' => function ($model) {
                $time = floor($model->date_first_call / 60 );
                return (string)$time;
                // return strtr(Time::formatDuration($model->date_first_call, 'short', false, ','),['m'=>'month', 'h' => 'hour','i' => 'minutes', 's' => 'seconds']);
            },
            'time_getting_order_crm' => function ($model) {
                $time = floor($model->time_getting_order_crm / 60);
                return $time;
            },
            'shipping_price',
            'date_first_call_on_crm' => function ($model) {
                $time = floor($model->date_first_call_on_crm / 60);
                return (string)$time;
                // return strtr(Time::formatDuration($model->date_first_call, 'short', false, ','),['m'=>'month', 'h' => 'hour','i' => 'minutes', 's' => 'seconds']);
            },
        ];
    }
}
<?php
namespace backend\modules\order\models;

use backend\modules\order\controllers\OrderTaskController;
use Yii;
use api\models\ApiLog;
use common\components\db\ActiveRecord;
use common\modules\order\models\Order;
use backend\modules\order\models\query\OrderTaskQuery;
use \yii\helpers\ArrayHelper;

/**
 * @property integer $id
 * @property integer $order_id
 * @property string $order_old jsonb
 * @property string $order_new jsonb
 * @property string $product_old jsonb
 * @property string $product_new jsonb
 * @property integer $status
 * @property integer $operator_id
 * @property integer $curator_id
 * @property string $curator_username
 * @property integer $created_at
 * @property integer $updated_at
 */
class OrderTask extends ActiveRecord
{
    public $log;
    public $foreign_id;

    public $delivery_from;
    public $delivery_to;

    const TASK_STATUS_NEW = 1;
    const TASK_STATUS_REJECTED = 2;
    const TASK_STATUS_APPROVED = 3;
    const TASK_STATUS_NOT_ACTUAL = 4;
    const TASK_STATUS_PENDING = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_task}}';
    }

    /**
     * @return array
     */
    public static function getTaskStatuses()
    {
        return [
            self::TASK_STATUS_NEW => yii::t('common', 'Новый'),
            self::TASK_STATUS_REJECTED => yii::t('common', 'Отказ'),
            self::TASK_STATUS_APPROVED => yii::t('common', 'Аппрув'),
            self::TASK_STATUS_NOT_ACTUAL => yii::t('common', 'Неактуален'),
            self::TASK_STATUS_PENDING => yii::t('common', 'Ожидает отправку'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'operator'], 'required'],
            [['order_id', 'status', 'operator', 'curator', 'created_at', 'updated_at'], 'integer'],
            [['order_old', 'order_new', 'product_old', 'product_new', 'curator_username'], 'string'],
            ['status', 'in', 'range' => array_keys($this->getTaskStatuses())]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'order_id' => 'Заказ',
            'operator' => 'Оператор',
            'curator' => 'ID куратора',
            'curator_username' => 'Имя куратора',
            'status' => 'Статус',
            'order_old' => 'Старые данные заказа',
            'order_new' => 'Новые данные заказа',
            'product_old' => 'Старые данные продуктов заказа',
            'product_new' => 'Новые данные продуктов заказа',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения'
        ];
    }

    /**
     * @return OrderTaskQuery
     */
    public static function find()
    {
        return new OrderTaskQuery(get_called_class());
    }


    /**
     * @param $orderTask
     * @return []
     */
    public function sendOrderTaskToPay($orderTask)
    {
        $orderTaskArray = ArrayHelper::toArray($orderTask, [], false);
        $orderTaskWithoutJson = self::ArrayWithoutJsonRecursive($orderTaskArray);

        $order = Order::find()->where(['id' => $orderTaskArray['order_id']])->one();

        $orderTaskWithoutJson['foreign_order_id'] = $order->foreign_id;

        $jsonData = json_encode($orderTaskWithoutJson, JSON_UNESCAPED_UNICODE);

        $options = [
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer ' . yii::$app->params['payApiToken']
            ],
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $jsonData,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonData)
            ]
        ];

        $c = curl_init(yii::$app->params['payApiUrl']);
        curl_setopt_array($c, $options);
        $error = curl_errno($c) . ',' . curl_error($c);
        $result = curl_exec($c);
        $info = curl_getinfo($c);
        curl_close($c);

        $this->log = new ApiLog();
        $this->log->url = yii::$app->params['payApiUrl'];
        $this->log->request = json_encode(['params' => ['url' => yii::$app->params['payApiUrl'], 'token' => yii::$app->params['payApiToken']], 'orderTask' => $orderTask], JSON_UNESCAPED_UNICODE);
        $this->log->response = json_encode(['orderTask send to Pay' => $info, 'error' => $error], JSON_UNESCAPED_UNICODE) . PHP_EOL . json_encode($orderTask, JSON_UNESCAPED_UNICODE);

        $this->log->save();

        $answerPay = json_decode($result);

        if ($info['http_code'] != 200 || !isset($answerPay->status) || $answerPay->status == 'fail') {
            $orderTask = OrderTask::find()->where(['id' => $orderTask['id']])->one();
            $orderTask->status = OrderTask::TASK_STATUS_PENDING;
            $orderTask->update();
        }

        return [
            'success' => true,
            'result' => $result
        ];
    }

    /**
     * @param $array
     * @return array|string
     */
    public static function ArrayWithoutJsonRecursive($array)
    {
        if (!is_array($array)) {
            return $array;
        } else {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    self::ArrayWithoutJsonRecursive($v);
                } else {
                    $decoded = json_decode($v, 1);

                    if (json_last_error() == JSON_ERROR_NONE) {
                        $array[$k] = $decoded;
                    } else {
                        $array[$k] = $v;
                    }
                }
            }
        }

        return $array;
    }
}
<?php
namespace backend\modules\order\models\search;

use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\order\models\OrderTask;

/**
 * Class OrderTaskSearch
 * @package backend\modules\catalog\models\search
 */
class OrderTaskSearch extends OrderTask
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderTask::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['order_id' => $this->order_id]);

        return $dataProvider;
    }
}

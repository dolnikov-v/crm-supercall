<?php
/**
 * Created by PhpStorm.
 * User: PCWORK
 * Date: 16.01.2018
 * Time: 10:54
 */

namespace backend\modules\order\models\search;

use backend\modules\order\models\AddressVerification;
use common\models\User;
use common\modules\order\models\Order;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class AddressVerificationSearch extends AddressVerification
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [[['country_id', 'partner_id'], 'safe']];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['updated_at'] = SORT_ASC;
        }

        $query =  Order::find()
            ->where(['status' => Order::STATUS_APPROVED])
            ->andWhere(['confirm_address' => AddressVerification::ADDRESS_NEED_CONFIRM])
            ->andWhere(['country_id' => ArrayHelper::getColumn(User::getAllowCountries()->all(), 'country_id')])
            ->andWhere(['>=', 'updated_at', time()-3600])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        if($this->country_id){
            $query->andWhere(['country_id' => $this->country_id]);
        }

        if($this->partner_id){
            $query->andWhere(['partner_id' => $this->partner_id]);
        }

        return $dataProvider;
    }
}
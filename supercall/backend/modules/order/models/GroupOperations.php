<?php
namespace backend\modules\order\models;

use common\models\Product;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use yii\db\Query;


class GroupOperations extends Order{
    const LOGGER_TYPE = 'order_group_operations';

    const OPERATION_CHANGE_STATUS = '/order/group-operations/change-status';
    const OPERATION_SENT_TO_SQS = '/order/group-operations/sent-to-sqs';
    const OPERATION_CHANGE_QUEUE = '/order/group-operations/change-queue';
    const OPERATION_CHANGE_COST_PRODUCTS = '/order/group-operations/change-cost-products';
    const OPERATION_SENT_TO_CHECK_DEMAND = '/order/group-operations/sent-to-check-demand';
    const OPERATION_RESEND_TO_CALL = '/order/group-operations/resend-to-call';

    /**
     * @param $id
     * @return Order
     */
    public static function getOrderBy($id){
        return Order::find()->where(['id' => $id])->one();
    }

    /**
     * @param $id
     * @return array|bool
     */
    public static function isBlocked($id){
        return (new Query())->from("{{%queue_order}}")->where(['order_id' => $id])->one();
    }

    /**
     * @param $id
     * @return OrderProduct[]
     */
    public static function getGroupProductsBy($id){
        return OrderProduct::find()
            ->select([
                Product::tableName() . '.name',
                OrderProduct::tableName() . '.promo',
                OrderProduct::tableName() . '.product_id',
            ])
            ->leftJoin(Product::tableName(), Product::tableName() . '.id = ' . OrderProduct::tableName() . '.product_id')
            ->where([OrderProduct::tableName() . '.order_id' => $id])
            ->andWhere(["!=", OrderProduct::tableName() . '.gift', true])
            ->orderBy([Product::tableName() . '.name' => SORT_ASC])
            ->groupBy([Product::tableName() . '.name', OrderProduct::tableName() . '.promo', OrderProduct::tableName() . '.product_id'])
            ->asArray()
            ->all();
    }

    /**
     * @param $id
     * @return OrderProduct[]
     */
    public static function getOrderProductsBy($id){
        return OrderProduct::find()
            ->where(['order_id' => $id])
            ->andWhere(["!=", OrderProduct::tableName() . '.gift', true])
            ->all();
    }

    /**
     * @param $id
     * @return Order[]
     */
    public static function getOrderInFinalStatus($id){
        return Order::find()
            ->where(['id' => $id])
            ->andFilterWhere(['in', 'status', array_keys(Order::getFinalStatusesCollection())])
            ->asArray()
            ->all();
    }
}

?>
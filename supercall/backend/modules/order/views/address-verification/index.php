<?php

use backend\modules\order\assets\AddressVerificationAsset;
use common\components\grid\ActionColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\modules\order\models\Order;
use common\modules\partner\models\PartnerFormAttribute;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var $countries array */
/** @var $modelSearch \backend\modules\order\models\search\AddressVerificationSearch */
/** @var $dataProvider \yii\data\ActiveDataProvider */

AddressVerificationAsset::register($this);

$this->title = Yii::t('common', 'Подтверждение адреса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'countries' => $countries,
        'modelSearch' => $modelSearch
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заказами'),
    'alert' => yii::t('common', 'Список заказов содержит только заказы в статусе APPROVE, адреса которых, требуют подтверждения.'),
    'alertStyle' => Panel::ALERT_INFO,
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'country_id',
                'headerOptions' => ['class' => 'width-150'],
                'enableSorting' => false,
                'content' => function ($model) {
                    return yii::t('common', $model->country->name);
                }
            ],
            [
                'attribute' => 'partner_id',
                'headerOptions' => ['class' => 'width-150'],
                'enableSorting' => false,
                'content' => function ($model) {
                    return $model->partner->name;
                }
            ],
            [
                'attribute' => 'customer_components',
                'enableSorting' => false,
                'header' => yii::t('common', 'Данные адреса'),
                /** @var \backend\modules\order\models\AddressVerification $model */
                'content' => function ($model) {
                    $data = json_decode($model->customer_components);
                    /** @var Order $model*/
                    $kladerData = $model->verificateAddress();

                    if (!isset($data->address)) {
                        return yii::t('common', '<span class="text-danger">{error}</span>', ['error' => 'Отсутсвуют данные адреса в customer_components заказа']);
                    }

                    if (is_null($model->form_id)) {
                        return yii::t('common', '<span class="text-danger">{error}</span>', ['error' => 'Атрибуты формы не определены, у заказа не указан form_id']);
                    }

                    $form_attributes = ArrayHelper::map(PartnerFormAttribute::getFormAttributesAll($model->form_id), 'name', 'label');
                    $form_attributes_params = ArrayHelper::map(PartnerFormAttribute::getFormAttributesAll($model->form_id), 'name', 'id');
                    $form_attributes_is_klader = ArrayHelper::getColumn(PartnerFormAttribute::getFormAttributes($model->form_id),'id');

                    if (!is_array($form_attributes)) {
                        return yii::t('common', '<span class="text-danger">{error} form_id = {form_id}</span>', ['error' => 'Не найдены атрибуты формы: ', 'form_id' => $model->form_id]);
                    }

                    if (isset($form_attributes) && is_array($form_attributes) && isset($data->address)) {
                        $address = [];

                        $address[] = '<ul class="address-data" id="data_' . $model->id . '"">';

                        foreach ($data->address as $k => $v) {
                            if (isset($form_attributes[$k])) {
                                $class = (in_array($form_attributes_params[$k], $kladerData) || !in_array($form_attributes_params[$k], $form_attributes_is_klader) ? '' : 'text-danger');
                                $address[] = '<li data-class="' . $class . '" data-name="' . $k . '" data-id="' . $form_attributes_params[$k] . '" data-value="' . $v . '" data-label="' . $form_attributes[$k] . '"><b>'
                                    . yii::t('common', $form_attributes[$k]) . '</b> : <span  class="' . $class .'">' . $v . '</span></li>';
                            }
                        }

                        $address[] = '</ul>';

                        return implode('<br/>', $address);
                    }
                }
            ],
            [
                'attribute' => 'updated_at',
                'headerOptions' => ['class' => 'width-150'],
                'enableSorting' => false,
                'header' => yii::t('common', 'Остаток блокировки, мин.'),
                'content' => function ($model) {
                    $balance = (3600 - (time() - $model->updated_at)) > 0 ? ceil((3600 - (time() - $model->updated_at)) / 60) : 0;

                    if ($balance > 30) {
                        $class = 'text-success';
                    } elseif ($balance < 30 && $balance > 15) {
                        $class = 'text-warning';
                    } else {
                        $class = 'text-danger';
                    }

                    return '<span class="' . $class . '">' . $balance . '</span>';
                }
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Подтвердить'),
                        'style' => 'confirm-address',
                        'url' => function ($data) {
                            return Url::toRoute(['confirm', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.addressverification.confirm');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Изменить'),
                        'style' => 'edit-address',
                        'url' => function ($data) {
                            return Url::toRoute(['/order/address-verification/']);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.addressverification.edit');
                        },
                        'attributes' => function ($model) {
                            return [
                                'data-order_id' => $model->id
                            ];
                        }
                    ]
                ]
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?php Modal::begin([
    'id' => 'confirm_address_window',
    'header' => yii::t('common', '<h3>{text}</h3>', ['text' => 'Подтверждение']),
    'options' => ['tabindex' => 1],
    'footer' => Html::button('Подтвердить', ['class' => 'btn btn-default', 'id' => 'confirm_address_window_aprove']) . ' ' .
        Html::button('Отмена', ['class' => 'btn btn-danger', 'id' => 'confirm_address_window_cancel'])
]); ?>

<?= yii::t('common', 'Вы действительно хотите подтвердить адрес данного заказа ?'); ?>

<?php Modal::end(); ?>

<?= ModalConfirmDelete::widget(); ?>

<?php Modal::begin([
    'id' => 'edit_confirm_address',
    'header' => '<h3 class="title_modal">' . yii::t('common', 'Редактирование адреса заказа') . '</h3>',
    'options' => ['tabindex' => 1],
    'size' => Modal::SIZE_DEFAULT,
    'footer' => Html::button('Сохранить и подтвердить', ['class' => 'btn btn-default', 'id' => 'edit_address_save']) . ' ' .
        Html::button('Отмена', ['class' => 'btn btn-danger', 'id' => 'edit_address_cancel'])
]); ?>

<div class="edit_content_address"></div>

<?php Modal::end(); ?>

<?= ModalConfirmDelete::widget(); ?>

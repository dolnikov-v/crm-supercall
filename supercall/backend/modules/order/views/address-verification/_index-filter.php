<?php
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var $countries array */
/** @var $modelSearch \backend\modules\order\models\search\AddressVerificationSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'partner_id')->select2List(ArrayHelper::map(yii::$app->user->identity->partners, 'id', 'name'), [
                'prompt' => '-'
            ]); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'country_id')->select2List($countries, [
                'prompt' => '-'
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php $this->registerCss("
    .select2-search--hide {
        display: block !important; 
    }
") ?>
<?php

use common\components\grid\GridView;
use common\components\i18n\Formatter;
use common\modules\order\models\genesys\Call;
use common\modules\order\models\Order;
use common\widgets\base\Panel;
use yii\bootstrap\Tabs;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\assets\modules\order\OrderViewAsset;
use backend\modules\order\widgets\AddressView;

/** @var yii\web\View $this */
/** @var Order $model */
/** @var  $lastOrderTask */
/** @var array $invalid_address_orders */

$this->title = Yii::t('common', 'Заказ №' . $model->id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

OrderViewAsset::register($this);

$components = json_decode($model->customer_components);

/** @var Formatter $formatter */
$formatter = Yii::$app->formatter;
$default_timezone = $formatter->timeZone;
$formatter->timeZone = Yii::$app->user->timezone->timezone_id;
$similar_count = $model->getSimilar()->count();
echo Panel::widget([
    'title' => Yii::t('common', 'Просмотр заказа'),
    'content' => Tabs::widget([
        'encodeLabels' => false,
        'items' => [
            [
                'label' => Yii::t('common', 'Данные заказа'),
                'content' => DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'country.name',
                        'type.name',
                        [
                            'attribute' => 'status',
                            'value' => ArrayHelper::getValue(Order::getStatusesCollection(), $model->status)
                        ],
                        'sub_status',
                        'partner.name',
                        'foreign_id',
                        'form_id',
                        [
                            'attribute' => 'customer_phone',
                            'value' => function ($model) {
                                if (!Yii::$app->user->can('order.index.viewcontacts')) {
                                    return empty($model->customer_phone) ? $model->customer_phone : substr_replace($model->customer_phone, 'XXXX', -4);
                                } else {
                                    return $model->customer_phone;
                                }
                            }
                        ],
                        [
                            'attribute' => 'customer_mobile',
                            'value' => function ($model) {
                                if (!Yii::$app->user->can('order.index.viewcontacts')) {
                                    return empty($model->customer_mobile) ? $model->customer_mobile : substr_replace($model->customer_mobile, 'XXXX', -4);
                                } else {
                                    return $model->customer_mobile;
                                }
                            }
                        ],
                        'customer_address',
                        'customer_ip',
                        'source_uri',
                        [
                            'attribute' => 'customer_components',
                            'format' => 'raw',
                            'value' => function ($model) use ($components) {
                                /** @var Order $$model */
                                $modified_data = [];

                                if (isset($components->general)) {
                                    foreach ($components->general as $name => $data) {
                                        if (!Yii::$app->user->can('order.index.viewcontacts') && in_array($name, ['customer_phone', 'customer_mobile'])) {
                                            $modified_data[$name] = empty($data) ? $data : substr_replace($data, 'XXXX', -4);
                                        } else {
                                            $modified_data[$name] = $data;
                                        }
                                    }
                                }
                                return Html::tag('p', Yii::t('common', 'Данные заказчика')) .
                                DetailView::widget([
                                    'model' => $modified_data
                                ]) .
                                Html::tag('p', Yii::t('common', 'Адрес')) .
                                AddressView::widget([
                                    'model' => isset($components->address) ? $components->address : [],
                                    'address_attributes' => $model->form->addressAttrs,
                                    'form' => $model->form
                                ]);
                            }
                        ],
                        'init_price',
                        'final_price',
                        'shipping_price',
                        'recall_date',
                        'customer_first_name',
                        'customer_last_name',
                        'customer_middle_name',
                        'shipping_id',
                        'customer_email',
                        [
                            'attribute' => 'order_phones',
                            'format' => 'raw',
                            'value' => GridView::widget([
                                'dataProvider' => new ArrayDataProvider([
                                    'allModels' => $model->phones
                                ])
                            ])
                        ],
                        'language.name',
                        'last_queue_id',

                        [
                            'attribute' => 'ordered_at',
                            'value' => $formatter->asDateFullTime($model->ordered_at),
                            'enableSorting' => false,
                        ],
                        [
                            'attribute' => 'created_at',
                            'value' => $formatter->asDateFullTime($model->created_at),
                            'enableSorting' => false,
                        ],
                        [
                            'attribute' => 'updated_at',
                            'value' => $formatter->asDateFullTime($model->updated_at),
                            'enableSorting' => false,
                        ],

                        [
                            'attribute' => 'orderProducts',
                            'format' => 'raw',
                            'value' => GridView::widget([
                                'dataProvider' => new ArrayDataProvider([
                                    'allModels' => ArrayHelper::merge($model->orderProducts, $model->orderPromoProducts),
                                ]),
                                'columns' => [
                                    'product.name',
                                    'quantity',
                                    'price',
                                    'cost'
                                ]
                            ])
                        ]
                    ]
                ])
            ], [
                'label' => Yii::t('common', 'История заказа'),
                'content' => $this->render('@common/modules/order/views/change/_tab-history', [
                    'order' => $model,
                    'form' => null
                ])
            ],
            [
                'label' => Yii::t('common', 'Записи разговоров'),
                'content' => $this->render('_view-records', [
                    'records' => $records,
                    'model' => $model,
                    'invalid_address_orders' => $invalid_address_orders,
                ]),
                'linkOptions' => [
                    'onclick' => 'startRecordLoad(' . $model->id . ')',
                    'class' => 'tab-record-view'
                ],
            ],
            [
                'label' => Yii::t('common', 'Комментарии'),
                'content' => $this->render('@common/modules/order/views/change/_tab-comment', [
                    'comments' => $model->comments
                ])
            ],
            [
                'label' => Yii::t('common', 'Похожие заказы')
                    . ($similar_count > 0
                        ? ' ' . Html::tag('span', $similar_count, ['class' => 'label label-warning'])
                        : ''
                    ),
                'content' => $this->render('@common/modules/order/views/change/_tab-similar', [
                    'order' => $model,
                ]),
                'visible' => $similar_count
            ],
            [
                'label' => Yii::t('common', 'Таски'),
                'content' => $this->render('@common/modules/order/views/change/_tab-tasks-history', [
                    'orderTasks' => $lastOrderTask
                ])
            ],
        ],
    ])
]);

$formatter->timeZone = $default_timezone;
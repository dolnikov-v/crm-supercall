<?php

/** @var yii\web\View $this */
/** @var \common\models\search\ProductSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $statuses */
/** @var array $products */
/** @var array $partners */

$this->title = Yii::t('common', 'Мои перезвоны');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php
echo $this->render('@common/modules/order/views/index/recall', [
    'dataProvider' => $dataProvider,
]);
?>

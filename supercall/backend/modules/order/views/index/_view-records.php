<?php

use backend\widgets\callrecords\CallRecords;
use common\modules\order\models\Order;

/** @var Order $model */
/** @var array $invalid_address_orders , */
/** @var array $records; */
?>

<?php echo CallRecords::widget([
    'order' => $model,
    'records' => $records,
    'show_transcribe' => false
]); ?>

<?php foreach ($invalid_address_orders as $order) : ?>
    <?php /** @var Order $order */ ?>
    <h4> <?= yii::t('common', 'Заказ'); ?> #<?= $order->id; ?> <?= yii::t('common', 'Статус') ?>
        : <?= Order::getStatusByNumber($order->status) ?></h4></hr>
    <?= CallRecords::widget([
        'order' => $order,
        'records' => [],
        'show_transcribe' => false
    ]); ?>

<?php endforeach; ?>




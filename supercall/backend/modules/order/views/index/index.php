<?php

/** @var yii\web\View $this */
/** @var \common\models\search\ProductSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $statuses */
/** @var array $products */
/** @var array $partners */

use common\modules\call\assets\CallHistoryAsset;
use common\modules\order\assets\ScriptsOperatorsAsset;
use yii\helpers\ArrayHelper;
use backend\modules\queue\models\Queue;

$this->title = Yii::t('common', 'Список заказов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

CallHistoryAsset::register($this);
ScriptsOperatorsAsset::register($this);
?>

<?php
echo $this->render('@common/modules/order/views/index/index', [
    'modelSearch' => $modelSearch,
    'contentVisible' => $contentVisible,
    'showButtons' => $showButtons,
    'dataProvider' => $dataProvider,
    'statuses' => $statuses,
    'partners' => $partners,
    'types' => $types,
    'products' => $products,
    'countries' => $countries,
    'visiblePartner' => true,
    'visiblePartnerNumber' => true,
    'queues' => ArrayHelper::map(Queue::find()->all(), 'id', 'name'),
    'operators' => $operators,
]);
?>

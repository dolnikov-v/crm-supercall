<?php
use common\widgets\base\Panel;

/** @var \yii\web\View $this */
/** @var array  $orderTasks  */
?>

<?= Panel::widget([
    'title' => Yii::t('common', ''),
    'content' => $this->render('_tab-order-tasks', [
        'orderTasks' => $orderTasks
    ])
]); ?>



<?php
use common\modules\order\widgets\PanelGroupTasks;

/** @var yii\web\View $this */
/** @var array $orderTasks */
?>

<?php if ($orderTasks): ?>
    <?= PanelGroupTasks::widget([
        'tasks' => $orderTasks,
    ]) ?>
<?php else: ?>
    <div class="row">
        <p class="text-center"><?= Yii::t('common', 'Таски отсутствуют') ?></p>
    </div>
<?php endif; ?>

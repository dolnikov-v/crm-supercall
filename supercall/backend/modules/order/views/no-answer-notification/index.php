<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \common\modules\order\models\search\NoAnswerNotificationRequestSearch $searchModel
 * @var \yii\web\View $this
 */
use common\widgets\base\Panel;
use yii\widgets\LinkPager;
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'searchModel' => $searchModel,
        'countries' => $countries,
    ])
]) ?>


<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с уведомлениями'),
    'content' => $this->render('_index-content', [
        'dataProvider' => $dataProvider,
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
<?php
/**
 * @var \common\modules\order\models\search\NoAnswerNotificationRequestSearch $searchModel
 */
use common\models\Country;
use common\modules\order\models\NoAnswerNotificationRequest;
use common\widgets\ActiveForm;
use yii\helpers\Url;

$form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
])
?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'country_id')
            ->select2List($countries, ['prompt' => '—'])
            ->label(Yii::t('common', 'Страна')) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'status')
            ->select2List(NoAnswerNotificationRequest::statusLabels(), ['prompt' => '—']) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($searchModel, 'order_id')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index'])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

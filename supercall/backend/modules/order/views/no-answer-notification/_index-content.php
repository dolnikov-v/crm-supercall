<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\modules\order\models\NoAnswerNotificationRequest;

?>

<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'order.country.name',
            'label' => Yii::t('common', 'Страна'),
        ],
        [
            'attribute' => 'order_id',
        ],
        [
            'attribute' => 'status',
            'value' => function ($model) {
                $labels = NoAnswerNotificationRequest::statusLabels();
                return Yii::t('common', $labels[$model->status] ?? $model->status);
            },
        ],
        [
            'attribute' => 'phone',
        ],
        [
            'class' => DateColumn::className(),
            'formatType' => 'Datetime',
            'attribute' => 'sent_at',
        ],
        [
            'class' => DateColumn::className(),
            'formatType' => 'Datetime',
            'attribute' => 'created_at',
        ]
    ]
])
?>

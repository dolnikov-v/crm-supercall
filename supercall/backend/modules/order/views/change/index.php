<?php
use common\modules\order\models\Order;
use common\widgets\base\Panel;
use yii\helpers\Url;
use common\modules\call\assets\CallHistoryAsset;
use common\modules\order\assets\ScriptsOperatorsAsset;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var bool $editable */
/** @var array $types */
/** @var array $products */
/** @var array $shippings */
/** @var array $shipping_options */
/** @var string $operator */
/** @var array  $orderTasks*/
/** @var array  $lastOrderTask*/
/** @var integer  $countOrdersOnPhone*/
/** @var array $formAttributes*/
/** @var array $packageList */
/** @var string $packageData */
/** @var boolean $masked_phones*/
/** @var @var string $time_now_with_timezone*/
/** @var  array $operators */
/** @var bool $hide_status_buttons */
/** @var bool $disabled_status_buttons */
/** @var string $partnerFormAdditionalPrice */
/** @var string $productPrice */

$this->title = Yii::t('common', 'Редактирование заказа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => Url::to(['/order/index'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];

CallHistoryAsset::register($this);
//ScriptsOperatorsAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Заказ'),
    'content' => $this->render('@common/modules/order/views/change/index', [
        'order' => $order,
        'editable' => $editable,
        'id_param' => $order->id,
        'types' => $types,
        'products' => $products,
        'vat_list' => $vat_list,
        'shippings' => $shippings,
        'countries' => $countries,
        'is_default_shipping' => $is_default_shipping,
        'shipping_options' => $shipping_options,
        'products_price_data' => $products_price_data,
        'operator' => $operator,
        'orderTasks' => $orderTasks,
        'lastOrderTask' => $lastOrderTask,
        'countOrdersOnPhone' => $countOrdersOnPhone,
        'formAttributes' => $formAttributes,
        'packageList' => $packageList,
        'packageData' => $packageData,
        'priceShipping' => $priceShipping,
        'disabled_status_buttons' => false,
        'masked_phones' => $masked_phones,
        //для бекенда данный  функционал не нужен
        'partner_created_at' => null,
        'time_now_with_timezone' => $time_now_with_timezone,
        'operators' => $operators,
        'hide_status_buttons' => $hide_status_buttons,
        'partnerFormAdditionalPrice' => $partnerFormAdditionalPrice,
        'productPrice' => $productPrice
    ])
]); ?>

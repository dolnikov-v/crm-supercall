<?php

namespace backend\modules\order\controllers;

use api\components\filters\VerbFilter;
use common\components\base\AjaxFilter;
use common\models\LeadSQS;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use Yii;
use yii\helpers\Json;

/**
 * Class ChangeController
 * @package backend\modules\order\controllers
 */
class ChangeController extends \common\modules\order\controllers\ChangeController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update-order-product' => ['POST'],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'shipping-list-by-broker'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getOrderEditable(Order $order)
    {
        return !$order->final || $order->buffered;
    }

    public function actionUpdateOrderProduct()
    {
        $result = [
            'success' => true,
            'message' => yii::t('common', 'Цена товара обновлена')
        ];

        $orderProduct = OrderProduct::findOne(['id' => yii::$app->request->post('id')]);

        if (!$orderProduct) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'OrderProduct #{id} не найден', ['id' => yii::$app->request->post('id')])
            ];
        } else {
            $orderProduct->price = yii::$app->request->post('price');

            if (!$orderProduct->save(true, ['price'])) {
                $result = [
                    'success' => false,
                    'message' => $orderProduct->getFirstErrorAsString()
                ];
            }
        }

        return Json::encode($result);
    }
}

<?php

namespace backend\modules\order\controllers;

use api\components\filters\VerbFilter;
use backend\modules\order\models\TranscribeQueue;
use common\components\web\Controller;
use common\modules\call\models\CallHistory;
use Yii;
use yii\helpers\Json;


/**
 * Class TranscribeQueueController
 * @package backend\modules\order\controllers
 */
class TranscribeQueueController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'set-in-queue' => ['post'],
                ],
            ],
        ];
    }


    /**
     * @return string
     */
    public function actionSetInQueue()
    {
        $call_history_id = yii::$app->request->post('call_history_id');
        $callRecord = CallHistory::findOne(['id' => $call_history_id]);

        $result = [
            'success' => false,
            'message' => '',
            'transcribe' => false
        ];

        if (!$callRecord) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Сведения о записи разговора не найдены'),
                'transcribe' => false
            ];
        } else {
            $transcribeQueue = TranscribeQueue::findOne(['call_history_id' => $call_history_id]);

            if ($transcribeQueue) {
                if (in_array($transcribeQueue->status, TranscribeQueue::getErrorStatuses())) {
                    $result = [
                        'success' => false,
                        'message' => TranscribeQueue::getNameStatus($transcribeQueue->status),
                        'transcribe' => false
                    ];
                } elseif (in_array($transcribeQueue->status, TranscribeQueue::getInProgressStatuses())) {
                    $result = [
                        'success' => true,
                        'message' => yii::t('common', 'В процессе'),
                        'transcribe' => false
                    ];
                } elseif (in_array($transcribeQueue->status, TranscribeQueue::getFinalStatuses())) {
                    $result = [
                        'success' => true,
                        'message' => $transcribeQueue->callHistory->transcribe,
                        'transcribe' => true
                    ];
                }
            } else {
                $transcribeQueue = new TranscribeQueue([
                    'call_history_id' => $call_history_id,
                    'status' => TranscribeQueue::STATUS_IN_PROGRESS
                ]);

                if ($transcribeQueue->save(false)) {
                    $result = [
                        'success' => true,
                        'message' => yii::t('common', 'Задание для получение транскрибации успешно создано'),
                        'transcribe' => false
                    ];
                } else {
                    $result = [
                        'success' => false,
                        'message' => Json::encode($transcribeQueue->getErrors()),
                        'transcribe' => false
                    ];
                }
            }
        }

        return Json::encode($result);
    }
}
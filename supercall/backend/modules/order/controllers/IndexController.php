<?php
namespace backend\modules\order\controllers;

use backend\components\web\Controller;
use common\models\Product;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderProductLog;
use common\modules\order\models\search\OrderSearch;
use common\modules\partner\models\Partner;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class IndexController
 * @package backend\modules\order\controllers
 */
class IndexController extends \common\modules\order\controllers\IndexController
{
    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if ($action->id == 'get-operators-by-country') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
}

<?php

namespace backend\modules\order\controllers;

use backend\modules\catalog\models\KladerCountries;
use backend\modules\order\models\AddressVerification;
use backend\modules\order\models\search\AddressVerificationSearch;
use common\components\Notifier;
use common\models\LeadSQS;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\partner\models\PartnerFormAttribute;
use backend\components\web\Controller;
use yii;
use yii\web\HttpException;

/**
 * Class AddressVerificationController
 * @package backend\modules\order\controllers
 */
class AddressVerificationController extends Controller
{
    public function actionIndex()
    {
        $countries = [];

        if (is_array(Yii::$app->user->countries)) {
            foreach (Yii::$app->user->countries as $k => $country) {
                $countries[$country->id] = yii::t('common', $country->name);
            }
        }

        $modelSearch = new AddressVerificationSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'countries' => $countries
        ]);
    }

    public function actionConfirm()
    {
        $order = Order::findOne(['id' => yii::$app->request->get('id')]);

        if (!$order) {
            header("HTTP/1.0 404");
            throw new HttpException(404, yii::t('common', 'Заказ не найден'));
        } else {
            if (is_null($order->form_id)) {
                header("HTTP/1.0 400");
                throw new HttpException(400, yii::t('common', 'Форма заказа не определена'));
            }

            $data = json_decode($order->customer_components, 1);

            if (!isset($data['address'])) {
                header("HTTP/1.0 400");
                throw new HttpException(400, yii::t('common', 'Отсутствуют данные адреса в customer_components'));
            }
            //добавляем в кладер данные после подтверждения
            KladerCountries::addTreeToKladr($order);

            $order->confirm_address = AddressVerification::ADDRESS_CONFIRMED;

            if (!$order->saveWithoutLogs(false, ['status'])) {
                yii::$app->notifier->addNotifier(yii::t('common', 'Ошибка смены значения confirm_address признака заказа #{order_id}', ['order_id' => $order->id]), Notifier::TYPE_ERROR);
            } else {
                yii::$app->notifier->addNotifier(yii::t('common', 'Адрес заказа #{order_id} подтверждён и добавлен в КЛАДР', ['order_id' => $order->id]), Notifier::TYPE_SUCCESS);
            }

            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionEdit()
    {
        $order_id = yii::$app->request->post('order_id');
        $address = yii::$app->request->post('address');

        $order = Order::findOne(['id' => $order_id]);

        if (!$order) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Заказ №{order_id} не найден.', ['order_id' => $order_id])
            ];
        } else {
            $customer_components = json_decode($order->customer_components, 1);
            $customer_components['address'] = $address;

            $order->customer_components = json_encode($customer_components, JSON_UNESCAPED_UNICODE);
            $order->confirm_address = AddressVerification::ADDRESS_CONFIRMED;

            if (!$order->save(false)) {
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка сохранения адреса заказа №{order_id}: {error}', ['order_id' => $order_id, 'error' => $order->getFirstErrorAsString()])
                ];
            } else {
                $order->refresh();

                KladerCountries::addTreeToKladr($order);

                $sqs = new LeadSQS();
                $sqs->sendLeadStatus($order);

                $result = [
                    'success' => true,
                    'message' => yii::t('common', 'Адрес заказа №{order_id} успешно обновлён', ['order_id' => $order_id])
                ];
            }
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);


    }
}
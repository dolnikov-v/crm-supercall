<?php

namespace backend\modules\order\controllers;

use backend\modules\order\models\OrderExport;
use backend\modules\order\Module;
use common\modules\order\models\search\OrderSearch;
use phpnt\exportFile\controllers\ExportController;
use Yii;
use yii\helpers\ArrayHelper;
use Dompdf\Dompdf;
use Dompdf\Options;
use yii\helpers\Json;
use yii\web\HttpException;

/**
 * https://packagist.org/packages/phpnt/yii2-export
 * Class OrderExportController
 * @package common\modules\order\controllers
 */
class OrderExportController extends ExportController
{
    /** @var array */
    public $rules;
    /** @var array */
    public $mappedNames;

    public function __construct($id, Module $module, array $config = [])
    {
        $this->rules = $this->getRules();
        $this->mappedNames = $this->getMappedNames();

        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'get-export-columns' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionGetExportColumns()
    {
        $phones_columns = ['customer_mobile', 'customer_address'];
        $mappedColumns = OrderExport::columnMapNames();

        $list = OrderExport::getDefaultExportList();

        $checked = OrderExport::getSelectedExportColumns();

        $labels = (new OrderExport())->attributeLabels();
        $columnsList = [];

        foreach ($list as $k => $column) {
            $index = is_numeric($k) ? $column : $k;

            if (isset($labels[$index])) {
                if (in_array($index, $phones_columns)) {
                    if (!yii::$app->user->can('order.index.viewcontacts')) {
                        continue;
                    }
                }
                $columnsList[$index] = [
                    'checked' => (int)in_array($index, $checked),
                    'label' => yii::t('common', (in_array($index, array_keys($mappedColumns))) ? $mappedColumns[$index] : $labels[$index])
                ];
            }
        }

        ArrayHelper::multisort($columnsList, ['label'], [SORT_ASC]);

        $result = [
            'success' => !empty($columnsList),
            'message' => empty($columnsList) ? yii::t('common', 'Конфигурация полей не корректна') : $columnsList
        ];

        return json_encode($result, 256);
    }

    /**
     * @return array
     */
    public function getRules()
    {
        return OrderExport::getDefaultExportList();
    }

    /**
     * @return array
     */
    public function getMappedNames()
    {
        return OrderExport::columnMapNames();
    }

    public function actionExcel()
    {
        $data = $this->getData();

        /** @var OrderSearch $searchModel */
        $searchModel = $data['searchModel'];
        $dataProvider = $data['dataProvider'];
        $title = $data['title'];
        $fields = json_decode($data['fields'], 1);

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle(substr($title, 0, 31));
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
        $objPHPExcel->getDefaultStyle()->getFont()->setBold(true);
        $objPHPExcel->getDefaultStyle()->getFont()->setBold(true);
        $letter = 65;
        foreach ($fields as $one) {
            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($letter))->setAutoSize(true);
            $letter++;
        }
        $letter = 65;
        //шапка
        foreach ($fields as $one) {
            $label = isset($this->mappedNames['one']) ? yii::t('common', $this->mappedNames['one']) : $searchModel->getAttributeLabel($one);
            $objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . '1', yii::t('common', $label));
            $objPHPExcel->getActiveSheet()->getStyle(chr($letter) . '1')->getAlignment()->setHorizontal(
                \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $letter++;
        }
        $row = 2;
        $letter = 65;

        $objPHPExcel->getDefaultStyle()->getFont()->setBold(false);

        //данные
        foreach ($dataProvider->getModels() as $model) {
            foreach ($fields as $field) {
                foreach ($this->rules as $key => $rule) {
                    $field_name = is_numeric($key) ? $rule : $key;

                    if ($field == $field_name) {
                        $value = is_numeric($key) ? $model[$rule] : $rule($model);
                        $value = empty($value) ? '-' : $value;

                        $objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $row, $value);
                        $objPHPExcel->getActiveSheet()->getStyle(chr($letter) . $row)->getAlignment()->setHorizontal(
                            \PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                        $letter++;
                    } else {
                        continue;
                    }
                }
            }
            $letter = 65;
            $row++;

        }

        header('Content-Type: application/vnd.ms-excel');
        $filename = $title . ".xls";
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function actionCsvComma()
    {
        $data = $this->getData();
        $dataProvider = $data['dataProvider'];
        $title = $data['title'];
        $fields = json_decode($data['fields'], 1);

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Description: File Transfer');
        header('Content-Type: text/csv');
        $filename = $title . ".csv";
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Content-Transfer-Encoding: binary');
        $fp = fopen('php://output', 'w');

        fputs($fp, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));

        if ($fp) {
            $items = [];
            $i = 0;
            foreach ($fields as $one) {
                $items[$i] = $one;
                $i++;
            }
            fputs($fp, implode($items, ',') . "\n");
            $items = [];
            $i = 0;

            //данные
            foreach ($dataProvider->getModels() as $model) {
                foreach ($fields as $field) {
                    foreach ($this->rules as $key => $rule) {
                        $field_name = is_numeric($key) ? $rule : $key;

                        if ($field == $field_name) {
                            $value = is_numeric($key) ? $model[$rule] : $rule($model);
                            $value = empty($value) ?  is_numeric($value) ? $value : '-' : $value;

                            $item = str_replace('"', '\"', $value);

                            if ($item) {
                                $items[$i] = '"' . $item . '"';
                            } else {
                                $items[$i] = $item;
                            }
                            $i++;
                        } else {
                            continue;
                        }
                    }
                }
                fputs($fp, implode($items, ',') . "\n");
                $items = [];
                $i = 0;

            }
        }
        fclose($fp);
    }

    public function actionCsvSemicolon()
    {
        $data = $this->getData();
        $dataProvider = $data['dataProvider'];
        $title = $data['title'];
        $fields = json_decode($data['fields'], 1);

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Description: File Transfer');
        header('Content-Type: text/csv');
        $filename = $title . ".csv";
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Content-Transfer-Encoding: binary');
        $fp = fopen('php://output', 'w');

        fputs($fp, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));

        if ($fp) {
            $items = [];
            $i = 0;
            foreach ($fields as $one) {
                $items[$i] = $one;
                $i++;
            }
            fputs($fp, implode($items, ',') . "\n");
            $items = [];
            $i = 0;

            //данные
            foreach ($dataProvider->getModels() as $model) {
                foreach ($fields as $field) {
                    foreach ($this->rules as $key => $rule) {
                        $field_name = is_numeric($key) ? $rule : $key;

                        if ($field == $field_name) {
                            $value = is_numeric($key) ? $model[$rule] : $rule($model);
                            $value = empty($value) ?  is_numeric($value) ? $value : '-' : $value;

                            $item = str_replace('"', '\"', $value);

                            if ($item) {
                                $items[$i] = '"' . $item . '"';
                            } else {
                                $items[$i] = $item;
                            }
                            $i++;
                        } else {
                            continue;
                        }
                    }
                }
                fputs($fp, implode($items, ';') . "\n");
                $items = [];
                $i = 0;

            }
        }
        fclose($fp);
    }

    /** сырой */
    public function actionWord()
    {
        $data = $this->getData();
        $searchModel = $data['searchModel'];
        $dataProvider = $data['dataProvider'];
        $title = $data['title'];
        $fields = json_decode($data['fields'], 1);

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $sectionStyle = $section->getSettings();
        $sectionStyle->setLandscape();
        $sectionStyle->setBorderTopColor('C0C0C0');
        $sectionStyle->setMarginTop(300);
        $sectionStyle->setMarginRight(300);
        $sectionStyle->setMarginBottom(300);
        $sectionStyle->setMarginLeft(300);
        $phpWord->addTitleStyle(1, ['name' => 'HelveticaNeueLT Std Med', 'size' => 16], ['align' => 'center']); //h
        $section->addTitle('<p style="font-size: 24px; text-align: center;">' . $title . '</p>');

        $table = $section->addTable(
            [
                'name' => 'Tahoma',
                'align' => 'center',
                'cellMarginTop' => 30,
                'cellMarginRight' => 30,
                'cellMarginBottom' => 30,
                'cellMarginLeft' => 30,
            ]);
        $table->addRow(300, ['exactHeight' => true]);
        foreach ($fields as $one) {
            $table->addCell(1500, [
                'bgColor' => 'eeeeee',
                'valign' => 'center',
                'borderTopSize' => 5,
                'borderRightSize' => 5,
                'borderBottomSize' => 5,
                'borderLeftSize' => 5
            ])->addText($searchModel->getAttributeLabel($one), ['bold' => true, 'size' => 10], ['align' => 'center']);
        }
        foreach ($dataProvider->getModels() as $model) {
            $table->addRow(300, ['exactHeight' => true]);
            foreach ($fields as $one) {
                if (is_string($one)) {
                    $table->addCell(1500, [
                        'valign' => 'center',
                        'borderTopSize' => 1,
                        'borderRightSize' => 1,
                        'borderBottomSize' => 1,
                        'borderLeftSize' => 1
                    ])->addText($model[$one], ['bold' => false, 'size' => 10], ['align' => 'left']);
                } else {
                    $table->addCell(1500, [
                        'valign' => 'center',
                        'borderTopSize' => 1,
                        'borderRightSize' => 1,
                        'borderBottomSize' => 1,
                        'borderLeftSize' => 1
                    ])->addText($one($model), ['bold' => false, 'size' => 10], ['align' => 'left']);
                }
            }
        }

        header('Content-Type: application/vnd.ms-word');
        $filename = $title . ".docx";
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('php://output');
    }

    /** сырой */
    public function actionHtml()
    {
        $data = $this->getData();
        $searchModel = $data['searchModel'];
        $dataProvider = $data['dataProvider'];
        $title = $data['title'];
        $fields = json_decode($data['fields'], 1);

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $section->addTitle($title);
        $table = $section->addTable(
            [
                'name' => 'Tahoma',
                'size' => 10,
                'align' => 'center',
            ]);
        $table->addRow(300, ['exactHeight' => true]);
        foreach ($fields as $one) {
            $table->addCell(1500, [
                'bgColor' => 'eeeeee',
                'valign' => 'center',
                'borderTopSize' => 5,
                'borderRightSize' => 5,
                'borderBottomSize' => 5,
                'borderLeftSize' => 5
            ])->addText($searchModel->getAttributeLabel($one), ['bold' => true, 'size' => 10], ['align' => 'center']);
        }
        foreach ($dataProvider->getModels() as $model) {
            $table->addRow(300, ['exactHeight' => true]);
            foreach ($fields as $one) {
                if (is_string($one)) {
                    $table->addCell(1500, [
                        'valign' => 'center',
                        'borderTopSize' => 1,
                        'borderRightSize' => 1,
                        'borderBottomSize' => 1,
                        'borderLeftSize' => 1
                    ])->addText('<p style="margin-left: 10px;">' . $model[$one] . '</p>', ['bold' => false, 'size' => 10], ['align' => 'right']);
                } else {
                    $table->addCell(1500, [
                        'valign' => 'center',
                        'borderTopSize' => 1,
                        'borderRightSize' => 1,
                        'borderBottomSize' => 1,
                        'borderLeftSize' => 1
                    ])->addText('<p style="margin-left: 10px;">' . $one($model) . '</p>', ['bold' => false, 'size' => 10], ['align' => 'right']);
                }
            }
        }

        header('Content-Type: application/html');
        $filename = $title . ".html";
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
        $objWriter->save('php://output');
    }

    /** сырой */
    public function actionPdf()
    {
        $data = $this->getData();
        $searchModel = $data['searchModel'];
        $dataProvider = $data['dataProvider'];
        $title = $data['title'];
        $fields = json_decode($data['fields'], 1);

        $options = new Options();
        $options->set('defaultFont', 'times');
        $dompdf = new Dompdf($options);
        $html = '<html><body>';
        $html .= '<h1>' . $title . '</h1>';
        $html .= '<table width="100%" cellspacing="0" cellpadding="0">';
        $html .= '<tr style="background-color: #ececec;">';
        foreach ($fields as $one) {
            $label = isset($this->mappedNames['one']) ? yii::t('common', $this->mappedNames['one']) : $searchModel->getAttributeLabel($one);
            $html .= '<th style="border: 2px solid #cccccc; text-align: center; font-weight: 500;">' . $label . '</th>';
        }
        $html .= '</tr>';

        //данные
        foreach ($dataProvider->getModels() as $model) {
            foreach ($fields as $field) {
                foreach ($this->rules as $key => $rule) {
                    $field_name = is_numeric($key) ? $rule : $key;

                    if ($field == $field_name) {
                        $value = is_numeric($key) ? $model[$rule] : $rule($model);

                        $html .= '<td style="border: 1px solid #cccccc; text-align: left; font-weight: 300; padding-left: 10px;">' . (empty($value) ?  is_numeric($value) ? $value : '-' : $value) . '</td>';

                    } else {
                        continue;
                    }
                }
            }
            $html .= '</tr>';
        }


        $html .= '</table>';
        $html .= '</body></html>';
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream($title . '_' . time());
    }

    /**
     * @return array
     * @throws HttpException
     */
    private function getData()
    {
        if (!yii::$app->request->isPost) {
            throw new HttpException(400, Yii::t('common', 'Bad params'));
        }

        $queryParams = Json::decode(\Yii::$app->request->post('queryParams'));
        /** @var OrderSearch $searchModel */
        $searchModel = yii::$app->request->post('model');
        $searchModel = new $searchModel;
        $orders = yii::$app->request->post('orders');
        $orders = json_decode($orders, 1);
        $dataProvider = $orders ? $searchModel->searchById($orders) : $searchModel->search($queryParams, false);
        $fields = yii::$app->request->post('fields');
        $title = strtr(\Yii::$app->request->post('title'), [
            '/' => '-',
            ' ' => '_',
            ':' => '-'
        ]);

        $getAll = yii::$app->request->post('getAll');

        if ($getAll) {
            $dataProvider->pagination = false;
        }

        return [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $title,
            'fields' => $fields,
        ];
    }
}
<?php

namespace backend\modules\order\controllers;

use Yii;
use backend\components\web\Controller;
use common\modules\order\models\search\NoAnswerNotificationRequestSearch;

/**
 * Class NoAnswerNotificationController
 * @package backend\modules\order\controllers
 */
class NoAnswerNotificationController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new NoAnswerNotificationRequestSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $countries = [];

        foreach (yii::$app->user->countries as $country) {
            $countries[$country->id] = Yii::t('common', $country->name);
        }


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'countries' => $countries,
        ]);
    }
}
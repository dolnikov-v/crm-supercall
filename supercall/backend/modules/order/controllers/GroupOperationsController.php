<?php

namespace backend\modules\order\controllers;

use backend\components\web\Controller;
use backend\modules\order\models\AddressVerification;
use backend\modules\order\models\GroupOperations;
use backend\modules\queue\models\Queue;
use common\models\LeadSQS;
use common\models\QueueOrder;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderType;
use console\controllers\QueueController;
use Yii;
use yii\console\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class GroupOperationsController
 * @package backend\modules\order\controllers
 */
class GroupOperationsController extends Controller
{
    public $action = null;

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws HttpException
     */
    public function beforeAction($action)
    {
        if (!yii::$app->request->isAjax && !yii::$app->request->post()) {
            throw new HttpException(403, Yii::t('common', 'Запрос запрещён'));
        }
        return parent::beforeAction($action);
    }

    /**
     * @param $tags
     * @param null $error
     */
    public function toLog($tags, $error = null)
    {
        $route = isset($tags['route']) ? $tags['route'] : null;

        if ($route) {
            unset($tags['route']);
        }

        $logger = yii::$app->get('logger');
        $logger->action = $this->action;
        $logger->route = Url::to([$route]);
        $logger->type = GroupOperations::LOGGER_TYPE;
        $logger->tags = array_merge([
            'user_id' => yii::$app->user->identity->id,
            'created_at' => time(),
        ], $tags);

        if (!is_null($error)) {
            $logger->error = $error;
        }

        $logger->save();
    }

    /**
     * @param $id
     * @return array
     */
    public function orderNotFound($id)
    {
        return [
            'success' => false,
            'message' => yii::t('common', 'Заказ №{id} не найден', ['id' => $id]),
            'id' => $id
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function orderIsBlocked($id)
    {
        return [
            'success' => false,
            'message' => yii::t('common', 'Заказ №{id} в работе', ['id' => $id]),
            'id' => $id
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function getErrorForOrderInFinalStatus($id)
    {
        return [
            'success' => false,
            'message' => yii::t('common', 'Заказ №{id} находятся в финальном статусе', ['id' => $id]),
            'id' => $id
        ];
    }

    /**
     * Смена статусов
     * @return string
     */
    public function actionChangeStatus()
    {
        $this->action = 'group-change-status';

        $id = yii::$app->request->post('id');
        $status = yii::$app->request->post('status');
        $group = yii::$app->request->post('group');

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'new_status' => $status,
            'route' => '/order/group-operations/change-status'
        ];

        //если неизвестный статус
        if (!in_array($status, array_keys(Order::getStatusesCollection()))) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Для заказа №{id} указан несуществующий статус: {status}', ['id' => $id, 'status' => $status]),
                'id' => $id
            ];
        } else {
            $order = GroupOperations::getOrderBy($id);

            //заказ найден
            if ($order) {
                if (GroupOperations::isBlocked($id)) {
                    $result = $this->orderIsBlocked($id);
                    //заказ не блокирован
                } else {
                    $tags['old_status'] = $order->status;

                    $order->status = $status;

                    if (in_array($status, array_keys(Order::getFinalStatusesCollection()))) {
                        $order->prevent_queue_id = $order->last_queue_id;
                        $order->last_queue_id = null;
                    }

                    if (!$order->save(false)) {
                        $result = [
                            'success' => false,
                            'message' => yii::t('common', 'Ошибка при смене статуса заказа №{id} : {error}', ['id' => $id, 'error' => Json::encode($order->getErrors())]),
                            'id' => $id
                        ];
                    } else {
                        $result = [
                            'success' => true,
                            'message' => null,
                            'id' => $id
                        ];
                    }
                }
            } else {
                $result = $this->orderNotFound($id);
            }
        }

        $this->toLog($tags, $result['message']);

        return Json::encode($result);
    }

    /**
     * Отправка в очередь
     * @return string
     */
    public function actionSentToSqs()
    {
        $this->action = 'group-sent-to-sqs';

        $id = yii::$app->request->post('id');
        $group = yii::$app->request->post('group');

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'route' => '/order/group-operations/sent-to-sqs'
        ];

        $order = GroupOperations::getOrderBy($id);

        if (!$order) {
            $result = $this->orderNotFound($id);

            $this->toLog($tags, $result['message']);
        } else {
            if (GroupOperations::isBlocked($id)) {
                $result = $this->orderIsBlocked($id);
            } else {
                $leadSQS = new LeadSQS();

                $sent = is_null($order->foreign_id) ? $leadSQS->sendNewLead($order) : $leadSQS->sendLeadStatus($order);

                if ($sent) {
                    $result = [
                        'success' => true,
                        'message' => null,
                        'id' => $id
                    ];
                } else {
                    $result = [
                        'success' => false,
                        'message' => yii::t('common', 'Неудачная попытка отправки заказа №{id}', ['id' => $id]),
                        'id' => $id
                    ];
                }
            }
        }

        return Json::encode($result);
    }

    /**
     * @return string
     */
    public function actionGetSecondaryQueuesByCountry()
    {
        $country_id = yii::$app->request->post('country_id');

        if (empty($country_id)) {
            $country_id = ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id');
        }

        $queues = ArrayHelper::map(Queue::findAll([
            'active' => Queue::STATUS_ACTIVE,
            'country_id' => $country_id,
            'is_primary' => false,
        ]), 'id', function ($item) {
            return $item->id . '. ' . $item->name;
        });

        return Json::encode($queues);
    }

    /**
     * Пересборка очередей Rabbit MQ
     */
    public function actionReAssembleRabbitQueues()
    {
        $queue_id = yii::$app->request->post('queue_id');
        if (empty($queue_id)) {
            return true;
        }

        if (!defined('STDOUT')) {
            define('STDOUT', null);
        }
        $controller = new QueueController('queue', yii::$app->getModule('app-console'));

        try {
            $controller->actionCreate($queue_id, true);
        } catch (Exception $e) {
            sleep(30);
            $controller->actionCreate($queue_id, true);
        }
    }

    /**
     * Смена очереди
     * @return string
     */
    public function actionChangeQueue()
    {
        $id = yii::$app->request->post('id');
        $queue_id = yii::$app->request->post('queue_id');
        $country_id = yii::$app->request->post('country_id');
        $group = yii::$app->request->post('group');

        if (empty($country_id)) {
            $queue = Queue::findOne(['id' => $queue_id]);

            if ($queue) {
                $country_id = [$queue->country_id];
            }
        }

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'new_queue' => $queue_id,
            'route' => '/order/group-operations/change-queue'
        ];

        $order = GroupOperations::getOrderBy($id);

        if (!$order) {
            $result = $this->orderNotFound($id);
        } elseif (!in_array($order->country_id, $country_id)) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Ошибка смены очереди у заказа #{id}. Страна заказа отличается от страны очереди', ['id' => $id]),
                'id' => $id
            ];
        } else {
            if (GroupOperations::isBlocked($id)) {
                $result = $this->orderIsBlocked($id);
            } else {
                $tags['old_queue'] = $order->last_queue_id;
                $order->last_queue_id = $queue_id;
                if ($order->save(false)) {
                    $result = [
                        'success' => true,
                        'message' => null,
                        'id' => $id
                    ];
                } else {
                    $result = [
                        'success' => false,
                        'message' => yii::t('common', 'Неудачная смена очереди заказа №{id}: {error}', ['id' => $id, 'error' => Json::encode($order->getErrors())]),
                        'id' => $id
                    ];
                }
            }
        }
        $this->toLog($tags, $result['message']);

        return Json::encode($result);
    }

    /**
     * Смена стоимости товара в заказе
     * @return string
     */
    public function actionChangeCostProducts()
    {
        $id = yii::$app->request->post('id');
        $formProducts = yii::$app->request->post('form_products');

        $tags = [
            'order_id' => $id,
            'route' => '/order/group-operations/change-cost-products'
        ];


        $order = GroupOperations::getOrderBy($id);
        $orderProducts = GroupOperations::getOrderProductsBy($id);

        if (!$order && !$orderProducts) {
            $result = $this->orderNotFound($id);
        } else {

            if (GroupOperations::isBlocked($id)) {
                $result = $this->orderIsBlocked($id);
            } else {

                if (GroupOperations::getOrderInFinalStatus($id)) {
                    $result = $this->getErrorForOrderInFinalStatus($id);
                } else {

                    QueueOrder::setBlockOrder('1', $id);

                    $transaction = yii::$app->db->beginTransaction();
                    $sumCost = 0;
                    $errors = false;

                    foreach ($orderProducts as $orderProduct) {
                        foreach ($formProducts as $formProduct) {

                            $promo = $formProduct['promo'] == 'false' ? false : true;
                            if ($orderProduct['product_id'] == $formProduct['product_id'] && $orderProduct['promo'] == $promo) {
                                $cost = $formProduct['price'] * $orderProduct['quantity'];
                                $sumCost += $cost;

                                $orderProduct->price = $formProduct['price'];
                                $orderProduct->cost = $cost;

                                if (!$orderProduct->save(false)) {
                                    $result = [
                                        'success' => false,
                                        'message' => yii::t('common', 'Ошибка транзакции при обновлении заказа №{id} : {error}', ['id' => $id, 'error' => Json::encode($orderProduct->getErrors())]),
                                        'id' => $id
                                    ];

                                    $transaction->rollBack();
                                    $errors = true;
                                    break;
                                }
                            }
                        }

                        if ($errors) {
                            break;
                        }
                    }

                    if (!$errors) {
                        $order->final_price = $sumCost + $order['shipping_price'];

                        if (!$order->save(false)) {
                            $transaction->rollBack();
                            $result = [
                                'success' => false,
                                'message' => yii::t('common', 'Ошибка при изменении финальной стоимости заказа №{id} : {error}', ['id' => $id, 'error' => Json::encode($order->getErrors())]),
                                'id' => $id
                            ];
                        } else {
                            $transaction->commit();

                            try {
                                $leadSQS = new LeadSQS();

                                if (!$leadSQS->sendLeadStatus($order)) {
                                    $result = [
                                        'success' => false,
                                        'message' => yii::t('common', 'Заказа №{id} обновлен, но не отправлен партнеру. Необходимо отправить заказ в очередь в ручном режиме сомощю груповой операции', ['id' => $id]),
                                        'id' => $id
                                    ];
                                } else {
                                    $result = [
                                        'success' => true,
                                        'message' => yii::t('common', 'Заказа №{id} обновлен, данные отправлены партнеру.', ['id' => $id]),
                                        'id' => $id
                                    ];
                                }
                            } catch (\yii\base\Exception $e) {
                                $result = [
                                    'success' => false,
                                    'message' => $e->getMessage(),
                                    'id' => $id
                                ];
                            } finally {
                                QueueOrder::clearBlocksById($id);
                            }
                        }
                    }
                }
            }
        }

        $this->toLog($tags, $result['message']);
        return Json::encode($result);
    }

    /**
     * Получение группированного списка товаров
     * @return string
     */
    public function actionGetProducts()
    {
        $orderIds = yii::$app->request->post('orderIds');
        $products = GroupOperations::getGroupProductsBy($orderIds);

        return Json::encode($products);
    }

    /**
     * @return string
     */
    public function actionSentToCheckDemand()
    {
        $id = yii::$app->request->post('id');

        $order = GroupOperations::getOrderBy($id);


        if (!$order) {
            $result = $this->orderNotFound($id);
        } else {
            $queueCheckDemand = Queue::find()
                ->byCountry($order->country_id)
                ->byType(OrderType::TYPE_ORDER_STILL_WAITING)
                ->active()
                ->one();

            if (!$queueCheckDemand) {
                $result = [
                    'success' => false,
                    'message' => Yii::t('common', 'Заказ #{id}. Очередь Check demand (с типом Still waitnig) по стране заказа не найдена', ['id' => $id]),
                    'id' => $id
                ];
            } else {
                if (GroupOperations::isBlocked($id)) {
                    $result = $this->orderIsBlocked($id);
                } else {
                    if ($order->status != Order::STATUS_REJECTED) {
                        $result = [
                            'success' => false,
                            'message' => Yii::t('common', 'Заказ #{id} должен находится в статусе {status}', ['id' => $id, 'status' => yii::t('common', Order::getStatusByNumber(Order::STATUS_REJECTED))]),
                            'id' => $id
                        ];
                    } else {
                        $attributes = $order->getAttributes();

                        unset($attributes['id']);

                        $clonedOrder = new Order($attributes);
                        $clonedOrder->status = Order::STATUS_NEW;
                        $clonedOrder->type_id = OrderType::TYPE_CREATED;
                        $clonedOrder->created_at = time();
                        $clonedOrder->updated_at = time();
                        $clonedOrder->partner_created_at = time();
                        $clonedOrder->last_queue_id = $queueCheckDemand->id;
                        $clonedOrder->foreign_id = $order->foreign_id;
                        $clonedOrder->shipping_price = null;
                        $clonedOrder->shipping_id = null;
                        $clonedOrder->ordered_at = null;
                        $clonedOrder->confirm_address = AddressVerification::ADDRESS_CONFIRM_DEFAULT;
                        $clonedOrder->queue_counter = 0;
                        $clonedOrder->date_first_call = 0;
                        $clonedOrder->time_getting_order_crm = 0;
                        $clonedOrder->original_id = $order->id;
                        $clonedOrder->sent_to_sqs = false;
                        $clonedOrder->sub_status = null;

                        if (!$clonedOrder->save()) {
                            $result = [
                                'success' => false,
                                'message' => Yii::t('common', 'Заказ #{id}. ошибка создания дубликата для отправки в очередь: {error}', ['id' => $id, 'error' => Json::encode($clonedOrder->getErrors())]),
                                'id' => $id
                            ];
                        } else {
                            $error = false;

                            foreach (array_merge($order->orderProducts, $order->orderPromoProducts) as $product) {
                                /** @var OrderProduct $productAttributes */
                                $productAttributes = $product->getAttributes();
                                unset($productAttributes['id']);

                                $clonedProduct = new OrderProduct($productAttributes);
                                $clonedProduct->order_id = $clonedOrder->id;
                                $clonedProduct->created_at = time();
                                $clonedProduct->updated_at = time();

                                if (!$clonedProduct->save()) {
                                    $error = true;

                                    $result = [
                                        'success' => false,
                                        'message' => Yii::t('common', 'Заказ #{id}. ошибка создания дубликата для отправки в очередь: {error}', ['id' => $id, 'error' => Json::encode($clonedProduct->getErrors())]),
                                        'id' => $id
                                    ];

                                    break;
                                }

                            }

                            if (!$error) {
                                $result = [
                                    'success' => true,
                                    'message' => Yii::t('common', 'По заказу #{id} создан дубликат #{duplicate_id} и отправлен в очередь', ['id' => $order->id, 'duplicate_id' => $clonedOrder->id]),
                                    'id' => $id
                                ];
                            }
                        }
                    }
                }
            }
        }

        $tags = [
            'order_id' => $id,
            'route' => '/order/group-operations/sent-to-check-demand'
        ];

        $this->toLog($tags, $result['message']);
        return Json::encode($result);
    }

    /**
     * Получение первичных очередей по стране из фильтра
     * @return string
     */
    public function actionGetPrimaryQueues()
    {
        $country_id = yii::$app->request->post('country_id');

        if (count($country_id) == 1) {

            $queue = ArrayHelper::map(Queue::find()
                ->byCountry($country_id)
                ->active()
                ->all(), 'id', function ($item) {
                return $item->id . '. ' . $item->name;
            });


            if (empty($queue)) {
                $result = [
                    'success' => false,
                    'text' => Yii::t('common', 'В выбранной стране нет первичной очереди')
                ];
            } else {
                $result = $queue;
            }

        } else {
            $result = [
                'success' => false,
                'text' => Yii::t('common', 'Должна быть указана одна страна')
            ];
        }

        return Json::encode($result);
    }

    /**
     * Отправка заказов на повторный обзвон
     * @return string
     */
    public function actionResendToCall() {

        $id = yii::$app->request->post('id');
        $queue = yii::$app->request->post('queue_id');
        $order = GroupOperations::getOrderBy($id);

        $tags = [
            'order_id' => $id,
            'route' => '/order/group-operations/resent-to-call'
        ];


        if (!$order) {
            $result = $this->orderNotFound($id);
        } else {
            if(!in_array($order->status, array_keys(Order::getFinalStatusesCollection())) ){
                $result = [
                    'success' => false,
                    'message' => Yii::t('common', 'Заказ #{id} должен находиться в финальном статусе', ['id' => $id]),
                    'id' => $id
                ];
            }else {
                if (!$queue) {
                    $result = [
                        'success' => false,
                        'message' => Yii::t('common', 'Заказ #{id}. Не выбрана первичная очередь', ['id' => $id]),
                        'id' => $id
                    ];

                } else {
                    if (GroupOperations::isBlocked($id)) {
                        $result = $this->orderIsBlocked($id);
                    } else {

                        $attributes = $order->getAttributes();

                        unset($attributes['id']);

                        $clonedOrder = new Order($attributes);
                        $clonedOrder->status = Order::STATUS_NEW;
                        $clonedOrder->type_id = OrderType::TYPE_CREATED;
                        $clonedOrder->created_at = time();
                        $clonedOrder->updated_at = time();
                        $clonedOrder->partner_created_at = time();
                        $clonedOrder->last_queue_id = $queue;
                        $clonedOrder->foreign_id = $order->foreign_id;
                        $clonedOrder->shipping_price = null;
                        $clonedOrder->shipping_id = null;
                        $clonedOrder->ordered_at = null;
                        $clonedOrder->confirm_address = AddressVerification::ADDRESS_CONFIRM_DEFAULT;
                        $clonedOrder->queue_counter = 0;
                        $clonedOrder->date_first_call = 0;
                        $clonedOrder->time_getting_order_crm = 0;
                        $clonedOrder->original_id = $order->id;
                        $clonedOrder->sent_to_sqs = false;
                        $clonedOrder->sub_status = null;

                        if (!$clonedOrder->save()) {
                            $result = [
                                'success' => false,
                                'message' => Yii::t('common', 'Заказ #{id}. Ошибка создания дубликата для отправки в очередь: {error}', ['id' => $id, 'error' => Json::encode($clonedOrder->getErrors())]),
                                'id' => $id
                            ];
                        } else {
                            $error = false;

                            foreach (array_merge($order->orderProducts, $order->orderPromoProducts) as $product) {
                                /** @var OrderProduct $productAttributes */
                                $productAttributes = $product->getAttributes();
                                unset($productAttributes['id']);

                                $clonedProduct = new OrderProduct($productAttributes);
                                $clonedProduct->order_id = $clonedOrder->id;
                                $clonedProduct->created_at = time();
                                $clonedProduct->updated_at = time();

                                if (!$clonedProduct->save()) {
                                    $error = true;

                                    $result = [
                                        'success' => false,
                                        'message' => Yii::t('common', 'Заказ #{id}. Ошибка создания дубликата для отправки в очередь: {error}', ['id' => $id, 'error' => Json::encode($clonedProduct->getErrors())]),
                                        'id' => $id
                                    ];

                                    break;
                                }
                            }

                            if (!$error) {
                                $result = [
                                    'success' => true,
                                    'message' => Yii::t('common', 'По заказу #{id} создан дубликат #{duplicate_id} и отправлен в первичную очередь', ['id' => $order->id, 'duplicate_id' => $clonedOrder->id]),
                                    'id' => $id
                                ];
                            }

                        }
                    }
                }
            }
        }


        $this->toLog($tags, $result['message']);
        return Json::encode($result);
    }
}
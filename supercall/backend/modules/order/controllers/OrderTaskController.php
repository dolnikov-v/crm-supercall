<?php
namespace backend\modules\order\controllers;

use api\models\ApiLog;
use backend\modules\order\models\OrderTask;
use common\modules\order\models\Order;
use backend\components\web\Controller;
use backend\modules\order\models\search\OrderTaskSearch;
use common\modules\order\models\OrderProduct;
use api\modules\v1\controllers\OrderTaskController as ApiController;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use common\modules\partner\helpers\PartnerProduct;
use common\modules\partner\models\PartnerProduct as PartnerProductModel;
use backend\modules\catalog\models\KladerCountries;

/**
 * Class OrderTask
 * @package backend\modules\order\controllers
 */
class OrderTaskController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new OrderTaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $order_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getOrderTasks($order_id)
    {
        $orderTasks = OrderTask::find()
            ->where([OrderTask::tableName() . '.order_id' => $order_id])
            ->orderBy(['id' => SORT_ASC])
            ->all();
        if(!$orderTasks){
            return [];
        }
        return $orderTasks;
    }

    /**
     * for ajax reload
     * @param $order_id
     * @return string
     */
    public function actionGetOrderTasks()
    {
        $order_id = yii::$app->request->get('order_id');

        $orderTasks = $this->getOrderTasks($order_id);

        return $this->render('get-order-tasks', [
            'orderTasks' => $orderTasks
        ]);
    }

    /**
     * @param  integer|array $orders_id
     * @param  integer $status
     * @return bool
     */
    public function changeStatusOrderTask($orders_id, $status)
    {
        if (!is_array($orders_id)) {
            $orders_id = [$orders_id];
        }

        $result = OrderTask::updateAll(['status' => $status], ['id' => $orders_id]);

        return is_numeric($result) ? true : false;
    }

    /**
     * @param $order_id
     * @param integer | array $status
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getLastTask($order_id, $status)
    {
        $status = is_array($status) ? $status : [$status];

        $orderTasks = OrderTask::find()
            ->where([OrderTask::tableName() . '.order_id' => $order_id])
            ->andWhere(['in', OrderTask::tableName() . '.status', $status])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1)
            ->one();

        return $orderTasks;
    }

    /**
     * @return string
     */
    public function saveTask()
    {
        $data = yii::$app->request->post();

        $taskOrder = $data['Order'];
        $order_id = $taskOrder['order_id'];

        $oldDataOrder = Order::find()->where(['id' => $order_id])->asArray()->one();
        $oldDataOrderProduct = OrderProduct::find()->where(['order_id' => $order_id])->asArray()->all();

        foreach ($oldDataOrderProduct as $k => $order_product) {
            $oldDataOrderProduct[$k]['partner_product_id'] = PartnerProduct::getForeign($taskOrder['partner_id'], $order_product['product_id'], null);
        }

        if (!isset($data['OrderTaskAddress'])) {
            $data['OrderTaskAddress'] = [];
            $data['OrderTaskCustomer']['general'] = isset($taskOrder['general']) ? $taskOrder['general'] :[];
            $data['OrderTaskAddress']['address'] = isset($taskOrder['address']) ? $taskOrder['address'] : [];
        }

        //в форме создания таска - если тасков у заказа не было то форму рисует модель ордера
        if (!isset($data['OrderTaskCustomer'])) {
            $customerGeneral = $taskOrder['general'];
            $customerAddress = $taskOrder['address'];
        } //если у заказа есть актуальный таск - то форму будет рисовать модель таска OrderTaskCustomer по данным клиента
        else {
            $customerGeneral = $data['OrderTaskCustomer']['general'];
            $customerAddress = $data['OrderTaskAddress']['address'];
        }

        $comments = [
            [
                'author' => $data['Order']['comment_author'],
                'text' => $data['Order']['comment']
            ]
        ];

        $lastOrderTask = $this->getLastTask($order_id, [OrderTask::TASK_STATUS_NEW, OrderTask::TASK_STATUS_PENDING]);

        if (!is_null($lastOrderTask)) {
            $order_new = json_decode($lastOrderTask->order_new);
            $customer_components = json_decode($order_new->customer_components, 1);

            if (isset($customer_components['comments'])) {
                $comments_old = $customer_components['comments'];

                //был добавлен коммент - добавим в json
                if (!empty($data['Order']['comment'])) {
                    $comments = array_merge($comments, $comments_old);
                    //поле было пустое - оставим старые комменты
                } else {
                    $comments = $comments_old;
                }
            }
        }

        $newDataOrder = [
            'id' => $order_id,
            'country_id' => $oldDataOrder['country_id'],
            'status' => $oldDataOrder['status'],
            'partner_id' => $taskOrder['partner_id'],
            'customer_address' => $taskOrder['customer_address'],
            'customer_components' => json_encode(['general' => $customerGeneral, 'address' => $customerAddress, 'comments' => $comments], JSON_UNESCAPED_UNICODE),
            'init_price' => $taskOrder['init_price_task'],
            'final_price' => $taskOrder['final_price_task'],
            'shipping_price' => $taskOrder['shipping_price_task'],
            'delivery_from' => strtotime($data['OrderTask']['delivery_from']),
            'delivery_to' => strtotime($data['OrderTask']['delivery_to']),
            'created_at' => $oldDataOrder['created_at'],
            'updated_at' => $oldDataOrder['updated_at'],
            'shipping_id' => $oldDataOrder['shipping_id'],
            'order_phones' => isset($taskOrder['phones']) ? $taskOrder['phones'] : null,
            'customer_full_name' => isset($customerGeneral['customer_full_name']) ? $customerGeneral['customer_full_name'] : yii::t('common', 'Имя не указано')
        ];

        $pseudo_order = new \StdClass;
        $pseudo_order->country_id = $newDataOrder['country_id'];
        $pseudo_order->partner_id = $newDataOrder['partner_id'];
        $pseudo_order->customer_components = $newDataOrder['customer_components'];


        KladerCountries::addTreeToKladr($pseudo_order);

        $newDataOrderProduct = $data['OrderProduct'];

        foreach ($newDataOrderProduct as $k => $order_product) {
            $newDataOrderProduct[$k]['partner_product_id'] = PartnerProduct::getForeign($taskOrder['partner_id'], $order_product['product_id'], null);
        }

        foreach($oldDataOrderProduct as $k => &$product){
            $partnerProduct = PartnerProductModel::getPartnerProduct($product['product_id'], $oldDataOrder['partner_id'], $oldDataOrder['country_id']);
            $product['partner_product_id'] = $partnerProduct instanceof PartnerProductModel ?  $partnerProduct->partner_product_id : null;
        }

        foreach($newDataOrderProduct as $k => &$product){
            $partnerProduct = PartnerProductModel::getPartnerProduct($product['product_id'], $oldDataOrder['partner_id'], $oldDataOrder['country_id']);
            $product['partner_product_id'] = $partnerProduct instanceof PartnerProductModel ?  $partnerProduct->partner_product_id : null;
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $orderTask = new OrderTask();
        $orderTask->order_id = $order_id;
        $orderTask->status = OrderTask::TASK_STATUS_NEW;
        $orderTask->order_old = json_encode($oldDataOrder, JSON_UNESCAPED_UNICODE);
        $orderTask->order_new = json_encode($newDataOrder, JSON_UNESCAPED_UNICODE);
        $orderTask->product_old = json_encode($oldDataOrderProduct, JSON_UNESCAPED_UNICODE);
        $orderTask->product_new = json_encode($newDataOrderProduct, JSON_UNESCAPED_UNICODE);
        $orderTask->operator = yii::$app->user->id;
        $orderTask->curator = null;
        $orderTask->curator_username = null;
        $orderTask->created_at = time();
        $orderTask->updated_at = time();

        //смена статусов неактуальных тасков
        $notActualOrderTasks = OrderTask::find()
            ->where(['order_id' => $order_id])
            ->andWhere(['in', 'status', [OrderTask::TASK_STATUS_NEW, OrderTask::TASK_STATUS_PENDING]])
            ->asArray()
            ->all();

        $orders_id = ArrayHelper::getColumn($notActualOrderTasks, 'id');

        if (!$this->changeStatusOrderTask($orders_id, OrderTask::TASK_STATUS_NOT_ACTUAL)) {
            $transaction->rollBack();

            return json_encode([
                'success' => false,
                'message' => yii::t('common', 'Произошла ошибка при установки статуса неактуальности тасков')
            ], JSON_UNESCAPED_UNICODE);
        } else {
            if (!$orderTask->save()) {
                $transaction->rollBack();

                return json_encode([
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка при создании таска')
                ], JSON_UNESCAPED_UNICODE);
            } else {
                $transaction->commit();

                $orderTask = OrderTask::find()->where(['id' => Yii::$app->db->getLastInsertID()])->asArray()->one();

                //отправка таска на пей
                $orderTaskModel = new OrderTask();

                $result = $orderTaskModel->sendOrderTaskToPay($orderTask);

                $answerPay = json_decode($result['result']);

                if(!is_object($answerPay)){
                    return json_encode([
                        'success' => false,
                        'message' => yii::t('common', 'Таск создан, но при отправке произошла ошибка {error}: ', ['error' => yii::t('common', 'Сервер не доступен')])
                    ], JSON_UNESCAPED_UNICODE);
                }

                    if ($answerPay->status == 'fail') {
                        return json_encode([
                            'success' => false,
                            'message' => yii::t('common', 'Таск создан, но при отправке произошла ошибка {error}: ', ['error' => implode("\r\n", $answerPay->data->messages)])
                        ], JSON_UNESCAPED_UNICODE);
                    } else {
                        return json_encode([
                            'success' => true,
                            'message' => yii::t('common', 'Таск на изменение заказа успешно создан')
                        ], JSON_UNESCAPED_UNICODE);
                    }
            }
        }

    }

}

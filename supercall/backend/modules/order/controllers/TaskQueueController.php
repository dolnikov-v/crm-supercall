<?php

namespace backend\modules\order\controllers;

use backend\components\web\Controller;
use api\components\filters\VerbFilter;
use backend\models\TaskQueue;
use backend\models\TaskQueueOrder;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use common\modules\order\models\search\OrderSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class TaskQueueController
 * @package backend\modules\order\controllers
 */
class TaskQueueController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add-task-to-export-call-records' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionAddTaskToExportCallRecords()
    {
        $result = [
            'success' => false,
            'message' => yii::t('common', 'Ошибка передачи списка заказов для экспорта записей')
        ];

        $orders = [];
        $type = yii::$app->request->post('type');
        $data = yii::$app->request->post('data');

        if (!in_array($type, array_keys(TaskQueue::getTypeCollection()))) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Неизвестный тип для операции экспорта')
            ];
        } else {
            switch ($type) {
                case TaskQueue::BY_QUERY:
                    $modelSearch = new OrderSearch();
                    /** @var ActiveDataProvider $dataProvider */
                    $dataProvider = $modelSearch->search($this->parseQueryParams($data), false);
                    /** @var Query $modelsOrders ->query */
                    $modelsOrders = $dataProvider->query->joinWith('callHistory')->where(['is not', 'uniqueid', null]);
                    break;
                case TaskQueue::BY_ID:
                    $modelsOrders = Order::find()
                        ->joinWith('callHistory')
                        ->where([CallHistory::tableName() . '.order_id' => $data])
                        ->andWhere(['is not', 'uniqueid', null]);
                    break;
            }

            if (isset($modelsOrders)) {
                foreach ($modelsOrders->batch(100) as $models) {
                    foreach ($models as $model) {
                        if ($model->callHistory) {
                            if (isset($model->callHistory[0])) {
                                $orders[$model->id] = ArrayHelper::getColumn($model->callHistory, 'uniqueid');
                            }
                        }
                    }
                }

                $chunkBy20 = array_chunk($orders, 20, true);

                $result = $this->addTasksToQueue($chunkBy20);
            }
        }

        return Json::encode($result);
    }

    /**
     * @param array $dataForTasks
     * @return array
     */
    public function addTasksToQueue($dataForTasks)
    {
        $transaction = yii::$app->db->beginTransaction();

        $dataForCreateTasks = [];

        foreach ($dataForTasks as $dataForTask) {
            $dataForCreateTasks[] = [
                'name' => null,
                'type' => TaskQueue::TYPE_CALL_RECORD_IMPORT,
                'user_id' => yii::$app->user->identity ? yii::$app->user->identity->id : TaskQueue::SYSTEM_USER_ID,
                'path' => null,
                'status' => TaskQueue::STATUS_READY,
                'error' => null,
                'created_at' => time(),
                'updated_at' => time()
            ];
        }

        try {
            $error = false;

            if (!empty($dataForCreateTasks)) {
                $count = yii::$app->db->createCommand()->batchInsert(
                    TaskQueue::tableName(),
                    [
                        'name',
                        'type',
                        'user_id',
                        'path',
                        'status',
                        'error',
                        'created_at',
                        'updated_at'
                    ],
                    $dataForCreateTasks
                )->execute();

                if ($count > 0) {
                    $dataForTaskQueueOrder = [];
                    $inseredRecordsQuery = TaskQueue::find()->orderByDesc()->limit($count);

                    foreach ($inseredRecordsQuery->batch(10) as $tasks) {
                        /** @var TaskQueue $task */
                        foreach ($tasks as $num => $task) {
                            foreach ($dataForTasks[$num] as $order_id => $uniqueid) {
                                foreach($uniqueid as $uniq) {
                                    $dataForTaskQueueOrder[] = [
                                        'task_queue_id' => $task->id,
                                        'order_id' => $order_id,
                                        'uniqueid' => $uniq,
                                        'created_at' => time(),
                                        'updated_at' => time()
                                    ];
                                }

                                $update = $task->generateName(count($dataForTasks[$num]));
                                if (!$update) {
                                    $transaction->rollBack();
                                    $error = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (!empty($dataForTaskQueueOrder) && !$error) {
                        yii::$app->db->createCommand()->batchInsert(
                            TaskQueueOrder::tableName(),
                            [
                                'task_queue_id',
                                'order_id',
                                'uniqueid',
                                'created_at',
                                'updated_at'
                            ],
                            $dataForTaskQueueOrder
                        )->execute();
                    }
                }
            }

            if (!$error) {
                $transaction->commit();
            }
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        if (!$error) {
            if(count($dataForTasks) == 0){
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'У выбранных заказов отсутствуют данные о звонка')
                ];
            }else{
                $result = [
                    'success' => true,
                    'message' => yii::t('common', 'Задачи для экспорта успешно созданы ({count})', ['count' => count($dataForTasks)])
                ];
            }
        } else {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Произошла ошибка при попытке добавит задание экспорта в очередь')
            ];
        }

        return $result;
    }

    /**
     * @param $url
     * @return mixed
     */
    public function parseQueryParams($url)
    {
        $url = substr($url, 1);
        parse_str(urldecode($url), $params);

        return $params;
    }
}
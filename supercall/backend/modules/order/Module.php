<?php
namespace backend\modules\order;

use backend\components\base\Module as BackendModule;

/**
 * Class Module
 * @package backend\modules\order
 */
class Module extends BackendModule
{
    public $controllerNamespace = 'backend\modules\order\controllers';
}

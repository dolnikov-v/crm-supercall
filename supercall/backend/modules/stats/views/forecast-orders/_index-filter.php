<?php
use backend\modules\stats\models\ForecastOrders;
use common\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/** @var \backend\modules\stats\models\search\ForecastOrdersSearch $modelSearch */
/** @var array $types */
/** @var array $partners */
/** @var array $countries */
/** @var array $params */
?>

<?php


$form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'country')->select2List($countries, [
                'multiple' => true,
                'value' => isset($params['country']) ? $params['country'] : '',
                'placeholder' => yii::t('common', 'Укажите страну')
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'partner')->select2List($partners, [
                'value' => isset($params['partner']) ? $params['partner'] : '',
                'placeholder' => yii::t('common', 'Все')
            ]) ?>
        </div>

        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'type')->select2List(['' => '-'] + $types, [
                'multiple' => false,
                'value' => isset($params['type']) ? $params['type'] : '',
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'date_range')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'presetDropdown' => true,
                'hideInput' => true,
                'pluginOptions' => [
                    'autoUpdateOnInit' => true,
                    'autoUpdateInput' => true,
                    'timePicker' => true,
                    'timePicker24Hour' => true,
                    'timePickerIncrement' => 1,
                    'options' => [
                        'value' => date('Y/m/d 00:00:00') . ' - ' . date('Y/m/d 23:59:59')
                    ],
                    'locale' => [
                        'format' => 'DD/MM/YYYY HH:mm',
                    ],
                ],
                'pluginEvents' => [
                    'cancel.daterangepicker' => 'function(ev, picker) { $("input#orderreport-date_range").val(""); $(this).find("span.range-value").text("") }',
                ],
            ]); ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($modelSearch, 'group')->select2List(ForecastOrders::groupCollection(), [
                'value' => isset($params['group']) ? $params['group'] : ForecastOrders::GROUP_DAYS,
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
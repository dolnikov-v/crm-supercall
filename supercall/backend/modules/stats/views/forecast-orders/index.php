<?php

use common\widgets\base\Panel;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

$this->title = Yii::t('common', 'Прогнозирование количества заказов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

/** @var \backend\modules\stats\models\search\ForecastOrdersSearch $model */
/** @var array $types */
/** @var array $partners */
/** @var array $countries */
/** @var array $data */
/** @var array $params */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $model,
        'types' => $types,
        'partners' => $partners,
        'countries' => $countries,
        'params' => $params
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', ''),
    'showButtons' => true,
    'content' => Highcharts::widget([
        'options' => [
            'chart' => [
                'type' => 'line',
                'zoomType' => 'x',
                'events' => [
                    'redraw' => new JsExpression('function(e){ $(".highcharts-background").first().attr("stroke", "");}'),
                    'load' => new JsExpression('function(e){ $(".highcharts-background").first().attr("stroke", "");}')
                ],
            ],
            'title' => [
                'text' => yii::t('common', 'Количество заказов')
            ],
            'subtitle' => [
                'text' => ''
            ],
            'xAxis' => [
                'categories' => array_values($data['categories'])
            ],
            'yAxis' => [
                'title' => [
                    'text' => yii::t('common', 'Количество')
                ]
            ],
            'plotOptions' => [
                'line' => [
                    'dataLabels' => [
                        'enabled' => true
                    ],
                    'enableMouseTracking' => true
                ],
            ],
            'series' => $data['series'],
            'tooltip' => [
                'formatter' => new JsExpression('function(){
        		                                    var data = \'<b>\' + this.points[0].x + \'</b><br/>\';

                                                    for(var i in this.points){
                                                        data += \'<b>\' + this.points[i].series.name + \'</b>\' + \' - \' + this.points[i].y + "<br/>";
                                                    }

                                                    return data;

                }'),
                'shared' => true
            ],
            'credits' => ['enabled' => false],
        ],
        'scripts' => [
            'highcharts-more',   // enables supplementary chart types (gauge, arearange, columnrange, etc.)
            'modules/exporting', // adds Exporting button/menu to chart
            'themes/grid'        // applies global 'grid' theme to all charts
        ],
    ])

]) ?>



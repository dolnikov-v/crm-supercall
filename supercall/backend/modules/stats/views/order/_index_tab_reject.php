<?php
use common\components\grid\GridView;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\OrderReport $model */

?>

<?= GridView::widget([
        'dataProvider'     => $model->getRejectStats(),
        'showFooter'       => true,
        'footerRowOptions' => [
                'style' => 'font-weight:bold',
                'class' => 'success',
        ],
        'tableOptions'     => [
                'data-id'     => 'table-reject',
                'data-locale' => 'en-US',
        ],
        'options' => [
                'class' => 'table-responsive',
        ],
        'columns'          => $model->getRejectColumns(),
]) ?>

<script>
    var tableId = '[data-id="table-reject"]'
    $(tableId).bootstrapTable()

    mvFooterToHead(tableId)
</script>

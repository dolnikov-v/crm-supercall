<?php
use common\components\grid\GridView;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\OrderReport $model */

$reportData = $model->getApprovedStats();
?>

<?= GridView::widget([
        'dataProvider'     => $model->getApprovedStats(),
        'showFooter'       => true,
        'footerRowOptions' => [
                'style' => 'font-weight:bold',
                'class' => 'success',
        ],
        'tableOptions' => [
                'data-id'     => 'table-approved',
                'data-locale' => 'en-US',
        ],
        'options' => [
                'class' => 'table-responsive',
        ],
        'columns' => $model->getApprovedColumns(),
]) ?>

<script>
    var tableId = '[data-id="table-approved"]'
    $(tableId).bootstrapTable()

    mvFooterToHead(tableId)
</script>

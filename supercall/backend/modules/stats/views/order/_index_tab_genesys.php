<?php
use common\components\grid\GridView;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\OrderStats $model */

?>

<?= GridView::widget([
        'dataProvider'     => $model->getGenesysStats(),
        'showFooter'       => true,
        'footerRowOptions' => [
                'style' => 'font-weight:bold',
                'class' => 'success',
        ],
        'tableOptions'     => [
                'data-id'     => 'table-genesys',
                'data-locale' => 'en-US',
        ],
        'options'          => [
                'class' => 'table-responsive',
        ],
        'columns'          => $model->getGenesysColumns(),
]) ?>

<script>
    var tableId = '[data-id="table-genesys"]'
    $(tableId).bootstrapTable()

    mvFooterToHead(tableId)
</script>
<?php

use backend\modules\stats\assets\StatsAsset;
use backend\modules\stats\models\export\OrderExport;
use backend\modules\stats\models\OrderStats;
use common\widgets\base\Panel;
use common\widgets\export\Export;
use kartik\tabs\TabsX;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\modules\stats\assets\ExportStatsOrdersAsset;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\OrderReport $model_general */
/** @var OrderStats $model_stats */
/** @var array $groups */
/** @var array $types */
/** @var array $products */
/** @var array $countries */
/** @var array $teams */
/** @var array $partners */
/** @var boolean $contentVisible */

$this->title = Yii::t('common', 'Статистика по заказам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

StatsAsset::register($this);
ExportStatsOrdersAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $model_general,
        'groups' => $groups,
        'types' => $types,
        'products' => $products,
        'partners' => $partners,
        'countries' => $countries,
        'teams' => $teams
    ])
]) ?>

<?php if (yii::$app->user->can('stats.order.export')) : ?>

    <div class="row">
        <div class="col-lg-11"></div>
        <div class="col-lg-1">
            <?= Export::widget([
                'model' => OrderStats::className(),
                'controllerPathWithAction' => '/stats/order-export',
                'title' => yii::$app->formatter->asDateFullTime(time()) . '_export_stats_orders_',
                'queryParams' => Yii::$app->request->queryParams,
                'getAll' => true,
                'csvCharset' => 'utf-8',
                'blockClass' => 'pull-right',
                'blockStyle' => 'padding-bottom: 20px;',
                'xls' => true,
                'csv' => false,
                'word' => false,
                'html' => false,
                'pdf' => false,
            ]) ?>
        </div>
    </div>

<?php endif; ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статистика'),
    'id' => 'panel-order-stats',
    'contentPadding' => false,
    'content' => TabsX::widget([
        'items' => [
            [
                'label' => Yii::t('common', 'Статистика по заказам'),
                'content' => $this->render('_index_tab_general', [
                    'model' => $model_general,
                    'group' => ArrayHelper::getValue(\Yii::$app->request->get('OrderStats'), 'group'),
                ]),
                'linkOptions' => [
                    'data-type' => OrderExport::TAB_GENERAL
                ]
            ],
            [
                'label' => Yii::t('common', 'Одобрено с попыток'),
                'linkOptions' => [
                    'data-url' => Url::current(['tab' => OrderExport::TAB_ATTEMPTS]),
                    'data-type' => OrderExport::TAB_ATTEMPTS
                ],
            ],
            [
                'label' => Yii::t('common', 'Статистика одобренных'),
                'linkOptions' => [
                    'data-url' => Url::current(['tab' => OrderExport::TAB_APPROVED]),
                    'data-type' => OrderExport::TAB_APPROVED
                ],
            ],
            [
                'label' => Yii::t('common', 'Статистика отказов'),
                'linkOptions' => [
                    'data-url' => Url::current(['tab' => OrderExport::TAB_REJECT]),
                    'data-type' => OrderExport::TAB_REJECT
                ],
            ],
            [
                'label' => Yii::t('common', 'Статистика трешей'),
                'linkOptions' => [
                    'data-url' => Url::current(['tab' => OrderExport::TAB_TRASH]),
                    'data-type' => OrderExport::TAB_TRASH
                ],
            ],
            /*
            [
                'label' => Yii::t('common', 'Статистика Genesys'),
                'linkOptions' => [
                    'data-url' => Url::current(['tab' => 'genesys'])
                ],
            ]
            */
        ]
    ]),
]) ?>
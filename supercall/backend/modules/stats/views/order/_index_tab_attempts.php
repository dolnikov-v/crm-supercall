<?php
use common\components\grid\GridView;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\OrderReport $model */
?>

<?= GridView::widget([
    'dataProvider' => $model->getAttempts(),
    'showFooter'       => true,
    'footerRowOptions' => [
        'style' => 'font-weight:bold',
        'class' => 'success',
    ],
    'tableOptions'     => [
        'data-id'     => 'table-attempts',
        'data-locale' => 'en-US',
    ],
    'options' => [
        'class' => 'table-responsive',
    ],
    'columns' => $model->getAttemptsColumns(),
]) ?>

<script>
    var tableId = '[data-id="table-attempts"]'
    $(tableId).bootstrapTable()

    mvFooterToHead(tableId)
</script>
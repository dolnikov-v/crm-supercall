<?php
use common\components\grid\GridView;
use backend\modules\stats\assets\StatsOperatorLinksAsset;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\OrderReport $model */
$reportData = $model->getGeneralAggregate();
StatsOperatorLinksAsset::register($this);
?>

<?= GridView::widget([
    'dataProvider' => $reportData['dataProvider'],
    //'pager'       => $reportData['pager'],
    'showFooter' => true,
    'footerRowOptions' => [
        'style' => 'font-weight:bold',
        'class' => 'success',
    ],
    'tableOptions' => [
        'data-id' => 'table-general',
        'data-locale' => 'en-US',
    ],
    'options' => [
        'class' => 'table-responsive',
    ],
    'columns' => $model->getGeneralColumnsAggregate($group),
]) ?>

<?php $this->registerJs("
    $(function () {
        var tableId = '[data-id=\"table-general\"]'
        $(tableId).bootstrapTable()
        
        mvFooterToHead(tableId)
    });
" );
?>

<?php
use common\components\grid\GridView;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\OrderReport $model */

?>

<?= GridView::widget([
        'dataProvider'     => $model->getTrashStats(),
        'showFooter'       => true,
        'footerRowOptions' => [
                'style' => 'font-weight:bold',
                'class' => 'success',
        ],
        'tableOptions'     => [
                'data-id'     => 'table-trash',
                'data-locale' => 'en-US',
        ],
        'options' => [
                'class' => 'table-responsive',
        ],
        'columns'          => $model->getTrashColumns(),
]) ?>

<script>
    var tableId = '[data-id="table-trash"]'
    $(tableId).bootstrapTable()

    mvFooterToHead(tableId)
</script>
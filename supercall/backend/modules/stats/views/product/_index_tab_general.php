<?php
use common\components\grid\GridView;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\ProductReport $model */
$reportData = $model->getGeneralAggregate();
?>

<?= GridView::widget([
    'dataProvider' => $reportData['dataProvider'],
    'showFooter' => true,
    'footerRowOptions' => [
        'style' => 'font-weight:bold',
        'class' => 'success',
    ],
    'tableOptions' => [
        'data-id' => 'table-general',
        'data-locale' => 'en-US',
    ],
    'options' => [
        'class' => 'table-responsive',
    ],
    'columns' => $model->getGeneralColumnsAggregate($group),
]) ?>

<?php
use backend\modules\stats\assets\StatsAsset;
use common\widgets\base\Panel;
use kartik\tabs\TabsX;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\ProductReport $model_general */
/** @var array $groups */
/** @var array $types */
/** @var array $products */
/** @var array $countries */
/** @var array $teams */

$this->title = Yii::t('common', 'Статистика по продуктам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

StatsAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $model_general,
        'groups' => $groups,
        'types' => $types,
        'products' => $products,
        'partners' => $partners,
        'countries' => $countries,
        'teams' => $teams
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статистика'),
    'contentPadding' => false,
    'content' => $this->render('_index_tab_general', [
        'model' => $model_general,
        'group' => ArrayHelper::getValue(\Yii::$app->request->get('OrderStats'), 'group'),
    ]),
]) ?>

<?php
    $js = <<<JS
    var tableId = '[data-id="table-general"]'
    $(tableId).bootstrapTable()

    mvFooterToHead(tableId)
JS;

    $this->registerJs($js, $this::POS_LOAD)

?>


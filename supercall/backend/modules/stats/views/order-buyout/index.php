<?php

use backend\modules\stats\assets\StatsAsset;
use common\modules\order\models\OrderBuyout;
use common\widgets\base\Panel;
use kartik\tabs\TabsX;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\components\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var common\modules\order\models\OrderBuyout $model_general */
/** @var array $groups */
/** @var array $types */
/** @var array $products */
/** @var array $countries */
/** @var array $partners */
/** @var array $teams */
/** @var array $operators */
/** @var  integer $count */
/** @var array $sessionFilterData */
/** @var \yii\data\ArrayDataProvider $dataProviderWithOrders */
/** @var array $operatorsWithOrders */
/** @var integer $count */

$this->title = Yii::t('common', 'Статистика по выкупам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

StatsAsset::register($this);

$reportData = $model_general->getGeneralAggregate();
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $model_general,
        'groups' => $groups,
        'types' => $types,
        'products' => $products,
        'partners' => $partners,
        'countries' => $countries,
        'teams' => $teams,
        'sessionFilterData' => $sessionFilterData,
        'operators' => $operators
    ])
]) ?>


<?= $this->render('_index_tab_general', [
    'model' => $model_general,
    'group' => ArrayHelper::getValue(\Yii::$app->request->get('OrderStats'), 'group'),
    'count' => $count,
    'reportData' => $reportData,
    'dataProviderWithOrders' => $dataProviderWithOrders,
    'operatorsWithOrders' => $operatorsWithOrders,
]);
?>

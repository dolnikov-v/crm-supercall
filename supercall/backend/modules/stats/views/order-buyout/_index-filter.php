<?php
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use common\modules\order\models\Order;

/** @var common\modules\order\models\OrderBuyout $modelSearch */
/** @var array $groups */
/** @var array $types */
/** @var array $products */
/** @var array $countries */
/** @var array $operators */
/** @var array $teams */
/** @var array $partners */
/** @var  array $sessionFilterData */

$params = ArrayHelper::getValue($sessionFilterData, 'OperatorReport');
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?php
        echo $form->field($modelSearch, 'countries')->select2List($countries, [
            'multiple' => true,
            'placeholder' => yii::t('common', 'All')
        ]) ?>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'type')->select2List($types, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'date_range')->widget(DateRangePicker::className(), [
            'convertFormat' => false,
            'presetDropdown' => true,
            'hideInput' => true,
            'pluginOptions' => [
                'autoUpdateOnInit' => true,
                'autoUpdateInput' => true,
                'timePicker' => true,
                'timePicker24Hour' => true,
                'timePickerIncrement' => 1,
                'locale' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                ],
            ],
            'pluginEvents' => [
                'cancel.daterangepicker' => 'function(ev, picker) { $("input#orderreport-date_range").val(""); $(this).find("span.range-value").text("") }',
            ],
        ]); ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'username')->select2List($operators, [
            'prompt' => '—'
        ])->label(yii::t('common', 'Оператор')) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'product')->select2List($products, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'partner')->select2List($partners, [
            'prompt' => '—'
        ]) ?>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'team')->select2List($teams, [
            'multiple' => true,
        ])->label(Yii::t('common', 'Команда')) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'group')->select2List($groups, ['prompt' => yii::t('common', 'Оператор')]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

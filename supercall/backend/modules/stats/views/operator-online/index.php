<?php

use backend\modules\stats\assets\StatsAsset;
use backend\modules\stats\models\report\OperatorOnlineReport;
use backend\modules\stats\models\report\OperatorReport;
use common\components\grid\NumberColumn;
use common\components\grid\NumberTotalColumn;
use common\widgets\base\Panel;
use common\widgets\export\Export;
use kartik\tabs\TabsX;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\components\grid\GridView;
use backend\modules\stats\assets\ExportStatsOperatorAsset;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\OperatorReport $model_general */
/** @var array $countries */
/** @var array $teams */
/** @var array $operators */
/** @var  integer $count */
/** @var array $sessionFilterData */
/** @var \yii\data\ArrayDataProvider $dataProvider */
/** @var array $operatorsWithOrders */
/** @var integer $count */
/** @var array $groups */

$this->title = Yii::t('common', 'Статистика операторов по времени онлайн');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

StatsAsset::register($this);
ExportStatsOperatorAsset::register($this);

$reportData = $model_general->getGeneralAggregate();
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $model_general,
        'countries' => $countries,
        'teams' => $teams,
        'sessionFilterData' => $sessionFilterData,
        'operators' => $operators,
        'groups' => $groups
    ])
]) ?>

<div class="row">
    <div class="col-lg-12">
        <?= Export::widget([
            'controllerPathWithAction' => '/stats/operator-online-export',
            'model' => OperatorReport::className(),
            'title' => yii::$app->formatter->asDateFullTime(time()) . '_export_stats_operator_',
            'queryParams' => Yii::$app->request->queryParams,
            'getAll' => true,
            'csvCharset' => 'utf-8',
            'blockClass' => 'pull-right',
            'blockStyle' => 'padding: 5px;',
            'xls' => true,
            'csv' => false,
            'word' => false,
            'html' => false,
            'pdf' => false,
        ]) ?>
    </div>
</div>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список операторов ({count})', ['count' => $count]),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'showFooter' => true,
        'footerRowOptions' => [
            'style' => 'font-weight:bold',
            'class' => 'success',
        ],
        'tableOptions' => [
            'data-id' => 'table-general-with-orders',
            'data-locale' => 'en-US',
        ],
        'options' => [
            'class' => 'table-responsive',
        ],
        'columns' => [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'time'],
                'contentOptions' => [
                        'class' => 'width-150'
                ],
                'value' => function ($row) use ($model_general){
                    return yii::t('common', $model_general->formatGroupView($row));
                },
                'footer' => \Yii::t('common', 'Всего:'),
            ],
            [
                'attribute' => 'username',
                'label' => \Yii::t('common', 'Оператор'),
                'contentOptions' => [
                    'class' => 'width-150'
                ],
                'visible' => $model_general->group != OperatorOnlineReport::GROUP_OPERATOR,
                'value' => function ($row) {
                    return $row['username'];
                }
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'online',
                'label' => \Yii::t('common', 'Время онлайн'),
                'value' => function ($row) {
                    return Yii::$app->formatter->asDecimal($row['online'], 2);
                },
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'online-data'],
            ]
        ]
    ]),
    /** @var \yii\data\ActiveDataProvider $reportData ['dataProvider'] */
    'footer' => !yii::$app->request->get('rest')  ? LinkPager::widget(['pagination' => $dataProvider->getPagination()]) : ''
]); ?>

<?php
$js = <<<JS
    var tableWithOrdersId = '[data-id="table-general-with-orders"]';
    $(tableWithOrdersId).bootstrapTable();

    mvFooterToHead(tableWithOrdersId);
JS;

$this->registerJs($js, $this::POS_LOAD);
?>

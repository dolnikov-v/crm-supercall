<?php

use backend\modules\stats\assets\OperatorAsset;
use common\assets\base\ParseUrlAsset;
use backend\modules\stats\assets\QueueAsset;
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use common\modules\order\models\Order;

/** @var \backend\modules\stats\models\report\OperatorReport $modelSearch */
/** @var array $groups */
/** @var array $types */
/** @var array $products */
/** @var array $countries */
/** @var array $operators */
/** @var array $teams */
/** @var array $partners */
/** @var  array $sessionFilterData */

$params = ArrayHelper::getValue($sessionFilterData, 'OperatorReport');
QueueAsset::register($this);
OperatorAsset::register($this);
ParseUrlAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?php
        echo $form->field($modelSearch, 'countries')->select2List($countries, [
            'value' => isset($params['countries']) ? $params['countries'] : '',
            'multiple' => true,
            'placeholder' => yii::t('common', 'Все')
        ]) ?>
    </div>

    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'type')->select2List($types, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'date_range')->widget(DateRangePicker::className(), [
            'convertFormat' => false,
            'presetDropdown' => true,
            'hideInput' => true,
            'pluginOptions' => [
                'autoUpdateOnInit' => true,
                'autoUpdateInput' => true,
                'timePicker' => true,
                'timePicker24Hour' => true,
                'timePickerIncrement' => 1,
                'locale' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                ],
            ],
            'pluginEvents' => [
                'cancel.daterangepicker' => 'function(ev, picker) { $("input#orderreport-date_range").val(""); $(this).find("span.range-value").text("") }',
            ],
        ]); ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'username')->select2List($operators, [
            'multiple' => true
        ])->label(yii::t('common', 'Оператор')) ?>
        <div class="loader mp-13 hidden"></div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'product')->select2List($products, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'partner')->select2List($partners, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'queue')->select2List([],[
            'prompt' => '-',
            'multiple' => true,
            'placeholder' => yii::t('common', 'Все')
        ]) ?>
        <div class="loader mp-13 hidden"></div>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'team')->select2List($teams, [
            'multiple' => true,
        ])->label(Yii::t('common', 'Команда')) ?>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'group')->select2List($groups, ['prompt' => yii::t('common', 'Оператор')]) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($modelSearch, 'statistics_by_orders')->checkbox([
            'checked' => $modelSearch->statistics_by_orders,
            'value' => true,
            'unchecked_value' => false,
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

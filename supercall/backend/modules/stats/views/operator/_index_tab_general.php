<?php

use common\components\grid\GridView;
use backend\modules\stats\assets\StatsOperatorLinksAsset;
use yii\bootstrap\Tabs;
use common\widgets\base\LinkPager;
use common\widgets\base\Panel;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\OperatorReport $model */
/** @var array $reportData */
/** @var \yii\data\ArrayDataProvider $dataProviderWithOrders */
/** @var array $operatorsWithOrders */
/** @var integer $count */
StatsOperatorLinksAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Операторы с заказами ({count})', ['count' => $count]),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProviderWithOrders,
        'showFooter' => true,
        'footerRowOptions' => [
            'style' => 'font-weight:bold',
            'class' => 'success',
        ],
        'tableOptions' => [
            'data-id' => 'table-general-with-orders',
            'data-locale' => 'en-US',
        ],
        'options' => [
            'class' => 'table-responsive',
        ],
        'columns' => $model->getGeneralColumnsAggregate()
    ]),
    /** @var \yii\data\ActiveDataProvider $reportData ['dataProvider'] */
    'footer' => LinkPager::widget(['pagination' => $dataProviderWithOrders->getPagination()])
]); ?>

<?php
$js = <<<JS
    var tableWithOrdersId = '[data-id="table-general-with-orders"]';
    $(tableWithOrdersId).bootstrapTable();

    mvFooterToHead(tableWithOrdersId);
JS;

$this->registerJs($js, $this::POS_LOAD);
?>
<?php

use backend\modules\stats\assets\StatsAsset;
use backend\modules\stats\models\report\OperatorReport;
use common\widgets\base\Panel;
use common\widgets\export\Export;
use kartik\tabs\TabsX;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\components\grid\GridView;
use backend\modules\stats\assets\ExportStatsOperatorAsset;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\OperatorReport $model_general */
/** @var array $groups */
/** @var array $types */
/** @var array $products */
/** @var array $countries */
/** @var array $partners */
/** @var array $teams */
/** @var array $operators */
/** @var  integer $count */
/** @var array $sessionFilterData */
/** @var \yii\data\ArrayDataProvider $dataProviderWithOrders */
/** @var array $operatorsWithOrders */
/** @var integer $count */

$this->title = Yii::t('common', 'Статистика по операторам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

StatsAsset::register($this);
ExportStatsOperatorAsset::register($this);

$reportData = $model_general->getGeneralAggregate();
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'modelSearch' => $model_general,
        'groups' => $groups,
        'types' => $types,
        'products' => $products,
        'partners' => $partners,
        'countries' => $countries,
        'teams' => $teams,
        'sessionFilterData' => $sessionFilterData,
        'operators' => $operators
    ])
]) ?>

<div class="row">
    <div class="col-lg-12">
        <?= Export::widget([
            'controllerPathWithAction' => '/stats/operator-export',
            'model' => OperatorReport::className(),
            'title' => yii::$app->formatter->asDateFullTime(time()) . '_export_stats_operator_',
            'queryParams' => Yii::$app->request->queryParams,
            'getAll' => true,
            'csvCharset' => 'utf-8',
            'blockClass' => 'pull-right',
            'blockStyle' => 'padding: 5px;',
            'xls' => true,
            'csv' => false,
            'word' => false,
            'html' => false,
            'pdf' => false,
        ]) ?>
    </div>
</div>

<?= $this->render('_index_tab_general', [
    'model' => $model_general,
    'group' => ArrayHelper::getValue(\Yii::$app->request->get('OrderStats'), 'group'),
    'count' => $count,
    'reportData' => $reportData,
    'dataProviderWithOrders' => $dataProviderWithOrders,
    'operatorsWithOrders' => $operatorsWithOrders,
]);
?>

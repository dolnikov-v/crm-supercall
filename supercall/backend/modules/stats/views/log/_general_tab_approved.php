<?php
use common\components\grid\GridView;

?>

<?= GridView::widget([
    'dataProvider'     => $model->getApprovedStats(),
    'showFooter'       => true,
    'footerRowOptions' => [
        'style' => 'font-weight:bold',
        'class' => 'success',
    ],
    'tableOptions' => [
        'data-id'     => 'table-approved',
        'data-locale' => 'en-US',
    ],
    'options' => [
        'class' => 'table-responsive',
    ],
    'columns' => $model->getApprovedColumns(),
]) ?>

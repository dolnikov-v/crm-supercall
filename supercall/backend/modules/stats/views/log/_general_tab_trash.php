<?php
    use common\components\grid\GridView;

?>

<?= GridView::widget([
    'dataProvider'     => $model->getTrashStats(),
    'showFooter'       => true,
    'footerRowOptions' => [
        'style' => 'font-weight:bold',
        'class' => 'success',
    ],
    'tableOptions'     => [
        'data-id'     => 'table-trash',
        'data-locale' => 'en-US',
    ],
    'options' => [
        'class' => 'table-responsive',
    ],
    'columns'          => $model->getTrashColumns(),
]) ?>

<?php
use common\components\grid\GridView;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\LogListStats $model */

echo GridView::widget([
    'dataProvider' => $model->getListStats(),
    'columns' => $model->getListColumns(),
]);
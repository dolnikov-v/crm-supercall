<?php
use common\components\grid\GridView;

/** @var \backend\modules\stats\models\report\LogReport $model */
?>

<?= GridView::widget([
    'dataProvider'     => $model->getRejectStats(),
    'showFooter'       => true,
    'footerRowOptions' => [
        'style' => 'font-weight:bold',
        'class' => 'success',
    ],
    'tableOptions'     => [
        'data-id'     => 'table-reject',
        'data-locale' => 'en-US',
    ],
    'options' => [
        'class' => 'table-responsive',
    ],
    'columns'          => $model->getRejectColumns(),
]) ?>


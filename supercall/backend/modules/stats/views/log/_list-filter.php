<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

/** @var \backend\modules\stats\models\LogListStats $model */
/** @var array $products */
/** @var array $operators */
/** @var array $statuses */
/** @var array $countries */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('list'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'team_id')->select2List($teams, [
            'multiple' => true
        ])->label( Yii::t('common', 'Команда') ); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'operator')->select2List($operators, [
            'multiple' => true,
            'placeholder' => yii::t('common', 'Все')
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?php
        echo $form->field($model, 'countries')->select2List($countries, [
            'value' => $model->countries,
            'multiple' => true,
            'placeholder' => yii::t('common', 'Все')
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'date_range')->widget(DateRangePicker::className(), [
            'convertFormat'  => false,
            'presetDropdown' => true,
            'hideInput'      => true,
            'pluginOptions'  => [
                'autoUpdateOnInit' => true,
                'autoUpdateInput' => true,
                'timePicker' => true,
                'timePicker24Hour' => true,
                'timePickerIncrement' => 1,
                'locale' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                ],
            ],
            'pluginEvents'  => [
                'cancel.daterangepicker' => 'function(ev, picker) { $("input#ordersearch-createdrange").val(""); $(this).find("span.range-value").text("") }',
            ],
        ]); ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'order')->textInput() ?>
    </div>

    <div class="col-lg-4">
        <?= $form->field($model, 'product')->select2List($products, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'type')->select2List($types, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'status')->select2List($statuses, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'partner')->select2List($partners, [
            'prompt' => '—'
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['list', 'reset' => true])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
use backend\modules\stats\assets\StatsAsset;
use common\components\grid\GridView;
use common\widgets\base\Panel;
use kartik\tabs\TabsX;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\report\LogReport $model */
/** @var array $groups */
/** @var array $types */
/** @var array $products */
/** @var array $countries */

$this->title = Yii::t('common', 'Статистика по заказам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
    
StatsAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_general-filter', [
        'modelSearch' => $model,
        'groups'      => $groups,
        'types'       => $types,
        'products'    => $products,
        'countries'   => $countries,
        'partners'    => $partners,
        'teams' => $teams
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статистика'),
    'contentPadding' => false,
    'content' => TabsX::widget([
        'containerOptions' => [
            'id' => 'general-table',
        ],
        'items' => [
            [
                'label' => Yii::t('common', 'Статистика по заказам'),
                'content' => GridView::widget([
                    'dataProvider' => $model->getGeneralAggregate()['dataProvider'],
                    'showFooter' => true,
                    'footerRowOptions' => ['style' => 'font-weight:bold', 'class' => 'success'],
                    'tableOptions'     => [
                        'data-id'     => 'table-log-general',
                        'data-locale' => 'en-US',
                    ],
                    'options' => [
                        'class' => 'table-responsive',
                    ],
                    'columns' => $model->getGeneralColumnsAggregate(),
                ]),
            ],
            [
                'label' => Yii::t('common', 'Статистика одобренных'),
                'options' => ['id' => 'approved'],
                'content' => $this->render('_general_tab_approved', [
                    'model' => $model,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Статистика отказов'),
                'options' => ['id' => 'reject'],
                'content' => $this->render('_general_tab_reject', [
                    'model' => $model,
                ]),
            ],
            [
                'label' => Yii::t('common', 'Статистика трешей'),
                'options' => ['id' => 'trash'],
                'content' => $this->render('_general_tab_trash', [
                    'model' => $model,
                ]),
            ],
            [
                'label' => Yii::t('common', 'История изменений'),
                'url' => ['list']
            ],
        ]
    ]),
]) ?>

<?php
    // При переходе из истории открываем сразу нужную вкладку
    if ($tab = \Yii::$app->request->get('tab'))
    $this->registerJs("
        $('#general-table ul a[href=\"#" .$tab. "\"]').tab('show') 
    ")
?>
<?php $this->registerJs("
    $(function () {
        var tableId = '[data-id=\"table-log-general\"]'
        $(tableId).bootstrapTable()
        
        mvFooterToHead(tableId)
        
        var tableIdApp = '[data-id=\"table-approved\"]'
        $(tableIdApp).bootstrapTable()
        
        mvFooterToHead(tableIdApp)
        
        var tableIdRej = '[data-id=\"table-reject\"]'
        $(tableIdRej).bootstrapTable()
        
        mvFooterToHead(tableIdRej)
        
        var tableIdTrash = '[data-id=\"table-trash\"]'
        $(tableIdTrash).bootstrapTable()
    
        mvFooterToHead(tableIdTrash)
        
        
        $('body').on('click', '.extra-trigger', function(){
            var trigger = $(this);
            if (trigger.data('loading')) {
                return;
            }
            trigger.data('loading', true);
            
            var row = trigger.closest('tr');
            
            if (!trigger.hasClass('success')){
                row.find('.extra-trigger.success').click();
                trigger.addClass('success');
                
                var extra_content = '';
                var queryData = $('#form-filter').serializeArray();
                var status = trigger.attr('data-status');
                queryData.push({
                    name: 'LogReport[status]',
                    value: status
                });
                
                var group = $('#logreport-group').val();
                var groupValue = false;
                
                groupValue = row.find('.row-group').data('group');
                if (group=='product') {
                    queryData.push({
                        name: 'LogReport[product]',
                        value: groupValue
                    });
                } else if (group=='operator') {
                    queryData.push({
                        name: 'LogReport[operator]',
                        value: groupValue
                    });
                } else if (group=='country') {
                    queryData.push({
                        name: 'LogReport[country]',
                        value: groupValue
                    });
                } else {
                    queryData.push({
                        name: 'LogReport[period]',
                        value: groupValue
                    });    
                }
                
                
                $.get('" . \yii\helpers\Url::to(['/stats/log/details']) . "', queryData, function(html) {
                    extra_content = $('<td class=\"details\">' + html + '</td>');
                    extra_content.attr('colspan', 15).removeClass('hidden');
                    $('<tr></tr>').append(extra_content).insertAfter(row);
                    trigger.data('loading', false);
                });    
            }
            else{
                trigger.removeClass('success');     
                var extra_content = row.next().find('>td');
                extra_content.removeAttr('colspan').addClass('hidden').appendTo(row);
                row.next().remove();
                trigger.data('loading', false);
            }
        });
        
        $('body').on('click', '.details .pagination a', function(e) {
            e.preventDefault();
            var that = $(this);
            $.get(that.attr('href'), function(html) {
                that.parents('.details').html(html);
            });
        });
    });
" ); ?>

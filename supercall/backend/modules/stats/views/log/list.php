<?php
use common\components\grid\GridView;
use common\widgets\base\Panel;
use kartik\tabs\TabsX;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \backend\modules\stats\models\LogListStats $model */
/** @var array $products */
/** @var array $operators */
/** @var array $statuses */
/** @var array $partners */

$this->title = Yii::t('common', 'Статистика по заказам');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_list-filter', [
        'model'     => $model,
        'products'  => $products,
        'operators' => $operators,
        'teams'     => $teams,
        'statuses'  => $statuses,
        'types'     => $types,
        'partners'  => $partners,
        'countries' => $countries,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Статистика'),
    'contentPadding' => false,
    'content' => TabsX::widget([
        'items' => [
            [
                'label' => Yii::t('common', 'Статистика по заказам'),
                'url' => ['general']
            ],
            [
                'label' => Yii::t('common', 'Статистика одобренных'),
                'url' => [Url::to(['general', 'tab' => 'approved'])],
            ],
            [
                'label' => Yii::t('common', 'Статистика отказов'),
                'url' => [Url::to(['general', 'tab' => 'reject'])],
            ],
            [
                'label' => Yii::t('common', 'Статистика трешей'),
                'url' => [Url::to(['general', 'tab' => 'trash'])],
            ],
            [
                'label' => Yii::t('common', 'История изменений'),
                'content' => GridView::widget([
                    'dataProvider' => $model->getList(),
                    'layout'=>"<div align='right'>{summary}</div>{items}{pager}",
                    'columns' => $model->getListColumns($products),
                ]),
                'active' => true
            ],
        ]
    ]),
    'footer' => \yii\widgets\LinkPager::widget(['pagination' => $model->getListStats()->getPagination()])
]) ?>
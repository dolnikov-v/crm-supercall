<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

/** @var \common\models\search\ProductSearch $modelSearch */
/** @var array $groups */
/** @var array $types */
/** @var array $products */
/** @var array $countries */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('general'),
    'method' => 'get',
    'enableClientValidation' => false,
    'id' => 'form-filter'
]); ?>
<div class="row">
    <div class="col-lg-4">
        <?php
        echo $form->field($modelSearch, 'countries')->select2List($countries, [
            'value' => $modelSearch->countries,
            'multiple' => true,
            'placeholder' => yii::t('common', 'Все')
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'group')->select2List($groups) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'date_range')->widget(DateRangePicker::className(), [
            'convertFormat'  => false,
            'presetDropdown' => true,
            'hideInput'      => true,
            'pluginOptions'  => [
                'autoUpdateOnInit' => true,
                'autoUpdateInput' => true,
                'timePicker' => true,
                'timePicker24Hour' => true,
                'timePickerIncrement' => 1,
                'locale' => [
                    'format' => 'DD/MM/YYYY HH:mm',
                ],
            ],
            'pluginEvents'  => [
                'cancel.daterangepicker' => 'function(ev, picker) { $("input#logreport-date_range").val(""); $(this).find("span.range-value").text("") }',
            ],
        ]); ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'type')->select2List($types, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'product')->select2List($products, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'partner')->select2List($partners, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'team')->select2List($teams, [
            'multiple' => true
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['general', 'reset' => true])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use backend\assets\SiteAsset;
use common\modules\order\models\Order;
use common\models\Product;

/** @var  $modelSearch */
/** @var  array $products */

SiteAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('products'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">
        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= $form->field($modelSearch, 'groupBy')->select2List($modelSearch->getDateInterval())->label(Yii::t('common', 'Группировка')) ?>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= $form->field($modelSearch, 'period')->widget(DateRangePicker::className(), [
                'convertFormat'  => false,
                'presetDropdown' => true,
                'hideInput'      => true,
                'pluginOptions'  => [
                    'autoUpdateOnInit' => true,
                    'autoUpdateInput' => true,
                    'timePicker' => true,
                    'timePicker24Hour' => true,
                    'timePickerIncrement' => 1,
                    'options' => [
                        'value' => date('Y/m/d 00:00:00') . ' - ' . date('Y/m/d 23:59:59')
                    ],
                    'locale' => [
                        'format' => 'YYYY/MM/DD HH:mm:ss',
                    ],
                ],
                'pluginEvents'  => [
                    'cancel.daterangepicker' => 'function(ev, picker) { $("input#orderreport-date_range").val(""); $(this).find("span.range-value").text("") }',
                ],
            ]); ?>

        </div>
        <div class="col-lg-6 col-sm-3 col-xs-4">
            <?php
            echo $form->field($modelSearch, 'countries')->select2List($countries, [
                'multiple' => true,
                'placeholder' => yii::t('common', 'Все')
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-sm-4 col-xs-4">
            <?php
            echo $form->field($modelSearch, 'status')->select2List(Order::getStatusesCollection(), [
                'multiple' => true,
                'placeholder' => yii::t('common', 'Все')
            ]) ?>
        </div>
        <div class="col-lg-4 col-sm-4 col-xs-4">
            <?php
            echo $form->field($modelSearch, 'product')->select2List($products, [
                'multiple' => true,
                'placeholder' => 'Все'
            ]) ?>
        </div>
        <div class="col-lg-4 col-sm-4 col-xs-4">
            <?= $form->field($modelSearch, 'team')->select2List($teams, [
                'multiple' => true,
            ])->label(Yii::t('common', 'Команда')) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
<?php
use common\widgets\base\Panel;
use common\helpers\grid\DataProvider;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use yii\helpers\Url;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use yii\widgets\LinkPager;


/** @var  $modelSearch */
/** @var  $dataProvider */
/** @var  array $products */


$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $title];
?>

<?=Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render($filterTemplate, [
        'modelSearch' => $modelSearch,
        'countries' => $countries,
        'teams' => $teams,
        'products' => $products
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с данными'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {

        },
        'columns' => $columns,
    ]),
    'footer' =>  LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>


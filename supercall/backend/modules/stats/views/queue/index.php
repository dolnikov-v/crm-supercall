<?php

use common\widgets\base\Panel;
use common\components\grid\GridView;
use yii\widgets\LinkPager;

$this->title = Yii::t('common', 'Статистика по очередям');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статистика'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var array $columns */
/** @var \backend\modules\stats\models\search\QueueSearch $modelSearch */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
        'countries' => $countries
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица статистика'),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active > 0 ? '' : 'text-muted'),
            ];
        },
        'showFooter' => false,
        'footerRowOptions' => [
            'style' => 'font-weight:bold',
            'class' => 'success',
        ],
        'tableOptions' => [
            'data-id' => 'table-stats-queue',
        ],
        'options' => [
            'class' => 'table-responsive',
        ],
        'columns' => $columns
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]); ?>

<script>
    var tableId = '[data-id="table-stats-queue"]';
</script>

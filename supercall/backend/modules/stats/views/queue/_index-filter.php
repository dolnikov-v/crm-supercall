<?php

use backend\modules\stats\models\Queue;
use common\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\modules\stats\assets\QueueAsset;
use common\assets\base\ParseUrlAsset;

/** @var \backend\modules\stats\models\search\QueueSearch $modelSearch */

QueueAsset::register($this);
ParseUrlAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'countries')->select2List($countries, [
                'prompt' => '-',
                'multiple' => false,
                'placeholder' => yii::t('common', 'Все')
            ]) ?>
        </div>

        <div class="col-lg-3 search-by-name">
            <?= $form->field($modelSearch, 'queue')->select2List([], [
                'prompt' => '-',
                'multiple' => true,
                'placeholder' => yii::t('common', 'Все'),
            ]) ?>
            <div class="loader mp-13 hidden"></div>
        </div>

        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'date_type')->select2List([
                Queue::IGNORE_DATE_TYPE => yii::t('common', 'Игнорировать'),
                Queue::PARTNER_CREATED_DATE_TYPE => yii::t('common', 'Дата создания заказа у партнёра'),
                Queue::CC_CREATED_DATE_TYPE => yii::t('common', 'Дата создания заказа в КЦ'),
            ], [
                'multiple' => false,
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'date_range')->widget(DateRangePicker::className(), [
                'convertFormat' => false,
                'presetDropdown' => true,
                'hideInput' => true,
                'pluginOptions' => [
                    'autoUpdateOnInit' => true,
                    'autoUpdateInput' => false,
                    'timePicker' => true,
                    'timePicker24Hour' => true,
                    'timePickerIncrement' => 1,
                    'locale' => [
                        'format' => 'DD/MM/YYYY H:mm',
                    ],
                ],
                'pluginEvents' => [
                    'cancel.daterangepicker' => 'function(ev, picker) { $("input#ordersearch-updatedrange").val(""); $(this).find("span.range-value").text("") }',
                ],
            ])->label(yii::t('common', 'Дата')); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'partner_id')->select2List(
                    ArrayHelper::map(yii::$app->user->identity->partners, 'id', 'name'), [
                'prompt' => '-',
                'multiple' => false,
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
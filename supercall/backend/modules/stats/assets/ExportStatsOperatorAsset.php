<?php

namespace backend\modules\stats\assets;

use yii\web\AssetBundle;

/**
 * Class ExportStatsOperatorAsset
 * @package backend\modules\stats\assets
 */
class ExportStatsOperatorAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/stats/export/';

    public $js = [
        'export-stats-operator.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
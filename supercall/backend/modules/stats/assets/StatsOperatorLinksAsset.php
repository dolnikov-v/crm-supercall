<?php

namespace backend\modules\stats\assets;


use yii\web\AssetBundle;

class StatsOperatorLinksAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/stats';

    public $js = [
        'stats-operator-links.js'
    ];
}
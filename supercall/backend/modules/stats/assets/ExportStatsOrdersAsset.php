<?php

namespace backend\modules\stats\assets;

use yii\web\AssetBundle;

/**
 * Class ExportStatsOrdersAsset
 * @package backend\modules\stats\assets
 */
class ExportStatsOrdersAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/stats/export/';

    public $js = [
        'export-stats-orders.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
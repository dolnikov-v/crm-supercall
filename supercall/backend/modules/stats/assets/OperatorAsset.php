<?php

namespace backend\modules\stats\assets;

use yii\web\AssetBundle;

/**
 * Class OperatorAsset
 * @package backend\modules\stats\assets
 */
class OperatorAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/stats/operator';

    public $css = [
        'operator.css'
    ];

    public $js = [
        'operator.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

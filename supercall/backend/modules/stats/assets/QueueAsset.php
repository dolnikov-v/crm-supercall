<?php

namespace backend\modules\stats\assets;

use yii\web\AssetBundle;

/**
 * Class QueueAsset
 * @package backend\modules\stats\assets
 */
class QueueAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/stats/queue';

    public $css = [
        'queue.css'
    ];

    public $js = [
        'queue.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
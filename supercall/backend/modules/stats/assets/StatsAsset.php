<?php
namespace backend\modules\stats\assets;


use yii\web\AssetBundle;

/**
 * Class StatsAsset
 * @package backend\modules\stats\assets
 */
class StatsAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/stats';
    public $css = [
        'bootstrap-table/bootstrap-table.css',
    ];
    
    public $js = [
        '//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js',
        'bootstrap-table/stats.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}

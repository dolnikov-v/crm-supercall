<?php
namespace backend\modules\stats;

use backend\components\base\Module as BackendModule;

/**
 * Class Module
 * @package backend\modules\stats
 */
class Module extends BackendModule
{
    public $controllerNamespace = 'backend\modules\stats\controllers';
}

<?php
namespace backend\modules\stats\controllers;

use backend\components\web\Controller;
use backend\modules\stats\models\report\ProductReport;
use common\models\Country;
use common\models\Product;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\Teams;

class ProductController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex($tab = null)
    {
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);

        $model_general = new ProductReport();

        $model_general->load($query);

        if (is_null($model_general->date_range)) {
            $model_general->date_range = date('d/m/Y 00:00') . ' - ' . date('d/m/Y 23:59');
        }
        $date = Yii::$app->formatter->asConvertToFrom($model_general->date_range);
        $model_general->date_start = $date->from->format('d.m.Y H:i');
        $model_general->date_end = $date->to->format('d.m.Y H:i');

        $countries = [];

        if(is_array(Yii::$app->user->countries)){
            foreach(Yii::$app->user->countries as $k => $country){
                $countries[$country->id] = yii::t('common', $country->name);
            }
        }

        if ($tab) {
            $html = $this->renderPartial('_index_tab_' . $tab, ['model' => $model_general]);
            return Json::encode($html);
        } else {
            $userModel = yii::$app->user->getUserModel();
            $teams = $userModel->getTeamsByUser();
            $teams_list = is_array($teams) ? ArrayHelper::map($teams, 'id', 'name') : [];

            return $this->render('index', [
                'model_general' => $model_general,
                'contentVisible' => Yii::$app->session->get($filterSessId . '.isVisible', true),
                'groups' => ProductReport::getGroupsCollection(),
                'types' => OrderType::find()->collection('common'),
                'products' => Product::getActiveListProducts(),
                'countries' => $countries,
                'partners' => ArrayHelper::map(Partner::find()->active()->all(), 'id', 'name'),
                'teams' => $teams_list
            ]);
        }
    }
}
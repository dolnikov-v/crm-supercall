<?php
namespace backend\modules\stats\controllers;

use backend\components\web\Controller;
use backend\modules\stats\models\LogListStats;
use backend\modules\stats\models\report\LogReport;
use common\components\db\ActiveQuery;
use common\models\Product;
use common\models\Teams;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLogs;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use Yii;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/**
 * Class LogController
 * @package backend\modules\stats\controllers
 */
class LogController extends Controller
{
    /**
     * @return string
     */
    public function actionGeneral()
    {
        $model_olap = new LogReport();
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);
        $model_olap->load($query);

        if (is_null($model_olap->date_range)) {
            $model_olap->date_range = date('d/m/Y 00:00') . ' - ' . date('d/m/Y 23:59');
        }
        $date = Yii::$app->formatter->asConvertToFrom($model_olap->date_range);
        $model_olap->date_start = $date->from->format('d.m.Y H:i');
        $model_olap->date_end = $date->to->format('d.m.Y H:i');

        $countries = [];

        if(is_array(Yii::$app->user->countries)){
            foreach(Yii::$app->user->countries as $k => $country){
                $countries[$country->id] = yii::t('common', $country->name);
            }
        }

        $userModel = yii::$app->user->getUserModel();
        $teams = $userModel->getTeamsByUser();
        $teams_list = is_array($teams) ? ArrayHelper::map($teams, 'id', 'name') : [];

        return $this->render('general', [
            'model'     => $model_olap,
            'contentVisible' => Yii::$app->session->get($filterSessId.'.isVisible', true),
            'groups'    => LogReport::getGroupsCollection(),
            'types'     => OrderType::find()->collection('common'),
            'products'  => Product::find()->collection(),
            'countries' => $countries,
            'teams' => $teams_list,
            'partners'  => ArrayHelper::map( Partner::find()->active()->all(), 'id', 'name' ),
        ]);
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $model = new LogListStats();
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);
        $model->load($query);

        if (is_null($model->date_range)) {
            $model->date_range = date('d/m/Y 00:00') . ' - ' . date('d/m/Y 23:59');
        }
        $date = Yii::$app->formatter->asConvertToFrom($model->date_range);
        $model->date_start = $date->from->format('d.m.Y H:i');
        $model->date_end = $date->to->format('d.m.Y H:i');
        $model->country = Yii::$app->user->country->id;

        $operators = (new ActiveQuery(OrderLogs::className()))
            ->asArray(true)
            ->select('operator')
            ->where(['is not', 'operator', null])
            ->groupBy('operator')
            ->all();
        $operators = ArrayHelper::map($operators, 'operator', 'operator');

        $users = ArrayHelper::map(User::find()->all(), 'username', 'username');

        $operators = ArrayHelper::merge($operators, $users);

        $countries = [];

        if(is_array(Yii::$app->user->countries)){
            foreach(Yii::$app->user->countries as $k => $country){
                $countries[$country->id] = $country->name;
            }
        }

        return $this->render('list', [
            'model'          => $model,
            'contentVisible' => Yii::$app->session->get($filterSessId.'.isVisible', true),
            'products'       => Product::find()->collection(),
            'operators'      => $operators,
            'teams'          => Teams::find()->active()->collection(),
            'types'          => OrderType::find()->collection(),
            'statuses'       => Order::getStatusesCollection(),
            'partners'       => ArrayHelper::map(Partner::find()->active()->all(), 'id', 'name'),
            'countries'      => $countries,
        ]);
    }

    /**
     * @return string
     */
    public function actionDetails()
    {
        $model = new LogReport();

        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);
        $model->load($query);

        if (is_null($model->date_range)) {
            $model->date_range = date('d/m/Y 00:00') . ' - ' . date('d/m/Y 23:59');
        }
        $date = Yii::$app->formatter->asConvertToFrom($model->date_range);
        $model->date_start = $date->from->format('d.m.Y H:i');
        $model->date_end = $date->to->format('d.m.Y H:i');

        return GridView::widget([
            'dataProvider' => $model->getDetails(),
            'columns' => $model->getDetailsColumns()
        ]);
    }
}

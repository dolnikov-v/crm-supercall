<?php

namespace backend\modules\stats\controllers;

use backend\components\web\Controller;
use backend\models\TaskQueue;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class OrderExportController
 * @package backend\modules\stats\controllers
 */
class OrderExportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $param
     * @return string
     */
    public function generateName($param)
    {
        return 'task_#' . $param['id'] . '_' . TaskQueue::TYPE_STATS_ORDER_IMPORT . '_' . $param['tab'] . '_' . $param['typeExport'] . '_' . time();
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $post = yii::$app->request->post();

        $taskQueue = new TaskQueue([
            'type' => TaskQueue::TYPE_STATS_ORDER_IMPORT,
            'user_id' => yii::$app->user->identity->id,
            'path' => '',
            'status' => TaskQueue::STATUS_READY,
            'error' => null,
            'created_at' => time(),
            'updated_at' => time(),
            'query' => Json::encode([
                'tab' => $post['tab'],
                'format' => $post['typeExport'],
                'group' => $post['group'],
                'params' => $post['queryParams']
            ])
        ]);

        if ($taskQueue->save()) {
            $result = [
                'success' => true,
                'message' => yii::t('common', 'Задание #{id}, на экспорт статистики заказов, успешно создано', ['id' => $taskQueue->id])
            ];

            $taskQueue->name = $this->generateName(ArrayHelper::merge($post, ['id' => $taskQueue->id]));
            $taskQueue->update();
        } else {
            $result = [
                'success' => true,
                'message' => $taskQueue->getErrors()
            ];
        }

        return Json::encode($result);
    }
}
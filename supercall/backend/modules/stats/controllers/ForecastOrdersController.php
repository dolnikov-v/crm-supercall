<?php
namespace backend\modules\stats\controllers;

use backend\components\web\Controller;
use backend\modules\stats\models\search\ForecastOrdersSearch;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ForecastOrdersController
 * @package backend\modules\stats\controllers
 */
class ForecastOrdersController extends Controller
{
    public function actionIndex()
    {
        $model = new ForecastOrdersSearch();
        $params = Yii::$app->request->queryParams;
        $data = $model->search($params);

        return $this->render('index', [
            'model' => $model,
            'types' => OrderType::find()->collection('common'),
            'partners' => ArrayHelper::map(yii::$app->user->getPartners(), 'id', 'name'),
            'countries' => ArrayHelper::map(yii::$app->user->getCountries(), 'id', function($data){ return yii::t('common', $data['name']);}),
            'data' => $data,
            'params' => $params['ForecastOrdersSearch'] ?? []
        ]);
    }
}
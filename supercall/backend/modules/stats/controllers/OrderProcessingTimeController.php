<?php
namespace backend\modules\stats\controllers;

use backend\modules\stats\models\OrderProcessingTime;
use common\components\web\Controller;
use backend\modules\stats\models\search\OrderProcessingTimeSearch;
use common\models\Product;
use common\modules\order\models\Order;
use Yii;
use common\components\grid\DateColumn;
use yii\helpers\ArrayHelper;
use common\models\Teams;
/**
 * Class OrderProcessingTimeController
 * @package backend\modules\stats\controllers
 */
class OrderProcessingTimeController extends Controller
{
    public function getTeams()
    {
        $userModel = yii::$app->user->getUserModel();
        $teams = $userModel->getTeamsByUser();
        return  is_array($teams) ? ArrayHelper::map($teams, 'id', 'name') : [];
    }

    public function getListCountries()
    {
        $countries = [];

        if(is_array(Yii::$app->user->countries)){
            foreach(Yii::$app->user->countries as $k => $country){
                $countries[$country->id] = yii::t('common', $country->name);
            }
        }

        return $countries;
    }
    /**
     * @return string
     */
    public function actionDates()
    {
        $modelSearch = new OrderProcessingTimeSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $reportConfig = $this->getReportConfig();


        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'title' => $reportConfig['title'],
            'columns' => $reportConfig['columns'],
            'filterTemplate' => $reportConfig['filterTemplate'],
            'teams' => $this->getTeams(),
            'countries' => $this->getListCountries(),
            'products' =>  Product::getActiveListProducts()
        ]);
    }

    /**
     * @return string
     */
    public function actionOrders()
    {
        $modelSearch = new OrderProcessingTimeSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $reportConfig = $this->getReportConfig();


        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'title' => $reportConfig['title'],
            'columns' => $reportConfig['columns'],
            'filterTemplate' => $reportConfig['filterTemplate'],
            'teams' => $this->getTeams(),
            'countries' => $this->getListCountries(),
            'products' =>  Product::getActiveListProducts()
        ]);
    }

    /**
     * @return string
     */
    public function actionStatuses()
    {
        $modelSearch = new OrderProcessingTimeSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $reportConfig = $this->getReportConfig();

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'title' => $reportConfig['title'],
            'columns' => $reportConfig['columns'],
            'filterTemplate' => $reportConfig['filterTemplate'],
            'teams' => $this->getTeams(),
            'countries' => $this->getListCountries(),
            'products' =>  Product::getActiveListProducts()
        ]);
    }

    /**
     * @return string
     */
    public function actionProducts()
    {
        $modelSearch = new OrderProcessingTimeSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $reportConfig = $this->getReportConfig();

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'title' => $reportConfig['title'],
            'columns' => $reportConfig['columns'],
            'filterTemplate' => $reportConfig['filterTemplate'],
            'teams' => $this->getTeams(),
            'countries' => $this->getListCountries(),
            'products' =>  Product::getActiveListProducts()
        ]);
    }

    /**
     * @param $date
     * @return string
     */
    public function formatDate($date)
    {
        $params = yii::$app->request->get('OrderProcessingTimeSearch');
        $groupBy = isset($params['groupBy']) ? $params['groupBy'] : OrderProcessingTime::GROUP_DAY;

        switch ($groupBy) {
            case OrderProcessingTime::GROUP_HOUR :
                $formatedDate = date('H:00 F d Y', $date);
                break;
            case OrderProcessingTime::GROUP_DAY :
                $formatedDate = date('d F Y', $date);
                break;
            case OrderProcessingTime::GROUP_MONTH :
                $formatedDate = date('F Y', $date);
                break;
            default :
                $formatedDate = date('d F Y', $date);
                break;
        }

        return $formatedDate;
    }

    /**
     * @return array
     */
    public function commonFields()
    {
        return [
            [
                'attribute' => 'countOrders',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'visible' => Yii::$app->controller->action->id != 'products'
            ],
            [
                'attribute' => 'minimum',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'average',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'maximum',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'sumtime',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center']
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getReportConfig()
    {
        $reports = [
            'index' => [
                'title' => yii::t('common', 'Затраты времени по датам'),
                'columns' => $this->getDatesReportColumns(),
                'filterTemplate' => '_filter-dates-form'
            ],
            'orders' => [
                'title' => yii::t('common', 'Затраты времени по заказам'),
                'columns' => $this->getOrdersReportColumns(),
                'filterTemplate' => '_filter-orders-form'
            ],
            'statuses' => [
                'title' => yii::t('common', 'Затраты времени по статусам заказов'),
                'columns' => $this->getStatusesReportColumns(),
                'filterTemplate' => '_filter-statuses-form'
            ],
            'products' => [
                'title' => yii::t('common', 'Затраты времени по товарам заказов'),
                'columns' => $this->getProductsReportColumns(),
                'filterTemplate' => '_filter-products-form'
            ]
        ];

        $action = Yii::$app->controller->action->id;

        return isset($reports[$action]) ? $reports[$action] : $reports['index'];
    }

    /**
     * @return array
     */
    public function getDatesReportColumns()
    {
        $fields = [
            [
                'attribute' => 'groupBy',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($data){
                    return $this->formatDate($data->groupBy);
                }
            ],
            [
                'attribute' => 'country',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($data){
                    return yii::t('common', $data->country);
                }
            ],
        ];

        return array_merge($fields, $this->commonFields());
    }

    /**
     * @return array
     */
    public function getOrdersReportColumns()
    {
        $fields = [
            [
                'attribute' => 'groupBy',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($data){
                    return $this->formatDate($data->groupBy);
                }
            ],
            [
                'attribute' => 'country',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($data){
                    return yii::t('common', $data->country);
                }
            ],
            [
                'attribute' => 'order_id',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-left']
            ],
            [
                'attribute' => 'status',
                'label' => yii::t('common', 'Статус'),
                'headerOptions' => ['class' => 'width-150 text-left'],
                'contentOptions' => ['class' => 'text-left'],
                'content' => function ($data) {
                    return Order::getStatusByNumber($data->status);
                }
            ],
            [
                'attribute' => 'countOrders',
                'label' => yii::t('common', 'Кол-во'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'visible' => false
            ],
            [
                'attribute' => 'time',
                'label' => yii::t('common', 'Общее время, мин.'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center']
            ],
            [
                'attribute' => 'average',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'visible' => false
            ],
        ];

        return $fields;
    }

    /**
     * @return array
     */
    public function getStatusesReportColumns()
    {
        $fields = [
            [
                'attribute' => 'groupBy',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($data){
                    return $this->formatDate($data->groupBy);
                }
            ],
            [
                'attribute' => 'country',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($data){
                    return yii::t('common', $data->country);
                }
            ],
            [
                'attribute' => 'status',
                'label' => yii::t('common', 'Статус'),
                'headerOptions' => ['class' => 'width-150 text-left'],
                'contentOptions' => ['class' => 'text-left'],
                'content' => function ($data) {
                    return Order::getStatusByNumber($data->end_status);
                }
            ],
        ];

        return array_merge($fields, $this->commonFields());
    }

    /**
     * @return array
     */
    public function getProductsReportColumns()
    {
        $fields = [
            [
                'attribute' => 'groupBy',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($data){
                    return $this->formatDate($data->groupBy);
                }
            ],
            [
                'attribute' => 'country',
                'headerOptions' => ['class' => 'width-200'],
                'contentOptions' => ['class' => 'text-left'],
                'value' => function($data){
                    return $data->country;
                }
            ],
            [
                'attribute' => 'product',
                'label' => yii::t('common', 'Товар'),
                'headerOptions' => ['class' => 'width-150 text-left'],
                'contentOptions' => ['class' => 'text-left'],
                'content' => function ($data) {
                    return yii::t('common', $data->product);
                }
            ],
        ];

        return array_merge($fields, $this->commonFields());
    }
}
<?php

namespace backend\modules\stats\controllers;

use backend\components\web\Controller;
use backend\modules\stats\models\report\OperatorOnlineReport;
use common\models\Country;
use common\models\User;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class OperatorOnlineController extends Controller
{
    public function actionIndex()
    {
        $model_general = new OperatorOnlineReport();
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);

        $model_general->load($query);

        /** @var User $userModel */
        $userModel = yii::$app->user->getUserModel();
        $teams = $userModel->getTeamsByUser();
        $teams_list = is_array($teams) ? ArrayHelper::map($teams, 'id', 'name') : [];
        $reportData = $model_general->getGeneralAggregate();

        /** @var ArrayDataProvider $dataProvider */
        $dataProvider = $reportData['dataProvider'];

        $listUsers = ArrayHelper::getColumn($dataProvider->getModels(), 'user.username');

        $onlyOperators = [];

        foreach ($listUsers as $k => $v) {
            if ($v) {
                $onlyOperators[] = $v;
            }
        }

        $operators = User::getActiveByRole(yii::$app->user->getCountries(), [User::ROLE_OPERATOR, User::ROLE_APPLICANT, User::ROLE_SENIOR_OPERATOR]);

        $pagination = yii::$app->request->get('rest') ? false : ['pageSize' => 20];

        return $this->render('index', [
            'model_general' => $model_general,
            'groups' => OperatorOnlineReport::getGroupsCollection(),
            'count' => count(array_flip($onlyOperators)),
            'countries' => Country::find()->collection('common'),
            'teams' => $teams_list,
            'sessionFilterData' => $query,
            'operators' => ArrayHelper::map($operators, 'id', 'username'),
            'dataProvider' =>new ArrayDataProvider([
                'allModels' =>$dataProvider->getModels(),
                'pagination' => $pagination
            ]),
        ]);
    }

}
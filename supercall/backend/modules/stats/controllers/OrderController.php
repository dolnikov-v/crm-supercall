<?php
namespace backend\modules\stats\controllers;

use backend\components\web\Controller;
use backend\modules\stats\models\OrderStats;
use backend\modules\stats\models\report\OrderReport;
use common\components\web\User;
use common\models\Country;
use common\models\Product;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use Yii;
use yii\base\Object;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\Teams;
use yii\web\User as UserModel;

/**
 * Class OrderController
 * @package backend\modules\stats\controllers
 */
class OrderController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex($tab = null)
    {
        $filterSessId = $this->route;

        $this->getFilterBySession($filterSessId, $query);

        $model_general = new OrderReport();
        $model_general->load($query);

        if (is_null($model_general->date_range)) {
            $model_general->date_range = date('d/m/Y 00:00:00') . ' - ' . date('d/m/Y 23:59:59');
        }
        /** @var Object $date */
        $date = Yii::$app->formatter->asConvertToFrom($model_general->date_range);
        $model_general->date_start = $date->from->format('d.m.Y H:i:00');
        $model_general->date_end = $date->to->format('d.m.Y H:i:59');

        $model_stats = new OrderStats();
        $model_stats->load($query, $model_general->formName());

        $model_stats->date_start = $model_general->date_start;
        $model_stats->date_end = $model_general->date_end;

        $old_tabs = ['genesys'];

        $countries = [];
        /** @var  User Yii::$app->user*/
        if(is_array(Yii::$app->user->countries)){
            foreach(Yii::$app->user->countries as $k => $country){
                $countries[$country->id] = yii::t('common', $country->name);
            }
        }

        if ($tab){
            $html = $this->renderPartial('_index_tab_'.$tab, ['model' => in_array($tab, $old_tabs) ? $model_stats : $model_general]);
            return Json::encode($html);
        }
        else{
            /** @var UserModel $userModel */
            $userModel = yii::$app->user->getUserModel();
            $teams = $userModel->getTeamsByUser();
            $teams_list = is_array($teams) ? ArrayHelper::map($teams, 'id', 'name') : [];

            return $this->render('index', [
                'model_general'     => $model_general,
                'model_stats'     => $model_stats,
                'contentVisible' => Yii::$app->session->get($filterSessId.'.isVisible', true),
                'groups'    => OrderReport::getGroupsCollection(),
                'types'     => OrderType::find()->collection('common'),
                'products'  => Product::getActiveListProducts(),
                'countries' => $countries,
                'partners'  => ArrayHelper::map( yii::$app->user->identity->partners, 'id', 'name' ),
                'teams' => $teams_list
            ]);
        }
    }
}

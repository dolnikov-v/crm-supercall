<?php
namespace backend\modules\stats\controllers;


use backend\components\web\Controller;
use backend\modules\stats\models\report\OrderBuyoutReport;
use common\models\Product;
use common\models\User;
use common\modules\order\models\OrderBuyout;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;


class OrderBuyoutController extends Controller
{
    public function actionIndex($tab = null)
    {
        $model_general = new OrderBuyoutReport();
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);
        $model_general->load($query);


        $countries = [];
        if (is_array(Yii::$app->user->countries)) {
            foreach (Yii::$app->user->countries as $k => $country) {
                $countries[$country->id] = yii::t('common', $country->name);
            }
        }

        if ($tab) {
            $html = $this->renderPartial('_index_tab_' . $tab, ['model' => $model_general]);
            return Json::encode($html);
        } else {
            /** @var User $userModel */
            $userModel = yii::$app->user->getUserModel();
            $teams = $userModel->getTeamsByUser();
            $teams_list = is_array($teams) ? ArrayHelper::map($teams, 'id', 'name') : [];
            $reportData = $model_general->getGeneralAggregate();

            /** @var ArrayDataProvider $dataProvider */
            $dataProvider = $reportData['dataProvider'];

            $models = $dataProvider->getModels();

            $modelsWithTotal = [];

            $operatorsWithOrders = [];

            foreach ($models as $model) {

                $modelsWithTotal[] = $model;
                if (!in_array($model['username'], $operatorsWithOrders)) {
                    $operatorsWithOrders[] = $model['username'];
                }

            }

            $model_general->dataProviderWithOrdersTotal = new ArrayDataProvider([
                'allModels' => $modelsWithTotal,
                'pagination' => false
            ]);

            $listUsers = ArrayHelper::getColumn($models, 'user.username');

            $onlyOperators = [];

            foreach ($listUsers as $k => $v) {
                if ($v) {
                    $onlyOperators[] = $v;
                }
            }

            $operators = User::getActiveByRole(yii::$app->user->getCountries(), [User::ROLE_OPERATOR, User::ROLE_APPLICANT, User::ROLE_SENIOR_OPERATOR]);

            return $this->render('index', [
                'model_general' => $model_general,
                'count' => count(array_flip($onlyOperators)),
                'groups' => OrderBuyoutReport::getGroupsCollection(),
                'types' => OrderType::find()->collection('common'),
                'products' => Product::getActiveListProducts(),
                'countries' => $countries,
                'partners' => ArrayHelper::map(Partner::find()->active()->all(), 'id', 'name'),
                'teams' => $teams_list,
                'sessionFilterData' => $query,
                'operators' => ArrayHelper::map($operators, 'id', 'username'),
                'dataProviderWithOrders' => new ArrayDataProvider([
                    'allModels' => $modelsWithTotal,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                ]),
                'operatorsWithOrders' => $operatorsWithOrders,
            ]);
        }

    }
}
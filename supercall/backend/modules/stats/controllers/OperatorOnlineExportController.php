<?php

namespace backend\modules\stats\controllers;

use backend\components\web\Controller;
use backend\models\TaskQueue;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * Class OperatorOnlineExportController
 * @package backend\modules\stats\controllers
 */
class OperatorOnlineExportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'excel' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionExcel()
    {
        $queryParams = yii::$app->request->post('queryParams');

        if (empty($queryParams)) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Ошибка при постановке задачи. Примените фильтр и после выдачи результатов статистики повторите попытку')
            ];
        } else {

            $taskQueue = new TaskQueue([
                'name' => null,
                'type' => TaskQueue::TYPE_STATS_OPERATOR_ONLINE_IMPORT,
                'user_id' => yii::$app->user->identity->id,
                'path' => null,
                'status' => TaskQueue::STATUS_READY,
                'error' => null,
                'created_at' => time(),
                'updated_at' => time(),
                'query' => $queryParams
            ]);

            if (!$taskQueue->save()) {
                $result = [
                    'success' => false,
                    'message' => $taskQueue->getErrors()
                ];
            } else {
                if (!$taskQueue->generateNameForStatsOperators()) {
                    $result = [
                        'success' => false,
                        'message' => $taskQueue->getErrors()
                    ];
                } else {
                    $result = [
                        'success' => true,
                        'message' => yii::t('common', 'Задание #{id} для экспорта успешно создано. Посмотреть состояние задания можно в профиле, в разделе "Загрузки"', ['id' => $taskQueue->id])
                    ];
                }
            }
        }

        return Json::encode($result);
    }
}
<?php

namespace backend\modules\stats\controllers;

use backend\components\web\Controller;
use backend\modules\stats\models\Queue;
use backend\modules\stats\models\search\QueueSearch;
use common\components\grid\IdColumn;
use common\components\grid\NumberColumn;
use common\components\grid\NumberTotalColumn;
use common\components\grid\RatioColumn;
use common\modules\order\models\Order;
use common\widgets\base\Label;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class QueueController
 * @package backend\modules\stats\controllers
 */
class QueueController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new QueueSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $countries = [];

        foreach (yii::$app->user->countries as $country) {
           $countries[$country->id] = Yii::t('common', $country->name);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'columns' => $this->generateColumns(),
            'countries' => $countries
        ]);
    }

    /**
     * @return array
     */
    public function generateColumns()
    {
        $columns = [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name'
            ],
            [
                'attribute' => 'is_primary',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->is_primary ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->is_primary ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                }
            ],
            [
                'attribute' => 'country',
                'label' => yii::t('common', 'Страна'),
                'content' => function ($model) {
                    /** @var Queue $model */
                    if (!$model->is_primary) {
                        $secondaryCountry = $model->getCountry()->one();
                        $country = $secondaryCountry ? yii::t('common', $secondaryCountry->name) : '';
                    } else {
                        $country = implode(', ', array_map(function ($v) {
                            return yii::t('common', $v);
                        }, ArrayHelper::getColumn($model->getCountries(), 'name')));
                    }

                    return $country;
                }
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => [
                    'class' => 'text-center',
                    'data-sortable' => 'true', 'data-field' => 'total'
                ],
            ]
        ];

        foreach (Order::getStatusesCollection() as $status_id => $status_label) {
            $columns[] = [
                'class' => NumberColumn::className(),
                'attribute' => 'quantity_' . $status_id,
                'label' => $status_label,
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center']
            ];
        }

        return $columns;
    }
}
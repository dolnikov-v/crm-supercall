<?php

namespace backend\modules\stats\models;

use backend\modules\queue\models\Queue as QueueModel;
use Yii;

/**
 * Class Queue
 * @package backend\modules\stats\models
 */
class Queue extends QueueModel
{
    const IGNORE_DATE_TYPE = 'ignore';
    const PARTNER_CREATED_DATE_TYPE = 'partner_created_at';
    const CC_CREATED_DATE_TYPE = 'cc_created_at';

    /** @var  integer */
    public $total;

    /** @var  integer */
    public $country;

    /** @var  string */
    public $date_type;

    /** @var  string */
    public $date_range;

    /** @var  integer */
    public $quantity_1;
    /** @var  integer */
    public $quantity_2;
    /** @var  integer */
    public $quantity_3;
    /** @var  integer */
    public $quantity_4;
    /** @var  integer */
    public $quantity_5;
    /** @var  integer */
    public $quantity_6;
    /** @var  integer */
    public $quantity_7;
    /** @var  integer */
    public $quantity_8;

    public function attributeLabels()
    {
        $labels = [
            'data_type' => yii::t('common', 'Тип даты'),
            'date_range' => yii::t('common', 'Период'),
            'queue' => Yii::t('common', 'Очередь'),
        ];

        return array_merge(parent::attributeLabels(), $labels);
    }
}
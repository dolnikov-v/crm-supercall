<?php

namespace backend\modules\stats\models;

use common\components\grid\DateColumn;
use common\models\Teams;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderLogs;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderProductLog;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * Class LogListStats
 * @package backend\modules\stats\models.
 */
class LogListStats extends Model
{
    public $date_range;
    public $date_start;
    public $date_end;
    public $country;
    public $countries;
    public $product;
    public $operator;
    public $team_id;
    public $order;
    public $status;
    public $sub_status;
    public $type;
    public $partner;

    protected $_dataProvider;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_range', 'date_start', 'date_end', 'country', 'countries', 'product', 'operator', 'team_id', 'order', 'status', 'sub_status', 'type', 'partner'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'date_range' => \Yii::t('common', 'Дата'),
            'date_start' => \Yii::t('common', 'От'),
            'date_end' => \Yii::t('common', 'До'),
            'country' => \Yii::t('common', 'Страна'),
            'countries' => \Yii::t('common', 'Страна'),
            'product' => \Yii::t('common', 'Товар'),
            'operator' => \Yii::t('common', 'Оператор'),
            'order' => \Yii::t('common', 'Заказ'),
            'status' => \Yii::t('common', 'Статус'),
            'sub_status' => \Yii::t('common', 'Автоотказ'),
            'type' => \Yii::t('common', 'Тип заказа'),
            'partner' => \Yii::t('common', 'Партнер'),
            'team_id' => \Yii::t('common', 'Команда'),
        ];
    }

    /**
     * @return DataProviderInterface
     */
    public function getList()
    {
        $date_start = new \DateTime($this->date_start . ':00');
        $date_end = new \DateTime($this->date_end . ':59');

        $query = new ActiveQuery(OrderLogs::className());
        $query
            ->asArray(true)
            ->select([
                'order_logs.id',
                'order_logs.order_id',
                'product',
                'fields',
                'user.username as username',
                'operator',
                'user_id',
                'comment',
                'new',
                'old',
                'timezone.timezone_id as timezone',
                "to_char(to_timestamp(order_logs.created_at), 'YYYY-MM-DD HH24:MI:SS') as time"
            ])
            ->joinWith(['order.country.timezone', 'user'])
            ->andFilterWhere(['>=', 'order_logs.created_at', $date_start->getTimestamp()])
            ->andFilterWhere(['<=', 'order_logs.created_at', $date_end->getTimestamp()])
            ->andFilterWhere(['or', ['in','operator', $this->operator], ['in','user.username', $this->operator]])
            ->andFilterWhere([
                'order.id'           => $this->order,
                'order_logs.new'     => $this->status,
                'order.type_id'      => $this->type,
                'order.partner_id'   => $this->partner,
            ])
            ->andFilterWhere(['in', 'order.country_id', $this->getCountries()]);

        if($this->team_id) {
            $users = Teams::getUsers($this->team_id);

            if(is_array($users)){
                $user_ids = ArrayHelper::getColumn($users, 'id');
                $query->andFilterWhere(['user.id' => $user_ids]);
            }
        }

        return new ActiveDataProvider([
            'query' => $query,
            'sort'       => [
                'attributes'   => [
                    'time',
                ],
                'defaultOrder' => [
                    'time' => SORT_ASC,
                ],
            ]
        ]);
    }

    /**
     * @param $refresh boolean
     * @return DataProviderInterface
     */
    public function getListStats($refresh = false)
    {
        if (!$this->_dataProvider || $refresh) {
            $date_start = new \DateTime($this->date_start . ':00');
            $date_end = new \DateTime($this->date_end . ':59');

            $log_query = new ActiveQuery(OrderLogs::className());
            $product_log_query = new ActiveQuery(OrderProductLog::className());
            $product_log_query
                ->asArray(true)
                ->select([
                    'order.id as order',
                    'product.name as product',
                    new Expression('null as operator'),
                    new Expression('null as username'),
                    new Expression('null as comment'),
                    'order.sub_status as sub_status',
                    new Expression('null as oldStatus'),
                    new Expression('null as newStatus'),
                    'timezone.timezone_id as timezone',
                    'order_product_log.created_at as date'
                ])
                ->joinWith(['order.country.timezone', 'user', 'product'])
                ->andFilterWhere(['>=', 'order_product_log.created_at', $date_start->getTimestamp()])
                ->andFilterWhere(['<=', 'order_product_log.created_at', $date_end->getTimestamp()])
                ->andFilterWhere([
//                    'order.country_id' => $this->country,
                    'order.id'         => $this->order,
                    'order.status'     => $this->status,
                    'order.type_id'    => $this->type,
                    'order.partner_id' => $this->partner,
                ])
                ->andFilterWhere(['in', 'order.country_id', $this->getCountries()]);

            if($this->team_id) {
                $users = Teams::getUsers($this->team_id);

                if(is_array($users)){
                    $user_ids = ArrayHelper::getColumn($users, 'id');
                    $product_log_query->andFilterWhere(['user.id' => $user_ids]);
                }
            }



            if ($this->operator) {
                $product_log_query->andWhere([
                    'or',
                    ['operator' => $this->operator],
                    ['user.username' => $this->operator]
                ]);
            }
            if ($this->product) {
                $product_log_query->andWhere(['order_product_log.product_id' => $this->product]);
            }

            $log_query
                ->asArray(true)
                ->select([
                    'order.id as order',
                    // new Expression('null as product'),
                    'product.name as product',
                    'operator',
                    'user.username as username',
                    'order_logs.comment as comment',
                    'order.sub_status as sub_status',
                    'old as oldStatus',
                    'new as newStatus',
                    'timezone.timezone_id as timezone',
                    'order_logs.created_at as date'
                ])
                ->joinWith(['order.country.timezone', 'user'])
                ->leftJoin('order_product', 'order_product.order_id=order_logs.order_id')
                ->leftJoin('product', 'product.id=order_product.product_id')
//                ->where(['field' => 'status'])
                ->andFilterWhere(['>=', 'order_logs.created_at', $date_start->getTimestamp()])
                ->andFilterWhere(['<=', 'order_logs.created_at', $date_end->getTimestamp()])
                ->andFilterWhere([
//                    'order.country_id' => $this->country,
                    'order.id'         => $this->order,
                    'order.status'     => $this->status,
                    'order.type_id'    => $this->type,
                    'order.partner_id' => $this->partner,
                ])
                ->andFilterWhere(['in', 'order.country_id', $this->getCountries()]);

            if($this->team_id) {
                $users = Teams::getUsers($this->team_id);

                if(is_array($users)){
                    $user_ids = ArrayHelper::getColumn($users, 'id');
                    $log_query->andFilterWhere(['user.id' => $user_ids]);
                }
            }

            if ($this->operator) {
                $log_query->andWhere([
                    'or',
                    ['operator' => $this->operator],
                    ['user.username' => $this->operator]
                ]);
            }
            $log_query->union($product_log_query);

            $query = new Query();
            $query
                ->select([
                    'order',
                    "string_agg(product, ', ') as products",
                    'operator',
                    'username',
                    'comment',
                    'sub_status',
                    'min("oldStatus") as "oldStatus"',
                    'max("newStatus") as "newStatus"',
                    'timezone',
                    "to_char(to_timestamp(max(date)), 'YYYY-MM-DD HH24:MI:SS') as time"
                ])
                ->from(['data' => $log_query])
                ->andWhere(['is not', 'newStatus', null]) // строку создания заказа в order_log игнорируем
                ->groupBy(['order', 'timezone', 'operator', 'username', 'comment', 'sub_status'])
                ->orderBy('time DESC');
            
            $this->_dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort'       => [
                    'attributes'   => [
                        'time',
                    ],
                    'defaultOrder' => [
                        'time' => SORT_ASC,
                    ],
                ]
            ]);
        }

        return $this->_dataProvider;
    }

    /**
     * Колонки для вкладки "История изменений"
     * @return array
     */
    public static function getListColumns($orderProducts = false)
    {
        $columns = [
            [
                'attribute' => 'order_id',
                'label' => \Yii::t('common', 'Заказ'),
                'format' => 'raw'
            ],
            [
                'attribute' => 'product',
                'label' => \Yii::t('common', 'Товары'),
                'value' => function ($row) use ($orderProducts) {
                    $text = [];
                    $products = json_decode($row['product'], true);
                    foreach ($products as $product){
                        if(
                            !empty($orderProducts) &&
                            !empty($product['product_id']) &&
                            !empty($orderProducts[$product['product_id']]) &&
                            !empty($product['quantity'])
                          )
                        {
                            $text[] =  $orderProducts[$product['product_id']].' - '.$product['quantity'];
                        }
                    }
                    return implode(', ',$text);
                }
            ],
            [
                'attribute' => 'old',
                'label' => \Yii::t('common', 'Старый статус'),
                'value' => function ($row) {
                    return ArrayHelper::getValue(Order::getStatusesCollection(), $row['old']);
                }
            ],
            [
                'attribute' => 'new',
                'label' => \Yii::t('common', 'Новый статус'),
                'value' => function ($row) {
                    $result = ArrayHelper::getValue(Order::getStatusesCollection(), $row['new']);
                    // UN-40
                    // Если субстатус 505 - автоматич.отклонение по крону
                    $fields = json_decode($row['fields'], true);
                    if ($row['new'] == 5 && !empty($fields['sub_status']) && $fields['sub_status'] == 505) {
                        $result .= ' (авто)';
                    }
                    
                    return $result;
                }
            ],
            [
                'attribute' => 'operator',
                'label' => \Yii::t('common', 'Оператор'),
                'value' => function ($row) {
                    return !empty($row['operator'])
                        ? $row['operator']
                        : ( array_key_exists('username', $row) ? $row['username'] : null);
                }
            ],
            [
                'attribute' => 'comment',
                'label' => \Yii::t('common', 'Комментарий'),
                'value' => function ($row) {
                    return array_key_exists('comment', $row) ? $row['comment'] : null;
                }
            ],
            [
                'attribute' => 'time',
                'class' => DateColumn::className(),
                'timezone' => function ($row) {
                    return $row['timezone'];
                },
                'label' => \Yii::t('common', 'Дата')
            ],
        ];
        return $columns;
    }

    /**
     * @param OrderLog $log
     * @return array
     */
    public static function formatLog(OrderLog $log)
    {
        $products = '';
        if (!empty($log->order->orderProducts)) {
            $products = [];
            foreach ($log->order->orderProducts as $orderProduct) {
                $products[] = $orderProduct->product->name
                    . ': ' . \Yii::t('common', 'Кол-во: ') . $orderProduct->quantity
                    . ', ' . \Yii::t('common', 'Сумма: ') . $orderProduct->cost;
            }
            $products = implode(', ', $products);
        }

        return [
            'order' => Html::a($log->order_id, ['/order/index/view','id'=>$log->order_id], [
                'target' => 'blank'
            ]),
            'products' => $products,
            'group' => $log->group_id,
            'operator' => $log->operator,
            'sub_status' => $log->order->sub_status,
            'oldStatus' => $log->old,
            'newStatus' => $log->new,
            'timezone' => $log->order->country->timezone->timezone_id,
            'time' => $log->created_at
        ];
    }

    /**
     * Функция вернет массив стран для фильтра статы
     * По умолчанию фильтр пустой, потому будут возвращены все доступные Пользователю страны.
     *
     * @return array
     */
    public function getCountries()
    {
        if (!$this->countries) {
            $this->countries = ArrayHelper::getColumn( \Yii::$app->user->countries, 'id' );
        }

        return $this->countries;
    }
}
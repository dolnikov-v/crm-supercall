<?php
namespace backend\modules\stats\models;

use Yii;
use yii\base\Model;

/**
 * Class ForecastOrders
 * @package backend\modules\stats\models
 */
class ForecastOrders extends Model
{
    const GROUP_MINUTES = 'minutes';
    const GROUP_HOURS = 'hours';
    const GROUP_DAYS = 'days';
    const GROUP_MONTHS = 'months';

    /** @var  string */
    public $date_range;
    /** @var  string */
    public $group;
    /** @var  integer */
    public $type;
    /** @var  string */
    public $country;
    /** @var  integer */
    public $partner;

    public function rules()
    {
        return [
            [['date_range', 'country', 'partner', 'type', 'group'], 'safe']
        ];
    }
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date_range' => Yii::t('common', 'Дата'),
            'date_start' => Yii::t('common', 'От'),
            'date_end' => Yii::t('common', 'До'),
            'type' => Yii::t('common', 'Тип заказа'),
            'country' => Yii::t('common', 'Страна'),
            'partner' => Yii::t('common', 'Партнер'),
            'group' => Yii::t('common', 'Группировка'),
        ];
    }

    /**
     * @return array
     */
    public static function groupCollection()
    {
        return [
            self::GROUP_MINUTES => Yii::t('common', 'Минута'),
            self::GROUP_HOURS => Yii::t('common', 'Час'),
            self::GROUP_DAYS => Yii::t('common', 'День'),
            self::GROUP_MONTHS => Yii::t('common', 'Месяц')
        ];
    }
    
    public function formatDate($timestamp){

        $timestamp = $timestamp + yii::$app->user->getTimezone()->time_offset;

        switch($this->group){
            case self::GROUP_MINUTES:
                $data = yii::$app->formatter->asDateFullTime($timestamp, 'php:Y-m-d H:i');
                break;
            case self::GROUP_HOURS:
                $data = yii::$app->formatter->asDateFullTime($timestamp, 'php:Y-m-d H:00');
                break;
            case self::GROUP_DAYS:
                $data = yii::$app->formatter->asDateFullTime($timestamp, 'php:Y-m-d');
                break;
            case self::GROUP_MONTHS:
                $data = yii::$app->formatter->asDateFullTime($timestamp, 'php:Y-m');
                break;
            default: $data = yii::$app->formatter->asDateFullTime($timestamp, 'php:Y-m-d');
                break;
        }

        return $data;
    }
}
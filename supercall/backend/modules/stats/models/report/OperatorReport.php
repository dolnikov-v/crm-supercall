<?php

namespace backend\modules\stats\models\report;

use backend\models\TaskQueue;
use backend\modules\access\models\AuthAssignment;
use backend\modules\stats\models\Queue;
use common\components\grid\AverageOrderCostColumn;
use common\components\grid\AverageTotalOrderCostColumn;
use common\components\grid\NumberColumn;
use common\components\grid\NumberTotalColumn;
use common\components\grid\RatioColumn;
use common\components\grid\RatioTotalColumn;
use common\components\grid\TimeTotalColumn;
use common\models\Country;
use common\models\Product;
use common\models\User;
use common\models\UserCountry;
use common\models\UserReady;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderType;
use Yii;
use yii\base\Object;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use common\models\Teams;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

class OperatorReport extends AbstractReport
{
    /** @var  ArrayDataProvider */
    public $dataProviderWithOrdersTotal;
    /** @var  ArrayDataProvider */
    public $dataProviderWithoutOrdersTotal;

    public $role;

    public $username;
    /**
     * @var array
     */
    public $timeDataOperators = [];
    public $is_export = false;

    /**
     * @return array
     */
    public function getGeneralAggregate()
    {
        if ($this->countries || $this->is_export || $this->username) {
            $this->group = empty($this->group) ? self::GROUP_OPERATOR : $this->group;

            $groupExpression = $this->getFieldNameByGroupAggregate(OrderLog::tableName());

            $select = [
                'group' => $groupExpression,
                'username' => 'u.username',
                'user_id' => 'u.id',
                'total' => new Expression('COUNT(' . Order::tableName() . '.id)'),
                'midcheck' => new Expression('SUM(CASE WHEN ' . OrderLog::tableName() . '.new::integer = ' . Order::STATUS_APPROVED . ' THEN init_price ELSE 0 END) / 
                                                        (CASE WHEN sum(CASE WHEN ' . OrderLog::tableName() . '.new::integer = ' . Order::STATUS_APPROVED . ' THEN 1 ELSE 0 END) > 0
                                                        THEN sum(CASE WHEN ' . OrderLog::tableName() . '.new::integer = ' . Order::STATUS_APPROVED . ' THEN 1 ELSE 0 END) ELSE 1 END)')
            ];

            foreach (Order::getStatusesCollection() as $id => $status) {
                if ($status == Order::STATUS_NEW) {
                    $statusField = 'CASE WHEN ' . OrderLog::tableName() . '.new::integer = ' . $id . ' or ' . OrderLog::tableName() . '.new is null THEN 1 ELSE 0 END';
                } else {
                    $statusField = 'CASE WHEN ' . OrderLog::tableName() . '.new::integer = ' . $id . ' THEN 1 ELSE 0 END';
                }

                $select['status_quantity_' . $id] = new Expression('SUM(' . $statusField . ')');
            }

            $query = OrderLog::find()
                ->leftJoin(Order::tableName(), Order::tableName() . '.id=' . OrderLog::tableName() . '.order_id')
                ->leftJoin(['u' => User::tableName()], OrderLog::tableName() . '.user_id=u.id')
                ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id=u.id')
                ->where([OrderLog::tableName() . '.field' => 'status'])
                ->andWhere([
                    'or',
                    [
                        'and',
                        [AuthAssignment::tableName() . '.item_name' => [User::ROLE_OPERATOR, User::ROLE_SENIOR_OPERATOR]],
                        ['u.status' => [User::STATUS_DEFAULT, User::STATUS_APPLICANT]]
                    ],
                    ['is', 'u.id', NULL]
                ]);

            $query->select($select);
            $query->groupBy(['u.id', 'u.username']);
            if (empty($this->date_range) || is_null($this->date_range)) {
                $this->date_range = date('d/m/Y 00:00:00') . ' - ' . date('d/m/Y 23:59:59');
            }

            /** @var Object $date */
            $date = Yii::$app->formatter->asConvertToFrom($this->date_range);
            $from = yii::$app->formatter->asTimestampWithoutTimeZone($date->from->format('d.m.Y H:i:s'));
            $to = yii::$app->formatter->asTimestampWithoutTimeZone($date->to->format('d.m.Y H:i:s'));

            $query->andWhere(['between', OrderLog::tableName() . '.created_at', $from, $to]);

            if ($this->countries) {
                $query->andWhere([Order::tableName() . '.country_id' => $this->countries]);
                $byUserCountry = UserCountry::find()->select('user_id')->where(['country_id' => $this->countries]);
                $query->andWhere(['u.id' => $byUserCountry]);
            }

            if ($this->type) {
                $query->andWhere([Order::tableName() . '.type_id' => $this->type]);
            }

            if ($this->username) {
                $query->andWhere([OrderLog::tableName() . '.user_id' => $this->username]);
            }

            if($this->product){
                $query->andWhere([Product::tableName().'.id' => $this->product]);
            }

            if (!$this->partner) {
                if (Yii::$app instanceof \yii\web\Application) {
                    $query->andWhere([Order::tableName() . '.partner_id' => ArrayHelper::getColumn(yii::$app->user->identity->partners, 'id')]);
                }
            } else {
                $query->andWhere([Order::tableName() . '.partner_id' => $this->partner]);
            }

            if ($this->queue) {
                $finalStatusAsStr = implode(',', array_keys(Order::getFinalStatusesCollection()));
                $queueIdsAsStr = implode(',', $this->queue);

                $query->andWhere('
                   CASE WHEN ' . Order::tableName() . '.status IN (' . $finalStatusAsStr . ')
                   THEN ' . Order::tableName() . '.prevent_queue_id IN (' . $queueIdsAsStr . ')
                   ELSE ' . Order::tableName() . '.last_queue_id IN (' . $queueIdsAsStr . ') END ');
            }

            if ($this->group == self::GROUP_PRODUCT || $this->product) {
                $query->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id=' . Order::tableName() . '.id');
                $query->leftJoin(Product::tableName(), Product::tableName() . '.id=' . OrderProduct::tableName() . '.product_id');
            }

            if ($this->group == self::GROUP_COUNTRY) {
                $query->leftJoin(Country::tableName(), Country::tableName() . '.id=' . Order::tableName() . '.country_id');
            }

            if ($this->statistics_by_orders) {

                $subQuery = OrderLog::find()
                    ->select(['max(id) as max_id'])
                    ->where([OrderLog::tableName() . '.field' => 'status'])
                    ->andWhere(['between', OrderLog::tableName() . '.created_at', $from, $to])
                    ->groupBy(['order_id', 'user_id']);

                $query->rightJoin(['group_max_id' => $subQuery], 'group_max_id.max_id =' . OrderLog::tableName() . '.id');
            }

            $query->addGroupBy($groupExpression);

            $dataProvider = new ArrayDataProvider([
                'allModels' => $query->all(),
                'pagination' => false
            ]);

        } else {
            $dataProvider = new ArrayDataProvider([
                'allModels' => []
            ]);
        }

        return [
            'dataProvider' => $dataProvider,
        ];
    }

    /**
     * @return array|mixed
     */
    public function getListColumns()
    {
        $columns = [];

        foreach ($this->getGeneralColumnsAggregate() as $column) {
            $columns[] = $column['attribute'];
        }

        return $columns;
    }

    /**
     * @param $provider
     * @param $fieldName
     * @return int
     */
    public function pageTotal($provider, $fieldName)
    {
        $total = 0;
        foreach ($provider as $item) {
            if (isset($item[$fieldName])) {
                $total += $item[$fieldName];
            }
        }
        return $total;
    }

    /**
     * @param Query $query
     */
    public function addCustomFilters($query, $table)
    {

        if ($table == UserReady::tableName()) {
            if ($this->product) {
                if ($this->group != self::GROUP_PRODUCT) {
                    $query->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id=o.id');
                    $query->leftJoin(Product::tableName(), Product::tableName() . '.id=' . OrderProduct::tableName() . '.product_id');
                }
                $query->andFilterWhere([Product::tableName() . '.id' => $this->product]);
            }

            if ($this->username) {
                $query->andWhere([$table . '.user_id' => $this->username]);
            }
        } else {

            if ($this->countries) {
                $query->andWhere([$table . '.country_id' => $this->countries]);
            }
        }


        /*
        if($this->team) {
            $users = Teams::getUsers($this->team);

            if(is_array($users)){
                $user_ids = ArrayHelper::getColumn($users, 'id');
                $query->andFilterWhere(['user.id' => $user_ids]);
            }
        }else{
            $userModel = yii::$app->user->getUserModel();
            $users = $userModel->getCoTeamsUsers();
            if(!empty($users)){
                $user_ids = ArrayHelper::getColumn($users, 'id');
                $query->andFilterWhere(['user.id' => $user_ids]);
            }
        }
        */
    }

    /**
     * @return array
     */
    public function getGeneralColumnsAggregate($withOrders = true)
    {
        $report = $this;

        $columns = [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function ($row) use ($report) {
                    return yii::t('common', $report->formatGroupView($row));
                },
                'footer' => \Yii::t('common', 'Всего:'),
            ],
            [
                'attribute' => 'username',
                'label' => \Yii::t('common', 'Оператор'),
                'value' => function ($row) use ($report) {
                    return $row['username'];
                }
            ],
        ];

        if ($withOrders) {
            $columns[] = [
                'class' => NumberTotalColumn::className(),
                'dataProviderWithoutPaginate' => $this->dataProviderWithOrdersTotal,
                'attribute' => 'total',
                'label' => \Yii::t('common', 'Всего:'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'total'],
            ];

            $columns[] = [
                'class' => NumberTotalColumn::className(),
                'dataProviderWithoutPaginate' => $this->dataProviderWithOrdersTotal,
                'attribute' => 'status_quantity_' . Order::STATUS_APPROVED,
                'label' => Order::getStatusByNumber(Order::STATUS_APPROVED),
                'contentOptions' => function ($model) {
                    $cell = 'status_quantity_' . Order::STATUS_APPROVED;
                    $url = $model[$cell] > 0 && !is_null($model['user_id'])
                        ? Url::toRoute([
                            '/order/index',
                            'OrderSearch' => [
                                'countries' => $this->countries,
                                'partner_id' => $this->partner,
                                'status' => Order::STATUS_APPROVED,
                                'type_id' => $this->type,
                                'type_date' => Order::UPDATED_DATA_RANGE,
                                'product' => $this->product,
                                'id' => '',
                                'foreign_id' => '',
                                'date_range' => $this->getPeriodByGroup($model['group']),
                                'customer_full_name' => '',
                                'customerPhone' => '',
                                'customerMobile' => '',
                                'last_queue_id' => '',
                                'operator' => $model['user_id'],
                                'with_timezone' => 1,
                            ]
                        ]) : null;
                    return [
                        'class' => 'text-success',
                        'data-url' => $url
                    ];
                },
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => Order::STATUS_APPROVED],
            ];
            $columns[] = [
                'class' => RatioTotalColumn::className(),
                'dataProviderWithoutPaginate' => $this->dataProviderWithOrdersTotal,
                'attribute' => 'status_quantity_' . Order::STATUS_APPROVED,
                'label' => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'value' => function ($row) {
                    $curr = (int)$row['status_quantity_' . Order::STATUS_APPROVED];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr / $total * 100, 2) : 0;
                },
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => Order::STATUS_APPROVED . '-percent'],
            ];

            foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                //апрувы сразу после тотал идут, новые не нужны
                if (!in_array($status_id, [Order::STATUS_NEW, Order::STATUS_APPROVED])) {
                    $columns[] = [
                        'class' => NumberTotalColumn::className(),
                        'dataProviderWithoutPaginate' => $this->dataProviderWithOrdersTotal,
                        'attribute' => 'status_quantity_' . $status_id,
                        'label' => $status_label,
                        'contentOptions' => function ($model) use ($status_id) {
                            $url = Url::toRoute([
                                '/order/index',
                                'OrderSearch' => [
                                    'countries' => $this->countries,
                                    'partner_id' => $this->partner,
                                    'type_id' => $this->type,
                                    'type_date' => Order::UPDATED_DATA_RANGE,
                                    'status' => $status_id,
                                    'product' => $this->product,
                                    'id' => '',
                                    'foreign_id' => '',
                                    'date_range' => $this->getPeriodByGroup($model['group']),
                                    'customer_full_name' => '',
                                    'customerPhone' => '',
                                    'customerMobile' => '',
                                    'last_queue_id' => '',
                                    'operator' => $model['user_id'],
                                    'with_timezone' => 1,
                                ]
                            ]);
                            return [
                                'class' => 'text-success',
                                'data-url' => $url
                            ];
                        },
                        'footerOptions' => ['class' => 'text-success'],
                        'headerOptions' => ['data-sortable' => 'true', 'data-field' => $status_id],
                    ];
                    $columns[] = [
                        'class' => RatioTotalColumn::className(),
                        'dataProviderWithoutPaginate' => $this->dataProviderWithOrdersTotal,
                        'attribute' => 'status_quantity_' . $status_id,
                        'label' => '%',
                        'contentOptions' => ['class' => 'text-danger'],
                        'footerOptions' => ['class' => 'text-danger'],
                        'value' => function ($row) use ($status_id) {
                            $curr = (int)$row['status_quantity_' . $status_id];
                            $total = (int)$row['total'];
                            return $total > 0 ? round($curr / $total * 100, 2) : 0;
                        },
                        'headerOptions' => ['data-sortable' => 'true', 'data-field' => $status_id . '-percent'],
                    ];
                }
            }
            $columns[] = [
                'class' => AverageTotalOrderCostColumn::className(),
                'dataProviderWithoutPaginate' => $this->dataProviderWithOrdersTotal,
                'countColumn' => 'status_quantity_' . Order::STATUS_APPROVED,
                'attribute' => 'midcheck',
                'label' => \Yii::t('common', 'Средний чек'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'average-price'],
                'value' => function ($model) {
                    return round($model['midcheck'], 2);
                }
            ];

            $columns[] = [
                'attribute' => 'mark',
                'label' => \Yii::t('common', 'Метка'),
                'value' => function ($row) use ($report) {
                    return $row['mark'];
                },
                'visible' => false
            ];

        }

//
        /*\        $columns[] = [
            'class' => NumberTotalColumn::className(),
            'dataProviderWithoutPaginate' => $withOrders ? $this->dataProviderWithOrdersTotal : $this->dataProviderWithoutOrdersTotal,
            'attribute' => 'online',
            'label' => \Yii::t('common', 'Время онлайн'),
            'value' => function ($row) {
                return $row['online'];
            },
            'visible' => in_array($this->group, [self::GROUP_HOUR, self::GROUP_DAY, self::GROUP_MONTH, '']),
            'footerOptions' => ['class' => 'text-success'],
            'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'online-data'],
        ];
        $columns[] = [
            'attribute' => 'average',
            'label' => \Yii::t('common', 'Среднее время, мин.'),
            'value' => function ($row) {
                return $row['average'];
            },
            'visible' => in_array($this->group, [self::GROUP_HOUR, self::GROUP_DAY, self::GROUP_MONTH])
        ];
        */
        return $columns;
    }
}
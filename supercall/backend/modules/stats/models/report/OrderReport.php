<?php

namespace backend\modules\stats\models\report;

use common\components\grid\AverageOrderCostColumn;
use common\components\grid\NumberColumn;
use common\components\grid\RatioColumn;
use common\components\grid\TotalColumn;
use common\models\Country;
use common\models\Product;
use common\models\Teams;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderSubStatus;
use common\modules\order\models\OrderSubStatusGroup;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Url;

/**
 * Class OrderGeneralStats
 * @package backend\modules\stats\models\report
 */
class OrderReport extends AbstractReport
{
    const DATA_TYPE_CREATED_AT = 'created_at';
    const DATA_TYPE_UPDATED_AT = 'updated_at';
    const DATA_TYPE_CREATED_AT_BY_PARTNER = 'partner_created_at';

    public $prefix = 'sub_status_';

    /** @var  integer */
    public $user_id;

    /**
     * @param $group_id
     * @return array
     */
    public function getSubStatusesList($group_id)
    {
        $sub_statuses_by_group = OrderSubStatus::getSubStatusesByGroup($group_id);
        return ArrayHelper::map($sub_statuses_by_group, 'code', 'name');
    }

    /**
     * Главный отчет по заказам
     * @return array
     */
    public function getGeneralAggregate()
    {

        //$table = in_array($this->type_date, ['created_at', 'partner_created_at']) ? 'o' : 'ol';

        $select = [
            'group' => $this->getFieldNameByGroupAggregate(),
            'total' => new Expression('COUNT(*)'),
            'midcheck' => new Expression('SUM(CASE WHEN o.status::integer = ' . Order::STATUS_APPROVED . ' THEN final_price ELSE 0 END) / 
                                                        (CASE WHEN sum(CASE WHEN o.status::integer = ' . Order::STATUS_APPROVED . ' THEN 1 ELSE 0 END) > 0
                                                        THEN sum(CASE WHEN o.status::integer = ' . Order::STATUS_APPROVED . ' THEN 1 ELSE 0 END) ELSE 1 END)'),
        ];

        //вариант учитывает историю заказа - т.е. подсчитывается кол-во статусов заказов на конкретный период, текущий статус заказа не учитывается, только статусы на период выборки
        foreach (Order::getStatusesCollection() as $id => $name) {
            //при создании заказа field = status е формируется
            if ($id == Order::STATUS_NEW) {
                $statusField = 'CASE WHEN o.status::integer = ' . $id . ' or o.status is null THEN 1 ELSE 0 END';
            } else {
                $statusField = 'CASE WHEN o.status::integer = ' . $id . ' THEN 1 ELSE 0 END';
            }

            $select[] = new Expression('SUM(' . $statusField . ') as status_quantity_' . $id);
            $select[] = new Expression('CASE WHEN COUNT(*) = 0 THEN 0 ELSE ROUND(SUM(' . $statusField . ')*100/COUNT(*)::decimal, 2) END as status_quantity_' . $id . '_percent');
        }

        $query = (new Query())
            ->from(Order::tableName() . ' as o')
            //->leftJoin('{{%order_log}} ol', 'ol.order_id=o.id')
            //->where(['ol.field' => 'status'])
            ->where(['>=', 'o.' . $this->type_date, strtotime($this->date_start)])
            ->andWhere(['<=', 'o.' . $this->type_date, strtotime($this->date_end)])
            ///->andWhere(new Expression('new <> old'))
            ->groupBy('group')
            ->orderBy('group');

        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = o.country_id');
            $select['group'] = 'c.name';
        }

        if ($this->group == static::GROUP_PRODUCT || $this->product) {
            $query->innerJoin(['op' => '(select order_id, product_id from order_product group by order_id, product_id)'], 'op.order_id = o.id');

            if ($this->group == static::GROUP_PRODUCT) {
                $query->innerJoin('{{%product}} p', 'op.product_id = p.id');
                $select['group'] = 'p.name';
            }

            if ($this->product) {
                $query->andWhere(['op.product_id' => $this->product]);
            }
        }

        /*$query->leftJoin(OrderLog::tableName(), OrderLog::tableName().'.order_id=o.id');
          $query->leftJoin(User::tableName(), User::tableName().'.id='.OrderLog::tableName().'.user_id');*/

        /*if($this->team) {
            $users = Teams::getUsers($this->team);

             if(is_array($users)){
                 $user_ids = ArrayHelper::getColumn($users, 'id');
                 $query->andFilterWhere(['user.id' => $user_ids]);
             }
         }else{*/
        /*$userModel = yii::$app->user->getUserModel();
         $users = $userModel->getCoTeamsUsers();
         if(!empty($users)){
         $user_ids = ArrayHelper::getColumn($users, 'id');
        $query->andFilterWhere(['user.id' => $user_ids]);
        }*/

        //}

        //метод дергается так же кроном
        if (!yii::$app->user->id && $this->user_id) {
            $user = User::findOne(['id' => $this->user_id]);
        } else {
            $user = yii::$app->user->identity;
        }

        if (!$user->isSuperadmin) {
            $query->andWhere(['o.partner_id' => ArrayHelper::getColumn($user->partners, 'id')]);
        }

        $query->select($select);

        if (!$this->country) {
            $query->andFilterWhere(['in', 'o.country_id', $this->getCountries()]);
        }

        if ($this->queue) {
            $finalStatusAsStr = implode(',', array_keys(Order::getFinalStatusesCollection()));
            $queueIdsAsStr = implode(',', $this->queue);

            $query->andWhere('
               CASE WHEN  o.status IN (' . $finalStatusAsStr . ')
               THEN  o.prevent_queue_id IN (' . $queueIdsAsStr . ')
               ELSE  o.last_queue_id IN (' . $queueIdsAsStr . ') END ');
        }
        if($this->type){
            $query->andWhere(['o.type_id' => $this->type]);
        }

        if($this->partner){
            $query->andWhere(['o.partner_id' => $this->partner]);
        }

        return [
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $query->all(),
                'pagination' => false
            ]),
        ];
    }

    /**
     * Главный отчет по заказам
     * @return array
     */
    public function getGeneral()
    {
        $select = [
            'group' => $this->getFieldNameByGroup(),
            'total' => new Expression('SUM(rp.quantity)'),
        ];
        foreach (Order::getStatusesCollection() as $id => $name) {
            $statusField = '
                CASE
                    WHEN rp.status = ' . $id . ' THEN rp.quantity ELSE 0
                END
            ';
            $select[] = new Expression('SUM(' . $statusField . ') as status_quantity_' . $id);
        }

        $query = (new Query())
            ->from('{{%order_olap_general}} rp')
            ->groupBy('group')
            ->orderBy('group');

        // Если фильтруется по стране
        // Если в фильтре страна не выбрана, то стата формируется для всех доступных стран
        $query->andWhere(['in', 'country_id', $this->getCountries()]);

        $query->andWhere([
            'period_type' => $this->getPeriodType()
        ]);
        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = rp.country_id');
            $select['group'] = 'c.name';
        } elseif ($this->group == static::GROUP_PRODUCT) {
            $query->innerJoin('{{%product}} p', 'p.id = rp.product_id');
            $select['group'] = 'p.name';
        }

        $query->select($select);

        $this->addFilters($query);

        $q = (new Query())
            ->select(['t1.*', 't2.price_data'])
            ->from(['t1' => $query])
            ->leftJoin([
                't2' => (clone $query)
                    ->addSelect(['price_data' => new Expression('JSONB_AGG(rp.price_data)')])
                    ->orderBy([])
                    ->andWhere(['status' => Order::STATUS_APPROVED])
            ], 't1.group = t2.group');

        return $this->returnWithPagination($q);
    }

    /**
     * @return array
     */
    public function getGeneralColumnsAggregate()
    {
        $report = $this;
        $columns = [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function ($row) use ($report) {
                    return $report->formatGroupView($row);
                },
                'footer' => \Yii::t('common', 'Всего:'),
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => \Yii::t('common', 'Всего:'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'total'],
            ]
        ];
        foreach (Order::getStatusesCollection() as $status_id => $status_label) {
            $columns[] = [
                'class' => NumberColumn::className(),
                'attribute' => 'status_quantity_' . $status_id,
                'label' => $status_label,
                'contentOptions' => function ($model) use ($status_id, $report) {
                    $cell = 'status_quantity_' . $status_id;
                    $url = $model[$cell] > 0
                        ? Url::toRoute([
                            '/order/index',
                            'OrderSearch' => [
                                'countries' => $report->group == static::GROUP_COUNTRY ? Country::findOne(['name' => $model['group']])->id : $this->countries,
                                'partner_id' => $this->partner,
                                'status' => $status_id,
                                'type_id' => $this->type,
                                'type_date' => $this->type_date,
                                'product' => $report->group == static::GROUP_PRODUCT ? Product::findOne(['name' => $model['group']])->id : $this->product,
                                'id' => '',
                                'foreign_id' => '',
                                'date_range' => $this->getPeriodByGroup($model['group']),
                                'customer_full_name' => '',
                                'customerPhone' => '',
                                'customerMobile' => '',
                                'last_queue_id' => '',
                                'with_timezone' => 1,
                            ]
                        ]) : null;
                    return [
                        'class' => 'text-success',
                        'data-url' => $url
                    ];
                },
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $status_id],
            ];
            $columns[] = [
                'class' => RatioColumn::className(),
                'attribute' => 'status_quantity_' . $status_id,
                'label' => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'value' => function ($row) use ($status_id) {
                    $curr = (int)$row['status_quantity_' . $status_id];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr / $total * 100, 2) : 0;
                },
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $status_id . '-percent'],
            ];
        }
        $columns[] = [
            'class' => AverageOrderCostColumn::className(),
            'countColumn' => 'status_quantity_' . Order::STATUS_APPROVED,
            'attribute' => 'midcheck',
            'label' => \Yii::t('common', 'Средний чек'),
            'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'average-price'],
        ];
        return $columns;
    }

    /**
     * @return array
     */
    public function getGeneralColumns()
    {
        $report = $this;
        $columns = [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function ($row) use ($report) {
                    return $report->formatGroupView($row);
                },
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => 'Всего',
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'total'],
            ]
        ];
        foreach (Order::getStatusesCollection() as $status_id => $status_label) {
            $columns[] = [
                'class' => NumberColumn::className(),
                'attribute' => 'status_quantity_' . $status_id,
                'label' => $status_label,
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $status_id],
            ];
            $columns[] = [
                'class' => RatioColumn::className(),
                'attribute' => 'status_quantity_' . $status_id,
                'label' => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'value' => function ($row) use ($status_id) {
                    $curr = (int)$row['status_quantity_' . $status_id];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr / $total * 100, 2) : 0;
                },
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $status_id . '-percent'],
            ];
        }
        $columns[] = [
            // Если группировка по стране, то средн.чек вместе с конвертацией д.б.
            /*'class' => $this->group == self::GROUP_COUNTRY
                ? AverageCurrencyColumn::className()
                : AverageColumn::className(),*/
            'class' => AverageOrderCostColumn::className(),
            'countColumn' => 'status_quantity_' . Order::STATUS_APPROVED,
            'attribute' => 'average-price',
            'label' => \Yii::t('common', 'Средний чек'),
            'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'average-price'],
        ];
        return $columns;
    }

    public function getAttempts()
    {
        $select = [
            'quantity' => new Expression('count(rp.id)'),
            'attempts' => new Expression('(rp.attempts)'),
        ];

        $query = (new Query())
            ->from(Order::tableName() . ' rp')
            ->where(['rp.status' => Order::STATUS_APPROVED])
            ->groupBy(['attempts'])
            ->orderBy('attempts');

        $query->select($select);

        $this->addFilters($query);

        if ($this->queue) {
            $query->andWhere(['rp.prevent_queue_id' => $this->queue]);
        }

        $data = $query->all();

        $total = array_reduce($data, function ($carry, $row) {
            return $carry + $row['quantity'];
        }, 0);
        $data = array_map(function ($row) use ($total) {
            $row['ratio'] = round($row['quantity'] / $total * 100, 2);
            return $row;
        }, $data);

        return new ArrayDataProvider(['allModels' => $data, 'pagination' => false]);
    }

    public function getAttemptsColumns()
    {
        return [
            [
                'class' => NumberColumn::className(),
                'attribute' => 'quantity',
                'label' => \Yii::t('common', 'Заказов'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'quantity'],
            ],
            [
                'class' => TotalColumn::className(),
                'totalColumn' => 'quantity',
                'attribute' => 'attempts',
                'label' => \Yii::t('common', 'Попыток'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'attempts'],

            ],
            [
                'attribute' => 'ratio',
                'label' => \Yii::t('common', '%'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'ratio'],
            ]
        ];
    }

    public function getApprovedStats()
    {
        $list_sub_statuses = $this->getSubStatusesList(Order::SUB_STATUS_GROUP_BUYOUT);

        $select = [
            'group' => $this->getFieldNameByGroup(),
            'total' => new Expression('count(rp.status in(' . Order::STATUS_APPROVED . '))'),
            'in-progress' => new Expression('count(rp.sub_status is null or null)')
        ];

        foreach ($list_sub_statuses as $code => $name) {
            $select[$this->prefix . $code] = new Expression('count(rp.sub_status in (' . $code . ') or null)');
        }

        $query = (new Query())
            ->from(Order::tableName() . ' rp')
            ->andWhere(['rp.status' => Order::STATUS_APPROVED])
            ->groupBy('group')
            ->orderBy('group');

        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = rp.country_id');
            $select['group'] = 'c.name';
        } elseif ($this->group == static::GROUP_PRODUCT) {
            $query->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id=rp.id');
            $query->leftJoin(Product::tableName() . ' p', 'p.id=' . OrderProduct::tableName() . '.product_id');
            $select['group'] = 'p.name';
        }

        if ($this->queue) {
            $query->andWhere(['rp.prevent_queue_id' => $this->queue]);
        }

        $query->select($select);

        $this->addFilters($query);

        $data = $query->all();

        $total = array_reduce($data, function ($carry, $row) {
            return $carry + $row['total'];
        }, 0);
        $data = array_map(function ($row) use ($total, $list_sub_statuses) {
            $row['in-progress-percent'] = round($row['in-progress'] / $total * 100, 2);

            foreach ($list_sub_statuses as $code => $name) {
                $row[$this->prefix . $code . '-percent'] = round($row[$this->prefix . $code] / $total * 100, 2);
            }

            return $row;
        }, $data);

        return new ArrayDataProvider(['allModels' => $data, 'pagination' => false]);
    }

    /**
     * @return array
     */
    public function getApprovedColumns()
    {
        $report = $this;

        $list_sub_statuses = $this->getSubStatusesList(Order::SUB_STATUS_GROUP_BUYOUT);

        $columns = [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function ($row) use ($report) {
                    return $report->formatGroupView($row);
                },
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => \Yii::t('common', 'Всего'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'in-progress',
                'label' => yii::t('common', yii::t('common', 'В процессе')),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'in-progress'],
            ],
            [
                'class' => RatioColumn::className(),
                'attribute' => 'in-progress-percent',
                'label' => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'in-progress-percent'],
            ]
        ];

        foreach ($list_sub_statuses as $code => $column) {

            $columns[] = [
                'class' => NumberColumn::className(),
                'attribute' => $this->prefix . $code,
                'label' => yii::t('common', $column),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $this->prefix . $code],
            ];

            $columns[] = [
                'class' => RatioColumn::className(),
                'attribute' => $this->prefix . $code . '-percent',
                'label' => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $this->prefix . $code . '-percent'],
            ];
        }

        return $columns;
    }

    public function getRejectStats()
    {
        $list_sub_statuses = $this->getSubStatusesList(Order::SUB_STATUS_GROUP_REJECT);

        $select = [
            'group' => $this->getFieldNameByGroup(),
            'total' => new Expression('count(rp.sub_status in(' . implode(", ", array_keys($list_sub_statuses)) . '))'),
        ];

        foreach ($list_sub_statuses as $code => $name) {
            $select[$this->prefix . $code] = new Expression('count(rp.sub_status in (' . $code . ') or null)');
        }

        $query = (new Query())
            ->from(Order::tableName() . ' rp')
            ->andWhere(['rp.status' => Order::STATUS_REJECTED])
            ->groupBy('group')
            ->orderBy('group');

        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = rp.country_id');
            $select['group'] = 'c.name';
        } elseif ($this->group == static::GROUP_PRODUCT) {
            $query->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id=rp.id');
            $query->leftJoin(Product::tableName() . ' p', 'p.id=' . OrderProduct::tableName() . '.product_id');
            $select['group'] = 'p.name';
        }

        if ($this->queue) {
            $query->andWhere(['rp.prevent_queue_id' => $this->queue]);
        }

        $query->select($select);

        $this->addFilters($query);

        $data = $query->all();

        $total = array_reduce($data, function ($carry, $row) {
            return $carry + $row['total'];
        }, 0);

        $data = array_map(function ($row) use ($total, $list_sub_statuses) {

            foreach ($list_sub_statuses as $code => $name) {
                $row[$this->prefix . $code . '-percent'] = round($row[$this->prefix . $code] / $total * 100, 2);
            }

            return $row;
        }, $data);

        return new ArrayDataProvider(['allModels' => $data, 'pagination' => false]);
    }

    /**
     * @return array
     */
    public function getRejectColumns()
    {
        $report = $this;

        $list_sub_statuses = $this->getSubStatusesList(Order::SUB_STATUS_GROUP_REJECT);

        $columns = [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function ($row) use ($report) {
                    return $report->formatGroupView($row);
                },
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => \Yii::t('common', 'Всего'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
        ];

        foreach ($list_sub_statuses as $code => $column) {
            $columns[] = [
                'class' => NumberColumn::className(),
                'attribute' => $this->prefix . $code,
                'label' => yii::t('common', $column),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $this->prefix . $code],
            ];

            $columns[] = [
                'class' => RatioColumn::className(),
                'attribute' => $this->prefix . $code . '-percent',
                'label' => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $this->prefix . $code . '-percent'],
            ];
        }

        return $columns;
    }

    public function getTrashStats()
    {
        $list_sub_statuses = $this->getSubStatusesList(Order::SUB_STATUS_GROUP_TRASH);

        $select = [
            'group' => $this->getFieldNameByGroup(),
            'total' => new Expression('count(rp.sub_status in(' . implode(", ", array_keys($list_sub_statuses)) . '))'),
        ];

        foreach ($list_sub_statuses as $code => $name) {
            $select[$this->prefix . $code] = new Expression('count(rp.sub_status in (' . $code . ')  or null)');
        }

        $query = (new Query())
            ->from(Order::tableName() . ' rp')
            ->andWhere(['rp.status' => Order::STATUS_TRASH])
            ->groupBy('group')
            ->orderBy('group');

        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = rp.country_id');
            $select['group'] = 'c.name';
        } elseif ($this->group == static::GROUP_PRODUCT) {
            $query->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id=rp.id');
            $query->leftJoin(Product::tableName() . ' p', 'p.id=' . OrderProduct::tableName() . '.product_id');
            $select['group'] = 'p.name';
        }

        if ($this->queue) {
            $query->andWhere(['rp.prevent_queue_id' => $this->queue]);
        }

        $query->select($select);

        $this->addFilters($query);

        $data = $query->all();

        $total = array_reduce($data, function ($carry, $row) {
            return $carry + $row['total'];
        }, 0);

        $data = array_map(function ($row) use ($total, $list_sub_statuses) {

            foreach ($list_sub_statuses as $code => $name) {
                $row[$this->prefix . $code . '-percent'] = round($row[$this->prefix . $code] / $total * 100, 2);
            }

            return $row;
        }, $data);

        return new ArrayDataProvider(['allModels' => $data, 'pagination' => false]);
    }

    /**
     * @return array
     */
    public function getTrashColumns()
    {
        $report = $this;

        $list_sub_statuses = $this->getSubStatusesList(Order::SUB_STATUS_GROUP_TRASH);

        $columns = [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function ($row) use ($report) {
                    return $report->formatGroupView($row);
                },
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => \Yii::t('common', 'Всего'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
        ];

        foreach ($list_sub_statuses as $code => $column) {
            $columns[] = [
                'class' => NumberColumn::className(),
                'attribute' => $this->prefix . $code,
                'label' => yii::t('common', $column),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $this->prefix . $code],
            ];

            $columns[] = [
                'class' => RatioColumn::className(),
                'attribute' => $this->prefix . $code . '-percent',
                'label' => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $this->prefix . $code . '-percent'],
            ];
        }

        return $columns;
    }
}
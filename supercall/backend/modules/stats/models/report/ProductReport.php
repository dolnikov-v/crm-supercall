<?php

namespace backend\modules\stats\models\report;

use backend\modules\stats\models\Queue;
use common\components\grid\AverageOrderCostColumn;
use common\components\grid\NumberColumn;
use common\components\grid\RatioColumn;
use common\models\Product;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProduct;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use common\models\Teams;
use yii\helpers\ArrayHelper;

/**
 * Class ProductReport
 * @package backend\modules\stats\models\report
 */
class ProductReport extends AbstractReport
{
    /**
     * Главный отчет по заказам
     * @return array
     */
    public function getGeneralAggregate()
    {
        $select = [
            'product_name' => Product::tableName() . '.name',
            'group' => $this->getFieldNameByGroupAggregate(),
            'total' => new Expression('COUNT(*)'),
            'midcheck' => new Expression('SUM(CASE WHEN o.status = 4 THEN init_price ELSE 0 END) / 
                                                        (CASE WHEN sum(CASE WHEN o.status = 4 THEN 1 ELSE 0 END) > 0
                                                        THEN sum(CASE WHEN o.status = 4 THEN 1 ELSE 0 END) ELSE 1 END)'),
        ];

        foreach (Order::getStatusesCollection() as $id => $name) {
            $statusField = 'CASE WHEN o.status = ' . $id . ' THEN 1 ELSE 0 END';
            $select[] = new Expression('SUM(' . $statusField . ') as status_quantity_' . $id);
        }

        $order_product = OrderProduct::find()
            ->select(['product_id', 'order_id'])
            ->distinct()
            ->leftJoin(Order::tableName(), Order::tableName() . '.id = order_id');

        if ($this->with_timezone) {
            $order_product->andFilterWhere(['>=', Order::tableName() . '.' . $this->type_date, strtotime($this->date_start . ':00 ' . \Yii::$app->user->timezone->timezone_id)]);
            $order_product->andFilterWhere(['<=', Order::tableName() . '.' . $this->type_date, strtotime($this->date_end . ':59 ' . \Yii::$app->user->timezone->timezone_id)]);
        } else {
            $order_product->andFilterWhere(['>=', Order::tableName() . '.' . $this->type_date, strtotime($this->date_start . ':00 ')]);
            $order_product->andFilterWhere(['<=', Order::tableName() . '.' . $this->type_date, strtotime($this->date_end . ':59 ')]);
        }

        if ($this->countries) {
            $order_product->andWhere([Order::tableName() . '.country_id' => $this->countries]);
        }

        if ($this->queue) {
            $finalStatusAsStr = implode(',', array_keys(Order::getFinalStatusesCollection()));
            $queueIdsAsStr = implode(',', $this->queue);

            $order_product->andWhere('
               CASE WHEN ' . Order::tableName() . '.status IN (' . $finalStatusAsStr . ')
               THEN ' . Order::tableName() . '.prevent_queue_id IN (' . $queueIdsAsStr . ')
               ELSE ' . Order::tableName() . '.last_queue_id IN (' . $queueIdsAsStr . ') END ');
        }


        $query = (new Query())
            ->from(Product::tableName())
            ->leftJoin(['op' => $order_product], 'op.product_id = ' . Product::tableName() . '.id')
            ->leftJoin(Order::tableName() . ' as o', 'o.id=op.order_id')
            ->groupBy(['group', Product::tableName() . '.id'])
            ->orderBy('group');

        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = o.country_id');
            $select['group'] = 'c.name';
        }

        if ($this->group == static::GROUP_PRODUCT) {
            $select['group'] = Product::tableName() . '.id';
        }

        $query->select($select);
        $this->addFiltersAggregate($query);
        $this->addCustomFilters($query);

        return [
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $query->all(),
                'pagination' => false
            ]),
        ];
    }

    /**
     * @param Query $query
     */
    public function addCustomFilters($query)
    {
        if ($this->product) {
            $query->andFilterWhere([Product::tableName() . '.id' => $this->product]);
        }

        /*$query->leftJoin(OrderLog::tableName(), OrderLog::tableName().'.order_id=o.id');
        $query->leftJoin(User::tableName(), User::tableName().'.id='.OrderLog::tableName().'.user_id');*/

        /*if($this->team) {
            $users = Teams::getUsers($this->team);

            if(is_array($users)){
                $user_ids = ArrayHelper::getColumn($users, 'id');
                $query->andFilterWhere(['user.id' => $user_ids]);
            }
        }else{*/
        /*$userModel = yii::$app->user->getUserModel();
        $users = $userModel->getCoTeamsUsers();
        if(!empty($users)){
            $user_ids = ArrayHelper::getColumn($users, 'id');
            $query->andFilterWhere(['user.id' => $user_ids]);
        }*/
        //}
    }

    /**
     * @return array
     */
    public function getGeneralColumnsAggregate()
    {
        $report = $this;
        $columns = [
            [
                'attribute' => 'product_name',
                'label' => \Yii::t('common', 'Товар'),
                'value' => function ($row) use ($report) {
                    return $row['product_name'];
                }
            ],
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function ($row) use ($report) {
                    return $report->formatGroupView($row);
                },
                'footer' => \Yii::t('common', 'Всего:'),
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => \Yii::t('common', 'Всего:'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'total'],
            ]
        ];
        foreach (Order::getStatusesCollection() as $status_id => $status_label) {
            $columns[] = [
                'class' => NumberColumn::className(),
                'attribute' => 'status_quantity_' . $status_id,
                'label' => $status_label,
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $status_id],
            ];
            $columns[] = [
                'class' => RatioColumn::className(),
                'attribute' => 'status_quantity_' . $status_id,
                'label' => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions' => ['class' => 'text-danger'],
                'value' => function ($row) use ($status_id) {
                    $curr = (int)$row['status_quantity_' . $status_id];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr / $total * 100, 2) : 0;
                },
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => $status_id . '-percent'],
            ];
        }
        $columns[] = [
            'class' => AverageOrderCostColumn::className(),
            'countColumn' => 'status_quantity_' . Order::STATUS_APPROVED,
            'attribute' => 'midcheck',
            'label' => \Yii::t('common', 'Средний чек'),
            'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'average-price'],
        ];
        return $columns;
    }


}
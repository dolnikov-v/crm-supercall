<?php

namespace backend\modules\stats\models\report;

use common\models\Product;
use common\models\Teams;
use common\models\Timezone;
use common\models\UserReady;
use common\modules\order\models\Order;
use backend\modules\stats\models\olap\AbstractOlap;
use common\modules\order\models\OrderProduct;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class AbstractReport
 * @package backend\modules\stats\models\report
 */
abstract class AbstractReport extends Model
{
    const GROUP_HOUR = 'hour';
    const GROUP_DAY = 'day';
    const GROUP_MONTH = 'month';
    const GROUP_PRODUCT = 'product';
    const GROUP_COUNTRY = 'country';
    const GROUP_OPERATOR = 'operator';

    public $date_range;
    public $date_start;
    public $date_end;
    public $group = self::GROUP_DAY;
    public $type;
    public $country;
    public $product;
    public $partner;
    public $team;
    public $team_id;
    public $type_date = 'created_at';
    public $with_timezone;
    public $queue;
    public $statistics_by_orders;

    /** @var При мульти-выборе страны в фильтре */
    public $countries;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_range', 'date_start', 'date_end', 'group', 'type', 'country', 'product', 'partner', 'countries', 'team', 'type_date', 'with_timezone', 'username', 'queue', 'statistics_by_orders'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date_range' => \Yii::t('common', 'Дата'),
            'date_start' => \Yii::t('common', 'От'),
            'date_end' => \Yii::t('common', 'До'),
            'group' => \Yii::t('common', 'Группировка'),
            'type' => \Yii::t('common', 'Тип заказа'),
            'country' => \Yii::t('common', 'Страна'),
            'product' => \Yii::t('common', 'Товар'),
            'partner' => \Yii::t('common', 'Партнер'),
            'countries' => \Yii::t('common', 'Страна'),
            'type_date' => \Yii::t('common', 'Тип даты'),
            'with_timezone' => \Yii::t('common', 'Использовать часовой пояс'),
            'queue' => \Yii::t('common', 'Очередь'),
            'statistics_by_orders' =>  \Yii::t('common', 'Статистика по заказам'),
        ];
    }

    /**
     * Возвращает имя поля по группировке
     * @return mixed
     */
    public function getFieldNameByGroupAggregate($table = 'o')
    {
        $dateField = $this->type_date ?? 'created_at';

        if($table == UserReady::tableName()){
            $dateField = 'start_time';
        }

        if ($this->with_timezone) {
            $group_hour = 'extract(epoch from date_trunc(\'hour\', to_timestamp(' . $table . '.' . $dateField . ') at time zone \'' . \Yii::$app->user->timezone->timezone_id . '\'))';
            $group_day = 'extract(epoch from date_trunc(\'day\', to_timestamp(' . $table . '.' . $dateField . ') at time zone \'' . \Yii::$app->user->timezone->timezone_id . '\'))';
            $group_month = 'extract(epoch from date_trunc(\'month\', to_timestamp(' . $table . '.' . $dateField . ') at time zone \'' . \Yii::$app->user->timezone->timezone_id . '\'))';
        } else {
            $group_hour = 'extract(epoch from date_trunc(\'hour\', to_timestamp(' . $table . '.' . $dateField . ') at time zone \'UTC\'))';
            $group_day = 'extract(epoch from date_trunc(\'day\', to_timestamp(' . $table . '.' . $dateField . ') at time zone \'UTC\'))';
            $group_month = 'extract(epoch from date_trunc(\'month\', to_timestamp(' . $table . '.' . $dateField . ') at time zone \'UTC\'))';
        }

        $map = [
            static::GROUP_HOUR => $group_hour,
            static::GROUP_DAY => $group_day,
            static::GROUP_MONTH => $group_month,
            static::GROUP_PRODUCT => 'product.name',
            static::GROUP_COUNTRY => 'country.name',
            static::GROUP_OPERATOR => 'case when "operator" ISNULL OR "operator" = \'\' then "u"."username" else "operator" end',
        ];

        return empty($this->group) ? false : $map[$this->group];
    }

    public function getWorkTimeFieldNameByGroupAggregate()
    {
        if ($this->with_timezone) {
            $group_hour = 'extract(epoch from date_trunc(\'hour\', to_timestamp(' . UserReady::tableName() . '.start_time) at time zone \'' . \Yii::$app->user->timezone->timezone_id . '\'))';
            $group_day = 'extract(epoch from date_trunc(\'day\', to_timestamp(' . UserReady::tableName() . '.start_time) at time zone \'' . \Yii::$app->user->timezone->timezone_id . '\'))';
            $group_month = 'extract(epoch from date_trunc(\'month\', to_timestamp(' . UserReady::tableName() . '.start_time) at time zone \'' . \Yii::$app->user->timezone->timezone_id . '\'))';
        } else {
            $group_hour = 'extract(epoch from date_trunc(\'hour\', to_timestamp(' . UserReady::tableName() . '.start_time) at time zone \'UTC\'))';
            $group_day = 'extract(epoch from date_trunc(\'day\', to_timestamp(' . UserReady::tableName() . '.start_time) at time zone \'UTC\'))';
            $group_month = 'extract(epoch from date_trunc(\'month\', to_timestamp(' . UserReady::tableName() . '.start_time) at time zone \'UTC\'))';
        }

        $map = [
            static::GROUP_HOUR => $group_hour,
            static::GROUP_DAY => $group_day,
            static::GROUP_MONTH => $group_month,
            static::GROUP_PRODUCT => 'product.name',
            static::GROUP_COUNTRY => 'country.name',
            static::GROUP_OPERATOR => 'case when "operator" ISNULL OR "operator" = \'\' then "u"."username" else "operator" end',
        ];

        return empty($this->group) ? false : $map[$this->group];
    }

    /**
     * Возвращает имя поля по группировке
     * @param bool $is_log true - для отчетов по order_log...
     * @return mixed
     */
    public function getFieldNameByGroup($is_log = false)
    {
        $column = ($is_log) ? 'period' : $this->type_date;

        if ($this->with_timezone) {
            $group_hour = 'extract(epoch from date_trunc(\'hour\', to_timestamp(rp.' . $column . ') at time zone \'' . Timezone::DEFAULT_TIMEZONE . '\'))';
            $group_day = 'extract(epoch from date_trunc(\'day\', to_timestamp(rp.' . $column . ') at time zone \'' . Timezone::DEFAULT_TIMEZONE . '\'))';
            $group_month = 'extract(epoch from date_trunc(\'month\', to_timestamp(rp.' . $column . ') at time zone \'' . Timezone::DEFAULT_TIMEZONE . '\'))';
        } else {
            $group_hour = 'extract(epoch from date_trunc(\'hour\', to_timestamp(rp.' . $column . ') at time zone \'UTC\'))';
            $group_day = 'extract(epoch from date_trunc(\'day\', to_timestamp(rp.' . $column . ') at time zone \'UTC\'))';
            $group_month = 'extract(epoch from date_trunc(\'month\', to_timestamp(rp.' . $column . ') at time zone \'UTC\'))';
        }

        $map = [
            static::GROUP_HOUR => $group_hour,
            static::GROUP_DAY => $group_day,
            static::GROUP_MONTH => $group_month,
            static::GROUP_PRODUCT => 'product_id',
            static::GROUP_COUNTRY => 'country_id',
            static::GROUP_OPERATOR => 'operator',
        ];

        return empty($this->group) ? false : $map[$this->group];
    }

    /**
     * @return mixed
     */
    public function getPeriodType()
    {
        return AbstractOlap::PERIOD_TYPE_HOUR;
//        switch ($this->group){
//            case static::GROUP_HOUR:
//                return AbstractOlap::PERIOD_TYPE_HOUR;
//                break;
//
//            case static::GROUP_DAY:
//                return AbstractOlap::PERIOD_TYPE_DAY;
//                break;
//
//            case static::GROUP_MONTH:
//                $from = (new \DateTime($this->date_start));
//                $from_test = (new \DateTime($this->date_start))->modify('first day of this month');
//                $to = (new \DateTime($this->date_end));
//                $to_test = (new \DateTime($this->date_end))->modify('last day of this month');
//
//                if ($from->getTimestamp() == $from_test->getTimestamp() && $to->getTimestamp() == $to_test->getTimestamp()){
//                    return AbstractOlap::PERIOD_TYPE_MONTH;
//                }
//                else {
//                    return AbstractOlap::PERIOD_TYPE_DAY;
//                }
//
//                break;
//            default:
//                return AbstractOlap::PERIOD_TYPE_DAY;
//        }
    }

    /**
     * Определяет явлется группировка группировкой по периоду
     * @return mixed
     */
    public function isGroupPeriod()
    {
        $map = [
            static::GROUP_HOUR => true,
            static::GROUP_DAY => true,
            static::GROUP_MONTH => true,
            static::GROUP_PRODUCT => false,
            static::GROUP_COUNTRY => false,
            static::GROUP_OPERATOR => false,
        ];

        return empty($this->group) ? false : $map[$this->group];
    }

    /**
     * @return array
     */
    public static function getGroupsCollection()
    {
        return [
            self::GROUP_HOUR => \Yii::t('common', 'Час'),
            self::GROUP_DAY => \Yii::t('common', 'День'),
            self::GROUP_MONTH => \Yii::t('common', 'Месяц'),
            self::GROUP_PRODUCT => \Yii::t('common', 'Товар'),
            self::GROUP_COUNTRY => \Yii::t('common', 'Страна'),
        ];
    }

    /**
     * форматирует группировку при выводе
     * @param $row
     * @return false|string
     */
    public function formatGroupView($row)
    {
        switch ($this->group) {
            case static::GROUP_HOUR:
                return date('d.m.Y H:00', $row['group']);
                break;
            case static::GROUP_DAY:
                return date('d.m.Y', $row['group']);
                break;
            case static::GROUP_MONTH:
                return date('F Y', $row['group']);
                break;
            default:
                return $row['group'];
                break;
        }
    }

    /**
     * Добавляет фильтры
     * @param Query $query
     * @param string $tableName
     */
    protected function addFiltersAggregate($query, $tableName = 'o', $useCountry = true)
    {
        $date_field = 'created_at';

        if ($tableName == UserReady::tableName()) {
            $date_field = 'start_time';
        }

        $this->with_timezone = $this->with_timezone == 'on';

        if ($useCountry) {
            $query->andFilterWhere(['in', $tableName . '.country_id', $this->getCountries()]);
        }

        if ($tableName != UserReady::tableName()) {
            $query->andFilterWhere([
                'o.type_id' => $this->type,
                'o.partner_id' => $this->partner,
            ]);
        }

        if ($this->with_timezone) {
            $query->andFilterWhere([
                'between',
                $tableName . '.' . $date_field,
                strtotime($this->date_start . ':00 ' . \Yii::$app->user->timezone->timezone_id),
                strtotime($this->date_end . ':59 ' . \Yii::$app->user->timezone->timezone_id)
            ]);

        } else {
            $query->andFilterWhere([
                'between',
                $tableName . '.' . $date_field,
                strtotime($this->date_start . ':00'),
                strtotime($this->date_end . ':59')
            ]);

        }
    }


    /**
     * Добавляет фильтры
     * @param Query $query
     * @param string $tableName
     * @param bool $is_log true - для отчетов по order_log...
     */
    protected function addFilters($query, $tableName = 'rp', $is_log = false)
    {
        $column = ($is_log) ? 'period' : $this->type_date;

        if ($this->product && $this->group != self::GROUP_PRODUCT) {
            $query->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id=rp.id');
            $query->leftJoin(Product::tableName() . ' p', 'p.id=' . OrderProduct::tableName() . '.product_id');
            $query->andWhere(['p.id' => $this->product]);
        }

        if ($this->with_timezone) {
            $query->andFilterWhere(['>=', $tableName . '.' . $column, strtotime($this->date_start . \Yii::$app->user->timezone->timezone_id)]);
            $query->andFilterWhere(['<=', $tableName . '.' . $column, strtotime($this->date_end . \Yii::$app->user->timezone->timezone_id)]);
        } else {
            $query->andFilterWhere(['>=', $tableName . '.' . $column, strtotime($this->date_start)]);
            $query->andFilterWhere(['<=', $tableName . '.' . $column, strtotime($this->date_end)]);
        }

        $query->andFilterWhere([
            $tableName . '.type_id' => $this->type,
            $tableName . '.partner_id' => $this->partner,
        ]);

        if ($this->countries) {
            $query->andWhere([$tableName . '.country_id' => $this->countries]);
        }
    }

    /**
     * Возвращает результат с пагинацией
     * @param Query $query
     * @return array
     */
    protected function returnWithPagination($query)
    {
        $pager = new Pagination(['totalCount' => $query->count()]);

        return [
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $query
                    ->all(),
                'pagination' => false
            ]),
        ];
    }

    protected function getPeriodData()
    {
        //switch ()
    }

    /**
     * Функция вернет массив стран для фильтра статы
     * По умолчанию фильтр пустой, потому будут возвращены все доступные Пользователю страны.
     *
     * @return array
     */
    public function getCountries()
    {
        if (!$this->countries) {
            $this->countries = ArrayHelper::getColumn(\Yii::$app->user->countries, 'id');
        }

        return $this->countries;
    }

    /**
     * @param $groupField
     * @return string
     */
    public function getPeriodByGroup($groupField)
    {
        switch ($this->group) {
            case self::GROUP_COUNTRY :
            case self::GROUP_OPERATOR :
            case self::GROUP_PRODUCT :
                $date_range = $this->date_range;
                break;
            case self::GROUP_HOUR :
                $date_range_start = date('d/m/Y H:00:00', $groupField);
                $date_range = $date_range_start . ' - ' . date('d/m/Y H:i:s', strtotime('+59 minutes +59 seconds', $groupField));
                break;
            case self::GROUP_DAY :
                $date_range = date('d/m/Y 00:00:00', $groupField) . ' - ' . date('d/m/Y 23:59:59', $groupField);
                break;
            case self::GROUP_MONTH :
                $temp = explode(' - ', $this->date_range);

                $time_start = strtotime(strtr($temp[0], ['/' => '-']));
                $mounth_year_start = date('m/Y', $time_start);
                $time_end = strtotime(strtr($temp[1], ['/' => '-']));
                $mounth_year_end = date('m/Y', $time_end);

                //если инттервал дат в пределах одного месяца
                if ($mounth_year_start == $mounth_year_end) {
                    $date_range = date('d', $time_start) . '/' . date("m/Y 00:00:00", $groupField) . ' - ' . date('d', $time_end) . '/' . date("m/Y 23:59:59", $groupField);
                } else {
                    //если интервал более 1го месяца, а запрашиваемый месяц совпадает с первым
                    if (date('m/Y', $groupField) == $mounth_year_start) {
                        $date_range = date('d', $time_start) . '/' . date("m/Y 00:00:00", $groupField) . ' - ' . date("t/m/Y 23:59:59", $time_start);
                    } elseif (date('m/Y', $groupField) == $mounth_year_end) {
                        //если интервал более 1го месяца, а запрашиваемый месяц совпадает с последним
                        $date_range = date("01/m/Y 00:00:00", $time_end) . ' - ' . date('d', $time_end) . '/' . date("m/Y 23:59:59", $time_end);
                    } else {
                        //если интервал более 1го месяца, а запрашиваемый месяц совпадает с промежуточным
                        $date_range = date("01/m/Y 00:00:00", $groupField) . ' - ' . date("t/m/Y 23:59:59", $groupField);
                    }
                }
                break;
            default :
                $date_range = $this->date_range;
                break;
        }

        return $date_range;
    }
}
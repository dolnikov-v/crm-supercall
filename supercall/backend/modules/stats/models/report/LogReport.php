<?php
namespace backend\modules\stats\models\report;

use common\components\grid\AverageOrderCostColumn;
use common\components\grid\DateColumn;
use common\components\grid\NumberColumn;
use common\components\grid\RatioColumn;
use common\components\grid\TotalColumn;
use common\models\Teams;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class LogReport
 * @package backend\modules\stats\models\report
 */
class LogReport extends AbstractReport
{
    public $period;
    public $operator;
    public $status;

    /**
     * @var array
     */
    public static $date_formats = [
        self::GROUP_HOUR => 'Y-m-d H:00',
        self::GROUP_DAY => 'Y-m-d',
        self::GROUP_MONTH => 'Y-m',
    ];

    /**
     * @var array
     */
    public static $date_intervals = [
        self::GROUP_HOUR => '+1hour',
        self::GROUP_DAY => '+1day',
        self::GROUP_MONTH => '+1month',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['period', 'operator', 'status'], 'safe']
        ]);
    }

    /**
     * @return array
     */
    public static function getGroupsCollection()
    {
        return [
            self::GROUP_HOUR => \Yii::t('common', 'Час'),
            self::GROUP_DAY => \Yii::t('common', 'День'),
            self::GROUP_MONTH => \Yii::t('common', 'Месяц'),
//            self::GROUP_PRODUCT => \Yii::t('common', 'Товар'),
            self::GROUP_COUNTRY => \Yii::t('common', 'Страна'),
            self::GROUP_OPERATOR => \Yii::t('common', 'Оператор'),
        ];
    }

    /**
     * Главный отчет по заказам аггрегация
     * @return array
     */
    public function getGeneralAggregate()
    {
        $date = \Yii::$app->formatter->asConvertToFrom($this->date_range);
        $date_start = $date->from->format('d.m.Y H:i');
        $date_end = $date->to->format('d.m.Y H:i');

        $select = [
            'group' => $this->getFieldNameByGroupAggregate('ol'),
            'total' => new Expression('count(*)'),
            'midcheck' => new Expression('SUM(CASE WHEN new = 4 THEN init_price ELSE 0 END) / 
                                                        (CASE WHEN sum(CASE WHEN new = 4 THEN 1 ELSE 0 END) > 0
                                                        THEN sum(CASE WHEN new = 4 THEN 1 ELSE 0 END) ELSE 1 END)'),
        ];
        foreach (Order::getStatusesCollection() as $id => $name) {
            $statusField = 'CASE WHEN  new = ' . $id . ' THEN 1 ELSE 0 END';
            $select[] = new Expression('SUM(' . $statusField . ') as status_quantity_' . $id);
        }

        $query = (new Query())
            ->from('{{%order_logs}} ol')
            ->groupBy(['group'])
            ->orderBy('group');

        $query->innerJoin('{{%order}} o', 'o.id = ol.order_id');
        $query->leftJoin('{{%user}} u', 'u.id = ol.user_id');

        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = o.country_id');
            $select['group'] = 'c.name';
        }

        $query->select($select);
        $query
            ->andFilterWhere(['>=', 'ol.created_at', strtotime($date_start.\Yii::$app->user->timezone->timezone_id)])
            ->andFilterWhere(['<=', 'ol.created_at', strtotime($date_end.\Yii::$app->user->timezone->timezone_id)])
            ->andFilterWhere([
               'o.type_id' => $this->type,
               'o.partner_id' => $this->partner,
            ])
            ->andFilterWhere(['in', 'o.country_id', $this->getCountries()]);

        if($this->team) {
            $users = Teams::getUsers($this->team);

            if(is_array($users)){
                $user_ids = ArrayHelper::getColumn($users, 'id');
                $query->andFilterWhere(['u.id' => $user_ids]);
            }
        }else{
            $userModel = yii::$app->user->getUserModel();
            $users = $userModel->getCoTeamsUsers();
            if(!empty($users)){
                $user_ids = ArrayHelper::getColumn($users, 'id');
                $query->andFilterWhere(['user.id' => $user_ids]);
            }
        }

        return [
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $query->all(),
                'pagination' => false
            ]),
        ];
    }

    /**
     * Главный отчет по заказам
     * @return array
     */
    public function getGeneral()
    {
        $select = [
            'group' => $this->getFieldNameByGroup(),
            'group_value' => $this->getFieldNameByGroup(),
            'total' => new Expression('SUM(rp.quantity)'),
        ];
        foreach (Order::getStatusesCollection() as $id => $name) {
            $statusField = '
                CASE
                    WHEN rp.status = ' . $id . ' THEN rp.quantity ELSE 0
                END
            ';
            $select[] = new Expression('SUM(' . $statusField . ') as status_quantity_' . $id);
        }

        $query = (new Query())
            ->from('{{%order_log_olap_general}} rp')
            ->groupBy(['group', 'group_value'])
            ->orderBy('group');

        // Если фильтруется по стране
        // Если в фильтре страна не выбрана, то стата формируется для всех доступных стран
        $query->andWhere(['in', 'country_id', $this->getCountries()]);

        $query->andWhere([
            'period_type' => $this->getPeriodType()
        ]);
        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = rp.country_id');
            $select['group'] = 'c.name';
        } elseif ($this->group == static::GROUP_PRODUCT) {
            $query->innerJoin('{{%product}} p', 'p.id = rp.product_id');
            $select['group'] = 'p.name';
        }

        $query->select($select);

        $this->addFilters($query);

        $q = (new Query())
            ->select(['t1.*', 't2.price_data'])
            ->from(['t1' => $query])
            ->leftJoin([
                't2' => (clone $query)
                    ->addSelect(['price_data' => new Expression('JSONB_AGG(rp.price_data)')])
                    ->orderBy([])
                    ->andWhere(['status' => Order::STATUS_APPROVED])
            ], 't1.group = t2.group');

        return $this->returnWithPagination($q);
    }

    /**
     * @return array
     */
    public function getGeneralColumnsAggregate()
    {
        $report = $this;
        $columns = [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function($row) use ($report) {
                    return $report->formatGroupView($row) . Html::tag('span', '', [
                            'class' => 'row-group',
                            'data-group' => $row['group']
                        ]);
                },
                'footer' => \Yii::t('common', 'Всего:'),
                'format' => 'raw'
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => 'Всего',
                'contentOptions' => ['class' => 'text-success extra-trigger', 'data-status' => ''],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
            ]
        ];
        foreach (Order::getStatusesCollection() as $status_id => $status_label) {
            if($status_id == Order::STATUS_NEW){
                continue;
            }
            $columns[] = [
                'class'          => NumberColumn::className(),
                'attribute'      => 'status_quantity_' . $status_id,
                'label'          => $status_label,
                'contentOptions' => ['class' => 'text-success extra-trigger', 'data-status' => $status_id],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => $status_id],
            ];
            $columns[] = [
                'class'          => RatioColumn::className(),
                'attribute'      => 'status_quantity_' . $status_id,
                'label'          => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'value' => function($row) use ($status_id) {
                    $curr = (int)$row['status_quantity_' . $status_id];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr/$total * 100, 2) : 0;
                },
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => $status_id . '-percent'],
            ];
        }
        $columns[] = [
            'class' => AverageOrderCostColumn::className(),
            'countColumn' => 'status_quantity_'.Order::STATUS_APPROVED,
            'attribute' => 'midcheck',
            'label' => \Yii::t('common', 'Средний чек'),
            'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'average-price'],
        ];
        return $columns;
    }

    /**
     * @return array
     */
    public function getGeneralColumns()
    {
        $report = $this;
        $columns = [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function($row) use ($report) {
                    return $report->formatGroupView($row) . Html::tag('span', '', [
                        'class' => 'row-group',
                        'data-group' => $row['group_value']
                    ]);
                },
                'footer' => 'Всего:',
                'format' => 'raw'
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => 'Всего',
                'contentOptions' => ['class' => 'text-success extra-trigger', 'data-status' => ''],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
            ]
        ];
        foreach (Order::getStatusesCollection() as $status_id => $status_label) {
            $columns[] = [
                'class'          => NumberColumn::className(),
                'attribute'      => 'status_quantity_' . $status_id,
                'label'          => $status_label,
                'contentOptions' => ['class' => 'text-success extra-trigger', 'data-status' => $status_id],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => $status_id],
            ];
            $columns[] = [
                'class'          => RatioColumn::className(),
                'attribute'      => 'status_quantity_' . $status_id,
                'label'          => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'value' => function($row) use ($status_id) {
                    $curr = (int)$row['status_quantity_' . $status_id];
                    $total = (int)$row['total'];
                    return $total > 0 ? round($curr/$total * 100, 2) : 0;
                },
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => $status_id . '-percent'],
            ];
        }
        $columns[] = [
            // Если группировка по стране, то средн.чек вместе с конвертацией д.б.
            /*'class' => $this->group == self::GROUP_COUNTRY
                ? AverageCurrencyColumn::className()
                : AverageColumn::className(),*/
            'class' => AverageOrderCostColumn::className(),
            'countColumn' => 'status_quantity_'.Order::STATUS_APPROVED,
            'attribute' => 'average-price',
            'label' => \Yii::t('common', 'Средний чек'),
            'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'average-price'],
            'value' => function($row) {
                $priceData = json_decode($row['price_data'], true);
                if (!empty($priceData)) {
                    $sum = 0;
                    $quantity = 0;
                    foreach ($priceData as $row) {
                        if (!empty($row)) {
                            foreach ($row as $price => $ids) {
                                $quantity += count($ids);
                                $sum += (count($ids) * $price);
                            }
                        }
                    }
                    if ($quantity > 0) {
                        return round($sum/$quantity, 2);
                    }
                }
                return 0;
            },
        ];
        return $columns;
    }


    public function getDetails()
    {
        if ($this->status == 'total') {
            $this->status = null;
        }

        $create_query = (new Query())
            ->select(new Expression("
                log.order_id as order_id,
                log.request::text as fields,
                jsonb_agg(tmp.product) as product,
                string_agg(tmp.name::text, ', ') as \"productString\",
                null as old,
                1 as new,
                log.operator as operator,
                null as username,
                null as comment,
                log.created_at as created_at
            "))
            ->from('api_log as log')
            ->innerJoin(['tmp' => '(
              select
                api_log.id,
                jsonb_set(api_log.request::jsonb->\'products\', array[(row_number() over(partition by api_log.id) -1)::text, \'product_id\'], concat(\'"\',product.id,\'"\')::jsonb)->(row_number() over(partition by api_log.id) -1)::int as product,
                case when (api_log.request::jsonb->\'products\'->(row_number() over(partition by api_log.id) -1)::int->>\'quantity\')::int > 1 then concat(product.name, \'(\', api_log.request::jsonb->\'products\'->(row_number() over(partition by api_log.id) -1)::int->>\'quantity\', \')\') else product.name end as name
              from api_log
              inner join "order" as o on o.id = api_log.order_id
              inner join jsonb_to_recordset(api_log.request::jsonb->\'products\') as tmp(id integer, price text) on true
              inner join partner_product on partner_product.partner_id = o.partner_id and partner_product.partner_product_id = tmp.id
              inner join product on product.id = partner_product.product_id
            )'], 'tmp.id = log.id')
            ->innerJoin('order as o', 'o.id = log.order_id')
            ->where('log.url = \'/v1/order/create\'')
            ->andFilterWhere([
                'o.type_id' => $this->type,
                'o.partner_id' => $this->partner,
            ])
            ->andFilterWhere(['in', 'o.country_id', $this->countries])
            ->groupBy('log.id')
        ;

        if ($this->status) {
            $create_query->andWhere(new Expression('1 = :status', [':status' => $this->status]));
        }

        $change_query = (new Query())
            ->select(new Expression("
                log.order_id as order_id,
                log.fields::text as fields,
                jsonb_agg(tmp.product) as product,
                string_agg(tmp.name::text, ', ') as \"productString\",
                log.old as old,
                log.new as new,
                log.operator as operator,
                max(u.username) as username,
                log.comment as comment,
                log.created_at as created_at
            "))
            ->from('order_logs as log')
            ->innerJoin(['tmp' => '(
              select
                order_logs.id,
                order_logs.product->tmp.key as product,
                case when (order_logs.product->tmp.key->>\'quantity\')::int > 1 then concat(product.name, \'(\', order_logs.product->tmp.key->>\'quantity\', \')\') else product.name end as name
              from order_logs
              inner join "order" as o on o.id = order_logs.order_id
              inner join jsonb_each(order_logs.product) as tmp on true
              inner join product on product.id = (tmp.value->>\'product_id\')::int
            )'], 'tmp.id = log.id')
            ->innerJoin('order as o', 'o.id = log.order_id')
            ->leftJoin('user as u', 'u.id = log.user_id')
            ->andFilterWhere([
                'o.type_id' => $this->type,
                'o.partner_id' => $this->partner,
                'log.new' => $this->status,
            ])
            ->andFilterWhere(['in', 'o.country_id', $this->countries])
            ->groupBy('log.id')
        ;

        if ($this->product) {
            $create_query->andHaving('exists (select product_id from jsonb_to_recordset(jsonb_agg(tmp.product)) as p(product_id int) where product_id = :product)', [':product' => $this->product]);
            $change_query->andHaving('exists (select product_id from jsonb_to_recordset(jsonb_agg(tmp.product)) as p(product_id int) where product_id = :product)', [':product' => $this->product]);
        }

        if ($this->group == self::GROUP_OPERATOR) {
            $create_query->andWhere(['coalesce(log.operator, \'\')' => $this->operator]);
            $change_query->andWhere(['or', ['coalesce(log.operator, \'\')' => $this->operator], ['u.username' => $this->operator]]);
        }

        if ($this->country) {
            $create_query->andWhere(['country_id' => $this->country]);
            $change_query->andWhere(['country_id' => $this->country]);
        }

        $tz = \Yii::$app->user->timezone->timezone_id;

        if (in_array($this->group, [self::GROUP_PRODUCT, self::GROUP_OPERATOR, self::GROUP_COUNTRY])) {
            $create_query->andWhere(['>=', 'log.created_at', strtotime($this->date_start.':00 '. $tz)])
                ->andWhere(['<=', 'log.created_at', strtotime($this->date_end.':59 '. $tz)]);
            $change_query->andWhere(['>=', 'log.created_at', strtotime($this->date_start.':00 '. $tz)])
                ->andWhere(['<=', 'log.created_at', strtotime($this->date_end.':59 '. $tz)]);
        } else {
            $period = strtotime(date('Y-m-d H:i:s', $this->period).' '. $tz);

            $date_start = max(
                $period,
                strtotime($this->date_start.':00 '. $tz)
            );
            $date_end = min(
                strtotime(date('Y-m-d H:i:s', $this->period).' '.self::$date_intervals[$this->group].' '.$tz),
                strtotime($this->date_end.':59 '. $tz)
            );
            $create_query->andFilterWhere(['>=', 'log.created_at', $date_start])
                ->andFilterWhere(['<=', 'log.created_at', $date_end]);
            $change_query->andFilterWhere(['>=', 'log.created_at', $date_start])
                ->andFilterWhere(['<=', 'log.created_at', $date_end]);
        }

        $query = (new Query())->select('*')
            ->from(['log' => $create_query->union($change_query, true)])
            ->orderBy(['log.created_at' => SORT_ASC]);

        return new ActiveDataProvider(['query' => $query]);
    }

    public function getDetailsColumns()
    {
        $columns = [
            [
                'attribute' => 'order_id',
                'label' => \Yii::t('common', 'Заказ'),
                'format' => 'raw'
            ],
            [
                'attribute' => 'productString',
                'label' => \Yii::t('common', 'Товары'),
            ],
            [
                'attribute' => 'old',
                'label' => \Yii::t('common', 'Старый статус'),
                'value' => function ($row) {
                    return ArrayHelper::getValue(Order::getStatusesCollection(), $row['old']);
                }
            ],
            [
                'attribute' => 'new',
                'label' => \Yii::t('common', 'Новый статус'),
                'value' => function ($row) {
                    $result = ArrayHelper::getValue(Order::getStatusesCollection(), $row['new']);
                    // UN-40
                    // Если субстатус 505 - автоматич.отклонение по крону
                    $fields = Json::decode($row['fields']);
                    if ($row['new'] == 5 && ArrayHelper::getValue($fields, 'sub_status') == 505) {
                        $result .= ' (авто)';
                    }

                    return $result;
                }
            ],
            [
                'attribute' => 'operator',
                'label' => \Yii::t('common', 'Оператор'),
                'value' => function ($row) {
                    return !empty($row['operator'])
                        ? $row['operator']
                        : ( array_key_exists('username', $row) ? $row['username'] : null);
                }
            ],
            [
                'attribute' => 'comment',
                'label' => \Yii::t('common', 'Комментарий'),
                'value' => function ($row) {
                    return array_key_exists('comment', $row) ? $row['comment'] : null;
                }
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'timezone' => \Yii::$app->user->timezone->timezone_id,
                'enableSorting' => false,
                'label' => \Yii::t('common', 'Дата')
            ],
        ];
        return $columns;
    }

    public function getAttempts()
    {
        $select = [
            'quantity' => new Expression('SUM(rp.quantity)'),
            'attempts' => new Expression('(rp.attempts)'),
        ];

        $query = (new Query())
            ->from('{{%order_log_olap_attempts}} rp')
            ->groupBy(['attempts'])
            ->orderBy('attempts');

        $query->andWhere([
            'period_type' => $this->getPeriodType()
        ]);

        $query->select($select);

        $this->country = \Yii::$app->user->country->id;
        $this->addFilters($query);

        $data = $query->all();

        $total = array_reduce($data, function ($carry, $row) {
            return $carry + $row['quantity'];
        }, 0);
        $data = array_map(function ($row) use ($total) {
            $row['ratio'] = round($row['quantity'] / $total * 100, 2);
            return $row;
        }, $data);

        return new ArrayDataProvider(['allModels' => $data, 'pagination' => false]);
    }

    public function getAttemptsColumns()
    {
        return [
            [
                'class' => NumberColumn::className(),
                'attribute' => 'quantity',
                'label' => \Yii::t('common', 'Заказов'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'quantity'],
            ],
            [
                'class' => TotalColumn::className(),
                'totalColumn' => 'quantity',
                'attribute' => 'attempts',
                'label' => \Yii::t('common', 'Попыток'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions' => ['class' => 'text-success'],
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'attempts'],

            ],
            [
                'attribute' => 'ratio',
                'label' => \Yii::t('common', '%'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'ratio'],
            ]
        ];
    }

    public function getApprovedStats()
    {
        $select = [
            'group' => $this->getFieldNameByGroup(true),
            'total' => new Expression('SUM(rp.quantity)'),
            'progress' => new Expression('SUM(rp.progress)'),
            'buyout' => new Expression('SUM(rp.buyout)'),
            'nobuyout' => new Expression('SUM(rp.nobuyout)'),
        ];

        $query = (new Query())
            ->from('{{%order_log_olap_approved}} rp')
            ->groupBy('group')
            ->orderBy('group');

        $query->andWhere([
            'period_type' => $this->getPeriodType()
        ]);

        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = rp.country_id');
            $select['group'] = 'c.name';
        } elseif ($this->group == static::GROUP_PRODUCT) {
            $query->innerJoin('{{%product}} p', 'p.id = rp.product_id');
            $select['group'] = 'p.name';
        }

        $query->select($select);

        $this->addFilters($query, 'rp', true);

        $data = $query->all();

        $total = array_reduce($data, function ($carry, $row) {
            return $carry + $row['total'];
        }, 0);
        $data = array_map(function ($row) use ($total) {
            $row['progress-percent'] = round($row['progress'] / $total * 100, 2);
            $row['buyout-percent'] = round($row['buyout'] / $total * 100, 2);
            $row['nobuyout-percent'] = round($row['nobuyout'] / $total * 100, 2);
            return $row;
        }, $data);

        return new ArrayDataProvider(['allModels' => $data, 'pagination' => false]);
    }

    /**
     * @return array
     */
    public function getApprovedColumns()
    {
        $report = $this;
        return [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function($row) use ($report) {
                    return $report->formatGroupView($row);
                },
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' =>\Yii::t('common', 'Всего'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'progress',
                'label'          => \Yii::t('common', 'В процессе'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'progress'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'progress-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'progress-percent'],
            ],
            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'buyout',
                'label'          => \Yii::t('common', 'Выкуп'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'buyout'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'buyout-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'buyout-percent'],
            ],
            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'nobuyout',
                'label'          => \Yii::t('common', 'Невыкуп'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'nobuyout'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'nobuyout-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'nobuyout-percent'],
            ],
        ];
    }


    public function getRejectStats()
    {
        $select = [
            'group' => $this->getFieldNameByGroup(true),
            'total' => new Expression('SUM(rp.quantity)'),
            'expensive' => new Expression('SUM(rp.expensive)'),
            'changed_mind' => new Expression('SUM(rp.changed_mind)'),
            'medical' => new Expression('SUM(rp.medical)'),
            'no_comments' => new Expression('SUM(rp.no_comments)'),
            'auto' => new Expression('SUM(rp.auto)'),
        ];

        $query = (new Query())
            ->from('{{%order_log_olap_reject}} rp')
            ->groupBy('group')
            ->orderBy('group');

        $query->andWhere([
            'period_type' => $this->getPeriodType()
        ]);

        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = rp.country_id');
            $select['group'] = 'c.name';
        } elseif ($this->group == static::GROUP_PRODUCT) {
            $query->innerJoin('{{%product}} p', 'p.id = rp.product_id');
            $select['group'] = 'p.name';
        }

        $query->select($select);

        $this->addFilters($query, 'rp', true);

        $data = $query->all();

        $total = array_reduce($data, function ($carry, $row) {
            return $carry + $row['total'];
        }, 0);
        $data = array_map(function ($row) use ($total) {
            $row['expensive-percent'] = round($row['expensive'] / $total * 100, 2);
            $row['changed_mind-percent'] = round($row['changed_mind'] / $total * 100, 2);
            $row['medical-percent'] = round($row['medical'] / $total * 100, 2);
            $row['no_comments-percent'] = round($row['no_comments'] / $total * 100, 2);
            $row['auto-percent'] = round($row['auto'] / $total * 100, 2);
            return $row;
        }, $data);

        return new ArrayDataProvider(['allModels' => $data, 'pagination' => false]);
    }

    /**
     * @return array
     */
    public function getRejectColumns()
    {
        $report = $this;
        return [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function($row) use ($report) {
                    return $report->formatGroupView($row);
                },
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' =>\Yii::t('common', 'Всего'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'expensive',
                'label'          => Order::getRejectReasonCollection()[Order::SUB_STATUS_REJECT_REASON_EXPENSIVE],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'progress'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'expensive-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'expensive-percent'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'changed_mind',
                'label'          => Order::getRejectReasonCollection()[Order::SUB_STATUS_REJECT_REASON_CHANGED_MIND],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'changed_mind'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'changed_mind-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'changed_mind-percent'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'medical',
                'label'          => Order::getRejectReasonCollection()[Order::SUB_STATUS_REJECT_REASON_MEDICAL],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'medical'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'medical-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'medical-percent'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'no_comments',
                'label'          => Order::getRejectReasonCollection()[Order::SUB_STATUS_REJECT_REASON_NO_COMMENTS],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'no_comments'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'no_comments-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'no_comments-percent'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'auto',
                'label'          => Order::getRejectReasonCollection()[Order::SUB_STATUS_REJECT_REASON_AUTO],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'auto'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'auto-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'auto-percent'],
            ],
        ];
    }


    public function getTrashStats()
    {
        $select = [
            'group' => $this->getFieldNameByGroup(true),
            'total' => new Expression('SUM(rp.quantity)'),

            'wrong_number' => new Expression('SUM(rp.wrong_number)'),
            'double' => new Expression('SUM(rp.double)'),
            'unaware' => new Expression('SUM(rp.unaware)'),
            'consulting' => new Expression('SUM(rp.consulting)'),
            'competitor' => new Expression('SUM(rp.competitor)'),
            'return' => new Expression('SUM(rp.return)'),
            'denied' => new Expression('SUM(rp.denied)'),
        ];

        $query = (new Query())
            ->from('{{%order_log_olap_trash}} rp')
            ->groupBy('group')
            ->orderBy('group');

        $query->andWhere([
            'period_type' => $this->getPeriodType()
        ]);

        if ($this->group == static::GROUP_COUNTRY) {
            $query->innerJoin('{{%country}} c', 'c.id = rp.country_id');
            $select['group'] = 'c.name';
        } elseif ($this->group == static::GROUP_PRODUCT) {
            $query->innerJoin('{{%product}} p', 'p.id = rp.product_id');
            $select['group'] = 'p.name';
        }

        $query->select($select);

        $this->addFilters($query, 'rp', true);

        $data = $query->all();

        $total = array_reduce($data, function ($carry, $row) {
            return $carry + $row['total'];
        }, 0);
        $data = array_map(function ($row) use ($total) {
            $row['wrong_number-percent'] = round($row['wrong_number'] / $total * 100, 2);
            $row['double-percent'] = round($row['double'] / $total * 100, 2);
            $row['unaware-percent'] = round($row['unaware'] / $total * 100, 2);
            $row['consulting-percent'] = round($row['consulting'] / $total * 100, 2);
            $row['competitor-percent'] = round($row['competitor'] / $total * 100, 2);
            $row['return-percent'] = round($row['return'] / $total * 100, 2);
            $row['denied-percent'] = round($row['denied'] / $total * 100, 2);
            return $row;
        }, $data);

        return new ArrayDataProvider(['allModels' => $data, 'pagination' => false]);
    }

    /**
     * @return array
     */
    public function getTrashColumns()
    {
        $report = $this;
        return [
            [
                'attribute' => 'group',
                'label' => \Yii::t('common', 'Группировка'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'value' => function($row) use ($report) {
                    return $report->formatGroupView($row);
                },
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' =>\Yii::t('common', 'Всего'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'wrong_number',
                'label'          => Order::getTrashReasonCollection()[Order::SUB_STATUS_TRASH_REASON_WRONG_NUMBER],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'wrong_number'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'wrong_number-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'wrong_number-percent'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'double',
                'label'          => Order::getTrashReasonCollection()[Order::SUB_STATUS_TRASH_REASON_DOUBLE],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'double'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'double-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'double-percent'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'unaware',
                'label'          => Order::getTrashReasonCollection()[Order::SUB_STATUS_TRASH_REASON_UNAWARE],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'unaware'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'unaware-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'unaware-percent'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'consulting',
                'label'          => Order::getTrashReasonCollection()[Order::SUB_STATUS_TRASH_REASON_CONSULTING],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'consulting'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'consulting-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'consulting-percent'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'competitor',
                'label'          => Order::getTrashReasonCollection()[Order::SUB_STATUS_TRASH_REASON_COMPETITOR],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'competitor'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'competitor-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'competitor-percent'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'return',
                'label'          => Order::getTrashReasonCollection()[Order::SUB_STATUS_TRASH_REASON_RETURN],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'return'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'return-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'return-percent'],
            ],

            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'denied',
                'label'          => Order::getTrashReasonCollection()[Order::SUB_STATUS_TRASH_REASON_DENIED],
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'denied'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'denied-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'denied-percent'],
            ],
        ];
    }
}
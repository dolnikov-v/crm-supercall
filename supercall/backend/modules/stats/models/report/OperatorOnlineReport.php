<?php

namespace backend\modules\stats\models\report;


use backend\modules\access\models\AuthAssignment;
use common\models\Country;
use common\models\User;
use common\models\UserCountry;
use common\models\UserReady;
use common\models\UserTeam;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;

class OperatorOnlineReport extends AbstractReport
{
    public $is_export;
    public $username;
    public $online;
    public $group = self::GROUP_OPERATOR;

    /**
     * @return array
     */
    public function getGeneralAggregate()
    {
        if ($this->countries || $this->is_export || $this->username) {
            $query = UserReady::find()
                ->select([
                    'user_id' => User::tableName() . '.id',
                    'username' => User::tableName() . '.username',
                    'online' => new Expression('sum(diff::decimal)/3600')
                ])
                ->leftJoin(User::tableName(), User::tableName() . '.id=' . UserReady::tableName() . '.user_id')
                ->leftJoin(AuthAssignment::tableName(), AuthAssignment::tableName() . '.user_id=' . User::tableName() . '.id');

            $query->andWhere([
                'or',
                [
                    'and',
                    [AuthAssignment::tableName() . '.item_name' => [User::ROLE_OPERATOR, User::ROLE_SENIOR_OPERATOR]],
                    [User::tableName() . '.status' => [User::STATUS_DEFAULT, User::STATUS_APPLICANT]]
                ],
                ['is', User::tableName() . '.id', NULL]
            ]);

            $query->andWhere(['is not', User::tableName() . '.id', null]);
            $query->groupBy([User::tableName() . '.id', User::tableName() . '.username']);
            $query->orderBy(User::tableName() . '.username');

            $query = $this->applyFilter($query);
            $query = $this->applyGroup($query);

            //echo $query->createCommand()->getRawSql(); exit;

            $dataProvider = new ArrayDataProvider([
                'allModels' => $query->all(),
                'pagination' => false
            ]);
        } else {
            $dataProvider = new ArrayDataProvider([
                'allModels' => []
            ]);
        }

        return [
            'dataProvider' => $dataProvider,
        ];
    }

    public function getFieldByGroup()
    {
        $table = UserReady::tableName();
        $dateField = 'start_time';

        $group_hour = 'extract(epoch from date_trunc(\'hour\', to_timestamp(' . $table . '.' . $dateField . ') at time zone \'UTC\'))';
        $group_day = 'extract(epoch from date_trunc(\'day\', to_timestamp(' . $table . '.' . $dateField . ') at time zone \'UTC\'))';
        $group_month = 'extract(epoch from date_trunc(\'month\', to_timestamp(' . $table . '.' . $dateField . ') at time zone \'UTC\'))';

        $map = [
            static::GROUP_HOUR => $group_hour,
            static::GROUP_DAY => $group_day,
            static::GROUP_MONTH => $group_month,
            static::GROUP_COUNTRY => 'country.name',
            static::GROUP_OPERATOR => User::tableName().'.username'
        ];

        return empty($this->group) ? false : $map[$this->group];
    }

    /**
     * @param $query
     * @return mixed
     */
    public function applyGroup(Query $query)
    {
        if($this->group == self::GROUP_COUNTRY){
            $query->leftJoin(UserCountry::tableName(), UserCountry::tableName().'.user_id=' . User::tableName().'.id');
            $query->leftJoin(Country::tableName(), Country::tableName().'.id=' . UserCountry::tableName().'.country_id');
            $query->andWhere([UserCountry::tableName().'.country_id' => $this->countries]);
        }

        $group = $this->getFieldByGroup();
        $query->addSelect(['group' => $group]);
        $query->addGroupBy($group);

        return $query;
    }

    /**
     * @param Query $query
     * @return Query
     */
    public function applyFilter(Query $query)
    {
        if (empty($this->date_range) || is_null($this->date_range)) {
            $this->date_range = date('d/m/Y 00:00:00') . ' - ' . date('d/m/Y 23:59:59');
        }

        /** @var Object $date */
        $date = Yii::$app->formatter->asConvertToFrom($this->date_range);
        $from = yii::$app->formatter->asTimestampWithoutTimeZone($date->from->format('d.m.Y H:i:s'));
        $to = yii::$app->formatter->asTimestampWithoutTimeZone($date->to->format('d.m.Y H:i:s'));

        $query->andWhere(['between', UserReady::tableName() . '.start_time', $from, $to]);

        if ($this->countries) {
            $byUserCountry = UserCountry::find()->select('user_id')->where(['country_id' => $this->countries]);
            $query->andWhere([User::tableName() . '.id' => $byUserCountry]);
        }

        if ($this->username) {
            $query->andWhere([User::tableName() . '.id' => $this->username]);
        }

        if ($this->team) {
            $query->leftJoin(UserTeam::tableName(), UserTeam::tableName() . '.user_id=' . User::tableName() . '.id');
            $query->andWhere([UserTeam::tableName() . '.team_id' => $this->team]);
        }

        return $query;
    }

    /**
     * @return array
     */
    public static function getGroupsCollection()
    {
        return [
            self::GROUP_HOUR => \Yii::t('common', 'Час'),
            self::GROUP_DAY => \Yii::t('common', 'День'),
            self::GROUP_MONTH => \Yii::t('common', 'Месяц'),
            self::GROUP_COUNTRY => \Yii::t('common', 'Страна'),
            self::GROUP_OPERATOR => \Yii::t('common', 'Оператор'),
        ];
    }
}
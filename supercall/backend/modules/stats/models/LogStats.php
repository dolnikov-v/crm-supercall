<?php
namespace backend\modules\stats\models;

use common\components\grid\AverageOrderCostColumn;
use common\components\grid\NumberColumn;
use common\components\grid\RatioColumn;
use common\models\Product;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class LogStats
 * @package backend\modules\stats\models.
 */
class LogStats extends Model
{
    const GROUP_HOUR = 'hour';
    const GROUP_DAY = 'day';
    const GROUP_MONTH = 'month';
    const GROUP_USER = 'user';
    const GROUP_PRODUCT = 'product';
    
    public $date_range;
    public $date_start;
    public $date_end;
    public $group = self::GROUP_HOUR;
    public $type;
    public $country;
    public $product;
    public $partner;
    public $user;
    public $status;
    public $period;

    /**
     * @var array
     */
    public static $date_formats = [
        self::GROUP_HOUR => 'Y-m-d H:00',
        self::GROUP_DAY => 'Y-m-d',
        self::GROUP_MONTH => 'Y-m',
    ];

    /**
     * @var array
     */
    public static $date_intervals = [
        self::GROUP_HOUR => '+1hour',
        self::GROUP_DAY => '+1day',
        self::GROUP_MONTH => '+1month',
    ];



    /**
     * @return array
     */
    public static function getGroupsCollection()
    {
        return [
            self::GROUP_HOUR => \Yii::t('common', 'Час'),
            self::GROUP_DAY => \Yii::t('common', 'День'),
            self::GROUP_MONTH => \Yii::t('common', 'Месяц'),
            self::GROUP_USER => \Yii::t('common', 'Оператор'),
            self::GROUP_PRODUCT => \Yii::t('common', 'Товар'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'date_range',
                'date_start',
                'date_end',
                'group',
                'type',
                'country',
                'product',
                'partner',
                'user',
                'status',
                'period'
            ], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date_range' => \Yii::t('common', 'Дата'),
            'date_start' => \Yii::t('common', 'От'),
            'date_end' => \Yii::t('common', 'До'),
            'group' => \Yii::t('common', 'Группировка'),
            'type' => \Yii::t('common', 'Тип заказа'),
            'country' => \Yii::t('common', 'Страна'),
            'product' => \Yii::t('common', 'Товар'),
            'partner' => \Yii::t('common', 'Партнер'),
        ];
    }

    /**
     * @param int $date_start
     * @param int $date_end
     * @return ActiveQuery
     */
    public function getGeneralLogsQuery($date_start, $date_end)
    {
        // @todo Сменить на OrderLogs
        $query = OrderLog::find()
            ->joinWith(['order.country.timezone', 'order.orderProducts', 'user'])
            ->where(['field' => 'status'])
            ->andFilterWhere(['>=', '"order_log"."created_at"', $date_start])
            ->andFilterWhere(['<=', '"order_log"."created_at"', $date_end])
            ->andFilterWhere([
                '"order"."type_id"' => $this->type,
                '"order"."country_id"' => $this->country,
                '"order"."partner_id"' => $this->partner,
                '"order_log"."new"' => $this->status,
            ])
            ->orderBy(['order_log.created_at' => SORT_ASC]);
        if ($this->user){
            $query->andWhere(['or' , ['"order_log"."operator"' => $this->user], ['"user"."username"' => $this->user]]);
        }
        return $query;
    }

    /**
     * @return DataProviderInterface
     */
    public function getGeneralStats()
    {
        $date_start = new \DateTime($this->date_start . ':00');
        $date_end = new \DateTime($this->date_end . ':59');

        $logs = $this->getGeneralLogsQuery(
            $date_start->getTimestamp(),
            $date_end->getTimestamp()
        )->all();

        $result = [];

        if ($this->group == self::GROUP_USER){

            //подсчет общего количества и количества по статусам
            foreach ($logs as $log) {
                $user = ArrayHelper::getValue($log, 'operator', '-');
                if (!$user){
                    $user = ArrayHelper::getValue($log, 'user.username', '-');
                }

                if (!isset($result[$user])){
                    $result[$user] = [
                        'time' => $user,
                        'total' => 0,
                        'average-price' => 0,
                        'total-price' => 0,
                        'total-products' => 0,
                        'prices' => [],
                    ];
                    foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                        $result[$user][$status_id] = 0;
                    }
                }

                $result[$user]['total']++;
                $result[$user][$log->new]++;

                if ($log->new == Order::STATUS_APPROVED) {
                    foreach ($log->order->orderProducts as $op) {
                        if ($op->cost > 0) {
                            $result[$user]['total-price'] += $op->cost;
                            $result[$user]['total-products']++;
                        }
                    }
                }

            }

        }
        elseif ($this->group == self::GROUP_PRODUCT) {

            foreach (Product::find()->collection() as $product_id => $product_name) {
                if (!isset($result[$product_id])){
                    $result[$product_id] = [
                        'time' => $product_name,
                        'product_id' => $product_id,
                        'total' => 0,
                        'total-price' => 0,
                        'total-products' => 0,
                        'average-price' => 0,
                    ];
                }

                foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                    if (!isset($result[$product_id][$status_id])){
                        $result[$product_id][$status_id] = 0;
                        $result[$product_id][$status_id.'-ids'] = [];
                    }

                    foreach ($logs as $log) {
                        if ($log->new != $status_id){
                            continue;
                        }

                        if (!in_array($product_id, array_map(function($op){ return $op->product_id; }, $log->order->orderProducts))){
                            continue;
                        }

                        if (in_array($log->order->id, $result[$product_id][$status_id.'-ids'])){
                            continue;
                        }

                        $result[$product_id][$status_id]++;
                        $result[$product_id][$status_id.'-ids'][] = $log->order->id;
                        $result[$product_id]['total']++;
                        if ($log->new == Order::STATUS_APPROVED) {
                            foreach ($log->order->orderProducts as $op) {
                                if ($op->cost > 0) {
                                    $result[$product_id]['total-price'] += $op->cost;
                                    $result[$product_id]['total-products']++;
                                }
                            }
                        }

                    }
                }
            }

        }
        else {
            $date_formats = static::$date_formats;
            $date_intervals = static::$date_intervals;

            //заполнение пустых дат
            $date = new \DateTime($date_start->format('Y-m-d H:i:s'));
            while ($date <= $date_end) {
                $time = $date->format($date_formats[$this->group]);

                $item = [
                    'time' => $time,
                    'total' => 0,
                    'average-price' => 0,
                    'total-products' => 0,
                    'total-price' => 0,
                ];
                foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                    $item[$status_id] = 0;
                    $item[$status_id . '-percent'] = 0;
                }

                $result[$time] = $item;

                $date->modify($date_intervals[$this->group]);
            }

            //подсчет общего количества и количества по статусам
            foreach ($logs as $log) {
                $date = (new \DateTime(date('Y-m-d H:i:s', $log->created_at)))->format($date_formats[$this->group]);

                $result[$date]['total']++;
                $result[$date][$log->new]++;

                if ($log->new == Order::STATUS_APPROVED) {
                    foreach ($log->order->orderProducts as $op) {
                        if ($op->cost > 0) {
                            $result[$date]['total-price'] += $op->cost;
                            $result[$date]['total-products']++;
                        }
                    }
                }
            }
        }

        //подсчет средней цены и процентов по статусам
        foreach ($result as $key => &$item) {
            if ($item['total'] > 0) {
                if ($item['total-products'] > 0) {
                    $item['average-price'] = round(doubleval($item['total-price'] / $item['total-products']), 2);
                }
                foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                    $item[$status_id . '-percent'] = round(doubleval($item[$status_id] / $item['total'] * 100), 2);
                }
            }
            else{
                // UN-40 пустые строки удаляем из статистики
                unset($result[$key]);
            }
        }
        unset($item);
    
        // Подготовить данные для сортировки таблицы
        $statuses = [];
        $statusesPercent = [];
        foreach (Order::getStatusesCollection() as $status_id => $status_label) {
            $statuses[] = $status_id;
            $statusesPercent[] = $status_id.'-percent';
        }
    
        $attributes = ArrayHelper::merge(['time', 'total', 'average-price'], $statuses, $statusesPercent);
    
        ArrayHelper::multisort($result, 'time');
        $dataProvider = new ArrayDataProvider([
            'allModels'  => $result,
            'pagination' => false,
        ]);
    
        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getGeneralColumns()
    {
        $columns = [
            [
                'attribute' => 'time',
                'label' => $this->group == self::GROUP_USER ? \Yii::t('common', 'Оператор') : \Yii::t('common', 'Дата'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'format' => 'raw',
                'footer' => 'Всего:',
                'value' => function($row) {
                    return $row['time'] . Html::tag('span', '', [
                        'class' => 'row-group',
                        'data-group' => isset($row['product_id']) ? $row['product_id'] : $row['time']
                    ]);
                },
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => 'Всего',
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
                'contentOptions' => [
                    'class' => 'text-success extra-trigger',
                    'data-status' => 'total',
                ],
            ]
        ];
        
        $statuses = Order::getStatusesCollection();
        // убираем статус "Новый"
        unset($statuses[1]);
        
        foreach ($statuses as $status_id => $status_label) {
            $columns[] = [
                'class'          => NumberColumn::className(),
                'attribute'      => $status_id,
                'label'          => $status_label,
                'contentOptions' => [
                    'class' => 'text-success extra-trigger',
                    'data-status' => $status_id,
                ],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => $status_id],
            ];
            $columns[] = [
                'class'          => RatioColumn::className(),
                'attribute'      => $status_id . '-percent',
                'label'          => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => $status_id . '-percent'],
            ];
            $columns[] = [
                'format' => 'raw',
                'label' => '',
                'value' => '',
                'contentOptions' => ['class' => 'hidden extra-content', 'data-status' => $status_id],
                'footerOptions'  => ['class' => 'hidden'],
                'headerOptions'  => ['class' => 'hidden', 'data-field' => $status_id . '-extra'],
            ];
        }
        $columns[] = [
            'class' => AverageOrderCostColumn::className(),
            'attribute' => 'average-price',
            'label' => \Yii::t('common', 'Средний чек'),
            'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'average-price'],
        ];
        return $columns;
    }

    /**
     * @return ArrayDataProvider
     */
    public function getApprovedStats()
    {
        $date_start = new \DateTime($this->date_start . ':00');
        $date_end = new \DateTime($this->date_end . ':59');
    
        // @todo Сменить на OrderLogs
        $logs = OrderLog::find()
            ->joinWith(['order', 'order.orderProducts', 'user'])
            ->where(['field' => 'status'])
            ->andWhere(['new' => Order::STATUS_APPROVED])
            ->andFilterWhere(['>=', '"order_log"."created_at"', $date_start->getTimestamp()])
            ->andFilterWhere(['<=', '"order_log"."created_at"', $date_end->getTimestamp()])
            ->andFilterWhere([
                '"order"."type_id"' => $this->type,
                '"order"."country_id"' => $this->country,
                '"order"."partner_id"' => $this->partner,
            ])
            ->all();
        
        $result = [];
        
        if ($this->group == self::GROUP_USER) {
            
            //подсчет общего количества и количества по статусам
            foreach ($logs as $log) {
                $user = ArrayHelper::getValue($log, 'operator', '-');
                if (!$user){
                    $user = ArrayHelper::getValue($log, 'user.username', '-');
                }
                
                if ( !array_key_exists($user, $result) ) {
                    $result[ $user ] = [
                        'time'        => $user,
                        'total'       => 0,
                        'in-progress' => 0,
                        'buyout'      => 0,
                        'no-buyout'   => 0,
                    ];
                }
                
                $result[$user]['in-progress']++;
                if ($log->order->sub_status == Order::SUB_STATUS_BUYOUT){
                    $result[$user]['buyout']++;
                }
                if ($log->order->sub_status == Order::SUB_STATUS_NO_BUYOUT){
                    $result[$user]['no-buyout']++;
                }
    
                $result[$user]['total']++;
            }
            
        } elseif ($this->group == self::GROUP_PRODUCT){
    
            foreach ($logs as $log) {
                foreach ($log->order->orderProducts as $orderProduct) {
                    $product = $orderProduct->product;
                    if ( !array_key_exists($product->id, $result) ) {
                        
                        $result[ $product->id ] = [
                            'time'        => $product->name,
                            'total'       => 0,
                            'in-progress' => 0,
                            'buyout'      => 0,
                            'no-buyout'   => 0,
                        ];
                    }
                    
                    
                    $result[$product->id]['in-progress']++;
                    if ($log->order->sub_status == Order::SUB_STATUS_BUYOUT){
                        $result[$product->id]['buyout']++;
                    }
                    if ($log->order->sub_status == Order::SUB_STATUS_NO_BUYOUT){
                        $result[$product->id]['no-buyout']++;
                    }
            
                    $result[$product->id]['total']++;
            
                }
            }
        }
        else {
            
            $date_formats = [
                self::GROUP_HOUR => 'Y-m-d H:00',
                self::GROUP_DAY => 'Y-m-d',
                self::GROUP_MONTH => 'Y-m',
            ];
            
            $date_intervals = [
                self::GROUP_HOUR => '+1hour',
                self::GROUP_DAY => '+1day',
                self::GROUP_MONTH => '+1month',
            ];
    
            //заполнение пустых дат
            $date = new \DateTime($date_start->format('Y-m-d H:i:s'));
            while ($date <= $date_end) {
                $time = $date->format($date_formats[$this->group]);
        
                $item = [
                    'time' => $time,
                    'total'       => 0,
                    'in-progress' => 0,
                    'buyout'      => 0,
                    'no-buyout'   => 0,
                ];
                $result[$time] = $item;
                $date->modify($date_intervals[$this->group]);
            }
    
            //подсчет общего количества и количества по статусам
            foreach ($logs as $log) {
                $date = (new \DateTime(date('Y-m-d H:i:s', $log->created_at)))->format($date_formats[$this->group]);
    
                $result[$date]['total']++;
    
                if ($log->order->status == Order::STATUS_APPROVED){
                    $result[$date]['in-progress']++;
                    if ($log->order->sub_status == Order::SUB_STATUS_BUYOUT){
                        $result[$date]['buyout']++;
                    }
                    if ($log->order->sub_status == Order::SUB_STATUS_NO_BUYOUT){
                        $result[$date]['no-buyout']++;
                    }
                }
            }
            
        }
        
        //подсчет процентов
        foreach ($result as $key => &$item) {
            if ($item['total'] > 0){
                foreach (['in-progress', 'buyout', 'no-buyout'] as $column_id) {
                    if (!isset($item[$column_id])){
                        $item[$column_id] = 0;
                    }
                    $item[$column_id.'-percent'] = round(doubleval($item[$column_id] / $item['total'] * 100), 2);
                }
            }
            else{
                // UN-40 пустые строки удаляем из статистики
                unset($result[$key]);
            }
        }
        unset($item);
        
        ArrayHelper::multisort($result, 'time');
        return new ArrayDataProvider(['allModels' => $result, 'pagination' => false]);
    }
    
    /**
     * @return array
     */
    public function getApprovedColumns()
    {
        return [
            [
                'attribute' => 'time',
                'label' => \Yii::t('common', 'Дата'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' =>\Yii::t('common', 'Всего'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'in-progress',
                'label'          => \Yii::t('common', 'В процессе'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'in-progress'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'in-progress-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'in-progress-percent'],
            ],
            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'buyout',
                'label'          => \Yii::t('common', 'Выкуп'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'buyout'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'buyout-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'buyout-percent'],
            ],
            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'no-buyout',
                'label'          => \Yii::t('common', 'Невыкуп'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'no-buyout'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'no-buyout-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'no-buyout-percent'],
            ],
        ];
    }
    
    /**
     * Данные для Статистики отказов
     *
     * @return ArrayDataProvider
     */
    public function getRejectStats()
    {
        $date_start = new \DateTime($this->date_start . ':00');
        $date_end = new \DateTime($this->date_end . ':59');
    
        // @todo Сменить на OrderLogs
        $logs = OrderLog::find()
            ->joinWith(['order', 'order.orderProducts', 'user'])
            ->where(['field' => 'status'])
            ->andWhere(['new' => Order::STATUS_REJECTED])
            ->andFilterWhere(['>=', '"order_log"."created_at"', $date_start->getTimestamp()])
            ->andFilterWhere(['<=', '"order_log"."created_at"', $date_end->getTimestamp()])
            ->andFilterWhere([
                '"order"."type_id"' => $this->type,
                '"order"."country_id"' => $this->country,
                '"order"."partner_id"' => $this->partner,
            ])
            ->all();

        $result = [];
    
        if ($this->group == self::GROUP_USER) {
        
            //подсчет общего количества и количества по статусам
            foreach ($logs as $log) {
                $user = ArrayHelper::getValue($log, 'operator', '-');
                if (!$user){
                    $user = ArrayHelper::getValue($log, 'user.username', '-');
                }
            
                if ( !array_key_exists($user, $result) ) {
                    $result[ $user ] = [
                        'time'        => $user,
                        'total'         => 0,
                        'average-price' => 0,
                        'total-price'   => 0,
                    ];
                    foreach (Order::getRejectReasonCollection() as $i => $text) {
                        $result[ $user ]['reject-reason-'.$i] = 0;
                        $result[ $user ]['reject-reason-'.$i.'-percent'] = 0;
                    }
                }
            
                if ( array_key_exists('reject-reason-'.$log->order->sub_status, $result[ $user ]) ) {
                    $result[ $user ]['reject-reason-'.$log->order->sub_status]++;
                }
            
                $result[ $user ]['total']++;
            }
        
        } elseif ($this->group == self::GROUP_PRODUCT){
    
            foreach ($logs as $log) {
                foreach ($log->order->orderProducts as $orderProduct) {
                    $product = $orderProduct->product;
                    if ( !array_key_exists($product->id, $result) ) {
                        $result[ $product->id ] = [
                            'time'        => $product->name,
                            'total' => 0,
                            'average-price' => 0,
                            'total-price' => 0,
                        ];
                        foreach (Order::getRejectReasonCollection() as $i => $text) {
                            $result[ $product->id ]['reject-reason-'.$i] = 0;
                            $result[ $product->id ]['reject-reason-'.$i.'-percent'] = 0;
                        }
                    }
                    if ( array_key_exists('reject-reason-'.$log->order->sub_status, $result[ $product->id ]) ) {
                        $result[$product->id]['reject-reason-'.$log->order->sub_status]++;
                    }
                    $result[$product->id]['total']++;
            
                }
            }
        }
        else {
            
            $date_formats = [
                self::GROUP_HOUR => 'Y-m-d H:00',
                self::GROUP_DAY => 'Y-m-d',
                self::GROUP_MONTH => 'Y-m',
            ];
            
            $date_intervals = [
                self::GROUP_HOUR => '+1hour',
                self::GROUP_DAY => '+1day',
                self::GROUP_MONTH => '+1month',
            ];
            
            //заполнение пустых дат
            $date = new \DateTime($date_start->format('Y-m-d H:i:s'));
            while ($date <= $date_end) {
                $time = $date->format($date_formats[$this->group]);
                
                $item = [
                    'time' => $time,
                    'total' => 0,
                    'average-price' => 0,
                    'total-price' => 0,
                ];
                foreach (Order::getRejectReasonCollection() as $i => $text) {
                    $item['reject-reason-'.$i] = 0;
                    $item['reject-reason-'.$i.'-percent'] = 0;
                }
                $result[$time] = $item;
                $date->modify($date_intervals[$this->group]);
            }
    
            //подсчет общего количества и количества по статусам
            foreach ($logs as $log) {
                $date = (new \DateTime(date('Y-m-d H:i:s', $log->created_at)))->format($date_formats[$this->group]);
                $result[$date]['total']++;
                if (array_key_exists('reject-reason-'.$log->order->sub_status, $result[$date])){
                    $result[$date]['reject-reason-'.$log->order->sub_status]++;
                }
            }
        }
        
        //подсчет процентов
        foreach ($result as $key => &$item) {
            if ($item['total'] > 0){
                foreach (Order::getRejectReasonCollection() as $i => $text) {
                    $item['reject-reason-'.$i.'-percent'] = round(doubleval($item['reject-reason-'.$i] / $item['total'] * 100), 2);
                }
            }
            else{
                // UN-40 пустые строки удаляем из статистики
                unset($result[$key]);
            }
        }
        unset($item);
        
        ArrayHelper::multisort($result, 'time');
        return new ArrayDataProvider(['allModels' => $result, 'pagination' => false]);
    }
    
    /**
     * Колонки для Статистики отказов
     * @return array
     */
    public function getRejectColumns()
    {
        $columns = [
            [
                'attribute' => 'time',
                'label' => \Yii::t('common', 'Дата'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'time'],
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' =>\Yii::t('common', 'Всего'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
        ];
        foreach (Order::getRejectReasonCollection() as $i => $text) {
            $columns[] = [
                'class'          => NumberColumn::className(),
                'attribute'      => 'reject-reason-' . $i,
                'label'          => $text,
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'reject-reason-' . $i],
            ];
            $columns[] = [
                'class'          => RatioColumn::className(),
                'attribute'      => 'reject-reason-' . $i . '-percent',
                'label'          => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'reject-reason-' . $i . '-percent'],
            ];
        }
        return $columns;
    }
    
    /**
     * Статистика по трешам
     * @return ArrayDataProvider
     */
    public function getTrashStats()
    {
        $date_start = new \DateTime($this->date_start . ':00');
        $date_end = new \DateTime($this->date_end . ':59');
    
        // @todo Сменить на OrderLogs
        $logs = OrderLog::find()
            ->joinWith(['order', 'order.orderProducts', 'user'])
            ->where(['field' => 'status'])
            ->andWhere(['new' => Order::STATUS_TRASH])
            ->andFilterWhere(['>=', '"order_log"."created_at"', $date_start->getTimestamp()])
            ->andFilterWhere(['<=', '"order_log"."created_at"', $date_end->getTimestamp()])
            ->andFilterWhere([
                '"order"."type_id"' => $this->type,
                '"order"."country_id"' => $this->country,
                '"order"."partner_id"' => $this->partner,
            ])
            ->all();
        
        $result = [];
    
        if ($this->group == self::GROUP_USER) {
        
            //подсчет общего количества и количества по статусам
            foreach ($logs as $log) {
                $user = ArrayHelper::getValue($log, 'operator', '-');
                if (!$user){
                    $user = ArrayHelper::getValue($log, 'user.username', '-');
                }
            
                if ( !array_key_exists($user, $result) ) {
                    $result[ $user ] = [
                        'time'        => $user,
                        'total'         => 0,
                        'average-price' => 0,
                        'total-price'   => 0,
                    ];
                    foreach (Order::getTrashReasonCollection() as $i => $text) {
                        $result[ $user ]['trash-reason-'.$i] = 0;
                        $result[ $user ]['trash-reason-'.$i.'-percent'] = 0;
                    }
                }
    
                if ( array_key_exists('trash-reason-'.$log->order->sub_status, $result[ $user ]) ) {
                    $result[ $user ]['trash-reason-'.$log->order->sub_status]++;
                }
            
                $result[ $user ]['total']++;
            }
        
        } elseif ($this->group == self::GROUP_PRODUCT){
            foreach ($logs as $log) {
                foreach ($log->order->orderProducts as $orderProduct) {
                    $product = $orderProduct->product;
                    if ( !array_key_exists($product->id, $result) ) {
                        $result[ $product->id ] = [
                            'time'          => $product->name,
                            'total'         => 0,
                            'average-price' => 0,
                            'total-price'   => 0,
                        ];
    
                        foreach (Order::getTrashReasonCollection() as $i => $text) {
                            $result[ $product->id ]['trash-reason-'.$i] = 0;
                            $result[ $product->id ]['trash-reason-'.$i.'-percent'] = 0;
                        }
                    }
    
                    if ( array_key_exists('trash-reason-'.$log->order->sub_status, $result[ $product->id ]) ) {
                        $result[$product->id]['trash-reason-'.$log->order->sub_status]++;
                    }
                    $result[$product->id]['total']++;
                }
            }
        }
        else {
            
            $date_formats = [
                self::GROUP_HOUR => 'Y-m-d H:00',
                self::GROUP_DAY => 'Y-m-d',
                self::GROUP_MONTH => 'Y-m',
            ];
            
            $date_intervals = [
                self::GROUP_HOUR => '+1hour',
                self::GROUP_DAY => '+1day',
                self::GROUP_MONTH => '+1month',
            ];
            
            //заполнение пустых дат
            $date = new \DateTime($date_start->format('Y-m-d H:i:s'));
            while ($date <= $date_end) {
                $time = $date->format($date_formats[$this->group]);
                
                $item = [
                    'time' => $time,
                    'total' => 0,
                    'average-price' => 0,
                    'total-price' => 0,
                ];
                foreach (Order::getTrashReasonCollection() as $i => $text) {
                    $item['trash-reason-'.$i] = 0;
                    $item['trash-reason-'.$i.'-percent'] = 0;
                }
                $result[$time] = $item;
                $date->modify($date_intervals[$this->group]);
            }
            
            //подсчет общего количества и количества по статусам
            foreach ($logs as $log) {
                $date = (new \DateTime(date('Y-m-d H:i:s', $log->created_at)))->format($date_formats[$this->group]);
                $result[$date]['total']++;
                if (array_key_exists('trash-reason-'.$log->order->sub_status, $result[$date])){
                    $result[$date]['trash-reason-'.$log->order->sub_status]++;
                }
            }
        }
        
        //подсчет процентов
        foreach ($result as $key => &$item) {
            if ($item['total'] > 0){
                foreach (Order::getTrashReasonCollection() as $i => $text) {
                    $item['trash-reason-'.$i.'-percent'] = round(doubleval($item['trash-reason-'.$i] / $item['total'] * 100), 2);
                }
            }
            else{
                // UN-40 пустые строки удаляем из статистики
                unset($result[$key]);
            }
        }
        unset($item);
        
        ArrayHelper::multisort($result, 'time');
        return new ArrayDataProvider(['allModels' => $result, 'pagination' => false]);
    }
    
    /**
     * Колонки для Статистики трешей
     * @return array
     */
    public function getTrashColumns()
    {
        $columns = [
            [
                'attribute' => 'time',
                'label' => \Yii::t('common', 'Дата'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' =>\Yii::t('common', 'Всего'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
        ];
        foreach (Order::getTrashReasonCollection() as $i => $text) {
            $columns[] = [
                'class'          => NumberColumn::className(),
                'attribute'      => 'trash-reason-' . $i,
                'label'          => $text,
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'trash-reason-' . $i],
            ];
            $columns[] = [
                'class'          => RatioColumn::className(),
                'attribute'      => 'trash-reason-' . $i . '-percent',
                'label'          => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'trash-reason-' . $i . '-percent'],
            ];
        }
        return $columns;
    }


    /**
     * Возвращает расшифровку обработки заказов для основной статистики
     * @return ArrayDataProvider
     */
    public function getDetailsProviderForGeneralStats()
    {
        $result = [];
        if ($this->user == '-') {
            $this->user = null;
        }

        if ($this->status == 'total') {
            $this->status = null;
        }

        if (!empty($this->period)) {
            //если группировка по времени, необходимо выстроить интервал для этого времени
            $period = new \DateTime(date(static::$date_formats[$this->group], strtotime($this->period)));
            $date_start = $period->getTimestamp();
            $period->modify(static::$date_intervals[$this->group]);
            $date_end = $period->getTimestamp();
        } else {
            $date_start = new \DateTime($this->date_start . ':00');
            $date_end = new \DateTime($this->date_end . ':59');
            $date_start = $date_start->getTimestamp();
            $date_end = $date_end->getTimestamp();
        }

        $logs = $this->getGeneralLogsQuery($date_start, $date_end)
            ->joinWith(['order', 'order.orderProducts', 'order.orderProducts.product'])
            ->all();
        $usedOrders = [];
        foreach ($logs as $log) {
            if (!empty($this->product)) {
                if (!in_array(
                    $this->product,
                    array_map(function($op){ return $op->product_id; }, $log->order->orderProducts)
                )) {
                    continue;
                }

                if (!in_array($log->order->id, $usedOrders)) {
                    $usedOrders[] = $log->order->id;
                } else {
                    continue;
                }
            }
            $result[] = LogListStats::formatLog($log);
        }

        unset($usedOrders);
        return new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }
    
    /**
     * Функция вернет массив стран для фильтра статы
     * По умолчанию будет возвращена страна - первая из массива доступных стран
     * @return array
     */
    public function getCountry()
    {
        if (!$this->country) {
            $countries = ArrayHelper::getColumn( \Yii::$app->user->countries, 'id' );
            // По дефолту вернем только данные первой страны в массиве
            // во избежание долгого расчета
            $this->country = $countries[0];
        }
        
        return (int) $this->country;
    }
}
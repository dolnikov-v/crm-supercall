<?php

namespace backend\modules\stats\models;

use backend\modules\stats\models\filter\OrderApproved;
use backend\modules\stats\models\filter\OrderGeneral;
use backend\modules\stats\models\filter\OrderReject;
use backend\modules\stats\models\filter\OrderTrash;
use common\components\grid\AverageOrderCostColumn;
use common\components\grid\NumberColumn;
use common\components\grid\NumberGenesysColumn;
use common\components\grid\RatioColumn;
use common\components\grid\TotalColumn;
use common\models\Country;
use common\modules\order\models\Order;
use yii\base\ErrorException;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\data\DataProviderInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Class OrderStats
 * @package backend\modules\stats\models.
 */
class OrderStats extends Model
{
    const GROUP_HOUR = 'hour';
    const GROUP_DAY = 'day';
    const GROUP_MONTH = 'month';
    const GROUP_PRODUCT = 'product';
    const GROUP_COUNTRY = 'country';
    
    public $date_range;
    public $date_start;
    public $date_end;
    public $group = self::GROUP_DAY;
    public $type;
    public $country;
    public $countries;
    public $product;
    public $partner;
    public $type_date;

    
    /**
     * @return array
     */
    public static function getGroupsCollection()
    {
        return [
            self::GROUP_HOUR => \Yii::t('common', 'Час'),
            self::GROUP_DAY => \Yii::t('common', 'День'),
            self::GROUP_MONTH => \Yii::t('common', 'Месяц'),
//            self::GROUP_PRODUCT => \Yii::t('common', 'Товар'),
            self::GROUP_COUNTRY => \Yii::t('common', 'Страна'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_range', 'date_start', 'date_end', 'group', 'type', 'country', 'countries', 'product', 'partner', 'type_date'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'date_range' => \Yii::t('common', 'Дата'),
            'date_start' => \Yii::t('common', 'От'),
            'date_end'   => \Yii::t('common', 'До'),
            'group'      => \Yii::t('common', 'Группировка'),
            'type'       => \Yii::t('common', 'Тип заказа'),
            'country'    => \Yii::t('common', 'Страна'),
            'countries'  => \Yii::t('common', 'Страна'),
            'product'    => \Yii::t('common', 'Товар'),
            'partner'    => \Yii::t('common', 'Партнер'),
        ];
    }

    /**
     * FIXME: удалить после успешных тестов OLAP
     * Статистика по заказам
     * @return DataProviderInterface
     */
    public function getGeneralStats()
    {
        $result = OrderGeneral::find($this->attributes);
        //подсчет средней цены и процентов по статусам
        foreach ($result as &$item) {
            if ($item['total'] > 0){
                $item['average-price'] = round(doubleval($item['total-price'] / $item['total']), 2);
                foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                    if (!isset($item[$status_id])){
                        $item[$status_id] = 0;
                    }
                    $item[$status_id.'-percent'] = round(doubleval($item[$status_id] / $item['total'] * 100), 2);
                }
            }
            else{
                $item['average-price'] = 0;
                foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                    if (!isset($item[$status_id])){
                        $item[$status_id] = 0;
                    }
                    $item[$status_id.'-percent'] = 0;
                }
            }
        }
        unset($item);

        ArrayHelper::multisort($result, 'time');
        return new ArrayDataProvider(['allModels' => $result, 'pagination' => false]);
    }

    /**
     * @return array
     */
    public function getGeneralColumns($group = null)
    {
        $columns = [
            [
                'attribute' => 'time',
                'label' => \Yii::t('common', 'Дата'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' => 'Всего',
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
            ]
        ];
        foreach (Order::getStatusesCollection() as $status_id => $status_label) {
            $columns[] = [
                'class'          => NumberColumn::className(),
                'attribute'      => $status_id,
                'label'          => $status_label,
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => $status_id],
            ];
            $columns[] = [
                'class'          => RatioColumn::className(),
                'attribute'      => $status_id . '-percent',
                'label'          => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => $status_id . '-percent'],
            ];
        }
        $columns[] = [
            // Если группировка по стране, то средн.чек вместе с конвертацией д.б.
            'class' => $group == self::GROUP_COUNTRY
                ? AverageCurrencyColumn::className()
                : AverageOrderCostColumn::className(),
            'attribute' => 'average-price',
            'label' => \Yii::t('common', 'Средний чек'),
            'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'average-price'],
        ];
        return $columns;
    }

    /**
     * @return DataProviderInterface
     */
    public function getAttemptStats()
    {
        $date_start = new \DateTime($this->date_start . ' 00:00:00');
        $date_end = new \DateTime($this->date_end . ' 23:59:59');

        $data = Order::find()
            ->asArray(true)
            ->select(['attempts.count as attempts', 'count("order"."id") as orders'])
            ->filterWhere(['>=', '"order"."created_at"', $date_start->getTimestamp()])
            ->andFilterWhere(['<=', '"order"."created_at"', $date_end->getTimestamp()])
            ->andFilterWhere([
                'type_id' => $this->type,
                'country_id' => $this->country,
                'partner_id' => $this->partner,
            ])
            ->leftJoin(
                '(select order_id, count(order_id) as count from order_log where field = \'status\' group by order_id) as attempts',
                'attempts.order_id = "order"."id"'
            )
            ->byStatus(Order::STATUS_APPROVED)
            ->groupBy(['attempts.count'])
            ->orderBy(['attempts.count' => SORT_ASC])
            ->having('attempts.count > 0')
            ->all();

        $result = [];

        $i = 0;
        $total_orders = array_reduce($data, function($carry, $row){ return $carry + $row['orders']; }, 0);
        foreach ($data as $row) {
            while ($i < $row['attempts']){
                $i++;
            }
            $result[] = [
                'attempts' => $i,
                'orders' => $row['orders'],
                'percent' => round(doubleval($row['orders'] / $total_orders) * 100, 2)
            ];
            $i++;
        }

        return new ArrayDataProvider(['allModels' => $result, 'pagination' => false]);
    }

    /**
     * @return array
     */
    public function getAttemptColumns()
    {
        return [
            [
                'class' => NumberColumn::className(),
                'attribute' => 'orders',
                'label' => \Yii::t('common', 'Заказов'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'orders'],
            ],
            [
                'class'          => TotalColumn::className(),
                'attribute'      => 'attempts',
                'label'          => \Yii::t('common', 'Попыток'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'attempts'],

            ],
            [
                'attribute' => 'percent',
                'label' => \Yii::t('common', '%'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'percent'],
            ]
        ];
    }

    /**
     * @return DataProviderInterface
     */
    public function getRejectStats()
    {
        $result = OrderReject::find($this->attributes, Order::STATUS_REJECTED);

        //подсчет процентов
        foreach ($result as $key => &$item) {
            if ($item['total'] > 0){
                foreach (Order::getRejectReasonCollection() as $i => $text) {
                    $item['reject-reason-'.$i.'-percent'] = round(doubleval($item['reject-reason-'.$i] / $item['total'] * 100), 2);
                }
            }
            else{
                // UN-40 пустые строки удаляем из статистики
                unset($result[$key]);
            }
        }
        unset($item);

        ArrayHelper::multisort($result, 'time');
        return new ArrayDataProvider(['allModels' => $result, 'pagination' => false]);
    }

    /**
     * @return array
     */
    public function getRejectColumns()
    {
        $columns = [
            [
                'attribute' => 'time',
                'label' => \Yii::t('common', 'Дата'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'time'],
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' =>\Yii::t('common', 'Всего'),
                'headerOptions' => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
        ];
        foreach (Order::getRejectReasonCollection() as $i => $text) {
            $columns[] = [
                'class'          => NumberColumn::className(),
                'attribute'      => 'reject-reason-' . $i,
                'label'          => $text,
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'reject-reason-' . $i],
            ];
            $columns[] = [
                'class'          => RatioColumn::className(),
                'attribute'      => 'reject-reason-' . $i . '-percent',
                'label'          => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'reject-reason-' . $i . '-percent'],
            ];
        }
        return $columns;
    }

    /**
     * @return DataProviderInterface
     */
    public function getTrashStats()
    {
        $result = OrderTrash::find($this->attributes, Order::STATUS_TRASH);

        //подсчет процентов
        foreach ($result as $key => &$item) {
            if ($item['total'] > 0){
                foreach (Order::getTrashReasonCollection() as $i => $text) {
                    $item['trash-reason-'.$i.'-percent'] = round(doubleval($item['trash-reason-'.$i] / $item['total'] * 100), 2);
                }
            }
            else{
                // UN-40 пустые строки удаляем из статистики
                unset($result[$key]);
            }
        }
        unset($item);

        ArrayHelper::multisort($result, 'time');
        return new ArrayDataProvider(['allModels' => $result, 'pagination' => false]);
    }

    /**
     * @return array
     */
    public function getTrashColumns()
    {
        $columns = [
            [
                'attribute' => 'time',
                'label' => \Yii::t('common', 'Дата'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' =>\Yii::t('common', 'Всего'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
        ];
        foreach (Order::getTrashReasonCollection() as $i => $text) {
            $columns[] = [
                'class'          => NumberColumn::className(),
                'attribute'      => 'trash-reason-' . $i,
                'label'          => $text,
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'trash-reason-' . $i],
            ];
            $columns[] = [
                'class'          => RatioColumn::className(),
                'attribute'      => 'trash-reason-' . $i . '-percent',
                'label'          => '%',
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'trash-reason-' . $i . '-percent'],
            ];
        }
        return $columns;
    }
    
    public function getApprovedStats()
    {
        $result = OrderApproved::find($this->attributes, Order::STATUS_APPROVED);


        //подсчет процентов
        foreach ($result as $key => &$item) {
            if ($item['total'] > 0){
                foreach (['in-progress', 'buyout', 'no-buyout'] as $column_id) {
                    if (!isset($item[$column_id])){
                        $item[$column_id] = 0;
                    }
                    $item[$column_id.'-percent'] = round(doubleval($item[$column_id] / $item['total'] * 100), 2);
                }
            }
            else{
                // UN-40 пустые строки удаляем из статистики
                unset($result[$key]);
            }
        }
        unset($item);
        
        ArrayHelper::multisort($result, 'time');
        return new ArrayDataProvider(['allModels' => $result, 'pagination' => false]);
    }
    
    /**
     * @return array
     */
    public function getApprovedColumns()
    {
        return [
            [
                'attribute' => 'time',
                'label' => \Yii::t('common', 'Дата'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'time'],
                'footer' => 'Всего:',
            ],
            [
                'class' => NumberColumn::className(),
                'attribute' => 'total',
                'label' =>\Yii::t('common', 'Всего'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'total'],
            ],
            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'in-progress',
                'label'          => \Yii::t('common', 'В процессе'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'in-progress'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'in-progress-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'in-progress-percent'],
            ],
            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'buyout',
                'label'          => \Yii::t('common', 'Выкуп'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'buyout'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'buyout-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'buyout-percent'],
            ],
            [
                'class'          => NumberColumn::className(),
                'attribute'      => 'no-buyout',
                'label'          => \Yii::t('common', 'Невыкуп'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'no-buyout'],
            ],
            [
                'class'          => RatioColumn::className(),
                'attribute'      => 'no-buyout-percent',
                'label'          => \Yii::t('common', '%'),
                'contentOptions' => ['class' => 'text-danger'],
                'footerOptions'  => ['class' => 'text-danger'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'no-buyout-percent'],
            ],
        ];
    }
    
    
    /**
     * UN-54 Статистика обработки заказов в генезисе
     *
     * строится по таблице колинг листов
     * привязка к стране, таблица вида "CALLINGLIST_FR"
     *
     * @return ArrayDataProvider
     * @throws ErrorException
     */
    public function getGenesysStats()
    {
        $date_start = new \DateTime($this->date_start . ':00');
        $date_end = new \DateTime($this->date_end . ':59');
    
        $result = [];
        try {
            // timestamp UTC
            $timestamp = (new \DateTime('now', new \DateTimeZone('UTC')))->getTimestamp();
            // вытащим char_code для всех стран
            $countryCharCode = Country::find()->select(['char_code'])->where(['in', 'id', $this->getCountries()]);
            $prepare = [];

            // Для каждой страны отд.таблица
            foreach ($countryCharCode->column() as $charCode) {
                $table = 'CALLINGLIST_' . $charCode;

                // только если таблица в ORACLE существует
                if (\Yii::$app->genesys->db->getTableSchema($table, true) !== null) {
                    // Также отфильтруем заказы с фин.статусом 34,35,36,37
                    $data = \Yii::$app->genesys->db->createCommand('SELECT ATTEMPT AS attempts, 
                        COUNT(RECORD_ID) AS orders FROM ' .$table. ' 
                        WHERE CRM_CREATE>=:timefrom AND CRM_CREATE<=:timeto 
                        AND CALL_RESULT NOT IN (34,35,36,37)
                        GROUP BY ATTEMPT ORDER BY ATTEMPT ASC')
                        ->bindValues([
                            ':timefrom' => $date_start->getTimestamp(),
                            ':timeto'   => $date_end->getTimestamp(),
                        ])
                        ->queryAll();

                    // Если в таблице есть записи
                    if (count($data) > 0) {
                        $data_available = \Yii::$app->genesys->db->createCommand('SELECT ATTEMPT AS attempts, 
                            COUNT(RECORD_ID) AS orders FROM ' .$table. ' 
                            WHERE CRM_CREATE>=:timefrom AND CRM_CREATE<=:timeto 
                            AND CALL_RESULT NOT IN (34,35,36,37)
                            AND DIAL_SCHED_TIME <= ' .$timestamp. '
                            GROUP BY ATTEMPT ORDER BY ATTEMPT ASC')
                            ->bindValues([
                                ':timefrom' => $date_start->getTimestamp(),
                                ':timeto'   => $date_end->getTimestamp(),
                            ])
                            ->queryAll();

                        // смаппим, чтобы попытки были ключами, а кол-во заказов - значением
                        $calls_array = ArrayHelper::map($data_available, 'ATTEMPTS', 'ORDERS');

                        $i = 0;
                        $total_orders = array_reduce($data, function($carry, $row){ return $carry + $row['ORDERS']; }, 0);
                        foreach ($data as $key => $row) {
                            while ($i < $row['ATTEMPTS']) {
                                $i++;
                            }
                            // кол-во доступных заказов
                            $orders_available = key_exists($i, $calls_array) ? $calls_array[$i] : '0';
                            $prepare[$i]['orders_available'][$table] = $orders_available;
                            $prepare[$i]['orders'][$table] = $row['ORDERS'];
                            $prepare[$i]['total_orders'][$table] = $total_orders;

//                            $result[] = [
//                                'attempts'         => $i,
//                                'orders_available' => $orders_available,
//                                'orders'           => $row['ORDERS'] . ' (' .$orders_available. ')',
//                                'percent'          => round(doubleval($row['ORDERS'] / $total_orders) * 100, 2),
//                            ];
                            $i++;
                        }
                    }
                }
            }

            // подготовить данные для вывода
            foreach ($prepare as $key => $value) {
                $sumOrdersAvailable = array_sum($value['orders_available']);
                $sumOrders = array_sum($value['orders']);
                $sumTotalOrders = array_sum($value['total_orders']);
                $result[] = [
                    'attempts'         => $key,
                    'orders_available' => $sumOrdersAvailable,
                    'orders'           => $sumOrders . ' (' .$sumOrdersAvailable. ')',
                    'percent'          => round(doubleval($sumOrders / $sumTotalOrders) * 100, 2),
                ];
            }

        }
        catch (ErrorException $Exception) {
            \Yii::error('Ошибка выборки данных из ORACLE \n\r' .VarDumper::dumpAsString($Exception));
            $result = [];
        }
        finally {
            return new ArrayDataProvider(['allModels' => $result, 'pagination' => false]);
        }
    }
    
    /**
     * @return array
     */
    public function getGenesysColumns()
    {
        return [
            [
                'class' => NumberGenesysColumn::className(),
                'attribute' => 'orders',
                'label' => \Yii::t('common', 'Заказов'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'orders'],
            ],
            [
                'class'          => TotalColumn::className(),
                'totalColumn'    => 'orders',
                'attribute'      => 'attempts',
                'label'          => \Yii::t('common', 'Попыток'),
                'contentOptions' => ['class' => 'text-success'],
                'footerOptions'  => ['class' => 'text-success'],
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'attempts'],
            
            ],
            [
                'attribute' => 'percent',
                'label' => \Yii::t('common', '%'),
                'headerOptions'  => ['data-sortable' => 'true', 'data-field' => 'percent'],
            ]
        ];
    }

    /**
     * Функция вернет массив стран для фильтра статы
     * По умолчанию фильтр пустой, потому будут возвращены все доступные Пользователю страны.
     *
     * @return array
     */
    public function getCountries()
    {
        if (!$this->countries) {
            $this->countries = ArrayHelper::getColumn( \Yii::$app->user->countries, 'id' );
        }

        return $this->countries;
    }
}
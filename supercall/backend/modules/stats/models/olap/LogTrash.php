<?php

namespace backend\modules\stats\models\olap;


use common\models\Country;
use common\models\Product;
use common\modules\order\models\Order;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%order_log_olap_trash}}".
 *
 * @property integer $period_type
 * @property integer $period
 * @property integer $product_id
 * @property integer $type_id
 * @property integer $partner_id
 * @property integer $country_id
 *
 * @property integer $wrong_number
 * @property integer $double
 * @property integer $unaware
 * @property integer $consulting
 * @property integer $competitor
 * @property integer $return
 * @property integer $denied
 * @property integer $quantity
 *
 * @property \common\modules\partner\models\Partner $partner
 * @property \common\models\Product $product
 * @property \common\models\Country $country
 * @property \common\modules\order\models\OrderType $type
 */
class LogTrash extends AbstractOlap
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_log_olap_trash}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period_type', 'period', 'partner_id', 'country_id', 'type_id', 'wrong_number', 'double', 'unaware', 'consulting', 'competitor', 'return', 'denied'], 'required'],
            [['operator'], 'safe'],
            [['period_type', 'product_id', 'partner_id', 'country_id', 'period', 'type_id', 'wrong_number', 'double', 'unaware', 'consulting', 'competitor', 'return', 'denied', 'quantity'], 'integer'],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partner::className(), 'targetAttribute' => ['partner_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'period_type' => 'Period Type',
            'period' => 'Period',
            'product_id' => 'Product',
            'partner_id' => 'Partner',
            'country_id' => 'Country',
            'attempts' => 'Attempts',
            'quantity' => 'Quantity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(OrderType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * Добавляет данные заказа в куб
     * @param $data
     * @return bool
     */
    public static function incrementWithHierarchy($data)
    {
        $child = new static();
        if (!$child->load([$child->formName() => $data]) || !$child->validate()) {
            return false;
        }

        $pkData = $child->getPrimaryKey(true);
        $hierarchy = static::getHierarchy('period_type');

        unset($pkData['period']);
        unset($pkData['period_type']);
        $pkData['operator'] = (string)$child->operator;
        $query = static::find()->where($pkData);
        $periods = [];
        foreach ($hierarchy as $periodType) {
            $periods[$periodType] = [
                'period_type' => $periodType,
                'period' => static::normalizePeriod($child->period, $periodType)
            ];
        }
        $query->andWhere(ArrayHelper::merge(['or'], $periods));
        $models = ArrayHelper::index($query->all(), 'period_type');

        foreach ($hierarchy as $periodType) {
            /** @var static $model */
            if (isset($models[$periodType])) {
                $model = $models[$periodType];
            }
            else {
                $model = new static();
                $modelData = $pkData;
                $modelData['period_type'] = $periodType;
                $modelData['period'] = $periods[$periodType]['period'];
                $modelData['quantity'] = 0;

                $modelData['wrong_number'] = 0;
                $modelData['double'] = 0;
                $modelData['unaware'] = 0;
                $modelData['consulting'] = 0;
                $modelData['competitor'] = 0;
                $modelData['return'] = 0;
                $modelData['denied'] = 0;

                $model->load([$child->formName() => $modelData]);
            }

            $model->quantity = (int)$model->quantity + 1;

            $model->wrong_number += $data['wrong_number'] ? 1 : 0;
            $model->double += $data['double'] ? 1 : 0;
            $model->unaware += $data['unaware'] ? 1 : 0;
            $model->consulting += $data['consulting'] ? 1 : 0;
            $model->competitor += $data['competitor'] ? 1 : 0;
            $model->return += $data['return'] ? 1 : 0;
            $model->denied += $data['denied'] ? 1 : 0;

            if (!$model->save()) {
                return false;
            }
        }
        unset($child);
        return true;
    }

    /**
     * Собирает данные из заказа
     * @param Order $order
     * @param array $log
     * @return array
     */
    public static function collectDataFromOrder($order, $log = [])
    {
        $operator = ArrayHelper::getValue($log, 'operator', '');
        if (!$operator) {
            $user = ArrayHelper::getValue($log, 'user');
            if ($user) {
                $operator = $user->username;
            }
        }

        $sub_status = ArrayHelper::getValue(Json::decode($log['fields']), 'sub_status');

        $head = [
            'id' => $order->id,
            'period_type' => static::PERIOD_TYPE_HOUR,
            'period' => static::normalizePeriod($log['created_at'], static::PERIOD_TYPE_HOUR),
            'partner_id' => $order->partner_id,
            'country_id' => $order->country_id,
            'type_id' => $order->type_id,
            'operator' => $operator,

            'wrong_number' => $sub_status == Order::SUB_STATUS_TRASH_REASON_WRONG_NUMBER ? 1 : 0,
            'double' => $sub_status == Order::SUB_STATUS_TRASH_REASON_DOUBLE ? 1 : 0,
            'unaware' => $sub_status == Order::SUB_STATUS_TRASH_REASON_UNAWARE ? 1 : 0,
            'consulting' => $sub_status == Order::SUB_STATUS_TRASH_REASON_CONSULTING ? 1 : 0,
            'competitor' => $sub_status == Order::SUB_STATUS_TRASH_REASON_COMPETITOR ? 1 : 0,
            'return' => $sub_status == Order::SUB_STATUS_TRASH_REASON_RETURN ? 1 : 0,
            'denied' => $sub_status == Order::SUB_STATUS_TRASH_REASON_DENIED ? 1 : 0,
        ];

        $data = self::addProductData($head, Json::decode($log['product']));

        return $data;
    }
}

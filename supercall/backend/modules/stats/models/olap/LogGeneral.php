<?php
namespace backend\modules\stats\models\olap;


use common\models\Country;
use common\models\Product;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%order_log_olap_general}}".
 *
 * @property integer $period_type
 * @property integer $period
 * @property integer $product_id
 * @property integer $type_id
 * @property integer $partner_id
 * @property integer $country_id
 * @property integer $status
 * @property integer $quantity
 * @property string $price_data
 *
 * @property \common\modules\partner\models\Partner $partner
 * @property \common\models\Product $product
 * @property \common\models\Country $country
 * @property \common\modules\order\models\OrderType $type
 */
class LogGeneral extends AbstractOlap
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_log_olap_general}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period_type', 'period', 'partner_id', 'status', 'country_id', 'type_id'], 'required'],
            [['operator'], 'safe'],
            [['period_type', 'product_id', 'partner_id', 'country_id', 'status', 'quantity', 'period', 'type_id'], 'integer'],
            [['price_data'], 'string'],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partner::className(), 'targetAttribute' => ['partner_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'period_type' => 'Period Type',
            'period' => 'Period',
            'product_id' => 'Product',
            'partner_id' => 'Partner',
            'country_id' => 'Country',
            'status' => 'Status',
            'quantity' => 'Quantity',
            'price_data' => 'Price Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(OrderType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (is_array($this->price_data)) {
            $this->price_data = json_encode($this->price_data);
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->price_data = json_decode($this->price_data, true);
        parent::afterFind();
    }

    /**
     * Добавляет данные заказа в куб
     * @param $data
     * @return bool
     */
    public static function incrementWithHierarchy($data)
    {
        $child = new static();
        if (!$child->load([$child->formName() => $data]) || !$child->validate()) {
            return false;
        }

        $pkData = $child->getPrimaryKey(true);
        $pkData['operator'] = (string)$child->operator;
        $hierarchy = static::getHierarchy('period_type');
        $price = (int)ArrayHelper::getValue($data, 'price');

        unset($pkData['period']);
        unset($pkData['period_type']);
        $query = static::find()->where($pkData);
        $periods = [];
        foreach ($hierarchy as $periodType) {
            $periods[$periodType] = [
                'period_type' => $periodType,
                'period' => static::normalizePeriod($child->period, $periodType)
            ];
        }
        $query->andWhere(ArrayHelper::merge(['or'], $periods));
        $models = ArrayHelper::index($query->all(), 'period_type');

        foreach ($hierarchy as $periodType) {
            /** @var static $model */
            if (isset($models[$periodType])) {
                $model = $models[$periodType];
            } else {
                $model = new static();
                $modelData = $pkData;
                $modelData['period_type'] = $periodType;
                $modelData['period'] = $periods[$periodType]['period'];
                $modelData['quantity'] = 0;
                $modelData['price_data'] = [];
                $model->load([$child->formName() => $modelData]);
            }

            $priceData = $model->price_data;
            if (!is_array($priceData)){
                $priceData = [];
            }
            if (!isset($priceData[$price])){
                $priceData[$price] = [];
            }

            $priceData[$price][] = $data['id'];
            $model->quantity = (int)$model->quantity + 1;
            $model->price_data = $priceData;
            if (!$model->save()){
                return false;
            }
        }
        unset($child);
        return true;
    }

    /**
     * Собирает данные из заказа
     * @param Order $order
     * @param array $log
     * @return array
     * @throws \Exception
     */
    public static function collectDataFromOrder($order, $log = [])
    {
        $operator = ArrayHelper::getValue($log, 'operator', '');
        if (!$operator) {
            $user = User::find()->where(['id' => ArrayHelper::getValue($log, 'user_id')])->one();
            if ($user) {
                $operator = $user->username;
            }
        }
        $head = [
            'id' => $order->id,
            'period_type' => static::PERIOD_TYPE_HOUR,
            'period' => static::normalizePeriod($log['created_at'], static::PERIOD_TYPE_HOUR),
            'partner_id' => $order->partner_id,
            'country_id' => $order->country_id,
            'type_id' => $order->type_id,
            'status' => $log['new'],
            'operator' => $operator,
        ];

        $data = self::addProductData($head, Json::decode($log['product']));

        return $data;
    }
}

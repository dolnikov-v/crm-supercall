<?php
namespace backend\modules\stats\models\olap;
use yii\helpers\ArrayHelper;

/**
 * Class AbstractOlap
 * @package backend\modules\stats\models\olap
 */
abstract class AbstractOlap extends \yii\db\ActiveRecord
{
    const PERIOD_TYPE_HOUR = 1;
    const PERIOD_TYPE_DAY = 2;
    const PERIOD_TYPE_MONTH = 3;

    /**
     * @param $field
     * @return array
     */
    public static function getHierarchy($field)
    {
        switch ($field) {
            case 'period_type':
                return [
                    self::PERIOD_TYPE_HOUR,
                ];
        }
        return [];
    }

    /**
     * Конвертирует дату в корректный период по его типу
     * @param $period
     * @param $periodType
     * @return false|int
     */
    public static function normalizePeriod($period, $periodType)
    {
        $map = [
            self::PERIOD_TYPE_HOUR => 'Y-m-d H:00',
            self::PERIOD_TYPE_DAY => 'Y-m-d',
            self::PERIOD_TYPE_MONTH => 'Y-m-01',
        ];

        return strtotime(date($map[$periodType], $period));
    }

    /**
     * Разбирает массив данных $head по товарам $products
     * @param $head
     * @param $products
     * @return array
     */
    protected static function addProductData($head, $products)
    {
        if (!is_array($products)) {
            return [];
        }

        $costs = [];
        foreach ($products as $op) {
            $id = $op['product_id'];
            $cost = intval(ArrayHelper::getValue($op, 'cost', 0));
            try {
                $costs[$id] = ArrayHelper::getValue($costs, $id, 0) + $cost;
            } catch (\Exception $e) {
                echo $e;
            }
        }

        $cost_data = [];
        foreach ($costs as $id => $cost) {
            $cost_data[] = [
                'product_id' => $id,
                'cost' => $cost
            ];
        }

        $result = [
            $result[] = ArrayHelper::merge($head, [
                'product_id' => 0,
                'price' => array_sum($costs)
            ])
        ];
        foreach ($cost_data as $row) {
            $result[] = ArrayHelper::merge($head, [
                'product_id' => $row['product_id'],
                'price' => ArrayHelper::getValue($row, 'cost', 0),
            ]);
        }
        return $result;
    }
}
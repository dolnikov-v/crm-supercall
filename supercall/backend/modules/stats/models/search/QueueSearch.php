<?php

namespace backend\modules\stats\models\search;

use backend\modules\stats\models\Queue;
use common\models\Country;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\partner\models\Partner;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class QueueSearch
 * @package backend\modules\stats\models\search
 */
class QueueSearch extends Queue
{
    public $countries;
    public $queue;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id', 'countries', 'active', 'date_type', 'date_range', 'name', 'partner_id', 'queue'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $select = [
            'total' => new Expression("SUM(CASE WHEN status IN(" . implode(',', array_keys(Order::getStatusesCollection())) . ") THEN 1 ELSE 0 END)"),
            Queue::tableName() . '.*',
        ];

        foreach (array_keys(Order::getStatusesCollection()) as $status) {
            $select['quantity_' . $status] = new Expression("SUM(CASE WHEN status = {$status} THEN 1 ELSE 0 END)");
        }

        $query = Queue::find()
            ->select($select)
            ->joinWith('country')
            ->leftJoin(
                Order::tableName(),
                Queue::tableName() . '.id = CASE WHEN ' . Order::tableName() . '.last_queue_id is null THEN ' . Order::tableName() . '.prevent_queue_id ELSE ' . Order::tableName() . '.last_queue_id END')
            ->groupBy([Queue::tableName() . '.id'])
            ->orderBy([
                Queue::tableName() . '.id' => SORT_ASC
            ]);

        $query->andWhere(['queue.id' => ArrayHelper::getColumn(Queue::find()->byIdentityUser()->all(), 'id')]);

        $config = [
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ]
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->countries && !$this->partner_id) {
            return new ActiveDataProvider([
                'query' => Queue::find()->where(['id' => null])
            ]);
        }

        if($this->partner_id){
            $query->andWhere(['queue.id' => ArrayHelper::getColumn(Queue::find()->byPartner($this->partner_id)->all(), 'id')]);
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->countries) {
            $query->andWhere("
                CASE WHEN is_primary = true THEN primary_components->>'primary_country' = '" . $this->countries . "'
                                                 OR primary_components->>'primary_country' LIKE '%\"" . $this->countries . "\"%'
                ELSE " . Queue::tableName() . ".country_id = " . $this->countries . " END");
        }

        if ($this->date_type != self::IGNORE_DATE_TYPE) {
            if (empty($this->date_range)) {
                $this->date_range = date('d/m/Y 00:00') . ' - ' . date('d/m/Y 23:59');
            }
            /** @var Object $date */
            $date = Yii::$app->formatter->asConvertToFrom($this->date_range);

            if ($this->date_type == self::PARTNER_CREATED_DATE_TYPE) {
                $query->andWhere(['between', Order::tableName() . '.partner_created_at', $date->from->getTimestamp(), $date->to->getTimestamp()]);
            } elseif ($this->date_type == self::CC_CREATED_DATE_TYPE) {
                $query->andWhere(['between', Order::tableName() . '.created_at', $date->from->getTimestamp(), $date->to->getTimestamp()]);
            }
        }

        if ($this->queue) {
            $query->andWhere([Queue::tableName() . '.name' => $this->queue]);
        }

        return $dataProvider;
    }
}
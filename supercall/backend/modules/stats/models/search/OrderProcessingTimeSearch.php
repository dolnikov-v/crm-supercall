<?php
namespace backend\modules\stats\models\search;

use backend\modules\stats\models\OrderProcessingTime;
use common\models\Country;
use common\models\Product;
use common\models\Teams;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProduct;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class OrderProcessingTimeSearch extends OrderProcessingTime
{
    const GROUP_HOUR = 'hour';
    const GROUP_DAY = 'day';
    const GROUP_MONTH = 'month';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period', 'type_date', 'order_id', 'created_at', 'begin_date', 'end_date', 'groupBy', 'countries', 'status', 'product', 'team'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     * @throws HttpException
     */
    public function search($params = [])
    {
        $this->load($params);

        $report = yii::$app->controller->action->id;
        $this->groupBy = is_null($this->groupBy) ? self::GROUP_DAY : $this->groupBy;
        $this->period = is_null($this->period) ? date('Y/m/d 00:00:00') . ' - ' . date('Y/m/d 23:59:59') : $this->period;

        if ($report == 'dates') {
            $select = [
                'countOrders' => new Expression('count('.OrderProcessingTime::tableName().'.order_id)'),
                'country' => Country::tableName().'.name',
                'groupBy' => $this->getDateGroupIntervals($this->groupBy),
            ];

            $groupBy = $this->getDateGroupIntervals($this->groupBy);
        } elseif ($report == 'orders') {
            $select = [
                'countOrders' => new Expression('count('.OrderProcessingTime::tableName().'.order_id)'),
                'country' => Country::tableName().'.name',
                'order_id' => OrderProcessingTime::tableName().'.order_id',
                'status' => OrderProcessingTime::tableName().'.end_status',
                'time' => new Expression('round(sum(end_date-begin_date)/60.0, 2)'),
                'groupBy' => $this->getDateGroupIntervals($this->groupBy),
            ];

            $groupBy = OrderProcessingTime::tableName().".order_id, end_status," . $this->getDateGroupIntervals($this->groupBy);
        } elseif ($report == 'statuses') {
            $select = [
                'countOrders' => new Expression('count(end_status)'),
                'country' => Country::tableName().'.name',
                'end_status' => OrderProcessingTime::tableName() . '.end_status',
                'groupBy' => $this->getDateGroupIntervals($this->groupBy),
            ];

            $groupBy = "end_status, " . $this->getDateGroupIntervals($this->groupBy);
        } elseif ($report == 'products') {
            $select = [
                'countOrders' => new Expression('count('.OrderProcessingTime::tableName().'.order_id)'),
                'country' => Country::tableName().'.name',
                'product' => Product::tableName() . '.name',
                'groupBy' => $this->getDateGroupIntervals($this->groupBy),
            ];

            $groupBy = "product, " . $this->getDateGroupIntervals($this->groupBy);
        }else {
            throw new HttpException(404, Yii::t('common', 'Отчёт не найден'));
        }

        $sort = [];

        if (empty($params['sort'])) {
            $sort[OrderProcessingTime::tableName() . '.id'] = SORT_DESC;
        }

        $select['sumtime'] = new Expression('round(sum(end_date-begin_date)/60.0, 2)');
        $select['minimum'] = new Expression('round(min(end_date-begin_date)/60.0, 2)');
        $select['average'] = new Expression('round(avg(end_date-begin_date)/60.0, 2)');
        $select['maximum'] = new Expression('round(max(end_date-begin_date)/60.0, 2)');

        $query = OrderProcessingTime::find()
            ->select($select)
            ->joinWith('order')
            ->leftJoin(Country::tableName(), Country::tableName().'.id='.Order::tableName().'.country_id')
            ->where(['is not', 'end_date', null])
            ->groupBy($groupBy)
            ->orderBy([$this->getDateGroupIntervals($this->groupBy) => SORT_DESC]);

        if($report == 'products'){
            $query->leftJoin(OrderProduct::tableName(), OrderProduct::tableName().'.order_id='.Order::tableName().'.id');
            $query->leftJoin(Product::tableName(), Product::tableName().'.id='.OrderProduct::tableName().'.product_id');
        }

        $query->addGroupBy(Order::tableName().'.country_id');
        $query->addGroupBy(Country::tableName().'.id');

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->countries) {
            $query->andWhere(['in', Order::tableName().'.country_id',  array_values($this->countries)]);
        }

        if ($this->status) {
            $query->andWhere(['in', 'end_status',  array_values($this->status)]);
        }

        if ($this->product) {
            $query->andWhere(['in', 'product_id',  array_values($this->product)]);
        }

        if ($this->order_id) {
            $query->andWhere([OrderProcessingTime::tableName().'order_id' => $this->order_id]);
        }

        if ($this->period) {
            $period = explode(' - ', $this->period);
            $query->andFilterWhere(['>=', OrderProcessingTime::tableName() . '.begin_date', strtotime(date($period[0]))]);
            $query->andFilterWhere(['<=', OrderProcessingTime::tableName() . '.begin_date', strtotime(date($period[1]))]);
        }

        $query->leftJoin(OrderLog::tableName(), OrderLog::tableName().'.order_id='.Order::tableName().'.id');
        $query->leftJoin(User::tableName(), User::tableName().'.id='.OrderLog::tableName().'.user_id');

        if($this->team) {
            $users = Teams::getUsers($this->team);

            if(is_array($users)){
                $user_ids = ArrayHelper::getColumn($users, 'id');
                $query->andFilterWhere(['user.id' => $user_ids]);
            }
        }else{
            $userModel = yii::$app->user->getUserModel();
            $users = $userModel->getCoTeamsUsers();
            if(!empty($users)){
                $user_ids = ArrayHelper::getColumn($users, 'id');
                $query->andFilterWhere(['user.id' => $user_ids]);
            }
        }

        return $dataProvider;
    }
}
<?php
namespace backend\modules\stats\models\search;

use backend\modules\stats\models\ForecastOrders;
use common\models\Country;
use common\modules\order\models\Order;
use common\modules\order\models\OrderType;
use Yii;
use yii\base\Object;
use yii\db\Expression;

class ForecastOrdersSearch extends ForecastOrders
{
    /**
     * @return array
     */
    public function rules()
    {
        return parent::rules();
    }

    public function search($params = [])
    {
        $result = [
            'series' => [],
            'categories' => []
        ];

        if (empty($params) || (isset($params['country']) && empty($params['country']))) {
            return $result;
        }

        if (!$this->load($params) || !$this->validate($params)) {
            return $result;
        }

        //$dateType = (!$this->type || $this->type == OrderType::TYPE_NEW) ? 'created_at' : 'updated_at';
        $dateType = 'created_at';

        $group = 'extract(EPOCH FROM date_trunc(\'minutes\', to_timestamp(' . Order::tableName() . '.' . $dateType . ')))';

        switch ($this->group) {
            case ForecastOrders::GROUP_MINUTES:
                $group = 'extract(EPOCH FROM date_trunc(\'minutes\', to_timestamp(' . Order::tableName() . '.' . $dateType . ')))';
                break;
            case ForecastOrders::GROUP_HOURS:
                $group = 'extract(EPOCH FROM date_trunc(\'hours\', to_timestamp(' . Order::tableName() . '.' . $dateType . ')))';
                break;
            case ForecastOrders::GROUP_DAYS:
                $group = 'extract(EPOCH FROM date_trunc(\'days\', to_timestamp(' . Order::tableName() . '.' . $dateType . ')))';
                break;
            case ForecastOrders::GROUP_MONTHS:
                $group = 'extract(EPOCH FROM date_trunc(\'months\', to_timestamp(' . Order::tableName() . '.' . $dateType . ')))';
                break;
        }

        $query = Order::find()
            ->select([
                'count' => new Expression('distinct count(' . Order::tableName() . '.id)'),
                'country_name' => Country::tableName() . '.name',
                'group' => $group
            ])
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . Order::tableName() . '.country_id')
            ->groupBy(['group', 'country_name']);


        if ($this->country) {
            $query->andWhere([Order::tableName() . '.country_id' => $this->country]);
        }

        if ($this->partner) {
            $query->andWhere([Order::tableName() . '.partner_id' => $this->partner]);
        }

        if ($this->type) {
            $query->andWhere([Order::tableName() . '.type_id' => $this->type]);
        }

        if (!$this->date_range) {
            $this->date_range = date('d/m/Y 00:00:00') . ' - ' . date('d/m/Y 23:59:59');
        }
        /** @var Object $date */
        $date = Yii::$app->formatter->asConvertToFrom($this->date_range);


        $from = new \DateTime($date->from->format('Y-m-d H:i:s'), new \DateTimeZone(yii::$app->user->timezone->timezone_id));
        $to = new \DateTime($date->to->format('Y-m-d H:i:s'), new \DateTimeZone(yii::$app->user->timezone->timezone_id));

        $query->andWhere(['between', Order::tableName() . '.' . $dateType, $from->getTimestamp(), $to->getTimestamp()]);

        $query->orderBy(['group' => SORT_ASC]);

        $data = $query->all();

        foreach ($data as $model) {
            $date = $this->formatDate($model->group);
            $result['categories'][$date] = $date;
            $result['series'][$model->country_name]['name'] = yii::t('common', $model->country_name);
            $result['series'][$model->country_name]['data'][$date] = $model->count;
        }

        foreach ($result['series'] as &$value) {
            $value['data'] = array_values($value['data']);
        }

        unset($value);

        $result['series'] = array_values($result['series']);
        $result['categories'] = array_values($result['categories']);

        return $result;

    }
}
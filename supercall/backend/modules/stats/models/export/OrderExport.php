<?php

namespace backend\modules\stats\models\export;


use backend\models\TaskQueue;
use backend\modules\stats\models\report\OrderReport;
use Yii;
use yii\base\Object;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;

class OrderExport
{
    const FORMAT_EXCEL = 'excel';
    const FORMAT_CSV_COMMA = 'csv-comma';
    const FORMAT_CSV_SEMICOLON = 'csv-semicolon';

    const TAB_GENERAL = 'general';
    const TAB_ATTEMPTS = 'attempts';
    const TAB_APPROVED = 'approved';
    const TAB_REJECT = 'reject';
    const TAB_TRASH = 'trash';
    const EMPTY_QUERY = '[]';

    const FIELD_GROUP = 'group';
    const FIELD_TOTAL = 'total';

    const GROUP_BY_HOUR = 'hour';
    const GROUP_BY_DAY = 'day';
    const GROUP_BY_MONTH = 'month';
    const GROUP_BY_PRODUCT = 'product';
    const GROUP_BY_COUNTRY = 'country';

    const UNNAMED_FIELD_NAME = 'unnamed';

    const FIELD_GROUP_NAME = 'Группировка';
    const FIELD_TOTAL_NAME = 'Всего';
    const FIELD_NAME_RATIO = '%';

    //general
    const FIELD_MIDCHECK = 'midcheck';
    const FIELD_STATUS_QUANTITY_1 = 'status_quantity_1';
    const FIELD_STATUS_QUANTITY_2 = 'status_quantity_2';
    const FIELD_STATUS_QUANTITY_3 = 'status_quantity_3';
    const FIELD_STATUS_QUANTITY_4 = 'status_quantity_4';
    const FIELD_STATUS_QUANTITY_5 = 'status_quantity_5';
    const FIELD_STATUS_QUANTITY_6 = 'status_quantity_6';
    const FIELD_STATUS_QUANTITY_7 = 'status_quantity_7';
    const FIELD_STATUS_QUANTITY_8 = 'status_quantity_8';

    const FIELD_STATUS_QUANTITY_1_PERCENT = 'status_quantity_1_percent';
    const FIELD_STATUS_QUANTITY_2_PERCENT = 'status_quantity_2_percent';
    const FIELD_STATUS_QUANTITY_3_PERCENT = 'status_quantity_3_percent';
    const FIELD_STATUS_QUANTITY_4_PERCENT = 'status_quantity_4_percent';
    const FIELD_STATUS_QUANTITY_5_PERCENT = 'status_quantity_5_percent';
    const FIELD_STATUS_QUANTITY_6_PERCENT = 'status_quantity_6_percent';
    const FIELD_STATUS_QUANTITY_7_PERCENT = 'status_quantity_7_percent';
    const FIELD_STATUS_QUANTITY_8_PERCENT = 'status_quantity_8_percent';

    const FIELD_MIDCHECK_NAME = 'Средний чек';
    const FIELD_STATUS_QUANTITY_1_NAME = 'Новый';
    const FIELD_STATUS_QUANTITY_2_NAME = 'Перезвон';
    const FIELD_STATUS_QUANTITY_3_NAME = 'Недозвон';
    const FIELD_STATUS_QUANTITY_4_NAME = 'Одобрен';
    const FIELD_STATUS_QUANTITY_5_NAME = 'Отклонен';
    const FIELD_STATUS_QUANTITY_6_NAME = 'Треш';
    const FIELD_STATUS_QUANTITY_7_NAME = 'Дубль';
    const FIELD_STATUS_QUANTITY_8_NAME = 'Системный треш';

    //attempts
    const FIELD_QUANTITY = 'quantity';
    const FIELD_ATTEMPTS = 'attempts';
    const FIELD_RATIO = 'ratio';

    const FIELD_QUANTITY_NAME = 'Заказов';
    const FIELD_ATTEMPTS_NAME = 'Попыток';
    const FIELD_RATIO_NAME = '%';

    //approved
    const FIELD_IN_PROGRESS = 'in-progress';
    const FIELD_SUB_STATUS_402 = 'sub_status_402';
    const FIELD_SUB_STATUS_403 = 'sub_status_403';
    const FIELD_SUB_STATUS_404 = 'sub_status_404';
    const FIELD_SUB_STATUS_402_PERCENT = 'sub_status_402-percent';
    const FIELD_SUB_STATUS_403_PERCENT = 'sub_status_403-percent';
    const FIELD_SUB_STATUS_404_PERCENT = 'sub_status_404-percent';

    const FIELD_IN_PROGRESS_NAME = 'В процессе';
    const FIELD_SUB_STATUS_402_NAME = 'Выкуплено';
    const FIELD_SUB_STATUS_403_NAME = 'Не выкуплено';
    const FIELD_SUB_STATUS_404_NAME = 'Ожидает заказ';

    //rejected
    const FIELD_SUB_STATUS_501 = 'sub_status_501';
    const FIELD_SUB_STATUS_502 = 'sub_status_502';
    const FIELD_SUB_STATUS_503 = 'sub_status_503';
    const FIELD_SUB_STATUS_504 = 'sub_status_504';
    const FIELD_SUB_STATUS_505 = 'sub_status_505';
    const FIELD_SUB_STATUS_506 = 'sub_status_506';
    const FIELD_SUB_STATUS_507 = 'sub_status_507';
    const FIELD_SUB_STATUS_508 = 'sub_status_508';
    const FIELD_SUB_STATUS_509 = 'sub_status_509';
    const FIELD_SUB_STATUS_510 = 'sub_status_510';
    const FIELD_SUB_STATUS_511 = 'sub_status_511';
    const FIELD_SUB_STATUS_512 = 'sub_status_512';
    const FIELD_SUB_STATUS_513 = 'sub_status_513';
    const FIELD_SUB_STATUS_501_PERCENT = 'sub_status_501-percent';
    const FIELD_SUB_STATUS_502_PERCENT = 'sub_status_502-percent';
    const FIELD_SUB_STATUS_503_PERCENT = 'sub_status_503-percent';
    const FIELD_SUB_STATUS_504_PERCENT = 'sub_status_504-percent';
    const FIELD_SUB_STATUS_505_PERCENT = 'sub_status_505-percent';
    const FIELD_SUB_STATUS_506_PERCENT = 'sub_status_506-percent';
    const FIELD_SUB_STATUS_507_PERCENT = 'sub_status_507-percent';
    const FIELD_SUB_STATUS_508_PERCENT = 'sub_status_508-percent';
    const FIELD_SUB_STATUS_509_PERCENT = 'sub_status_509-percent';
    const FIELD_SUB_STATUS_510_PERCENT = 'sub_status_510-percent';
    const FIELD_SUB_STATUS_511_PERCENT = 'sub_status_511-percent';
    const FIELD_SUB_STATUS_512_PERCENT = 'sub_status_512-percent';
    const FIELD_SUB_STATUS_513_PERCENT = 'sub_status_513-percent';

    const FIELD_SUB_STATUS_501_NAME = 'Слишком дорого';
    const FIELD_SUB_STATUS_502_NAME = 'Передумал';
    const FIELD_SUB_STATUS_503_NAME = 'Медицинские противопоказания';
    const FIELD_SUB_STATUS_504_NAME = 'Без комментариев';
    const FIELD_SUB_STATUS_505_NAME = 'Автоматическое отклонение';
    const FIELD_SUB_STATUS_506_NAME = 'Не известный продукт';
    const FIELD_SUB_STATUS_507_NAME = 'Совершил покупку в другом магазине';
    const FIELD_SUB_STATUS_508_NAME = 'Плохие отзывы в интернете';
    const FIELD_SUB_STATUS_509_NAME = 'Личная причина не хочет обсуждать';
    const FIELD_SUB_STATUS_510_NAME = 'Не может позволить себе';
    const FIELD_SUB_STATUS_511_NAME = 'Заказ доставлен';
    const FIELD_SUB_STATUS_512_NAME = 'Клиент отказался';
    const FIELD_SUB_STATUS_513_NAME = 'КС не связалась с клиентом';

    //trashed
    const FIELD_SUB_STATUS_601 = 'sub_status_601';
    const FIELD_SUB_STATUS_602 = 'sub_status_602';
    const FIELD_SUB_STATUS_603 = 'sub_status_603';
    const FIELD_SUB_STATUS_604 = 'sub_status_604';
    const FIELD_SUB_STATUS_605 = 'sub_status_605';
    const FIELD_SUB_STATUS_606 = 'sub_status_606';
    const FIELD_SUB_STATUS_607 = 'sub_status_607';
    const FIELD_SUB_STATUS_608 = 'sub_status_608';
    const FIELD_SUB_STATUS_609 = 'sub_status_609';
    const FIELD_SUB_STATUS_610 = 'sub_status_610';
    const FIELD_SUB_STATUS_601_PERCENT = 'sub_status_601-percent';
    const FIELD_SUB_STATUS_602_PERCENT = 'sub_status_602-percent';
    const FIELD_SUB_STATUS_603_PERCENT = 'sub_status_603-percent';
    const FIELD_SUB_STATUS_604_PERCENT = 'sub_status_604-percent';
    const FIELD_SUB_STATUS_605_PERCENT = 'sub_status_605-percent';
    const FIELD_SUB_STATUS_606_PERCENT = 'sub_status_606-percent';
    const FIELD_SUB_STATUS_607_PERCENT = 'sub_status_607-percent';
    const FIELD_SUB_STATUS_608_PERCENT = 'sub_status_608-percent';
    const FIELD_SUB_STATUS_609_PERCENT = 'sub_status_609-percent';
    const FIELD_SUB_STATUS_610_PERCENT = 'sub_status_610-percent';

    const FIELD_SUB_STATUS_601_NAME = 'Несуществующий номер';
    const FIELD_SUB_STATUS_602_NAME = 'Дубль';
    const FIELD_SUB_STATUS_603_NAME = 'Клиент ничего не знает о заказе';
    const FIELD_SUB_STATUS_604_NAME = 'Консультация';
    const FIELD_SUB_STATUS_605_NAME = 'Заказано у конкурентов';
    const FIELD_SUB_STATUS_606_NAME = 'Возврат';
    const FIELD_SUB_STATUS_607_NAME = 'Не отправляем туда';
    const FIELD_SUB_STATUS_608_NAME = 'Не правильный формат телефона';
    const FIELD_SUB_STATUS_609_NAME = 'Шуточный заказ';
    const FIELD_SUB_STATUS_610_NAME = 'Другая причина';

    /** @var  string */
    public static $tab;

    /** @var  string */
    public static $format;

    /** @var  string */
    public static $group;

    /**
     * @return array
     */
    public static function map()
    {
        return [
            self::FIELD_GROUP => self::FIELD_GROUP_NAME,
            self::FIELD_TOTAL => self::FIELD_TOTAL_NAME,
            self::FIELD_MIDCHECK => self::FIELD_MIDCHECK_NAME,
            self::FIELD_STATUS_QUANTITY_1 => self::FIELD_STATUS_QUANTITY_1_NAME,
            self::FIELD_STATUS_QUANTITY_2 => self::FIELD_STATUS_QUANTITY_2_NAME,
            self::FIELD_STATUS_QUANTITY_3 => self::FIELD_STATUS_QUANTITY_3_NAME,
            self::FIELD_STATUS_QUANTITY_4 => self::FIELD_STATUS_QUANTITY_4_NAME,
            self::FIELD_STATUS_QUANTITY_5 => self::FIELD_STATUS_QUANTITY_5_NAME,
            self::FIELD_STATUS_QUANTITY_6 => self::FIELD_STATUS_QUANTITY_6_NAME,
            self::FIELD_STATUS_QUANTITY_7 => self::FIELD_STATUS_QUANTITY_7_NAME,
            self::FIELD_STATUS_QUANTITY_8 => self::FIELD_STATUS_QUANTITY_8_NAME,
            self::FIELD_QUANTITY => self::FIELD_QUANTITY_NAME,
            self::FIELD_ATTEMPTS => self::FIELD_ATTEMPTS_NAME,
            self::FIELD_RATIO => self::FIELD_RATIO_NAME,
            self::FIELD_IN_PROGRESS => self::FIELD_IN_PROGRESS_NAME,
            self::FIELD_SUB_STATUS_402 => self::FIELD_SUB_STATUS_402_NAME,
            self::FIELD_SUB_STATUS_403 => self::FIELD_SUB_STATUS_403_NAME,
            self::FIELD_SUB_STATUS_404 => self::FIELD_SUB_STATUS_404_NAME,
            self::FIELD_SUB_STATUS_402_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_403_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_404_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_501 => self::FIELD_SUB_STATUS_501_NAME,
            self::FIELD_SUB_STATUS_502 => self::FIELD_SUB_STATUS_502_NAME,
            self::FIELD_SUB_STATUS_503 => self::FIELD_SUB_STATUS_503_NAME,
            self::FIELD_SUB_STATUS_504 => self::FIELD_SUB_STATUS_504_NAME,
            self::FIELD_SUB_STATUS_505 => self::FIELD_SUB_STATUS_505_NAME,
            self::FIELD_SUB_STATUS_506 => self::FIELD_SUB_STATUS_506_NAME,
            self::FIELD_SUB_STATUS_507 => self::FIELD_SUB_STATUS_507_NAME,
            self::FIELD_SUB_STATUS_508 => self::FIELD_SUB_STATUS_508_NAME,
            self::FIELD_SUB_STATUS_509 => self::FIELD_SUB_STATUS_509_NAME,
            self::FIELD_SUB_STATUS_510 => self::FIELD_SUB_STATUS_510_NAME,
            self::FIELD_SUB_STATUS_511 => self::FIELD_SUB_STATUS_511_NAME,
            self::FIELD_SUB_STATUS_512 => self::FIELD_SUB_STATUS_512_NAME,
            self::FIELD_SUB_STATUS_513 => self::FIELD_SUB_STATUS_513_NAME,
            self::FIELD_SUB_STATUS_501_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_502_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_503_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_504_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_505_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_506_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_507_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_508_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_509_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_510_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_511_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_512_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_513_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_601 => self::FIELD_SUB_STATUS_601_NAME,
            self::FIELD_SUB_STATUS_602 => self::FIELD_SUB_STATUS_602_NAME,
            self::FIELD_SUB_STATUS_603 => self::FIELD_SUB_STATUS_603_NAME,
            self::FIELD_SUB_STATUS_604 => self::FIELD_SUB_STATUS_604_NAME,
            self::FIELD_SUB_STATUS_605 => self::FIELD_SUB_STATUS_605_NAME,
            self::FIELD_SUB_STATUS_606 => self::FIELD_SUB_STATUS_606_NAME,
            self::FIELD_SUB_STATUS_607 => self::FIELD_SUB_STATUS_607_NAME,
            self::FIELD_SUB_STATUS_608 => self::FIELD_SUB_STATUS_608_NAME,
            self::FIELD_SUB_STATUS_609 => self::FIELD_SUB_STATUS_609_NAME,
            self::FIELD_SUB_STATUS_610 => self::FIELD_SUB_STATUS_610_NAME,
            self::FIELD_SUB_STATUS_601_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_602_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_603_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_604_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_605_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_606_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_607_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_608_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_609_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_SUB_STATUS_610_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_STATUS_QUANTITY_1_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_STATUS_QUANTITY_2_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_STATUS_QUANTITY_3_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_STATUS_QUANTITY_4_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_STATUS_QUANTITY_5_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_STATUS_QUANTITY_6_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_STATUS_QUANTITY_7_PERCENT => self::FIELD_RATIO_NAME,
            self::FIELD_STATUS_QUANTITY_8_PERCENT => self::FIELD_RATIO_NAME
        ];
    }

    /**
     * @param $field
     * @return bool|string
     */
    public static function getNameField($field)
    {
        $map = self::map();

        if (isset($map[$field])) {
            return yii::t('common', $map[$field], [], 'en-US');
        }

        return self::UNNAMED_FIELD_NAME;
    }

    /**
     * @return array
     */
    public static function fieldCollectionGeneral()
    {
        return [
            self::FIELD_GROUP,
            self::FIELD_MIDCHECK,
            self::FIELD_TOTAL,
            self::FIELD_STATUS_QUANTITY_1,
            self::FIELD_STATUS_QUANTITY_1_PERCENT,
            self::FIELD_STATUS_QUANTITY_2,
            self::FIELD_STATUS_QUANTITY_2_PERCENT,
            self::FIELD_STATUS_QUANTITY_3,
            self::FIELD_STATUS_QUANTITY_3_PERCENT,
            self::FIELD_STATUS_QUANTITY_4,
            self::FIELD_STATUS_QUANTITY_4_PERCENT,
            self::FIELD_STATUS_QUANTITY_5,
            self::FIELD_STATUS_QUANTITY_5_PERCENT,
            self::FIELD_STATUS_QUANTITY_6,
            self::FIELD_STATUS_QUANTITY_6_PERCENT,
            self::FIELD_STATUS_QUANTITY_7,
            self::FIELD_STATUS_QUANTITY_7_PERCENT,
            self::FIELD_STATUS_QUANTITY_8,
            self::FIELD_STATUS_QUANTITY_8_PERCENT,
        ];
    }

    /**
     * @return array
     */
    public static function fieldCollectionAttempts()
    {
        return [
            self::FIELD_QUANTITY,
            self::FIELD_ATTEMPTS,
            self::FIELD_RATIO
        ];
    }

    /**
     * @return array
     */
    public static function fieldCollectionApproved()
    {
        return [
            self::FIELD_GROUP,
            self::FIELD_TOTAL,
            self::FIELD_IN_PROGRESS,
            self::FIELD_SUB_STATUS_402,
            self::FIELD_SUB_STATUS_402_PERCENT,
            self::FIELD_SUB_STATUS_403,
            self::FIELD_SUB_STATUS_403_PERCENT,
            self::FIELD_SUB_STATUS_404,
            self::FIELD_SUB_STATUS_404_PERCENT,
        ];
    }

    /**
     * @return array
     */
    public static function fieldCollectionRejected()
    {
        return [
            self::FIELD_GROUP,
            self::FIELD_TOTAL,
            self::FIELD_SUB_STATUS_501,
            self::FIELD_SUB_STATUS_501_PERCENT,
            self::FIELD_SUB_STATUS_502,
            self::FIELD_SUB_STATUS_502_PERCENT,
            self::FIELD_SUB_STATUS_503,
            self::FIELD_SUB_STATUS_503_PERCENT,
            self::FIELD_SUB_STATUS_504,
            self::FIELD_SUB_STATUS_504_PERCENT,
            self::FIELD_SUB_STATUS_505,
            self::FIELD_SUB_STATUS_505_PERCENT,
            self::FIELD_SUB_STATUS_506,
            self::FIELD_SUB_STATUS_506_PERCENT,
            self::FIELD_SUB_STATUS_507,
            self::FIELD_SUB_STATUS_507_PERCENT,
            self::FIELD_SUB_STATUS_508,
            self::FIELD_SUB_STATUS_508_PERCENT,
            self::FIELD_SUB_STATUS_509,
            self::FIELD_SUB_STATUS_509_PERCENT,
            self::FIELD_SUB_STATUS_510,
            self::FIELD_SUB_STATUS_510_PERCENT,
            self::FIELD_SUB_STATUS_511,
            self::FIELD_SUB_STATUS_511_PERCENT,
            self::FIELD_SUB_STATUS_512,
            self::FIELD_SUB_STATUS_512_PERCENT,
            self::FIELD_SUB_STATUS_513,
            self::FIELD_SUB_STATUS_513_PERCENT,
        ];
    }

    /**
     * @return array
     */
    public static function fieldCollectionTrashed()
    {
        return [
            self::FIELD_GROUP,
            self::FIELD_TOTAL,
            self::FIELD_SUB_STATUS_601,
            self::FIELD_SUB_STATUS_601_PERCENT,
            self::FIELD_SUB_STATUS_602,
            self::FIELD_SUB_STATUS_602_PERCENT,
            self::FIELD_SUB_STATUS_603,
            self::FIELD_SUB_STATUS_603_PERCENT,
            self::FIELD_SUB_STATUS_604,
            self::FIELD_SUB_STATUS_604_PERCENT,
            self::FIELD_SUB_STATUS_605,
            self::FIELD_SUB_STATUS_605_PERCENT,
            self::FIELD_SUB_STATUS_606,
            self::FIELD_SUB_STATUS_606_PERCENT,
            self::FIELD_SUB_STATUS_607,
            self::FIELD_SUB_STATUS_607_PERCENT,
            self::FIELD_SUB_STATUS_608,
            self::FIELD_SUB_STATUS_608_PERCENT,
            self::FIELD_SUB_STATUS_609,
            self::FIELD_SUB_STATUS_609_PERCENT,
            self::FIELD_SUB_STATUS_610,
            self::FIELD_SUB_STATUS_610_PERCENT,
        ];
    }

    /**
     * @return array
     */
    public static function getRules()
    {
        $rules = [];


        switch (self::$tab) {
            case self::TAB_GENERAL:
                $rules = [
                    self::FIELD_GROUP => function ($value) {
                        return self::formatGroup($value[self::FIELD_GROUP]);
                    },
                    self::FIELD_MIDCHECK => function ($value) {
                        return yii::$app->formatter->asDecimal($value[self::FIELD_MIDCHECK], 2);
                    }
                ];
                break;
            case self::TAB_APPROVED:
            case self::TAB_REJECT:
            case self::TAB_TRASH:
                $rules = [
                    self::FIELD_GROUP => function ($value) {
                        return self::formatGroup($value[self::FIELD_GROUP]);
                    }
                ];
                break;
        }

        $fieldCollection = self::replaceRules($rules);

        return ArrayHelper::merge($rules, $fieldCollection);
    }

    /**
     * @param TaskQueue $taskQueue
     * @return array|bool
     */
    public static function export(TaskQueue $taskQueue)
    {
        $getData = self::getData($taskQueue);

        if (!$getData['success']) {
            return $getData;
        } else {
            $export = [
                'success' => false,
                'message' => yii::t('common', 'Неизвестный формат экспорта: {format}', ['format' => self::$format])
            ];

            switch (self::$format) {
                case self::FORMAT_EXCEL:
                    $export = self::exportExcel($getData['message'], $taskQueue);
                    break;
            }

            return $export;
        }
    }

    /**
     * @param $data
     * @param TaskQueue $taskQueue
     * @return array
     */
    public static function exportExcel($data, TaskQueue $taskQueue)
    {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle(substr($taskQueue->name, 0, 31));
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
        $objPHPExcel->getDefaultStyle()->getFont()->setBold(true);
        $objPHPExcel->getDefaultStyle()->getFont()->setBold(true);
        $letterA = 65;
        $secondLetter = 65;
        $letter = 65;
        foreach (self::getFieldCollection() as $one) {
            $fieldLetter = ($letter <= 90 ? chr($letter) : chr($letterA) . chr($secondLetter++));

            $objPHPExcel->getActiveSheet()->getColumnDimension($fieldLetter)->setAutoSize(true);

            $letter++;

        }

        $letterA = 65;
        $secondLetter = 65;
        $letter = 65;
        //шапка
        foreach (self::getFieldCollection() as $index => $label) {
            $fieldLetter = ($letter <= 90 ? chr($letter) : chr($letterA) . chr($secondLetter++));

            $objPHPExcel->getActiveSheet()->setCellValue($fieldLetter . '1', self::getNameField($label));
            $objPHPExcel->getActiveSheet()->getStyle($fieldLetter . '1')->getAlignment()->setHorizontal(
                \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $letter++;

        }
        $row = 2;
        $letterA = 65;
        $secondLetter = 65;
        $letter = 65;

        $objPHPExcel->getDefaultStyle()->getFont()->setBold(false);

        //данные
        /** @var array|ArrayDataProvider $data */
        $data = !is_array($data) ? $data->getModels() : $data['dataProvider']->getModels();

        foreach ($data as $model) {
            foreach (self::getFieldCollection() as $field => $name) {
                foreach (self::getRules() as $key => $rule) {
                    $field_name = is_numeric($key) ? $rule : $key;
                    if ($field == $field_name) {
                        $value = is_numeric($key) ? $model[$rule] : $rule($model);
                        $value = empty($value) ? 0 : $value;

                        $fieldLetter = ($letter <= 90 ? chr($letter) : chr($letterA) . chr($secondLetter++));

                        $objPHPExcel->getActiveSheet()->setCellValue($fieldLetter . $row, $value);
                        $objPHPExcel->getActiveSheet()->getStyle($fieldLetter . $row)->getAlignment()->setHorizontal(
                            \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $letter++;
                    } else {
                        continue;
                    }
                }
            }

            $letter = 65;
            $secondLetter = 65;
            $row++;

        }

        $filename = $taskQueue->name . ".xls";
        $path = yii::getAlias('@backend/files/export/') . $taskQueue->user_id . DIRECTORY_SEPARATOR;

        if (!FileHelper::createDirectory($path, 0755, true)) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Ошибка создания директории для экспорта: {path}', ['path' => $path], 'en-US')
            ];
        } else {

            $filePath = $path . $filename;
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save($filePath);

            if (!file_exists($filePath)) {
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка сохранения файла: {filePath}', ['filePath' => $filePath], 'en-US')
                ];
            } else {
                $result = [
                    'success' => true,
                    'message' => $filePath
                ];
            }
        }

        return $result;

    }

    /**
     * @param $value
     * @return string
     */
    public static function formatGroup($value)
    {
        switch (self::$group) {
            case self::GROUP_BY_HOUR:
                $formatedData = yii::$app->formatter->asDate($value, 'php: H:00 Y-m-d');
                break;
            case self::GROUP_BY_DAY:
                $formatedData = yii::$app->formatter->asDate($value);
                break;
            case self::GROUP_BY_MONTH :
                $formatedData = yii::$app->formatter->asDate($value, 'php: F Y');
                break;
            default:
                $formatedData = $value;
                break;
        }

        return $formatedData;
    }

    /**
     * @param $rules
     * @return array
     */
    public static function replaceRules($rules)
    {
        $replacedFieldCollection = [];
        $field_rules = array_keys($rules);

        foreach (self::getFieldCollection() as $field) {
            if (!in_array($field, $field_rules)) {
                $replacedFieldCollection[] = $field;
            }
        }

        return $replacedFieldCollection;
    }

    /**
     * @param TaskQueue $taskQueue
     * @return array
     */
    public static function getData(TaskQueue $taskQueue)
    {
        $orderReport = new OrderReport();
        $query = Json::decode($taskQueue->query);
        $params = Json::decode($query['params']);
        $data = [];

        self::$tab = $query['tab'];
        self::$format = $query['format'];
        self::$group = $query['group'];

        if ($orderReport->load($params) && $orderReport->validate()) {
            /** @var Object $period */
            $period = yii::$app->formatter->asConvertToFrom($orderReport->date_range);

            $orderReport->date_start = $period->from->format('Y-m-d H:i:s');
            $orderReport->date_end = $period->to->format('Y-m-d H:i:s');
            $orderReport->user_id = $taskQueue->user_id;

            switch (self::$tab) {
                case self::TAB_GENERAL:
                    /** @var array $data */
                    $data = $orderReport->getGeneralAggregate();
                    break;
                case self::TAB_ATTEMPTS:
                    $data = $orderReport->getAttempts();
                    break;
                case self::TAB_APPROVED:
                    $data = $orderReport->getApprovedStats();
                    break;
                case self::TAB_REJECT:
                    $data = $orderReport->getRejectStats();
                    break;
                case self::TAB_TRASH:
                    $data = $orderReport->getTrashStats();
                    break;
            }

            $aggregate = [];

            $aggregate['dataProvider'] = is_array($data) ? $data['dataProvider'] : $data;
            $models = $aggregate['dataProvider']->getModels();

            if (empty($models)) {
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Данные не найдено')
                ];
            } else {
                return [
                    'success' => true,
                    'message' => $data,
                ];
            }
        } else {
            $result = [
                'success' => false,
                'message' => Json::encode($orderReport->getErrors())
            ];
        }

        return $result;
    }

    /**
     * @return array
     */
    public static function getFieldCollection()
    {
        $collection = [];

        switch (self::$tab) {
            case self::TAB_GENERAL:
                $collection = self::fieldCollectionGeneral();
                break;
            case self::TAB_ATTEMPTS:
                $collection = self::fieldCollectionAttempts();
                break;
            case self::TAB_APPROVED:
                $collection = self::fieldCollectionApproved();
                break;
            case self::TAB_REJECT:
                $collection = self::fieldCollectionRejected();
                break;
            case self::TAB_TRASH:
                $collection = self::fieldCollectionTrashed();
                break;
        }

        return $collection;

    }
}
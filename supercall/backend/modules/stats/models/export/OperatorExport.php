<?php

namespace backend\modules\stats\models\export;


use backend\models\TaskQueue;
use backend\modules\stats\models\report\OperatorReport;
use Yii;
use yii\base\Object;
use yii\helpers\FileHelper;
use yii\helpers\Json;

/**
 * Class OperatorExport
 * @package backend\modules\stats\models\export
 */
class OperatorExport
{
    const FIELD_USER_NAME = 'username';
    const FIELD_TOTAL = 'total';
    const FIELD_MIDCHECK = 'midcheck';
    const FIELD_GROUP = 'group';
    const FIELD_QUANTITY_NEW = 'status_quantity_1';
    const FIELD_QUANTITY_RECALL = 'status_quantity_2';
    const FIELD_QUANTITY_NOANSWER = 'status_quantity_3';
    const FIELD_QUANTITY_APPROVE = 'status_quantity_4';
    const FIELD_QUANTITY_REJECT = 'status_quantity_5';
    const FIELD_QUANTITY_TRASH = 'status_quantity_6';
    const FIELD_QUANTITY_DOUBLE = 'status_quantity_7';
    const FIELD_QUANTITY_SYSTEM_TRASH = 'status_quantity_8';
    const FIELD_TIME_ONLINE = 'online';

    const GROUP_HOUR = 'hour';
    const GROUP_DAY = 'day';
    const GROUP_MONTH = 'month';
    const GROUP_PRODUCT = 'product';
    const GROUP_COUNTRY = 'country';

    /** @var  OperatorReport $model*/
    public static $model;

    /**
     * @return array
     */
    public static function getFields()
    {
        return [
            self::FIELD_GROUP => yii::t('common', 'Группировка', [], 'en-US'),
            self::FIELD_USER_NAME => yii::t('common', 'Оператор', [], 'en-US'),
            self::FIELD_TOTAL => yii::t('common', 'Всего', [], 'en-US'),
            self::FIELD_QUANTITY_NEW => yii::t('common', 'Новый', [], 'en-US'),
            self::FIELD_QUANTITY_APPROVE => yii::t('common', 'Одобрен', [], 'en-US'),
            self::FIELD_QUANTITY_RECALL => yii::t('common', 'Перезвон', [], 'en-US'),
            self::FIELD_QUANTITY_NOANSWER => yii::t('common', 'Недозвон', [], 'en-US'),
            self::FIELD_QUANTITY_REJECT => yii::t('common', 'Отклонен', [], 'en-US'),
            self::FIELD_QUANTITY_TRASH => yii::t('common', 'Треш', [], 'en-US'),
            self::FIELD_QUANTITY_DOUBLE => yii::t('common', 'Дубль', [], 'en-US'),
            self::FIELD_QUANTITY_SYSTEM_TRASH => yii::t('common', 'Системный треш', [], 'en-US'),
            self::FIELD_MIDCHECK => yii::t('common', 'Средний чек', [], 'en-US'),
        ];
    }

    public static function getFieldsPercentAfter()
    {
        return[
            self::FIELD_QUANTITY_APPROVE,
            self::FIELD_QUANTITY_RECALL,
            self::FIELD_QUANTITY_NOANSWER,
            self::FIELD_QUANTITY_REJECT,
            self::FIELD_QUANTITY_TRASH,
            self::FIELD_QUANTITY_DOUBLE,
            self::FIELD_QUANTITY_SYSTEM_TRASH,
        ];

    }

    /**
     * @return array
     */
    public static function getRules()
    {
        return [
            self::FIELD_USER_NAME,
            self::FIELD_TOTAL,
            self::FIELD_GROUP => function($data){
                return self::$model->formatGroupView($data);
            },
            self::FIELD_QUANTITY_NEW,
            self::FIELD_QUANTITY_APPROVE,
            self::FIELD_QUANTITY_RECALL,
            self::FIELD_QUANTITY_NOANSWER,
            self::FIELD_QUANTITY_REJECT,
            self::FIELD_QUANTITY_TRASH,
            self::FIELD_QUANTITY_DOUBLE,
            self::FIELD_QUANTITY_SYSTEM_TRASH,
            self::FIELD_MIDCHECK => function($data){
                return yii::$app->formatter->asDecimal($data[self::FIELD_MIDCHECK], 2);
            },
        ];
    }

    /**
     * @param array $queryParams
     * @return array
     */
    public static function agregateParams($queryParams)
    {
        if (empty($queryParams['OperatorReport']['date_range'])) {
            $queryParams['OperatorReport']['date_range'] = date('d/m/Y 00:00') . ' - ' . date('d/m/Y 23:59');
        }

        /** @var Object $date */
        $date = Yii::$app->formatter->asConvertToFrom($queryParams['OperatorReport']['date_range']);
        $queryParams['OperatorReport']['date_start'] = $date->from->format('d.m.Y H:i');
        $queryParams['OperatorReport']['date_end'] = $date->to->format('d.m.Y H:i');

        return $queryParams;
    }

    /**
     * @param $task
     * @param $model
     * @return array
     */
    public static function getData($task, $model)
    {
        $queryParams = Json::decode($task->query);

        /** @var OperatorReport $searchModel */
        $searchModel = $model;
        $searchModel = new $searchModel;
        $searchModel->is_export = true;
        $searchModel->load(self::agregateParams($queryParams));
        $dataProvider = $searchModel->getGeneralAggregate()['dataProvider'];
        $fields = self::getFields();

        self::$model = $searchModel;

        $title = strtr($task->name, [
            '/' => '-',
            ' ' => '_',
            ':' => '-'
        ]);

        $dataProvider->pagination = false;

        return [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $title,
            'fields' => $fields,
        ];
    }

    /**
     * @param $task
     * @return array
     */
    public static function exportExcel($task)
    {
        if (TaskQueue::TYPE_STATS_OPERATOR_IMPORT != $task->type) {
            return [
                'success' => false,
                'message' => 'undefined type export'
            ];
        }

        if (!$task->query || empty($task->query)) {
            return [
                'success' => false,
                'message' => 'empty query params'
            ];
        }

        $model = OperatorReport::className();

        $data = self::getData($task, $model);
        $dataProvider = $data['dataProvider'];
        $title = $data['title'];
        $fields = $data['fields'];

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle(substr($title, 0, 31));
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
        $objPHPExcel->getDefaultStyle()->getFont()->setBold(true);

        $letter = 65;
        //шапка
        foreach ($fields as $index=>$label) {
            $objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . '1', yii::t('common', $label));
            $objPHPExcel->getActiveSheet()->getStyle(chr($letter) . '1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($letter))->setAutoSize(true);
            if(in_array($index, self::getFieldsPercentAfter())){
                $objPHPExcel->getActiveSheet()->getStyle(chr($letter+1) . '1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getColumnDimension(chr($letter+1))->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->setCellValue(chr($letter+1) . '1', '%');
                $letter++;
            }
            $letter++;

        }
        $row = 2;
        $letter = 65;
        $total = 0;

        $objPHPExcel->getDefaultStyle()->getFont()->setBold(false);

        //данные
        foreach ($dataProvider->getModels() as $model) {
            foreach ($fields as $field=>$name) {
                foreach (self::getRules() as $key => $rule) {
                    $field_name = is_numeric($key) ? $rule : $key;
                    if ($field == $field_name) {
                        $value = is_numeric($key) ? $model[$rule] : $rule($model);
                        $value = empty($value) ? '-' : $value;

                        if($field_name == self::FIELD_TOTAL && is_numeric($value)){
                            $total = $value;
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $row, $value);
                        $objPHPExcel->getActiveSheet()->getStyle(chr($letter) . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                        if(in_array($field_name, self::getFieldsPercentAfter())){
                            $objPHPExcel->getActiveSheet()->setCellValue(chr($letter+1) . $row, $total > 0 ? round($value / $total * 100, 2) : 0);
                            $objPHPExcel->getActiveSheet()->getStyle(chr($letter+1) . $row)->getFont() ->getColor()->setRGB('E74C3C');
                            $letter++;
                        }

                        $letter++;
                    } else {
                        continue;
                    }
                }
            }
            $letter = 65;
            $row++;

        }

        $filename = $title . ".xls";
        $path = yii::getAlias('@backend/files/export/') . $task->user_id . DIRECTORY_SEPARATOR;

        if(!FileHelper::createDirectory($path, 0755, true)){
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Ошибка создания директории для экспорта: {path}', ['path' => $path], 'en-US')
            ];
        }else{

            $filePath = $path . $filename;
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save($filePath);

            if(!file_exists($filePath)){
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка сохранения файла: {filePath}', ['filePath' => $filePath],  'en-US')
                ];
            }else{
                $result = [
                    'success' => true,
                    'message' => $filePath
                ];
            }
        }

        return $result;

    }
}
<?php

namespace backend\modules\stats\models\export;

use backend\models\TaskQueue;
use backend\modules\stats\models\report\OperatorOnlineReport;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;

/**
 * Class OperatorOnlineExport
 * @package backend\modules\stats\models\export
 */
class OperatorOnlineExport
{
    const FIELD_USERNAME = 'username';
    const FIELD_GROUP = 'group';
    const FIELD_TIME_ONLINE = 'online';
    const FIELD_OPERATOR = 'operator';
    const FIELD__MASK_OPERATOR = 'Operator';

    const GROUP_HOUR = 'hour';
    const GROUP_DAY = 'day';
    const GROUP_MONTH = 'month';
    const GROUP_OPERATOR= 'operator';
    const GROUP_COUNTRY = 'country';

    /** @var  OperatorOnlineReport $model*/
    public static $model;

    /**
     * @return array
     */
    public static function getFields()
    {
        return [
            self::FIELD_USERNAME => yii::t('common', 'Оператор', [], 'en-US'),
            self::FIELD_GROUP => yii::t('common', 'Группировка', [], 'en-US'),
            self::FIELD_TIME_ONLINE => yii::t('common', 'Время онлайн', [], 'en-US'),
        ];
    }

    /**
     * @return array
     */
    public static function getRules()
    {
        return [
            self::FIELD_USERNAME,
            self::FIELD_GROUP => function($data){
                return self::$model->formatGroupView($data);
            },
            self::FIELD_TIME_ONLINE => function($data){
                return yii::$app->formatter->asDecimal($data[self::FIELD_TIME_ONLINE], 2);
            },
        ];
    }

    /**
     * @param array $queryParams
     * @return array
     */
    public static function agregateParams($queryParams)
    {
        if (empty($queryParams['OperatorReport']['date_range'])) {
            $queryParams['OperatorReport']['date_range'] = date('d/m/Y 00:00') . ' - ' . date('d/m/Y 23:59');
        }

        /** @var Object $date */
        $date = Yii::$app->formatter->asConvertToFrom($queryParams['OperatorReport']['date_range']);
        $queryParams['OperatorReport']['date_start'] = $date->from->format('d.m.Y H:i');
        $queryParams['OperatorReport']['date_end'] = $date->to->format('d.m.Y H:i');

        return $queryParams;
    }

    /**
     * @param $task
     * @param $model
     * @return array
     */
    public static function getData($task, $model)
    {
        $queryParams = Json::decode($task->query);

        /** @var OperatorOnlineReport $searchModel */
        $searchModel = $model;
        $searchModel = new $searchModel;
        $searchModel->is_export = true;
        $searchModel->load(self::agregateParams($queryParams));
        $dataProvider = $searchModel->getGeneralAggregate()['dataProvider'];
        $fields = self::getFields();

        self::$model = $searchModel;

        $title = strtr($task->name, [
            '/' => '-',
            ' ' => '_',
            ':' => '-'
        ]);

        $dataProvider->pagination = false;

        return [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $title,
            'fields' => $fields,
        ];
    }

    /**
     * @param $task
     * @return array
     */
    public static function exportExcel($task)
    {
        if (TaskQueue::TYPE_STATS_OPERATOR_ONLINE_IMPORT != $task->type) {
            return [
                'success' => false,
                'message' => 'undefined type export'
            ];
        }

        if (!$task->query || empty($task->query)) {
            return [
                'success' => false,
                'message' => 'empty query params'
            ];
        }

        $transaction = yii::$app->db->beginTransaction();

        try {
            $model = OperatorOnlineReport::className();
            $query = Json::decode($task->query);

            $data = self::getData($task, $model);
            $dataProvider = $data['dataProvider'];
            $title = $data['title'];
            $fields = $data['fields'];

            $objPHPExcel = new \PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle(substr($title, 0, 31));
            $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
            $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
            $objPHPExcel->getDefaultStyle()->getFont()->setBold(true);
            $objPHPExcel->getDefaultStyle()->getFont()->setBold(true);
            $letter = 65;
            foreach ($fields as $one) {
                $objPHPExcel->getActiveSheet()->getColumnDimension(chr($letter))->setAutoSize(true);
                $letter++;
            }
            $letter = 65;
            //шапка

            foreach ($fields as $index => $label) {
                if ($query['OperatorOnlineReport']['group'] == self::GROUP_OPERATOR && $label == self::FIELD__MASK_OPERATOR) {
                    continue;
                }

                $objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . '1', yii::t('common', $label));
                $objPHPExcel->getActiveSheet()->getStyle(chr($letter) . '1')->getAlignment()->setHorizontal(
                    \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $letter++;

            }
            $row = 2;
            $letter = 65;

            $objPHPExcel->getDefaultStyle()->getFont()->setBold(false);

            //данные
            foreach ($dataProvider->getModels() as $model) {
                foreach ($fields as $field => $name) {

                    if ($query['OperatorOnlineReport']['group'] == self::GROUP_OPERATOR && $name == self::FIELD__MASK_OPERATOR) {
                        continue;
                    }

                    foreach (self::getRules() as $key => $rule) {
                        $field_name = is_numeric($key) ? $rule : $key;
                        if ($field == $field_name) {
                            $value = is_numeric($key) ? $model[$rule] : $rule($model);
                            $value = empty($value) ? '-' : $value;

                            $objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $row, $value);
                            $objPHPExcel->getActiveSheet()->getStyle(chr($letter) . $row)->getAlignment()->setHorizontal(
                                \PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $letter++;
                        } else {
                            continue;
                        }
                    }
                }
                $letter = 65;
                $row++;

            }

            $filename = $title . ".xls";
            $path = yii::getAlias('@backend/files/export/') . $task->user_id . DIRECTORY_SEPARATOR;

            if (!FileHelper::createDirectory($path, 0755, true)) {
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка создания директории для экспорта: {path}', ['path' => $path], 'en-US')
                ];
            } else {

                $filePath = $path . $filename;
                $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save($filePath);

                if (!file_exists($filePath)) {
                    $result = [
                        'success' => false,
                        'message' => yii::t('common', 'Ошибка сохранения файла: {filePath}', ['filePath' => $filePath], 'en-US')
                    ];
                } else {
                    $result = [
                        'success' => true,
                        'message' => $filePath
                    ];
                }
            }
            $transaction->commit();

            return $result;
        }catch (\Throwable $e){
            echo $e->getMessage();
            $transaction->rollBack();
        }
    }
}
<?php
namespace backend\modules\stats\models;

use backend\modules\stats\models\query\OrderProcessingTimeQuery;
use common\components\db\ActiveRecord;
use common\modules\order\models\Order;
use common\models\User;
use Yii;

class OrderProcessingTime extends ActiveRecord
{
    public $period;
    public $country;
    public $groupBy;
    public $date;
    public $countOrders;
    public $minimum;
    public $maximum;
    public $average;
    public $time;
    public $status;
    public $sumtime;
    public $countries;
    public $product;
    public $team;
    const GROUP_HOUR = 'hour';
    const GROUP_DAY = 'day';
    const GROUP_MONTH = 'month';
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_processing_time}}';
    }

    /**
     * @return OrderProcessingTimeQuery
     */
    public static function find()
    {
        return new OrderProcessingTimeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id', 'begin_date', 'created_at', 'begin_status'], 'required'],
            [['user_id', 'order_id', 'begin_date', 'end_date', 'created_at', 'updated_at', 'begin_status', 'end_status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'order_id' => Yii::t('common', 'Заказ'),
            'begin_date' => Yii::t('common', 'Начало работы'),
            'end_date' => Yii::t('common', 'Окончание работы'),
            'begin_status' => Yii::t('common', 'Начальный статус'),
            'end_status' => Yii::t('common', 'Конечный статус'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
            'countOrders' => Yii::t('common', 'Количество заказов'),
            'groupBy' => Yii::t('common', 'Группировка'),
            'sumtime' => yii::t('common', 'Суммарное время, мин.'),
            'minimum' => Yii::t('common', 'Минимальное время, мин.'),
            'average' => Yii::t('common', 'Среднеее  время, мин.'),
            'maximum' => Yii::t('common', 'Максимальное  время, мин.'),
        ];
    }

    /**
     * @param $data
     * @return bool|integer
     */
    public static function saveData($data)
    {
        if(!isset($data['order_id']) || !isset($data['status'])){
            return false;
        }

        $model = new OrderProcessingTime();
        $model->order_id = $data['order_id'];
        $model->user_id = yii::$app->user->identity->id;
        $model->begin_date = time();
        $model->begin_status = $data['status'];
        $model->created_at = time();

        $model->save();

        return $model->id;

    }

    /**
     * @param $data
     * @return bool
     */
    public static function updateData($data)
    {
        if(!isset($data['order_id']) || !isset($data['status'])){
            return false;
        }

        $model = OrderProcessingTime::find()
            ->where([
                'order_id' => $data['order_id']
            ])
            ->orderBy(['id' => SORT_DESC])->limit(1)->one();

        if($model){
            $model->end_date = time();
            $model->updated_at = time();
            $model->end_status = $data['status'];
            
            return $model->save(false);
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public function getDateInterval()
    {
        return [
            self::GROUP_HOUR => yii::t('common', 'Час'),
            self::GROUP_DAY => yii::t('common', 'День'),
            self::GROUP_MONTH => yii::t('common', 'Месяц'),
        ];
    }

    public function getDateGroupIntervals($interval)
    {
        $groupPeriods =  [
            self::GROUP_HOUR => 'extract(epoch from date_trunc(\'hour\', to_timestamp(begin_date) at time zone \'' . \Yii::$app->user->timezone->timezone_id . '\'))',
            self::GROUP_DAY => 'extract(epoch from date_trunc(\'day\', to_timestamp(begin_date) at time zone \'' . \Yii::$app->user->timezone->timezone_id . '\'))',
            self::GROUP_MONTH => 'extract(epoch from date_trunc(\'month\', to_timestamp(begin_date) at time zone \'' . \Yii::$app->user->timezone->timezone_id . '\'))',
        ];

        return $groupPeriods[$interval];
    }
}
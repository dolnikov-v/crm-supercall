<?php

namespace backend\modules\stats\models\filter;


use common\modules\order\models\Order;
use yii\base\Model;

/**
 * FIXME: удалить после успешного внедрения OLAP
 * Общий класс-фильтр для обработки запросов по фильтрации статистики заказов
 * Class OrderFilter
 * @package backend\modules\stats\models\filter
 */
class OrderFilter extends Model
{
    const GROUP_HOUR = 'hour';
    const GROUP_DAY = 'day';
    const GROUP_MONTH = 'month';
    const GROUP_PRODUCT = 'product';
    const GROUP_COUNTRY = 'country';
    
    /**
     * Распределяет запрос по типу группировки
     * поздним статич.связыванием
     *
     * @param array $attributes
     * @return array|mixed
     */
    public static function find(array $attributes, $status = null)
    {
        $group = $attributes['group'];
        
        $date_start = new \DateTime($attributes['date_start'] . ' 00:00:00');
        $date_end = new \DateTime($attributes['date_end'] . ' 23:59:59');
        
        $query = Order::find()
            ->joinWith('orderProducts')
            ->andFilterWhere(['>=', Order::tableName(). '.created_at', $date_start->getTimestamp()])
            ->andFilterWhere(['<=', Order::tableName(). '.created_at', $date_end->getTimestamp()])
            ->andFilterWhere([
                'type_id'    => $attributes['type'],
                'partner_id' => $attributes['partner'],
                'order_product.product_id' => $attributes['product'],
            ])
            ->orderBy(['created_at' => SORT_ASC]);
        
        // Для данных, содержащих статусы: Reject, Approved, Trash и т.д.
        if ($status && array_key_exists($status, Order::getStatusesCollection())) {
            $query->andFilterWhere([Order::tableName(). '.status' => $status]);
        }
        
        // Для группировки по стране доп.фильтр по доступным странам
        $queryOrdersCountry= clone $query;
        
        // Обычные заказы
        $orders = $query
            ->andFilterWhere([
                'country_id' => \Yii::$app->user->country->id,
            ])
            ->all();
        
        // группировка по стране
        if ($group == self::GROUP_COUNTRY) {
            return static::findByCountry($queryOrdersCountry);
        }
        // группировка по товару
        elseif ($group == self::GROUP_PRODUCT) {
            return static::findByProduct($orders);
        }
        else {
            return static::findByDefault($orders, $group, $date_start, $date_end);
        }
        
    }
    
}
<?php
    
namespace backend\modules\stats\models\filter;


use common\modules\order\models\Order;
use common\components\db\ActiveQuery;
use common\models\{
    Country, Product
};

/**
 * Class OrderReject, для вкладки "Статистика отказов"
 *
 * @package modules\stats\models
 */
class OrderReject extends OrderFilter
{
    /**
     * Подготовит данные при группировке по СТРАНЕ
     *
     * @param ActiveQuery $queryOrdersCountry
     * @return array
     */
    protected static function findByCountry(ActiveQuery $queryOrdersCountry)
    {
        $result = [];
        // доступные страны
        $countries = Country::getAllowCountries();
        
        // заказы только для доступных стран
        $ordersCountry = $queryOrdersCountry
            ->andFilterWhere(['in', 'country_id', array_keys($countries)])
            ->all();
    
        // группируем по стране
        foreach ($countries as $countryId => $countryName) {
            if (!array_key_exists($countryName, $result)) {
                $result[ $countryName ] = [
                    'time' => $countryName,
                    'total' => 0,
                    'average-price' => 0,
                    'total-price' => 0,
                ];
            
                foreach (Order::getRejectReasonCollection() as $i => $text) {
                    $result[ $countryName ]['reject-reason-'.$i] = 0;
                    $result[ $countryName ]['reject-reason-'.$i.'-percent'] = 0;
                }
            }
        
            foreach ($ordersCountry as $order) {
                if ($order->country_id != $countryId) {
                    continue;
                }
            
                $result[ $countryName ]['total']++;
                if (isset($result[ $countryName ]['reject-reason-'.$order->sub_status])){
                    $result[ $countryName ]['reject-reason-'.$order->sub_status]++;
                }
            }
        }
        
        return $result;
    }
    
    /**
     * Подготовит данные при группировке по Продукту
     *
     * @param $orders
     * @return array
     */
    protected static function findByProduct($orders)
    {
        $result = [];
    
        foreach (Product::find()->collection() as $product_id => $product_name) {
            if (!isset($result[$product_id])){
                $item = [
                    'time' => $product_name,
                    'total' => 0,
                    'average-price' => 0,
                    'total-price' => 0,
                ];
                foreach (Order::getRejectReasonCollection() as $i => $text) {
                    $item['reject-reason-'.$i] = 0;
                    $item['reject-reason-'.$i.'-percent'] = 0;
                }
                $result[$product_id] = $item;
            }
        
            foreach ($orders as $order) {
                if (!in_array($product_id, array_map(function($op){ return $op->product_id; }, $order->orderProducts))){
                    continue;
                }
            
                $result[$product_id]['total']++;
                if (isset($result[$product_id]['reject-reason-'.$order->sub_status])){
                    $result[$product_id]['reject-reason-'.$order->sub_status]++;
                }
            }
        }
        
        return $result;
        
    }
    
    /**
     * Подготовит данные при группировке по Часу, Дню, Месяце
     *
     * @param $orders
     * @param $group
     * @param $date_start
     * @param $date_end
     * @return mixed
     */
    protected static function findByDefault($orders, $group, $date_start, $date_end)
    {
        $result = [];
        $date_formats = [
            self::GROUP_HOUR => 'Y-m-d H:00',
            self::GROUP_DAY => 'Y-m-d',
            self::GROUP_MONTH => 'Y-m',
        ];
    
        $date_intervals = [
            self::GROUP_HOUR => '+1hour',
            self::GROUP_DAY => '+1day',
            self::GROUP_MONTH => '+1month',
        ];
    
        //заполнение пустых дат
        $date = new \DateTime($date_start->format('Y-m-d H:i:s'));
        while ($date <= $date_end) {
            $time = $date->format($date_formats[ $group ]);
        
            $item = [
                'time' => $time,
                'total' => 0,
                'average-price' => 0,
                'total-price' => 0,
            ];
            foreach (Order::getRejectReasonCollection() as $i => $text) {
                $item['reject-reason-'.$i] = 0;
                $item['reject-reason-'.$i.'-percent'] = 0;
            }
            $result[$time] = $item;
        
            $date->modify($date_intervals[ $group ]);
        }
    
        //подсчет общего количества и количества по статусам
        foreach ($orders as $order) {
            $date = (new \DateTime(date('Y-m-d H:i:s', $order->created_at)))->format($date_formats[ $group ]);
            $result[$date]['total']++;
            if (isset($result[$date]['reject-reason-'.$order->sub_status])){
                $result[$date]['reject-reason-'.$order->sub_status]++;
            }
        }
        
        return $result;
        
    }
    
}
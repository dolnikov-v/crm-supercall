<?php
    
namespace backend\modules\stats\models\filter;


use common\modules\order\models\Order;
use common\components\db\ActiveQuery;
use common\models\{
    Country, Product
};

/**
 * Class OrderApproved, для вкладки "Статистика одобренных"
 * "
 * @package modules\stats\models
 */
class OrderApproved extends OrderFilter
{
    /**
     * Подготовит данные при группировке по СТРАНЕ
     *
     * @param ActiveQuery $queryOrdersCountry
     * @return array
     */
    protected static function findByCountry(ActiveQuery $queryOrdersCountry)
    {
        $result = [];
        // доступные страны
        $countries = Country::getAllowCountries();
    
        // заказы только для доступных стран
        $ordersCountry = $queryOrdersCountry
            ->andFilterWhere(['in', 'country_id', array_keys($countries)])
            ->all();
    
        // группируем по стране
        foreach ($countries as $countryId => $countryName) {
            if (!array_key_exists($countryName, $result)) {
                $result[ $countryName ] = [
                    'time' => $countryName,
                    'total' => 0,
                    'in-progress' => 0,
                    'buyout' => 0,
                    'no-buyout' => 0
                ];
            }
        
            foreach ($ordersCountry as $order) {
                if ($order->country_id != $countryId) {
                    continue;
                }
            
                $result[ $countryName ]['in-progress']++;
                if ($order->sub_status == Order::SUB_STATUS_BUYOUT){
                    $result[ $countryName ]['buyout']++;
                }
                if ($order->sub_status == Order::SUB_STATUS_NO_BUYOUT){
                    $result[ $countryName ]['no-buyout']++;
                }
            
                $result[ $countryName ]['total']++;
            }
        }
        
        return $result;
    }
    
    /**
     * Подготовит данные при группировке по Продукту
     *
     * @param $orders
     * @return array
     */
    protected static function findByProduct($orders)
    {
        $result = [];
    
        foreach (Product::find()->collection() as $product_id => $product_name) {
            if (!isset($result[$product_id])){
                $result[$product_id] = [
                    'time' => $product_name,
                    'total' => 0,
                    'in-progress' => 0,
                    'buyout' => 0,
                    'no-buyout' => 0
                ];
            }
        
            foreach ($orders as $order) {
                if (!in_array($product_id, array_map(function($op){ return $op->product_id; }, $order->orderProducts))){
                    continue;
                }
            
                $result[$product_id]['in-progress']++;
                if ($order->sub_status == Order::SUB_STATUS_BUYOUT){
                    $result[$product_id]['buyout']++;
                }
                if ($order->sub_status == Order::SUB_STATUS_NO_BUYOUT){
                    $result[$product_id]['no-buyout']++;
                }
            
                $result[$product_id]['total']++;
            }
        }
        
        return $result;
        
    }
    
    /**
     * Подготовит данные при группировке по Часу, Дню, Месяце
     *
     * @param $orders
     * @param $group
     * @param $date_start
     * @param $date_end
     * @return mixed
     */
    protected static function findByDefault($orders, $group, $date_start, $date_end)
    {
        $result = [];
        $date_formats = [
            self::GROUP_HOUR => 'Y-m-d H:00',
            self::GROUP_DAY => 'Y-m-d',
            self::GROUP_MONTH => 'Y-m',
        ];
    
        $date_intervals = [
            self::GROUP_HOUR => '+1hour',
            self::GROUP_DAY => '+1day',
            self::GROUP_MONTH => '+1month',
        ];
    
        //заполнение пустых дат
        $date = new \DateTime($date_start->format('Y-m-d H:i:s'));
        while ($date <= $date_end) {
            $time = $date->format($date_formats[ $group ]);
        
            $item = [
                'time' => $time,
                'total' => 0,
                'average-price' => 0,
                'total-price' => 0,
                'in-progress' => 0,
                'in-progress-percent' => 0,
                'buyout' => 0,
                'buyout-percent' => 0,
                'no-buyout' => 0,
                'no-buyout-percent' => 0,
            ];
            foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                $item[$status_id] = 0;
                $item[$status_id.'-percent'] = 0;
            }
            $result[$time] = $item;
        
            $date->modify($date_intervals[ $group ]);
        }
    
        //подсчет общего количества и количества по статусам
        foreach ($orders as $order) {
            $date = (new \DateTime(date('Y-m-d H:i:s', $order->created_at)))->format($date_formats[ $group ]);
        
            $result[$date]['total']++;
            $result[$date]['in-progress']++;
            if ($order->sub_status == Order::SUB_STATUS_BUYOUT){
                $result[$date]['buyout']++;
            }
            if ($order->sub_status == Order::SUB_STATUS_NO_BUYOUT){
                $result[$date]['no-buyout']++;
            }
        }
        
        return $result;
        
    }
    
}
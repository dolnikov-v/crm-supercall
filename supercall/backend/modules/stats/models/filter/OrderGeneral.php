<?php
    
namespace backend\modules\stats\models\filter;


use common\modules\order\models\Order;
use common\components\db\ActiveQuery;
use common\models\Country;
use common\models\Product;
/**
 * FIXME: удалить после успешного внедрения OLAP
 * Class OrderGeneral, для вкладки "Статистика по заказам"
 * "
 * @package modules\stats\models
 */
class OrderGeneral extends OrderFilter
{
    /**
     * Подготовит данные при группировке по СТРАНЕ
     *
     * @param ActiveQuery $queryOrdersCountry
     * @return array
     */
    protected static function findByCountry(ActiveQuery $queryOrdersCountry)
    {
        $result = [];
        // доступные страны
        $countries = Country::getAllowCountries();
        
        // заказы только для доступных стран
        $ordersCountry = $queryOrdersCountry
            ->andFilterWhere(['in', 'country_id', array_keys($countries)])
            ->all();
        
        // группируем по стране
        foreach ($countries as $countryId => $countryName) {
            if (!array_key_exists($countryName, $result)) {
                $result[ $countryName ] = [
                    'time'          => $countryName,
                    'total'         => 0,
                    'total-price'   => 0,
                    'average-price' => 0,
                    'ids'           => [],
                ];
            }
            
            foreach ($ordersCountry as $order) {
                if ($order->country_id != $countryId) {
                    continue;
                }
                // переберем все статусы
                foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                    if (!array_key_exists($status_id, $result[ $countryName ])) {
                        $result[ $countryName ][$status_id] = 0;
                        $result[ $countryName ][$status_id.'-ids'] = [];
                    }
                    
                    if ($order->status != $status_id){
                        continue;
                    }
                    
                    if (in_array($order->id, $result[ $countryName ][$status_id.'-ids'])){
                        continue;
                    }
                    
                    $result[ $countryName ][$status_id]++;
                    $result[ $countryName ][$status_id.'-ids'][] = $order->id;
                    
                    if (!in_array($order->id, $result[ $countryName ]['ids'])){
                        $result[ $countryName ]['total']++;
                        $result[ $countryName ]['ids'][] = $order->id;
                        $result[ $countryName ]['total-price'] += $order->final_price;
                    }
                }
            }
        }

        return $result;
    }
    
    /**
     * Подготовит данные при группировке по Продукту
     *
     * @param $orders
     * @return array
     */
    protected static function findByProduct($orders)
    {
        $result = [];
        foreach (Product::find()->collection() as $product_id => $product_name) {
            if (!isset($result[$product_id])){
                $result[$product_id] = [
                    'time' => $product_name,
                    'total' => 0,
                    'total-price' => 0,
                    'average-price' => 0,
                    'ids' => []
                ];
            }
            
            foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                if (!isset($result[$product_id][$status_id])){
                    $result[$product_id][$status_id] = 0;
                    $result[$product_id][$status_id.'-ids'] = [];
                }
                
                foreach ($orders as $order) {
                    if ($order->status != $status_id){
                        continue;
                    }
                    
                    if (!in_array($product_id, array_map(function($op){ return $op->product_id; }, $order->orderProducts))){
                        continue;
                    }
                    
                    if (in_array($order->id, $result[$product_id][$status_id.'-ids'])){
                        continue;
                    }
                    
                    $result[$product_id][$status_id]++;
                    $result[$product_id][$status_id.'-ids'][] = $order->id;
                    
                    if (!in_array($order->id, $result[$product_id]['ids'])){
                        $result[$product_id]['total']++;
                        $result[$product_id]['ids'][] = $order->id;
                        $result[$product_id]['total-price'] += $order->final_price;
                    }
                }
            }
        }
        
        return $result;
        
    }
    
    /**
     * Подготовит данные при группировке по Часу, Дню, Месяце
     *
     * @param $orders
     * @param $group
     * @param $date_start
     * @param $date_end
     * @return mixed
     */
    protected static function findByDefault($orders, $group, $date_start, $date_end)
    {
        $result = [];
        $date_formats = [
            self::GROUP_HOUR => 'Y-m-d H:00',
            self::GROUP_DAY => 'Y-m-d',
            self::GROUP_MONTH => 'Y-m',
        ];
        
        $date_intervals = [
            self::GROUP_HOUR => '+1hour',
            self::GROUP_DAY => '+1day',
            self::GROUP_MONTH => '+1month',
        ];
        
        //подсчет общего количества и количества по статусам
        foreach ($orders as $order) {
            $date = (new \DateTime(date('Y-m-d H:i:s', $order->created_at)))->format($date_formats[ $group ]);
            
            if (!isset($result[$date])){
                $result[$date] = [
                    'time' => $date,
                    'total' => 0,
                    'average-price' => 0,
                    'total-price' => 0,
                    'ids' => []
                ];
            }
            
            $result[$date]['total']++;
            
            foreach (Order::getStatusesCollection() as $status_id => $status_label) {
                if ($order->status != $status_id){
                    continue;
                }
                if (!isset($result[$date][$status_id])){
                    $result[$date][$status_id] = 0;
                }
                $result[$date][$status_id]++;
            }
            
            if (!in_array($order->id, $result[$date]['ids'])){
                $result[$date]['ids'][] = $order->id;
                $result[$date]['total-price'] += $order->final_price;
            }
        }
        
        //заполнение пустых дат
        $date = new \DateTime($date_start->format('Y-m-d H:i:s'));
        while ($date <= $date_end) {
            $time = $date->format($date_formats[ $group ]);
            
            if (!isset($result[$time])){
                // UN-40 пустые строки удаляем из статистики
                unset($result[$time]);
            }
            
            $date->modify($date_intervals[ $group ]);
        }
        
        return $result;
        
    }
    
}
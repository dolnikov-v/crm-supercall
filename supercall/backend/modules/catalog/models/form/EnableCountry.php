<?php

namespace backend\modules\catalog\models\form;

use common\models\Country;
use common\models\Language;
use Yii;
use yii\base\Model;

class EnableCountry extends Model
{
    public $country;
    public $language;

    protected $countryModel;
    protected $languageModel;

    public function attributeLabels()
    {
        return [
            'country' => Yii::t('common', 'Страна'),
            'language' => Yii::t('common', 'Язык'),
        ];
    }

    public function rules()
    {
        return [
            [['country', 'language'], 'required'],
            ['country', 'validateCountry'],
            ['language', 'validateLanguage'],
        ];
    }

    /**
     * @return Country
     */
    public function getCountryModel()
    {
        if (!$this->countryModel){
            $this->countryModel = Country::findOne($this->country);
        }
        return $this->countryModel;
    }

    /**
     * @return Language
     */
    public function getLanguageModel()
    {
        if (!$this->languageModel){
            $this->languageModel = Language::findOne($this->language);
        }
        return $this->languageModel;
    }

    public function validateCountry()
    {
        if (!$this->getCountryModel()){
            $this->addError('language', Yii::t('common', 'Страна не найдена'));
            return;
        }
        if (!$this->getCountryModel()->char_code){
            $this->addError('language', Yii::t('common', 'У страны не задан char_code'));
        }
    }

    public function validateLanguage()
    {
        if (!$this->getLanguageModel()){
            $this->addError('language', Yii::t('common', 'Язык не найден'));
            return;
        }
        if (!$this->getLanguageModel()->locale){
            $this->addError('language', Yii::t('common', 'У языка не задана локаль'));
        }
        if (!$this->getLanguageModel()->genesys_name){
            $this->addError('language', Yii::t('common', 'У язык не задан genesys_name'));
        }
        if ($this->getCountryModel()->language_id && $this->getCountryModel()->language_id != $this->getLanguageModel()->id){
            $this->addError('language', Yii::t('common', 'У страны уже выбран другой язык'));
        }
    }

    public function enable()
    {
        $country = $this->getCountryModel();
        $language = $this->getLanguageModel();

        $country->language_id = $language->id;
        $country->save(false);

        return true;
    }
}
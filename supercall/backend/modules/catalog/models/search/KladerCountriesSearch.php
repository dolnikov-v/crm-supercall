<?php
namespace backend\modules\catalog\models\search;

use backend\modules\catalog\models\KladerCountries;
use yii\data\ActiveDataProvider;

/**
 * Class KladerCountriesSearch
 * @package backend\modules\catalog\models\search
 */
class KladerCountriesSearch extends KladerCountries
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_ASC;
        }

        $query = KladerCountries::find()
            ->with(['country'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}

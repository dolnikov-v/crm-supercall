<?php

namespace backend\modules\catalog\models\search;

use backend\modules\catalog\models\Source;
use yii\data\ActiveDataProvider;

/**
 * Class SourceSearch
 * @package backend\modules\catalog\models\search
 */
class SourceSearch extends Source
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['active'] = SORT_DESC;
            $sort['id'] = SORT_ASC;
        }

        $query = Source::find()
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params)) {
            return $dataProvider;
        }

        $query->andFilterWhere(['name' => $this->name]);
        $query->andFilterWhere(['code' => $this->code]);

        return $dataProvider;
    }
}
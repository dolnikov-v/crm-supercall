<?php
namespace backend\modules\catalog\models\search;

use backend\modules\catalog\models\ScriptsOperators;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\User;

class ScriptsOperatorsSearch extends ScriptsOperators
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['partner_id', 'country_id', 'product_id', 'active'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort[ScriptsOperators::tableName().'.id'] = SORT_DESC;
        }

        $query = ScriptsOperators::find()
            ->joinWith(['partner', 'country', 'product'])
            ->andWhere(['country_id' =>  ArrayHelper::getColumn(User::getAllowCountries()->all(), 'country_id')])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([ScriptsOperators::tableName().'.partner_id' => $this->partner_id]);
        $query->andFilterWhere([ScriptsOperators::tableName().'.country_id'=> $this->country_id]);
        $query->andFilterWhere([ScriptsOperators::tableName().'.product_id'=> $this->product_id]);
        $query->andFilterWhere([ScriptsOperators::tableName().'.active'=> $this->active]);

        return $dataProvider;
    }
}
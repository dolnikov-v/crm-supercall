<?php
namespace backend\modules\catalog\models\search;

use backend\modules\catalog\models\PriceShipping;
use yii\data\ActiveDataProvider;

/**
 * Class PriceShippingSearch
 * @package backend\modules\catalog\models\search
 */
class PriceShippingSearch extends PriceShipping
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['partner_id', 'country_id', 'shipping_id'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort[PriceShipping::tableName().'.id'] = SORT_DESC;
        }

        $query = PriceShipping::find()
            ->joinWith(['partner', 'country', 'shipping'])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params)) {
            return $dataProvider;
        }

        $query->andFilterWhere([PriceShipping::tableName().'.partner_id' => $this->partner_id]);
        $query->andFilterWhere([PriceShipping::tableName().'.country_id'=> $this->country_id]);
        $query->andFilterWhere([PriceShipping::tableName().'.shipping_id'=> $this->shipping_id]);

        return $dataProvider;
    }
}

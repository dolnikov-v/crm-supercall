<?php

namespace backend\modules\catalog\models;

use common\components\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "source".
 *
 * @property integer $id
 * @property integer $code
 * @property string $name
 * @property boolean $active
 */
class Source extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%source}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['code'], 'unique'],
            [['code'], 'integer'],
            [['active'], 'boolean'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'code' => Yii::t('common', 'Код'),
            'name' => Yii::t('common', 'Название'),
            'active' => Yii::t('common', 'Доступность')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourcePartnerCountries()
    {
        return $this->hasMany(SourcePartnerCountry::className(), ['source_id' => 'id']);
    }
}

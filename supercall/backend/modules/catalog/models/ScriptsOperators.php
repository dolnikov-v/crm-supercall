<?php
namespace backend\modules\catalog\models;

use common\components\db\ActiveRecord;
use backend\modules\catalog\models\query\ScriptsOperatorsQuery;
use common\modules\partner\models\Partner;
use common\models\Country;
use common\models\Product;
use common\models\User;
use Yii;

/**
 * Class ScriptsOperators
 * @package backend\modules\catalog\models
 */
class ScriptsOperators extends ActiveRecord
{
    const ACTIVE = true;
    const NOT_ACTIVE = false;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%scripts_operators}}';
    }

    /**
     * @return ScriptsOperatorsQuery
     */
    public static function find()
    {
        return new ScriptsOperatorsQuery(get_called_class());
    }

    public function rules()
    {
        return [
            [['country_id', 'partner_id', 'product_id', 'created_user_id'], 'required'],
            [['country_id', 'partner_id', 'product_id', 'created_user_id', 'updated_user_id', 'created_at', 'updated_at'], 'integer'],
            ['active', 'boolean'],
            ['content', 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('common', 'Страна'),
            'partner_id' => Yii::t('common', 'Партнёр'),
            'product_id' => Yii::t('common', 'Продукт'),
            'active' => Yii::t('common', 'Активность'),
            'content' => yii::t('common', 'Содержание'),
            'created_user_id' => Yii::t('common', 'Создан пользователем'),
            'updated_user_id' => Yii::t('common', 'Обновлен пользователем'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdate()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }


}
<?php

namespace backend\modules\catalog\models;

use common\components\db\ActiveRecord;
use common\models\Country;
use common\modules\partner\models\Partner;
use Yii;

/**
 * This is the model class for table "source_partner_country".
 *
 * @property integer $id
 * @property integer $source_id
 * @property integer $country_id
 * @property integer $partner_id
 *
 * @property Partner $partner
 * @property Source $country
 */
class SourcePartnerCountry extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%source_partner_country}}';
    }


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['source_id', 'country_id', 'partner_id'], 'required'],
            [['source_id', 'country_id', 'partner_id'], 'integer'],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partner::className(), 'targetAttribute' => ['partner_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['source_id'], 'exist', 'skipOnError' => true, 'targetClass' => Source::className(), 'targetAttribute' => ['source_id' => 'id']]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_id' => 'Source ID',
            'country_id' => Yii::t('common', 'Страны'),
            'partner_id' => Yii::t('common', 'Партнер'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasMany(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::className(), ['id' => 'source_id']);
    }
}

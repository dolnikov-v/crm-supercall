<?php
namespace backend\modules\catalog\models\query;

use backend\modules\catalog\models\ScriptsOperators;
use common\components\db\ActiveQuery;

/**
 * Class ScriptsOperatorsQuery
 * @package backend\modules\catalog\models\query
 */
class ScriptsOperatorsQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function getActive()
    {
        return $this->andWhere(['active' => ScriptsOperators::ACTIVE]);
    }

    /**
     * @param $country_id
     * @return $this
     */
    public function byCountry($country_id){
        return $this->andWhere(['country_id' => $country_id]);
    }

    /**
     * @param $partner_id
     * @return $this
     */
    public function byPartner($partner_id){
        return $this->andWhere(['partner_id' => $partner_id]);
    }

    /**
     * @param $product_id
     * @return $this
     */
    public function byProduct($product_id){
        return $this->andWhere(['product_id' => $product_id]);
    }
}
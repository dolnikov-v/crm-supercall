<?php
namespace backend\modules\catalog\models\query;

use common\components\db\ActiveQuery;
use backend\modules\catalog\models\KladerCountries;
use backend\modules\catalog\components\MaterializedPathQueryTrait;

/**
 * Class KladerCountriesQuery
 * @package backend\modules\catalog\models\query
 * @method KladerCountries one($db = null)
 * @method KladerCountries [] all($db = null)
 */
class KladerCountriesQuery extends ActiveQuery
{
    use MaterializedPathQueryTrait;
}

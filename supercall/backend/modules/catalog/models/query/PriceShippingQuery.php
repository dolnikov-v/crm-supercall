<?php
namespace backend\modules\catalog\models\query;

use common\components\db\ActiveQuery;
use backend\modules\catalog\models\PriceShipping;

/**
 * Class PriceShippingQuery
 * @package common\modules\partner\models\query
 * @method PriceShipping one($db = null)
 * @method PriceShipping [] all($db = null)
 */
class PriceShippingQuery extends ActiveQuery
{

}

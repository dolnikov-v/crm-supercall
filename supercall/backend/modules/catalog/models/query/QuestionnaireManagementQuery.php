<?php
namespace backend\modules\catalog\models\query;

use common\components\db\ActiveQuery;
use backend\modules\operator\models\QuestionnaireManagement;

/**
 * Class QuestionnaireManagementQuery
 * @package cobackend\modules\catalog\models\query
 * @method QuestionnaireManagement one($db = null)
 * @method QuestionnaireManagement [] all($db = null)
 */
class QuestionnaireQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionValue = 'id';
}

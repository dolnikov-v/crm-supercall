<?php

namespace backend\modules\catalog\models;

use backend\modules\catalog\controllers\KladerCountriesController;
use common\components\db\ActiveRecord;
use backend\modules\catalog\models\query\KladerCountriesQuery;
use backend\modules\catalog\components\MaterializedPathBehavior;
use common\models\Country;
use common\modules\order\models\Order;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerFormAttribute;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class KladerCountries
 * @package backend\modules\catalog\models
 * @property integer $id
 * @property string $path
 * @property integer $depth
 * @property integer $sort
 * @property string $name
 * @property string $country_id
 * @property integer $partner_form_id
 * @property integer $partner_form_attribute_id
 */
class KladerCountries extends ActiveRecord
{
    /**
     * @var []
     */
    public $countries;

    /**
     * @var KladerCountries
     */
    public $root;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => MaterializedPathBehavior::className(),
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%klader_countries}}';
    }

    /**
     * @return KladerCountriesQuery
     */
    public static function find()
    {
        return new KladerCountriesQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'path'], 'string', 'max' => 255],
            [['country_id', 'depth', 'partner_form_id', 'partner_form_attribute_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'path' => Yii::t('common', 'Путь'),
            'depth' => Yii::t('common', 'Вложенность'),
            'sort' => Yii::t('common', 'Сортировка'),
            'name' => Yii::t('common', 'Значение'),
            'partner_form_id' => Yii::t('common', 'Форма'),
            'partner_form_attribute_id' => Yii::t('common', 'Атрибут'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * Автозаполнение КЛАДРа стран по формам заказа
     * @param Order|StdClass
     */
    public static function addTreeToKladr($data)
    {
        $controller = new KladerCountriesController('klader-countries', yii::$app->getModule('catalog'));

        /** @var Order $data */
        $form = PartnerForm::find()
            ->where(['country_id' => $data->country_id])
            ->andWhere(['partner_id' => $data->partner_id])
            ->andWhere(['active' => PartnerForm::ACTIVE])
            ->one();

        if (is_null($form)) {
            return;
        }

        $rootGeneral = KladerCountries::find()
            ->where(['country_id' => $data->country_id])
            ->andWhere(['partner_form_id' => $form->id])
            ->one();

        //если для данной страны и партнёра в КЛАДР не создан корневой каталог
        if (is_null($rootGeneral)) {
            $getDataForRoot = Country::find()->where(['id' => $data->country_id])->one();

            //нужна страна заказа
            if (!is_null($getDataForRoot)) {
                //создали корневой каталог в КЛАДР
                $rootGeneral = $controller->createRoot($getDataForRoot->id, $getDataForRoot->name, $getDataForRoot->id, $form->id);

            } //не определили страну - создавать ничего не будем
            else {
                return;
            }
        }

        //далее необходимо сохранить все дерево из формы заказа - по адресу (поля  с is_kladr с partner_form_attributes)
        //получить список полей формы
        $attributes = PartnerFormAttribute::find()
            ->where(['form_id' => $form->id])
            ->andWhere(['is_klader' => true])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        //если у данной формы нет атрибутов с признаком is_klader = true - КЛАДР обновлять нет смысла
        if (is_null($attributes)) {
            return;
        }

        $components = json_decode($data->customer_components);
        $address = (array)$components->address;
        $values = [];

        foreach ($attributes as $key => $attribute) {
            foreach ($address as $property => $value) {
                //для первого элемента - корневой каталог это $root (от страны), для последующих - предыдущий элемент
                if ($attribute->name == $property) {
                    if (empty($value)) {
                        return;
                    }

                    $values[$key] = $value;
                    if ($key == 0 && $rootGeneral != false) {
                        $childs = $rootGeneral->getChildren()->where(['partner_form_attribute_id' => $attribute->id])->all();
                        $childs_collection = ArrayHelper::getColumn($childs, 'name');
                        $valueIsChild = in_array($value, $childs_collection);
                        //значение не является наследником родителя
                        if (!$valueIsChild) {
                            if (!KladerCountries::find()->where([
                                'name' => $value,
                                'partner_form_attribute_id' => $attribute->id
                            ])->exists()) {
                                $root = $controller->addPath($rootGeneral, $value, $form->id, $attribute->id);
                            }

                        } else {
                            //или определим его
                            foreach ($childs as $child) {
                                if ($value == $child->name) {
                                    $root = $child;
                                }
                            }
                        }
                    } else {
                        //добавим в цикле чилдов парентам
                        if (isset($root)) {
                            if (!KladerCountries::find()->where([
                                'name' => $value,
                                'partner_form_attribute_id' => $attribute->id
                            ])->exists()) {
                                $root = $controller->addPath($root, $value, $form->id, $attribute->id);
                            }
                        }
                    }
                }
            }
        }
    }
}
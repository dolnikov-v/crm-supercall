<?php

namespace backend\modules\catalog\models;

use common\components\db\ActiveRecord;
use backend\modules\catalog\models\query\PriceShippingQuery;
use common\models\Country;
use common\models\Shipping;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerProduct;
use Yii;

class PriceShipping extends ActiveRecord
{
    //for tpl param
    public $_name_;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%price_shipping}}';
    }

    /**
     * @return PriceShippingQuery
     */
    public static function find()
    {
        return new PriceShippingQuery(get_called_class());
    }

    public function rules()
    {
        return [
            [['country_id', 'partner_id', 'shipping_id', 'price_shipping'], 'required'],
            [['country_id', 'partner_id', 'shipping_id'], 'integer'],
            ['param_shipping', 'string'],
            ['price_shipping', 'double']
        ];
    }

    public function attributeLabels()
    {
        return [
            'partner_id' => Yii::t('common', 'Партнёр'),
            'country_id' => Yii::t('common', 'Страна'),
            'shipping_id' => Yii::t('common', 'Служба доставки'),
            'param_shipping' => Yii::t('common', 'Область доставки'),
            'price_shipping' => Yii::t('common', 'Стоимость'),
            'created_at' => Yii::t('common', 'Создана'),
            'updated_at' => Yii::t('common', 'Обновлена')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipping()
    {
        return $this->hasOne(Shipping::className(), ['id' => 'shipping_id']);
    }

    /**
     * @param $insert
     * @return bool
     */
    public function _beforeSave($insert)
    {
        $this->price_shipping = floatval($this->price_shipping);

        return parent::beforeSave($insert);
    }

    public function beforeValidate()
    {
        $this->param_shipping = is_array($this->param_shipping) ? json_encode($this->param_shipping, JSON_UNESCAPED_UNICODE) : $this->param_shipping;
        parent::beforeValidate();
    }

    public static function getPriceShipping($partner_id, $country_id)
    {
        return PriceShipping::findAll([
            'partner_id' => $partner_id,
            'country_id' => $country_id
        ]);
    }
}
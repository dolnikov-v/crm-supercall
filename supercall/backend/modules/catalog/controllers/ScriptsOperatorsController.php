<?php

namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use backend\modules\catalog\models\ScriptsOperators;
use backend\modules\catalog\models\search\ScriptsOperatorsSearch;
use common\models\Country;
use common\models\Product;
use Yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\helpers\Url;
use yii\filters\AccessControl;

/**
 * Class ScriptsOperatorsController
 * @package backend\modules\catalog\controllers
 */
class ScriptsOperatorsController extends Controller
{
    public function beforeAction($action)
    {
        if ($action->id === 'get-scripts') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ScriptsOperatorsSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
        $countries = [];

        foreach (yii::$app->user->countries as $country) {
            $countries[$country->id] = Yii::t('common', $country->name);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'countries' => $countries
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new ScriptsOperators();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Скрипт для оператора успешно добавлен.') : Yii::t('common', 'Скрипт для оператора успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $data['model'] = $model;
        $data['countries'] = [];

        foreach (yii::$app->user->countries as $country) {
            $data['countries'][$country->id] = Yii::t('common', $country->name);
        }


        return $this->render('edit', $data);
    }

    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Скрипт для оператора успешно удалён.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return bool|\yii\web\Response
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $activeScripts = $this->getActiveScripts($model->country_id, $model->partner_id, $model->product_id);

        if (count($activeScripts) == 0) {

            $model->active = ScriptsOperators::ACTIVE;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier(Yii::t('common', 'Скрипт для оператора успешно активирован.'), 'success');
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Для данного товара и партнёра в указанной стране уже есть активный скрипт. Для активации текущего скрипта, необходимо, деактивировать его.'));
            return $this->redirect(Url::toRoute(['index',
                'ScriptsOperatorsSearch[country_id]' => $model->country_id,
                'ScriptsOperatorsSearch[partner_id]' => $model->partner_id,
                'ScriptsOperatorsSearch[product_id]' => $model->product_id,
                'ScriptsOperatorsSearch[active]' => ''
            ]));
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return bool|\yii\web\Response
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = ScriptsOperators::NOT_ACTIVE;

        if ($model->save()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Скрипт для оператора успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return ScriptsOperators
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = ScriptsOperators::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Скрипт не найден.'));
        }

        return $model;
    }

    public static function getAllowedScriptsOperators()
    {
        $countries_id = ArrayHelper::getColumn(User::getAllowCountries()->all(), 'country_id');

        return ScriptsOperators::find()->where(['country_id' => $countries_id])->orderBy([ScriptsOperators::tableName() . '.active' => SORT_DESC]);
    }

    /**
     * @param null $country_id
     * @param null $partner_id
     * @param null $product_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getActiveScripts($country_id = null, $partner_id = null, $product_id = null)
    {
        $activeScripts = ScriptsOperators::find()->getActive();

        if ($country_id) {
            $activeScripts->byCountry($country_id);
        }

        if ($partner_id) {
            $activeScripts->byPartner($partner_id);
        }

        if ($product_id) {
            $activeScripts->byProduct($product_id);
        }

        return $activeScripts->all();
    }

    /**
     * @return string
     */
    public function actionGetScripts()
    {
        header('Access-Control-Allow-Origin: *');
        $product_ids = yii::$app->request->post('product_ids');
        $country_id = yii::$app->request->post('country_id');
        $partner_id = yii::$app->request->post('partner_id');

        $scripts = ScriptsOperators::find()
            ->joinWith('product')
            ->where([
                'country_id' => $country_id,
                'partner_id' => $partner_id,
                'product_id' => $product_ids,
                ScriptsOperators::tableName() . '.active' => ScriptsOperators::ACTIVE
            ])
            ->asArray()
            ->all();

        return json_encode($scripts, JSON_UNESCAPED_UNICODE);
    }
}
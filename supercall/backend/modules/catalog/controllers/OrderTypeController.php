<?php
namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use common\modules\order\models\OrderType;
use common\modules\order\models\search\OrderTypeSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class OrderTypeController
 * @package backend\modules\catalog\controllers
 */
class OrderTypeController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new OrderTypeSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new OrderType();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Тип заказа успешно добавлен.') : Yii::t('common', 'Тип заказа успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Тип заказа успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return OrderType
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = OrderType::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Тип заказа не найден.'));
        }

        return $model;
    }
}

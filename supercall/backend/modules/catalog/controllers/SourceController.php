<?php

namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use backend\modules\catalog\models\search\SourceSearch;
use backend\modules\catalog\models\Source;
use backend\modules\catalog\models\SourcePartnerCountry;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class SourceController
 * @package backend\modules\catalog\controllers
 */
class SourceController extends Controller
{
    /**
     * @return array
     */
    public function beforeActionIndex()
    {
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);

        return ['query' => $query];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $data = $this->beforeActionIndex();
        $modelSearch = new SourceSearch();
        $dataProvider = $modelSearch->search($data['query']);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Source();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common',
                    'Источник успешно добавлен.') : Yii::t('common', 'Источник успешно отредактирован.'), 'success');

                SourcePartnerCountry::deleteAll(['source_id' => $model->id]);

                $post = Yii::$app->request->post();

                foreach ($post['partner'] as $key => $partner) {
                    if (isset($post['country'][$key])) {
                        foreach ($post['country'][$key] as $country) {
                            $sourcePartnerCountryModel = new SourcePartnerCountry();
                            $sourcePartnerCountryModel->partner_id = $partner;
                            $sourcePartnerCountryModel->country_id = $country;
                            $sourcePartnerCountryModel->link('source', $model);
                        }
                    }
                }

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $data['model'] = $model;
        $data['partners'] = yii::$app->user->getPartners();
        $data['countries'] = yii::$app->user->getCountries();

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Источник успешно активирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Источник успешно деактивирован.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Source
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Source::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Источник не найден.'));
        }

        return $model;
    }
}

<?php
namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use common\models\search\TimezoneSearch;
use Yii;

/**
 * Class TimezoneController
 * @package backend\modules\catalog\controllers
 */
class TimezoneController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new TimezoneSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}

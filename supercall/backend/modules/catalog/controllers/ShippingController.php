<?php
namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use common\models\Shipping;
use common\models\search\ShippingSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class ShippingController
 * @package backend\modules\catalog\controllers
 */
class ShippingController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ShippingSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Shipping();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Курьерска служба успешно добавлена.') : Yii::t('common', 'Курьерска служба успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param integer $id
     * @return Shipping
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Shipping::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Курьерска служба не найдена.'));
        }

        return $model;
    }
}

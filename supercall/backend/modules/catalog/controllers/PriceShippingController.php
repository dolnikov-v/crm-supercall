<?php
namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use backend\modules\catalog\models\PriceShipping;
use backend\modules\catalog\models\search\PriceShippingSearch;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use common\modules\partner\models\PartnerShipping;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class PriceShippingController
 * @package backend\modules\catalog\controllers
 */
class PriceShippingController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new PriceShippingSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new PriceShipping();
        }

        if(Yii::$app->request->post()) {
            //форма уже подготовлена для мультивставки, но
            //пока рассматриваем только разовую вставку
            //пересоберём данные для разовой вставки
            $data['PriceShipping'] = [];

            foreach (Yii::$app->request->post('PriceShipping') as $k=>$value){
                if(isset($value[0])){
                    if($k == 'param_shipping'){
                        //отфильтровать пустые параметры
                        $params = [];

                        foreach($value[0] as $name=>$param){
                            if(!empty($param)){
                               $params[] = [
                                   'name' => yii::t('common', $name),
                                   'val' => $param];
                            }
                        }

                        $data['PriceShipping'][$k] = json_encode($params, JSON_UNESCAPED_UNICODE);
                    }else {
                        $data['PriceShipping'][$k] = $value[0];
                    }
                }
            }
        }

        if(isset($data)) {
            if ($model->load($data)) {

                $isNewRecord = $model->isNewRecord;

                if ($model->save(false)) {
                    Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Стоимость доставки успешно добавлена.') : Yii::t('common', 'Стоимость доставки успешно отредактирована.'), 'success');

                    return $this->redirect(Url::toRoute('index'));
                } else {
                    Yii::$app->notifier->addNotifierErrorByModel($model);
                }
            }
        }

        $data['model'] = $model;
        return $this->render('edit', $data);
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Стоимость доставки успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));


    }

    /**
     * @param integer $id
     * @return PriceShipping
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = PriceShipping::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Стоимость доставки не найдена.'));
        }

        return $model;
    }

    /**
     * @return string
     */
    public function actionGetPartnerCountries()
    {
        $partner_id = yii::$app->request->post('partner_id');

        if($partner_id == ''){
            $listCountries = [];
        }else {
            $countries = PartnerForm::getPartnerCountries($partner_id);
            $listCountries = [];

            foreach ($countries as $country) {
                $listCountries[$country->country_id] = Yii::t('common', $country->country->name);
            }

        }

        return json_encode($listCountries, JSON_UNESCAPED_UNICODE);

    }

    /**
     * @return string
     */
    public function actionGetFormAttributes()
    {
        $partner_id = yii::$app->request->post('partner_id');
        $country_id = yii::$app->request->post('country_id');

        $formQuery = PartnerForm::find()
            ->where(['active' => PartnerForm::ACTIVE])
            ->andWhere(['partner_id' => $partner_id])
            ->andWhere(['country_id' => $country_id]);

        if($formQuery->exists()){
            $form = $formQuery->one();

            $attributesData = PartnerFormAttribute::find()
                ->where(['form_id' => $form->id])
                ->andWhere(['is_klader' => true])
                ->orderBy(['sort' => SORT_ASC])
                ->all();

            $attributes = [];

            foreach($attributesData as $k=>$v){
                $attributes[] = [
                  'id' => $v['id'],
                  'name' => $v['name'],
                  'label' => $v['label'],
                  'sort' => $v['sort'],
                ];
            }
        }else{
            $attributes = [];
        }

        return json_encode($attributes, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionGetPartnerShipping()
    {
        $country_id = yii::$app->request->post('country_id');
        $partner_id = yii::$app->request->post('partner_id');

        $query = PartnerShipping::find()->where([
            'country_id' => $country_id,
        ]);

        if(!empty($partner_id)){
            $query->andWhere(['partner_id' => $partner_id]);
        }

        $shippings = [];

        if($query->exists()){
            $shippings = ArrayHelper::map($query->all(), 'shipping.id', 'shipping.name');
        }

        return json_encode($shippings, JSON_UNESCAPED_UNICODE);
    }

}
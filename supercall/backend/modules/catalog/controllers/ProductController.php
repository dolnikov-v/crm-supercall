<?php
namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use common\models\Product;
use common\models\search\ProductSearch;
use common\modules\partner\models\PartnerProduct;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class ProductController
 * @package backend\modules\catalog\controllers
 */
class ProductController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ProductSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Product();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Товар успешно добавлен.') : Yii::t('common', 'Товар успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param integer $id
     * @return Product
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Product::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Товар не найден.'));
        }

        return $model;
    }

    /**
     * @param $id
     */
    public function actionActivate($id)
    {
        Product::activate($id);
        $this->redirect(Url::toRoute(('index')));
    }

    /**
     * @param $id
     */
    public function actionDeactivate($id)
    {
        Product::deactivate($id);
        $this->redirect(Url::toRoute(('index')));
    }

    /**
     * @return string
     */
    public function actionGetProductsByPartner()
    {
        $products = PartnerProduct::getPartnerProducts((int)yii::$app->request->post('partner_id'),(int)yii::$app->request->post('country_id'));

        return Json::encode(['success' => true, 'message' => ArrayHelper::map($products, 'product.id', 'product.name')]);
    }
}

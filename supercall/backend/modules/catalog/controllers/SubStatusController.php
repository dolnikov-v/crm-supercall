<?php
namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use common\modules\order\models\OrderSubStatus;
use common\modules\order\models\OrderSubStatusGroup;
use common\modules\order\models\search\OrderSubStatusSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class SubStatusController
 * @package backend\modules\catalog\controllers
 */
class SubStatusController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new OrderSubStatusSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        $substatusesGroups = OrderSubStatusGroup::find()->all();
        $substatusesGroups = ArrayHelper::map($substatusesGroups, 'id', 'name');

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
            'groups' => $substatusesGroups,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
            $model->updated_at = time();
        } else {
            $model = new OrderSubStatus();
            $model->created_at = time();
            $model->updated_at = time();
        }

        $substatusesGroups = OrderSubStatusGroup::find()->all();
        $substatusesGroups = ArrayHelper::map($substatusesGroups, 'id', 'name');

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Субстатус успешно добавлен.') : Yii::t('common', 'Субстатус успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $data['model'] = $model;
        $data['groups'] = $substatusesGroups;

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Субстатус успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return OrderSubStatus
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = OrderSubStatus::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Субстатус не найден.'));
        }

        return $model;
    }
}

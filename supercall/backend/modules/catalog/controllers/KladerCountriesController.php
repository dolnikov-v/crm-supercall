<?php

namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use backend\modules\catalog\components\MaterializedPathBehavior;
use backend\modules\catalog\models\KladerCountries;
use backend\modules\catalog\models\search\KladerCountriesSearch;
use common\components\Notifier;
use common\models\Country;
use common\models\search\CountrySearch;
use common\modules\order\models\Order;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use yii;
use yii\db\Expression;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use api\components\filters\VerbFilter;



/**
 * Class KladerCountriesController
 * @package backend\modules\catalog\controllers
 */
class KladerCountriesController extends Controller
{
    /**
     * @param yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['get-types-address', 'get-values-address'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionIndex()
    {
        Yii::$app->session['var'] = 'value';

        $params = yii::$app->request->get();
        $paths = isset($params['path']) ? explode("/", urldecode($params['path'])) : (isset($params['country_id']) ? [$params['country_id']] : []);
        $model = new KladerCountries();
        $pathParent = !isset($params['path']) ? 'country_id' : 'path';
        $parents = [];
        $hierarchy = ['path' => '', 'name' => ''];

        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post('KladerCountries');
            //существует ли root для данной страны?
            $root = KladerCountries::findOne(['path' => ArrayHelper::getValue($params, $pathParent)]);

            if (is_null($root)) {
                $getDataForRoot = Country::find()->where(['id' => $data['country_id']])->one();

                if (!is_null($getDataForRoot)) {
                    $path = $getDataForRoot->id;
                    $name = $getDataForRoot->name;
                    $root = $this->createRoot((string)$path, $name, $getDataForRoot->id, $data['partner_form_id']);
                }
            }

            if ($root !== false) {
                $this->addPath($root, $data['name'], $data['partner_form_id'], $data['partner_form_attribute_id']);
            }
        }
        $modelSearch = new CountrySearch();
        //Список стран
        if (!isset($params['country_id'])) {
            //Список по фильтру
            if(isset($params['CountrySearch'])) {
                $dataProvider = $modelSearch->search($params);
            } else {
                //Список всех стран
                $dataProvider = $modelSearch->search();
            }
        } //Далее по уровням
        else {
            if (!isset($params['path'])) {
                $root = KladerCountries::findOne(['path' => ArrayHelper::getValue($params, 'country_id')]);
            } else {
                $root = KladerCountries::findOne(['path' => ArrayHelper::getValue($params, 'path')]);

                if (is_null($root)) {
                    throw new HttpException(404, Yii::t('common', 'Страна не найдена.'));
                }

                $allRoot = $root->getParents()->all();
                $thisCountry = $root->getRoot()->one();

                foreach ($allRoot as $v) {
                    $parents[] = [
                        'path' => $v->path,
                        'name' => $v->name
                    ];
                }
            }
            $thisCountry = Country::findOne(['id' => $params['country_id']]);
            //если есть root
            $childs = is_null($root) ? [] : $root->getDescendants(1)->all();
            $query = KladerCountries::find()->where(['in', 'path', ArrayHelper::getColumn($childs, 'path')]);
            $dataProvider = new ActiveDataProvider(['query' => $query]);
        }

        if (isset($thisCountry)) {
            $parentPath = isset($thisCountry->path) ? $thisCountry->path : $thisCountry->id;
            $hierarchy = array_merge([0 => ['path' => $parentPath, 'name' => $thisCountry->name]], $parents);
        }
        $country_id = yii::$app->request->get('country_id');

        $forms = PartnerForm::getForms($country_id);

        if (!is_null($country_id) && empty($forms)) {
            throw new HttpException(403, yii::t('common', 'Для данной страны не создано не одной партнёрской формы'));
        }

        $partnerForms = [];

        if (!is_null($country_id)) {
            foreach ($forms[$country_id] as $k => $form) {
                $partnerForms[$k] = $form['name'];
            }
        } else {
            $partnerForms = [];
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'modelSearch' => $modelSearch,
            'countries' => ArrayHelper::map(Country::find()->active()->all(), 'id', 'name'),
            'char_code' => ArrayHelper::map(Country::find()->active()->all(), 'id', 'char_code'),
            'params' => $params,
            'paths' => $paths,
            'parents' => $parents,
            'hierarchy' => implode(' {separate} ', ArrayHelper::getColumn($hierarchy, 'name')) . '{separate}',
            'partnerForms' => $partnerForms
        ]);

    }

    /**
     * @param $root
     * @param $name
     * @param $form_id
     * @param $form_attribute_id
     * @return KladerCountries
     */
    public function addPath($root, $name, $form_id, $form_attribute_id)
    {
        $path = new KladerCountries();
        $path->name = trim($name);
        $path->partner_form_id = $form_id;
        $path->partner_form_attribute_id = $form_attribute_id;

        if ($path->validate()) {
            $path->prependTo($root)->save();
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Запись успешно сохранена.'), Notifier::TYPE_SUCCESS);
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Ошибка сохранения значения.'));
        }

        return $path;
    }

    /**
     * @return string
     */
    public function actionGetData()
    {
        //попытаемся восстановить структуру родителей из КЛАДРа
        header('Content-Type: application/javascript');
        $partner_form_attribute_id = yii::$app->request->get('partner_form_attribute_id');
        $klader_in_all_string = yii::$app->request->get('klader_in_all_string');
        $parent = yii::$app->request->get('parent');
        $search = trim(yii::$app->request->get('search'));

        $result = KladerCountries::find()
            ->select([
                KladerCountries::tableName() . '.*',
                'text' => KladerCountries::tableName() . '.name'
            ])->orderBy(['text' => SORT_ASC]);

        if (!empty($search)) {
            if ($klader_in_all_string == Country::KLADER_IN_ALL_STRING_ON) {
                $result->andWhere(['like', 'LOWER(name)', '%' . strtolower($search) . '%', false]);
            } else {
                $result->andWhere(['like', 'LOWER(name)', strtolower($search) . '%', false]);
            }
        }

        //if (yii::$app->request->get('form') == 'price_shipping') {
        $result->andWhere(['partner_form_attribute_id' => $partner_form_attribute_id]);
        //}
        if (isset($parent) && !empty($parent['path'])) {
            //$result->andWhere(['LIKE', 'path', '%/' . $parent['path'] . '/%', false]);
        }

        $result->asArray();

        $uniqData = [];

        foreach ($result->all() as $item){
                $uniqData[$item['text']] = $item;
        }

        $uniqData = array_values($uniqData);

        $is_disabled_user_data = Yii::$app->request->get('is_disabled_user_data');

        if (!$is_disabled_user_data) {
            $data = array_merge($uniqData, [['id' => -time(), 'text' => $search]]);
        } else {
            $data = $uniqData;
        }

        $is_jsonp = yii::$app->request->get('callback');

        if (!$is_disabled_user_data) {
            $search_result = json_encode([
                'results' => empty($uniqData) ? [['id' => -time(), 'text' => $search]] : $data,
            ], JSON_UNESCAPED_UNICODE);
        } else {
            $search_result = json_encode([
                'results' => empty($uniqData) ? [] : $data,
            ], JSON_UNESCAPED_UNICODE);
        }

        return (is_null($is_jsonp)) ? $search_result : $is_jsonp . '(' . $search_result . ')';
    }

    /**
     * Метод возвращает всех родителей наследника
     * @return string
     */
    public function actionGetParentStructhure()
    {
        $child_id = yii::$app->request->post('child_id');
        $child = KladerCountries::findOne(['id' => $child_id]);
        $parents = $child->getParents()->all();
        $structhure = [];

        foreach ($parents as $k => $parent) {
            if ($k < $parent->depth) {
                $structhure[] = [
                    'id' => $parent->id,
                    'depth' => $parent->depth,
                    'name' => $parent->name
                ];
            }
        }

        return json_encode($structhure, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param $path string e.g. {1} or {2,10} ...
     * @param $name string
     * @param $country_id
     * @return KladerCountries|bool
     */
    public function createRoot($path, $name, $country_id, $form_id)
    {

        $model = new KladerCountries();
        $model->name = $name;
        $model->path = (string)$path;
        $model->partner_form_id = $form_id;
        $model->country_id = $country_id;

        if ($model->validate()) {
            /** @var MaterializedPathBehavior $model */
            $model->makeRoot()->save();

            return $model;
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Ошибка создания корневого каталога значения.'));
            return false;
        }
    }

    /**
     * @param $path string e.g {1} or {3,20,94}
     * @return yii\web\Response
     */
    public function actionDelete($path)
    {
        $model = KladerCountries::findOne(['path' => $path]);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Значение удалено.'), 'success');
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'ошибка удаления значения'));
        }

        return $this->redirect(Url::to(Yii::$app->request->referrer));
    }

    /**
     * @return string
     */
    public function actionGetTypesAddress()
    {
        $form = PartnerForm::findOne([
            'country_id' => yii::$app->request->post('country_id'),
            'type' => Order::STATUS_NEW
        ]);

        if ($form) {
            $klader_attributes = PartnerFormAttribute::find()
                ->where([
                    'is_klader' => true,
                    'form_id' => $form->id
                ])->orderBy(['label' => SORT_ASC])
                ->all();

            if ($klader_attributes) {
                $result = [
                    'success' => true,
                    'message' => ArrayHelper::index($klader_attributes, 'id')
                ];
            } else {
                $result = [
                    'success' => true,
                    'message' => []
                ];

            }
        } else {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Для данной страны не найдена форма с типом заказов: {type}', ['type' => Order::getStatusByNumber(Order::STATUS_NEW)])
            ];
        }

        return Json::encode($result);
    }

    /**
     * @return string
     */
    public function actionGetValuesAddress()
    {
        $values = KladerCountries::find()
            ->distinct()
            ->select([
                'id',
                'name' => new Expression('trim(name)')
            ])
            ->where(['partner_form_attribute_id' => yii::$app->request->post('partner_attribute_id')])
            ->orderBy(['name' => SORT_ASC])
            ->all();

        return Json::encode([
            'success' => true,
            'message' => ArrayHelper::map($values, 'name', 'name')
        ]);
    }
}
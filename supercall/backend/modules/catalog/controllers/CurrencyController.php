<?php
namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use common\models\Currency;
use common\models\search\CurrencySearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class CurrencyController
 * @package backend\modules\catalog\controllers
 */
class CurrencyController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new CurrencySearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Currency();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Валюта успешно добавлена.') : Yii::t('common', 'Валюта успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));

            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $data['model'] = $model;

        return $this->render('edit', $data);
    }

    /**
     * @param $id
     * @return Currency
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Currency::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Валюта не найдена.'));
        }

        return $model;
    }
}

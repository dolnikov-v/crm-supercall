<?php

namespace backend\modules\catalog\controllers;

use backend\components\web\Controller;
use common\models\Country;
use common\models\CountryPhoneCode;
use common\models\CountryWorkTime;
use common\models\search\CountrySearch;
use Yii;
use yii\db\Exception;
use yii\helpers\Url;
use yii\web\HttpException;
use common\models\AutoCallCountry;
use yii\helpers\ArrayHelper;

/**
 * Class CountryController
 * @package backend\modules\catalog\controllers
 */
class CountryController extends Controller
{
    /**
    * @return array
    */
    public function beforeActionIndex()
    {
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);

        return
            [
                'country' => ArrayHelper::map(yii::$app->user->getCountries(), 'id', function ($val) {
                    return yii::t('common', $val->name);
                }),
                'char_code' => ArrayHelper::map(yii::$app->user->getCountries(), 'id', function ($val) {
                    return yii::t('common', $val->char_code);
                }),

                'query' => $query,

            ];


    }


    /**
     * @return string
     */
    public function actionIndex()
    {
        $data = $this->beforeActionIndex();
        $modelSearch = new CountrySearch();
        $dataProvider = $modelSearch->search($data['query']);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'name' => $data['country'],
            'char_code' => $data['char_code'],
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Country();
        }

        if ($model->load(Yii::$app->request->post())) {

            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common',
                    'Страна успешно добавлена.') : Yii::t('common', 'Страна успешно отредактирована.'), 'success');

                $phoneData = Yii::$app->request->post('phoneData');
                if (!empty($phoneData)) {
                    if ($isNewRecord) {
                        $phCode = new CountryPhoneCode();
                    } else {
                        $phCode = CountryPhoneCode::findOne(['country_id' => $model->id]);
                        if (empty($phCode)) {
                            $phCode = new CountryPhoneCode();
                        }
                    }

                    $phoneData['country_id'] = $model->id;
                    $phCode->setAttributes($phoneData);

                    $phCode->save();
                }

                if ($isNewRecord) {
                    $phCode = new CountryPhoneCode();
                    $model->refresh();
                    $phCode->country_id = $model->id;
                } else {
                    $phCode = CountryPhoneCode::findOne(['country_id' => $model->id]);
                }

                $phCode->phone_code = $phoneData['phone_code'];
                $phCode->phone_length_min = $phoneData['phone_length_min'];
                $phCode->phone_length_max = $phoneData['phone_length_max'];
                $phCode->use_zorra_validate = $phoneData['use_zorra_validate'];

                $phCode->save();

                if ($workTime = Yii::$app->request->post('CountryWorkTime')) {
                    CountryWorkTime::clearWorkTime($model->id);
                    CountryWorkTime::updateWorkTime($model->id, $workTime);
                }

                if ($autoCallCountry = Yii::$app->request->post('CountryAutoCallCount')) {
                    AutoCallCountry::clearAutoCallCount($model->id);
                    AutoCallCountry::updateAutoCallCount($model->id, $autoCallCountry);
                }

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $data['model'] = $model;
        $data['phoneCode'] = new CountryPhoneCode();
        $data['workTime'] = new CountryWorkTime();
        $data['autoCallCount'] = new AutoCallCountry();

        if (!$model->isNewRecord) {
            $phoneCode = CountryPhoneCode::findOne(['country_id' => $model->id]);
            if ($phoneCode) {
                $data['phoneCode'] = $phoneCode;
            }

            $workTime = CountryWorkTime::findOne(['country_id' => $model->id]);
            if ($workTime) {
                $data['workTime'] = $workTime;
            }

            $autoCallCountry = AutoCallCountry::findOne(['country_id' => $model->id]);
            if ($autoCallCountry) {
                $data['autoCallCount'] = $autoCallCountry;
            }

        }
        return $this->render('edit', $data);
    }

    /**
     * @param integer $id
     * @return Country
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Country::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Страна не найдена.'));
        }

        return $model;
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Страна успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Страна успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        try{
            if ($model->delete()) {
                Yii::$app->notifier->addNotifier(Yii::t('common', 'Страна успешно удалена.'), 'success');
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }

            return $this->redirect(Url::toRoute('index'));

        } catch (Exception $e) {
            throw new HttpException(500, Yii::t('common', 'Попробуйте позже'));
        }
    }
}

<?php
namespace backend\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class PriceShippingAsset
 * @package backend\modules\access\assets
 */
class PriceShippingAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/catalog/price-shipping';

    public $js = [
        'price-shipping.js',
    ];

    public $css = [
        'price-shipping.css'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

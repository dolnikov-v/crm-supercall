<?php
namespace backend\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class KladerCountriesAsset
 * @package backend\assets\base
 */
class KladerCountriesAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/catalog/';

    public $js = [
        'klader-countries.js',
    ];

    public $css = [
        'klader-countries.css',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\BootstrapAsset',
    ];
}
<?php
namespace backend\modules\catalog\assets;

use yii\web\AssetBundle;

/**
 * Class CountryEditAsset
 * @package backend\modules\access\assets
 */
class CountryEditAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/catalog/country-edit';

    public $js = [
        'country-edit.js',
    ];

    public $css = [
        'country-edit.css'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

<?php
namespace backend\modules\catalog\assets;
use yii\web\AssetBundle;

/**
 * Class CountryStrategyAsset
 * @package backend\modules\catalog\assets
 */
class CountryStrategyAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/catalog/country-strategy';

    public $js = [
        'country-strategy.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
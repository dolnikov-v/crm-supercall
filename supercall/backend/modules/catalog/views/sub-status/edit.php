<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\order\models\OrderSubStatus $model */
/** @var array $groups */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление субстатуса') : Yii::t('common', 'Редактирование субстатуса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список субстатусов'), 'url' => Url::toRoute('/catalog/sub-status/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Субстатус'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'groups' => $groups,
    ])
]); ?>

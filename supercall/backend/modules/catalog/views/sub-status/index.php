<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Panel;
use common\widgets\base\ModalConfirmDelete;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var \common\modules\order\models\OrderSubStatus $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $groups */

$this->title = Yii::t('common', 'Список субстатусов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'groups' => $groups,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с субстатусами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'label' => Yii::t('common', 'Группа'),
                'format' => 'raw',
                'value' => function($model) use ($groups) {
                    return $groups[$model->group_id];
                },
                'enableSorting' => false,
            ],
            'code',
            'name',
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.sub-status.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.sub-status.delete');
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footer' =>
        (Yii::$app->user->can('catalog.sub-status.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить субстатус'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>

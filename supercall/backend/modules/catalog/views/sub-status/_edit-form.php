<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\modules\order\models\OrderSubStatus $model */
/** @var array $groups */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'group_id')->dropDownList($groups) ?>
    </div>
    <div class="col-lg-1">
        <?= $form->field($model, 'code')->textInput() ?>
    </div>
    <div class="col-lg-8">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить субстатус') : Yii::t('common', 'Сохранить субстатус')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use common\components\grid\ActionColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use yii\widgets\LinkPager;
use yii\helpers\ArrayHelper;
use backend\modules\catalog\assets\KladerCountriesAsset;

/** @var \backend\modules\catalog\models\KladerCountries $model */
/** @var \backend\modules\catalog\controllers\KladerCountriesController $countries */
/** @var \backend\modules\catalog\controllers\KladerCountriesController $dataProvider */
/** @var \backend\modules\catalog\controllers\KladerCountriesController $params */
/** @var \backend\modules\catalog\controllers\KladerCountriesController $paths */
/** @var \backend\modules\catalog\controllers\KladerCountriesController $parents */
/** @var \backend\modules\catalog\controllers\KladerCountriesController $hierarchy */
/** @var array $partnerForms */

$this->title = Yii::t('common', 'КЛАДР стран');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'КЛАДР стран')];

KladerCountriesAsset::register($this);

?>
<div class="row">
    <div id="beforeload" class="col-lg-12"><p><?= Yii::t('common', 'Пожалуйста, подождите, идет загрузка справочника...') ?> <img width="35px" src="/resources/images/common/spinner.gif"></p></div>
</div>
<div class="row" id="form_klader">


    <div class="col-lg-4">
        <?php $form = ActiveForm::begin(['action' => Url::toRoute(['index'] + $params)]); ?>

        <?php if (isset($params['country_id'])) : ?>

            <div class="row">
                <div class="col-lg-12">
                    <?= $form->field($model, 'country_id')->select2List($countries, [
                        'value' => ArrayHelper::getValue($params, 'country_id', 1)
                    ])->label(Yii::t('common', 'Страна')) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <?= $form->field($model, 'partner_form_id')->select2List($partnerForms, [
                        'value' => (yii::$app->request->get('partner_form_id')) ? yii::$app->request->get('partner_form_id') : '',
                        'id' => 'partner_form_id'
                    ])->label(Yii::t('common', 'Форма')) ?>

                    <?=$form->field($model, 'partner_form_attribute_id', ['template' =>'{input}'])->hiddenInput(['id' => 'partner_form_attribute_id']);?>

                </div>
            </div>

            <?php foreach ($parents as $parent) : ?>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    </div>
                </div>

                <br/>

                <div class="row">
                    <div class="col-lg-12 text-center">
                        <a href="<?= Url::to(['index', 'country_id' => $params['country_id'], 'path' => $parent['path']]) ?>">
                            <button type="button"
                                    class="btn btn-default"><?= $parent['name']; ?></button>
                        </a>
                    </div>
                </div>

                <br/>
            <?php endforeach; ?>

            <div class="row">
                <div class="col-lg-12">
                    <?= $form->field($model, 'name')->textInput(['value' => ''])->label(Yii::t('common', 'Новое значение')) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <?= $form->submit(Yii::t('common', 'Добавить значение')) ?>
                </div>
            </div>

        <?php endif; ?>

        <?php ActiveForm::end(); ?>

    </div>
    <div class="col-lg-<?= !isset($params['country_id']) ? 12 : 8 ?>">
        <div class="empty"></div>

        <?php

        if(isset($params['country_id'])){
            $alert = [
                'alert' => strtr($hierarchy, ['{separate}' => '&nbsp;&nbsp;<span class="glyphicon glyphicon-arrow-right"></span>&nbsp;&nbsp;']),
                'alertStyle' => Panel::ALERT_INFO,
            ];
        }else{
            $alert = [];
        }
        ?>

        <?php if(!yii::$app->request->get('country_id')) :?>

        <?= Panel::widget([
            'title' => Yii::t('common', 'Фильтр поиска'),
            'showButtons' => true,
            'contentVisible' => true,
            'content' => $this->render('_index-form', [
                'modelSearch' => $modelSearch,
                'name' => $countries,
                'char_code' => $char_code,
            ]),
        ])
        ?>

        <?php endif;?>

        <?= Panel::widget([
            'title' => Yii::t('common', 'Значения'),
            'actions' => DataProvider::renderSummary($dataProvider),
            'contentPadding' => false,
            //'alert' => strtr($hierarchy, ['{separate}' => '&nbsp;&nbsp;<span class="glyphicon glyphicon-arrow-right"></span>&nbsp;&nbsp;']),
            //'alertStyle' => Panel::ALERT_INFO,
            'content' => GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'class' => IdColumn::className(),
                    ],
                    [
                        'attribute' => 'name',
                        'content' => function ($data) {
                            return Yii::t('common', $data->name);
                        },
                        'enableSorting' => false,
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'items' => [
                            [
                                'label' => Yii::t('common', 'Управление'),
                                'url' => function ($data) use ($params) {
                                    $requestParams = isset($params['country_id'])
                                        ? ['index', 'country_id' => $params['country_id'], 'path' => $data->path]
                                        : ['index', 'country_id' => $data->id];

                                    return Url::toRoute($requestParams);
                                },
                                'can' => function () {
                                    return Yii::$app->user->can('catalog.kladercountries.edit');
                                },
                            ],
                            [
                                'label' => Yii::t('common', 'Удалить'),


                                'url' => function () {
                                    return '#';
                                },
                                'style' => 'confirm-delete-link',
                                'attributes' => function ($data) use ($params)  {
                                    return [
                                        'data-href' => isset($params['path']) ? Url::toRoute(['delete', 'path' => $data->path]) : '',
                                    ];
                                },
                                'can' => function () use ($params) {
                                    return isset($params['country_id']) && Yii::$app->user->can('catalog.kladercountries.edit');
                                }
                            ],
                        ]
                    ]
                ],
            ]),
            'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
        ] + $alert) ?>
    </div>
</div>

<?= ModalConfirmDelete::widget() ?>
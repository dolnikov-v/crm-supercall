<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \backend\modules\catalog\models\PriceShipping $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление стоимости доставки') : Yii::t('common', 'Редактирование стоимости доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Стоимость доставки'), 'url' => Url::toRoute('/catalog/price-shipping')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Стоимость доставки'),
    'content' => $this->render('_edit-form', [
        'model' => $model
    ])
]); ?>
<?php
use common\modules\partner\models\PartnerShipping;
use common\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Country;
use common\models\Shipping;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerProduct;

/** @var \backend\modules\catalog\models\search\PriceShippingSearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'partner_id')->select2List(Partner::find()->collection(), ['prompt' => '—']); ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'country_id')->select2List(Country::find()->collection('common'), ['prompt' => '—']); ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($modelSearch, 'shipping_id')->select2List(PartnerShipping::find()->collection(), ['prompt' => '—']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\widgets\base\ModalConfirmDelete;
use backend\modules\catalog\assets\PriceShippingAsset;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \backend\modules\catalog\models\search\PriceShippingSearch $modelSearch */

PriceShippingAsset::register($this);

$this->title = Yii::t('common', 'Стоимость доставки');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Стоимость доставки')];

?>

<?=Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с стоимостью доставки'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'partner_id',
                'content' => function ($model) {
                    return yii::t('common', $model->partner->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country_id',
                'content' => function ($model) {
                    return yii::t('common', $model->country->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'shipping_id',
                'content' => function ($model) {
                    return yii::t('common', $model->shipping->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'param_shipping',
                'content' => function ($model) {
                    return yii::t('common', $model->param_shipping);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'price_shipping',
                'content' => function ($model) {
                    //return yii::t('common', $model->price_shipping);
                    return Yii::$app->formatter->asDecimal($model->price_shipping, 2);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.priceshipping.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['/catalog/price-shipping/delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.price-shipping.delete');
                        }
                    ],

                ]
            ]
        ],
    ]),
    'footer' =>
        (Yii::$app->user->can('catalog.priceshipping.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить стоимость доставки'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>


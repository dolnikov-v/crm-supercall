<?php
use common\widgets\ActiveForm;
use common\models\Country;
use common\models\Shipping;
use common\modules\partner\models\Partner;
use backend\modules\catalog\assets\PriceShippingAsset;
use common\widgets\base\Button;
use common\widgets\base\GlyphIcon;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

PriceShippingAsset::register($this);


/** @var backend\modules\catalog\models\PriceShipping $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row shipping_groups">

        <div class="row shipping_group">

            <?php if(!$model->isNewRecord) : ?>

                <?= $form->field($model, 'price_shipping_data')->hiddenInput([
                    'value' => $model->param_shipping,
                    'name' => 'price_shipping_data',
                ])->label(false) ?>

            <?php endif;?>

            <div class="col-lg-4"  style="padding-left: 40px!important">

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'partner_id')->select2List(Partner::find()
                            ->where(['name' => [Partner::PARTNER_2WTRADE, Partner::PARNTER_EUROPAY]])->collection(), [
                                'name' => 'PriceShipping[partner_id][]',
                                'prompt' => '—'
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'country_id')->select2List(Country::find()->collection('common'), [
                            'prompt' => '—',
                            'name' => 'PriceShipping[country_id][]',
                            'disabled' => ($model->isNewRecord) ? true : false
                        ]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'shipping_id')->select2List(Shipping::find()->collection(),[
                            'name' => 'PriceShipping[shipping_id][]',
                        ]); ?>
                    </div>
                </div>

            </div>


            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12 place_attributes">

                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'price_shipping')->textInput([
                            'name' => 'PriceShipping[price_shipping][]',
                        ]) ?>
                    </div>
                </div>
            </div>

            <div class="col-lg-1">

                <div class="row">
                    <label>&nbsp;</label>
                    <div class="col-lg-12">
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_SUCCESS . ' add-price dropdown-toggle',
                                'icon' => GlyphIcon::PLUS,
                                'attributes' => ['data-toggle' => 'dropdown']
                            ]) ?>
                        </div>
                        <div class="btn-group">
                            <?= Button::widget([
                                'style' => Button::STYLE_DANGER . ' remove-price dropdown-toggle',
                                'icon' => GlyphIcon::MINUS,
                                'attributes' => ['data-toggle' => 'dropdown']
                            ]) ?>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="tpl_field hidden">
        <div class="row">
            <div class="col-lg-12">
                <?= $form->field($model, '_name_')->textInput(
                        [
                            'placeholder' => '',
                            'name' => 'PriceShipping[param_shipping][_index_][_name_]',
                            'autocomplete' => "new-password"
                        ])->label('_label_name_') ?>
                <div id="autocomplete_name_" class="autocomplete_field"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить') : Yii::t('common', 'Сохранить')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
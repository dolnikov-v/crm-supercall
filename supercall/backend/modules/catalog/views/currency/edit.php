<?php
use common\widgets\base\Panel;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\Currency $model */
$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление валюты') : Yii::t('common', 'Редактирование валюты');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Валюты'), 'url' => Url::toRoute('/catalog/currency/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление валюты') : Yii::t('common', 'Редактирование валюты')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные валюты'),
    'alert' => Yii::t('common', 'Цифровой и буквенный код можно посмотреть на сайте {link}.', [
        'link' => Html::a('WIKIPEDIA', 'https://en.wikipedia.org/wiki/ISO_4217', ['target' => '_blank']),
    ]),
    'content' => $this->render('_edit-form', ['model' => $model]),
]); ?>

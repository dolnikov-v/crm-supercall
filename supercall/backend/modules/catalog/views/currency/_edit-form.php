<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\models\Currency $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'num_code')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'char_code')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить валюту') : Yii::t('common', 'Сохранить валюту')); ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

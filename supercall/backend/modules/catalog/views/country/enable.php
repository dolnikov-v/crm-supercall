<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\Country $model */

$this->title = Yii::t('common', 'Подключение страны');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Страны'), 'url' => Url::toRoute('/catalog/country/index')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Подключение страны')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные страны'),
    'content' => $this->render('_enable-form', ['model' => $model])
]) ?>

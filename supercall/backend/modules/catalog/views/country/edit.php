<?php
use common\widgets\base\Panel;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\Country $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление страны') : Yii::t('common', 'Редактирование страны');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Страны'), 'url' => Url::toRoute('/catalog/country/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление страны') : Yii::t('common', 'Редактирование страны')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные страны'),
    'alert' => Yii::t('common', 'Код страны можно посмотреть на сайте {link}.', [
        'link' => Html::a('GOOGLE', 'https://developers.google.com/public-data/docs/canonical/countries_csv', ['target' => '_blank']),
    ]),
    'content' => $this->render('_edit-form', ['model' => $model, 'phoneCode' => $phoneCode, 'workTime' => $workTime, 'autoCallCount' => $autoCallCount])
]); ?>

<?php
use common\assets\vendor\ATimerAsset;
use common\components\grid\ActionColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\helpers\i18n\Moment;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

ATimerAsset::register($this);

$this->title = Yii::t('common', 'Список стран');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Страны')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => true,
    'content' => $this->render('_index-form', [
        'modelSearch' => $modelSearch,
        'name' => $name,
        'char_code' => $char_code,
    ]),
]) ?>



<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со странами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->active ? '' : 'text-muted'),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'content' => function ($data) {
                    return Yii::t('common', $data->name);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'helpline_phone',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->active ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->active ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'order_hold_verification',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->order_hold_verification ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->order_hold_verification ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'char_code',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'timezone.timezone_id',
                'headerOptions' => ['class' => 'width-200 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'header' => Yii::t('backend', 'Местное время'),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($data) {
                    $time = new DateTime();
                    if ($data->timezone){
                        $time->setTimezone(new DateTimeZone($data->timezone->timezone_id));
                    }
                    return Html::tag('span', Yii::$app->formatter->asFullTime($time->format('Y-m-d H:i:s')), ['class' => 'atimer-auto-init', 'data-format' => Moment::getFullTimeFormat()]);
                }
            ],
            [
                'header' => Yii::t('common', 'Валюта'),
                'attribute' => 'currency.char_code',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'header' => Yii::t('common', 'Язык'),
                'attribute' => 'language.name',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.country.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/catalog/country/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('catalog.country.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/catalog/country/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('catalog.country.deactivate') && $model->active;
                        }
                    ],
                    [
                        'can' => function ($model) {
                            return Yii::$app->user->can('catalog.country.delete')
                            || (Yii::$app->user->can('catalog.country.activate') && !$model->active)
                            || (Yii::$app->user->can('catalog.country.deactivate') && $model->active);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $data->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.country.delete');
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('catalog.country.edit')
            ? ButtonLink::widget([
                    'label' => Yii::t('common', 'Добавить страну'),
                    'url' => Url::toRoute('edit'),
                    'style' => ButtonLink::STYLE_SUCCESS,
                    'size' => ButtonLink::SIZE_SMALL,
                ])
            : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

<?= ModalConfirmDelete::widget() ?>

<?php
use backend\modules\catalog\assets\CountryStrategyAsset;
use backend\modules\queue\models\Queue;
use common\models\CountryWorkTime;
use common\models\Currency;
use common\models\Language;
use common\models\Timezone;
use common\modules\order\models\Order;
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use common\widgets\base\Switchery;
use backend\modules\catalog\assets\CountryEditAsset;

CountryEditAsset::register($this);
CountryStrategyAsset::register($this);

/** @var \common\models\Country $model */
/** @var \common\models\CountryWorkTime $workTime */
/**
 * @var \common\models\AutoCallCountry $autoCallCount
 */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-5">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'helpline_phone')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'slug')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'char_code')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'timezone_id')->select2List(Timezone::find()->collection(), ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'currency_id')->select2List(Currency::find()->collection(), ['prompt' => '—']); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'language_id')->select2List(Language::find()->collectionKey('id')->collectionAll(), ['prompt' => '—']); ?>
    </div>
    <div class="col-lg-2">
        <div class="control-label"><label>&nbsp;</label></div>
        <a href="<?= Url::to(['/i18n/language/edit', 'id' => '', 'returnUrl' => Url::current()]) ?>" class="btn btn-default"><?= Yii::t('common','Добавить язык')?></a>
    </div>
</div>

<div class="row">
    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label"><label><?= Yii::t('common','Телефонный код')?></label></div>
            <?= Html::textInput('phoneData[phone_code]',$phoneCode->phone_code,array('class'=>'form-control')) ?>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label"><label><?= Yii::t('common','Мин. длина номера')?></label></div>
            <?= Html::textInput('phoneData[phone_length_min]',$phoneCode->phone_length_min,array('class'=>'form-control')) ?>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <div class="control-label"><label><?= Yii::t('common','Макс. длина номера')?></label></div>
            <?= Html::textInput('phoneData[phone_length_max]',$phoneCode->phone_length_max,array('class'=>'form-control')) ?>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="col-lg-3">
            <div class="form-group check-phone">
                <div class="control-label"><label><?= Yii::t('common','Проверять номер?')?></label></div>
                <?= Html::hiddenInput('phoneData[check_phone]',$phoneCode->check_phone?1:0,array('id'=>'checkPhone')) ?>
                <?= Switchery::widget([
                    'checked' => $phoneCode->check_phone == true ? true : false,
                ])?>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group validate-type">
                <div class="control-label"><label><?= Yii::t('common','Zorra валидация')?></label></div>
                <?= Html::hiddenInput('phoneData[use_zorra_validate]',$phoneCode->use_zorra_validate,array('id'=>'validateType')) ?>
                <?= Switchery::widget([
                    'checked' => $phoneCode->use_zorra_validate,
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="control-label"><label><?= Yii::t('common','Действия с заказами с невалидными номерами')?></label></div>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'status')->select2List(Order::getFinalStatusesForCountryStrategyCollection(), ['prompt' => '—'])->label(yii::t('common', 'Сменить статус')); ?>
    </div>
    <div class="col-lg-1 text-center padding-top-45">
        <br/>
        <br/>
        <?=yii::t('common', 'или')?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'queue')->select2List(ArrayHelper::map(Queue::find()->byCountry($model->id, false)->all(), 'id', 'name'), ['prompt' => '—'])->label(yii::t('common', 'Сменить очередь')); ?>
    </div>

</div>
<br/>

<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <div class="control-label"><label><?= Yii::t('common','Время начала работы')?></label></div>
            <div class="col-md-5">
                <?= $form->field($workTime, 'begin_time_hour')->select2List(CountryWorkTime::getHourList()); ?>
                <?= $form->field($workTime, 'begin_time_minute')->select2List(CountryWorkTime::getMinuteList()); ?>
             </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <div class="control-label"><label><?= Yii::t('common','Время завершения работы')?></label></div>
            <div class="col-md-5">
                <?= $form->field($workTime, 'end_time_hour')->select2List(CountryWorkTime::getHourList()); ?>
                <?= $form->field($workTime, 'end_time_minute')->select2List(CountryWorkTime::getMinuteList()); ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <div class="form-group">
            <div class="control-label"><label><?= Yii::t('common','Количество звонков на оператора')?></label></div>
            <?= Html::textInput('CountryAutoCallCount[count]', $autoCallCount->count, array('class'=>'form-control')) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'order_hold_verification')->checkbox([
            'checked' => $model->order_hold_verification,
            'value' => 1,
            'unchecked_value' => 0
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'klader_in_all_string')->checkbox([
            'checked' => $model->klader_in_all_string,
            'value' => 1,
            'unchecked_value' => 0
        ]); ?>
    </div>
</div>
<br/>

<fieldset <?=yii::$app->user->can('catalog.country.whatsappedit') ? '' : 'disabled';?>>
    <legend><?=yii::t('common', 'Whatsapp');?></legend>

    <div class="row">
        <div class="col-lg-2">
            <?= $form->field($model, 'whatsapp_number')->textInput() ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'whatsapp_phone')->textInput() ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'whatsapp_token')->textInput() ?>
        </div>
        <div class="col-lg-5">
            <?= $form->field($model, 'whatsapp_url')->textInput() ?>
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'whatsapp_message_template')->textarea(['rows' => 10]) ?>
        </div>
        <div class="col-lg-12">
            <div class="alert alert-info"><b>{OrderID}</b> - номер заказа, <b>{ClientName}</b> - полное имя клиента, <b>{Product}</b> - список товаров (с переносом строки)</div>
        </div>
    </div>

    <?php if(yii::$app->user->can('catalog.country.whatsappviewdescription')):?>
        <br/>  <br/>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'whatsapp_description')->textarea() ?>
        </div>
    </div>

    <?php endif;?>

</fieldset>

<br/>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить страну') : Yii::t('common', 'Сохранить страну')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

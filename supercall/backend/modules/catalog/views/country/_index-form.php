<?php

use common\helpers\Collection;
use common\widgets\ActiveForm;
use yii\helpers\Url;
?>


<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
    <div class="row">

        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= /** @var array $countries */
            $form->field($modelSearch, 'name')->select2List($name, [
                'prompt' => '—',
            ])->label(\Yii::t('common', 'Страна')) ?>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-4">
            <?= /** @var array $countries */
            $form->field($modelSearch, 'char_code')->select2List($char_code, [
                'prompt' => '—',
            ])->label(\Yii::t('common', 'Код страны')) ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
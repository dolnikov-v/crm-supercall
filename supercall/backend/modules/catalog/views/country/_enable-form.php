<?php
use common\models\Country;
use common\models\Language;
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\models\Country $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['enable'])]); ?>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'country')->select2List(Country::find()->collection('common'), ['prompt' => '—']) ?>
    </div>
    <div class="col-lg-6">
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'language')->select2List(Language::find()->collectionKey('id')->collectionAll(), ['prompt' => '—']); ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Подключить страну')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

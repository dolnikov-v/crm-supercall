<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use common\modules\partner\models\Partner;
use common\models\Product;
use mihaildev\ckeditor\CKEditor;

/** @var \backend\modules\catalog\models\ScriptsOperators $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'country_id')->select2List($countries, [
                'prompt' => '-',
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'partner_id')->select2List(Partner::find()->collection(), [
                'prompt' => '-'
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'product_id')->select2List(Product::find()->collection(), [
                'prompt' => '-'
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'editorOptions' => [
                    'preset' => 'full',
                    'inline' => false,
                ],
            ]); ?>
        </div>
    </div>

<?= $form->field($model, (!$model->id) ? 'created_user_id' : 'updated_user_id')->hiddenInput([
    'value' => yii::$app->user->id
])->label(false); ?>

    <div class="row">
        <div class="col-lg-12">
            <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить скрипт для оператора') : Yii::t('common', 'Сохранить скрипт для оператора')) ?>
            <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
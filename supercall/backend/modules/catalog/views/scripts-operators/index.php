<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\widgets\base\Label;
use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use common\widgets\base\ModalConfirmDelete;

/** @var yii\web\View $this */
/** @var \common\models\search\ShippingSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список скриптов для операторов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
        'countries' => $countries
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица со скриптами операторов'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . (!$model->active ? 'text-muted' : ''),
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'country_id',
                'enableSorting' => false,
                'content' => function($data){
                    return $data->country->name;
                }
            ],
            [
                'attribute' => 'partner_id',
                'enableSorting' => false,
                'content' => function($data){
                    return $data->partner->name;
                }
            ],
            [
                'attribute' => 'product_id',
                'enableSorting' => false,
                'content' => function($data){
                    return $data->product->name;
                }
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model['active'] ? yii::t('common', 'Да') : yii::t('common', 'Нет'),
                        'style' => $model['active'] ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_user_id',
                'enableSorting' => false,
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center width-150'],
                'content' => function($data){
                    return $data->user->username;
                }
            ],
            [
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center width-150'],
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center width-150'],
                'attribute' => 'updated_user_id',
                'enableSorting' => false,
                'content' => function($data){
                    return ($data->userUpdate) ? $data->userUpdate->username : '';
                }
            ],
            [
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center width-150'],
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.scriptsoperators.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/catalog/scripts-operators/activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('catalog/scriptsoperators.activate') && !$model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/catalog/scripts-operators/deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('catalog/scriptsoperators/deactivate') && $model->active;
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model){
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.scriptsoperators.delete');
                        }
                    ],

                ]
            ]
        ],
    ]),
    'footer' =>
        (Yii::$app->user->can('catalog.shipping.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Создать скрипт для оператора'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>


<?php Modal::begin([
    'id' => 'confirm_google_zip',
    'header' => yii::t('common', 'Подтверждение'),
    'options' => ['tabindex' => 2],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-default btn-success', 'id' => 'google_confirm_success']) .
        Html::button(yii::t('common', 'Нет'), ['class' => 'btn btn-default btn-cancel', 'id' => 'google_confirm_cancel'])
]); ?>

<?= ModalConfirmDelete::widget(); ?>
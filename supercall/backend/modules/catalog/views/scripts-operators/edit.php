<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \backend\modules\catalog\models\ScriptsOperators $model*/

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление скрипта для оператора') : Yii::t('common', 'Редактирование скрипта для оператора');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Скрипты для оператора'), 'url' => Url::toRoute('/catalog/scripts-operators/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Скрипт для оператора'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'countries' => $countries
    ])
]); ?>
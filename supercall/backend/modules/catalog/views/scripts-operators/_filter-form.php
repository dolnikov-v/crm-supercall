<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;
use common\modules\partner\models\Partner;
use common\models\Product;

/** @var \backend\modules\catalog\models\search\ScriptsOperatorsSearch $modelSearch */
/** @var array $countries */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>

    <div class="row">
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'country_id')->select2List($countries, [
                'prompt' => '-',
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'partner_id')->select2List(Partner::find()->collection(), [
                'prompt' => '-'
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'product_id')->select2List(Product::find()->collection(), [
                'prompt' => '-'
            ]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($modelSearch, 'active')->select2List([
                true => yii::t('common', 'Да'),
                false => yii::t('common', 'Нет')
            ], [
                'prompt' => '-'
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->submit(Yii::t('common', 'Применить')); ?>
            <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
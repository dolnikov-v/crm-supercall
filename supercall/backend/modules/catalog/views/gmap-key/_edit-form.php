<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\models\GmapKey $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'email')->textInput() ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'key')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить ключ') : Yii::t('common', 'Сохранить ключ')); ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

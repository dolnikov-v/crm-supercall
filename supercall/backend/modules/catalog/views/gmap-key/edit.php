<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\GmapKey $model */
$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление ключа') : Yii::t('common', 'Редактирование ключа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Ключи Google'), 'url' => Url::toRoute('/catalog/gmap-key/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление валюты') : Yii::t('common', 'Редактирование ключа')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные ключа'),
    'content' => $this->render('_edit-form', ['model' => $model]),
]); ?>

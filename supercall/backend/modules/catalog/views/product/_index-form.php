<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\models\search\ProductSearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($modelSearch, 'name')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'active')->select2List([
            true => yii::t('common', 'Да'),
            false => yii::t('common', 'Нет')
        ], ['prompt' => '—']); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

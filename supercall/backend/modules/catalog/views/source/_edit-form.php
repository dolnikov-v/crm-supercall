<?php

use backend\assets\SourceAsset;
use backend\modules\catalog\models\Source;
use backend\modules\catalog\models\SourcePartnerCountry;
use common\widgets\ActiveForm;
use common\widgets\base\Button;
use common\widgets\base\GlyphIcon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var Source $model */
/** @var SourcePartnerCountry $sourcePartnerCountryModel */
/** @var array $partners */
/** @var array $countries */

SourceAsset::register($this);
$sourcePartnerCountryModel = SourcePartnerCountry::find()->where(['source_id' => $model->id])->all(); ?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'code')->textInput() ?>
    </div>
</div>

<div class="partner-countries">
    <div class="row">
        <div class="col-lg-3"><?= Html::label(yii::t('common', 'Партнер')); ?></div>
        <div class="col-lg-3"><?= Html::label(yii::t('common', 'Страны')); ?></div>
    </div>

<?php if ($sourcePartnerCountryModel) : foreach ($sourcePartnerCountryModel as $sourcePartnerCountry) : ?>
    <div class="row partner-countries-item">
        <div class="col-lg-3">
            <?= $form->field($sourcePartnerCountry, 'partner_id')->select2List(ArrayHelper::map($partners, 'id', 'name'), [
                'name' => "partner[{$sourcePartnerCountry->id}]"
            ])->label(false); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($sourcePartnerCountry, 'country_id')->select2List(ArrayHelper::map($countries, 'id', 'name'), [
                'multiple' => true,
                'name' => "country[{$sourcePartnerCountry->id}]"
            ])->label(false); ?>
        </div>

        <div class="col-lg-3">
            <div class="btn-group">
                <?= Button::widget([
                    'style' => Button::STYLE_SUCCESS . ' add-partner-countries',
                    'icon' => GlyphIcon::PLUS,
                    'attributes' => []
                ]) ?>
            </div>
            <div class="btn-group">
                <?= Button::widget([
                    'style' => Button::STYLE_DANGER . ' remove-partner-countries',
                    'icon' => GlyphIcon::MINUS,
                    'attributes' => []
                ]) ?>
            </div>
        </div>
    </div>
<?php endforeach; else : $sourcePartnerCountryModel = new SourcePartnerCountry(); ?>
    <div class="row partner-countries-item">
        <div class="col-lg-3">
            <?= $form->field($sourcePartnerCountryModel, 'partner_id')->select2List(ArrayHelper::map($partners, 'id', 'name'), [
                'name' => 'partner[0]'
            ])->label(false); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($sourcePartnerCountryModel, 'country_id')->select2List(ArrayHelper::map($countries, 'id', 'name'), [
                'multiple' => true,
                'name' => 'country[0]'
            ])->label(false); ?>
        </div>

        <div class="col-lg-3">
            <div class="btn-group">
                <?= Button::widget([
                    'style' => Button::STYLE_SUCCESS . ' add-partner-countries',
                    'icon' => GlyphIcon::PLUS,
                    'attributes' => []
                ]) ?>
            </div>
            <div class="btn-group">
                <?= Button::widget([
                    'style' => Button::STYLE_DANGER . ' remove-partner-countries',
                    'icon' => GlyphIcon::MINUS,
                    'attributes' => []
                ]) ?>
            </div>
        </div>
    </div>
<?php endif; ?>
</div>

<br/>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить источник') : Yii::t('common', 'Сохранить источник')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php

use backend\modules\catalog\models\Source;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\web\View;

/** @var View $this */
/** @var Source $model */
/** @var array $partners */
/** @var array $countries */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление источника') : Yii::t('common', 'Редактирование источника');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Источники'), 'url' => Url::toRoute('/catalog/source/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление источника') : Yii::t('common', 'Редактирование источника')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные источника'),
    'content' => $this->render('_edit-form', ['model' => $model, 'partners' => $partners, 'countries' => $countries])
]);

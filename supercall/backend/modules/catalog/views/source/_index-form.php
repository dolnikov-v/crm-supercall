<?php

use backend\modules\catalog\models\search\SourceSearch;
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var SourceSearch $modelSearch */

$form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]);
?>

<div class="row">
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($modelSearch, 'code')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

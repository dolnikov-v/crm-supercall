<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\modules\order\models\OrderType $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление типа заказа') : Yii::t('common', 'Редактирование типа заказа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Типы заказов'), 'url' => Url::toRoute('/catalog/order-type/index')];
$this->params['breadcrumbs'][] = ['label' => $model->isNewRecord ? Yii::t('common', 'Добавление страны') : Yii::t('common', 'Редактирование типа заказа')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные типа заказа'),
    'content' => $this->render('_edit-form', ['model' => $model])
]) ?>

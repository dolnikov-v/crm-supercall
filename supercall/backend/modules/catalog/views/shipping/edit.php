<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\Shipping $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление курьерской службы') : Yii::t('common', 'Редактирование курьерской службы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/catalog/shipping/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Курьерская служба'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>

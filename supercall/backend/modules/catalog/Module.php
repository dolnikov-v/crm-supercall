<?php
namespace backend\modules\catalog;

use backend\components\base\Module as BackendModule;
use Yii;
use yii\filters\AccessControl;

/**
 * Class Module
 * @package backend\modules\catalog
 */
class Module extends BackendModule
{
    public $controllerNamespace = 'backend\modules\catalog\controllers';

    /**
     * @return array
     */
//    public function behaviors()
//    {
//        return [];
//    }

}

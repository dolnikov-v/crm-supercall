<?php
namespace backend\modules\profile\controllers;

use backend\models\TaskQueue;
use common\components\web\Controller;
use common\models\Media;
use yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * Class ControlController
 * @package backend\modules\profile\controllers
 */
class ControlController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-profile-image' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model = User::findOne(['id' => yii::$app->user->id]);
        $mediaModel = new Media();
        $error = false;

        if (!is_dir(Media::getPath(Media::TYPE_DOCUMENT_SCAN))) {
            FileHelper::createDirectory(Media::getPath(Media::TYPE_DOCUMENT_SCAN), '0775', true);
        }
        
        if (yii::$app->request->isPost) {
            $post = yii::$app->request->post();
            $transaction = yii::$app->db->beginTransaction();

            //проверка данных пользователя
            if ($model->load($post)) {
                $model->username = $post['User']['username'];
                $model->email = $post['User']['email'];
                $model->first_name = $post['User']['first_name'];
                $model->last_name = $post['User']['last_name'];

                if (!empty($post['User']['password'])) {
                    $model->password = $post['User']['password'];
                }

                if (!$model->save()) {
                    $error = true;
                }

                $media = UploadedFile::getInstance($model, 'document');

                if (!is_null($media)) {
                    $temp = explode(".", $media->name);
                    $ext = end($temp);

                    $data = [
                        'Media' => [
                            'filename' => Yii::$app->security->generateRandomString() . ".{$ext}",
                            'filepath' => Media::getPath(Media::TYPE_DOCUMENT_SCAN),
                            'user_id' => $model->id,
                            'type' => Media::TYPE_DOCUMENT_SCAN,
                            'active' => Media::ACTIVE,
                            'created_at' => time(),
                            'updated_at' => time()
                        ]
                    ];

                    if ($mediaModel->load($data)) {
                        if (!$mediaModel->save()) {
                            Yii::$app->notifier->addNotifierErrorByModel($mediaModel);
                            $error = true;
                        } else {
                            $media->saveAs($data['Media']['filepath'] . '/' . $data['Media']['filename']);
                        }
                    } else {
                        Yii::$app->notifier->addNotifierErrorByModel($mediaModel);
                        $error = true;
                    }
                }

                $profileImage = UploadedFile::getInstance($model, 'profile_image');
                $modelProfileImage = Media::findOne(['user_id' => $model->id, 'type' => Media::TYPE_PROFILE_IMAGE]);

                if (!$modelProfileImage) {
                    $modelProfileImage = new Media();
                }

                if (!is_null($profileImage)) {
                    $temp = explode(".", $profileImage->name);
                    $ext = end($temp);

                    $data = [
                        'Media' => [
                            'filename' => Yii::$app->security->generateRandomString() . ".{$ext}",
                            'filepath' => Media::getPath(Media::TYPE_PROFILE_IMAGE),
                            'user_id' => $model->id,
                            'type' => Media::TYPE_PROFILE_IMAGE,
                            'active' => Media::ACTIVE,
                            'created_at' => time(),
                            'updated_at' => time()
                        ]
                    ];

                    if ($modelProfileImage->load($data)) {
                        if (!$modelProfileImage->save()) {
                            Yii::$app->notifier->addNotifierErrorByModel($modelProfileImage);
                            $error = true;
                        } else {
                            $profileImage->saveAs($data['Media']['filepath'] . '/' . $data['Media']['filename']);
                        }
                    } else {
                        Yii::$app->notifier->addNotifierErrorByModel($modelProfileImage);
                        $error = true;
                    }
                }
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
                $error = true;
            }

            if ($error) {
                $transaction->rollBack();
            } else {
                $this->redirect(Url::toRoute('/profile/control/', ['id' => $model->id]));
                Yii::$app->notifier->addNotifier(Yii::t('common', 'Профиль сохранён.'), 'success');
                $transaction->commit();
            }


        }

        $taskDataQuery = TaskQueue::find()->byUserId(yii::$app->user->id)->orderByDesc();
        $dataProviderTasks = new ActiveDataProvider([
            'query' => $taskDataQuery,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('@backend/modules/profile/views/index', [
            'model' => $model,
            'dataProviderTasks' => $dataProviderTasks,
            'documents' => Media::findAll([
                'user_id' => $model->id,
                'type' => Media::TYPE_DOCUMENT_SCAN,
                'active' => Media::ACTIVE
            ])
        ]);
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDeleteProfileImage()
    {
        $profileImage = Media::findOne(['user_id' => yii::$app->request->post('user_id'), 'type' => Media::TYPE_PROFILE_IMAGE]);

        if ($profileImage) {
            $profileImage->delete();
        }
    }

    public function actionDelete()
    {
        if (Media::deleteMedia(yii::$app->request->post('media_id'))) {
            return json_encode(['success' => true, 'message' => 'Данные успешно удалены', 'id' => yii::$app->request->post('media_id')], JSON_UNESCAPED_UNICODE);
        } else {
            return json_encode(['success' => false, 'message' => 'Ошибка удаления'], JSON_UNESCAPED_UNICODE);
        }
    }
}
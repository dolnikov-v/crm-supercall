<?php

namespace backend\modules\profile\controllers;

use backend\models\TaskQueue;
use backend\models\TaskQueueOrder;
use common\components\web\Controller;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * Class DownloadsController
 * @package backend\modules\profile\controllers
 */
class DownloadsController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionDelete()
    {

        $transaction = yii::$app->db->beginTransaction();
        $taskQueue_id = yii::$app->request->post('id');

        try {
            TaskQueueOrder::deleteAll(['task_queue_id' => $taskQueue_id]);
            TaskQueue::deleteAll(['id' => $taskQueue_id]);
        }catch (Exception $e){
            echo $e->getMessage();
            $transaction->rollBack();
        }

        $transaction->commit();

        return Json::encode([
            'success' => true,
            'message' => yii::t('common', 'Задание на экспорт успешно удалено')
        ]);
    }
}
<?php

use backend\models\TaskQueue;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\IdColumn;
use common\helpers\fonts\FontAwesome;
use common\helpers\grid\DataProvider;
use common\widgets\base\Label;
use common\widgets\base\LinkPager;
use common\widgets\base\Panel;
use common\components\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use backend\modules\profile\assets\DownloadsAsset;

/** @var \yii\data\ActiveDataProvider $dataProviderTasks */

DownloadsAsset::register($this);
?>
<?php Pjax::begin() ?>
<div class="table-responsive">
<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с загрузками'),
    'contentPadding' => false,
    'content' => GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped table-responsive'
        ],
        'dataProvider' => $dataProviderTasks,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->status == TaskQueue::STATUS_FAIL ? 'text-danger' : ''),
                'data-parent_id' => $model->id
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'label' => yii::t('common', 'Наименование'),
                'content' => function ($model) {
                    return $model->name;
                }
            ],
            [
                'attribute' => 'orders',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'text-center width-150'],
                'label' => yii::t('common', 'Заказы'),
                'content' => function ($model) {
                    return implode(", ", array_unique(ArrayHelper::getColumn($model->taskQueueOrders, 'order_id')));
                }
            ],
            [
                'attribute' => 'type',
                'headerOptions' => ['class' => 'text-center width-50'],
                'contentOptions' => ['class' => 'text-center width-50'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'headerOptions' => ['class' => 'text-center width-50'],
                'contentOptions' => ['class' => 'text-center width-50'],
                'content' => function ($model) {
                    $style = '';

                    switch ($model->status) {
                        case TaskQueue::STATUS_READY :
                            $style = Label::STYLE_WARNING;
                            break;
                        case TaskQueue::STATUS_IN_PROGRESS :
                            $style = Label::STYLE_INFO;
                            break;
                        case TaskQueue::STATUS_FAIL :
                            $style = Label::STYLE_DANGER;
                            break;
                        case TaskQueue::STATUS_COMPLETED :
                            $style = Label::STYLE_SUCCESS;
                            break;
                    }

                    return Label::widget([
                        'label' => TaskQueue::getStatus($model->status),
                        'style' => $style,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'error',
                'headerOptions' => ['class' => 'text-center width-150'],
                'contentOptions' => ['class' => 'width-150'],
                'content' => function ($model) {
                    return $model->error;
                }
            ],
            [
                'class' => DateColumn::className(),
                'formatType' => 'Datetime',
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'formatType' => 'Datetime',
                'attribute' => 'updated_at',
            ],
            [
                'attribute' => 'path',
                'enableSorting' => false,
                'contentOptions' => function () {
                    return ['class' => 'text-center'];
                },
                'content' => function ($model) {
                    if (empty($model->path)) {
                        return '';
                    } else {
                        if(in_array($model->type, [TaskQueue::TYPE_STATS_OPERATOR_IMPORT, TaskQueue::TYPE_STATS_ORDER_IMPORT, TaskQueue::TYPE_STATS_OPERATOR_ONLINE_IMPORT])){
                            $href = Url::toRoute(['/download/get-file/', 'file' => $model->path]);
                        }elseif ($model->type == TaskQueue::TYPE_CALL_RECORD_IMPORT)
                            $href = Url::toRoute(['/download/get-file-from-url/', 'file' => $model->path]);
                        else{
                            $href = $model->path;
                        }
                        $title = Html::tag('i', '', ['class' => FontAwesome::FILE_ARCHIVE_O]) . yii::t('common', 'Скачать');
                        return Html::tag('a', $title, ['href' => $href, 'data' => ['pjax' => 0]]);
                    }
                }
            ],
            [
                'class' => ActionColumn::className(),
                'headerOptions' => ['class' => 'width-150 text-center'],
                'items' => [
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function ($model) {
                            return Url::toRoute(['#']);
                        },
                        'can' => function ($model) {
                            return $model->status == TaskQueue::STATUS_READY;
                        },
                        'attributes' => function ($model) {
                            return [
                                'data-action' => 'delete',
                                'data-id' => $model->id
                            ];
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => LinkPager::widget(['pagination' => $dataProviderTasks->getPagination()]),
]) ?>
</div>
<?php Pjax::end() ?>

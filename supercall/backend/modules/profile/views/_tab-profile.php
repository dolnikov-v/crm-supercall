<?php

use common\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\file\FileInput;
use backend\assets\SiteAsset;
use common\components\web\View;
use common\models\Media;
use backend\modules\profile\assets\ProfileAsset;
use common\models\User;

$this->title = Yii::t('common', 'Профиль');
$this->params['breadcrumbs'][] = ['label' => $this->title];

/** @var \common\models\User $model */
/** @var [] Media $documents */
ProfileAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['/profile/control/index', 'id' => $model->id]),
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-12">
        <div  style="padding:10px" class="alert-info">
            <?= Yii::t('common', 'Если вы не хотите менять пароль, то необходимо оставить поле «Пароль» пустым.'); ?><br/>
            <?= Yii::t('common', 'Для смены картинки профиля - кликните по картинке профиля, загрузите изображение и сохраните профиль.');?>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'username')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'first_name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'last_name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'email')->textInput() ?>
    </div>

    <div class="col-lg-3">
        <?= $form->field($model, 'password')->passwordInputGenerate([
            'value' => '',
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'document')->widget(FileInput::classname(), [
            'options' => [
                'accept' => 'image/*',
            ],
            'pluginOptions' => [
                'showUpload' => false,
                'browseLabel' => '',
                'removeLabel' => '',
                'allowedFileExtensions' => ['jpg', 'png', 'jpeg'],
            ]
        ]) ?>
    </div>

    <div class="col-lg-3 place-avatar"></div>

    <div class="">
        <?=$this->render('@common/modules/profile/views/layout');?>
    </div>

</div>

<div class="row">

    <?php if (is_array($documents)): ?>

        <?php foreach ($documents as $document) : ?>

            <div class="col-md-2 col-sm-2 col-xs-2 media-<?= $document->id; ?>">.
                <img src="<?= Media::getAbsolutePath(Media::TYPE_DOCUMENT_SCAN) . '/' . $document->filename; ?>"
                     style="width:100%" class="img-rounded"><br/>

                <?php if (!in_array(User::ROLE_OPERATOR, array_keys(Yii::$app->authManager->getRolesByUser(yii::$app->user->id)))) : ?>

                    <br/>
                    <button class=" media-target btn btn-danger form-control" data-media="<?= $document->id; ?>">
                        <span class="lyphicon glyphicon-remove"></span><?= yii::t('common', 'Удалить'); ?>
                    </button>

                <?php endif; ?>

                <br/><br/>
            </div>

        <?php endforeach; ?>

    <?php endif; ?>

</div>
<br/>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Сохранить профиль')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

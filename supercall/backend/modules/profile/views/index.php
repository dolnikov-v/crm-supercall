<?php

use common\widgets\base\Panel;
use yii\bootstrap\Tabs;

/** @var \common\models\User $model */
/** @var array \common\models\Media $documents */
/** @var \yii\data\ActiveDataProvider $dataProviderTasks */
?>

<?= Panel::widget([
    'title' => '',
    'contentPadding' => true,
    'content' =>
        Tabs::widget([
            'items' => [
                [
                    'label' => Yii::t('common', 'Данные профиля'),
                    'content' => $this->render('_tab-profile', [
                        'model' => $model,
                        'documents' => $documents
                    ]),
                    'active' => true
                ],
                [
                    'label' => Yii::t('common', 'Загрузки'),
                    'options' => [
                        'id' => 'downloads'
                    ]
                    ,
                    'content' => $this->render('_tab-downloads', [
                        'dataProviderTasks' => $dataProviderTasks
                    ]),
                    'active' => false
                ],
            ]])
]); ?>

<?php

namespace backend\modules\profile;

use yii\base\Module as BaseModule;

/**
 * Class Module
 * @package backend\modules\profile
 */
class Module extends BaseModule
{    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}

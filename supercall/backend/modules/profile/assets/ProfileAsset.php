<?php

namespace backend\modules\profile\assets;

use yii\web\AssetBundle;

class ProfileAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/profile/';

    public $js = [
        'profile.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
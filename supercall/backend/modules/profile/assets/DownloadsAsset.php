<?php

namespace backend\modules\profile\assets;

use yii\web\AssetBundle;

/**
 * Class DownloadsAsset
 * @package backend\modules\profile\assets
 */
class DownloadsAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/profile/downloads/';

    public $js = [
        'downloads.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
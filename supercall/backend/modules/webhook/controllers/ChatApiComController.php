<?php

namespace backend\modules\webhook\controllers;

use common\components\ChatApiCom;
use console\controllers\LiveMessengerController;
use yii;
use yii\helpers\Json;
use yii\web\Controller;

/**
 * Class ChatApiComController
 * @package backend\modules\webhook\controllers
 */
class ChatApiComController extends Controller
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['index'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     *  срабатывание вебхука для получения новых сообщений
     */
    public function actionIndex()
    {
        try {
            $c = new LiveMessengerController(Yii::$app->controller->id, Yii::$app);
            $c->actionGetMessages();
        }catch (yii\base\Exception $e){
            echo Json::encode([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        echo Json::encode([
            'success' => true,
            'message' => 'Webhook is worked'
        ]);
    }
}
<?php
namespace backend\modules\webhook\controllers;

use backend\components\web\Controller;
use common\components\Amqp;
use Yii;
use yii\di\Instance;
use yii\helpers\Json;

/**
 * Class LogController
 * @package backend\modules\webhook\controllers
 */
class LogController extends Controller
{
    /**
     * @var Amqp
     */
    public $amqp;


    public function init()
    {
        parent::init();

        $this->amqp = Instance::ensure($this->amqp, Amqp::class);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['save-log'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionSaveLog()
    {
        $log = Yii::$app->request->post('log');
        $queue = Yii::$app->request->post('queue');

        //if ($log) {
            $this->amqp->saveLog($log, $queue);
        //}

        return Json::encode(['log' => $log, 'queue' => $queue]);
    }
}
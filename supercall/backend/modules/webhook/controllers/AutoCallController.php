<?php

namespace backend\modules\webhook\controllers;

use backend\modules\queue\models\Queue;
use common\components\Amqp;
use common\components\Logstash;
use common\modules\partner\models\Partner;
use Yii;
use yii\di\Instance;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Controller;

class AutoCallController extends Controller
{
    /**
     * @var Amqp
     */
    public $amqp;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->amqp = Instance::ensure($this->amqp, Amqp::class);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['call-status'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }


    /**
     * метод на время тестирования
     * @return string
     */
    public function actionCallStatus()
    {
        $params['method'] = __METHOD__;
        $params['number'] = 0;
        $params = Yii::$app->request->get();
        //$this->amqp->saveLog($params);

        $wtrade = Partner::find()->where(['name' => '2wtrade'])->one();

        if ($wtrade) {
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('get')
                ->setUrl('http://api.supercall.local/v1/auto-call/call-status?' . http_build_query($params))
                ->setHeaders(['Authorization' => 'Basic ' . base64_encode($wtrade->api_key)])
                ->send();

            //$this->amqp->saveLog(['response Api' => $response->content]);

            if ($response->isOk) {
                $result = [
                    'success' => true,
                    'message' => $response->content
                ];
            } else {
                $result = [
                    'success' => false,
                    'message' => $response->content
                ];
            }

        } else {
            //$this->amqp->saveLog('Partner 2wtrade not found');
            $result = [
                'success' => false,
                'message' => 'Partner 2wtrade not found'
            ];
        }

        return Json::encode($result);
    }
}
<?php
namespace backend\modules\webhook;

use api\components\filters\VerbFilter;
use backend\components\base\Module as BackendModule;
use yii\filters\AccessControl;

class Module extends BackendModule
{
    public function init()
    {
        parent::init();
    }

    public function behaviors(){
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'call-status' => ['GET'],
                    'save-log' => ['POST', 'GET'],
                    'index' => ['POST', 'GET'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['call-status', 'save-log', 'index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }
}
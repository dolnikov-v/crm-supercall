<?php
namespace backend\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class RoleAsset
 * @package backend\modules\access\assets
 */
class RoleAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/access/base/role';

    public $js = [
        'role.js',
    ];

    public $depends = [
        'backend\assets\base\ThemeAsset',
    ];
}

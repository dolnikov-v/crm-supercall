<?php
namespace backend\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class PermissionAsset
 * @package backend\modules\access\assets
 */
class PermissionAsset extends AssetBundle
{
    public $sourcePath = '@common/web/resources/modules/access/widgets/permission';

    public $js = [
        'permission.js',
    ];

    public $depends = [
        'common\assets\vendor\SwitcheryAsset',
    ];
}

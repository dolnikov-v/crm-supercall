<?php
namespace backend\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class ListUsersOfTeamAsset
 * @package backend\modules\access\assets
 */
class ListUserEditAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/access/user/';

    public $js = [
        'list-user-edit.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
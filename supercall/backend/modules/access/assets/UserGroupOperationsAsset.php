<?php
namespace backend\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class UserGroupOperationsAsset
 * @package backend\modules\access\assets
 */
class UserGroupOperationsAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/access/user/';

    public $js = [
        'user-group-operations.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
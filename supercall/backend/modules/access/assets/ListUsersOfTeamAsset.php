<?php

namespace backend\modules\access\assets;

use yii\web\AssetBundle;

/**
 * Class ListUsersOfTeamAsset
 * @package backend\modules\access\assets
 */
class ListUsersOfTeamAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/access/team/';

    public $js = [
        'list-users-of-team.js',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
<?php
namespace backend\modules\access\widgets;

use yii\base\Widget;

/**
 * Class Permission
 * @package app\modules\access\widgets
 */
class Permission extends Widget
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    public $permissionList;

    /**
     * @var
     */
    public $permissionsByRole;

    /**
     * @var bool
     */
    public $disabled = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('permission', [
            'model' => $this->model,
            'permissionList' => $this->permissionList,
            'permissionsByRole' => $this->permissionsByRole,
        ]);
    }
}

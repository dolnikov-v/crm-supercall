<?php

namespace backend\modules\access\widgets;

use yii\base\Widget;

/**
 * Class UserGroupOperations
 * @package backend\modules\access\widgets
 */
class UserGroupOperations extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('user-group-operations');
    }
}
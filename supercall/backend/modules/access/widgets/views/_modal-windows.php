<?php
//Файл содержит описание модальных окон групповых операций
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
?>

<!-- base modal-->

<?php Modal::begin([
    'id' => 'modal_confirm_group_operations',
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4>' . yii::t('common', 'Подтверждение') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-success', 'id' => 'modal_confirm_group_operations_ok'])
        . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-danger', 'data-dismiss' => 'modal'])
]); ?>

<div class="row">
    <div class="col-lg-12"><?=yii::t('common', 'Вы действительно собираетесь выполнить данную операцию ?')?></div>
</div>

<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'modal_warning_group_operations',
    'header' => '<h4>' . yii::t('common', 'Предупреждение') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Закрыть'), ['class' => 'btn btn-danger', 'data-dismiss' => 'modal'])
]); ?>

<div class="row">
    <div class="col-lg-12"><?=yii::t('common', 'Пльзователи не выбраны')?></div>
</div>

<?php Modal::end(); ?>


<?php Modal::begin([
    'id' => 'modal_progress_group_operations',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
    'size' => Modal::SIZE_LARGE,
    'header' => '<h4>' . yii::t('common', 'Прогресс') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Закрыть'), ['class' => 'btn btn-info', 'data-dismiss' => 'modal', 'id' => 'close_progress', 'disabled' => true])
]); ?>
<div class="progress-success">
    <div class="progress-bar progress-bar-indicating  progress-bar-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
</div>
<hr/>
<div class="group_errors text-danger"></div>
<br/>
<?php Modal::end(); ?>


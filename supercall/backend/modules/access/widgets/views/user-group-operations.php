<?php

use backend\modules\access\assets\UserGroupOperationsAsset;
use common\widgets\base\Button;
use backend\modules\access\models\UserGroupOperations;

UserGroupOperationsAsset::register($this);

?>

    <div class="row group-operations">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-2">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_DANGER . ' width-200 ' . (!yii::$app->user->can('access.usergroupoperations.blockusers') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Блокировать'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'block_users_button',
                            'data-action' => 'BlockUsers',
                            'data-url' => UserGroupOperations::OPERATION_BLOCK_USERS
                        ]
                    ]) ?>
                </div>
                <div class="col-lg-2">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_DANGER . ' width-200 ' . (!yii::$app->user->can('access.usergroupoperations.offwebrtc') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Отключить WebRTC'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'off_webrtc_button',
                            'data-action' => 'OffWebRTC',
                            'data-url' => UserGroupOperations::OPERATION_OFF_WEBRTC
                        ]
                    ]) ?>
                </div>
                <div class="col-lg-2">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_DANGER . ' width-200 ' . (!yii::$app->user->can('access.usergroupoperations.offautocall') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Отключить Автодозвон'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'off_autocall_button',
                            'data-action' => 'OffAutoCall',
                            'data-url' => UserGroupOperations::OPERATION_OFF_AUTOCALL
                        ]
                    ]) ?>
                </div>
                <div class="col-lg-2">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_DANGER . ' width-200 ' . (!yii::$app->user->can('access.usergroupoperations.deleteusers') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Удалить'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'delete_users_button',
                            'data-action' => 'DeleteUsers',
                            'data-url' => UserGroupOperations::OPERATION_DELETE_USERS
                        ]
                    ]) ?>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-2">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_INFO . ' width-200 ' . (!yii::$app->user->can('access.usergroupoperations.activateusers') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Разблокировать'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'activate_users_button',
                            'data-action' => 'ActivateUsers',
                            'data-url' => UserGroupOperations::OPERATION_ACTIVATE_USERS
                        ]
                    ]) ?>
                </div>
                <div class="col-lg-2">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_INFO . ' width-200 ' . (!yii::$app->user->can('access.usergroupoperations.onwebrtc') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Включить WebRTC'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'on_webrtc_button',
                            'data-action' => 'OnWebRTC',
                            'data-url' => UserGroupOperations::OPERATION_ON_WEBRTC
                        ]
                    ]) ?>
                </div>
                <div class="col-lg-2">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_INFO . ' width-200 ' . (!yii::$app->user->can('access.usergroupoperations.onautocall') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Включить Автодозвон'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'on_autocall_button',
                            'data-action' => 'OnAutoCall',
                            'data-url' => UserGroupOperations::OPERATION_ON_AUTOCALL
                        ]
                    ]) ?>
                </div>
                <div class="col-lg-2">
                    <?= Button::widget([
                        'size' => Button::SIZE_SMALL,
                        'style' => Button::STYLE_INFO . ' width-200 ' . (!yii::$app->user->can('access.usergroupoperations.undeleteusers') ? 'hidden' : ''),
                        'label' => yii::t('common', 'Восстановить'),
                        'icon' => '',
                        'attributes' => [
                            'id' => 'un_delete_button',
                            'data-action' => 'UnDeleteUsers',
                            'data-url' => UserGroupOperations::OPERATION_ON_AUTOCALL
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

<?= $this->render('_modal-windows'); ?>
<?php
use backend\modules\access\assets\PermissionAsset;
use common\widgets\base\Switchery;
use common\models\User;

/** @var \backend\modules\access\models\AuthItem $model */
/** @var \yii\rbac\Permission[] $permissionList */
/** @var array $permissionsByRole */
/** @var bool $disabled */

PermissionAsset::register($this);
?>

<div class="no-padding">
    <table id="table_allow_permissions" class="table table-striped table-hover"
           data-role="<?= $model->name ?>"
           data-i18n-success="<?= Yii::t('common', 'Действие выполненно') ?>"
           data-i18m-fail="<?= Yii::t('common', 'Действие не выполнено') ?>">
        <?php if (!empty($permissionList)): ?>
            <thead>
            <tr>
                <th class="width-300"><?= Yii::t('common', 'Операция') ?></th>
                <th><?= Yii::t('common', 'Описание') ?></th>
                <th class="text-center width-150"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($permissionList as $permission): ?>
                <tr class="tr-vertical-align-middle tr-permission" data-name="<?= $permission->name ?>">
                    <td><?= $permission->name ?></td>
                    <td><?= Yii::t('common', $permission->description) ?></td>
                    <td class="text-center" data-permission="<?= $permission->name ?>">
                        <?= Switchery::widget([
                            'checked' => $model->name == User::ROLE_SUPERADMIN ? true : in_array($permission->name, $permissionsByRole),
                            'disabled' => ($model->name == User::ROLE_SUPERADMIN) ? true : false,
                        ]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        <?php else: ?>
            <tr>
                <td class="text-center"><?= Yii::t('common', 'Операции отсутствуют') ?></td>
            </tr>
        <?php endif; ?>
    </table>
</div>

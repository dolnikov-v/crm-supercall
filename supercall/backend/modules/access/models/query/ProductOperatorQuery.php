<?php
namespace backend\modules\access\models\query;

use common\components\db\ActiveQuery;
use backend\modules\access\models\ProductOperator;

/**
 * Class ProductOperatorQuery
 * @package common\models\query
 * @method ProductOperator one($db = null)
 * @method ProductOperator [] all($db = null)
 */
class ProductOperatorQuery extends ActiveQuery
{
    /**
     * @param [] $product_ids
     * @return $this
     */
    public function byProducts($product_ids)
    {
        return $this->andWhere(['in', 'product_id' => $product_ids]);
    }

    /**
     * @param integer $user_id
     * @return $this
     */
    public function byUser($user_id)
    {
        return $this->andWhere(['user_id' => $user_id]);
    }
}

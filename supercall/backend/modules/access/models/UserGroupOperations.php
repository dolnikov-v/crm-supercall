<?php
namespace backend\modules\access\models;

use common\components\db\ActiveRecord;
use common\models\User;

/**
 * Class UserGroupOperations
 * @package backend\modules\access\models
 */
class UserGroupOperations extends ActiveRecord
{
    const LOGGER_TYPE = 'user_group_operations';

    const OPERATION_BLOCK_USERS = '/access/user-group-operations/block-users';
    const OPERATION_ACTIVATE_USERS = '/access/user-group-operations/activate-users';
    const OPERATION_ON_WEBRTC = '/access/user-group-operations/on-webrtc';
    const OPERATION_OFF_WEBRTC = '/access/user-group-operations/off-webrtc';
    const OPERATION_ON_AUTOCALL = '/access/user-group-operations/on-auto-call';
    const OPERATION_OFF_AUTOCALL = '/access/user-group-operations/off-auto-call';
    const OPERATION_DELETE_USERS = '/access/user-group-operations/delete-users';
    const OPERATION_UNDELETE_USERS = '/access/user-group-operations/un-delete-users';

    /**
     * @param $id
     * @return \common\models\UserCountry
     */
    public static function getUserBy($id){
        return User::find()->where(['id' => $id])->one();
    }
}
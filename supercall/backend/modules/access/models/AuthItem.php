<?php
namespace backend\modules\access\models;

use common\components\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Item;

/**
 * Class AuthItem
 * @package app\models
 * @property string $name
 * @property string $type
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 */
class AuthItem extends ActiveRecord
{
    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auth_item}}';
    }

    /**
     * @param integer $type
     * @param string $key
     * @param string $value
     * @return array
     */
    public static function getCollection($type, $key = 'name', $value = 'description')
    {
        if ($type == Item::TYPE_ROLE) {
            $items = Yii::$app->authManager->getRoles();
        } else {
            $items = Yii::$app->authManager->getPermissions();
        }

        return ArrayHelper::map($items, $key, $value);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['type', 'name', 'description'], 'required'],
            ['type', 'integer'],
            ['description', 'string'],
            ['name', 'string', 'max' => '64', 'message' => Yii::t('common', 'Максимальная длина поля «Название» составаляет 64 символа.')],
            ['name', 'validateName'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Название'),
            'description' => Yii::t('common', 'Описание'),
            'type' => Yii::t('common', 'Тип'),
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateName($attribute, $params)
    {
        if (empty($this->name)) {
            $this->addError($attribute, Yii::t('common', 'Необходимо указать «Название».'));
        } else {
            $this->name = mb_strtolower($this->name);

            $role = Yii::$app->authManager->getRole($this->name);
            $permission = Yii::$app->authManager->getPermission($this->name);

            if (!empty($role) || !empty($permission)) {
                $this->addError($attribute, Yii::t('common', 'Запись с таким названием уже существует.'));
            } else {
                if (!preg_match("/^[a-z0-9\.]+$/", $this->name)) {
                    $this->addError($attribute, Yii::t('common', 'Поле «Название» должно состоять только из букв латинского алфавита и символа точка.'));
                }

                if (preg_match("/^\./", $this->name)) {
                    $this->addError($attribute, Yii::t('common', 'Поле «Название» не должно начинаться с символа точка.'));
                }
            }
        }
    }

    /**
     * @param $itemName
     * @return bool
     */
    public static function checkRelation($itemName)
    {
        return AuthAssignment::find()
            ->where(['item_name' => $itemName])
            ->exists();
    }

    /**
     * @param $name
     * @param $description
     * @return \yii\rbac\Permission
     */
    public static function addPermission($name, $description)
    {
        $permission = Yii::$app->authManager->createPermission($name);
        $permission->description = $description;
        Yii::$app->authManager->add($permission);

        return $permission;
    }
}

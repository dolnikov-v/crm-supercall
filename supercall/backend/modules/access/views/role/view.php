<?php
use backend\modules\access\widgets\Permission;
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \backend\modules\access\models\AuthItem $model */
/** @var \yii\rbac\Permission[] $permissionList */
/** @var array $permissionsByRole */

$this->title = Yii::t('common', 'Просмотр роли');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список ролей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Роль'),
    'content' => $this->render('_view-form', [
        'model' => $model,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список операций'),
    'content' => Permission::widget([
        'model' => $model,
        'permissionList' => $permissionList,
        'permissionsByRole' => $permissionsByRole,
        'disabled' => true,
    ]),
]) ?>

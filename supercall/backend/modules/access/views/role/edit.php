<?php
use backend\modules\access\assets\RoleAsset;
use backend\modules\access\widgets\Permission;
use common\widgets\base\DropdownActions;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\web\View;

/** @var \yii\web\View $this */
/** @var \yii\rbac\Permission[] $permissionList */
/** @var array $permissionsByRole */
/** @var \backend\modules\access\models\AuthItem $model */
/** @var \backend\modules\access\models\search\AuthItemSearch $modelSearch */

RoleAsset::register($this);

Yii::$app->view->registerJs("var Vars = {};", View::POS_HEAD);
Yii::$app->view->registerJs("Vars['sections'] = " . json_encode($modelSearch->sections) . ";", View::POS_HEAD);

$this->title = Yii::t('common', $model->isNewRecord ? 'Добавление роли' : 'Редактирование роли');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список ролей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Роль'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]) ?>

<?php if (!$model->isNewRecord): ?>
    <?php
    $links = [];

    foreach ($modelSearch->sectionsCollection as $sectionName => $sectionLabel) {
        $links[] = [
            'url' => '#',
            'style' => 'permission-filter-section',
            'label' => $sectionLabel,
            'attributes' => [
                'data-name' => $sectionName,
            ],
        ];
    }
    ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Список операций'),
        'contentPadding' => false,
        'actions' => DropdownActions::widget([
            'label' => Yii::t('common', 'Раздел'),
            'links' => $links,
            'attributes' => [
                'data-default-name' => Yii::t('common', 'Список операций'),
            ],
        ]),
        'content' => Permission::widget([
            'model' => $model,
            'permissionList' => $permissionList,
            'permissionsByRole' => $permissionsByRole,
        ]),
    ]) ?>
<?php endif; ?>

<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \backend\modules\access\models\AuthItem $model */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'name')->textInput(['disabled' => 'disabled']); ?>
    </div>
    <div class="col-md-8">
        <?= $form->field($model, 'description')->textInput(['disabled' => 'disabled']); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->link(Yii::t('common', 'К списку ролей'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>



<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\helpers\grid\DataProvider;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список ролей');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с ролями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Название'),
                'headerOptions' => ['class' => 'width-200'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'description',
                'label' => Yii::t('common', 'Описание'),
                'content' => function ($data) {
                    return Yii::t('common', $data->description);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'createdAt',
                'label' => Yii::t('common', 'Дата добавления'),
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть'),
                        'url' => function ($data) {
                            return Url::toRoute(['view', 'name' => $data->name]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.role.view');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'name' => $data->name]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.role.edit');
                        },
                    ],
                    [
                        'can' => function () {
                            return (Yii::$app->user->can('access.role.view')
                                || Yii::$app->user->can('access.role.edit'))
                            && Yii::$app->user->can('access.role.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить роль'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'name' => $data->name]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.role.delete');
                        },
                    ],
                ]
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider
    ]),
]) ?>

<?= ModalConfirmDelete::widget() ?>

<?php
use common\widgets\base\ButtonLink;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<?php if (Yii::$app->user->can('access.role.edit') || $dataProvider->getPagination()->pageCount > 1): ?>
    <?php if (Yii::$app->user->can('access.role.edit')): ?>
        <?= ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить роль'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL
        ]); ?>
    <?php endif; ?>
    <?= LinkPager::widget(['pagination' => $dataProvider->getPagination()]) ?>
<?php endif; ?>

<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \backend\modules\access\models\AuthItem $model */
?>

<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ? Url::toRoute(['edit']) : Url::toRoute(['edit', 'name' => urlencode($model->name)])
]); ?>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <?= $form->field($model, 'name')->textInput(); ?>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <?= $form->field($model, 'description')->textInput(); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить операцию') : Yii::t('common', 'Сохранить операцию')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

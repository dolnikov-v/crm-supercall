<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \backend\modules\access\models\AuthItem $model */

$this->title = Yii::t('common', $model->isNewRecord ? 'Добавление операции' : 'Редактирование операции');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список операций'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Операция'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]) ?>

<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\helpers\grid\DataProvider;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \backend\modules\access\models\AuthItem $searchModel */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список операций');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'contentVisible' => $contentVisible,
    'content' => $this->render('_index-form', [
        'modelSearch' => $searchModel,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с операциями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'label' => Yii::t('common', 'Операция'),
                'headerOptions' => ['class' => 'width-200'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'description',
                'label' => Yii::t('common', 'Описание'),
                'content' => function ($data) {
                    return Yii::t('common', $data->description);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'attribute' => 'createdAt',
                'label' => Yii::t('common', 'Дата добавления'),
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'name' => urlencode($data->name)]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.permission.edit');
                        },
                    ],
                    [
                        'can' => function () {
                            return Yii::$app->user->can('access.permission.edit') && Yii::$app->user->can('access.permission.delete');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить операцию'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($data) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'name' => urlencode($data->name)]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.permission.delete');
                        },
                    ],
                ]
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider,
    ]),
]) ?>

<?= ModalConfirmDelete::widget() ?>

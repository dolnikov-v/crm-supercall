<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \backend\modules\access\models\search\AuthItemSearch $modelSearch */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute('index'), 'method' => 'get']); ?>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <?= $form->field($modelSearch, 'section')->select2List($modelSearch->sectionsCollection, [
                'prompt' => '—'
            ]); ?>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="form-group">
            <?= $form->field($modelSearch, 'name')->textInput(); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

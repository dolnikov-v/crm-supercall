<?php

use common\widgets\base\ButtonLink;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\data\ArrayDataProvider $dataProvider */
?>

<?php if (Yii::$app->user->can('permission.edit') || $dataProvider->getPagination()->pageCount > 1): ?>
    <div class="row">
        <div class="col-md-6">
            <?php if (Yii::$app->user->can('permission.edit')): ?>
                <?= ButtonLink::widget([
                    'url' => Url::toRoute('edit'),
                    'label' => Yii::t('common', 'Добавить операцию'),
                    'size' => ButtonLink::SIZE_SMALL,
                    'style' => ButtonLink::STYLE_SUCCESS
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="col-md-6 text-right">
            <?= LinkPager::widget(['pagination' => $dataProvider->getPagination()]) ?>
        </div>
    </div>


<?php endif; ?>

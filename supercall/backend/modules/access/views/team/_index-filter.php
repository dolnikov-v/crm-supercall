<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-3 col-sm-3 col-xs-4">
        <?= $form->field($searchModel, 'team_id')->select2List($teams, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($searchModel, 'country_id')->select2List($countries, [
            'multiple' => true
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute(['index', 'reset' => true])); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php $this->registerCss("
    .select2-search--hide {
        display: block !important; 
    }
") ?>
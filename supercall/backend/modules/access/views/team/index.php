<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\models\TeamsCountry;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\widgets\base\Label;
use common\models\Teams;
use common\widgets\base\ButtonLink;
use yii\widgets\LinkPager;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \common\models\search\TeamSearch $searchModel */
/** @var array $roles */

$this->title = Yii::t('common', 'Список команд');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'showButtons' => true,
    'content' => $this->render('_index-filter', [
        'searchModel' => $searchModel,
        'teams' => $teams,
        'countries' => $countries
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с командами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            /** @var User $model */
            return [
                'class' => 'tr-vertical-align-middle ' . (!$model->active ? 'text-muted' : ''),
            ];
        },
        'columns' => [
            ['class' => IdColumn::className()],
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'country_id',
                'enableSorting' => false,
                'content' => function($model){
                    return implode(', ', ArrayHelper::getColumn(TeamsCountry::getCountryByTeam($model->id), 'country.name'));
                }
            ],
            [
                'attribute' => 'active',
                'headerOptions' => ['class' => 'width-150 text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'content' => function ($model) {
                    return Label::widget([
                        'label' => Teams::getStatus($model['active']),
                        'style' => $model['active'] ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'withRelativeTime' => true,
                'attribute' => 'created_at',
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.team.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Активировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/queue/control/activate', 'id' => $model['id']]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('queue.control.activate') && !$model['active'];
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Деактивировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/queue/control/deactivate', 'id' => $model['id']]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('queue.control.deactivate') && $model['active'];
                        }
                    ],
                    [
                        'can' => function ($model) {
                            return Yii::$app->user->can('access.team.edit')
                                || (Yii::$app->user->can('access.team.activate') && !$model['active'])
                                || (Yii::$app->user->can('access.team.deactivate') && $model['active']);
                        }
                    ],
                ]
            ],
        ]
    ]),
    'footer' => (Yii::$app->user->can('access.team.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить команду'),
            'url' => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size' => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>
<?= ModalConfirmDelete::widget(); ?>


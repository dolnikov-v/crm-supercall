<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\Teams $model */
/** @var array $roles */

$this->title = !$model->isNewRecord ? Yii::t('common', 'Редактирование команды') : Yii::t('common', 'Добавление команды');

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список команд'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные команды'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'countries' => $countries
    ]),
]) ?>

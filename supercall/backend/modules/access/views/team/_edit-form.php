<?php
use common\models\TeamsCountry;
use common\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\modules\access\assets\ListUsersOfTeamAsset;

/** @var \common\models\User $model */
/** @var array $currentRoles */
/** @var array $roles */
/** @var array $countries */

ListUsersOfTeamAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['index']),
    'enableClientValidation' => false,
]); ?>
<div class="row">


    <?= $form->field($model, 'id')->hiddenInput(['value' => ($model->isNewRecord ? '' : $model->id)])->label(false) ?>

    <div class="col-lg-3">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>

    <div class="col-lg-4">
            <?= $form->field($model, 'country_id')->select2List($countries, [
                    'value' => ArrayHelper::getColumn(TeamsCountry::getCountryByTeam($model->id), 'country_id'),
                'multiple' => true
            ]); ?>
    </div>

</div>

<?php if(!$model->isNewRecord && yii::$app->user->can('access.team.manageuserofteam')) : ?>
    <hr/>
    <div class="row">
        <div class="col-lg-12">
        <fieldset><h4><?=yii::t('common', 'Список пользователей')?></h4>
            <div class="alert alert-warning compatibility hidden"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i> <?=yii::t('common', 'Проверка совместимости пользователей')?></div><br/>
            <div class="alert alert-danger excluded_users hidden"><h4><?=yii::t('common', 'Исключаемые пользователи')?></h4><div class="excluded_users_list"></div></div><br/>

            <div class="list-users-of-team">
            </div>
        </fieldset>
            <i class="fa fa-spinner fa-spin block-loader hidden" style="font-size:45px"></i>
        </div>
    </div>
    <br/>

<?php endif;?>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(!$model->isNewRecord ? Yii::t('common', 'Сохранить команду') : Yii::t('common', 'Добавить команду')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

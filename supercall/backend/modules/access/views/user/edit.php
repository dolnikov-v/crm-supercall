<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \common\models\User $model */
/** @var array $currentRoles */
/** @var array $roles */

$this->title = !$model->isNewRecord ? Yii::t('common', 'Редактирование пользователя') : Yii::t('common', 'Добавление пользователя');

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Список пользователей'), 'url' => Url::toRoute(['index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Данные пользователя'),
    'alert' => !$model->isNewRecord ? Yii::t('common', 'Если вы не хотите менять пароль, то необходимо оставить поле «Пароль» пустым.') : '',
    'content' => $this->render('_edit-form', [
        'model'            => $model,
        'currentRoles'     => $currentRoles,
        'roles'            => $roles,
        'teams'            => $teams,
        'countries'        => $countries,
        'currentCountries' => $currentCountries,
        'partners'         => $partners,
        'currentPartners'  => $currentPartners,
    ]),
]) ?>

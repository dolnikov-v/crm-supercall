<?php
use backend\modules\access\assets\ListUserEditAsset;
use common\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var \common\models\User $model */
/** @var array $currentRoles */
/** @var array $roles */

ListUserEditAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'username')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'first_name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'last_name')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'password')->passwordInputGenerate([
            'value' => '',
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'roles')->select2List($roles, [
            'value' => $currentRoles,
            'multiple' => true,
        ])->label(Yii::t('common', 'Роли')) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'team_id')->select2List($teams, [
            'value' => ArrayHelper::getColumn($model->getTeams(), 'id'),
            'multiple' => true,
        ])->label(Yii::t('common', 'Команда')) ?>
    </div>
    <div class="col-lg-2">
      <?= $form->field($model, 'auto_call')->checkbox(['checked' => $model->auto_call, 'value' => 1, 'unchecked_value' => 0]); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'incoming_call')->checkbox(['checked' => $model->incoming_call, 'value' => 1, 'unchecked_value' => 0]); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'webrtc')->checkbox(['checked' => $model->webrtc, 'value' => 1, 'unchecked_value' => 0]); ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'use_messenger')->checkbox(['checked' => $model->use_messenger, 'value' => 1, 'unchecked_value' => 0]); ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <?= $form->field($model, 'email')->textInput() ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'country')->select2List($countries, [
                'value' => $currentCountries,
                'multiple' => true,
        ]) ?>
        <u><?=html::a(Yii::t('common', 'Выбрать все страны'), null,['class'=>'check-all-country', 'style' => 'position: relative; top: -10px;']);?></u>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'partner')->select2List($partners, [
                'value' => $currentPartners,
                'multiple' => true,
        ]) ?>
    </div>
</div>
<?php if(!empty($model->sipArray)):?>
<div class="row">
    <?php foreach ($model->sipArray as $key => $sip): ?>
        <div class="col-lg-4">
            <div class="form-group">
                <?= Html::label($key) ?>
                <?= Html::textInput("User[sipArray][$key]", $sip, [
                    'class' => 'form-control  ',
                    'disabled' => !Yii::$app->user->can('access.user.sipedit')
                ]); ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php endif;?>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(!$model->isNewRecord ? Yii::t('common', 'Сохранить пользователя') : Yii::t('common', 'Добавить пользователя')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

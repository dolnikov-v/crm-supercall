<?php
use common\widgets\base\ButtonLink;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var \yii\data\ActiveDataProvider $dataProvider */
?>

<?php if (Yii::$app->user->can('access.user.edit') || $dataProvider->getPagination()->pageCount > 1) { ?>
    <div class="box-footer clearfix">
        <?php if (Yii::$app->user->can('access.user.edit')): ?>
            <?= ButtonLink::widget([
                'url' => Url::toRoute('edit'),
                'label' => Yii::t('common', 'Добавить пользователя'),
                'style' => ButtonLink::STYLE_SUCCESS,
                'size' => ButtonLink::SIZE_SMALL
            ]); ?>
        <?php endif; ?>

        <?= LinkPager::widget(['pagination' => $dataProvider->getPagination()]) ?>
    </div>
<?php } ?>

<?php

use backend\modules\access\widgets\UserGroupOperations;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ModalConfirmDelete;
use common\widgets\base\Panel;
use common\models\User;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use common\widgets\base\Label;
/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \common\models\search\UserSearch $searchModel */

$this->title = Yii::t('common', 'Список пользователей');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Доступы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $this->title)];

?>

<?= Tabs::widget([
    'encodeLabels' => false,
    'items' => [
        [
            'label' => Yii::t('common', 'Фильтр поиска'),
            'content' => Panel::widget([
                'showButtons' => true,
                'content' => $this->render('_index-filter', [
                    'searchModel' => $searchModel,
                    'users' => $users,
                    'teams' => $teams,
                    'roles' => $roles,
                    'countries' => $countries
                ])
            ])
        ],
        [
            'label' => yii::t('common', 'Групповые операции'),
            'content' => Panel::widget([
                'content' => UserGroupOperations::widget()
            ])
        ]
    ]
]); ?>


<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с пользователями'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'id' => 'userList',
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            /** @var User $model */
            return [
                'class' => 'tr-vertical-align-middle ' . ($model->hasStatus(User::STATUS_BLOCKED) ? 'text-muted' : ''),
            ];
        },
        'columns' => [
            //['class' => \yii\grid\SerialColumn::className()],
            [
                'class' => \yii\grid\CheckboxColumn::className(),
            ],
            ['class' => IdColumn::className()],
            [
                'attribute' => 'username',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'email',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'rolesNames',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'attribute' => 'team_id',
                'content' => function ($model) {
                    $teams = [];

                    if(is_array($model->getUserTeams()->all())){
                        foreach($model->getUserTeams()->all() as $user_team){
                            $teams[] = $user_team->getTeam()->one()->name;
                        }
                    }

                    return implode(", ", $teams);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'webrtc',
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->webrtc ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->webrtc ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
                'label' => yii::t('common', 'WebRTC')
            ],
            [
                'attribute' => 'auto_call',
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->auto_call > 0 ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->auto_call > 0 ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'use_messenger',
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->use_messenger > 0 ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->use_messenger > 0 ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'sip',
                'content' => function ($model) {
                    return Label::widget([
                        'label' => $model->sip ? Yii::t('common', 'Да') : Yii::t('common', 'Нет'),
                        'style' => $model->sip ? Label::STYLE_SUCCESS : Label::STYLE_DEFAULT,
                    ]);
                },
                'enableSorting' => false,
            ],
            [
                'class' => DateColumn::className(),
                'withRelativeTime' => true,
                'attribute' => 'created_at',
            ],
            [
                'class' => DateColumn::className(),
                'withRelativeTime' => true,
                'attribute' => 'last_activity',
            ],
            [
                'attribute' => 'ip',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ]
            ],
            [
                'attribute' => 'outer_ip',
                'headerOptions' => [
                    'class' => 'text-center'
                ],
                'contentOptions' => [
                    'class' => 'text-center'
                ]
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    /*[
                        'label' => Yii::t('common', 'Управление товарами'),
                        'url' => function ($model) {

                            return Url::toRoute(['set-product', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('access.user.setproduct') && $model->can('access.user.setproduct');
                        },
                    ],*/
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.user.edit');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Разблокировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['unblock', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            /** @var User $model */
                            return Yii::$app->user->can('access.user.unblock') && $model->hasStatus(User::STATUS_BLOCKED);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Заблокировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['block', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            /** @var User $model */
                            return Yii::$app->user->can('access.user.block') && !$model->hasStatus(User::STATUS_BLOCKED);
                        },
                    ],
                    [
                        'can' => function () {
                            return (Yii::$app->user->can('access.user.view')
                                    || Yii::$app->user->can('access.user.edit')
                                    || Yii::$app->user->can('access.user.unblock')
                                    || Yii::$app->user->can('access.user.block'))
                                && Yii::$app->user->can('access.user.delete');
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить пользователя'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.user.delete') && in_array(User::ROLE_SUPERADMIN, array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)));
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Управление командами'),
                        'url' => function ($model) {
                            return Url::toRoute(['/operator/list-operators/manage-team/', 'id' => $model['id']]);
                        },
                        'can' => function ($model) {
                            return Yii::$app->user->can('operator.listoperators.manageteam');
                        }
                    ],
                ]
            ],
        ]
    ]),
    'footer' => $this->render('_index-footer', [
        'dataProvider' => $dataProvider
    ]),
])

?>
<?= ModalConfirmDelete::widget(); ?>


<?php

namespace backend\modules\access\controllers;

use api\components\filters\VerbFilter;
use backend\components\web\Controller;
use backend\modules\access\models\AuthItem;
use backend\modules\access\models\search\AuthItemSearch;
use common\components\base\AjaxFilter;
use common\components\Notifier;
use Yii;
use yii\helpers\Url;
use yii\rbac\Item;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class RoleController
 * @package app\modules\access\controllers
 */
class RoleController extends Controller
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-controllers' => ['post'],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['set-permission'],
            ]
        ];
    }


    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AuthItemSearch(Item::TYPE_ROLE);
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param $name
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($name)
    {
        $model = $this->findModel($name);
        $permissionsByRole = Yii::$app->authManager->getPermissionsByRole($model->name);

        return $this->render('view', [
            'model' => $model,
            'permissionList' => $permissionsByRole,
            'permissionsByRole' => array_keys($permissionsByRole),
        ]);
    }

    /**
     * @param null $name
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit($name = null)
    {
        $model = is_null($name) ? new AuthItem(['type' => Item::TYPE_ROLE]) : $this->findModel($name);
        $modelSearch = new AuthItemSearch(Item::TYPE_PERMISSION);

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                $message = Yii::t('common',
                    $isNewRecord ? 'Роль успешно добавлена.' : 'Роль успешно отредактирована.');
                Yii::$app->notifier->addNotifier($message, Notifier::TYPE_SUCCESS);

                //update roles on pay to WebHooks
                $this->updateWtrade();

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'modelSearch' => $modelSearch,
            'permissionList' => $modelSearch->search([], false)->getModels(),
            'permissionsByRole' => array_keys(Yii::$app->authManager->getPermissionsByRole($model->name)),
        ]);
    }

    /**
     * @param $name
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($name)
    {
        $model = $this->findModel($name);

        if (AuthItem::checkRelation($model->name)) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Невозможно удалить запись. Есть зависимости.'));
        } else {
            $item = Yii::$app->authManager->getRole($model->name);
            Yii::$app->authManager->remove($item);
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Запись успешно удалена.'), Notifier::TYPE_SUCCESS);

            //update roles on pay to WebHooks
            $this->updateWtrade();
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionSetPermission()
    {
        $role = Yii::$app->authManager->getRole(Yii::$app->request->post('role'));

        if (!$role) {
            throw new BadRequestHttpException(Yii::t('common', 'Роль не существует.'));
        }

        $permission = Yii::$app->authManager->getPermission(Yii::$app->request->post('permission'));

        if (!$permission) {
            throw new BadRequestHttpException(Yii::t('common', 'Правило не существует.'));
        }

        if ($allow = Yii::$app->request->post('value') == 'on' ? true : false) {
            if (!Yii::$app->authManager->hasChild($role, $permission)) {
                Yii::$app->authManager->addChild($role, $permission);
            }
        } else {
            Yii::$app->authManager->removeChild($role, $permission);
        }

        return [];
    }

    /**
     * @param $name
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($name)
    {
        if ($model = AuthItem::find()
            ->where([
                'name' => $name,
                'type' => Item::TYPE_ROLE
            ])
            ->one()
        ) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Запись не найдена.'));
        }
    }

    public function updateWtrade()
    {
        $component = yii::$app->get('Wtrade');
        $component->sentData();
    }
}

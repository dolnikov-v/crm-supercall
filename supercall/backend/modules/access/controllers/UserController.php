<?php
namespace backend\modules\access\controllers;

use backend\components\web\Controller;
use backend\modules\access\models\AuthAssignment;
use backend\modules\access\models\AuthItem;
use backend\modules\administration\models\ApiLog;
use backend\modules\queue\models\Queue;
use backend\modules\queue\models\QueueUser;
use common\models\search\UserSearch;
use common\models\Teams;
use common\models\User;
use common\models\UserCountry;
use common\models\UserPartner;
use common\models\UserTeam;
use common\modules\partner\models\Partner;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\NotFoundHttpException;

/**
 * Class UserController
 * @package backend\modules\access\controllers
 */
class UserController extends Controller
{
    public $log;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /** @var User $userModel*/
        $userModel = yii::$app->user->getUserModel();
        $teams = $userModel->getTeamsByUser();
        $teams_list = is_array($teams) ? ArrayHelper::map($teams, 'id', 'name') : [];


        $user_country = [];
        $countries_list = ArrayHelper::map(yii::$app->user->getCountries(), 'id', 'name');

        foreach($countries_list  as $k=>$country){
            $user_country[$k] = yii::t('common', $country);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => User::find()->collection(),
            'roles' => AuthItem::getCollection($type = 1),
            'teams' => $teams_list,
            'countries' => $user_country
        ]);
    }

    /**
     * @param null|integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit($id = null)
    {
        $model = is_null($id) ? new User() : $this->findModel($id);
        $model->setScenario(is_null($id) ? User::SCENARIO_INSERT : User::SCENARIO_UPDATE);

        if($model->getScenario() == User::SCENARIO_UPDATE){
            $model->sipArray = json_decode($model->sip, true);
        }

        if ($model->load(Yii::$app->request->post())) {

            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                $user = Yii::$app->request->post('User');
                $team_ids = isset($user['team_id']) ? $user['team_id'] : [];

                UserTeam::updateUserTeams($model->id, $team_ids);

                // связка с user_country
                UserCountry::deleteAll([
                    'user_id' => $model->id
                ]);

                if (array_key_exists('country', $user)) {
                    foreach ($user['country'] as $countryId) {
                        $userCountry = new UserCountry();
                        $userCountry->user_id = $model->id;
                        $userCountry->country_id = $countryId;
                        $userCountry->save();
                    }
                }

                // связка с user_partner
                UserPartner::deleteAll([
                    'user_id' => $model->id
                ]);

                if (array_key_exists('partner', $user)) {
                    foreach ($user['partner'] as $partnerId) {
                        $userPartner = new UserPartner();
                        $userPartner->user_id = $model->id;
                        $userPartner->partner_id = $partnerId;
                        $userPartner->save();
                    }
                }


                $currentRoles = array_keys(Yii::$app->authManager->getRolesByUser($model->id));
                $newRoles = isset($user['roles']) ? $user['roles'] : [];

                $addRoles = array_diff($newRoles, $currentRoles);
                $deleteRoles = array_diff($currentRoles, $newRoles);

                if (empty($newRoles)) {
                    AuthAssignment::deleteAll([
                        'user_id' => $model->id
                    ]);
                } else {
                    if (!empty($deleteRoles)) {
                        AuthAssignment::deleteAll([
                            'AND', 'user_id = :user_id',
                            ['IN', 'item_name', $deleteRoles]
                        ],
                            [':user_id' => $model->id]
                        );
                    }
                }

                if (!empty($addRoles)) {
                    foreach ($addRoles as $role) {
                        $authRole = Yii::$app->authManager->getRole($role);
                        Yii::$app->authManager->assign($authRole, $model->id);
                    }
                }

                if($isNewRecord){
                    $this->addToAsterisk(yii::$app->request->post('User'));
                }

                //Хот фикс - обвновляем очереди на астере при сохранение пользователя
                //отключил, т.к. на астере операторы распределяются на подочереди в php yii auto-call/generate
//                if($model->auto_call = 1){
//                    $queue = QueueUser::find()->select('queue_id')->where(['user_id' => $model->id])->asArray()->all();
//                    if (!empty($queue)) {
//                        $queue = ArrayHelper::getColumn($queue, 'queue_id');
//                        Queue::updateAsteriskMemberQueues($model->id, $queue);
//                    }
//                }

                $message = $isNewRecord ? 'Пользователь успешно добавлен.' : 'Пользователь успешно отредактирован.';
                $message = Yii::t('common', $message);
                Yii::$app->notifier->addNotifier($message, 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        $countries = [];

        foreach (yii::$app->user->countries as $country) {
            $countries[$country->id] = Yii::t('common', $country->name);
        }

        return $this->render('edit', [
            'model' => $model,
            'roles' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description'),
            'teams' => ArrayHelper::map(Teams::find()->active()->all(), 'id', 'name'),
            'currentRoles' => ArrayHelper::getColumn(Yii::$app->authManager->getRolesByUser($model->id), 'name'),

            'countries' => $countries,
            'currentCountries' => ArrayHelper::map(($model->countries) ? $model->countries : [], 'id', 'id'),
            'partners' => ArrayHelper::map(Partner::find()->active()->all(), 'id', 'name'),
            'currentPartners' => ArrayHelper::map($model->partners, 'id', 'id'),
        ]);
    }

    /**
     * @param $data
     */
    public function addToAsterisk($data)
    {
        $user = User::findOne(['username' => $data['username']]);

        $client = new Client();

        if(yii::$app->params['is_dev']){
            $login = yii::$app->params['AsteriskSp_dev_prefix'].$user->id;
        }else{
            $login = yii::$app->params['AsteriskSp_prefix'].$user->id;
        }

        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(yii::$app->params['AsteriskSp_adduser'])
            ->setData([
                'login' => $login,
                'pass'  => $data['password']
            ])
            ->send();

        if($response->isOk){
            $answer = json_decode($response->content, 1);

            if(isset($answer['status']) && $answer['status'] === true) {

                $parseUrl = parse_url(yii::$app->params['AsteriskSp_adduser']);

                $sip = [
                    'sip_host' => $parseUrl['host'],
                    'sip_login' => $login,
                    'sip_pass' => $data['password']
                ];

                $user->sipArray = $sip;

                if (!$user->save()) {
                    $this->log = new ApiLog();
                    $this->log->request = 'insert sip data to User (id = ' . $user->id . ') table . ' . json_encode($sip, JSON_UNESCAPED_UNICODE);
                    $this->log->response = json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE);
                    $this->log->save();
                }

            }else{
                $this->log = new ApiLog();
                $this->log->request = 'insert sip data to Asterisk2 (id = '.$user->id.') . '.json_encode([
                        'login' => $login,
                        'pass'  => $data['password']
                    ], JSON_UNESCAPED_UNICODE);
                $this->log->response = $response->content;
                $this->log->save();
            }
        }else{
            $this->log = new ApiLog();
            $this->log->request = 'insert sip data to Asterisk2 (id = '.$user->id.') . '.json_encode([
                    'login' => $login,
                    'pass'  => $data['password']
                ], JSON_UNESCAPED_UNICODE);
            $this->log->response = $response->content;
            $this->log->save();
        }
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_DELETED;

        if ($model->save(true, ['status'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Пользователь {name} удален.', ['name' => $model->username]), 'success');
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionBlock($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_BLOCKED;

        if ($model->save(true, ['status'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Пользователь {name} заблокирован.', ['name' => $model->username]), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionUnblock($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_DEFAULT;

        if ($model->save(true, ['status'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Пользователь {name} разблокирован.', ['name' => $model->username]), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        /** @var User $model */
        $model = User::findOne($id);

        if ($model && !$model->hasStatus(User::STATUS_DELETED)) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Пользователь не найден.'));
        }
    }
}

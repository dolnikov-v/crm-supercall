<?php

namespace backend\modules\access\controllers;

use backend\components\web\Controller;
use common\models\search\TeamSearch;
use common\models\Teams;
use common\models\TeamsCountry;
use common\models\User;
use common\models\UserCountry;
use common\models\UserTeam;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class TeamController
 * @package backend\modules\access\controllers
 */
class TeamController extends Controller
{

    public function beforeAction($action)
    {
        if (in_array($action->id, ['get-list-users', 'manage-user-of-team'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TeamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $countries = [];

        if (is_array(Yii::$app->user->countries)) {
            foreach (Yii::$app->user->countries as $k => $country) {
                $countries[$country->id] = yii::t('common', $country->name);
            }
        }

        $teams = TeamsCountry::getTeamsByCountry(array_keys($countries));

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'countries' => $countries,
            'teams' => ArrayHelper::map($teams, 'teams.id', 'teams.name'),
        ]);
    }

    /**
     * @param null|integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEdit($id = null)
    {
        $model = is_null($id)
            ? new Teams()
            : $this->findModel($id);

        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $isNewRecord = $model->isNewRecord;

            $country_id = isset($post['Teams']['country_id']) ? $post['Teams']['country_id'] : [];


            $transaction = yii::$app->db->beginTransaction();

            if ($model->save()) {

                if (empty($country_id)) {
                    Yii::$app->notifier->addNotifier(yii::t('common', 'Укажите страну/страны для команды'));
                    $transaction->rollback();
                } else {
                    if (!$isNewRecord) {
                        if (!TeamsCountry::clearTeamsCountryByTeam($model->id)) {
                            $transaction->rollBack();
                            Yii::$app->notifier->addNotifierErrorByModel($model);
                        }
                    }

                    if (!TeamsCountry::addToTeamsCountry($model->id, $country_id)) {
                        $transaction->rollBack();
                        Yii::$app->notifier->addNotifierErrorByModel($model);
                    } else {
                        $transaction->commit();
                    }
                }

                $message = $isNewRecord ? 'Команда успешно добавлена.' : 'Команда успешно отредактирована.';
                $message = Yii::t('common', $message);
                Yii::$app->notifier->addNotifier($message, 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                $transaction->rollBack();
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }

        }

        $countries = [];

        if (is_array(Yii::$app->user->countries)) {
            foreach (Yii::$app->user->countries as $k => $country) {
                $countries[$country->id] = yii::t('common', $country->name);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'countries' => $countries
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Команда успешно активирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Команда успешно деактивирована.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        /** @var User $model */
        $model = Teams::findOne($id);

        if ($model && $model->active) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Команда не найдена.'));
        }
    }

    /**
     * @return string
     */
    public function actionGetListUsers()
    {
        $team_id = yii::$app->request->post('team_id');
        $country_id = yii::$app->request->post('country_id');

        if(!empty($country_id)) {
            $users = User::find()
                ->leftJoin(UserCountry::tableName(), 'user_id=' . User::tableName() . '.id')
                ->where(['status' => [User::STATUS_DEFAULT, User::STATUS_APPLICANT, User::STATUS_NEW]])
                ->andWhere(['country_id' => $country_id])
                ->all();
        }else{
            $users = [];
        }

        $userFromTeam = UserTeam::findAll(['team_id' => $team_id]);
        $team_users = ArrayHelper::getColumn($userFromTeam, 'user_id');

        return json_encode([
            'users' => ArrayHelper::map($users, 'id', 'username'),
            'team_users' => is_array($team_users) ? $team_users : []
        ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionManageUserOfTeam()
    {
        $team_id = yii::$app->request->post('team_id');
        $user_id = yii::$app->request->post('user_id');
        $state = yii::$app->request->post('state');

        $result = [
            'success' => true,
            'message' => yii::t('common', 'Действие выполнено')
        ];

        if ($state) {
            $resultManage = UserTeam::addInTeam($user_id, $team_id);
        } else {
            $resultManage = UserTeam::removeFromTeam($user_id, $team_id);
        }

        if (!$resultManage) {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Произошла ошибка')
            ];
        }

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionCheckCompatibility()
    {
        $team_id = yii::$app->request->post('team_id');
        $country_id = yii::$app->request->post('country_id');
        $country_id = is_array($country_id) ? $country_id : [];

        $usersTeam = UserTeam::findAll(['team_id' => $team_id]);
        $userCountries = [];

        foreach ($usersTeam as $user_team) {
            /** @var UserTeam $user_team */
            $user = User::findOne(['id' => $user_team->user_id]);

            if ($user) {
                $userCountries[$user->id] = ArrayHelper::getColumn($user->getCountries()->all(), 'id');
            } else {
                continue;
            }
        }
        $excluded_users = [];

        foreach ($userCountries as $user => $country) {
            if (empty(array_intersect($country_id, $country))) {
                $excluded_users[] = $user;
            }
        }

        $users_list = ArrayHelper::map(User::findAll(['id' => $excluded_users]), 'id', 'username');

        return json_encode($users_list, 1);
    }
}

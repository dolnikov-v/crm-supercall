<?php
namespace backend\modules\access\controllers;

use backend\components\web\Controller;
use backend\modules\access\models\AuthItem;
use backend\modules\access\models\search\AuthItemSearch;
use common\components\Notifier;
use Yii;
use yii\helpers\Url;
use yii\rbac\Item;
use yii\web\NotFoundHttpException;

/**
 * Class PermissionController
 * @package backend\modules\access\controllers
 */
class PermissionController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AuthItemSearch(Item::TYPE_PERMISSION);
        
        $filterSessId = $this->route;
        $this->getFilterBySession($filterSessId, $query);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'contentVisible' => Yii::$app->session->get($filterSessId.'.isVisible', true),
            'dataProvider' => $searchModel->search($query)
        ]);
    }

    /**
     * @param $name
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEdit($name = null)
    {
        $model = is_null($name) ? new AuthItem(['type' => Item::TYPE_PERMISSION]) : $this->findModel($name);
        $searchModel = new AuthItemSearch(Item::TYPE_PERMISSION);

        if ($model->load(Yii::$app->request->post())) {
            $message = $model->isNewRecord ? 'Операция успешно добавлена.' : 'Операция успешно отредактирована.';

            if ($model->save()) {
                $message = Yii::t('common', $message, ['name' => $model->name]);
                Yii::$app->notifier->addNotifier($message, Notifier::TYPE_SUCCESS);

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'permissionList' => $searchModel->search([], false)->getModels(),
            'permissionsByRole' => array_keys(Yii::$app->authManager->getPermissionsByRole($model->name)),
        ]);
    }

    /**
     * @param $name
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($name)
    {
        $model = $this->findModel($name);

        if (AuthItem::checkRelation($model->name)) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Невозможно удалить запись. Есть зависимости.'));
        } else {
            $item = Yii::$app->authManager->getPermission($model->name);
            Yii::$app->authManager->remove($item);
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Запись успешно удалена.'), Notifier::TYPE_SUCCESS);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $name
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($name)
    {
        if ($model = AuthItem::find()->where(['name' => $name, 'type' => Item::TYPE_PERMISSION])->one()) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('common', 'Запись не найдена.'));
        }
    }
}

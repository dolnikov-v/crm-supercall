<?php

namespace backend\modules\access\controllers;

use backend\components\web\Controller;
use backend\modules\access\models\UserGroupOperations;
use common\models\User;
use HttpException;
use Yii;
use yii\helpers\Url;

/**
 * Class UserGroupOperationsController
 * @package backend\modules\access\controllers
 */
class UserGroupOperationsController extends Controller
{
    public $action = null;

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws HttpException
     */
    public function beforeAction($action)
    {
        if (!yii::$app->request->isAjax && !yii::$app->request->post()) {
            throw new HttpException(403, Yii::t('common', 'Запрос запрещён'));
        }
        return parent::beforeAction($action);
    }

    /**
     * @param $tags
     * @param null $error
     */
    public function toLog($tags, $error = null)
    {
        $route = isset($tags['route']) ? $tags['route'] : null;

        if ($route) {
            unset($tags['rouet']);
        }

        $logger = yii::$app->get('logger');
        $logger->action = $this->action;
        $logger->route = Url::to([$route]);
        $logger->type = UserGroupOperations::LOGGER_TYPE;
        $logger->tags = array_merge([
            'user_id' => yii::$app->user->identity->id,
            'created_at' => time(),
        ], $tags);

        if (!is_null($error)) {
            $logger->error = $error;
        }

        $logger->save();
    }

    /**
     * @param $id
     * @return array
     */
    public function userNotFound($id)
    {
        return [
            'success' => false,
            'message' => yii::t('common', 'Пользователь №{id} не найден', ['id' => $id]),
            'id' => $id
        ];
    }

    /**
     * @return string
     */
    public function actionBlockUsers()
    {
        $this->action = 'group-block-users';

        $id = yii::$app->request->post('id');
        $group = yii::$app->request->post('group');

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'new_status' => User::STATUS_BLOCKED,
            'route' => '/order/group-operations/block-users'
        ];

        $user = UserGroupOperations::getUserBy($id);

        if($user){
            $tags['old_status'] = $user->status;

            $user->status = User::STATUS_BLOCKED;

            if(!$user->save(false)){
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка блокировки пользователя №{id} : {error}',
                        [
                            'id' => $id,
                            'error' => json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE)
                        ]),
                    'id' => $id
                ];
            }else{
                $result = [
                    'success' => true,
                    'message' => null,
                    'id' => $id
                ];
            }
        }else{
            $result = $this->userNotFound($id);
        }

        $this->toLog($tags, $result['message']);

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionActivateUsers()
    {
        $this->action = 'group-activate-users';

        $id = yii::$app->request->post('id');
        $group = yii::$app->request->post('group');

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'new_status' => User::STATUS_DEFAULT,
            'route' => '/order/group-operations/activate-users'
        ];

        $user = UserGroupOperations::getUserBy($id);

        if($user){
            $tags['old_status'] = $user->status;

            $user->status = User::STATUS_DEFAULT;

            if(!$user->save(false)){
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка активации пользователя №{id} : {error}',
                        [
                            'id' => $id,
                            'error' => json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE)
                        ]),
                    'id' => $id
                ];
            }else{
                $result = [
                    'success' => true,
                    'message' => null,
                    'id' => $id
                ];
            }
        }else{
            $result = $this->userNotFound($id);
        }

        $this->toLog($tags, $result['message']);

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionOnWebrtc()
    {
        $this->action = 'group-on-webrtc';

        $id = yii::$app->request->post('id');
        $group = yii::$app->request->post('group');

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'new_webrtc' => User::WEBRTC_ON,
            'route' => '/order/group-operations/on-webrtc'
        ];

        $user = UserGroupOperations::getUserBy($id);

        if($user){
            $tags['old_webrtc'] = $user->webrtc;

            $user->webrtc = User::WEBRTC_ON;

            if(!$user->save(false)){
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка включения WebRTC для пользователя №{id} : {error}',
                        [
                            'id' => $id,
                            'error' => json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE)
                        ]),
                    'id' => $id
                ];
            }else{
                $result = [
                    'success' => true,
                    'message' => null,
                    'id' => $id
                ];
            }
        }else{
            $result = $this->userNotFound($id);
        }

        $this->toLog($tags, $result['message']);

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionOffWebrtc()
    {
        $this->action = 'group-off-webrtc';

        $id = yii::$app->request->post('id');
        $group = yii::$app->request->post('group');

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'new_webrtc' => User::WEBRTC_OFF,
            'route' => '/order/group-operations/off-webrtc'
        ];

        $user = UserGroupOperations::getUserBy($id);

        if($user){
            $tags['old_webrtc'] = $user->webrtc;

            $user->webrtc = User::WEBRTC_OFF;

            if(!$user->save(false)){
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка отклюючения WebRTC для пользователя №{id} : {error}',
                        [
                            'id' => $id,
                            'error' => json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE)
                        ]),
                    'id' => $id
                ];
            }else{
                $result = [
                    'success' => true,
                    'message' => null,
                    'id' => $id
                ];
            }
        }else{
            $result = $this->userNotFound($id);
        }

        $this->toLog($tags, $result['message']);

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionOnAutoCall()
    {
        $this->action = 'group-on-auto-call';

        $id = yii::$app->request->post('id');
        $group = yii::$app->request->post('group');

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'new_autocall' => User::AUTO_CALL_ON,
            'route' => '/order/group-operations/on-auto-call'
        ];

        $user = UserGroupOperations::getUserBy($id);

        if($user){
            $tags['old_auto_call'] = $user->auto_call;

            $user->auto_call = User::AUTO_CALL_ON;

            if(!$user->save(false)){
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка включения автодозвона для пользователя №{id} : {error}',
                        [
                            'id' => $id,
                            'error' => json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE)
                        ]),
                    'id' => $id
                ];
            }else{
                $result = [
                    'success' => true,
                    'message' => null,
                    'id' => $id
                ];
            }
        }else{
            $result = $this->userNotFound($id);
        }

        $this->toLog($tags, $result['message']);

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionOffAutoCall()
    {
        $this->action = 'group-off-auto-call';

        $id = yii::$app->request->post('id');
        $group = yii::$app->request->post('group');

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'new_autocall' => User::AUTO_CALL_OFF,
            'route' => '/order/group-operations/off-autocall'
        ];

        $user = UserGroupOperations::getUserBy($id);

        if($user){
            $tags['old_auto_call'] = $user->auto_call;

            $user->auto_call = User::AUTO_CALL_OFF;

            if(!$user->save(false)){
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка выключения автодозвона для пользователя №{id} : {error}',
                        [
                            'id' => $id,
                            'error' => json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE)
                        ]),
                    'id' => $id
                ];
            }else{
                $result = [
                    'success' => true,
                    'message' => null,
                    'id' => $id
                ];
            }
        }else{
            $result = $this->userNotFound($id);
        }

        $this->toLog($tags, $result['message']);

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionDeleteUsers()
    {
        $this->action = 'group-delete-users';

        $id = yii::$app->request->post('id');
        $group = yii::$app->request->post('group');

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'new_status' => User::STATUS_DELETED,
            'route' => '/order/group-operations/delete-users'
        ];

        $user = UserGroupOperations::getUserBy($id);

        if($user){
            $tags['old_status'] = $user->status;

            $user->status = User::STATUS_DELETED;

            if(!$user->save(false)){
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка удаления пользователя №{id} : {error}',
                        [
                            'id' => $id,
                            'error' => json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE)
                        ]),
                    'id' => $id
                ];
            }else{
                $result = [
                    'success' => true,
                    'message' => null,
                    'id' => $id
                ];
            }
        }else{
            $result = $this->userNotFound($id);
        }

        $this->toLog($tags, $result['message']);

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionUnDeleteUsers()
    {
        $this->action = 'group-un-delete-users';

        $id = yii::$app->request->post('id');
        $group = yii::$app->request->post('group');

        $tags = [
            'order_id' => $id,
            'group' => $group,
            'new_status' => User::STATUS_DEFAULT,
            'route' => '/order/group-operations/un-delete-users'
        ];

        $user = UserGroupOperations::getUserBy($id);

        if($user){
            $tags['old_status'] = $user->status;

            $user->status = User::STATUS_DEFAULT;

            if(!$user->save(false)){
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка восстановления пользователя №{id} : {error}',
                        [
                            'id' => $id,
                            'error' => json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE)
                        ]),
                    'id' => $id
                ];
            }else{
                $result = [
                    'success' => true,
                    'message' => null,
                    'id' => $id
                ];
            }
        }else{
            $result = $this->userNotFound($id);
        }

        $this->toLog($tags, $result['message']);

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
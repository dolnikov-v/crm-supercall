<?php
use backend\assets\modules\presets\PresetsAsset;
use backend\modules\queue\models\Queue;
use backend\modules\presets\widgets\Attempts;
use common\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \common\modules\partner\models\Queue $model */
    PresetsAsset::register($this);
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['edit', 'id' => $model->id])]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput(['disabled' => 'disabled']) ?>
        <?= $form->field($model, 'auto')->checkbox(['checked' => true, 'unchecked_value' => 0]); ?>
    </div>
</div>

<hr/>
<div id="block-attempts" class="alert alert-warning">
    <div class="original-attempts">
        <?php
            // для новых и если стратегия у очереди не заполнена
            if ($model->isNewRecord || empty($model->attempts)):
                $item = 1;
                echo Attempts::widget([
                        'numb'  => $item,
                        'label' => true,
                ]);
        endif; ?>
    </div>
    <div class="copy-attempts">
        <?php
            // для старых и тех, у кого Стратегия непустая
            if (!$model->isNewRecord && !empty($model->attempts)):
                $item = 1;
                foreach ($model->attempts as $key => $value):
                    echo Attempts::widget([
                            'key'   => $key,
                            'value' => $value,
                            'numb'  => $item,
                            'label' => ($item == 0) ? true : false,
                    ]);
                    $item++;
                endforeach;
            endif; ?>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= Html::a('+ Добавить', null, ['class' => 'btn-add-attempts btn btn-default btn-sm', 'onclick' => 'addAttempts(' .($item+1). ')']) ?>
        </div>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-lg-12">
        <?= $form->submit($model->isNewRecord ? Yii::t('common', 'Добавить пресет') : Yii::t('common', 'Сохранить пресет')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php

use backend\modules\queue\models\Queue;
use backend\modules\presets\models\Presets;
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\widgets\base\ButtonLink;
use common\widgets\base\Label;
use common\widgets\base\Panel;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\queue\models\search\QueueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Пресеты');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Пресеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с преетами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
            return [
                'class' => 'tr-vertical-align-middle',
            ];
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'attempts',
                'value' => function($model) {
                    return implode('-', $model->attempts);
                },
                'enableSorting' => false,
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    return $model->getDatetimeByTimezone($model->created_at);
                },
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($data) {
                            return Url::toRoute(['edit', 'id' => $data->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('presets.control.edit');
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footer' => (Yii::$app->user->can('presets.control.edit') ? ButtonLink::widget([
            'label' => Yii::t('common', 'Добавить пресет'),
            'url'   => Url::toRoute('edit'),
            'style' => ButtonLink::STYLE_SUCCESS,
            'size'  => ButtonLink::SIZE_SMALL,
        ]) : '') .
        LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
]) ?>

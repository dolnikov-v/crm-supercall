<?php
use common\widgets\base\Panel;
use yii\helpers\Url;

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление пресета') : Yii::t('common', 'Редактирование пресета');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Пресеты'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Управление'), 'url' => Url::toRoute('/presets/control/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Пресет'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]) ?>

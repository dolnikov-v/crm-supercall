<?php

namespace backend\modules\presets\models\query;


use common\components\db\ActiveQuery;
/**
 * This is the ActiveQuery class for [[\backend\modules\presets\models\Presets]].
 *
 * @see \backend\modules\presets\models\Queue
 */
class PresetsQuery extends ActiveQuery
{
    /**
     * Определяет приоритет очереди
     * @return $this
     */
    public function priority()
    {
        return $this->orderBy(['name' => SORT_ASC]);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\queue\models\Queue[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\queue\models\Queue|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace backend\modules\presets\models;

use common\components\db\ActiveRecord;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%presets}}".
 */
class Presets extends ActiveRecord
{
    // Если флаг = true, то имя пресета формируем автоматически
    public $auto;
    public $interval;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%presets}}';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'attempts'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['attempts'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'name'       => 'Название',
            'attempts'   => 'Attempts',
            'created_at' => 'Создан',
            'auto'       => 'авто',
            'interval'   => 'Интервал, в мин.',
        ];
    }
    
    public function beforeValidate()
    {
        if (!empty($this->attempts))
            $this->attempts = Json::encode($this->attempts);
        
        return parent::beforeValidate();
    }
    
    public function afterFind()
    {
        if (!empty($this->attempts))
            $this->attempts = Json::decode($this->attempts);
        
        parent::afterFind();
    }
    
    /**
     * Функция принимает данные Попыток POST и формирует массив перед сохранением в БД
     * @param $post
     * @return array
     */
    public static function transformAttempts($post)
    {
        if (empty($post))
            return null;
    
        asort($post);
        $attempts = [];
        $item = 1;
        foreach ($post as $value) {
            // если пустой статус, то пропустим
            if (empty($value['interval']))
                continue;
    
            $attempts[$item] = $value['interval'];
            $item++;
        }
        
        if (empty($attempts))
            return null;
        
        return $attempts;
    }

    /**
     * @inheritdoc
     * @return \backend\modules\presets\models\query\PresetsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\presets\models\query\PresetsQuery(get_called_class());
    }

}

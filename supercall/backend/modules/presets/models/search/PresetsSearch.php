<?php

namespace backend\modules\presets\models\search;

use backend\modules\presets\models\Presets;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PresetsSearch represents the model behind the search form about `backend\modules\queue\models\Presets`.
 */
class PresetsSearch extends Presets
{
    public $name;
    public $attempts;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'attempts'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Presets::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'attempts', $this->attempts]);

        return $dataProvider;
    }
}

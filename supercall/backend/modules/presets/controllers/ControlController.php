<?php

namespace backend\modules\presets\controllers;

use backend\modules\presets\widgets\Attempts;
use Yii;
use backend\modules\presets\models\Presets;
use backend\modules\presets\models\search\PresetsSearch;
use backend\components\web\Controller;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * Control Controller implements the CRUD actions for Queue model.
 */
class ControlController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'      => ['POST'],
                    'get-attempts' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Queue models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PresetsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Presets model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Presets the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Presets::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new Presets();
        }

        if ($model->load(Yii::$app->request->post())) {
            $presets = Yii::$app->request->post('Presets');
            $isNewRecord = $model->isNewRecord;
            
            // обработать попытки в json
            $attempts = Presets::transformAttempts(Yii::$app->request->post('Attempts'));
            $model->attempts = $attempts;
            
            // Определить имя
            if($presets['auto']) { // автоматически
                $model->name = implode('-', $attempts);
            }
            
            if ($model->save()) {
                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Пресет успешно добавлен.') : Yii::t('common', 'Пресет успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }
    
    /**
     * Функция вернет готовый html для формирования json-стратегии
     * @return \yii\console\Response|Response
     */
    public function actionGetAttempts()
    {
        $numb = (int) Yii::$app->request->post('numb');
        
        $widget = Attempts::widget([
            'numb' => $numb,
        ]);
        
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [
            'result' => 'ok',
            'data' => $widget
        ];
        
        return $response;
        
    }

}

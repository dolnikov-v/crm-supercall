<?php

namespace backend\modules\presets;


use backend\components\base\Module as BackendModule;
/**
 * presets module definition class
 */
class Module extends BackendModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\presets\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}

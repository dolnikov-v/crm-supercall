<?php

use yii\helpers\Html;
?>

<div class="row attempts-raw--<?= $numb ?>">
    <div class="col-lg-1" style="max-width:80px">
        <div style="padding: 6px">№<?= $numb ?></div>
    </div>
    <div class="col-lg-2">
        <div class="form-group">
            <?= $label ? Html::label($presets->getAttributeLabel('interval')) : null ?>
            <?= Html::textInput("Attempts[$numb][interval]", $value ? $value : null, [
                    'class' => 'form-control  ',
                    'type'  => 'number',
            ]); ?>
        </div>
    </div>
    <div class="col-lg-1">
        <?= $label ? '<br/>' : null; ?>
        <a href="javascript:void(0)" onclick="removeRawStrategy('<?= $numb ?>')">
            <i class="pe-7s-close" style="font-size: 28px"></i>
        </a>
    </div>
</div>
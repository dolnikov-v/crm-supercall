<?php
namespace backend\modules\presets\widgets;


use backend\modules\presets\models\Presets;
use yii\base\Widget;

class Attempts extends Widget
{
    public $numb;
    public $key = 0; // ключ - является статусом для сохр. стратегии
    public $value = []; // массив с параметрами - для сохр. стратегии
    public $label = false;
    
    public function run()
    {
        $presets = new Presets();
    
        return $this->render('attempts', [
            'numb'  => $this->numb,
            'key'   => $this->key,
            'value' => $this->value,
            'label' => $this->label,
            'presets' => $presets,
        ]);
    }
}
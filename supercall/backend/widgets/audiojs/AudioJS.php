<?php

namespace backend\widgets\audiojs;

use Yii;
use yii\base\Widget;
use backend\widgets\assets\audiojs\AudioJSAsset;
use yii\helpers\Html;

/**
 * Class AudioJS
 * @package backend\widgets\audiojs
 */
class AudioJS extends Widget
{
    public $file;
    public $uploads;
    public $type;
    public $currentTime;
    public $assetBundle;

    public function init()
    {
        parent::init();

        if (is_null($this->uploads)) {
            $this->uploads = Yii::getAlias('@web/uploads/');
        }

        if(is_null($this->currentTime)){
            $this->currentTime = 0;
        }
    }

    public function run()
    {
        $this->assetBundle = AudioJsAsset::register($this->view);

        $audio = "<audio   src='{$this->uploads}{$this->file}' preload='auto' currentTime='".$this->currentTime."'  type='audio/".$this->type."'></audio>";

        return Html::tag('div', $audio);
    }
}
<?php

namespace backend\widgets\callrecords;

use common\models\User;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use Yii;
use yii\base\Widget;

/**
 * Class CallRecords
 * @package backend\widgets\callrecords
 */
class CallRecords extends Widget
{
    const COLUMN_CALL_RECORD = 'call_record';
    const COLUMN_DURATION = 'total_call_duration';
    const COLUMN_GROUP_ID = 'group_id';
    const COLUMN_CONVERSATION_TIME = 'conversation_time';
    const COLUMN_CALL_STATUS = 'call_status';
    const COLUMN_ORDER_STATUS = 'order_status';
    const COLUMN_DATE_OF_CALL = 'date_of_call';
    const COLUMN_OPERATOR = 'operator';

    /** @var  Order */
    public $order;
    /** @var  array */
    public $records;
    /** @var  array */
    public $columns;
    /** @var  boolean */
    public $show_transcribe;

    public function init()
    {
        if(is_null($this->columns) || empty($this->columns)){
            $this->columns = [
                self::COLUMN_CALL_RECORD,
                self::COLUMN_GROUP_ID,
                self::COLUMN_DURATION,
                self::COLUMN_CONVERSATION_TIME,
                self::COLUMN_CALL_STATUS,
                self::COLUMN_ORDER_STATUS,
                self::COLUMN_DATE_OF_CALL,
                self::COLUMN_OPERATOR,
            ];
        }

        $this->records = CallHistory::find()
            ->select([
                'id' => CallHistory::tableName() . '.id',
                'user_id' => CallHistory::tableName() . '.user_id',
                'group_id' => CallHistory::tableName() . '.group_id',
                'order_id' => CallHistory::tableName() . '.order_id',
                'recordingfile' => CallHistory::tableName() . '.recordingfile',
                'username' => User::tableName() . '.username',
                'created_at' => CallHistory::tableName() . '.created_at',
                'disposition' => CallHistory::tableName() . '.disposition',
                'end_status' => CallHistory::tableName() . '.end_status',
                'billsec' => CallHistory::tableName() . '.billsec',
                'duration' => CallHistory::tableName() . '.duration',
                'action_address_time' => CallHistory::tableName() . '.action_address_time',
                'transcribe' => CallHistory::tableName() . '.transcribe',
            ])
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . CallHistory::tableName() . '.user_id')
            ->where(['order_id' => $this->order->id])
            ->orderBy(CallHistory::tableName() . '.id DESC')->all();
    }

    public function run()
    {
        $this->init();

        /*if($this->show_transcribe){
            foreach($this->records as $record) {
                $googleSpeech = yii::$app->get('GoogleSpeech');
                $googleSpeech->call_history_id = $record->id;
                $googleSpeech->file = $record->recordingfile;
                $googleSpeech->order = $this->order;
                $transcribe = $googleSpeech->getRecordTranscribe();
            }
        }*/

        return $this->render('call-records', [
            'records' => $this->records,
            'columns' => $this->columns,
            'transcribe' => isset($transcribe) ? $transcribe : []
        ]);
    }
}
<?php

use backend\modules\order\models\TranscribeQueue;
use backend\widgets\callrecords\CallRecords;
use common\helpers\fonts\FontAwesome;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use backend\widgets\audiojs\AudioJS;
use common\widgets\base\Button;
use common\modules\call\assets\GetTranscribeAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\web\View;

/** @var array $records */
/** @var array $columns */
//подключаемые ассеты в quality-control-edit.js - в момент прорисовки записей в форме заказа

GetTranscribeAsset::register($this);
?>

<table class="table table-striped table-records" style="margin-top:10px">
    <tr>
        <?php if (in_array(CallRecords::COLUMN_CALL_RECORD, $columns)): ?>
            <th><?= Yii::t('common', 'Запись разговора') ?></th>
        <?php endif; ?>

        <?php if (in_array(CallRecords::COLUMN_DURATION, $columns)): ?>
            <th><?= Yii::t('common', 'Общее время звонка') ?></th>
        <?php endif; ?>

        <?php if (in_array(CallRecords::COLUMN_CONVERSATION_TIME, $columns)): ?>
            <th><?= Yii::t('common', 'Время разговора') ?></th>
        <?php endif; ?>

        <?php if (in_array(CallRecords::COLUMN_CALL_STATUS, $columns)): ?>
            <th><?= Yii::t('common', 'Статус звонка') ?></th>
        <?php endif; ?>

        <?php if (in_array(CallRecords::COLUMN_ORDER_STATUS, $columns)): ?>
            <th><?= Yii::t('common', 'Статус заказа') ?></th>
        <?php endif; ?>

        <?php if (in_array(CallRecords::COLUMN_DATE_OF_CALL, $columns)): ?>
            <th><?= Yii::t('common', 'Дата звонка') ?></th>
        <?php endif; ?>

        <?php if (in_array(CallRecords::COLUMN_OPERATOR, $columns)): ?>
            <th><?= Yii::t('common', 'Оператор') ?></th>
        <?php endif; ?>
    </tr>
    <tbody>

    <?php if (!is_null($records)) {

        foreach ($records as $record): ?>

            <tr class="<?= $record->id; ?> <?=($record->duration == 0 && $record->disposition == CallHistory::DISPOSITION_WAITING && $record->updated_at + 7200 < time()) ? 'hidden' : ''?>">
                <td class="<?= in_array(CallRecords::COLUMN_CALL_RECORD, $columns) ? '' : 'hidden' ?>">
                    <?php if ($record->disposition == CallHistory::DISPOSITION_ANSWERED): ?>


                        <div class="call_record" id="audio_<?= $record->id; ?>">
                            <?php
                            $ext = pathinfo(CallHistory::checkRecordFilePath($record->recordingfile), PATHINFO_EXTENSION);
                            echo AudioJs::widget([
                                'file' => CallHistory::checkRecordFilePath($record->recordingfile),
                                'uploads' => '',
                                'type' => $ext,
                                'currentTime' => $record->action_address_time
                            ]);
                            ?>
                            <br/>

                            <fieldset id="record-list-<?= $record->id ?>">
                                <label><?= yii::t('common', 'Распознование речи') ?></label><br/>

                                <?php if ($record->transcribe): ?>

                                    <?= $record->transcribe ?>

                                <?php else: ?>
                                    <?php if (!$record->transcribeQueue): ?>

                                        <?= Button::widget([
                                            'classes' => 'get-transcribe-data',
                                            'id' => 'transcribe-button-' . $record->id,
                                            'style' => Button::STYLE_DARK,
                                            'size' => Button::SIZE_TINY,
                                            'label' => yii::t('common', 'Запросить транскрибацию'),
                                            'icon' => FontAwesome::CLOUD,
                                            'attributes' => [
                                                'data-call_history_id' => $record->id,
                                                'data-transcribe_price_total' => CallHistory::TRUNSCRIBE_PRICE_ONE_SECOND * $record->billsec
                                            ]
                                        ]); ?>

                                    <?php else: ?>

                                        <?php /** @var CallHistory $record */; ?>
                                        <?php if (in_array($record->transcribeQueue->status, TranscribeQueue::getErrorStatuses())): ?>

                                            <div class="text-danger">
                                                <?= TranscribeQueue::getNameStatus($record->transcribeQueue->status); ?>
                                            </div>

                                        <?php elseif (in_array($record->transcribeQueue->status, TranscribeQueue::getInProgressStatuses())): ?>

                                            <div class="text-warning">
                                                <?= yii::t('common', 'В процессе: {status}', ['status' => TranscribeQueue::getNameStatus($record->transcribeQueue->status)]); ?>
                                            </div>

                                        <?php endif; ?>


                                    <?php endif; ?>

                                <?php endif; ?>

                            </fieldset>

                        </div>


                    <?php else: ?>
                        <?= Yii::t('common', 'Запись отсутвует') ?>
                    <?php endif; ?>
                </td>
                <td class="<?= in_array(CallRecords::COLUMN_DURATION, $columns) ? '' : 'hidden' ?>">
                    <?= $record->duration > 0 ? $record->duration : '&mdash;' ?>
                </td>
                <td class="<?= in_array(CallRecords::COLUMN_CONVERSATION_TIME, $columns) ? '' : 'hidden' ?>">
                    <?= $record->billsec > 0 ? $record->billsec : '&mdash;' ?>
                </td>
                <td class="<?= in_array(CallRecords::COLUMN_CALL_STATUS, $columns) ? '' : 'hidden' ?>">
                    <?= $record->disposition ?></td>
                <td class="<?= in_array(CallRecords::COLUMN_ORDER_STATUS, $columns) ? '' : 'hidden' ?>">
                    <?= Order::getStatusByNumber($record->end_status) ?>
                </td>
                <td class="<?= in_array(CallRecords::COLUMN_DATE_OF_CALL, $columns) ? '' : 'hidden' ?>">
                    <?= yii::$app->formatter->asDateFullTime($record->created_at); ?>
                </td>
                <td class="<?= in_array(CallRecords::COLUMN_OPERATOR, $columns) ? '' : 'hidden' ?>">
                    <?= $record->username; ?>
                </td>

            </tr>
        <?php endforeach;
    } ?>

    </tbody>
</table>

<?php Modal::begin([
    'id' => 'transcribe_price_modal',
    'size' => Modal::SIZE_SMALL,
    'header' => yii::t('common', 'Стоимость транскрибации'),
    'options' => [
        'style' => 'z-index: 90000'
    ],
    'footer' => Html::button(yii::t('common', 'Продолжить'), [
            'class' => 'btn btn-default btn-success',
            'data-dismiss' => 'modal',
            'id' => 'transcribe_price_ok'
        ]) . Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-danger close_btn', 'data-dismiss' => 'modal']),
]); ?>

<div class="transcribe_price_total text-center"></div>

<?php Modal::end(); ?>

<?php
/**
 * ?>
 * <!--
 * <audio controls id="audio_<?= $record['id'] ?>">
 * <source src="<?= Yii::$app->params['AsteriskGetRecordUrl'] . $record['recordingfile']; ?>"
 * type="audio/wav"/>
 * </audio>
 * -->
 * */ ?>


<?php
namespace backend\widgets;

/**
 * Class PanelWidget
 * @package backend\widgets
 */
class PanelWidget extends \yii\base\Widget
{
    const COLOR_PRIMARY = 'panel-primary';
    const COLOR_SUCCESS = 'panel-success';
    const COLOR_INFO = 'panel-info';
    const COLOR_WARNING = 'panel-warning';
    const COLOR_DANGER = 'panel-danger';
    const COLOR_DARK = 'panel-dark';

    const ALERT_PRIMARY = 'alert-primary';
    const ALERT_DANGER = 'alert-danger';
    const ALERT_WARNING = 'alert-warning';
    const ALERT_INFO = 'alert-info';

    /**
     * @var string Id панели
     */
    public $id;

    /**
     * @var string Название панели
     */
    public $title;

    /**
     * @var boolean Использовать ли блок panel-body
     */
    public $withBody = true;

    /**
     * @var string Контент
     */
    public $content;

    /**
     * @var string Подвал
     */
    public $footer;

    /**
     * @var string Цвет панели
     */
    public $color;

    /**
     * @var boolean Границы
     */
    public $border = true;

    /**
     * @var string Текст предупреждения
     */
    public $alert;

    /**
     * @var string Стиль предупреждения
     */
    public $alertStyle = self::ALERT_PRIMARY;

    /**
     * @var Nav
     */
    public $nav;

    /**
     * @var string|boolean Текст или отображать панель действия
     */
    public $actions = false;

    /**
     * @var boolean Отображение кнопки свернуть
     */
    public $collapse = false;

    /**
     * @var boolean Отображение кнопки на весь экран
     */
    public $fullScreen = false;

    /**
     * @var boolean Отображение кноки закрыть
     */
    public $close = false;

    /**
     * @var boolean Отображение кноки перезагрузить
     */
    public $refresh = false;

    /**
     * @var boolean Отображение кноки экспорта
     */
    public $export = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('panel-widget', [
            'id' => $this->id,
            'title' => $this->title,
            'withBody' => $this->withBody,
            'content' => $this->content,
            'footer' => $this->footer,

            'alert' => $this->alert,
            'alertStyle' => $this->alertStyle,
            'nav' => $this->nav,
            'border' => $this->border,
            'color' => $this->color,

            'actions' => $this->getActions(),
            'collapse' => $this->collapse,
            'fullScreen' => $this->fullScreen,
            'close' => $this->close,
            'refresh' => $this->refresh,
            'export' => $this->export,
        ]);
    }

    /**
     * Нужно ли отображать панель действий
     *
     * @return bool|string
     */
    private function getActions()
    {
        if (!empty($this->actions)) {
            return $this->actions;
        }

        if ($this->collapse || $this->fullScreen || $this->close || $this->refresh) {
            return true;
        } else {
            return false;
        }
    }
}

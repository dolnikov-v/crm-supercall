<?php
namespace backend\widgets\auditedorders;

use yii\base\Widget;

/**
 * Class AuditorLogs
 * @package backend\widgets\auditedorders
 */
class AuditorLogs extends Widget
{
    public $log;
    public $labels;
    public $product_log;

    public function run()
    {
        return $this->render('auditor-logs', [
            'log' => $this->log,
            'labels' => $this->labels,
            'product_log' => $this->product_log
        ]);
    }
}
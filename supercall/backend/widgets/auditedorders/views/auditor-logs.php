<?php

use \backend\modules\operator\models\AuditedOrders;

/** @var array $log */
/** @var array $labels */
/** @var array $product_log */
?>

<?php
//echo '<pre>' . print_r($log, 1) . '</pre>'; exit;
//echo '<pre>' . print_r($product_log, 1) . '</pre>';
?>

<fieldset>
    <legend><?=yii::t('common', 'Изменение заказа');?></legend>

    <table class="table table-striped" id="auditor_edit">

        <thead class="thead-dark">
        <tr>
            <th scope="col"><?= yii::t('common', 'ID'); ?></th>
            <th scope="col"><?= yii::t('common', ''); ?></th>
            <th scope="col"><?= yii::t('common', 'Поле'); ?></th>
            <th scope="col"><?= yii::t('common', 'Старое значение'); ?></th>
            <th scope="col"><?= yii::t('common', 'Новое значение'); ?></th>
            <th scope="col"><?= yii::t('common', 'Дата'); ?></th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($log as $idx => $data): ?>

            <?php foreach ($data as $name => $value) : ?>

                <tr>
                    <td><?= $value['id']; ?></td>
                    <td data-group="true"></td>
                    <td data-field="name" data-label="<?=$name;?>"><strong><?= isset($labels[$name]) ? $labels[$name] : $name ?></strong></td>
                    <td class="danger"><?= $value['old'] ?></td>
                    <td class="success"><?= $value['new'] ?></td>
                    <td><?= Yii::$app->formatter->asDate($value['after_date'], 'php:d/m/Y H:i:s'); ?></td>
                </tr>

            <?php endforeach; ?>

            <tr><td colspan="5"></td></tr>

        <?php endforeach; ?>

        </tbody>
    </table>

</fieldset>

<fieldset>
    <legend><?=yii::t('common', 'Изменение товара');?></legend>

    <table class="table table-striped">

        <thead class="thead-dark">
        <tr>
            <th scope="col"><?= yii::t('common', 'ID'); ?></th>
            <th scope="col"><?= yii::t('common', 'Действие'); ?></th>
            <th scope="col"><?= yii::t('common', 'Товар'); ?></th>
            <th scope="col"><?= yii::t('common', 'Количество'); ?></th>
            <th scope="col"><?= yii::t('common', 'Стоимость'); ?></th>
            <th scope="col"><?= yii::t('common', 'Дата'); ?></th>

        </tr>
        </thead>
        <tbody>

        <?php if(empty($product_log)) :?>

            <tr>
                <td colspan="6" class="text-center"><?=yii::t('common', 'Данных нет');?></td>
            </tr>

        <?php endif;?>

        <?php foreach ($product_log as $idx => $log): ?>

            <?php
            $class = '';

            switch ($log['action']) {
                case 'create' :
                    $class = 'info';
                    break;
                case 'update' :
                    $class = 'success';
                    break;
                case 'delete' :
                    $class = 'danger';
                    break;
            }
            ?>

            <tr class="<?= $class; ?>">
                <td><?= $log['id']; ?></td>
                <td><strong><?= $log['action']; ?></strong></td>
                <td><?= $log['product']['name']; ?></td>
                <td><?= $log['quantity']; ?></td>
                <td><?= $log['price']; ?></td>
                <td><?= Yii::$app->formatter->asDate($log['created_at'], 'php:d/m/Y H:i:s'); ?></td>
            </tr>

        <?php endforeach; ?>

        </tbody>
    </table>

</fieldset>
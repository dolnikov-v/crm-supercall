<?php
namespace backend\widgets;
use backend\modules\administration\models\BaseLogger;
use backend\modules\queue\models\Queue;
use yii\widgets\LinkPager;
use Yii;
use backend\modules\administration\models\search\BaseLoggerSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

/**
 * Class PanelWidget
 * @package backend\widgets
 */
class HistoryWidget extends \yii\base\Widget
{
    /**
     * @var string route
     */
    public $route;
    
    /**
     * @return string
     */
    public function run()
    {
        $modelSearch = new BaseLoggerSearch();
        
        $params = Yii::$app->request->queryParams;
        $params['route'] = $this->route;
        $query = BaseLogger::find();
            //->joinWith('logger_tag');
    
        $config = [
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => 'DESC'
                ]
            ]
        ];
    
        $dataProvider = new ActiveDataProvider($config);
        $query->andWhere(['route' => $this->route]);
        $query->andWhere(['type' => 'HISTORY']);
        $query->andWhere(['model' => Queue::className()]);

        return $this->render('history-widget', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'actions' => $modelSearch->getActions(),
            'footer' => LinkPager::widget(['pagination' => $dataProvider->getPagination()]),
        ]);
    }
}

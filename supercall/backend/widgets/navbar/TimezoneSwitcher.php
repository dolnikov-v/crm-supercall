<?php
namespace backend\widgets\navbar;

use common\models\Timezone;
use common\widgets\base\Dropdown;
use Yii;

/**
 * Class TimezoneSwitcher
 * @package backend\widgets\navbar
 */
class TimezoneSwitcher extends Dropdown
{
    /**
     * @var Timezone[]
     */
    private $timezones;

    /**
     * @return string
     */
    public function run()
    {
        $current = Yii::$app->user->timezone;
        $this->timezones = Yii::$app->cache->get('TimezoneSwitcher.timezones.' . Yii::$app->user->id);

        if ($this->timezones === false) {
            $this->timezones = Timezone::find()->all();
            Yii::$app->cache->set('TimezoneSwitcher.timezones.' . Yii::$app->user->id, $this->timezones, 600);
        }

        foreach ($this->timezones as $timezone) {
            $this->links[] = [
                'url' => '/' . $timezone->id . Yii::$app->request->url,
                'label' => $this->getHoursFormat($timezone->time_offset). ' ' . $timezone->timezone_id,
            ];
        }

        return $this->render('timezone-switcher', [
            'label' => $this->getHoursFormat($current->time_offset). ' ' . $current->timezone_id,
            'links' => $this->links,
        ]);
    }
    
    /**
     * Функция переводит time_offset в формате "HH::ii"
     *
     * @param $offset
     * @return string
     */
    private function getHoursFormat($offset)
    {
        // знак
        $mark = ($offset < 0) ? '-' : '+';
        $time = abs($offset);
    
        $hour = str_pad( ($time/3600)%24, 2, '0', STR_PAD_LEFT);
        $min = str_pad( ($time/60)%60, 2, '0', STR_PAD_LEFT);
        
        return $mark . $hour . ':' . $min;
    }
}

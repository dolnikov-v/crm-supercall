<?php
    use backend\assets\widgets\navbar\TimezoneSwitcherAsset;
    use common\helpers\fonts\PeIcon;
    
    /** @var string $label */
    /** @var array $links */

    TimezoneSwitcherAsset::register($this);
?>

<?php if ($links): ?>
    <li id="timezone_switcher_dropdown" class="dropdown dropdown-timezones open">
        <a id="timezone_switcher_menu" class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="true">
            <?= $label ?>
            <?php if (count($links) > 1): ?>
                <span class="caret"></span>
            <?php endif; ?>
        </a>

        <?php if (count($links) > 1): ?>
            <ul id="timezone_switcher_dropdown_menu" aria-labelledby="timezone_switcher_menu" class="dropdown-menu dropdown-menu-left">
                <li class="dropdown-menu-header">
                    <h5>
                        <?= Yii::t('common', 'Часовые пояса ({count})', [
                            'count' => count($links),
                        ]) ?>
                    </h5>
                </li>

                <li id="timezone_switcher_searcher" class="search">
                    <div class="input-search input-group-sm">
                        <input id="timezone_switcher_searcher_input" class="form-control" type="text"/>
                        <i class="<?= PeIcon::SEARCH ?>"></i>
                    </div>
                </li>

                <li class="list-group" data-plugin="scrollable">
                    <div data-role="container">
                        <div data-role="content">
                            <?php foreach ($links as $link): ?>
                                <a class="list-group-item" href="<?= $link['url'] ?>" data-name="<?= $link['label'] ?>">
                                    <div class="inner-container">
                                        <?= $link['label'] ?>
                                    </div>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </li>
            </ul>
        <?php endif; ?>
    </li>
<?php endif; ?>
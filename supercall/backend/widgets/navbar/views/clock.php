<?php
use backend\assets\widgets\navbar\ClockAsset;

/** @var string $time */
/** @var string $timeFormat */
/** @var string $date */

ClockAsset::register($this);
?>

<li class="hidden-xs">
    <div class="clock text-center" style="opacity: 0;">
        <div class="time atimer-auto-init" data-format="<?= $timeFormat ?>"><?= $time ?></div>
        <div class="date"><?= $date ?></div>
    </div>
</li>

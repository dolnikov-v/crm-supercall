<?php
use backend\assets\widgets\navbar\CountrySwitcherAsset;
use common\helpers\fonts\PeIcon;

/** @var string $label */
/** @var array $links */

CountrySwitcherAsset::register($this);
?>

<?php if ($links): ?>
    <li id="country_switcher_dropdown" class="dropdown dropdown-countries open">
        <a id="country_switcher_menu" class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
            <?= $label ?>
            <?php if (count($links) > 1): ?><span class="caret"></span><?php endif; ?>
        </a>

        <?php if (count($links) > 1): ?>
            <ul id="country_switcher_dropdown_menu" class="dropdown-menu dropdown-menu-right" style="opacity: 0;">
                <li class="dropdown-menu-header">
                    <h5>
                        <?= Yii::t('common', 'Доступные страны ({count})', [
                            'count' => count($links),
                        ]) ?>
                    </h5>
                </li>

                <li id="country_switcher_searcher" class="search">
                    <div class="input-search input-group-sm">
                        <input id="country_switcher_searcher_input" class="form-control" type="text"/>
                        <i class="<?= PeIcon::SEARCH ?>"></i>
                    </div>
                </li>

                <li class="list-group" data-plugin="scrollable">
                    <div data-role="container">
                        <div data-role="content">
                            <?php foreach ($links as $link): ?>
                                <a class="list-group-item" href="<?= $link['url'] ?>" data-name="<?= $link['label'] ?>">
                                    <div class="inner-container">
                                        <?= $link['label'] ?>
                                    </div>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </li>
            </ul>
        <?php endif; ?>
    </li>
<?php endif; ?>

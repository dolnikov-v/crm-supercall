<?php
/** @var string $icon */
/** @var array $links */
?>

<?php if ($links): ?>
    <li id="language_switcher_dropdown" class="dropdown dropdown-languages">
        <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
            <span class="flag-icon <?= $icon ?>"></span>
        </a>

        <ul class="dropdown-menu hdropdown">
            <?php foreach ($links as $link): ?>
                <li>
                    <a href="<?= $link['url'] ?>" role="menuitem">
                        <span class="flag-icon <?= $link['icon'] ?>"></span> <?= $link['label'] ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </li>
<?php endif; ?>

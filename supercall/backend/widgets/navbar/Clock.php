<?php
namespace backend\widgets\navbar;

use common\components\i18n\Formatter;
use common\helpers\i18n\Moment;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class Clock
 * @package backend\widgets\navbar
 */
class Clock extends Widget
{
    /**
     * @var string
     */
    public $time;

    /**
     * @var string
     */
    public $timeFormat;

    /**
     * @var string
     */
    public $date;

    /**
     * @return string
     */
    public function run()
    {
        /** @var Formatter $formatter */
        $formatter = Yii::$app->formatter;
    
        $default_timezone = $formatter->timeZone;
        // $formatter->timeZone = Yii::$app->user->country->timezone->timezone_id;
        $formatter->timeZone = Yii::$app->user->timezone->timezone_id;
    
        $timezone = new \DateTimeZone(Yii::$app->user->timezone->timezone_id);
        $time = new \DateTime('now', $timezone);
    
        $this->time = Html::tag('div', $time->format('H:i'));
        $this->date = $this->date ? $this->date : $formatter->asDate(time());
    
        $formatter->timeZone = $default_timezone;

        $this->timeFormat = $this->timeFormat ? $this->timeFormat : Moment::getTimeFormat();

        return $this->render('clock', [
            'time' => $this->time,
            'timeFormat' => $this->timeFormat,
            'date' => $this->date,
        ]);
    }
}

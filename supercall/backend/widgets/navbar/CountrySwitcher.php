<?php
namespace backend\widgets\navbar;

use common\models\Country;
use common\widgets\base\Dropdown;
use Yii;

/**
 * Class CountrySwitcher
 * @package backend\widgets\navbar
 */
class CountrySwitcher extends Dropdown
{
    /**
     * @var Country[]
     */
    private $countries;

    /**
     * @return string
     */
    public function run()
    {
        $current = Yii::$app->user->country;
        $this->countries = Yii::$app->cache->get('CountrySwitcher.countries.' . Yii::$app->user->id);

        if ($this->countries === false) {
            if (Yii::$app->user->isSuperadmin) {
                $this->countries = Country::find()
                    ->active()
                    ->all();
            } else {
                $this->countries = Yii::$app->user->identity->countries;
            }

            Yii::$app->cache->set('CountrySwitcher.countries.' . Yii::$app->user->id, $this->countries, 600);
        }

        usort($this->countries, function ($a, $b) {
            if ($a->name == $b->name) {
                return 0;
            }

            return $a->name > $b->name ? 1 : -1;
        });

        foreach ($this->countries as $country) {
            $this->links[] = [
                'url' => '/' . $country->slug . Yii::$app->request->url,
                'label' => $country->name,
            ];
        }

        return $this->render('country-switcher', [
            'label' => $current->name,
            'links' => $this->links,
        ]);
    }
}

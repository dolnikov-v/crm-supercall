<?php
namespace backend\widgets\navbar;

use common\widgets\base\Dropdown;
use common\models\Language;
use Yii;
use yii\helpers\Url;

/**
 * Class LanguageSwitcher
 * @package backend\widgets\navbar
 */
class LanguageSwitcher extends Dropdown
{
    /**
     * @var Language[]
     */
    private $languages;

    /**
     * @var Language
     */
    private $current;

    /**
     * @return string
     */
    public function run()
    {
        $this->current = Yii::$app->user->getLanguage();
        $this->languages = Yii::$app->cache->get('LanguageSwitcher.languages');

        if ($this->languages === false) {
            $this->languages = Language::find()
                ->active()
                ->all();

            Yii::$app->cache->set('LanguageSwitcher.languages', $this->languages, 3600);
        }

        foreach ($this->languages as $language) {
            $this->links[] = [
                'url' => Url::toRoute(['/i18n/language/change', 'locale' => $language->locale]),
                'label' => $language->name,
                'icon' => $language->icon,
            ];
        }

        return $this->render('language-switcher', [
            'icon' => $this->current->icon,
            'links' => $this->links,
        ]);
    }
}

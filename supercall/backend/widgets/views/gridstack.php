<?php
use backend\assets\GridStackAsset;

GridStackAsset::register($this);
?>
<div class="grid-stack" data-plugin="gridstack" data-gs-width="12" data-gs-animate="yes">
    <?php foreach ($items as $item): ?>
        <div class="grid-stack-item"
             data-gs-x="<?= $item['x'] ?>"
             data-gs-y="<?= $item['y'] ?>"
             data-gs-width="<?= $item['width'] ?>"
             data-gs-height="<?= $item['height'] ?>">
            <div class="grid-stack-item-content panel"><?= $item['content'] ?></div>
        </div>
    <?php endforeach; ?>
</div>

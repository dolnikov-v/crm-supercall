<?php
    use common\components\grid\GridView;
    use common\components\grid\IdColumn;
    use common\components\grid\DateColumn;
?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model) {
        
        },
        'columns' => [
            [
                'class' => IdColumn::className(),
                'headerOptions' => ['style' => 'width:10%'],
            ],
            [
                'attribute' => 'action',
                'headerOptions' => ['style' => 'width:10%'],
            ],
            [
                'attribute' => 'model',
                'headerOptions' => ['style' => 'width:20%'],
            ],
            [
                'attribute' => 'old_tags',
                'headerOptions' => ['style' => 'width:20%'],
                'content' => function ($data) {
                    $tags = json_decode($data->old_tags, 1);
                    if (is_array($tags)) {
                        $array_tags = [];
                        foreach ($tags as $tag => $value) {
                            $array_tags[] = '<div class="btn-xs btn btn-info"><span>'.$tag.'</span> : <span>'.$value.'</span></div>';
                        }
                        return implode('<br/>', $array_tags);
                    } else {
                        return null;
                    }
                }
            ],
            [
                'attribute' => 'tags',
                'headerOptions' => ['style' => 'width:20%'],
                'content' => function ($data) {
                    $tags = json_decode($data->tags, 1);
                    
                    if (is_array($tags)) {
                        $array_tags = [];
                        foreach ($tags as $tag => $value) {
                            $array_tags[] = '<div class="btn-xs btn btn-info"><span>'.$tag.'</span> : <span>'.$value.'</span></div>';
                        }
                        
                        return implode('<br/>', $array_tags);
                    } else {
                        return null;
                    }
                }
            ],
            [
                'attribute' => 'user_id',
                'headerOptions' => ['style' => 'width:10%'],
                'content' => function($data) {
                    if (!empty($data->user_id)) {
                        $userData = \common\models\User::findIdentity($data->user_id);
                        return $userData->username;
                    } else {
                        return null;
                    }
                    
                }
            ],
            [
                'attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
                'headerOptions' => ['class' => 'text-center', 'style' => 'width:10%'],
            ],
        ],
    ]);

?>


<?php
    use yii\helpers\Html;
    use backend\assets\widgets\DialerModalStyleAsset;

    DialerModalStyleAsset::register($this);
?>

        <div class="form-group">
            <div class="row" style="margin-bottom: 0px;">
                <div class="caller_box">
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="text" class="form-control" id="phoneNumper">
                            <input type="text" class="form-control hidden" id="phoneNumper_dial">
                        </div>
                        <div class="col-lg-1">
                            <button type="button" id="remove_one" class="btn btn-danger btn-md"
                                    title="<?= yii::t('common', 'Удалить') ?>">
                                <span class="glyphicon glyphicon-arrow-left"></span>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">1</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">2</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">3</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">4</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">5</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">6</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">7</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">8</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">9</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">0</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">#</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">+</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <button disabled="disabled" type="button" id="call_btn"
                                    class="btn btn-warning btn-md btn-block"
                                    title="<?= yii::t('common', 'Звонить') ?>">
                                <span class="glyphicon glyphicon-earphone"></span>
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button type="button" id="reset_btn" class="btn btn-danger btn-md btn-block"
                                    title="<?= yii::t('common', 'Сброс') ?>">
                                <span class="glyphicon glyphicon-phone-alt"></span>
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button type="button" id="pause_btn" class="btn btn-warning btn-md btn-block"
                                    title="<?= yii::t('common', 'Удержать') ?>">
                                <span class="glyphicon glyphicon-pause"></span>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <button type="button" id="volume_mute_btn" class="btn btn-default btn-xs btn-block"
                                    title="<?= yii::t('common', 'Отключить динамик') ?>">
                                <span class="glyphicon glyphicon-volume-off"></span>
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button type="button" id="speaker_mute_btn" class="btn btn-default btn-xs btn-block"
                                    title="<?= yii::t('common', 'Отключить микрофон') ?>">
                                <span class="glyphicon glyphicon-remove-circle"></span>
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <input type="text" readonly class="form-control" id="time" style="height: 25px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <audio id="audio_remote" autoplay="autoplay"> </audio>
        <audio id="ringtone" loop="" src="/resources/sounds/ringtone.wav"> </audio>
        <audio id="ringbacktone" loop="" src="/resources/sounds/ringbacktone.wav"> </audio>
        <audio id="dtmfTone" src="/resources/sounds/dtmf.wav"> </audio>



<?php
use backend\widgets\assets\custom\PanelWidgetAsset;
use common\widgets\base\GlyphIcon;
/** @var string $id Id панели */
/** @var string $title Название панели */
/** @var boolean $withBody Использовать ли блок panel-body */
/** @var string $content Контент */
/** @var string $footer Подвал */
/** @var string $alert Предупреждение */
/** @var string $alertStyle Предупреждение */
/** @var \backend\widgets\Nav $nav Табы */
/** @var boolean $border Границы */
/** @var boolean $color Цвет панели */

/** @var boolean $actions Границы */
/** @var boolean $collapse Кнопка свернуть */
/** @var boolean $fullScreen Кнопка на весь экран */
/** @var boolean $close Кнопка закрыть */
/** @var boolean $refresh Кнопка обновить */
/** @var boolean $export Кнопка экспорта */

PanelWidgetAsset::register($this);
?>
<div
    <?php if ($id): ?>
        id="<?= $id ?>"
    <?php endif; ?>
    class="panel <?php if ($border): ?>panel-bordered<?php endif; ?> <?= $color ?>">
    <?php if (trim($title)): ?>
    <div class="panel-heading">
        <?php if ($actions): ?>
            <div class="panel-actions text-right">
                <?php if (!is_bool($actions)): ?>
                    <?= $actions ?>
                <?php endif; ?>

                <?php if ($collapse): ?>
                    <a class="panel-action icon wb-minus"
                       aria-expanded="true"
                       data-toggle="panel-collapse"
                       aria-hidden="true"></a>
                <?php endif; ?>

                <?php if ($fullScreen): ?>
                    <a class="panel-action icon wb-expand"
                       data-toggle="panel-fullscreen"
                       aria-hidden="true"></a>
                <?php endif; ?>

                <?php if ($export): ?>
                    <a class="panel-action panel-action-export <?=GlyphIcon::EXPORT;?>"
                       data-toggle="panel-export"
                       data-load-callback="widget_export"
                       aria-hidden="true">
                        <span class="icon wb-download dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"></span>
                    </a>
                <?php endif; ?>

                <?php if ($refresh): ?>
                    <a class="panel-action <?=GlyphIcon::REFRESH;?>"
                       data-toggle="panel-refresh"
                       data-load-callback="widget_refresh"
                       aria-hidden="true"></a>
                <?php endif; ?>

                <?php if ($close): ?>
                    <a class="panel-action <?=GlyphIcon::CLOSE;?>"
                       data-toggle="panel-close"
                       aria-hidden="true"></a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <h3 class="panel-title"><?= $title ?></h3>
    </div>
    <?php endif; ?>

    <?php if (trim($alert)): ?>
        <div class="alert alert-dismissible <?= $alertStyle ?>" role="alert">
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">×</span>
            </button>
            <?= $alert ?>
        </div>
    <?php endif; ?>

    <?php if ($nav): ?>
        <?= $nav->renderTabs() ?>
    <?php endif; ?>

    <?php if ($withBody): ?>
        <div class="panel-body">
    <?php endif; ?>

    <?= $content ?>

    <?php if ($nav): ?>
        <?= $nav->renderContent() ?>
    <?php endif; ?>

    <?php if ($withBody): ?>
        </div>
    <?php endif; ?>

    <?php if (trim($footer)): ?>
        <div class="panel-footer clearfix">
            <?= $footer ?>
        </div>
    <?php endif; ?>
</div>

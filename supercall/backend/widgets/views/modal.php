<?php
/** @var string $id */
/** @var string $size */
/** @var string $nifty */
/** @var string $position */
/** @var string $color */
/** @var string $title */
/** @var string $close */
/** @var string $body */
/** @var string $bodyStyle */
/** @var string $footer */
/** @var string $footerStyle */
?>

<div <?php if ($id): ?>id="<?= $id ?>"<?php endif; ?> class="modal fade <?= $nifty ?> <?= $color ?> in" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog <?= $size ?>">
        <div class="modal-content">
            <?php if ($title || $close): ?>
                <div class="modal-header">
                    <?php if ($close): ?>
                        <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    <?php endif; ?>

                    <?php if ($title): ?>
                        <h4 class="modal-title"><?= $title ?></h4>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if (trim($body)): ?>
                <div class="modal-body <?= $bodyStyle ?>">
                    <?= $body ?>
                </div>
            <?php endif; ?>

            <?php if (trim($footer)): ?>
                <div class="modal-footer <?= $footerStyle ?>">
                    <?= $footer ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

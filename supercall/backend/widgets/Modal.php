<?php
namespace backend\widgets;

/**
 * Class Modal
 * @package backend\widgets
 */
class Modal extends \yii\base\Widget
{
    const SIZE_SMALL = 'modal-sm';
    const SIZE_MEDIUM = 'modal-md';
    const SIZE_LARGE = 'modal-lg';

    const POSITION_TOP = 'top';
    const POSITION_CENTER = 'center';
    const POSITION_BOTTOM = 'bottom';
    const POSITION_SIDEBAR = 'sidebar';

    const COLOR_PRIMARY = 'modal-primary';
    const COLOR_SUCCESS = 'modal-success';
    const COLOR_DANGER = 'modal-danger';
    const COLOR_WARNING = 'modal-warning';
    const COLOR_INFO = 'modal-info';

    /**
     * @var string Идетификатор
     */
    public $id;

    /**
     * @var string Размер
     */
    public $size = self::SIZE_MEDIUM;

    /**
     * @var string Тип появления
     */
    public $nifty;

    /**
     * @var string Позиция появления
     */
    public $position;

    /**
     * @var string Цвет
     */
    public $color;

    /**
     * @var string Заголовок
     */
    public $title;

    /**
     * @var bool Отображение кноки закрыть
     */
    public $close = true;

    /** @var string Тело */
    public $body;

    /** @var string */
    public $bodyStyle;

    /** @var string Подвал */
    public $footer;

    /** @var string */
    public $footerStyle;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('modal', [
            'id' => $this->id,
            'size' => $this->size,
            'nifty' => $this->nifty,
            'position' => $this->position,
            'color' => $this->color,
            'title' => $this->title,
            'close' => $this->close,
            'body' => $this->body,
            'bodyStyle' => $this->bodyStyle,
            'footer' => $this->footer,
            'footerStyle' => $this->footerStyle,
        ]);
    }
}

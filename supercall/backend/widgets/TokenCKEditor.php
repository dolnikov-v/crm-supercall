<?php

namespace backend\widgets;

use dosamigos\ckeditor\CKEditor;
use dosamigos\ckeditor\CKEditorWidgetAsset;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\View;

/**
 * Class TokenCKEditor
 * @package backend\widgets\assets\custom
 */
class TokenCKEditor extends CKEditor
{
    /**
     * @var null|array
     */
    public $tokens = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->prepareTokenButton();
    }

    /**
     * @inheritdoc
     */
    protected function prepareTokenButton()
    {
        if ($this->tokens) {
            $tokens = [];
            foreach ($this->tokens as $attr => $name) {
                $tokens[] = [$name, $attr];
            }
            $toolbar = $this->clientOptions['toolbar'] ?? [];
            array_unshift($toolbar, ['CreateToken']);
            $this->clientOptions['toolbar'] = $toolbar;
            $options = [
                'availableTokens' => $tokens,
                'contentsCss' => Yii::getAlias('@web/resources/widgets/token-ckeditor/token.css'),
                'tokenStart' => '{',
                'tokenEnd' => '}',
                'removePlugins' => 'elementspath',
                'extraPlugins' => 'lineutils,widget,token'
            ];
            $this->clientOptions = ArrayHelper::merge($options, $this->clientOptions);
        }
    }

    /**
     * @inheritdoc
     */
    public function registerPlugin()
    {
        $js = [];

        $view = $this->getView();

        CKEditorWidgetAsset::register($view);

        $id = $this->options['id'];

        $options = $this->clientOptions !== false && !empty($this->clientOptions)
            ? Json::encode($this->clientOptions)
            : '{}';


        $externalPath = Yii::getAlias('@web/resources/widgets/token-ckeditor/plugin');
        $js[] = "CKEDITOR.plugins.addExternal('lineutils', '{$externalPath}/lineutils/', 'plugin.js');";
        $js[] = "CKEDITOR.plugins.addExternal('widgetselection', '{$externalPath}/widgetselection/', 'plugin.js');";
        $js[] = "CKEDITOR.plugins.addExternal('widget', '{$externalPath}/widget/', 'plugin.js');";
        $js[] = "CKEDITOR.plugins.addExternal('token', '{$externalPath}/token/', 'plugin.js');";
        $js[] = "CKEDITOR.replace('$id', $options);";
        $js[] = "dosamigos.ckEditorWidget.registerOnChangeHandler('$id');";

        if (isset($this->clientOptions['filebrowserUploadUrl']) || isset($this->clientOptions['filebrowserImageUploadUrl'])) {
            $js[] = "dosamigos.ckEditorWidget.registerCsrfImageUploadHandler();";
        }

        $view->registerJs(implode("\n", $js), View::POS_END);
    }
}
<?php
namespace backend\widgets\aside;

use yii\base\Widget;

/**
 * Class Profile
 * @package backend\widgets\aside
 */
class Profile extends Widget
{
    public function run()
    {
        return $this->render('profile');
    }
}

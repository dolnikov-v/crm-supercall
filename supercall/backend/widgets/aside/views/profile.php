<?php

use common\assets\ProfileCommonAsset;
use common\models\Media;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\base\Dropdown;

ProfileCommonAsset::register($this);

$avatar = \common\models\Media::findOne(['user_id' => yii::$app->user->identity->id, 'type' => \common\models\Media::TYPE_PROFILE_IMAGE]);
?>

<div class="profile-picture">
    <div class="avatar" style="max-height:150px; overflow:hidden">
    <img class="<?=$avatar ? 'img-thumbnail' : 'img-circle m-b' ?>  width-150" src="<?=($avatar ? Media::getAbsolutePath(Media::TYPE_PROFILE_IMAGE) . '/' . $avatar->filename : '/resources/images/common/avatar-profile.png')?>" alt="avatar"/>
    </div>
    <br/>
    <div class="stats-label text-color">
        <span class="font-extra-bold font-uppercase">
            <?= Yii::$app->user->identity->username ?>
        </span>

        <?= Dropdown::widget([
            'label' => Html::tag('small', Yii::t('common', 'Профиль')),
            'withButton' => false,
            'links' => [
                [
                    'label' => Yii::t('common', 'Профиль'),
                    'visible' => Yii::$app->user->can('profile.control.index'),
                    'url' => '/profile/control',
                ],
                '',
                ['label' => Yii::t('common', 'Выход'),
                    'url' => Url::to('/auth/logout')
                ],
            ]
        ]) ?>
    </div>

    <?php if (yii::$app->user->identity && yii::$app->user->identity->ip): ?>

        <div class="row text-center">
            <div class="col-lg-12"><code><?= yii::$app->user->identity->ip; ?></code></div>
        </div>
        <br/>

    <?php endif; ?>

</div>
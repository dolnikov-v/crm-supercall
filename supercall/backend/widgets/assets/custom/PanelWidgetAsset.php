<?php
namespace backend\widgets\assets\custom;
use yii\web\AssetBundle;

/**
 * Class PanelWidgetAsset
 * @package backend\widgets\assets\custom
 */
class PanelWidgetAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@backend/web/widgets/custom/panel-widget';

    /**
     * @var array
     */
    public $js = [
        'panel.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'backend\assets\SiteAsset',
    ];
}

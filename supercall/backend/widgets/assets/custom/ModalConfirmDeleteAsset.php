<?php
namespace backend\widgets\assets\custom;

use yii\web\AssetBundle;

/**
 * Class ModalConfirmDeleteAsset
 * @package backend\widgets\assets\custom
 */
class ModalConfirmDeleteAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@backend/web/widgets/custom/modal-confirm-delete';

    /**
     * @var array
     */
    public $js = [
        'modal-confirm-delete.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'backend\assets\SiteAsset',
    ];
}

<?php
namespace backend\widgets\assets\custom;

use yii\web\AssetBundle;

/**
 * Class ModalConfirmAsset
 * @package backend\widgets\assets\custom
 */
class ModalConfirmAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@backend/web/widgets/custom/modal-confirm';

    /**
     * @var array
     */
    public $js = [
        'modal-confirm.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}
<?php
namespace backend\widgets\assets\audiojs;

use yii\web\AssetBundle;


/**
 * Class AudioJSAsset
 * @package backend\widgets\assets\audiojs
 */
class AudioJSAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/modules/order/audiojs';

    public $js = [
        'audio.min.js',
        'audiojs-init.js'
    ];
    public $depends = [
        //'common\assets\vendor\JqueryAsset'
    ];
}
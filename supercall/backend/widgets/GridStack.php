<?php
namespace backend\widgets;
use common\widgets\base\Widget;

/**
 * Class Dropdown
 * @package app\widgets
 */
class GridStack extends Widget
{
    /**
     * @var array
     */
    public $items;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('gridstack', [
            'items' => $this->items,
        ]);
    }
}

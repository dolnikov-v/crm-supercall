<?php
namespace backend\widgets;

use common\models\User;
use common\widgets\base\Widget;

class DialerModal extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $user = User::findOne(\Yii::$app->user->id);
        return $this->render('dialer-modal', [
            'data' => !empty($user->sipArray) ? $user->sipArray : [],
        ]);
    }
}

<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

if(!defined('STDIN'))  define('STDIN',  fopen('php://stdin',  'r'));
if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'w'));
if(!defined('STDERR')) define('STDERR', fopen('php://stderr', 'w'));

return [
    'id' => 'app-backend',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'backend\controllers',
    'defaultRoute' => 'home/index',
    'name' => '2WCall',
    'components' => [
        'request' => [
            'class' => 'common\components\web\Request',
            'cookieValidationKey' => 'k9UL1hykVkvXNEtSJPFy8M8GZLcysaha',
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-backend',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'class' => 'yii\web\CacheSession',
            'name' => 'advanced-backend',
            'timeout' => 3600 * 24 * 30,
            'cookieParams' => [
                'lifetime' => 3600 * 24 * 30,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'errors/index',
        ],
        'assetManager' => [
            'linkAssets' => YII_ENV_DEV ? true : false,
            'bundles' => [
                'yii\web\YiiAsset' => [
                    'depends' => [
                        'common\assets\vendor\BootstrapAsset',
                    ],
                ],
                'yii\web\JqueryAsset' => [
                    'css' => [],
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'css' => [],
                    'js' => [],
                ],
            ],
        ],
        'urlManager' => [
            'class' => 'common\components\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<module:[\w-]+>/<folder:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<folder>/<controller>/<action>',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',

                '<module:[\w-]+>/<folder:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<folder>/<controller>/<action>',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
            ],
        ],
        'view' => [
            'class' => 'common\components\web\View'
        ],

        'frontendCache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => Yii::getAlias('@frontend') . '/runtime/cache',
        ],
        'apiCache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => Yii::getAlias('@api') . '/runtime/cache',
        ],
        'frontendAssetManager' => [
            'class' => 'yii\web\AssetManager',
            'basePath' => Yii::getAlias('@frontend') . '/web/assets',
        ],
        'apiAssetManager' => [
            'class' => 'yii\web\AssetManager',
            'basePath' => Yii::getAlias('@api') . '/web/assets',
        ],
        'GoogleSpeech' => [
            'class' => 'backend\components\GoogleSpeech',
            'key' => 'AIzaSyCzOhcnaanNNyd7mh7d1BGe7PSWV3i9beU',
            'api' => 'https://speech.googleapis.com/v1/speech:recognize?fields=results&key=',
            'getCropedByAsteriskUrl' => 'http://mcall-aster2.rtbads.com/transcribe/manage-crop-files.php',
            'deleteCropFromAsteriskUrl' => 'http://mcall-aster2.rtbads.com/transcribe/manage-crop-files.php',
            'path' => '/var/recording/in/',
            'path_asterisk' => 'http://136.243.130.196/acdr/download.php?audio='
        ],
        'Asterisk2' => include_once('asterisk2.php'),
        'Wtrade' => [
            'class' => 'backend\components\Wtrade',
            'webHookPayUrl' => 'http://2wtrade-pay.com/webhook/external-source-role/update',
            'DevWebHookPayUrl' => 'http://dev.2wtrade-pay.com/webhook/external-source-role/update'
        ],
        'AwsS3' => [
            'class' => 'backend\components\AwsS3',
            'region' => 'eu-west-1',
            'profile' => 'default',
            'version' => 'latest',
            'credentials' => include ('aws.php')
        ],
        'AwsTranscribe' => [
            'class' => 'backend\components\AwsTranscribe',
            'region' => 'eu-west-1',
            'version' => 'latest',
            'credentials' => include ('aws.php'),
        ],
    ],
    'modules' => [
        'access' => [
            'class' => 'backend\modules\access\Module',
        ],
        'administration' => [
            'class' => 'backend\modules\administration\Module',
        ],
        'audit' => [
            'class' => 'backend\modules\audit\Module',
        ],
        'auth' => [
            'class' => 'backend\modules\auth\Module',
        ],
        'catalog' => [
            'class' => 'backend\modules\catalog\Module',
        ],
        'home' => [
            'class' => 'backend\modules\home\Module',
        ],
        'i18n' => [
            'class' => 'backend\modules\i18n\Module',
        ],
        'order' => [
            'class' => 'backend\modules\order\Module',
        ],
        'stats' => [
            'class' => 'backend\modules\stats\Module'
        ],
        'queue' => [
            'class' => 'backend\modules\queue\Module',
        ],
        'wiki' => [
            'class' => 'backend\modules\wiki\Module',
        ],
        'presets' => [
            'class' => 'backend\modules\presets\Module',
        ],
        'v1' => [
            'class' => 'api\modules\v1\Module',
        ],
        'operator' => [
            'class' => 'backend\modules\operator\Module',
        ],
        'profile' => [
            'class' => 'backend\modules\profile\Module',
        ],
        'widget' => [
            'class' => 'backend\modules\widget\Module',
        ],
        'webhook' => [
            'class' => 'backend\modules\webhook\Module',
            'controllerMap' => [
                'log' => [
                    'class' => 'backend\modules\webhook\controllers\LogController',
                    'amqp' => 'amqp'
                ],
                'auto-call' => [
                    'class' => 'backend\modules\webhook\controllers\AutoCallController',
                    'amqp' => 'amqp'
                ],
            ],
        ],

    ],
    'params' => $params,
];

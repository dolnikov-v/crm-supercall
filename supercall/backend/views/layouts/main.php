<?php
use backend\assets\base\ThemeAsset;
use yii\helpers\Html;
use frontend\assets\UserReadyAsset;

use backend\assets\base\ThemeSloganAsset;

/** @var \yii\web\View $this */
/** @var string $content */

/* base */
ThemeAsset::register($this);
UserReadyAsset::register($this);
ThemeSloganAsset::register($this);
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: <?= Yii::$app->name ?></title>
    <link rel="icon" href="<?= Yii::$app->params['urlBackend'] ?>/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?= Yii::$app->params['urlBackend'] ?>/favicon.ico" type="image/x-icon">
    <?php $this->head(); ?>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(52570804, "init", {
            id:52570804,
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/52570804" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

</head>
<body class="fixed-navbar fixed-sidebar">
<?php $this->beginBody(); ?>
<?= $this->render('@backend/views/common/_splash') ?>
<?= $this->render('@backend/views/common/_header') ?>
<?= $this->render('@backend/views/common/_aside') ?>
<?= $this->render('@backend/views/common/_dialer_modal') ?>

<div id="wrapper">
    <?= $this->render('@backend/views/common/_panel-header') ?>
    <div class="content animate-panel">
        <?= $content ?>
    </div>
    <?= $this->render('@backend/views/common/_footer') ?>
</div>


<?php $this->endBody(); ?>
<?= $this->render('@backend/views/common/_notifiers') ?>
</body>
</html>
<?php $this->endPage(); ?>

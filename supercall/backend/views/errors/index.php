<?php
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\web\HttpException $exception */
/** @var string $name */
/** @var string $message */

$this->title = $name;
?>

<div class="color-line"></div>

<div class="error-container">
    <i class="pe-7s-way text-success big-icon"></i>
    <h1><?= $exception->statusCode ?></h1>
    <strong><?= Yii::t('common', 'При обработке вашего запроса произошла ошибка.') ?></strong>
    <p>
        <?= nl2br(Html::encode($message)) ?>
    </p>
    <a href="<?= Yii::$app->params['urlBackend'] ?>"
       class="btn btn-xs btn-success"><?= Yii::t('common', 'Вернуться на главную') ?></a>
</div>

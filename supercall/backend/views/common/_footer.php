<?php
use common\widgets\base\ButtonLink;
use backend\assets\JiraIssueCollectorsAsset;

JiraIssueCollectorsAsset::register($this);
?>

    <footer class="footer">
        © <?= date('Y') . ' ' . Yii::$app->params['companyName']; ?>

        <?php if (!Yii::$app->user->isGuest): ?>

        <?php endif; ?>
    </footer>
    <div id="document_domain" style="display: none"><?= yii::$app->params['document.domain']; ?></div>
    <div style="display: none" id="url_backend"><?= yii::$app->params['urlBackend']; ?></div>

<?php if (!Yii::$app->user->isGuest): ?>

    <div class="hidden" id="user_id"><?= yii::$app->user->identity->id; ?></div>
    <div class="hidden"
         id="user_email"><?= isset(yii::$app->user->identity->email) ? yii::$app->user->identity->email : ''; ?></div>

    <div class="hidden" id="jira_issue_collectors_url"><?= yii::$app->params['jiraIssueCollector']['url'] ?></div>
    <div class="hidden"
         id="jira_issue_collectors_language"><?= (isset(Yii::$app->params['jiraIssueCollector']['collectorId'][Yii::$app->language])
            ? Yii::$app->params['jiraIssueCollector']['collectorId'][Yii::$app->language]
            : Yii::$app->params['jiraIssueCollector']['collectorId']['en-US']); ?></div>


    <div style="display: none" id="asterisk_websocket"><?=yii::$app->params['asterisk_websocket'];?></div>
    <div style="display: none" id="asterisk_ice_server"><?=yii::$app->params['asterisk_ice_server'];?></div>
    <div style="display: none" id="asterisk_user_sip_data"><?=yii::$app->user->identity->sip;?></div>
    <div style="display: none" id="asterisk_identity_tpl"><?=yii::$app->params['asterisk_identity_tpl'];?></div>
    <div style="display: none" id="asterisk_server"><?=yii::$app->params['asterisk_server'];?></div>
    <div style="display: none" id="asterisk_sipml5_disabled_logs"><?=yii::$app->params['asterisk_sipml5_disabled_logs'];?></div>
<?php endif; ?>

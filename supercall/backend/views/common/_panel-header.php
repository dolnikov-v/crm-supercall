<?php
use common\widgets\base\Breadcrumbs;
use common\widgets\base\PanelHeader;

/** @var \yii\web\View $this */
?>


<div id="slogan" class="slogan-area" data-spy="affix" data-offset-top="0">
    <div class="normalheader small-header">
        <div class="hpanel">

            <div class="panel-body">

                <div class="text-center align-middle slogan-panel"><?= Yii::$app->params['slogan'] ?></div>
            </div>
        </div>
    </div>
</div>
<?php if (!isset($this->params['showPanelHeader']) || $this->params['showPanelHeader'] == true): ?>
    <?= PanelHeader::widget([
        'title' => $this->title,
        'breadcrumbs' => Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
    ]) ?>
<?php endif; ?>

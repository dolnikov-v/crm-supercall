<?php

use backend\widgets\aside\Profile;
use backend\widgets\Menu;
use common\helpers\fonts\PeIcon;
use common\modules\order\models\Order;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

$moduleId = Yii::$app->controller->module->id;;
$controllerId = Yii::$app->controller->id;
$actionId = Yii::$app->controller->action->id;
?>

<aside id="menu">
    <div id="navigation">
        <?= Profile::widget() ?>

        <?php
            $allowedRoles = [User::ROLE_AUDITOR, User::ROLE_SUPERADMIN, User::ROLE_QUALITY_MANAGER];
            if (in_array(Yii::$app->user->identity->getRolesNames(), $allowedRoles)) {
                echo  \yii\bootstrap\Button::widget([
                    'label' => Yii::t('common', 'Телефон'),
                    'options' => ['class' => 'btn btn-success btn-sm zoomed width-100 center-block'],
                    'id' => 'dialer_modal_btn'
                ]);
            }
        ?>

        <br />


        <?= Html::a(Yii::t('common', 'База знаний'), Url::to(yii::$app->params['urlFrontend'].'/wiki/article/index'), [
            'class' => 'btn btn-sm btn-primary btn-block zoomed width-100 center-block',
            'target' => '_blank',
        ]); ?>
        <br/>


        <?= Menu::widget([
            'options' => [
                'id' => 'side-menu',
                'class' => 'nav',
            ],
            'items' => [
                [
                    'label' => Yii::t('common', 'Главная страница'),
                    'visible' => Yii::$app->user->can('home.index.index'),
                    'icon' => PeIcon::HOME,
                    'url' => ['/home/index'],
                    'active' => $moduleId == 'home',
                ],
                [
                    'label' => Yii::t('common', 'Профиль'),
                    'visible' => Yii::$app->user->can('profile.control.index'),
                    'icon' => PeIcon::HOME,
                    'url' => ['/profile/control'],
                    'active' => $moduleId == 'profile',
                ],
                [
                    'label' => Yii::t('common', 'Заказы'),
                    'visible' => Yii::$app->user->can('order.index.index') ||
                        Yii::$app->user->can('order.addressverification.index') ||
                        Yii::$app->user->can('order.index.wrong') ||
                        Yii::$app->user->can('queue.control.index') ||
                        Yii::$app->user->can('queue.user.index'),
                    'icon' => PeIcon::ALBUMS,
                    'url' => '#',
                    // 'active' => $moduleId == 'order',
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Заказы с ошибками'),
                            'visible' => Yii::$app->user->can('order.index.wrong') && Order::getWrongOrders(),
                            'url' => ['/order/index/wrong'],
                            'active' => $moduleId == 'order' && $controllerId == 'index' && $actionId == 'wrong',
                        ],
                        [
                            'label' => Yii::t('common', 'Список заказов'),
                            'visible' => Yii::$app->user->can('order.index.index'),
                            'url' => ['/order/index'],
                            'active' => $moduleId == 'order' && $controllerId == 'index' && $actionId == 'index',
                        ],
                        [
                            'label' => Yii::t('common', 'Подтверждение адреса'),
                            'visible' => Yii::$app->user->can('order.addressverification.index'),
                            'url' => ['/order/address-verification'],
                            'active' => $moduleId == 'order' && ($controllerId == 'address-verification'),
                        ],
                        [
                            'label' => Yii::t('common', 'Очереди'),
                            'visible' => Yii::$app->user->can('queue.control.index'),
                            'url' => ['/queue/control'],
                            'active' => $moduleId == 'queue' && ($controllerId == 'control'),
                        ],
                        [
                            'label' => Yii::t('common', 'Доступ к очередям'),
                            'visible' => Yii::$app->user->can('queue.user.index'),
                            'url' => ['/queue/user'],
                            'active' => $moduleId == 'queue' && ($controllerId == 'user'),
                        ],
                        [
                            'label' => Yii::t('common', 'Пресеты'),
                            'visible' => false,
                            'url' => ['/presets/control'],
                            'active' => $moduleId == 'presets' && ($controllerId == 'control'),
                        ],
                        [
                            'label' => Yii::t('common', 'Оповещения при недозвоне'),
                            'visible' => Yii::$app->user->can('order.noanswernotification.index'),
                            'url' => ['/order/no-answer-notification/index'],
                            'active' => $moduleId == 'order' && ($controllerId == 'no-answer-notification'),
                        ],
                    ]
                ],
                [
                    'label' => Yii::t('common', 'Аудит КЦ'),
                    'visible' => Yii::$app->user->can('operator.questionnaire.index') ||
                        Yii::$app->user->can('operator.questionnaire.add') ||
                        Yii::$app->user->can('operator.questionnaire.edit') ||
                        Yii::$app->user->can('operator.request.confirm') ||
                        Yii::$app->user->can('operator.request.reject') ||
                        Yii::$app->user->can('operator.request.block') ||
                        Yii::$app->user->can('operator.qualitycontrol.index') ||
                        Yii::$app->user->can('operator.auditedorders.index') ||
                        Yii::$app->user->can('operator.statisticsofauditors.index') ||
                        Yii::$app->user->can('audit.verifiedaddress.index') ||
                        Yii::$app->user->can('audit.transcribesetting.index'),
                    'icon' => PeIcon::ALBUMS,
                    'url' => '#',
                    'active' => in_array($moduleId, ['operator', 'audit']),
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Проверенные адреса'),
                            'visible' => Yii::$app->user->can('audit.verifiedaddress.index'),
                            'url' => ['/audit/verified-address'],
                            'active' => $moduleId == 'audit' && ($controllerId == 'verified-address'),
                        ],
                        [
                            'label' => Yii::t('common', 'Планы оценки работы'),
                            'visible' => Yii::$app->user->can('operator.questionnaire.index'),
                            'url' => ['/operator/questionnaire'],
                            'active' => $moduleId == 'operator' && ($controllerId == 'questionnaire'),
                        ],
                        [
                            'label' => Yii::t('common', 'Управление опросами операторов'),
                            'visible' => Yii::$app->user->can('operator.questionnairemanagement.index'),
                            'url' => ['/operator/questionnaire-management'],
                            'active' => $moduleId == 'operator' && ($controllerId == 'questionnaire-management'),
                        ],
                        [
                            'label' => Yii::t('common', 'Контроль качества'),
                            'visible' => Yii::$app->user->can('operator.qualitycontrol.index'),
                            'url' => ['/operator/quality-control'],
                            'active' => $moduleId == 'operator' && ($controllerId == 'quality-control'),
                        ],
                        [
                            'label' => Yii::t('common', 'Проверенные заказы'),
                            'visible' => Yii::$app->user->can('operator.auditedorders.index'),
                            'url' => ['/operator/audited-orders'],
                            'active' => $moduleId == 'operator' && ($controllerId == 'audited-orders'),
                        ],
                        [
                            'label' => Yii::t('common', 'Статистика по аудиторам'),
                            'visible' => Yii::$app->user->can('operator.statisticsofauditors.index'),
                            'url' => ['/operator/statistics-of-auditors/index'],
                            'active' => $moduleId == 'operator' && ($controllerId == 'statistics-of-auditors'),
                        ],
                        [
                            'label' => Yii::t('common', 'Настройка транскрибации'),
                            'visible' => Yii::$app->user->can('audit.transcribesetting.index'),
                            'url' => ['/audit/transcribe-setting/index'],
                            'active' => $moduleId == 'audit' && ($controllerId == 'transcribe-setting'),
                        ],
                        [
                            'label' => Yii::t('common', 'Статистика распознанных ключевых слов'),
                            'visible' => Yii::$app->user->can('audit.statskeyword.index'),
                            'url' => ['/audit/stats-key-word/index'],
                            'active' => $moduleId == 'audit' && ($controllerId == 'statistics-key-word'),
                        ],
                    ]
                ],
                [
                    'label' => Yii::t('common', 'Операторы'),
                    'visible' =>
                        Yii::$app->user->can('operator.listoperators.index') ||
                        Yii::$app->user->can('operator.listoperators.queue') ||
                        Yii::$app->user->can('operator.listoperators.queueset') ||
                        Yii::$app->user->can('operator.listoperators.queueunset') ||
                        Yii::$app->user->can('operator.zingtree.index') ||
                        Yii::$app->user->can('operator.zingtree.edit') ||
                        Yii::$app->user->can('operator.zingtree.delete') ||
                        Yii::$app->user->can('operator.zingtree.activate'),
                    'icon' => PeIcon::OPERATORS,
                    'url' => '#',
                    'active' => $moduleId == 'operator',
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Список операторов'),
                            'visible' =>
                                Yii::$app->user->can('operator.listoperators.index') ||
                                Yii::$app->user->can('operator.qualitycontrol.index') ||
                                Yii::$app->user->can('operator.listoperators.queue') ||
                                Yii::$app->user->can('operator.listoperators.queueset') ||
                                Yii::$app->user->can('operator.listoperators.queueunset'),

                            'url' => ['/operator/list-operators'],
                            'active' => $moduleId == 'operator' && ($controllerId == 'list-operators'),
                        ],
                        [
                            'label' => Yii::t('common', 'Заявки'),
                            'visible' => Yii::$app->user->can('operator.request.confirm') ||
                                Yii::$app->user->can('operator.request.reject') ||
                                Yii::$app->user->can('operator.request.block'),
                            'url' => ['/operator/request'],
                            'active' => $moduleId == 'operator' && ($controllerId == 'request'),
                        ],
                        [
                            'label' => Yii::t('common', 'ZingTree'),
                            'visible' => Yii::$app->user->can('operator.zingtree.index'),
                            'url' => ['/operator/zing-tree'],
                            'active' => $moduleId == 'operator' && ($controllerId == 'zing-tree'),
                        ],
                    ]
                ],
                [
                    'label' => Yii::t('common', 'Партнеры'),
                    'visible' =>
                        Yii::$app->user->can('partner.control.index') ||
                        Yii::$app->user->can('partner.form.index') ||
                        Yii::$app->user->can('partner.vat.index') ||
                        Yii::$app->user->can('partner.settings.index') ||
                        Yii::$app->user->can('partner.shipping.index') ||
                        Yii::$app->user->can('partner.product.index'),
                    'icon' => PeIcon::PARTNER,
                    'url' => '#',
                    'active' => $moduleId == 'partner',
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Управление'),
                            'visible' => Yii::$app->user->can('partner.control.index'),
                            'url' => ['/partner/control'],
                            'active' => $moduleId == 'partner' && ($controllerId == 'control'),
                        ],
                        [
                            'label' => Yii::t('common', 'Товары'),
                            'visible' => Yii::$app->user->can('partner.product.index'),
                            'url' => ['/partner/product'],
                            'active' => $moduleId == 'partner' && ($controllerId == 'product'),
                        ],
                        [
                            'label' => Yii::t('common', 'Акции'),
                            'visible' => Yii::$app->user->can('partner.package.index'),
                            'url' => ['/partner/package'],
                            'active' => $moduleId == 'partner' && ($controllerId == 'package'),
                        ],
                        [
                            'label' => Yii::t('common', 'Курьерские службы'),
                            'visible' => Yii::$app->user->can('partner.shipping.index'),
                            'url' => ['/partner/shipping'],
                            'active' => $moduleId == 'partner' && ($controllerId == 'shipping'),
                        ],
                        [
                            'label' => Yii::t('common', 'Формы'),
                            'visible' => Yii::$app->user->can('partner.form.index'),
                            'url' => ['/partner/form'],
                            'active' => $moduleId == 'partner' && ($controllerId == 'form'),
                        ],
                        [
                            'label' => Yii::t('common', 'НДС'),
                            'visible' => Yii::$app->user->can('partner.vat.index'),
                            'url' => ['/partner/vat'],
                            'active' => $moduleId == 'partner' && ($controllerId == 'vat'),
                        ],
                        [
                            'label' => Yii::t('common', 'Настройки'),
                            'visible' => Yii::$app->user->can('partner.settings.index'),
                            'url' => ['/partner/settings'],
                            'active' => $moduleId == 'partner' && ($controllerId == 'settings'),
                        ],
                    ]
                ],

                [
                    'label' => Yii::t('common', 'Справочники'),
                    'visible' => Yii::$app->user->can('catalog.country.index')
                        || Yii::$app->user->can('catalog.timezone.index')
                        || Yii::$app->user->can('catalog.product.index'),
                    'icon' => PeIcon::BOOKMARKS,
                    'url' => '#',
                    'active' => $moduleId == 'catalog',
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Страны'),
                            'visible' => Yii::$app->user->can('catalog.country.index'),
                            'url' => ['/catalog/country'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'country'),
                        ],
                        [
                            'label' => Yii::t('common', 'КЛАДР стран'),
                            'visible' => Yii::$app->user->can('catalog.cladercountries.index'),
                            'url' => ['/catalog/klader-countries'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'klader-countries'),
                        ],
                        [
                            'label' => Yii::t('common', 'Временные зоны'),
                            'visible' => Yii::$app->user->can('catalog.timezone.index'),
                            'url' => ['/catalog/timezone'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'timezone'),
                        ],
                        [
                            'label' => Yii::t('common', 'Типы заказов'),
                            'visible' => Yii::$app->user->can('catalog.order-type.index'),
                            'url' => ['/catalog/order-type'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'order-type'),
                        ],
                        [
                            'label' => Yii::t('common', 'Субстатусы заказов'),
                            'visible' => Yii::$app->user->can('catalog.sub-status.index'),
                            'url' => ['/catalog/sub-status'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'sub-status'),
                        ],
                        [
                            'label' => Yii::t('common', 'Валюты'),
                            'visible' => Yii::$app->user->can('catalog.currency.index'),
                            'url' => ['/catalog/currency'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'currency'),
                        ],
                        [
                            'label' => Yii::t('common', 'Товары'),
                            'visible' => Yii::$app->user->can('catalog.product.index'),
                            'url' => ['/catalog/product'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'product'),
                        ],
                        [
                            'label' => Yii::t('common', 'Курьерские службы'),
                            'visible' => Yii::$app->user->can('catalog.shipping.index'),
                            'url' => ['/catalog/shipping'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'shipping'),
                        ],
                        [
                            'label' => Yii::t('common', 'Стоимость доставки'),
                            'visible' => Yii::$app->user->can('catalog.priceshipping.index'),
                            'url' => ['/catalog/price-shipping'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'price-shipping'),
                        ],
                        [
                            'label' => Yii::t('common', 'Скрипты для операторов'),
                            'visible' => Yii::$app->user->can('catalog.scriptsoperators.index'),
                            'url' => ['/catalog/scripts-operators'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'scripts-operators'),
                        ],
                        [
                            'label' => Yii::t('common', 'Ключи Google'),
                            'visible' => Yii::$app->user->can('catalog.gmap-key.index'),
                            'url' => ['/catalog/gmap-key'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'gmap-key'),
                        ],
                        [
                            'label' => Yii::t('common', 'Источники'),
                            'visible' => Yii::$app->user->can('catalog.source.index'),
                            'url' => ['/catalog/source'],
                            'active' => $moduleId == 'catalog' && ($controllerId == 'source'),
                        ],
                    ],
                ],

                [
                    'label' => Yii::t('common', 'Статистика'),
                    'visible' => Yii::$app->user->can('stats.order.index')
                        || Yii::$app->user->can('stats.queue.index')
                        || Yii::$app->user->can('stats.product.index')
                        || Yii::$app->user->can('stats.operator.index')
                        || Yii::$app->user->can('stats.operatoronline.index')
                        || Yii::$app->user->can('stats.orderprocessingtime.dates')
                        || Yii::$app->user->can('stats.orderprocessingtime.orders')
                        || Yii::$app->user->can('stats.orderprocessingtime.statuses')
                        || Yii::$app->user->can('stats.orderprocessingtime.products')
                        || Yii::$app->user->can('stats.orderbuyout.index'),
                    'icon' => PeIcon::STAT,
                    'url' => '#',
                    'active' => $moduleId == 'stats',
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Статистика заказов'),
                            'visible' => Yii::$app->user->can('stats.order.index'),
                            'url' => ['/stats/order'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'order'),
                        ],
                        [
                            'label' => Yii::t('common', 'Статистика очередей'),
                            'visible' => Yii::$app->user->can('stats.queue.index'),
                            'url' => ['/stats/queue'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'queue'),
                        ],
                        [
                            'label' => Yii::t('common', 'Статистика обработки заказов'),
                            'visible' => false,//Yii::$app->user->can('stats.log.general'),
                            'url' => ['/stats/log/general'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'log'),
                        ],
                        [
                            'label' => Yii::t('common', 'Статистика по товарам'),
                            'visible' => Yii::$app->user->can('stats.product.index'),
                            'url' => ['/stats/product'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'product'),
                        ],
                        [
                            'label' => Yii::t('common', 'Статистика по выкупам'),
                            'visible' => Yii::$app->user->can('stats.orderbuyout.index'),
                            'url' => ['/stats/order-buyout'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'order-buyout'),
                        ],
                        [
                            'label' => Yii::t('common', 'Статистика по операторам'),
                            'visible' => Yii::$app->user->can('stats.operator.index'),
                            'url' => ['/stats/operator'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'operator'),
                        ],
                        [
                            'label' => Yii::t('common', 'Статистика операторов по времени онлайн'),
                            'visible' => Yii::$app->user->can('stats.operatoronline.index'),
                            'url' => ['/stats/operator-online'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'operator-online'),
                        ],
                        [
                            'label' => Yii::t('common', 'Затраты времени по датам'),
                            'visible' => Yii::$app->user->can('stats.orderprocessingtime.dates'),
                            'url' => ['/stats/order-processing-time/dates'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'order-processing-time' && $actionId == 'dates'),
                        ],
                        [
                            'label' => Yii::t('common', 'Затраты времени по заказам'),
                            'visible' => Yii::$app->user->can('stats.orderprocessingtime.orders'),
                            'url' => ['/stats/order-processing-time/orders'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'order-processing-time' && $actionId == 'orders'),
                        ],
                        [
                            'label' => Yii::t('common', 'Затраты времени по статусам'),
                            'visible' => Yii::$app->user->can('stats.orderprocessingtime.statuses'),
                            'url' => ['/stats/order-processing-time/statuses'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'order-processing-time' && $actionId == 'statuses')
                        ],
                        [
                            'label' => Yii::t('common', 'Затраты времени по товарам'),
                            'visible' => Yii::$app->user->can('stats.orderprocessingtime.products'),
                            'url' => ['/stats/order-processing-time/products'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'order-processing-time' && $actionId == 'products'),
                        ],
                        [
                            'label' => Yii::t('common', 'Прогнозирование количества заказов'),
                            'visible' => Yii::$app->user->can('stats.forecastorders.index'),
                            'url' => ['/stats/forecast-orders/index'],
                            'active' => $moduleId == 'stats' && ($controllerId == 'forecast-orders' && $actionId == 'index'),
                        ],
                    ],
                ],

                [
                    'label' => Yii::t('common', 'Интернационализация'),
                    'visible' => Yii::$app->user->can('i18n.language.index')
                        || Yii::$app->user->can('i18n.phrase.index')
                        || Yii::$app->user->can('i18n.translation.index'),
                    'icon' => PeIcon::WORLD,
                    'url' => '#',
                    'active' => $moduleId == 'i18n',
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Список языков'),
                            'visible' => Yii::$app->user->can('i18n.language.index'),
                            'url' => ['/i18n/language'],
                            'active' => $moduleId == 'i18n' && ($controllerId == 'language'),
                        ],
                        [
                            'label' => Yii::t('common', 'Список фраз'),
                            'visible' => Yii::$app->user->can('i18n.phrase.index'),
                            'url' => ['/i18n/phrase'],
                            'active' => $moduleId == 'i18n' && ($controllerId == 'phrase'),
                        ],
                        [
                            'label' => Yii::t('common', 'Список переводов'),
                            'visible' => Yii::$app->user->can('i18n.translation.index'),
                            'url' => ['/i18n/translation'],
                            'active' => $moduleId == 'i18n' && ($controllerId == 'translation'),
                        ],
                    ],
                ],

                [
                    'label' => Yii::t('common', 'Доступы'),
                    'visible' => Yii::$app->user->can('access.user.index')
                        || Yii::$app->user->can('access.role.index')
                        || Yii::$app->user->can('access.permission.index')
                        || Yii::$app->user->can('access.team.index'),
                    'icon' => PeIcon::KEY,
                    'url' => '#',
                    'active' => $moduleId == 'access',
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Список пользователей'),
                            'visible' => Yii::$app->user->can('access.user.index'),
                            'url' => ['/access/user'],
                            'active' => $moduleId == 'access' && ($controllerId == 'user'),
                        ],
                        [
                            'label' => Yii::t('common', 'Список команд'),
                            'visible' => Yii::$app->user->can('access.team.index'),
                            'url' => ['/access/team'],
                            'active' => $moduleId == 'access' && ($controllerId == 'team'),
                        ],
                        [
                            'label' => Yii::t('common', 'Список ролей'),
                            'visible' => Yii::$app->user->can('access.role.index'),
                            'url' => ['/access/role'],
                            'active' => $moduleId == 'access' && ($controllerId == 'role'),
                        ],
                        [
                            'label' => Yii::t('common', 'Список операций'),
                            'visible' => Yii::$app->user->can('access.permission.index'),
                            'url' => ['/access/permission'],
                            'active' => $moduleId == 'access' && ($controllerId == 'permission'),
                        ],
                    ],
                ],

                [
                    'label' => Yii::t('common', 'Администрирование'),
                    'visible' => Yii::$app->user->can('administration.system.index')
                        || Yii::$app->user->can('administration.runtime.index')
                        || Yii::$app->user->can('administration.monitoring.index'),
                    'icon' => PeIcon::TOOLS,
                    'url' => '#',
                    'active' => $moduleId == 'administration',
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Система'),
                            'visible' => Yii::$app->user->can('administration.system.index'),
                            'url' => ['/administration/system'],
                            'active' => $moduleId == 'administration' && ($controllerId == 'system'),
                        ],
                        [
                            'label' => Yii::t('common', 'Мониторинг'),
                            'visible' => Yii::$app->user->can('administration.monitoring.index'),
                            'url' => ['/administration/monitoring'],
                            'active' => $moduleId == 'administration' && ($controllerId == 'monitoring'),
                        ],
                        [
                            'label' => Yii::t('common', 'Логирование'),
                            'visible' => Yii::$app->user->can('administration.runtime.index'),
                            'url' => ['/administration/runtime'],
                            'active' => $moduleId == 'administration' && ($controllerId == 'runtime'),
                        ],
                        [
                            'label' => Yii::t('common', 'Логгер'),
                            'visible' => Yii::$app->user->can('administration.baselogger.index'),
                            'url' => ['/administration/base-logger'],
                            'active' => $moduleId == 'administration' && ($controllerId == 'base-logger'),
                        ],
                        [
                            'label' => Yii::t('common', 'Статистика'),
                            'visible' => Yii::$app->user->can('administration.stats.index'),
                            'url' => ['/administration/stats'],
                            'active' => $moduleId == 'administration' && ($controllerId == 'stats'),
                        ],
                        [
                            'label' => Yii::t('common', 'API логи'),
                            'visible' => Yii::$app->user->can('administration.apilog.index'),
                            'url' => ['/administration/api-log'],
                            'active' => $moduleId == 'administration' && ($controllerId == 'api-log'),
                        ],
                        [
                            'label' => Yii::t('common', 'JS логгер'),
                            'visible' => Yii::$app->user->can('administration.jslogger.index'),
                            'url' => ['/administration/js-logger'],
                            'active' => $moduleId == 'administration' && ($controllerId == 'js-logger'),
                        ],
                        [
                            'label' => Yii::t('common', 'Логи автодозвона'),
                            'visible' => Yii::$app->user->can('administration.apilog.index'),
                            'url' => ['/administration/auto-call-log'],
                            'active' => $moduleId == 'administration' && ($controllerId == 'auto-call-log'),
                        ],
                        [
                            'label' => Yii::t('common', 'Автодозвон стран'),
                            'visible' => Yii::$app->user->can('administration.autocallcountry.index'),
                            'url' => ['/administration/auto-call-country'],
                            'active' => $moduleId == 'administration' && ($controllerId == 'auto-call-country'),
                        ],
                        [
                            'label' => Yii::t('common', 'Логи очередей по операторам'),
                            'visible' => Yii::$app->user->can('administration.queueuserlog.index'),
                            'url' => ['/administration/queue-user-log'],
                            'active' => $moduleId == 'administration' && ($controllerId == 'queue-user-log'),
                        ],
                    ],
                ],

                [
                    'label' => Yii::t('common', 'Wiki'),
                    'visible' => Yii::$app->user->can('wiki.default.index'),
                    'icon' => PeIcon::WIKI,
                    'url' => '#',
                    'active' => $moduleId == 'wiki',
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Статьи'),
                            'visible' => Yii::$app->user->can('wiki.article.index'),
                            'url' => ['/wiki/article'],
                            'active' => $moduleId == 'wiki' && ($controllerId == 'article'),
                        ],
                        [
                            'label' => Yii::t('common', 'Категории'),
                            'visible' => Yii::$app->user->can('wiki.category.index'),
                            'url' => ['/wiki/category'],
                            'active' => $moduleId == 'wiki' && ($controllerId == 'category'),
                        ],
                    ],
                ],
            ]
        ]) ?>
    </div>
</aside>

<?php
use backend\widgets\navbar\Clock;
use backend\widgets\navbar\TimezoneSwitcher;
use backend\widgets\navbar\LanguageSwitcher;
use frontend\assets\UserReadyAsset;

UserReadyAsset::register($this);
?>

<div id="header">
    <div class="color-line"></div>
    <div id="logo" class="light-version">
        <span><?= Yii::$app->params['companyName'];?></span>
    </div>

    <nav role="navigation">
        <div class="header-link hide-menu">
            <i class="fa fa-bars"></i>
        </div>
    </nav>

    <div class="text-center align-middle information"><?=yii::t('common', 'Emergency phone number for Urgent support +7 951 366 7788');?></div>


    <div class="navbar-right">
        <ul class="nav navbar-nav no-borders">
            <li>
        <div class="jira-btn btn btn-primary"  id="sendErrorToJira">
            <div class="square"><div class="sphere"></div></div><span><?=yii::t("common", "Нашли ошибку?");?></span>
        </div>
            </li>
        </ul>
        <ul class="nav navbar-nav no-borders">
            <?= TimezoneSwitcher::widget() ?>
            <?= LanguageSwitcher::widget() ?>
            <?= Clock::widget() ?>
        </ul>
    </div>
</div>

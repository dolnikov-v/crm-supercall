<?php

?>
<div class="splash">
    <div class="color-line"></div>
    <div class="splash-title">
        <h1><?= Yii::$app->params['companyName'];?></h1>
        <p><?= Yii::t('common', 'Пожалуйста, подождите, идет загрузка страницы...') ?></p>
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>

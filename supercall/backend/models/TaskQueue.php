<?php

namespace backend\models;

use backend\models\query\TaskQueueQuery;
use common\models\User;
use function GuzzleHttp\Promise\task;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task_queue".
 *
 * @property integer $id
 * @property string $type
 * @property integer $user_id
 * @property string $path
 * @property string $status
 * @property string $query
 * @property string $error
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 * @property TaskQueueOrder[] $taskQueueOrders
 */
class TaskQueue extends ActiveRecord
{
    const LOGGER_TYPE = 'TaskQueue';
    const SYSTEM_USER_ID = 1;

    const STATUS_READY = 'ready';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_COMPLETED = 'completed';
    const STATUS_FAIL = 'failed';

    const TYPE_CALL_RECORD_IMPORT = 'call_records';
    const TYPE_STATS_OPERATOR_IMPORT = 'stats_operator';
    const TYPE_STATS_OPERATOR_ONLINE_IMPORT = 'stats_online_operator';
    const TYPE_STATS_ORDER_IMPORT = 'stats_order';

    const BY_ID = 'by_id';
    const BY_QUERY = 'by_query';

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%task_queue}}';
    }

    /**
     * @return array
     */
    public static function getTypeCollection()
    {
        return [
            self::BY_ID => yii::t('common', 'По ID'),
            self::BY_QUERY => yii::t('common', 'По запросу')
        ];
    }

    /**
     * @return array
     */
    public static function getStatusesCollection()
    {
        return [
            self::STATUS_READY => yii::t('common', 'Ожидает обработку'),
            self::STATUS_IN_PROGRESS => yii::t('common', 'В обработке'),
            self::STATUS_COMPLETED => yii::t('common', 'Выполнен'),
            self::STATUS_FAIL => yii::t('common', 'Ошибка'),
        ];
    }

    /**
     * @param $status
     * @return mixed|null
     */
    public static function getStatus($status)
    {
        $statusesCollection = self::getStatusesCollection();

        return isset($statusesCollection[$status]) ? $statusesCollection[$status] : null;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'type', 'path', 'status', 'error', 'query'], 'string'],
            [['status'], 'in', 'range' => array_keys(self::getStatusesCollection())],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Наименование'),
            'type' => Yii::t('common', 'Тип'),
            'user_id' => Yii::t('common', 'Пользователь'),
            'path' => Yii::t('common', 'Ссылка'),
            'status' => Yii::t('common', 'Статус'),
            'query' => Yii::t('common', 'Параметры экспорта'),
            'error' => Yii::t('common', 'Ошибка'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Обновлено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskQueueOrders()
    {
        return $this->hasMany(TaskQueueOrder::className(), ['task_queue_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function generateName($countOrders)
    {
        $this->name = "task_#{$this->id}_{$this->user_id}_{$countOrders}_{$this->type}_" . $this->created_at;

        return $this->save(false, ['name']);
    }

    public function generateNameForStatsOperators()
    {
        $this->name = "task_#{$this->id}_{$this->user_id}_{$this->type}_" . $this->created_at;

        return $this->save(false, ['name']);
    }

    public function generateNameForStatsOnlineOperators()
    {
        $this->name = "task_#{$this->id}_{$this->user_id}_{$this->type}_" . $this->created_at;

        return $this->save(false, ['name']);
    }

    /**
     * @inheritdoc
     * @return TaskQueueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskQueueQuery(get_called_class());
    }

    /**
     * @param $id
     * @param $status
     * @return array
     */
    public static function changeStatus($id, $status, $error = null)
    {
        $taskQueue = TaskQueue::findOne(['id' => $id]);
        $taskQueue->status = $status;
        $taskQueue->error = $error;

        if (!$taskQueue->save(false, ['status', 'error'])) {
            return [
                'success' => false,
                'message' => implode(', ', $taskQueue->getFirstErrors())
            ];
        } else {
            return [
                'success' => true,
                'message' => ''
            ];
        }
    }
}
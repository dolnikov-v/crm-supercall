<?php

namespace backend\models;

use common\modules\order\models\Order;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "task_queue_order".
 *
 * @property integer $id
 * @property integer $task_queue_id
 * @property integer $order_id
 * @property string $uniqueid
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order $order
 * @property TaskQueue $taskQueue
 */
class TaskQueueOrder extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%task_queue_order}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['task_queue_id', 'order_id', 'created_at', 'updated_at'], 'integer'],
            [['order_id', 'uniqueid'], 'required'],
            [['uniqueid'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['task_queue_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskQueue::className(), 'targetAttribute' => ['task_queue_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskQueue()
    {
        return $this->hasOne(TaskQueue::className(), ['id' => 'task_queue_id']);
    }
}
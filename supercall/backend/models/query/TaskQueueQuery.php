<?php

namespace backend\models\query;

use backend\models\TaskQueue;

/**
 * This is the ActiveQuery class for [[\backend\models\TaskQueue]].
 *
 * @see \backend\models\TaskQueue
 */
class TaskQueueQuery extends \yii\db\ActiveQuery
{
    public function orderByDesc()
    {
        return $this->orderBy(['id' => SORT_DESC]);
    }
    /**
     * @return $this
     */
    public function completed()
    {
        return $this->andWhere(['status' => TaskQueue::STATUS_COMPLETED]);
    }

    /**
     * @return $this
     */
    public function fail()
    {
        return $this->andWhere(['status' => TaskQueue::STATUS_FAIL]);
    }

    /**
     * @param $user_id
     * @return $this
     */
    public function byUserId($user_id)
    {
        return $this->andWhere(['user_id' => $user_id]);
    }

    /**
     * @inheritdoc
     * @return \backend\models\TaskQueue[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\TaskQueue|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
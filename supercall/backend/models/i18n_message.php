<?php
namespace backend\models;

use yii\db\ActiveRecord;

class i18n_message extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%i18n_message}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'language', 'translation', 'created_at'], 'required'],
            [['language', 'translation'], 'string'],
            [['id', 'created_at', 'updated_at'], 'integer']
        ];
    }
}
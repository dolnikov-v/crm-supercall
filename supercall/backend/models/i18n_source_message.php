<?php
namespace backend\models;

use common\components\db\ActiveRecord;
use yii\console\Exception;
use yii\db\Query;
use yii;

/**
 * Class i18n_source_message
 * @package backend\models
 */
class i18n_source_message extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%i18n_source_message}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['category', 'message', 'created_at'], 'required'],
            [['category', 'message'], 'string'],
            [['created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @param $phrases
     * @param bool $console
     * @param string $category
     * @param string $language
     * @param bool $replace
     * @return array
     */
    public static function addTranslate($phrases, $console = false, $category = 'common', $language = 'en-US', $replace = true)
    {
        self::removeTranslate($phrases);

        try {

            if (!is_array($phrases)) {
                throw new Exception('The input data is not an array of the form: phrase => translation');
            } else {
                foreach ($phrases as $text => $translate) {

                    //есть ли сама фраза в справочнике?
                    $exists = (new Query())
                        ->select('id')
                        ->from(self::tableName())
                        ->where(['category' => $category])
                        ->andWhere(['message' => $text])
                        ->one();
                    //фразы нет, добавляем
                    if (!$exists) {
                        $model = new i18n_source_message();
                        $model->category = $category;
                        $model->message = $text;
                        $model->created_at = time();
                        $model->updated_at = time();
                        if ($model->save()) {
                            if($console){
                                echo 'added: '.$text.PHP_EOL;
                            }
                            //фраза добавлена
                            //добавляем перевод
                            $phrase_id = Yii::$app->db->lastInsertID ?? $model->id;

                            //если перевод уже есть ?
                            $translatePhrase = i18n_message::find()->where([
                                'id' => $phrase_id,
                                'translation' => $translate,
                                'language' => $language
                            ]);

                            //перевод найден, по переписовать не хотим
                            if($translatePhrase->exists() && !$replace){
                                if($console){
                                    echo 'translate exists: '.$text.PHP_EOL;
                                }
                                continue;
                            }
                            //перевод не найден или найден, но мы хотим переписать

                            if($replace) {

                                $findTranslate = i18n_message::find()->where(['id' => $phrase_id, 'language' => $language]);

                                if($findTranslate->exists()){
                                    if(!$findTranslate->one()->delete()){
                                        echo 'not replacing: '.$translate.PHP_EOL;
                                        continue;
                                    }
                                }

                                $modelMessage = new i18n_message();
                                $modelMessage->id = $phrase_id;
                                $modelMessage->language = $language;
                                $modelMessage->translation = $translate;
                                $modelMessage->created_at = time();
                                $modelMessage->updated_at = time();

                                if (!$modelMessage->save()) {

                                    echo 'not added translate: ' . $translate . PHP_EOL;
                                    throw new Exception($model->getFirstErrorAsString());
                                } else {
                                    if ($console) {
                                        echo 'added translate: ' . $translate . PHP_EOL;
                                    }
                                }
                            }
                        } else {
                            throw new Exception($model->getFirstErrorAsString());
                        }
                    }else{
                        $modelTranslate = i18n_message::find()->where(['translation' => $translate])->andWhere(['language' => $language]);

                        if($modelTranslate->exists()){
                            continue;
                        }

                        $modelMessage = new i18n_message();
                        $modelMessage->id = $exists['id'];
                        $modelMessage->language = $language;
                        $modelMessage->translation = $translate;
                        $modelMessage->created_at = time();
                        $modelMessage->updated_at = time();

                        if (!$modelMessage->save()) {
                            echo 'not added: '.$translate.PHP_EOL;
                            throw new Exception($modelMessage->getFirstErrorAsString());
                        }else{
                            if($console){
                                echo 'added: '.$translate.PHP_EOL;
                            }
                        }
                    }

                }
            }
        } catch (\Exception $e) {
            if ($console) {
                echo $e->getMessage();
            } else {
                return ['success' => false, 'message' => $e->getMessage()];
            }
        }

        return ['success' => true];
    }

    /**
     * @param $phrases
     * @param bool $console
     * @param string $category
     * @return array
     */
    public static function removeTranslate($phrases, $console = true, $category = 'common', $language='en-US')
    {
        try {
            if (!is_array($phrases)) {
                throw new Exception('The input data is not an array of the form: phrase => translation');
            } else {
                foreach ($phrases as $text => $translate) {
                    //нужно найти фразу для удаления
                    $model = i18n_source_message::find()
                        ->where([
                            'category' => $category,
                            'message' => $text
                        ])
                        ->one();

                    if ($model) {
                        //фраза найдена
                        if (!$model->delete()) {
                            if($console) {
                                echo 'not deleted: ' . $model->message . PHP_EOL;
                            }
                            throw new Exception(implode(PHP_EOL, $model->getErrors()));
                        }else{
                            echo 'deleted: '.$model->message.PHP_EOL;

                        }
                    }
                }
            }
        } catch (Exception $e) {
            if($console) {
                echo $e->getMessage();
                exit;
            }else{
                return ['success' => false, 'message' => $e->getMessage()];
            }
        }

        return ['success' => true];
    }
}
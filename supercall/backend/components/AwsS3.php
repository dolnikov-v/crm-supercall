<?php

namespace backend\components;

use Aws\Credentials\Credentials;
use Aws\S3\Exception\S3Exception;
use Yii;
use yii\base\Component;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;

/**
 * Class AwsS3
 * https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/s3-examples-creating-buckets.html
 * @package backend\components
 */
class AwsS3 extends Component
{
    const TRANSCRIBE_BUCKET = 'transcribe.files';

    /** @var  string */
    public $region;
    /** @var  string */
    public $version;
    /** @var  string */
    public $profile;
    /** @var  string */
    public $key;
    /** @var  string */
    public $secret;
    /** @var  array */
    public $credentials;

    /**
     * AwsS3 constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->key = $config['credentials']['key'];
        $this->secret = $config['credentials']['secret'];
        parent::__construct($config);
    }

    /**
     * https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_configuration.html#credentials
     * @return S3Client
     */
    public function getS3Client()
    {
        return new S3Client([
            'region' => $this->region,
            'credentials' => new Credentials($this->key, $this->secret),
            'version' => $this->version
        ]);
    }

    /**
     * https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/s3-examples-creating-buckets.html
     * @return \Aws\Result
     */
    public function getBuckets()
    {
        return $this->getS3Client()->listBuckets();
    }

    /**
     * https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/s3-examples-creating-buckets.html
     * @param $name
     * @return bool|\Aws\Result
     */
    public function createBucket($name)
    {
        try {
            return $this->getS3Client()->createBucket([
                'Bucket' => $name
            ]);
        } catch (AwsException $e) {
            echo $e->getMessage() . "\n";
        }

        return false;
    }

    /**
     * https://github.com/awsdocs/aws-doc-sdk-examples/blob/master/php/example_code/s3/DeleteBucket.php
     * @param $name
     * @return mixed
     */
    public function deleteBucket($name)
    {

        try {
            $objects = $this->getS3Client()->getIterator('ListObjects', ([
                'Bucket' => $name
            ]));

            foreach ($objects as $object) {
                $this->deleteObjectFromBucket($name, $object['Key']);
            }

            return $this->getS3Client()->deleteBucket([
                'Bucket' => $name
            ]);
        } catch (S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    /**
     * https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/s3-examples-creating-buckets.html
     * @param $bucket
     * @param $filePath
     * @return bool|\Aws\Result
     * $result['@metadata']['statusCode'] == 200 is ok
     * $result['@metadata']['effectiveUri'] - s3 url file
     */
    public function putObjectInBucket($bucket, $filePath)
    {
        try {
            return $this->getS3Client()->putObject([
                'Bucket' => $bucket,
                'Key' => basename($filePath),
                'SourceFile' => $filePath
            ]);
        } catch (S3Exception $e) {
            echo $e->getMessage() . "\n";
        }

        return false;
    }

    /**
     * https://github.com/awsdocs/aws-doc-sdk-examples/blob/master/php/example_code/s3/DeleteObject.php
     * @param string $bucket
     * @param string $object
     * @return \Aws\Result
     */
    public function deleteObjectFromBucket($bucket, $object)
    {
        try {
            return $this->getS3Client()->deleteObject([
                'Bucket' => $bucket,
                'Key' => $object,
            ]);
        } catch (S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    /**
     * https://docs.aws.amazon.com/AmazonS3/latest/dev/RetrieveObjSingleOpPHP.html
     * @return bool
     */
    public function checkTranscribeBucket()
    {
        foreach ($this->getBuckets()['Buckets'] as $num => $bucket) {
            if ($bucket['Name'] == self::TRANSCRIBE_BUCKET) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $bucket
     * @param $keyname
     * @return \Aws\Result|bool
     */
    public function getObject($bucket, $keyname)
    {
        $result = false;

        try {
            $result = $this->getS3Client()->getObject([
                'Bucket' => $bucket,
                'Key'    => $keyname
            ]);

            $result = $result['Body'];
            // Display the object in the browser.
            //header("Content-Type: {$result['ContentType']}");
            //echo $result['Body'];
        } catch (S3Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }

        return $result;
    }

}
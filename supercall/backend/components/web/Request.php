<?php
namespace backend\components\web;

use Yii;
use yii\base\InvalidConfigException;

/**
 * Class Request
 * @package backend\components\web
 */
class Request extends \yii\web\Request
{
    /**
     * @var string
     */
    private $countrySlug;

    /**
     * @var array
     */
    private $ignoredModules = [
        'auth',
    ];

    /**
     * @return string
     * @throws InvalidConfigException
     */
    protected function resolvePathInfo()
    {
        $this->setCountrySlug();

        $url = $this->getUrl();

        if ($this->countrySlug) {
            $url = mb_substr($url, mb_strlen($this->countrySlug) + 1);
        }

        $this->setUrl($url);

        return parent::resolvePathInfo();
    }

    /**
     * @return string
     */
    private function setCountrySlug()
    {
        $url = $this->getUrl();

        $partialsUrl = explode('/', $url);
        $slug = isset($partialsUrl[1]) ? $partialsUrl[1] : null;

        if (Yii::$app->user->isGuest && !in_array($slug, $this->ignoredModules)) {
            Yii::$app->user->returnUrl = Yii::$app->request->url;

            Yii::$app->response->redirect('/auth/login');
            Yii::$app->end();
        }

        Yii::$app->user->setCountry($slug);

        if (Yii::$app->user->getCountry() && $slug == Yii::$app->user->getCountry()->slug) {
            $this->countrySlug = $slug;
        }
    }
}

<?php
namespace backend\components\web;

use Yii;

/**
 * Class UrlManager
 * @package backend\components\web
 */
class UrlManager extends \yii\web\UrlManager
{
    /**
     * @var array
     */
    private $patternsIgnoredUrl = [
        '#^home/(index)#',
        '#^auth/(login|logout)#',
        '#^catalog/(country|timezone|currency|product)#',
        '#^partner/(control|product|shipping)#',
        '#^i18n/#',
        '#^access/#',
    ];

    /**
     * @param array|string $params
     * @return string
     */
    public function createUrl($params)
    {
        $url = parent::createUrl($params);
        $route = trim($params[0], '/');

        if (!$this->isIgnoredUrl($route)) {
            $country = Yii::$app->user->getCountry();

            if ($country) {
                return '/' . $country->slug . $url;
            }
        }

        return $url;
    }

    /**
     * @param string $url
     * @return boolean
     */
    public function isIgnoredUrl($url)
    {
        foreach ($this->patternsIgnoredUrl as $pattern) {
            if (preg_match($pattern, $url)) {
                return true;
            }
        }

        return false;
    }
}

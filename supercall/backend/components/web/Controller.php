<?php
namespace backend\components\web;

use common\components\web\Controller as CommonController;
use Yii;

/**
 * Class Controller
 * @package backend\components\web
 */
class Controller extends CommonController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!\Yii::$app->user->isGuest){
            $language = Yii::$app->user->getLanguage();
            Yii::$app->language = $language->locale;
            Yii::$app->formatter->setLocale($language->locale);
        } else {
            Yii::$app->user->language->id = 2;
            Yii::$app->language = 'en-US';
        }
    }
    
}

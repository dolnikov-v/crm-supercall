<?php

namespace backend\components;

use common\models\Country;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use Yii;
use yii\base\Component;

class GoogleSpeech extends Component
{
    const LOGGER_TYPE = 'google speech';
    const ACTION_TRANSCRIBE = 'get transcribe';
    const ACTION_ASTERISK_CROPED = 'croped Asterrisk';

    /** @var  string google key */
    public $key;
    /** @var  string goggole speech url */
    public $api;
    /** @var string */
    public $deleteCropFromAsteriskUrl;
    /** @var string */
    public $getCropedByAsteriskUrl;
    /** @var string transcribe audio file */
    public $file;
    /** @var array */
    public $croped_files = [];
    /** @var  Order */
    public $order;
    /** @var  string for transcribe */
    public $locale;
    /** @var  integer */
    public $call_history_id;
    /** @var  array */
    public $transcribe = [];
    /** @var  string */
    public $path;
    /** @var  string */
    public $path_asterisk;

    /**
     * Сеттер, если работаем из под консоли - ОБЯЗАТЕЛЬНО передать заказ
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        if (!$this->order) {
            $this->order = $order;
        }
    }

    /**
     * @param $content
     * @return bool|string
     */
    public function generateConfig($content)
    {
        if (!$this->order->country->language->locale) {
            $logger = yii::$app->get('logger');
            $logger->action = __METHOD__;
            $logger->route = __CLASS__;
            $logger->tags = [
                'country' => $this->order->country->char_code,
                'file' => $this->file,
                'order_id' => $this->order->id,
                'url' => $this->getCropedByAsteriskUrl,
            ];
            $logger->error = json_encode('Error: empty local on language the country settings', JSON_UNESCAPED_UNICODE);
            $logger->save();

            return false;
        }
        $this->locale = $this->order->country->language->locale; //'zh_TW';

        return json_encode(
            [
                'config' =>
                    [
                        'encoding' => 'AMR',
                        'sampleRateHertz' => 8000,
                        'languageCode' => 'id-ID',//$this->locale,
                        'enableWordTimeOffsets' => false,
                    ],
                'audio' => [
                    //'uri' => 'gs://2wtrade/56979288741-2wcall245-02_27 (3).wav'
                    'content' => $content
                ]
            ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Получить от астериска кропнутые файлы записи
     */
    public function getCropedFiles()
    {

        if (!preg_match("#" . $this->path . "#", $this->file)) {
            $this->file = $this->path_asterisk . $this->path . $this->file;
        } else {
            $this->file = $this->path_asterisk . $this->file;
        }

        $options = [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => 'file=' . $this->file
        ];

        $curl_response = $this->sendRequest($this->getCropedByAsteriskUrl, $options, null);
        $result = json_decode($curl_response['result'], 1);

        //неожиданный формат ответа от сервера астериска в момент получения данных о нарезанных файлах
        if (!isset($result['success'])) {
            $logger = yii::$app->get('logger');
            $logger->action = __METHOD__;
            $logger->route = __CLASS__;
            $logger->tags = [
                'file' => $this->file,
                'country' => $this->order->country->char_code,
                'order_id' => $this->order->id,
                'url' => $this->getCropedByAsteriskUrl,
                'options' => json_encode($options, JSON_UNESCAPED_UNICODE)
            ];
            $logger->error = json_encode($curl_response, JSON_UNESCAPED_UNICODE);
            $logger->save();
        } else {
            //астериск вернул список файлов
            if ($result['success']) {
                $files = $result['files'];
                //коллекция порезанных файлов записи
                foreach ($files as $file) {
                    if (!empty($file)) {
                        $this->croped_files[] = $file;
                    }
                }
            } else {
                //астериск не вернул порезанные файлы
                $logger = yii::$app->get('logger');
                $logger->action = self::ACTION_ASTERISK_CROPED;
                $logger->route = __CLASS__;
                $logger->tags = [
                    'fileRecord' => $this->file,
                    'country' => $this->order->country->char_code,
                    'url' => $this->getCropedByAsteriskUrl,
                    'order_id' => $this->order->id
                ];
                $logger->error = $curl_response['message'];
                $logger->save();
            }
        }
    }

    /**
     * Получить транскрибацию по аудиозаписи
     */
    public function getTranscribe()
    {
        $url = $this->api . $this->key;

        if (!empty($this->croped_files)) {

            /** TEST MULTI CURL */
            $options = [];
            $urls = [];
            foreach ($this->croped_files as $k => $file) {
                if (!empty($file)) {
                    $urls[$k] = $url;
                    $content = base64_encode(file_get_contents($this->path_asterisk . $file));
                    $jsondata = $this->generateConfig($content);

                    if ($jsondata[$k]) {
                        $options[$k] = [
                            CURLOPT_URL => $url,
                            CURLOPT_HEADER => 0,
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_FOLLOWLOCATION => 1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_HTTPHEADER => [
                                'Content-Type: application/json',
                                'Content-Length: ' . strlen($jsondata)
                            ],
                            CURLOPT_POSTFIELDS => $jsondata
                        ];
                    }
                }
            }

            $curl_responses = $this->multiCurlRequest($urls, $options);

            echo '<pre>' . print_r($curl_responses, 1) . '</pre>';

            foreach ($curl_responses as $curl_response) {
                if ($curl_response['error_code'] != 0) {
                    $result = [
                        'success' => false,
                        'message' => $curl_response['error_text']
                    ];
                } else {
                    //if ok => $response['results'][0]['alternatives'][0]['transcript']
                    $response = (object)$curl_response['result'];

                    if (!isset($response->scalar)) {
                        $error = json_encode($response, JSON_UNESCAPED_UNICODE);
                    } else {
                        $response = json_decode($response->scalar, 1);

                        if (isset($response['error'])) {
                            $error = $response['error']['message'];
                        }
                    }

                    if (isset($error)) {
                        $result = [
                            'success' => false,
                            'message' => $error
                        ];
                    } else {
                        if (isset($response['results'])) {
                            $message = '';

                            foreach ($response['results'] as $result) {
                                $message .= ' ' . $result['alternatives'][0]['transcript'] . PHP_EOL;
                            }

                            $result = [
                                'success' => true,
                                'message' => $message
                            ];
                        }
                    }
                }
                if (isset($result)) {
                    if ($result['success']) {
                        $this->transcribe[$this->call_history_id][] = $result['message'];
                    }
                }
            }

            /** END TEST */

            /**
             *  for singl request
             *
             * foreach ($this->croped_files as $file) {
             * continue;
             * if (!empty($file)) {
             *
             * $content = base64_encode(file_get_contents($this->path_asterisk . $file));
             * $jsondata = $this->generateConfig($content);
             *
             * if ($jsondata) {
             * $options = [
             * CURLOPT_RETURNTRANSFER => 1,
             * CURLOPT_FOLLOWLOCATION => 1,
             * ];
             *
             * $headers = [
             * 'Content-Type: application/json',
             * 'Content-Length: ' . strlen($jsondata)
             * ];
             *
             * $curl_response = $this->sendRequest($url, $options, $headers, $jsondata);
             *
             * if ($curl_response['error_code'] != 0) {
             * $result = [
             * 'success' => false,
             * 'message' => $curl_response['error_text']
             * ];
             * } else {
             * //if ok => $response['results'][0]['alternatives'][0]['transcript']
             * $response = (object)$curl_response['result'];
             *
             * if (!isset($response->scalar)) {
             * $error = json_encode($response, JSON_UNESCAPED_UNICODE);
             * } else {
             * $response = json_decode($response->scalar, 1);
             *
             * if (isset($response['error'])) {
             * $error = $response['error']['message'];
             * }
             * }
             *
             * if (isset($error)) {
             * $result = [
             * 'success' => false,
             * 'message' => $error
             * ];
             * } else {
             * if (isset($response['results'])) {
             * $message = '';
             *
             * foreach ($response['results'] as $result) {
             * $message .= ' ' . $result['alternatives'][0]['transcript'] . PHP_EOL;
             * }
             *
             * $result = [
             * 'success' => true,
             * 'message' => $message
             * ];
             * }
             * }
             * }
             * if (isset($result)) {
             * if ($result['success']) {
             * $this->transcribe[$this->call_history_id][] = $result['message'];
             * }
             * }
             * }
             * }
             * }*/
        } else {
            $logger = yii::$app->get('logger');
            $logger->action = self::ACTION_ASTERISK_CROPED;
            $logger->route = __CLASS__;
            $logger->tags = [
                'call_history_id' => $this->call_history_id,
                'country' => $this->order->country->char_code,
                'order_id' => $this->order->id,
                'fileRecord' => $this->file,
                'url' => $url
            ];
            $logger->error = json_encode('empty croped files', JSON_UNESCAPED_UNICODE);
            $logger->save();
        }
    }


    public function saveTranscribe()
    {
        if (isset($this->transcribe[$this->call_history_id])) {
            $callHistoryModel = CallHistory::find()
                ->where(['id' => $this->call_history_id]);

            if ($callHistoryModel->exists()) {
                //пишем
                $model = $callHistoryModel->one();
                $model->transcribe = implode(PHP_EOL, $this->transcribe[$this->call_history_id]);

                if (!$model->save()) {
                    $logger = yii::$app->get('logger');
                    $logger->action = 'get transcribe';
                    $logger->route = __CLASS__;
                    $logger->tags = [
                        'call_history_id' => $model->id,
                        'country' => $this->order->country->char_code,
                        'order_id' => $this->order->id,
                        'url' => $this->url . $this->key
                    ];
                    $logger->error = json_encode($model, JSON_UNESCAPED_UNICODE);
                    $logger->save();
                }
            }
        }
    }

    /**
     * Удаление порезанных файлов с сервера астериска
     */
    public function deleteCropedFromAsterisk()
    {
        $options = [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => 'delete=' . $this->file
        ];

        $this->sendRequest($this->deleteCropFromAsteriskUrl, $options, []);
    }

    /**
     * @param $url
     * @param $options
     * @param $headers
     * @param null $jsondata
     * @return array
     */
    public function sendRequest($url, $options, $headers, $jsondata = null)
    {
        $c = curl_init($url);
        curl_setopt_array($c, $options);
        if (!is_null($headers)) {
            curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($c, CURLOPT_CUSTOMREQUEST, "POST");
        }
        if (!is_null($jsondata)) {
            curl_setopt($c, CURLOPT_POSTFIELDS, $jsondata);
        }

        $error_code = curl_errno($c);
        $error_text = curl_error($c);
        $result = curl_exec($c);
        curl_close($c);

        return [
            'error_code' => $error_code,
            'error_text' => $error_text,
            'result' => $result
        ];
    }

    /**
     * @param $urls
     * @param $options
     * @return array
     */
    public function multiCurlRequest($urls, $options)
    {
        ini_set("display_errors", 1);
        error_reporting(-1);
        $results = [];
        $channels = [];
        $multi = curl_multi_init();

        foreach ($urls as $k => $url) {
            $ch = curl_init();
            curl_setopt_array($ch, $options[$k]);
            curl_multi_add_handle($multi, $ch);
            $error_code[$k] = curl_errno($ch);
            $error_text[$k] = curl_error($ch);
            $channels[$url] = $ch;
        }

        $active = null;

        do {
            $mrc = curl_multi_exec($multi, $active);
        } while ($active > 0);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($multi) == -1) {
                continue;
            }

            do {
                $mrc = curl_multi_exec($multi, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }

        foreach ($channels as $k => $channel) {
            $result = [
                'error_code' => isset($error_code) && isset($error_code[$k]) ? $error_code[$k] : null,
                'error_text' => isset($error_text) && isset($error_text[$k]) ? $error_text[$k] : null,
                'result' => curl_multi_getcontent($channel)
            ];
            $results[] = $result;
            curl_multi_remove_handle($multi, $channel);
        }

        curl_multi_close($multi);

        return $results;
    }


    /**
     * for Google Store https://cloud.google.com/speech/docs/async-recognize
     * @return bool|mixed
     */
    public function getRecordTranscribe()
    {
        if(!$this->call_history_id){
            return false;
        }
        $callHistory = CallHistory::find()->where(['id' => $this->call_history_id])->one();

        if ($callHistory) {

            if ($callHistory->transcribe) {
                return $callHistory->transcribe;
            } else {
                $this->getCropedFiles();
                $this->getTranscribe();
                $this->saveTranscribe();
                $this->deleteCropedFromAsterisk();

                return $callHistory->transcribe;
            }
        }

        return false;
    }
}
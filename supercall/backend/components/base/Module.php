<?php
namespace backend\components\base;

use common\components\base\Module as CommonModule;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * Class Module
 * @package backend\components\base
 */
class Module extends CommonModule
{
    /**
     * @var array
     */
    public $actions = [
        'get-data',
        'get-scripts',
        'user-ready',
        'insert-data-on-start-call',
        'save-order-data-backup',
        'update-data-on-call',
        'get-zing-tree',
        'get-list-users',
        'manage-user-of-team'
    ];

    /**
     * @var array
     */
    private $patternsAllowedPermissions = [
        '/^auth.(login|logout)/',
        '#^catalog\.kladercountries\.getdata#',
        '#^catalog\.scriptsoperators\.getscripts#',
        '#^order\.change\.saveorderdatabackup#',
        '#^call\.callhistory\.userready#',
        '#^call\.callhistory\.insertdataonstartcall#',
        '#^call\.callhistory\.updatedataoncall#',
        '#^operator.zingtree.getzingtree#',
        '#^access.team.getlistusers#',
        '#^access.team.manageuserofteam#'
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $controllerId = yii::$app->controller->id;
        $actionId = $action->id;

        if (!yii::$app->user->identity && !$this->isAllowedPermission($this->getPermissionName())) {
            if ($controllerId != 'login' && $actionId != 'index') {
                yii::$app->controller->redirect(Url::to(['/auth/login']));
            }
        }
        return parent::beforeAction($action);
    }

    /**
     * @param $permissionName
     * @return array
     */
    protected function isAllowedPermission($permissionName)
    {
        foreach ($this->patternsAllowedPermissions as $pattern) {
            $allow = (preg_match($pattern, $permissionName));
        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => $allow,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => $allow,
                        'actions' =>$this->actions,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];

    }

    /**
     * @return string
     */
    protected function getPermissionName()
    {
        $module = str_replace('-', '', strtolower(Yii::$app->controller->module->id));
        $controller = str_replace('-', '', strtolower(Yii::$app->controller->id));
        $action = str_replace('-', '', strtolower(Yii::$app->controller->action->id));

        $operation = str_replace('/', '.', $controller) . '.' . $action;

        if ($module && !in_array($module, ['backend'])) {
            $operation = $module . '.' . $operation;
        }

        return $operation;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $permissionName = $this->getPermissionName();
        $allow = $this->isAllowedPermission($permissionName) || Yii::$app->user->can($permissionName);

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => $allow,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => $allow,
                        'actions' => $this->actions,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }
}

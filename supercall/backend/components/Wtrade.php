<?php

namespace backend\components;

use backend\modules\access\models\AuthItem;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class Wtrade
 * @package backend\components
 */
class Wtrade extends Component
{
    const LOGGER_TYPE = 'webhook roles';
    const STATUS_FAIL = 'fail';
    const SOURCE = '2wcall';

    /** @var  string */
    public $webHookPayUrl;
    /** @var  string */
    public $DevWebHookPayUrl;

    /**
     * @return array
     */
    public function getData()
    {
        $data = [
            'source' => self::SOURCE,
            'roles' => []
        ];

        $roles = AuthItem::find()->where(['type' => 1])->all();

        $data['roles'] = ArrayHelper::map($roles, 'name', 'description');

        return $data;
    }

    public function sentData()
    {
        $data = $this->getData();

        $data_string = json_encode($data, JSON_UNESCAPED_UNICODE);

        $url = yii::$app->params['is_dev'] ? $this->DevWebHookPayUrl : $this->webHookPayUrl;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        $request = json_decode($result, 1);

        if(isset($request['status'])){
            $logger = yii::$app->get('logger');
            $logger->type = self::LOGGER_TYPE;
            $logger->route = '/backen/components/Wtrade';
            $logger->action = __METHOD__;

            if($request['status'] == self::STATUS_FAIL){
                $logger->tags = [
                    'action' => 'npay webhook'
                ];
                $logger->error = json_encode(['request' => $data, 'response' => $request], JSON_UNESCAPED_UNICODE);

            }else{
                $logger->tags = [
                    'result' => 'ok',
                    'request' => json_encode(['request' => $data], JSON_UNESCAPED_UNICODE)
                ];
            }

            $logger->save();
        }
    }
}
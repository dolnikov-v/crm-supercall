<?php

namespace backend\components;

use Aws\Credentials\Credentials;
use Yii;
use yii\base\Component;
use Aws\TranscribeService\TranscribeServiceClient;
use yii\base\Exception;

class AwsTranscribe extends Component
{
    const MAX_RESULTS = 100;
    const MARK_END_TRANSCRIBE_FILE = '_transcribe.txt';
    const EN_LANG = 'en-US';
    const ES_LANG = 'es-US';

    const FORMAT_MP3 = 'mp3';
    const FORMAT_MP4 = 'mp4';
    const FORMAT_WAV = 'wav';
    const FORMAT_FLAC = 'flac';

    const MIME_WAV = 'audio/x-wav';
    const MIME_MP3 = 'audio/mpeg';

    const DEFAUL_RATE_HERTZ = 8000;

    /** @var  string */
    public $key;
    /** @var  string */
    public $secret;
    /** @var  string */
    public $version;
    /** @var  string */
    public $region;
    /** @var  array */
    public $credentials;

    /**
     * AwsTranscribe constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->key = $config['credentials']['key'];
        $this->secret = $config['credentials']['secret'];
        parent::__construct($config);
    }

    public function getTranscribeClient()
    {
        return new TranscribeServiceClient([
            'version' => $this->version,
            'region' => $this->region,
            'credentials' => new Credentials($this->key, $this->secret),
        ]);
    }

    /**
     * @return array
     */
    public function getSupportedLanguage()
    {
        return [
            self::EN_LANG => yii::t('common', 'Английский'),
            self::ES_LANG => yii::t('common', 'Испанский')
        ];
    }

    /**
     * @return array
     */
    public function getSupportedFormats()
    {
        return [
            self::FORMAT_MP3,
            self::FORMAT_MP4,
            self::FORMAT_WAV,
            self::FORMAT_FLAC
        ];
    }

    /**
     * @param $file
     * @return bool|string
     */
    public static function getMediaFormat($file)
    {
        $mime = mime_content_type($file);
        $format = false;

        switch ($mime) {
            case self::MIME_MP3 :
                $format = self::FORMAT_MP3;
                break;
            case self::MIME_WAV :
                $format = self::FORMAT_WAV;
                break;
        }

        return $format;
    }

    public static function getDefaultSettingsParams()
    {
        return [
            'ChannelIdentification' => true,
            //'MaxSpeakerLabels' => 2,  //if ShowSpeakerLabels === true
            'ShowSpeakerLabels' => false
        ];
    }

    /**
     * @param string $lang
     * @return bool
     */
    public function verifyLanguage($lang)
    {
        return in_array($lang, array_keys($this->getSupportedLanguage()));
    }

    /**
     * @param $format
     * @return bool
     */
    public function verifyFormat($format)
    {
        return in_array($format, $this->getSupportedFormats());
    }

    /**
     * https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-transcribe-2017-10-26.html#starttranscriptionjob
     * @param $params
     * @return array
     */
    public function checkParams($params)
    {
        $error = null;

        if (!isset($params['LanguageCode'])) {
            $error = 'Missing required parameter: $params[LanguageCode]';
        } else {
            if (!$this->verifyLanguage($params['LanguageCode'])) {
                $error = 'Not allowed language code';
            }
        }

        if (!isset($params['Media'])) {
            $error = 'Missing required parameter: $params[Media]';
        } else {
            if (!isset($params['Media']['MediaFileUri'])) {
                $error = 'Missing required parameter: $params[Media][MediaFileUri]';
            }
        }

        if (!isset($params['MediaFormat'])) {
            $error = 'Missing required parameter: $params[MediaFormat]';
        } else {
            if (!$this->verifyFormat($params['MediaFormat'])) {
                $error = 'Not allowed MediaFormat';
            }
        }

        if ($error) {
            return [
                'success' => false,
                'message' => $error
            ];
        } else {
            return [
                'success' => true,
                'message' => $params
            ];
        }
    }

    /**
     * https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-transcribe-2017-10-26.html#starttranscriptionjob
     * @param $params
     * @return array
     *
     * answer
     * $result['@metadata']['statusCode'] == 200 is ok
     * $result['TranscriptionJob']['TranscriptionJobName'] is name
     * $result['TranscriptionJob']['TranscriptionJobStatus'] is status IN_PROGRESS|FAIL|COMPLETED
     */
    public function createJob($file, $params)
    {
        if (!isset($params['OutputBucketName'])) {
            $params['OutputBucketName'] = AwsS3::TRANSCRIBE_BUCKET;
        }

        if (!isset($params['MediaSampleRateHertz'])) {
            $params['MediaSampleRateHertz'] = self::DEFAUL_RATE_HERTZ;
        }

        if (!isset($params['Settings'])) {
            $params['Settings'] = self::getDefaultSettingsParams();
        }

        if (!isset($params['TranscriptionJobName'])) {
            $params['TranscriptionJobName'] = basename($file) . '_' . time() . '_' . self::MARK_END_TRANSCRIBE_FILE;
        }

        $checkParams = $this->checkParams($params);

        if (!$checkParams['success']) {
            return $checkParams;
        } else {
            return [
                'success' => true,
                'message' => $this->getTranscribeClient()->startTranscriptionJob($params)
            ];
        }
    }

    /**
     * https://docs.aws.amazon.com/transcribe/latest/dg/API_GetTranscriptionJob.html
     * @param $name
     * @return \Aws\Result
     * $result['@metadata']['statusCode'] == 200 is ok
     * $result['TranscriptionJob']['TranscriptionJobName'] is name
     * $result['TranscriptionJob']['TranscriptionJobStatus'] is status IN_PROGRESS|FAIL|COMPLETED
     * $result['TranscriptionJob']['Transcript']['TranscriptFileUri']  is transcribe file
     *
     * $result['@metadata']['statusCode'] == 200
     * $result['TranscriptionJob']['TranscriptionJobStatus'] == FAILED
     * $result['TranscriptionJob']['TranscriptionJobStatus']['FailureReason'] - error text
     */
    public function getJob($name)
    {
        return $this->getTranscribeClient()->getTranscriptionJob([
            'TranscriptionJobName' => $name
        ]);
    }

    /**
     * https://docs.aws.amazon.com/transcribe/latest/dg/API_ListTranscriptionJobs.html
     * @return \Aws\Result
     *
     * $result['TranscriptionJobSummaries'] is array
     *   array data
     *   [0] => ['TranscriptionJobName'] is name
     *          ['TranscriptionJobStatus'] is status IN_PROGRESS|FAIL|COMPLETED
     *
     */
    public function getListJobs()
    {
        return $this->getTranscribeClient()->listTranscriptionJobs([
            'MaxResults' => self::MAX_RESULTS
        ]);
    }
}
<?php

namespace backend\controllers;

use backend\components\web\Controller;
use yii;
use common\components\Telegram;

class TelegramController extends Controller
{
    public $telegramApi;

    public function actionIndex()
    {
        $telegram = yii::$app->get('telegram');

        $updatesUrl = $telegram->buildUrl('getupdates');

        $params = [
            'url' => $updatesUrl,
            'method' => 'get'
        ];

        $updates = $telegram->sendRequest($params, '/telegram/index');

        //иначе все есть в логгере
        if(isset($updates['success']) && $updates['success'] === true){

            if(isset($updates['content'])){
                $response = json_decode($updates['content'], 1);
                //по факту - вываливаются все сообщения.
                //либо храним $v['update_id'] - при успешном ответе на текст
                //либо учимся работать как-то с датой $v['message']['date']; - но это филькина грамота
                if($response['ok'] === true){
                    foreach($response['result'] as $k => $v){
                        /**
                        Array
                        (
                            [update_id] => 800906080
                            [message] => Array
                            (
                                [message_id] => 3
                                [from] => Array
                                (
                                    [id] => 478844722
                                    [is_bot] =>
                                    [first_name] => Игорь
                                    [last_name] => Васинский
                                    [username] => vasinsky
                                    [language_code] => ru-RU
                                )
                                [chat] => Array
                                (
                                    [id] => 478844722
                                    [first_name] => Игорь
                                    [last_name] => Васинский
                                    [username] => vasinsky
                                    [type] => private
                                )
                                [date] => 1509010012
                                [text] => Хелп
                            )
                        )
                         */

                        $update_id = $v['update_id'];
                        $message_id = $v['message']['message_id'];
                        $data = $v['message']['date'];

                        $text = isset($v['message']['text']) ? $v['message']['text'] : null;
                        $contact = isset($v['message']['contact']) ? $v['message']['contact'] : null;

                        $from = $v['message']['from'];
                        $chat = $v['message']['chat'];

                        $answer = 'Не разумею пра што ты кажаш.';

                        //распознаём текст
                        if(!is_null($text)){
                            switch($text){
                                case '1' : $answer = 'Один'; break;
                                case '2' : $answer = 'Два'; break;
                                case '3' : $answer = 'Три'; break;
                                case '4' : $answer = 'Четыре'; break;
                                default : $answer = 'я из колхоза, считаю только до четырёх'; break;
                            }
                        }
                        //или распознаём отправленный контакт, а может ещё и файл, видео, аудио, в общем - весь репиртуар Дурова.
                        if(!is_null($contact)){
                            $phone_number= $contact['phone_number'];
                            $first_name = $contact['first_name'];
                            $last_name= $contact['last_name'];
                            $user_id= $contact['user_id'];
                            $chat_id = $v['message']['from']['id'];
                            $answer = '...';
                        }
                        //так мы отвечаем от лица бота - конкретному пользователю
                        //$sendAnswer = file_get_contents($telegram->buildUrl('sendmessage?chat_id='.$chat_id.'&text='.$answer));
                        //echo '<pre>' . print_r($sendAnswer, 1) . '</pre>';
                    }

                }else{
                    $logger = yii::$app->get('logger');
                    $logger->action = 'getupdates';
                    $logger->route = '/telegram/index';
                    $logger->type = Telegram::LOGGER_TYPE;
                    $logger->tags = ['answer' => 'error'];
                    $logger->save();
                }
            }
        }
        exit;
    }
}
<?php

namespace backend\controllers;

use backend\components\AwsS3;
use backend\components\AwsTranscribe;
use backend\components\web\Controller;
use backend\modules\order\models\TranscribeQueue;
use Yii;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Class TestAwsController
 * @package backend\controllers
 */
class TestAwsController extends Controller
{
    /**
     * @return AwsS3
     */
    public function getS3Component()
    {
        return yii::$app->get('AwsS3');
    }

    /**
     * @return AwsTranscribe
     */
    public function getTranscribeComponent()
    {
        return yii::$app->get('AwsTranscribe');
    }

    public function actionTestS3Client()
    {
        $buckets = $this->getS3Component()->getS3Client();
        echo '<pre>' . print_r($buckets, 1);
    }

    public function actionGetBuckets()
    {
        $buckets = $this->getS3Component()->getBuckets();

        foreach ($buckets as $bucket) {
            echo '<pre>' . print_r($bucket, 1);
        }
    }

    public function actionTranscribeBucket()
    {
        $exists = $this->getS3Component()->checkTranscribeBucket();
        var_dump($exists);
    }

    /**
     * $result['@metadata']['status'] == 200 is ok
     */
    public function actionCreateBucket()
    {
        $result = $this->getS3Component()->createBucket(AwsS3::TRANSCRIBE_BUCKET);
        echo '<pre>' . print_r($result['@metadata'], 1);
    }

    /**
     * $result['@metadata']['status'] == 204 is ok
     */
    public function actionDeleteBucket()
    {
        $result = $this->getS3Component()->deleteBucket(AwsS3::TRANSCRIBE_BUCKET);
        echo '<pre>' . print_r($result['@metadata'], 1);
    }

    public function actionTestTranscribeClient()
    {
        $client = $this->getTranscribeComponent()->getTranscribeClient();
        echo '<pre>' . print_r($client, 1);
    }

    public function actionPutInBucket()
    {
        $file = 'http://136.243.130.196/acdr/get.php?audio=/var/recording/in/2018-08-30/11/6285368062075-2wcall1213-11:09.wav';
        //$file = 'http://136.243.130.196/acdr/get.php?audio=/var/recording/in/2018-08-29/21/5213467877065-2wcall886-21:35.wav';
        $name = yii::getAlias('@backend') . '/files/temp/' . strtr(basename($file), [':' => '-']);

         /*functional to get file
                $client = new Client([
                    'transport' => 'yii\httpclient\CurlTransport'
                ]);

                $response = $client->createRequest()
                    //->addHeaders(['Authorization' => 'Basic '.base64_encode("support:support")])
                    ->setUrl($file)
                    ->send();

                file_put_contents($name, $response->content);
        */


        $result = $this->getS3Component()->putObjectInBucket(AwsS3::TRANSCRIBE_BUCKET, $name);

        echo '<pre>' . print_r($result['@metadata'], 1) . '</pre>';


    }

    public function actionCreateJob()
    {
        //$file = 'https://s3.eu-west-1.amazonaws.com/transcribe.files/6285368062075-2wcall1213-11-09.wav';
        $file = 'https://s3.eu-west-1.amazonaws.com/transcribe.files/5213467877065-2wcall886-21-35.wav';

        $format = AwsTranscribe::getMediaFormat(yii::getAlias('@backend') . '/files/temp/' . basename($file));

        if(!$format){
            echo 'Incorrect mediaFormatFile'; exit;
        }

        $params = [
            'LanguageCode' => AwsTranscribe::ES_LANG,
            'Media' => [
                'MediaFileUri' => $file
            ],
            'MediaFormat' => $format,
            'MediaSampleRateHertz' => AwsTranscribe::DEFAUL_RATE_HERTZ,
            'OutputBucketName' => AwsS3::TRANSCRIBE_BUCKET,
            'Settings' => AwsTranscribe::getDefaultSettingsParams(),
        ];

        $result = $this->getTranscribeComponent()->createJob($file, $params);

        echo '<pre>' . print_r($result, 1) . '</pre>'; exit;


    }

    public function actionGetJob()
    {
        //$file = $file = 'https://s3.eu-west-1.amazonaws.com/transcribe.files/6285368062075-2wcall1213-11-09.wav';
        $file = 'https://s3.eu-west-1.amazonaws.com/transcribe.files/5213467877065-2wcall886-21-35.wav';
        $name = basename($file) . AwsTranscribe::MARK_END_TRANSCRIBE_FILE;

        $result = $this->getTranscribeComponent()->getJob($name);

        echo '<pre>' . print_r($result, 1) . '</pre>';
    }

    public function actionGetListJobs()
    {
        $result = $this->getTranscribeComponent()->getListJobs();
        echo '<pre>' . print_r($result, 1) . '</pre>';
    }

    public function actionGetObject()
    {
        $bucket = AwsS3::TRANSCRIBE_BUCKET;
        $file = '13_85570568056-2wcall1228-11-30.wav_1535952165__transcribe.txt.json';

        $result = $this->getS3Component()->getObject($bucket, $file);

        echo '<pre>' . print_r(Json::decode($result), 1) . '</pre>';
    }
}
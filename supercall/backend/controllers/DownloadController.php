<?php

namespace backend\controllers;

use backend\components\web\Controller;
use Yii;

/**
 * Class DownloadController
 * @package backend\controllers
 */
class DownloadController extends Controller
{
    public function actionGetFile()
    {
        $filePath = yii::$app->request->get('file');
        Yii::$app->response->sendFile($filePath);
        yii::$app->end();
    }

    /**
     * @param bool $url
     */
    public function actionGetFileFromUrl($url = false)
    {
        if(!$url){
            $filePath = yii::$app->request->get('file');
        } else {
            $filePath = $url;
        }

        $this->redirect($filePath);
    }
}
<?php
namespace backend\controllers;

use backend\components\web\Controller;
use Yii;

/**
 * Class ErrorsController
 * @package backend\controllers
 */
class ErrorsController extends Controller
{
    public $layout = '@backend/views/layouts/error';

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
}

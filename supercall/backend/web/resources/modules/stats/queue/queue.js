$(function () {
    var Queue = {
        url: '/queue/control/get-queue-by-country',
        name: "[name*=queue]",
        country: "[name*=countries]",
        loader: '.loader',
        searchByName: '.search-by-name',
        init: function () {
            Queue.getQueueByCountry();
        },
        loaderShow: function () {
            $(Queue.loader).removeClass('hidden');
            $(Queue.name).closest('div').addClass('hidden');
        },
        loaderHide: function () {
            $(Queue.loader).addClass('hidden');
            $(Queue.name).closest('div').removeClass('hidden');
        },
        setDisabled: function () {
            $(Queue.name).prop('disabled', true);
        },
        setEnabled: function () {
            $(Queue.name).prop('disabled', false);
        },
        getQueueByCountry: function () {
            Queue.loaderShow();
            $.ajax({
                url: Queue.url,
                dataType: 'json',
                type: 'post',
                data: {
                    country_id: $(Queue.country).val()
                },
                success: function (data) {
                    Queue.setOptions(data);
                    Queue.setEnabled();
                    Queue.loaderHide();
                },
                error: function (error) {
                    toastr.error(error.responseText);
                    Queue.setDisabled();
                    Queue.loaderHide();
                }
            });
        },
        setOptions: function (data) {
            $(Queue.name).find('option').remove();

            if ($(Queue.searchByName).length) {
                for (var k in data) {
                    var newOption = new Option(data[k], data[k], true, true);
                    $(Queue.name).append(newOption);
                }
            } else {
                for (var k in data) {
                    var newOption = new Option(data[k], k, true, true);
                    $(Queue.name).append(newOption);
                }
            }

            $(Queue.name).val(parseUrlMultiple(getSearchModelName() + '[queue][]'));
            $(Queue.name).trigger('change');
        }
    };

    Queue.init();
    $(Queue.country).on('change', function () {
        Queue.getQueueByCountry();
    });
});
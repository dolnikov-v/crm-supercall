/**
 * Move tfoot to the thead in the bootstrap-table
 *
 * @param tableId
 */
function mvFooterToHead(tableId) {
  var tableFoot = tableId + '>tfoot';
  var footerTable = $(tableFoot).html()

  $(tableId + '>thead').append(footerTable)
  $(tableFoot).remove()

}
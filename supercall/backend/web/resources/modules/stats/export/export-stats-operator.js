$(function () {
    var Export = {
        container: '.export_btn',
        table: '#statistic-of-operators',
        btns: '.export_to',
        checkedColor: 'red',
        uncheckedColor: '#686868',
        modal: '#orders_export',
        error_modal: '#orders_export_error',
        modal_body: '#order_export_modals',
        export_btn: '#orders_export_btn',
        title_modal: 'h4',
        spinner: '.loader',
        form: null,
        init: function () {
            Export.form = $(Export.container).find('form');
            Export.initBtns($(Export.container).find(Export.btns));
        },
        setTitleModal: function (end_phrase) {
            $(Export.modal).find(Export.title_modal).html('Export to ' + end_phrase);
        },
        initBtns: function (btns) {
            btns.map(function () {
                $(this).on('click', function () {
                    Export.createTaskToExport($(this).data('url'));
                    return false;
                });
            });
        },
        createTaskToExport: function(url){
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data : {
                    queryParams: $('[name*=queryParams]').val()
                },
                success: function(data){
                    if(data.success){
                        toastr.success(data.message);
                    }else{
                        toastr.error(data.message);
                    }
                },
                error: function(error){
                    toastr.error(error.responseText);
                }
            });
        }
    };

    Export.init();
});
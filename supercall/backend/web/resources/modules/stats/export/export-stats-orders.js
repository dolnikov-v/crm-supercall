$(function () {
    var Export = {
        url: '/stats/order-export',
        btnExport: '.export_btn',
        btns: '.export_to',
        dropDown: '.dropdown',
        mainPanel: '#panel-order-stats',
        dataType: null,
        form: null,
        country: '[name*=countries]',
        group: '[name*=group]',
        emptyQueryParam: '[]',
        queryParam: '[name*=queryParams]',
        init: function () {
            Export.form = $(Export.btnExport).find('form');
            Export.initBtns($(Export.btnExport).find(Export.btns));
        },
        checkCountryFilter: function () {
            return $(Export.countries).val();
        },
        getActiveTab: function () {
            return $(Export.mainPanel).find('ul').find('li.active');
        },
        getTypeExport: function () {
            var activeTab = Export.getActiveTab();
            return $(activeTab).find('a').attr('data-type');
        },
        initBtns: function (btns) {
            btns.map(function () {
                $(this).on('click', function () {
                    Export.dataType = $(this).attr('data-type');
                    Export.createTaskToExport();
                    return false;
                });
            });
        },
        closeDropDown: function () {
            $(Export.btnExport).find(Export.dropDown).removeClass('open');
        },
        waitAnswer: function () {
            $(Export.btnExport).css('cursor', 'wait');
        },
        answerAccepted: function () {
            $(Export.btnExport).css('cursor', 'pointer');
        },
        createTaskToExport: function () {
            Export.closeDropDown();
            Export.waitAnswer();

            if (!Export.checkCountryFilter() && $(Export.queryParam).val() === Export.emptyQueryParam) {
                toastr.error('You must specify at least 1 country in the filter');
                return false;
            }

            $.ajax({
                url: Export.url,
                type: 'post',
                dataType: 'json',
                data: {
                    queryParams: $(Export.queryParam).val(),
                    tab: Export.getTypeExport(),
                    typeExport: Export.dataType,
                    group: $(Export.group).val()
                },
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                    } else {
                        toastr.error(data.message);
                    }

                    Export.answerAccepted();
                },
                error: function (error) {
                    toastr.error(error.responseText);
                    Export.answerAccepted();
                }
            });
        }
    };

    Export.init();
});
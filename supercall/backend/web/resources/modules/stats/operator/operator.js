$(function () {
    var Operator = {
        url: '/operator/control/get-operators-by-countries',
        operators: "[name*=OperatorReport\\[username\\]\\[\\]]",
        countries: "[name*=countries]",
        loader: '.loader',
        init: function () {
            if (parseUrl('OperatorReport[username][]') != '') {
                $(Operator.countries).on('change', Operator.getOperatorsByCountries);
            }
        },
        loaderShow: function () {
            $(Operator.loader).removeClass('hidden');
            $(Operator.operators).closest('div').addClass('hidden');
        },
        loaderHide: function () {
            $(Operator.loader).addClass('hidden');
            $(Operator.operators).closest('div').removeClass('hidden');
        },
        getOperatorsByCountries: function () {
            if ($(Operator.countries).val() == '') {
                return;
            }

            Operator.loaderShow();

            $.ajax({
                url: Operator.url,
                dataType: 'json',
                type: 'post',
                data: {
                    countries: $(Operator.countries).val()
                },
                success: function (data) {
                    Operator.setOptions(data);
                    Operator.loaderHide();
                },
                error: function (error) {
                    toastr.error(error.responseText);
                    Operator.loaderHide();
                }
            });
        },
        setOptions: function (data) {
            console.log('data', data);
            $(Operator.operators).find('option').remove();

            for (var elem in data) {

                var newOption = new Option(data[elem]['username'], data[elem]['id'], true, true);
                $(Operator.operators).append(newOption);

            }
            $(Operator.operators).val(parseUrlMultiple('OperatorReport[username][]'));
            $(Operator.operators).trigger('change');
        }
    };

    Operator.init();
});

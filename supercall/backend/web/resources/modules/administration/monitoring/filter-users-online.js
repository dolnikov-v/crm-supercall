$(function () {
    DEBUG = false;

    var FilterUO = {
        form: 'form',
        btn_reset: '#reset-filter',
        typesSelect: [],
        typesText: [],
        filtersData: [],
        monitoring_table: '.monitoring-table',
        DATA_TYPE_NOT_FOUND: 'not-found',
        TEXT_NOT_FOUND: 'not found data',
        TYPE_COUNTRY: 'user_country',
        TYPE_OPERATOR: 'user_id',
        TYPE_TYPE_CALL: 'type_call',
        TYPE_QUEUE_PAUSED: 'queue_paused',
        TYPE_STATUS_ZOIPER: 'status_zoiper',
        TYPE_STATUS_CALL: 'status_call',
        TYPE_ORDER: 'order',
        TYPE_IP_ADDRESS: 'ip_address',
        TYPE_ELEM_TEXT: 'text',
        TYPE_ELEM_SELECT: 'select-one',
        field_country: "[name*=user_country]",
        field_operator: "[name*=user_id]",
        field_type_call: "[name*=type_call]",
        field_status_zoiper: "[name*=status_zoiper]",
        field_status_call: "[name*=status_call]",
        field_queue_paused: "[name*=queue_paused]",
        field_order: "[name*=order]",
        field_ip: "[name*=ip_address]",
        summary_users: '.summary_users',
        mapFilter: {
            'status_call': {
                'waiting': null
            },
            'queue_paused': {
                '': null,
                'pause_on': 1,
                'pause_off': 0
            },
            'type_call': {
                'auto_call': 'auto call',
                'incoming_call': 'incoming'
            }

        },
        init: function () {
            FilterUO.collectTypes();
            FilterUO.setEventSelectFiedsFilter();
            FilterUO.setEventTextFiedsFilter();

            $(FilterUO.btn_reset).on('click', FilterUO.eventReset);
        },
        eventReset: function () {
            var filterElems = FilterUO.getSelectTypeFilters().concat(FilterUO.getTextTypeFilters());

            for (var k in filterElems) {
                var elem = $('[name*=' + filterElems[k] + ']');

                switch (elem.prop('type')) {
                    case FilterUO.TYPE_ELEM_TEXT:
                        elem.val('');
                        break;
                    case FilterUO.TYPE_ELEM_SELECT:
                        elem.val('');
                        elem.trigger('change');
                        break;
                }
            }

            FilterUO.search();
        },
        search: function () {
            FilterUO.setFilters();

            var rows = FilterUO.getRows();
            var filterSet = FilterUO.filtersData;

            if (DEBUG) console.log('filtersData', FilterUO.filtersData);

            var foundedRows = [];

            $(rows).each(function () {
                var result = FilterUO.findInRow($(this), filterSet);

                if (result) {
                    foundedRows.push(result);
                }
            });

            if (DEBUG) console.log('foundedRows', foundedRows);

            if (foundedRows.length === 0) {
                FilterUO.getRows().addClass('hidden');
                FilterUO.setNotFound();
            } else {
                FilterUO.removeNotFound();
                FilterUO.showHideResult(foundedRows);
            }

            $(FilterUO.summary_users).text(foundedRows.length);
        },
        showHideResult: function (foundedRowsUniqueID) {
            if (DEBUG) console.log('foundedRowsUniqueID: ', foundedRowsUniqueID);

            FilterUO.getRows().each(function () {

                if (foundedRowsUniqueID.indexOf($(this).attr('data-uniqueid')) >= 0) {
                    $(this).removeClass('hidden');
                    $(this).addClass('visibled');
                } else {
                    $(this).removeClass('visibled');
                    $(this).addClass('hidden');
                }
            });
        },
        searchByColumnUserCountry: function (filterValue, ColumnValue) {
            return Object.keys(JSON.parse(ColumnValue)).indexOf(filterValue) >= 0;
        },
        searchByOrder: function (filterValue, ColumnValue) {
            var regExp = new RegExp('^' + filterValue);
            return regExp.test(ColumnValue);
        },
        searchByIP: function (filterValue, ColumnValue) {
            var regExp = new RegExp('^' + filterValue);

            return regExp.test(ColumnValue);
        },
        getRows: function () {
            return $(FilterUO.monitoring_table).find('tr.row_table');
        },
        findInRow: function (row, filterSet) {
            var mapFilter = FilterUO.mapFilter;
            var result = true;

            if (typeof $(row).attr('data-uniqueid') !== 'undefined') {
                $(row).find('td').each(function () {
                    var columnType = $(this).attr('data-type');
                    var columnValue = $(this).attr('data-value');
                    columnValue = columnValue === 'null' ? null : columnValue;

                    for (var filterType in filterSet) {

                        if (filterType === columnType) {
                            var filterValue = filterSet[filterType];

                            if (typeof mapFilter[filterType] !== 'undefined') {
                                if (typeof mapFilter[filterType][filterValue] !== 'undefined') {
                                    if (DEBUG) console.log('filtermap ', filterType, ':', mapFilter[filterType][filterValue]);

                                    filterValue = mapFilter[filterType][filterValue];
                                }
                            }

                            switch (filterType) {
                                case FilterUO.TYPE_COUNTRY:
                                    if (!FilterUO.searchByColumnUserCountry(filterValue, columnValue)) {
                                        result = false;
                                    }
                                    break;
                                case FilterUO.TYPE_ORDER:
                                    if (!FilterUO.searchByOrder(filterValue, columnValue)) {
                                        result = false;
                                    }
                                    break;
                                case FilterUO.TYPE_IP_ADDRESS:
                                    if (!FilterUO.searchByIP(filterValue, columnValue)) {
                                        result = false;
                                    }
                                    break;
                                default:
                                    if (columnValue === filterValue) {
                                        if (DEBUG) console.log('+ ', columnType, ':', columnValue, ' => ', filterValue);
                                    } else {
                                        result = false;
                                        if (DEBUG) console.log('- ', columnType, ':', columnValue, ' => ', filterValue);
                                    }
                                    break;
                            }
                        }
                    }

                });
            }

            return !result ? result : $(row).attr('data-uniqueid');
        },
        setNotFound: function () {
            FilterUO.renderRow();
        },
        removeNotFound: function () {
            $(FilterUO.monitoring_table).find(FilterUO.getMarkerNotFoundRow()).remove();
        },
        issetNotFoundRow: function () {
            return $(FilterUO.monitoring_table).find(FilterUO.getMarkerNotFoundRow()).prop('tagName') === 'TR';
        },
        getMarkerNotFoundRow: function () {
            return 'tr[data-type="' + FilterUO.DATA_TYPE_NOT_FOUND + '"]';
        },
        getColumnsInThead: function () {
            return $(FilterUO.monitoring_table).find('thead').find('th');
        },
        renderRow: function () {
            if (!FilterUO.issetNotFoundRow()) {
                var innerRow = $('<td colspan="' + FilterUO.getColumnsInThead().length + '"/>')
                    .addClass('text-center')
                    .addClass('text-muted')
                    .html(FilterUO.TEXT_NOT_FOUND);

                var row = $('<tr data-type="' + FilterUO.DATA_TYPE_NOT_FOUND + '"/>').append(innerRow);

                $(FilterUO.monitoring_table).append(row);
            }
        },
        collectTypes: function () {
            FilterUO.typesSelect = FilterUO.getSelectTypeFilters();
            FilterUO.typesText = FilterUO.getTextTypeFilters();
        },
        setFilters: function () {
            var filterVal = false;
            var filters = {};

            for (var k in FilterUO.typesSelect) {
                filterVal = FilterUO.getFiletrByType(FilterUO.typesSelect[k]);

                if (filterVal) {
                    filters[FilterUO.typesSelect[k]] = filterVal;
                }
            }

            for (var k in FilterUO.typesText) {
                filterVal = FilterUO.getFiletrByType(FilterUO.typesText[k]);

                if (filterVal) {
                    filters[FilterUO.typesText[k]] = filterVal;
                }
            }

            FilterUO.filtersData = filters;
        },
        getFiletrByType: function (type) {
            var val = $(FilterUO.form).find("[name*=" + type + "]").val();

            return val !== '' ? val : false;
        },
        getSelectTypeFilters: function () {
            return [
                FilterUO.TYPE_COUNTRY,
                FilterUO.TYPE_OPERATOR,
                FilterUO.TYPE_TYPE_CALL,
                FilterUO.TYPE_STATUS_ZOIPER,
                FilterUO.TYPE_STATUS_CALL,
                FilterUO.TYPE_QUEUE_PAUSED
            ];
        },
        getTextTypeFilters: function () {
            return [
                FilterUO.TYPE_ORDER,
                FilterUO.TYPE_IP_ADDRESS
            ];
        },
        setEventSelectFiedsFilter: function () {
            for (var k in FilterUO.typesSelect) {
                $("[name*=" + FilterUO.typesSelect[k] + "]").on('change.filter', function (e) {
                    e.preventDefault();
                    FilterUO.search();
                });
            }
        },
        setEventTextFiedsFilter: function () {
            for (var k in FilterUO.typesText) {
                $("[name*=" + FilterUO.typesText[k] + "]").on('keyup blur', function (e) {
                    FilterUO.search();
                });
            }
        },
        eventSelect: function (elem) {
            if (DEBUG) console.log($(elem).val());
        }
    };


    try {
        window.FilterUO = FilterUO;

        FilterUO.init();

        FilterUO = Object.assign({}, window.FilterUO);
    } catch (e) {
        console.log(e);
    }
});
$(function () {
    const LOCAL = false;

    var serverNodeJS = LOCAL ? 'http://socket2wcall.local' : 'http://socket.2wcall.com:44366';

    var MonitoringUO = {
        TOKEN: 'YzFhYjk5MjRlY2RhMWJlYWY4YmJhYTFlYjgyMzhiODNlMGVkOGM2Mw==',
        QUEUE_PAUSED: 1,
        TYPE_MEMORY_MEMORY: 'memory',
        TYPE_MEMORY_MONGODB: 'mongo',
        SOCKET: io.connect(serverNodeJS),
        TYPE_SET_DATA: 'type_set_data',
        TYPE_GET_DATA: 'type_get_data',
        TYPE_DELETE_DATA: 'type_delete_data',
        DATA_TYPE_USER_COUNTRY: 'user_country',
        DATA_TYPE_LAST_ACTIVITY: 'last_activity',
        DATA_TYPE_ACTION_TIME: 'action_time',
        DATA_TYPE_USER_READY: 'user_ready',
        DATA_TYPE_STATUS_ZOIPER: 'status_zoiper',
        DATA_TYPE_STATUS_CALL: 'status_call',
        DATA_TYPE_USERNAME: 'username',
        DATA_TYPE_USER_ID: 'user_id',
        DATA_TYPE_ORDER: 'order',
        DATA_TYPE_CALL: 'type_call',
        DATA_TYPE_QUEUE_PAUSED: 'queue_paused',
        DATA_TYPE_UNIQUEID: 'uniqueid',
        DATA_TYPE_MEMORY: 'type_memory',
        DATA_TYPE_USE_MESSENGER: 'use_messenger',
        DATA_TYPE_IP: 'ip',
        PSEUDO_TYPE_IP: 'ip_address',
        /*statuses*/
        STATUS_NEW: 'New',
        STATUS_NEW_CODE: '1',
        STATUS_RECALL: 'Recall',
        STATUS_RECALL_CODE: '2',
        STATUS_NO_ANSWER: 'No answer',
        STATUS_NO_ANSWER_CODE: '3',
        STATUS_APPROVE: 'Approve',
        STATUS_APPROVE_CODE: '4',
        STATUS_REJECT: 'Reject',
        STATUS_REJECT_CODE: '5',
        STATUS_TRASH: 'Trash',
        STATUS_TRASH_CODE: '6',
        STATUS_DOUBLE: 'Double',
        STATUS_DOUBLE_CODE: '7',
        /*end statuses*/
        FILTER: window.FilterUO,
        dropdownCountryList: '.ckl_block',
        table: '.monitoring-table',
        state_monitor: '.state_monitor',
        summary_users: '.summary_users',
        openedUserCountryList: [],
        data_prev: {},
        timers: {},
        first_load: {},
        init: function () {


            MonitoringUO.SOCKET.on('connect', function () {
                MonitoringUO.socketStateConnected();
                MonitoringUO.dropRows();

                MonitoringUO.listenWS();
            });

            MonitoringUO.SOCKET.on('disconnect', function () {
                MonitoringUO.socketStateDisconnected();
            });
        },
        getByDataType: function (td, dataType) {
            return $(td).closest('tr').find('td[data-type="' + dataType + '"]').attr('data-value');
        },
        toHHMMSS: function (seconds_count) {
            var sec_num = parseInt(seconds_count, 10); // don't forget the second param
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            return hours + ':' + minutes + ':' + seconds;
        },
        //запуск таймера и обнуление таймера при смене state  data-type="order"
        isStartTimerOrderStateChange: function (td, data) {
            var uniqueid = $(td).closest('tr').attr('data-uniqueid');
            var order_td_state = MonitoringUO.getByDataType(td, MonitoringUO.DATA_TYPE_ORDER);
            var prev_data = (isUndefined(MonitoringUO.data_prev[uniqueid]) || isNull(MonitoringUO.data_prev[uniqueid]) ? '' : MonitoringUO.data_prev[uniqueid]).toString();
            var new_data = (isNull(order_td_state) || isUndefined(order_td_state) ? '' : order_td_state).toString();

            if(DEBUG) {
                console.log('first_load', isUndefined(MonitoringUO.first_load[uniqueid]));
                console.log('NEW DATA', new_data);
                console.log('FIND PREV_DATA: ', uniqueid, data[MonitoringUO.DATA_TYPE_ORDER]);
            }

            MonitoringUO.data_prev[uniqueid] = data[MonitoringUO.DATA_TYPE_ORDER];

            if(DEBUG) {
                console.log('SET PREV_DATA: ', uniqueid, data[MonitoringUO.DATA_TYPE_ORDER]);
            }
            if (isUndefined(MonitoringUO.first_load[uniqueid])) {
                return true;
            }
            else if (isEmpty(new_data)) {
                return false;
            }
            else {
                return prev_data !== new_data
            }
        },
        resetTimer: function(data){
            var rowByField = MonitoringUO.findRow(data.uniqueid);
            var td_action_time = $(rowByField).find('td[data-type="' + MonitoringUO.DATA_TYPE_ACTION_TIME + '"]');

            if (MonitoringUO.isStartTimerOrderStateChange(td_action_time, data)) {
                MonitoringUO.stopTimer(td_action_time);
                MonitoringUO.startTimer(td_action_time, 1);
            }
        },
        startTimer: function (td, startBy) {
            var uniqueid = $(td).closest('tr').attr('data-uniqueid');
            var data_value_action_time = $(td).attr('data-value');

            MonitoringUO.first_load[uniqueid] = false;

            MonitoringUO.timers[uniqueid] = setInterval(function () {
                var counter = data_value_action_time++;

                $(td).attr('data-value', counter);
                $(td).closest('tr').find('td[data-type="' + MonitoringUO.DATA_TYPE_LAST_ACTIVITY + '"]').attr('data-value', counter);

                startBy++;
                $(td).text(MonitoringUO.toHHMMSS(startBy));
            }, 1000);
        },
        stopTimer: function (td) {
            var uniqueid = $(td).closest('tr').attr('data-uniqueid');

            clearInterval(MonitoringUO.timers[uniqueid]);
        },
        getDiff: function (td) {
            return Math.floor(($(td).attr('data-value') - MonitoringUO.getTime()) / 1000);
        },
        listenWS: function () {
            //запросить данные
            if (LOCAL) console.warn(MonitoringUO.TYPE_GET_DATA);

            MonitoringUO.SOCKET.emit(MonitoringUO.TYPE_GET_DATA, true);

            //получить данные
            if (LOCAL) console.warn(MonitoringUO.TYPE_SET_DATA);

            MonitoringUO.SOCKET.on(MonitoringUO.TYPE_SET_DATA, function (data) {
                if (typeof data === 'object') {
                    if (MonitoringUO.renderBodyTable(data)) {
                        MonitoringUO.FILTER.search();
                    }
                }
            });

            MonitoringUO.SOCKET.on(MonitoringUO.TYPE_DELETE_DATA, function (data) {
                //в случае выхода юзера - сюда придёт только user_id
                if (typeof data === 'string') {
                    MonitoringUO.removeRow(data);
                }
            });
        },
        socketStateConnected: function () {
            $(MonitoringUO.state_monitor)
                .removeClass('fa fa-chain-broken')
                .removeClass('text-danger')
                .addClass('fa fa-chain')
                .addClass('text-success');
        },
        socketStateDisconnected: function () {
            $(MonitoringUO.state_monitor)
                .removeClass('fa fa-chain')
                .removeClass('text-success')
                .addClass('fa fa-chain-broken')
                .addClass('text-danger');
        },
        dropRows: function () {
            $(MonitoringUO.table).find('tbody').find('tr').remove();
            MonitoringUO.setSummaryUsers([]);
        },
        sortTd: function (data) {
            return {
                user_id: data.user_id,
                username: data.username,
                sip_login: typeof data.user_sip !== 'undefined' ? data.user_sip.sip_login : '',
                user_ready: data.user_ready,
                type_call: data.type_call,
                use_messenger: data.use_messenger,
                status_zoiper: data.status_zoiper,
                queue_paused: data.queue_paused,
                status_call: data.status_call,
                order: data.order,
                user_country: data.user_country,
                action_time: data.last_activity,
                last_activity: data.last_activity,
                uniqueid: data.uniqueid,
                ip: data.ip,
                //type_memory: data.type_memory
            };
        },
        checkIsOpened: function (uniqueid) {
            return MonitoringUO.openedUserCountryList.indexOf(uniqueid) !== -1;
        },
        eventDropDownListCountry: function () {
            $(MonitoringUO.dropdownCountryList).on('click', function () {
                var uniqueid = $(this).closest('tr').attr('data-uniqueid');

                if ($(this).attr('data-close') == 1) {
                    MonitoringUO.showCountry($(this));

                    if (!MonitoringUO.checkIsOpened($(this))) {
                        MonitoringUO.openedUserCountryList.push($(this).closest('tr').attr('data-uniqueid'));
                    }
                    //console.log('MonitoringUO.openedUserCountryList', MonitoringUO.openedUserCountryList);
                } else {
                    MonitoringUO.hideCountry($(this));
                    MonitoringUO.openedUserCountryList.splice(MonitoringUO.openedUserCountryList.indexOf(uniqueid), 1);
                }
            });
        },
        showCountry: function (block) {
            $(block).attr('data-close', 0);
            $(block).html($(block).attr('data-full'));
            $(block).removeClass('dropdown-list-country');
        },
        hideCountry: function (block) {
            $(block).attr('data-close', 1);
            $(block).html($(block).attr('data-short'));
            $(block).addClass('dropdown-list-country');
        },
        formatUserCountry: function (data, uniqueid) {
            var list_country = '';

            for (var k in data) {
                list_country += '<span title=\'' + k + '. ' + data[k] + '\'>' + data[k] + '</span><br/>';
            }
            var size = Object.keys(data).length;
            var close = MonitoringUO.checkIsOpened(uniqueid) ? 0 : 1;
            var dropDownClass = (close == 0) ? '' : 'dropdown-list-country';

            var dataShort = size + ' countries';

            if (size > 1 && close === 1) {
                return '<div class="ckl_block ' + dropDownClass + '" data-close="' + close + '" data-full="' + list_country + '" data-short="' + dataShort + '">' + dataShort + '</div>';
            } else {
                return '<div class="ckl_block" data-close="0" data-full="' + list_country + '" data-short="' + dataShort + '">' + list_country + '</div>';
            }
        },
        formatLastActivity: function (timestamp) {
            var date = new Date(timestamp);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var seconds = date.getSeconds();
            var day = date.getDate();
            var month = date.getMonth();
            var year = date.getFullYear();

            if (day < 10) {
                day = '0' + day;
            }

            if (month < 10) {
                month = '0' + month;
            }

            if (minutes < 10) {
                minutes = '0' + minutes;
            }

            if (hours < 10) {
                hours = '0' + hours;
            }

            if (seconds < 10) {
                seconds = '0' + seconds;
            }

            return year + "/" + month + "/" + day + " " + hours + ":" + minutes + ":" + seconds;
        },
        formatActionTime: function (timestamp) {
            return '00:00:00';
        },
        formatUsername: function (data) {
            return '<span class="text-uppercase">' + data + '</span>';
        },
        formatRedGreen: function (data) {
            return '<span class="dot ' + (data ? 'dot-green' : 'dot-red') + '"></span>';
        },
        formatQueuePaused: function (data) {
            if (data === null) {
                return '<span class="text-muted">no data</span>';
            }

            var title = data === MonitoringUO.QUEUE_PAUSED ? 'paused' : 'not paused';

            return '<span title="' + title + '" class="dot ' + (data === MonitoringUO.QUEUE_PAUSED ? 'dot-red' : 'dot-green') + '"></span>';
        },
        statusCollection: function () {
            var collection = {};

            collection[MonitoringUO.STATUS_NEW_CODE] = MonitoringUO.STATUS_NEW;
            collection[MonitoringUO.STATUS_RECALL_CODE] = MonitoringUO.STATUS_RECALL;
            collection[MonitoringUO.STATUS_NO_ANSWER_CODE] = MonitoringUO.STATUS_NO_ANSWER;
            collection[MonitoringUO.STATUS_APPROVE_CODE] = MonitoringUO.STATUS_APPROVE;
            collection[MonitoringUO.STATUS_REJECT_CODE] = MonitoringUO.STATUS_REJECT;
            collection[MonitoringUO.STATUS_TRASH_CODE] = MonitoringUO.STATUS_TRASH;
            collection[MonitoringUO.STATUS_DOUBLE_CODE] = MonitoringUO.STATUS_DOUBLE;

            return collection;
        },
        mapStatus: function (string_state_status) {
            var temp = string_state_status.split(/: /);

            if (isUndefined(temp[1])) {
                return string_state_status;
            } else {
                var statusCollection = MonitoringUO.statusCollection();

                if (isUndefined(statusCollection[temp[1]])) {
                    return string_state_status;
                } else {
                    return temp[0] + ': ' + statusCollection[temp[1]];
                }
            }
        },
        formatOrder: function (data, uniqueid) {
            var patterns = {
                EXCEPTION: 'Open status: status_open',
                SEARCH: 'searching',
                OPEN: /Open status/,
                SAVE: /Save status/,
                CLOSE: /Close status/,
                ERROR: /error/
            };

            if (data != patterns.EXCEPTION) {
                var action = data;
                var labelClass = '';
                var title = '';

                if (data === null || data === patterns.SEARCH) {
                    action = 'Search';
                    labelClass = 'label-info';
                } else if (typeof data === 'number') {
                    labelClass = 'label-success';
                } else if (patterns.OPEN.test(data)) {
                    action = MonitoringUO.mapStatus(data);
                    title = action;
                    labelClass = 'label-warning';
                } else if (patterns.SAVE.test(data)) {
                    action = MonitoringUO.mapStatus(data);
                    title = action;
                    labelClass = 'label-primary';
                } else if (patterns.ERROR.test(data)) {
                    labelClass = 'label-danger';
                    action = 'error';
                    title = data;
                } else if (patterns.CLOSE.test(data)) {
                    labelClass = 'label-success';
                    title = data.replace('Close status: ', '');
                    action = title;

                    //+ переписать на order_id data_value
                    $(MonitoringUO.table).find('tr[data-uniqueid="' + uniqueid + '"]').find('td[data-type="' + MonitoringUO.DATA_TYPE_ORDER + '"]').attr('data-value', action);
                }

            if (data === null || data === 'searching') {
                action = 'Search';
                labelClass = 'label-info';
            } else if (typeof data === 'number') {
                labelClass = 'label-success';
            } else if (/Open status/.test(data)) {
                labelClass = 'label-default';
            } else if (/Save status/.test(data)) {
                labelClass = 'label-primary';
            } else if (/error/.test(data)) {
                labelClass = 'label-danger';
                action = 'error';
                title = data;
            }

                return '<span title="' + title + '" class="label ' + labelClass + '">' + action + '</span>';
            } else {
                return '';
            }
        },
        formatStatusCall: function (data) {
            var action = '';
            var labelClass = '';

            switch (data) {
                case null:
                case 'reject':
                case 'hang':
                    action = 'State of rest';
                    labelClass = 'label-primary';
                    break;
                case 'ring':
                    action = 'Ring';
                    labelClass = 'label-danger';
                    break;
                case 'accept':
                    action = 'Accept';
                    labelClass = 'label-success';
                    break;
                case 'hold':
                    action = 'Hold';
                    labelClass = 'label-warning';
                    break;
                case 'unhold':
                    action = 'Unhold / return to accept';
                    labelClass = 'label-success';
                    break;
                case 'incoming':
                    action = 'Incoming';
                    labelClass = 'label-info';
                    break;
            }

            return '<span class="label ' + labelClass + '">' + action + '</span>';
        },
        formatTypeCall: function (data) {
            var action = '';
            var labelClass = '';

            switch (data) {
                case null:
                case 'default':
                case 'hang':
                    action = 'Default';
                    labelClass = 'label-success';
                    break;
                case 'auto call':
                    action = 'Auto call';
                    labelClass = 'label-warning';
                    break;
                case 'incoming':
                    action = 'Incoming';
                    labelClass = 'label-info';
                    break;
            }

            return '<span class="label ' + labelClass + '">' + action + '</span>';
        },
        formatStatusZoiper: function (data) {
            var className = 'dot-red';
            var action = 'Initialization';

            switch (data) {
                case 'register':
                    className = 'dot-yellow';
                    action = 'Try to register';
                    break;
                case 'registered':
                    className = 'dot-green';
                    action = 'Registered success';
                    break;
                case 'failed_register':
                    className = 'dot-red';
                    action = 'Register failed';
                    break;
                case 'unregister':
                    className = 'dot-red';
                    action = 'Unregistered';
                    break;
            }
            return '<span class="dot ' + className + '"></span>&nbsp;&nbsp;' + action;
        },
        formatTypeMemory: function (data) {
            var className = 'fa fa-file-o';

            if (data === MonitoringUO.TYPE_MEMORY_MEMORY) {
                className = 'fa fa-floppy-o';
            } else if (data === MonitoringUO.TYPE_MEMORY_MONGODB) {
                className = 'fa fa-table';
            }

            return '<i class="' + className + '"></i>';
        },
        formatIp: function (data) {
            return '<code>' + data + '</code>';
        },
        getCountRows: function () {
            return $(MonitoringUO.table).find('tbody').find('tr').length;
        },
        setSummaryUsers: function (data) {
            $(MonitoringUO.summary_users).text(data.length);
        },
        findRow: function (uniqueid) {
            var rowFound = $(MonitoringUO.table).find('tbody').find('tr[data-uniqueid="' + uniqueid + '"]');

            if ($(rowFound).hasClass('row_table')) {
                return $(rowFound);
            } else {
                return false;
            }
        },
        removeRow: function (uniqueid) {
            var rowFound = MonitoringUO.findRow(uniqueid);

            if (rowFound) {
                $(rowFound).remove();
            }
            $(MonitoringUO.summary_users).text(MonitoringUO.getCountRows());
        },
        getTime: function () {
            return (new Date).getTime();
        },
        renderBodyTable: function (data) {
            if (Object.keys(data).length === 0) {
                $(MonitoringUO.table).find('tbody').find('tr').remove();
            }

            for (var k in data) {
                if (!data[k].last_activity) {
                    data[k].last_activity = MonitoringUO.getTime();
                }

                if (MonitoringUO.findRow(data[k].uniqueid)) {
                    MonitoringUO.updateRow(data[k]);
                } else {
                    MonitoringUO.appendRow(data[k]);
                }
            }

            MonitoringUO.eventDropDownListCountry();

            MonitoringUO.setSummaryUsers(data);

            return true;
        },
        appendRow: function (data) {
            var row = MonitoringUO.renderRow(MonitoringUO.sortTd(data));
            if($(MonitoringUO.table).find('tbody').append(row)){

                var rowByField = MonitoringUO.findRow(data.uniqueid);
                var td_action_time = $(rowByField).find('td[data-type="' + MonitoringUO.DATA_TYPE_ACTION_TIME + '"]');

                if(DEBUG) {
                    console.log('APPEND');
                    console.log('контролируем счетчики при APPEND', MonitoringUO.isStartTimerOrderStateChange(td_action_time, data));
                }

                MonitoringUO.resetTimer(data);
            }
        },
        updateRow: function (data) {

            MonitoringUO.data_prev[uniqueid] = data[MonitoringUO.DATA_TYPE_ORDER];

            var row = MonitoringUO.findRow(data.uniqueid);
            var td_action_time = $(row).find('td[data-type="' + MonitoringUO.DATA_TYPE_ACTION_TIME + '"]');

            if (!row) {
                MonitoringUO.appendRow(data);
            } else {
                $(row).find('td').each(function () {

                    var type = $(this).attr('data-type');

                    if (type === MonitoringUO.DATA_TYPE_ORDER) {
                        if(DEBUG) {
                            console.log('контролируем счетчики при UPDATE', MonitoringUO.isStartTimerOrderStateChange(td_action_time, data));
                        }
                        MonitoringUO.resetTimer(data);
                    }

                    if (type === MonitoringUO.DATA_TYPE_USER_COUNTRY) {
                        $(this).attr('data-value', JSON.stringify(data[type]))
                    } else {
                        $(this).attr('data-value', data[type]);
                    }

                    if (type !== MonitoringUO.DATA_TYPE_UNIQUEID) {
                        switch (type) {
                            case MonitoringUO.DATA_TYPE_USER_READY:
                            case MonitoringUO.DATA_TYPE_USE_MESSENGER:
                                $(this).html(MonitoringUO.formatRedGreen(data[type]));
                                break;
                            case MonitoringUO.DATA_TYPE_USER_COUNTRY:
                                $(this).html(MonitoringUO.formatUserCountry(data[type], data.uniqueid));
                                break;
                            case MonitoringUO.DATA_TYPE_LAST_ACTIVITY:
                                $(this).html(MonitoringUO.formatLastActivity(data[type]));
                                break;
                            //если произошёл апдейт этого поля - сбросим счетчик
                            case MonitoringUO.DATA_TYPE_ACTION_TIME:
                                break;
                            case MonitoringUO.DATA_TYPE_USER_ID:
                                $(this).html(data[type]);
                                break;
                            case MonitoringUO.DATA_TYPE_USERNAME:
                                $(this).html(MonitoringUO.formatUsername(data[type]));
                                break;
                            case MonitoringUO.DATA_TYPE_STATUS_ZOIPER:
                                $(this).html(MonitoringUO.formatStatusZoiper(data[type]));
                                break;
                            case MonitoringUO.DATA_TYPE_STATUS_CALL:
                                $(this).html(MonitoringUO.formatStatusCall(data[type]));
                                break;
                            case MonitoringUO.DATA_TYPE_ORDER:
                                $(this).html(MonitoringUO.formatOrder(data[type], data.uniqueid));
                                break;
                            case MonitoringUO.DATA_TYPE_QUEUE_PAUSED:
                                $(this).html(MonitoringUO.formatQueuePaused(data[type]));
                                break;
                            case MonitoringUO.DATA_TYPE_CALL:
                                $(this).html(MonitoringUO.formatTypeCall(data[type]));
                                break;
                            default:
                                $(this).html(data[type]);
                                break;
                        }
                    }
                });
            }
        },
        renderRow: function (data) {
            var row, rowFound;

            if (rowFound = MonitoringUO.findRow(data.uniqueid)) {
                row = rowFound;
                $(row).find('td').remove();
            } else {
                row = $('<tr class="row_table"/>');
            }

            for (var k in data) {
                if (k === MonitoringUO.DATA_TYPE_UNIQUEID) {
                    continue;
                }

                switch (k) {
                    case MonitoringUO.DATA_TYPE_USER_READY:
                    case MonitoringUO.DATA_TYPE_USE_MESSENGER:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '"  class="text-center">' + MonitoringUO.formatRedGreen(data[k]) + '</td>');
                        break;
                    case MonitoringUO.DATA_TYPE_USER_COUNTRY:
                        $(row).append('<td data-type="' + k + '" data-value=\'' + JSON.stringify(data[k]) + '\' >' + MonitoringUO.formatUserCountry(data[k], data.uniqueid) + '</td>');
                        break;
                    case MonitoringUO.DATA_TYPE_LAST_ACTIVITY:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '"  class="text-center">' + MonitoringUO.formatLastActivity(data[k]) + '</td>');
                        break;
                    case MonitoringUO.DATA_TYPE_ACTION_TIME:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '"  class="text-center">' + MonitoringUO.formatActionTime(data[k]) + '</td>');
                        break;
                    case MonitoringUO.DATA_TYPE_USER_ID:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '"  title="' + data.uniqueid + '">' + data[k] + '</td>');
                        break;
                    case MonitoringUO.DATA_TYPE_USERNAME:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '"  class="text-center"><b>' + MonitoringUO.formatUsername(data[k]) + '</b></td>');
                        break;
                    case MonitoringUO.DATA_TYPE_STATUS_ZOIPER:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '" >' + MonitoringUO.formatStatusZoiper(data[k]) + '</td>');
                        break;
                    case MonitoringUO.DATA_TYPE_IP:
                        $(row).append('<td data-type="' + MonitoringUO.PSEUDO_TYPE_IP + '" data-value="' + data[k] + '" >' + MonitoringUO.formatIp(data[k]) + '</td>');
                        break;
                    case MonitoringUO.DATA_TYPE_STATUS_CALL:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '"  class="text-center">' + MonitoringUO.formatStatusCall(data[k]) + '</td>');
                        break;
                    case MonitoringUO.DATA_TYPE_ORDER:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '"  class="text-center"><b>' + MonitoringUO.formatOrder(data[k], data.uniqueid) + '</b></td>');
                        break;
                    case MonitoringUO.DATA_TYPE_QUEUE_PAUSED:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '"  class="text-center">' + MonitoringUO.formatQueuePaused(data[k]) + '</td>');
                        break;
                    case MonitoringUO.DATA_TYPE_CALL:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '"  class="text-center">' + MonitoringUO.formatTypeCall(data[k]) + '</td>');
                        break;
                    case MonitoringUO.DATA_TYPE_MEMORY:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '"  class="hidden text-center">' + MonitoringUO.formatTypeMemory(data[k]) + '</td>');
                        break;
                    default:
                        $(row).append('<td data-type="' + k + '" data-value="' + data[k] + '" class="text-center">' + data[k] + '</td>');
                        break;
                }
            }

            $(row).attr('data-uniqueid', data.uniqueid);
            $(row).find('td').addClass('align-middle');

            return row;
        }
    };

    try {
        MonitoringUO.init();
    } catch (e) {
        console.error('Exception: ', e);
    }
});
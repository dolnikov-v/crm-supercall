$(document).ready(function(){
    BaseLogger = {
        tags : $('.tags'),
        firstRow : $('.tags').find('.row').first(),
        init : function(){
            if($('.tags').find('.row').length == 1){
                this.firstRow.find('.remove-filters').hide();
            }else{
                this.tags.find('.remove-filters').show();
            }

        },
        add : function(e){
            var row = this.tags.find('.row').last().clone();
            row.find('.add-filters').on('click', function(){
                BaseLogger.add();
            });
            row.find('.remove-filters').on('click', function(){
                BaseLogger.remove($(this));
            });
            this.tags.append(row);
            this.init();
        },
        remove : function(elem){
            elem.closest('.row').remove()
            this.init(false);
        }
    }

    BaseLogger.init();

    $('.add-filters').on('click', function (e) {
        e.preventDefault();
        BaseLogger.add();
    });

    $('.remove-filters').on('click', function (e) {
        e.preventDefault();
        BaseLogger.remove($(this))
    });
})

/**
 *
 * @param data {script, order_id, user_id}
 * @param error catched error
 * @constructor
 */
function JSLogger(data, error){
    var script = 'not indicated'
    var order_id = null;
    var user_id = null;
    var type = error.name;
    var message = error.message;
    var stack = error.stack;

    if(typeof data.script !='undefined'){
        script = data.script;
    }

    if(typeof data.order_id !='undefined'){
        order_id = data.order_id;
    }

    if(typeof data.user_id !='undefined'){
        user_id = data.user_id;
    }

    //console.error('Catched Exception on JS file');
    //console.log('-->', error);
    //console.log(type);
    //console.log(message);

    //нужно определить где мы - дев, мастер, локальный ?
    var domain = location.host.split(/\./)
    if (domain[1] == 'dev') {
        var host = location.protocol + '//' + 'panel.dev.2wcall.com'
    } else if (domain[1] == '2wcall') {
        var host = location.protocol + '//' + 'panel.2wcall.com/'
    } else {
        var host = location.protocol + '//panel.' + $("#document_domain").text();
    }

    $.ajax({
        method : 'get',
        url : host + '/administration/js-logger/save-log?callback=?',
        dataType : 'jsonp',
        jsonpCallback : 'logResults',
        data : {
            script : script,
            order_id : order_id,
            user_id : user_id,
            type : type,
            message: message,
            stack : stack
        },
    });
}

function logResults(json){
    console.log(json);
}
$(document).ready(function(){
    $(".media-target").on('click', function(){

        $.ajax({
            method: 'post',
            data : {
                media_id : $(this).data('media')
            },
            url : '/profile/control/delete',
            complete : function(data){
                var result = JSON.parse(data.responseText);

                if(result.success === false){
                    toastr.error(result.message);

                }else{
                    toastr.success(result.message);
                    $('.media-' + result.id).remove();
                }

            }
        });

        return false;
    });
});
$(function () {
    var Downloads = {
        init: function () {
            $('a[href="#downloads"]').closest('li').on('click', function (e) {
                e.preventDefault();

                $('*[data-action="delete"]').on('click', function () {
                    $('a[href="#downloads"]').attr('data-tr', $(this).attr('data-id'));

                    Downloads.sentAction('/profile/downloads/delete', {id: $(this).attr('data-id')}, function () {
                        var parent = $('[data-parent_id="' + $('a[href="#downloads"]').attr('data-tr') + '"]');
                        parent.remove();
                    })
                });

            });
        },
        sentAction: function (url, params, callback) {
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: params,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        callback();
                    }
                },
                error: function (error) {
                    toastr.error(error.responseText);
                }
            });
        }
    };

    Downloads.init();
});
$(document).ready(function () {
    $('#presets-auto').on('click', function() {
      if ($(this).prop('checked')) {
        $('#presets-name').attr('disabled', 'disabled')
      }
      else
        $('#presets-name').removeAttr('disabled')
    });
});

/**
 * Add new raw attempts in the form Queue
 * @param numb
 */
function addAttempts(numb)
{
    $.post("/presets/control/get-attempts", {'numb': numb})
        .success(function(response){
            if (response.result === 'ok') {
                $('#block-attempts .copy-attempts').append(response.data)
            }
        })
        .error(function(response){

        });

    count = Number(numb)
    count++
    $('.btn-add-attempts').attr('onclick', 'addAttempts("' + count + '")');
}

/**
 * init select2-plugin
 */
function initSelect2()
{
  $(".select2-form").select2();
}

/**
 * Удалить строку
 * @param numb
 */
function removeRawStrategy(numb)
{
    $('.attempts-raw--' + numb).remove();
}
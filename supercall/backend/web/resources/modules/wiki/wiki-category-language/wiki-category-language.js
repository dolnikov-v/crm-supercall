$(document).ready(function () {

    var WikiCategoryTranslate = {
        plus: $(".add-translate"),
        minus: $(".remove-translate"),
        add: function () {

            $('.translate_messages').find("[name*=translate_language]").select2('destroy');

            var row = $('.translate_messages').find('.translate_message').last().clone();
            var id = row.find("[name*=translate_language]").prop('id').replace(/_\d/, '');

            row.find("[name*=translate_language]").prop('id', id + '_' + $("[name*=translate_language]").length);

            row.find('.add-translate').on('click', function () {
                add($(this));
            });


            row.find('.remove-translate').on('click', function () {
                WikiCategoryTranslate.remove($(this).closest('.translate_message'));
                addToSelect($(this));
            });

            row.appendTo($('.translate_messages'));

            $('.translate_messages').last().find("[name*=translate_message]").last().val('');

            $('.translate_messages').find("[name*=translate_language]").select2();
            $('.translate_messages').find("[name*=translate_language]").last().val('');
            $('.translate_messages').find("[name*=translate_language]").last().on('change', function () {
                setData();
            });

            $('.translate_messages').find("[name*=translate_language]").last().trigger('change');
        },
        remove: function (elem) {
            elem.remove();
        },
        init: function () {
            this.plus.hide();
            this.plus.last().show();

            this.minus.show();
            this.minus.last().hide();

            setData();
        }
    }

    WikiCategoryTranslate.init();

    $("[name*=translate_language]").on('change', function () {
        setData();
    });

    $(".add-translate").map(function () {
        $(this).on('click', function () {
            add($(this));
        });
    });

    $(".remove-translate").map(function () {
        $(this).on('click', function () {
            WikiCategoryTranslate.remove($(this).closest('.translate_message'));
            addToSelect($(this))
        });
    });

    function add(elem) {
        WikiCategoryTranslate.add();
        elem.hide();
        elem.closest('.translate_message').find('.remove-translate').show();
    }

    function addToSelect(button) {
        var val = button.attr('data-val');
        var text = button.attr('data-text');

        $("[name*=translate_language]").map(function () {
            var option = $("<option/>", {value: val, text: text});
            $(this).append(option);
        });
    }

    function setData() {
        var vals = [];
        var i = 0;

        $("[name*=translate_language]").map(function () {

            i++;
            $(this).prop('id', $(this).prop('id').replace(/_\d/, '') + '_' + i);

            var buttons = $(this).closest('.translate_message').find('button');
            var val = $(this).val();
            var text = $('#' + $(this).prop('id') + ' option:selected').text();

            buttons.map(function () {
                $(this).attr('data-val', val);
                $(this).attr('data-text', text);
            });

            vals.push(val);
        });

        //clear duplicate
        $("[name*=translate_language]").map(function () {
            for (var k in vals) {
                if ($(this).val() != vals[k]) {
                    $('#' + $(this).prop('id') + ' option[value="' + vals[k] + '"]').remove();
                }
            }
        });
    }


    function validate() {
        var rows = $('.translate_message');
        var no_errors = true;

        rows.map(function () {
            var title = $(this).find('[name*=translate_message]').val();
            var id = $(this).find('[name*=translate_language]').prop('id');
            var language = $('#' + id + ' option:selected').text();

            if (title != '' && language == '-') {
                no_errors = false;
                $(this).find('[name*=translate_message]').closest('div').addClass('has-error');
                $(this).find('.select2-selection--single').css('border', '1px solid #D62C1A');
            } else {
                $(this).find('[name*=translate_message]').closest('div').removeClass('has-error');
                $(this).find('.select2-selection--single').css('border', '1px solid #E4E5E7');
            }
        });

        return no_errors;
    }

    $('[type="submit"]').on('click', function (e) {
        e.preventDefault();

        if (!validate()) {
            toastr.error('No translation language for title')
            return false;
        } else {
            $('form').submit();
        }
    });
});
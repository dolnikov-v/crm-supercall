$(document).ready(function () {

    $('.add-price').hide();
    //отложил пока. там мутки с select2 - он требует оы
    PriceShipping = {
        shipping_group: $('.shipping_groups'),
        firstRow: $('.shipping_groups').find('.shipping_group').first(),
        init: function () {
            if ($('.shipping_groups').find('.shipping_group').length == 1) {
                this.firstRow.find('.remove-price').hide();
            } else {
                this.shipping_group.find('.remove-price').show();
            }

        },
        add: function (e) {
            var row = this.shipping_group.find('.shipping_group').last().clone();
            row.find('.add-price').on('click', function () {
                PriceShipping.add();
            });
            row.find('.remove-price').on('click', function () {
                PriceShipping.remove($(this));
            });
            this.shipping_group.append(row);
            this.init();
        },
        remove: function (elem) {
            elem.closest('.row').remove()
            this.init(false);
        }
    }

    PriceShipping.init();

    $('.add-price').on('click', function (e) {
        e.preventDefault();
        PriceShipping.add();
    });

    $('.remove-price').on('click', function (e) {
        e.preventDefault();
        PriceShipping.remove($(this))
    });


    if (typeof $('[name*=country_id]').attr('rel') == 'undefined') {
        $('[name*=country_id]').attr('rel', 0)
    }

    //подгрузка стран по партнёру
    $('[name*=partner_id]').on('change', function () {

        $('[name*=country_id]').prop('disabled', true);

        $.ajax({
            url: '/catalog/price-shipping/get-partner-countries',
            type: 'post',
            data: {
                partner_id: $(this).val()
            },
            dataType: 'json',
            success: function (data) {
                $('[name*=country_id]').prop('disabled', false);

                addSelectedEmptiOption();
                for (id in data) {
                    $('[name*=country_id]').append(new Option(data[id], id));
                }
            },
            error: function (error) {
                toastr.error(error.responseText);
            }
        });
    });

    //подгрузка атрибутов is_klader формы по partner_id и country_id
    $('[name*=country_id]').on('change', function () {

        if($(this).prop('id') == 'priceshippingsearch-country_id'){
            return;
        }

        var index = $(this).attr('rel');

        if ($(this).val() != '' && $('[name*=partner_id]').val() != '') {
            $.ajax({
                url: '/catalog/price-shipping/get-form-attributes',
                type: 'post',
                data: {
                    partner_id: $('[name*=partner_id]').val(),
                    country_id: $(this).val()
                },
                dataType: 'json',
                beforeSend: function () {
                    var spinner = '<img width="55px" src="/resources/images/common/spinner.gif">';
                    $('.place_attributes').html(spinner)
                },
                success: function (data) {
                    $('.place_attributes').html('');

                    if (data.length == 0) {
                        toastr.error('Form attributes with the is_klader parameter not found');
                    } else {
                        var tpl_param = $('.tpl_field').html();

                        for (var key in data) {
                            var param = tpl_param
                                .replace(/_label_name_/g, data[key].label + ' (' +data[key].name + ')')
                                .replace(/_name_/g, data[key].name)
                                .replace(/_index_/g, index);

                            $(param).appendTo($('.place_attributes'));

                            $('[name*='+data[key].name+']').attr('data-sort', data[key].sort);

                            searchOnKlader(data[key].name, data[key].id, index);
                        }

                        //если edit - восстановить param
                        //данные по параметрам зашиты в name=price_shipping_data - поле доступно только при редактировании

                        if (typeof $('[name*=price_shipping_data]').val() != 'undefined') {
                            var param = JSON.parse($('[name*=price_shipping_data]').val());

                            if (Object.keys(param).length > 0) {
                                for (var k in param) {
                                    $('[name*=' + param[k].name + ']').val(param[k].val);
                                }
                            }
                        }

                    }
                    //this is magic
                },
                error: function (error) {
                    toastr.error(error.responseText);
                }
            });

        }
    });


    //обновить список партнёрских курьерок
    $('[name*=country_id]').on('change', function () {
        getShippings()
    });

    function searchOnKlader(name, id, index) {
        //search in klader
        ///catalog/klader-countries/get-data
        var spinner = '<img width="35px" src="/resources/images/common/spinner.gif">';

        $('[name*=' + name + ']').on('keyup', function () {
            $('#autocomplete' + name).show();
            var rect = $('#autocomplete' + name).position();

            $('#autocomplete' + name).width(parseInt($('.place_attributes').width()) - 13 + 'px');

            $.ajax({
                url: '/catalog/klader-countries/get-data',
                type: 'get',
                data: {
                    search: $('[name="PriceShipping[param_shipping][' + index + '][' + name + ']"]').val(),
                    partner_form_attribute_id: id.toString().replace(/_/,''),
                    form: 'price_shipping'
                },
                beforeSend: function () {
                    //подменить имена - чтобы params -пришли отдельным массивом
                    $('#autocomplete' + name).html(spinner);
                },
                success: function (data) {
                    var res = JSON.parse(data);
                    var res_blocks = '';

                    if (typeof res.results != 'undefined') {

                        for (var key in res.results) {
                            res_blocks += '<div class="res_block" rel="' + name + '">' + res.results[key].text + '</div>';
                        }
                    }
                    $('#autocomplete' + name).html(res_blocks);

                    //постанова autocomplete
                    $('.res_block').on('click', function () {
                        $('[name="PriceShipping[param_shipping][' + index + '][' + $(this).attr('rel') + ']"]').val($(this).html());
                        $('#autocomplete' + name).hide();
                    });
                }
            });


        });
    }

    function getShippings() {
        //подгрузка партнёрских курьерок по стране и партнёру
        $.ajax({
            url: '/catalog/price-shipping/get-partner-shipping',
            type: 'post',
            data: {
                partner_id: $('[name*=partner_id]').val(),
                country_id: $('[name*=country_id]').val()
            },
            dataType: 'json',
            success: function (data) {
                var prev_val = $('[name*=shipping_id]').val();

                addSelectedPartnerEmptiOption();
                for (id in data) {
                    $('[name*=shipping_id]').append(new Option(data[id], id));
                }

                $('[name*=shipping_id]').val(prev_val);
                $('[name*=shipping_id]').select2();
            },
            error: function (error) {
                toastr.error(error.responseText);
            }
        });
    }


    function addSelectedEmptiOption() {
        $('[name*=country_id]').empty();
        $('[name*=country_id]').append(new Option('-', '')).val('');
        $('[name*=country_id]').select2();
    }

    //если edit
    if ($('[name*=country_id]').val() != '') {
        $('[name*=country_id]').trigger('change');
    }

    function addSelectedPartnerEmptiOption() {
        $('[name*=shipping_id]').empty();
        $('[name*=shipping_id]').append(new Option('-', '')).val('');
        $('[name*=shipping_id]').select2();
    }

});
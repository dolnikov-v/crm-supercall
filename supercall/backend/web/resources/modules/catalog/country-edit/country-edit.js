$(document).ready(function () {

    $('.validate-type input[data-type-checkbox="switchery"]').on('switchChange.bootstrapSwitch', function () {
        $('#validateType').val($(this).prop('checked')?1:0);
    });


    $('.check-phone input[data-type-checkbox="switchery"]').on('switchChange.bootstrapSwitch', function () {
        $('#checkPhone').val($(this).prop('checked')?1:0);
    });

});

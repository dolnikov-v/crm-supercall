$(function () {
    var Source = {
        rows: '.partner-countries',
        row: '.partner-countries-item',
        btnAdd: '.add-partner-countries',
        btnRemove: '.remove-partner-countries',
        init: function () {
            Source.eventRemove();
            Source.eventAdd();
            Source.manageButtons();
        },
        eventAdd: function () {
            $(Source.btnAdd).on('click', function (e) {
                e.stopImmediatePropagation();
                return Source.add();
            });
        },
        eventRemove: function () {
            $(Source.btnRemove).on('click', function () {
                return Source.remove($(this));
            });
        },
        add: function () {
            var clone = $(Source.rows).find(Source.row).first().clone();
            var timestamp = Date.now();

            $(Source.rows).append(clone);
            $(Source.rows).find(Source.row).last().find('span').remove();
            $(Source.rows).find(Source.row).last().find('select').select2();
            $(Source.rows).find(Source.row).last().find('span').removeAttr('style');
            $(Source.rows).find(Source.row).last().find('#sourcepartnercountry-partner_id').attr('name', "partner[" + timestamp + "]");
            $(Source.rows).find(Source.row).last().find('#sourcepartnercountry-country_id').attr('name', "country[" + timestamp + "][]");

            Source.eventAdd();
            Source.eventRemove();
            Source.manageButtons();
        },
        remove: function (btn) {
            $(btn).closest(Source.row).remove();
        },
        manageButtons: function () {
            if ($(Source.row).length === 1) {
                $(Source.btnRemove).first().hide();
            } else {
                $(Source.btnRemove).show();
                $(Source.btnRemove).last().hide();
                $(Source.btnAdd).hide();
                $(Source.btnAdd).last().show();
            }
        },
    };

    Source.init();
});

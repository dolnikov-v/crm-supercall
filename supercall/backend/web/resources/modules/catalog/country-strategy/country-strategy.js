$(function(){
    var CountryStrategy = {
        status: '[name*=status]',
        queue: '[name*=queue]',
        init: function(){
            $(CountryStrategy.status).on('change', CountryStrategy.eventStatus);
            $(CountryStrategy.queue).on('change', CountryStrategy.eventQueue);
        },
        eventStatus: function(){
            if($(this).val() != ''){
                $(CountryStrategy.queue).val('');
                $(CountryStrategy.queue).trigger('change');
            }
        },
        eventQueue: function(){
            if($(this).val() != ''){
                $(CountryStrategy.status).val('');
                $(CountryStrategy.status).trigger('change');
            }
        }
    };

    CountryStrategy.init();
});

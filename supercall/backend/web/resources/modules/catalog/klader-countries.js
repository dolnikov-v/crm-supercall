$(document).ready(function () {
    getFormAttributes();

    //страница со списком стран
    if($.urlParam('country_id') == null) {
        $("#beforeload").hide();
        $("#form_klader").show();
        $(".content").css("cursor", "default");
    }else {
        //кладр страны
        $("#form_klader").hide();
        $("#beforeload").show();
        $(".content").css("cursor", "wait");

        $("#kladercountries-country_id").change(function () {
            location.href = "/catalog/klader-countries/index?country_id=" + $(this).val();
        });

        $("#partner_form_id").change(function () {
            location.href = "/catalog/klader-countries/index?country_id=" + $("#kladercountries-country_id").val() + "&partner_form_id=" + $(this).val();
        })
    }
});

function getFormAttributes(){
    var country_id = $.urlParam('country_id');

    $(".content").prop('disabled', true);

    if(country_id != null) {
        $.ajax({
            url: '/partner/form/get-form-attributes',
            type: 'POST',
            data: {
                partner_form_id: $("#partner_form_id").val(),
                _csrf: yii.getCsrfToken()
            },
            success: function (data) {
                var answer = JSON.parse(data);

                $("#beforeload").hide();
                $("#form_klader").show();
                $(".content").css("cursor", "default");

                if (answer.result == 'fail') {
                    $(".animate-panel").prepend('<div id="error" class="col-lg-12 alert alert-danger" role="alert"> ' + answer.error + '</div><p class="error">&nbsp;</p>')
                    $(".btn-success").prop("disabled", true);
                } else {
                    $("#error, .error").remove();
                    $(".btn-success").prop("disabled", false);

                    var path = $.urlParam('path');

                    if (path == null) {
                        $("label[for='kladercountries-name']").html(answer.data[0].name)
                        $("#kladercountries-name").prop("placeholder", answer.data[0].name)
                        $("#partner_form_attribute_id").val(answer.data[0].id);
                    }else{
                        var level = path.split('%2F').lastIndexOf(path.split('%2F').pop());

                        if(typeof data[level] == 'undefined'){
                            $(".field-kladercountries-name").hide();
                            $(".btn-success").hide();
                        }else {
                            $("label[for='kladercountries-name']").html(answer.data[level].name)
                            $("#kladercountries-name").prop("placeholder", answer.data[level].name)
                            $("#partner_form_attribute_id").val(answer.data[level].id);
                        }
                    }
                }
            }
        });
    }
}
//функция вернёт GET параметры в читаемом привычном виде - пары: ключ=значение
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return decodeURI(results[1]) || 0;
    }
}
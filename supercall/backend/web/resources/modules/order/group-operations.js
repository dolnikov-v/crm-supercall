$(document).ready(function () {
    OrderGroupOperations = {
        finalQueue_id: null,
        operation: null,
        buttons: $(".group-operations").find("button.btn"),
        init: function () {
            this.buttons.map(function () {
                $(this).on('click', function () {
                    resetProgressBar();

                    var url = $(this).data('url').toString();
                    var action = $(this).data('action').toString();

                    //call
                    if (typeof OrderGroupOperations.callbacks[action] === 'function') {
                        if (getSelections().length > 0) {
                            $('.final-operaton-text').html('');
                            OrderGroupOperations.callbacks[action](url);
                            OrderGroupOperations.operation = action;
                        } else {
                            $("#modal_warning_group_operations").modal('show');
                        }
                    } else {
                        console.error(action + ' is not a function');
                    }

                });
            });
        },
        //обработчики кнопок групповых операций
        callbacks: {
            ChangeStatus: function (url) {
                $("#modal_change_status").modal('show');

                $("#modal_change_status_ok").on('click', function (e) {
                    confirmOpen(url, {status: $("[name=status]").val()});
                });
            },
            SentToSqs: function (url) {
                confirmOpen(url, {status: $("[name=status]").val()});
            },
            ChangeQueue: function (url) {
                $("[name*=cq_country_id]").val($("[name*=countries]").val());
                var btn_ok = $("#modal_change_queue_ok");
                var modal = $('#modal_change_queue');
                var queue = $("[name=cq_queue_id]");
                var country = $("[name=cq_country_id]");
                var option = {
                    dropdownAutoWidth: true,
                    width: 'auto',
                    dropdownParent: modal
                };

                modal.find('.loader').show();
                queue.parent().addClass('hidden');

                country.on('change', function () {
                    modal.find('.loader').show();
                    queue.parent().addClass('hidden');
                    OrderGroupOperations.getQueuesByCountry(url, $(this));
                });

                queue.on('change', function () {
                    btn_ok.on('click', function (e) {
                        modal.modal('hide');
                        OrderGroupOperations.finalQueue_id = $("[name=cq_queue_id]").val();
                        confirmOpen(url, {queue_id: queue.val(), country_id: country.val()});
                    });
                });

                modal.modal('show');
                country.select2(option);
                queue.select2(option);

                btn_ok.prop('disabled', true);

                OrderGroupOperations.getQueuesByCountry(url, country);
            },
            ChangeCostProducts: function (url) {
                var modal = $('#modal_change_cost_products');
                var btn_ok = $("#modal_change_cost_products_ok");
                var no_price = $(".error-no-price");
                var url_get_product = '/order/group-operations/get-products';

                modal.modal('show');
                OrderGroupOperations.getProductsByOrder(url_get_product, modal);

                btn_ok.on('click', function (e) {
                    var form_products = {};

                    $('.product').each(function (i) {
                        var productPrice = $(this).find('.product-price').val();

                        if(productPrice !== "" && productPrice > 0){
                            form_products[i] = {
                                'product_id': $(this).attr('product-id'),
                                'price': productPrice,
                                'promo': $(this).attr('is-promo')
                            };
                        }
                    });

                    if(Object.keys(form_products).length > 0){
                        no_price.css('display', 'none');
                        confirmOpen(url, {orderIds: getSelections(), form_products: form_products});
                    }else {
                        no_price.css('display', 'block');
                    }
                });
            },
            sentToCheckDemand: function(url){
                confirmOpen(url, {});
            },
            resendToCall: function (url) {
                var modal = $('#modal_resend_to_call');
                var btn_ok = $("#modal_resend_to_call_ok");
                var queue = $("[name=primary_queue_id]");
                var option = {
                    dropdownAutoWidth: true,
                    width: 'auto',
                    dropdownParent: modal
                };

                modal.find('.loader').show();
                queue.parent().addClass('hidden');
                queue.select2(option);
                modal.modal('show');

                queue.on('change', function () {
                    btn_ok.on('click', function (e) {
                        modal.modal('hide');
                        OrderGroupOperations.primaryQueue_id = $("[name=primary_queue_id]").val();
                        confirmOpen(url, {queue_id: queue.val()});
                    });
                });

                OrderGroupOperations.getPrimaryQueues(url);
            }
        },
        finalCallbacks: {
            ChangeQueue: function () {
                $('.final-operaton-text').html('<span class="alert alert-info"><span class="glyphicon glyphicon-refresh"></span> The process of rebuilding the queues Rabbit MQ is launched. You can close this window</span>');

                $.ajax({
                    url: '/order/group-operations/re-assemble-rabbit-queues',
                    type: 'post',
                    data: {
                        is_not_console: true,
                        queue_id: OrderGroupOperations.finalQueue_id
                    },
                    success: function (data) {
                        $('.final-operaton-text').html('<span class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> Queue, for selected orders has been changed and they are available for the call</span>');
                    },
                    error: function () {
                        $('.final-operaton-text').html('<span class="alert alert-warning"><span class="glyphicon glyphicon-remove"></span> Queue, for selected orders has been changed, but for the call they will be made a bit later</span>');
                    }
                });
            }
        },
        /**Вспомагательный метод для ChangeQueue*/
        getQueuesByCountry: function (url, country) {
            var btn_ok = $("#modal_change_queue_ok");
            var modal = $('#modal_change_queue');
            var queue = $("[name=cq_queue_id]");

            queue.val('').trigger('change');
            queue.find('option').remove();
            queue.prop('disabled', true);
            btn_ok.prop('disabled', true);

            $.ajax({
                url: '/order/group-operations/get-secondary-queues-by-country',
                type: 'post',
                data: {
                    country_id: country.val()
                },
                success: function (response) {
                    console.log('response', response);

                    var data = JSON.parse(response);

                    modal.find('.loader').hide();
                    modal.find("[name=cq_queue_id]").parent().removeClass('hidden');

                    for (var k in data) {
                        $(queue).append(new Option(data[k], k));
                    }
                    if (Object.keys(data).length > 0) {
                        btn_ok.prop('disabled', false);
                        $(queue).val(Object.keys(data)[0]).trigger('change');
                        queue.prop('disabled', false);
                        $("#modal_change_queue_ok").prop('disabled', false);
                    }

                    country.prop('disabled', false);

                    btn_ok.on('click', function (e) {
                        modal.modal('hide');
                        confirmOpen(url, {queue_id: queue.val(), country_id: country.val()});

                    });

                },
                error: function (response) {
                    var bar = $(".progress-bar");
                    bar.removeClass('progress-bar-success');
                    bar.addClass('progress-bar-danger');
                    $('#close_progress').prop('disabled', false);
                    toastr.error(response.responseText);
                }
            });
        },

        /**Вспомагательный метод для resendToCall*/
        getPrimaryQueues: function (url) {
            var btn_ok = $("#modal_resend_to_call_ok");
            var modal = $('#modal_resend_to_call');
            var queue = $("[name=primary_queue_id]");
            var country = $('[name*=countries]');
            var error = $('.queue_error_message');

            queue.val('').trigger('change');
            queue.find('option').remove();
            queue.prop('disabled', true);
            btn_ok.prop('disabled', true);

            $.ajax({
                url: '/order/group-operations/get-primary-queues',
                type: 'post',
                data: {country_id: country.val()},
                success: function (response) {
                    var data = JSON.parse(response);
                    modal.find('.loader').hide();

                    if(data.success != false){
                        modal.find("[name=primary_queue_id]").parent().removeClass('hidden');
                        error.text('');

                        for (var k in data) {
                            $(queue).append(new Option(data[k], k));
                        }
                        if (Object.keys(data).length > 0) {
                            btn_ok.prop('disabled', false);
                            $(queue).val(Object.keys(data)[0]).trigger('change');
                            queue.prop('disabled', false);
                            btn_ok.prop('disabled', false);
                        }

                        btn_ok.on('click', function (e) {
                            modal.modal('hide');
                            confirmOpen(url, {queue_id: queue.val()});
                        });
                    }else{
                        error.text(data.text);
                        toastr.error(data.text);
                    }
                },
                error: function (response) {
                    var bar = $(".progress-bar");
                    bar.removeClass('progress-bar-success');
                    bar.addClass('progress-bar-danger');
                    $('#close_progress').prop('disabled', false);
                    toastr.error(response.responseText);
                }
            });
        },

        /**Вспомогательный метод для ChangeCostProducts, получение групированого списка товаров*/
        getProductsByOrder: function (url, modal) {
            $.ajax({
                url: url,
                type: 'post',
                data: {orderIds: getSelections()},
                beforeSend: function () {
                    modal.find('.loader').show();
                },
                success: function (response) {
                    var result = JSON.parse(response);
                    $('.product-table-body').text('');
                    modal.find('.loader').hide();

                    $.each(result, function (key, value) {
                        var promo = value.promo ? '<b>(Промо)</b>' : '';

                        $('.product-table-body').append('<tr class="tr-vertical-align-middle product" product-id="' + value.product_id + '" is-promo="' + value.promo + '"></tr>');
                        $('.product:last').append('<td class="td-record-id product-name">' + value.name + ' ' + promo + '</td>');
                        $('.product:last').append('<td><input class="form-control product-price" min="1" type="number"></td>');
                    });
                }
            });
        }
    };

    OrderGroupOperations.init();
    DomReady();


    /**
     * Функционал, необхоимый при загрузке страницы
     */
    function DomReady() {
        $("#modal_confirm_group_operations_ok").on('click', function () {
            var modalConfirm = $("#modal_confirm_group_operations");
            var url = modalConfirm.data('url');
            var data = modalConfirm.data('data');

            sendRequest(url, data);
        });

        $('#close_progress').on('click', function () {
            if ($("#close_progress").closest('div.modal').find('.progress-bar').hasClass('progress-bar-success')) {
                //location.reload();
            }
        });

        $('.select-on-check-all').on('click', function () {
            var chbx = $("#orderList").find('input[name*=selection]');
            var all = $(this);

            chbx.map(function () {
                $(this).prop('checked', all.prop('checked'));
            });

        });
    }

    /**
     * @returns {Array}
     */
    function getSelections() {
        var chbx = $("#orderList").find('input[name*=selection]:checked');
        var ids = [];

        chbx.map(function () {
            if (!$(this).hasClass('select-on-check-all')) {
                ids.push($(this).val());
            }
        });

        return ids;
    }

    /**
     *
     * @param url {string}
     * @param data {object} for merge params with defaul selections
     */
    function sendRequest(url, data) {
        window.order_group_operations_count = 0;
        var selections = getSelections();
        var group = window.performance.now().toString().replace(/\./g, '');

        for (var k in selections) {
            var defaultData = {
                id: selections[k],
                group: group
            };

            showProgress();

            $.ajax({
                url: url,
                type: 'post',
                data: typeof data === 'object' ? Object.assign(defaultData, data) : defaultData,
                error: function (response) {
                    var bar = $(".progress-bar");
                    bar.removeClass('progress-bar-success');
                    bar.addClass('progress-bar-danger');

                    $('#close_progress').prop('disabled', false);
                    toastr.error(response.responseText);
                },
                success: function (response) {
                    checkAnswer(response, selections.length);
                }
            })
        }
    }

    /**
     * @param data {object} - доп параметры (например статус)
     * @param total  {integer} - count selections
     */
    function checkAnswer(data, total) {
        var response = JSON.parse(data);
        var chbx = $("input[type=checkbox][value=\"" + response.id + "\"]");

        window.order_group_operations_count++;

        processingBar(total, window.order_group_operations_count);

        if (response.success) {
            chbx.prop('checked', false);
            chbx.closest('tr').css('background-color', '#C8FFE6');
        } else {
            chbx.closest('tr').css('background-color', '#FFCECE');
            toastr.error(response.message);
            $(".group_errors").append('<br/>' + response.message);
            $('#close_progress').prop('disabled', false);
        }
    }

    /**
     * @param url  {string} yii server route
     * @param data {object} - доп параметры (например статус)
     */
    function confirmOpen(url, data) {
        var modalConfirm = $("#modal_confirm_group_operations");
        modalConfirm.addClass('hmodal-info');
        modalConfirm.data('url', url);
        modalConfirm.data('data', data);
        modalConfirm.modal('show');
    }

    function showProgress() {
        $(".in").modal('hide');
        $(".group_errors").text('');
        var bar = $(".progress-bar");
        bar.removeClass('progress-bar-danger');
        bar.addClass('progress-bar-success');
        $("#modal_progress_group_operations").modal('show');
    }

    function resetProgressBar() {
        var bar = $(".progress-bar");

        bar.attr('aria-valuenow', 0);
        bar.css('width', 0 + '%');
        bar.text(0 + '%');
    }

    /**
     * @param total count selections
     * @param current  current itteration
     */
    function processingBar(total, current) {
        var bar = $(".progress-bar");
        var percent = Math.ceil((current / total) * 100);

        bar.attr('aria-valuenow', percent);
        bar.css('width', percent + '%');
        bar.text(percent + '%');

        if (percent == 100) {


            //Если необходимо выполнить общее действие на всю групповую операцию
            setTimeout(function () {
                switch (OrderGroupOperations.operation) {
                    case 'ChangeQueue' :
                        OrderGroupOperations.finalCallbacks.ChangeQueue();
                        break;
                }
                $('#close_progress').prop('disabled', false);
            }, 2000);


        }
    }

});
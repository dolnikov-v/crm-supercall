$(document).ready(function () {
    //Подтверждение адреса с грида
    var modal_confirm = $('#confirm_address_window');
    var modal_edit = $('#edit_confirm_address');

    $('.confirm-address').on('click', function () {
        modal_confirm.prop('url', $(this).prop('href'));
        modal_confirm.modal('show');
        return false;
    });

    $('#confirm_address_window_cancel').on('click', function () {
        $('#confirm_address_window').modal('hide');
        return false;
    });

    $('#confirm_address_window_aprove').on('click', function () {
        modal_confirm.modal('hide');
        location.href = modal_confirm.prop('url');
    });

    //Редактирование адреса
    $('.edit-address').on('click', function () {
        var order_id = $(this).data('order_id');
        var data_address = prepareAddressList($("#data_" + order_id));
        var columns = $(this).closest('.tr-vertical-align-middle').find('td');
        var country = $(columns[1]).text();
        var partner = $(columns[2]).text();
        var h3 = $('h3.title_modal');
        var sub_title = '<br/><small>#' + order_id + ' ' + country + ' ' + partner + '</small>';
        h3.html(h3.html().replace(/(<br.*)$/, ''));
        $(sub_title).appendTo(h3);


        modal_edit.prop('url', $(this).prop('href'));
        modal_edit.prop('order_id', order_id);
        modal_edit.prop('data_address', data_address);

        //нарисовать форму для редактирования адреса
        createEditFormAddress(data_address);

        modal_edit.modal('show');

        return false;
    });

    $('#edit_address_cancel').on('click', function () {
        $('#edit_confirm_address').modal('hide');
        return false;
    });

    $('#edit_address_save').on('click', function () {
        var address = {};

        $(".edit_content_address").find('input').each(function () {
            address[$(this).prop('name')] = $(this).val();
        });

        $('.edit_content_address').find('input').prop('disabled', true);

        var spinner = '<img src="/resources/images/common/spinner.gif" width="40px">';

        $(spinner).appendTo($("h3"));

        $.ajax({
            url: '/order/address-verification/edit/',
            method: 'post',
            data: {
                order_id: $('#edit_confirm_address').prop('order_id'),
                address: address
            },
            success: function (response) {
                console.log('success', response);
                $('.edit_content_address').find('input').prop('disabled', false);
                $("h3").html($("h3").html().replace(spinner, ''));

                var response = JSON.parse(response);

                if (!response.success) {
                    toastr.error(response.message);
                } else {
                    toastr.success(response.message);
                    $('#edit_confirm_address').modal('hide');
                    location.reload();
                }
            },
            error: function (response) {
                toastr.error(response.responseText);
                $('.edit_content_address').find('input').prop('disabled', false);
                $("h3").html($("h3").html().replace(spinner, ''));
            }
        });
    });


});

function prepareAddressList(ul) {
    var items = $(ul).find('li');
    var data = [];

    items.map(function () {
        var temp = $(this).data();
        data.push(temp);
    });

    return data;
}

function createEditFormAddress(data_address) {
    var container = $('.edit_content_address');

    container.html('');

    var order_id = $('#edit_confirm_address').prop('order_id');

    for (var k in data_address) {
        var label = '<label for="' + data_address[k].name + '">' + data_address[k].label + '</label>';
        var input = '<input data-old = "' + data_address[k].value + '" data-order_id = "' + order_id + '" data-id = "' + data_address[k].id + '" class ="form-control ' + data_address[k].class + '" name="' + data_address[k].name + '" type="text" placeholder="' + data_address[k].value + '" value="' + data_address[k].value + '">';
        var line = '<div class="row form-group"><div class="col-6">' + label + '</div><div class="col-6">' + input + '<br/></div></div>';

        $(line).appendTo(container);
    }

    container.find('input').map(function () {
        var inputJq = $(this);

        if (inputJq.hasClass('text-danger')) {
            inputJq.on('paste change keyup', function () {
                if (inputJq.val() != inputJq.attr('data-old')) {
                    inputJq.removeClass('text-danger');
                    inputJq.addClass('text-success');
                } else {
                    inputJq.removeClass('text-success');
                    inputJq.addClass('text-danger');
                }
            });
        }
    });

}


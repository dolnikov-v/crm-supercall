$(function () {
    var Export = {
        container: '.export_btn',
        table: '#orderList',
        btns: '.export_to',
        checkedColor: 'red',
        uncheckedColor: '#686868',
        modal: '#orders_export',
        error_modal: '#orders_export_error',
        modal_body: '#order_export_modals',
        export_btn: '#orders_export_btn',
        title_modal: 'h4',
        spinner: '.loader',
        form: null,
        init: function () {
            Export.form = $(Export.container).find('form');
            Export.initBtns($(Export.container).find(Export.btns));
            $(Export.modal).on('shown.bs.modal', Export.getColumns);
            $(Export.modal).on('hidden.bs.modal', Export.clearModalBody);

            $(Export.modal).find(Export.export_btn).on('click', function () {
                Export.beforeSubmit();
                $(Export.modal).modal('hide');
            });
        },
        beforeSubmit: function () {
            var data = [];
            var order_ids = [];

            $(Export.table).find('[type=checkbox]:checked').not('.select-on-check-all').map(function () {
                order_ids.push($(this).val());
            });

            $('<input/>', {
                type: 'hidden',
                name: 'orders',
                value: JSON.stringify(order_ids)
            }).appendTo($(Export.form));

            Export.form.find('.checked_orders').remove();


            $(Export.modal_body).find("[type=checkbox]:checked").map(function () {
                data.push($(this).prop('name'));
            });

            $('<input/>', {
                type: 'hidden',
                name: 'fields',
                value: JSON.stringify(data)
            }).appendTo($(Export.form));

            Export.form.submit();
        },
        setTitleModal: function (end_phrase) {
            $(Export.modal).find(Export.title_modal).html('Export to ' + end_phrase);
        },
        initBtns: function (btns) {
            btns.map(function () {
                $(this).on('click', function () {
                    Export.form.prop('action', $(this).data('url'));
                    Export.setTitleModal($(this).text());

                    if ($(this).data('type') === 'excel') {
                        if (Export.checkLimitExportExcel()) {
                            $(Export.modal).modal('show');
                        } else {
                            $(Export.error_modal).modal('show');
                        }
                    } else {
                        $(Export.modal).modal('show');
                    }

                    return false;
                });
            });
        },
        getColumns: function () {
            Export.clearModalBody();

            Export.form.find('.checked_orders').remove();
            Export.spinnerShow();

            $.ajax({
                url: '/order/order-export/get-export-columns',
                type: 'post',
                data: {},
                success: function (data) {
                    var response = JSON.parse(data);

                    if (!response.success) {
                        $(Export.modal_body).html(response.message);
                    } else {
                        Export.createColumnList(response.message);


                        $(Export.table).find('[type=checkbox]:checked').not('.select-on-check-all').map(function () {
                            $('<input/>', {
                                type: 'hidden',
                                class: 'checked_orders',
                                value: $(this).val(),
                                name: 'orders[]'
                            }).appendTo(Export.form);
                        });
                    }

                    Export.spinnerHide();
                },
                error: function (error) {
                    Export.spinnerHide();
                    $(Export.modal_body).html(error.responseText);
                }
            });
        },
        clearModalBody: function () {
            $(Export.modal_body).html('');
            $(Export.modal).find(Export.export_btn).prop('disabled', true);
        },
        spinnerShow: function () {
            $(Export.spinner).show();
        },
        spinnerHide: function () {
            $(Export.spinner).hide();
            $(Export.export_btn).prop('disabled', false);
        },
        chunkArray: function (array, chunk_size) {
            var tempArray = [];

            for (index = 0; index < array.length; index += chunk_size) {
                chunk = array.slice(index, index + chunk_size);
                tempArray.push(chunk);
            }

            return tempArray;
        },
        createChbx: function (option) {
            var chbx = $('<input/>', {
                type: 'checkbox',
                id: 'cbx_' + option.name,
                name: option.name
            });

            chbx.on('change', function () {
                $('#label_' + $(this).prop('name')).css({
                    'color': $(this).prop('checked') ? Export.checkedColor : Export.uncheckedColor
                });
            });

            if ((option.checked > 0)) {
                chbx.prop('checked', true);
            }

            return chbx;
        },
        createLabel: function (option) {
            var color = option.checked > 0 ? Export.checkedColor : Export.uncheckedColor;
            return $('<label/>', {
                id: 'label_' + option.name,
                for: 'cbx_' + option.name,
                text: option.label,
                style: 'cursor:pointer; color:' + color
            });
        },
        createColumnList: function (list) {
            var count_in_row = Math.ceil(Object.keys(list).length / Export.count_blocks);

            var line = [];

            for (var k in list) {
                var option = {
                    name: k,
                    checked: list[k].checked,
                    label: list[k].label
                };

                line.push({
                    cbx: Export.createChbx(option),
                    label: Export.createLabel(option)
                });
            }

            //кол-ов столбуов указано в стиле UL в export.css
            var ul = $('<ul/>', {
                'class': 'category-list clearfix'
            });

            for (var j in line) {
                var li = $('<li/>', {
                    'class': 'category-item'
                });

                line[j].cbx.appendTo(li);

                $('<span/>', {
                    style: 'margin-left:5px'
                }).appendTo(li);

                line[j].label.appendTo(li);

                li.appendTo(ul);
            }

            ul.appendTo($(Export.modal_body));
        },
        checkLimitExportExcel: function () {
            if ($(Export.table).find('[type=checkbox]:checked').not('.select-on-check-all').length > 0) {
                return true;
            }
            return 64000 > parseFloat($($('.summary').find('b')[1]).text().replace(/,/g, ''));
        }
    };

    Export.init();
});
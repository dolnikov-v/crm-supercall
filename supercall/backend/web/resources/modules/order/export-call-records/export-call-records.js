$(function () {
    const TYPE_BY_ID = 'by_id';
    const TYPE_BY_QUERY = 'by_query';

    var ExporterCallRecords = {
        url: '/order/task-queue/add-task-to-export-call-records',
        export_btn_html: '',
        modal_confirm: $('#modal-confirm-export-call-records'),
        modal_confirm_btn: $('#export-ok'),
        modal_cancel_btn: $('#export-cancel'),
        btn_export: $('#export-call-records-button'),
        cbx_collection: $('#orderList').find('[type=checkbox]'),
        init: function () {
            $(ExporterCallRecords.modal_confirm).on('hide.bs.modal', function(){
                ExporterCallRecords.setAsButton();
            });

            $(ExporterCallRecords.btn_export).on('click', function () {
                ExporterCallRecords.setAsLoader();
                $(ExporterCallRecords.modal_confirm).modal('show');
            });

            $(ExporterCallRecords.modal_confirm_btn).on('click', function () {
                var ids = ExporterCallRecords.byIds();
                var type = ids.length > 0 ? TYPE_BY_ID : TYPE_BY_QUERY;
                var data = ids.length > 0 ? ids : location.search;

                ExporterCallRecords.addTask({type: type, data: data});
                $(ExporterCallRecords.modal_confirm).modal('hide');
            });

            $(ExporterCallRecords.modal_cancel_btn).on('click', function () {
                ExporterCallRecords.setAsButton();
            });
        },
        setAsLoader: function () {
            $(ExporterCallRecords.btn_export).find('i').removeClass('fa fa-file-audio-o');
            $(ExporterCallRecords.btn_export).find('i').addClass('fa fa-spinner fa-spin');
            $(ExporterCallRecords.btn_export).prop('disabled', true);
        },
        setAsButton: function () {
            $(ExporterCallRecords.btn_export).find('i').removeClass('fa fa-spinner fa-spin');
            $(ExporterCallRecords.btn_export).find('i').addClass('fa fa-file-audio-o');
            $(ExporterCallRecords.btn_export).prop('disabled', false);
        },
        byIds: function () {
            var ids = [];

            $(ExporterCallRecords.cbx_collection).map(function(){
                    if (!$(this).hasClass('select-on-check-all') && $(this).prop('checked')) {
                        ids.push($(this).val());
                    }
            });

            return ids;
        },
        addTask: function (params) {
            $.ajax({
                url: ExporterCallRecords.url,
                type: 'post',
                dataType: 'json',
                data: params,
                success: function (data) {
                    console.log('data', data)
                    if (data.success) {
                        toastr.success(data.message);
                    } else {
                        toastr.error(data.message);
                    }

                    ExporterCallRecords.setAsButton();
                },
                error: function (error) {
                    toastr.error(error.responseText);
                    ExporterCallRecords.setAsButton();
                }
            });
        }
    };

    ExporterCallRecords.init();
});
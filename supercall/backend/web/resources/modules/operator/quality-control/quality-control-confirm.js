$(document).ready(function () {
    $("#order_audit").on("hidden.bs.modal", function () {
        var id = $(this).data('id');

        $('#confirm_' + id).css('display', 'block');

        $('a[data-action="confirm"]').map(function () {
            $(this).on('click', function () {
                $(this).closest('div.dropdown-actions').removeClass('open');
            });
        });
    });

    $('td.td-actions ul li a').on('click', function () {
        var action = $(this).attr('data-action');
        var modal = $("#order_log_modal");

        if (action == 'order-log') {
            modal.attr('order_id', $(this).attr('data-order_id'));
            modal.attr('controller_id', $(this).attr('data-controller_id'));
            modal.modal('show');

            return false;
        }
    });

    $("#order_log_modal_close").on('click', function () {
        $("#order_log_modal").modal('hide');
    });

    $('#order_log_modal').on('shown.bs.modal', function () {
        getOrderLogs($(this));

        var parentTr = $('[data-order_id="' + $(this).attr('order_id') + '"]').closest('tr');

        if (parentTr.css('background-color') != 'rgb(255, 250, 227)') {
            parentTr.data('bgcolor', parentTr.css('background-color'));
            parentTr.css('background-color', '#FFFAE3');
        }
    });

    $("#order_log_modal").on("hidden.bs.modal", function () {
        $('#order_log_modal_content').html('');
    });
});

function getOrderLogs(modal) {
    var modal_content = $('#order_log_modal_content');
    var spinner = '<img width="55px" src="/resources/images/common/spinner.gif">';

    modal_content.html(spinner + 'Loading data');

    $.ajax({
        url: '/operator/audited-orders/get-order-logs',
        type: 'post',
        data: {
            'order_id': modal.attr('order_id'),
            'controller_id': modal.attr('controller_id')
        },
        error: function (error) {
            modal_content.html("<span class='text-danger'>" + error.responseText + "</span>");
        },
        success: function (data) {
            var result = JSON.parse(data);

            if (result.success) {
                modal_content.html(result.message);
                //группировка таблицы
                groupData();
            } else {
                modal_content.html("<span class='text-danger'>" + result.message + "</span>");
            }
        }
    });
}

function groupData() {
    //группировка таблицы изменений аудитора
    var trs = $("#auditor_edit").find('tbody').find('tr');
    var names = $("td[data-field='name']");
    var base = [];

    names.map(function(){
        var label = $(this).data('label');
        if($.inArray(label, base) < 0){
            base.push(label);
        }else{
            $(this).closest('tr').addClass('warning');
            $(this).closest('tr').attr('data-hidden_label', 'hidden_'+label);

            var parent = $("td[data-label='"+$(this).data('label')+"']").closest('tr');

            $($(this).closest('tr')).insertAfter(parent);

            $('tr.warning').hide();

            $(this).closest('tr').remove();

            parent.find("td[data-group='true']").map(function(){
                $(this).html('<span style="cursor:pointer" class="expand">[+]<span>');
            });
        }
    });

    $(".expand").on('click', function(){
        var tr = $(this).closest('tr');
        var label = tr.find("td[data-label]").data('label');

        var hidden_tr_group = $("tr[data-hidden_label='hidden_"+label+"']");

        if($(this).text() == '[+]'){
            $(this).html('<span class="expand">[-]<span>');
            hidden_tr_group.map(function(){
                $(this).show();
            });
        }else{
            $(this).html('<span class="expand">[+]<span>');
            hidden_tr_group.map(function(){
                $(this).hide();
            });
        }
    })

}

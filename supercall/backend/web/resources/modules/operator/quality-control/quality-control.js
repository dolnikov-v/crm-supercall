$(document).ready(function () {
    $(".get-record").on('click', function () {
        getRecords();
    });

    $('td.td-actions ul li a').on('click', function () {
        var action = $(this).attr('data-action');
        var is_edited = $(this).attr('data-edit');
        var operator_status = $(this).attr('data-operator_status');
        var auditor_status = $(this).attr('data-auditor_status');
        var id = $(this).attr('data-id');
        var el = $(this);

        if (action == 'confirm') {
            var operator_id = $(this).attr('data-operator_id');
            $.ajax({
                url: '/operator/quality-control/confirm',
                type: 'post',
                data: {
                    order_id: id,
                    operator_id: operator_id,
                    edit: is_edited,
                    operator_status: operator_status,
                    auditor_status: auditor_status
                },
                response: 'json',
                success: function (data) {
                    var res = JSON.parse(data);
                    if (res.success) {
                        //вернуть цвет строки
                        var parentTr = $('tr[data-key="' + id + '"]');
                        parentTr.css('background-color', parentTr.data('bgcolor'))
                        $('body').find("[data-order_id="+el.data('id')+"]").html('<i class="icon glyphicon-ok" style="color: #090"></i>')
                    }
                },
                error: function (error) {
                    console.log('error getting record: ', error);
                }
            });

            return false;
        }

        if (action == 'edit') {
            $('#order_audit').modal('show');
            $('#order_audit').data('id', $(this).data('id'));

            //сменить цвет строки на активный
            var parentTr = $('tr[data-key="' + $(this).data('id') + '"]');

            if (parentTr.css('background-color') != 'rgb(255, 250, 227)') {
                parentTr.data('bgcolor', parentTr.css('background-color'));
                parentTr.css('background-color', '#FFFAE3');
            }

            return false;
        }
    });

    //modal close
    $('#denied_audit_close').on('click', function () {
        $('#denied_audit').modal('hide');
    });

    //modal close
    $('#order_audit_close').on('click', function () {
        $('#order_audit').modal('hide');
    });

    //открыли аудит - грузим заказ в него
    $('#order_audit').on('shown.bs.modal', function () {
        var order_id = $('#order_audit').data('id');
        var spinner = '<img width="55px" src="/resources/images/common/spinner.gif">';
        $('#order_audit_form').html(spinner + ' Loading order...');

        $.ajax({
            url: '/operator/quality-control/get-edit-form',
            data: {
                order_id: order_id,
            },
            type: 'post',
            error: function (error) {
                $("#order_audit_form").html('<span class="text-danger">An error has occurred : ' + error.responseText + '<br/>params: order_id: ' + order_id + ' url: /operator/quality-control/get-edit-form</span>');
            },
            success: function (data) {
                var res = JSON.parse(data);

                if (!res.success) {
                    $("#order_audit_form").html('<span class="text-danger">An error has occurred : ' + res.message + '<br/>params: order_id: ' + order_id + ' url: /operator/quality-control/get-edit-form</span>');
                } else {
                    $('#order_audit .modal-content').css({
                        marginLeft: '11%',
                        marginTop: '2%',
                    });

                    $('#order_audit .modal-dialog').css({
                        width: '90%',
                        height: '70%',
                        margin: 0,
                        padding: 0
                    });

                    $('#order_audit .modal-body').css({
                        height: 'auto',
                        minHeight: '70%',
                        borderRadius: 0
                    });

                    var iframe = $('<iframe>', {
                        src: res.message,
                        id: 'order_editor',
                        name: 'order_editor',
                        frameborder: 0,
                        width: '100%',
                        height: '600px',
                    });

                    $('#order_audit_form').html(iframe);

                    //работа с формой в шаблоне  /common/modules/order/change/index/_tab-order.php  по условию $quality_control
                }
            }
        });
    });

    $('#order_audit_close').on('click', function () {
        toIframe('close');
    });

    $('#order_audit_save').on('click', function () {
        toIframe('save');
    });

    //очистить модальное окно при закрытии
    $("#order_audit").on("hidden.bs.modal", function () {
        $("#order_audit_form").html('');

        var id = $(this).data('id');
        $('html, body').animate({
            scrollTop: $('tr[data-key="' + id + '"]').offset().top - 250
        }, 10);
    });

    //get from child quality-control-edit.js  toParent()
    function listener(event) {
        console.warn('parent assepted', event.data);
        switch (event.data) {
            //заказ был отредактирован
            case 'is_edited' :
                $('#confirm_' + $('#order_audit').data('id')).attr('data-edit', 1);
                console.log('#confirm_' + $('#order_audit').data('id'));
                break;
            case 'close' :
                $('#order_audit').modal('hide');
                break;
            //is loaded iframe
            case 'is_load' :
                $('#order_audit_close').prop('disabled', false);
                $('#order_audit_save').prop('disabled', false);
                getRecords();
                //когда прогрузился iframe - нужно подгрузить в него записи разговоров
                /*
                var button_get_records = $('#record_place_' + $('#order_audit').data('id')).find('button');

                if (button_get_records.prop('type') == 'button') {
                    button_get_records.trigger('click');
                }else{
                    toIframe('records-'+$('#record_place_'+$('#order_audit').data('id')).data('records'));
                }*/
                break;
            case 'validated' :
                toIframe('fade_in_after_validate');
                break;
        }

        if (/order_params:/.test(event.data)) {
            var temp = event.data.split('order_params:');
            var order_params = JSON.parse(temp[1]);
            var confirm_button = $('#confirm_' + order_params.id);

            confirm_button.attr('data-operator_status', order_params.operator_status);
            confirm_button.attr('data-auditor_status', order_params.auditor_status);

        }

        if (/address/.test(event.data)) {
            //новые данные по адресу
            var tmp = event.data.split('-');
            var order_id = tmp[1];
            var serialized_address = event.data.replace('address-' + order_id + '-', '');
            var address_new = unserialize(serialized_address);

            //старые данные по адресу
            var address_column = $('td[data-column="address-' + order_id + '"]');

            updateAddressColumn(address_column, address_new)
        }
    }

    if (window.addEventListener) {
        window.addEventListener("message", listener);
    } else {
        // IE8
        window.attachEvent("onmessage", listener);
    }
    //end
});

//to child quality-control-edit.js  listener()
function toIframe(command) {
    var iframe = window.frames.order_editor;
    iframe.postMessage(command, '*');
}


/**
 * Обновление адреса в гриде после редактирования заказа
 * @param column
 * @param address_new
 */
function updateAddressColumn(column, address_new) {
    var address_column_data = column.html().split('<br>')

    //пустышка для новых данных по адресу
    var updated_fields = {};

    for (var k in address_column_data) {
        var str = address_column_data[k];
        var str_tmp = str.split('</b> ');
        var val = str_tmp[1];

        //example <b data-name="house">Дом:</b> №241
        var name = str_tmp[0].match(/name="(.*)"/);
        var label = str_tmp[0].match(/">(.*):/);

        if (name && label) {
            //новые данные по адресу
            for (var j in address_new) {
                var find_name = j.match(/\[(.*?)\]/g);

                if (find_name !== null) {
                    if (find_name.length > 0) {
                        if (find_name.pop().replace(/\]|\[/g, '') == name[1]) {
                            updated_fields[name[1]] = {
                                'label': label[1],
                                'value': decodeURIComponent(address_new[j])
                            }
                        }
                    }
                }
            }
        }
    }

    var new_block_address = '';

    for (var k in updated_fields) {
        var field = updated_fields[k];
        new_block_address += '<b data-name="' + k + '">' + field.label + ':</b> ' + field.value + '<br>';
    }

    //update addres on gridView column
    column.html(new_block_address);
}

function unserialize(serializedString) {
    var str = decodeURI(serializedString).replace(/\+/g, ' ');
    var pairs = str.split('&');
    var obj = {}, p, idx, val;
    for (var i = 0, n = pairs.length; i < n; i++) {
        p = pairs[i].split('=');
        idx = p[0];

        if (idx.indexOf("[]") === (idx.length - 2)) {
            // Eh um vetor
            var ind = idx.substring(0, idx.length - 2)
            if (obj[ind] === undefined) {
                obj[ind] = [];
            }
            obj[ind].push(p[1]);
        }
        else {
            obj[idx] = p[1];
        }
    }
    return obj;
}

/**
 * Получение записей разговоров
 * Если есть CallHistory - то получим виджет
 * Если нет - то лезем на сервер астериска через наш экшн в контроллере
 */
function getRecords() {
    var id = $('#order_audit').data('id');
    var dataKeyId = $('[data-key=' + id + ']');
    var phones = dataKeyId.find('td.order_id').data('phones');//button.closest('.row').data('phones').toString().split(/,/g);
    var emptyCallHistory = (dataKeyId.find('td.order_id').data('call_history') === 0);

    console.log('phones', phones);
    console.log('emptyCallHistory', emptyCallHistory, dataKeyId.find('td.order_id').data('call_history'));

    //получение записей с астериска
    var url;

    if (emptyCallHistory) {
        url = '/operator/quality-control/get-records';
    } else {
        url = '/operator/quality-control/get-widget'
    }
    $.ajax({
        url: url,
        type: 'post',
        data: {
            order_id: id,
            phones: phones
        },
        response: 'json',
        success: function (data) {
            var res = JSON.parse(data);

            if (typeof  window.frames.order_editor !== "undefined") {
                //подгрузка записей в форме заказа - вместо карты. Передать в iframe
                toIframe('records-' + res.line_content);
            }
        },
        error: function (error) {
            toIframe('records-<span class="text-danger">An error has occurred</span>');
            console.log(error)
        }
    });
}
$(document).ready(function(){
    $('[name="checkbox_activity"]').map(function(){

        $(this).click(function(){
            var ids = $(this).prop('id').split(/-/);
            var queue_id = ids[0];
            var user_id = ids[1];
            var action = $(this).prop('checked') ? 'set' : 'un-set';

            accessQueue(action, queue_id, user_id);
        })
    });
});

function accessQueue(action, queue_id, user_id){
    $.ajax({
        url: '/operator/list-operators/queue-'+action,
        type: 'POST',
        data : {queue_id: queue_id, user_id: user_id},
        success : function(data){
            var result = JSON.parse(data);

            if(result.success){
                toastr.success(result.message);
            }else{
                toastr.error(result.message);
            }
        }
    });
}

$(document).ready(function () {
    $("#type_answer").change(function () {
        if ($(this).val() == 'options') {
            $(".answers").show()
        } else {
            $(".answers, .answers_other").hide();
        }
    });

    $("#answers").change(function () {
        if ($(this).val() == 'other') {
            $(".answers_other").show()
        } else {
            $(".answers_other").hide()
        }
    });
});


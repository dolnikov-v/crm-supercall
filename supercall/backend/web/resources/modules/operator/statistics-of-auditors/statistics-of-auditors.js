$(document).ready(function(){
    var tds = $("td.text-success");

    tds.map(function(){
        var url = $(this).data('url');

        if(typeof url != 'undefined' && $(this).text() > 0){
            var text = '<u><a href="'+url+'" target="_blank">'+$(this).text()+'</a></u>';
            $(this).html(text);
        }
    });
});
$(function () {
    $('input[data-type-checkbox="switchery"]').on('switchChange.bootstrapSwitch', function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            role: $(this).closest('table').data('role'),
            permission: $(this).closest('td').data('permission'),
            value: $(this).is(':checked') ? 'on' : 'off'
        };

        var success = $(this).closest('table').data('i18n-success');
        var fail = $(this).closest('table').data('i18n-fail');

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/access/role/set-permission',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {

                $.notify({message: success}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: fail}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});

$(document).ready(function () {
    var country_input = $("[name*=country_id]");

    if ($("[name*=id]").val() !== '') {
        country_input.on('change', function () {
            console.log('change');
            ListUsers.getListUsersOfTeam();
        });

        country_input.trigger('change');
    }
});

var ListUsers = {
    init: function () {
        ListUsers.getListUsersOfTeam();
    },
    renderCheckbox: function (val, checked) {
        return $('<input/>', {
            type: 'checkbox',
            name: 'user[' + val + ']',
            checked : checked,
            value: val
        });
    },
    getListUsersOfTeam: function () {
        ListUsers.showLoader();

        $.ajax({
            url: '/access/team/get-list-users',
            type: 'post',
            data: {
                team_id: $("[name*=id]").val(),
                country_id: $("[name*=country_id]").val()
            },
            dataType: 'json',
            success: function (data) {
                if (Object.keys(data.users).length > 0) {
                    ListUsers.generateList(data.users, data.team_users);
                } else {
                    $('.list-users-of-team').html('No data found');
                }
                ListUsers.hideLoader();

                ListUsers.checkCountryUser();

            },
            error: function (error) {
                toastr.error(error.responseText);
                ListUsers.hideLoader();
            }
        });
    },
    showLoader: function () {
        $('.excluded_users').hide();
        $('.btn-success').prop('disabled', true);
        var container = $('.list-users-of-team');
        container.html('');

        var loader = $('.block-loader').clone();
        loader.removeClass('hidden');
        loader.addClass('clone');
        loader.appendTo(container);
    },
    hideLoader: function () {
        $('.clone').hide();
    },
    generateList: function (users, team_users) {
        var container = $('.list-users-of-team');
        container.html('');

        for (var k in users) {
            ListUsers.renderCheckbox(k, false).appendTo(container);

            $('<label />', {for: 'user[' + k + ']', text: users[k]}).appendTo(container);
            $('<br/>').appendTo(container);

            container.find("[type=checkbox]").last().attr('data-id', k);

            if ($.inArray(parseInt(k), team_users) >= 0) {
                container.find("[type=checkbox]").last().prop('checked', true);
            }
        }

        $('label').css('margin-left', '10px');

        $('.list-users-of-team [type=checkbox]').map(function () {
            var cbx = $(this);

            cbx.on('click', function () {
                ListUsers.manageUserOfTeam($("[name*=id]").val(), cbx.data('id'), cbx.prop('checked'));
            });
        });
    },
    manageUserOfTeam: function (team_id, user_id, state) {
        var container = $('.list-users-of-team');
        container.css('opacity', 0.5);
        container.closest('fieldset').prop('disabled', true);

        $.ajax({
            url: '/access/team/manage-user-of-team',
            type: 'post',
            data: {
                team_id: team_id,
                user_id: user_id,
                state: state
            },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }

                container.css('opacity', 1);
                container.closest('fieldset').prop('disabled', false);
            },
            error: function (error) {
                var nameChbx = 'user[' + user_id + ']';

                toastr.error(error.responseText);
                container.css('opacity', 1);
                container.closest('fieldset').prop('disabled', false);

                $('*[data-id="' + user_id + '"]').prop('checked', !state);
            }
        });
    },
    checkCountryUser: function () {
        var container = $('.list-users-of-team');

        $('.compatibility').removeClass('hidden');
        $('.compatibility').show();

        container.find('[type=checkbox]').prop('disabled', true);

        $.ajax({
            url: '/access/team/check-compatibility',
            type: 'post',
            data: {
                team_id: $("[name*=id]").val(),
                country_id: $("[name*=country_id]").val()
            },
            dataType: 'json',
            success: function (data) {
                console.log('data', data);
                $('.btn-success').prop('disabled', false);

                ListUsers.manageCheckboxes(data);
                $('.compatibility').hide();

                container.find('[type=checkbox]').prop('disabled', false);
            },
            error: function () {
                $('.btn-success').prop('disabled', false);
                $('.compatibility').hide();
                container.find('[type=checkbox]').prop('disabled', false);
            }
        });
    },
    manageCheckboxes: function (excluded_users) {
        $('.list-users-of-team').find("[type=checkbox]").css('color', 'red');

        if (Object.keys(excluded_users).length > 0) {
            $('.btn-success').prop('disabled', true);

            var container = $('.excluded_users_list');
            container.html('');

            $('.excluded_users').removeClass('hidden');
            $('.excluded_users').show();

            for (var k in excluded_users) {

                ListUsers.renderCheckbox(k, true).appendTo(container);

                $('<label />', {for: 'user[' + k + ']', text: excluded_users[k]}).appendTo(container);
                $('<br/>').appendTo(container);

                $('label').css('margin-left', '10px');

                container.find("[type=checkbox]").last().attr('data-id', k);

                var cbx = container.find('[type=checkbox]');

                cbx.on('click', function () {
                    ListUsers.manageUserOfTeam($("[name*=id]").val(), cbx.data('id'), cbx.prop('checked'));

                    if(container.find("[type=checkbox]:checked").length === 0){
                        $('.btn-success').prop('disabled', false);
                    }else{
                        $('.btn-success').prop('disabled', true);
                    }
                });
            }
        }
    }
};
$(document).ready(function () {
    $(".check-all-country").on('click', function () {
        ListUsersEdit.selectAllCountries();
    });
});

var ListUsersEdit = {
    selectAllCountries: function () {
        var selectCountry = $("#user-country").select2();
        var hiddenCountry = $('#user-country option');
        var country_id = [];

        hiddenCountry.each(function(){
            country_id.push($(this).val());
        });

        selectCountry.val(country_id).trigger("change");
    },
};
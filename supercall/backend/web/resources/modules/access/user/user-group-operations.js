$(document).ready(function () {
    var UserGroupOperations = {
        buttons: $(".group-operations").find("button.btn"),
        init: function () {
            this.buttons.map(function () {
                $(this).on('click', function () {
                    resetProgressBar();

                    var url = $(this).data('url').toString();
                    var action = $(this).data('action').toString();

                    //call
                    if (typeof UserGroupOperations.callbacks[action] === 'function') {
                        if (getSelections().length > 0) {
                            UserGroupOperations.callbacks[action](url);
                        } else {
                            $("#modal_warning_group_operations").modal('show');
                        }
                    } else {
                        console.error(action + ' is not a function');
                    }

                });
            });
        },
        //обработчики кнопок групповых операций
        callbacks: {
            BlockUsers: function (url) {
                confirmOpen(url, {});
            },
            ActivateUsers: function (url) {
                confirmOpen(url, {});
            },
            OnWebRTC: function (url) {
                confirmOpen(url, {});
            },
            OffWebRTC: function (url) {
                confirmOpen(url, {});
            },
            OnAutoCall: function (url) {
                confirmOpen(url, {});
            },
            OffAutoCall: function (url) {
                confirmOpen(url, {});
            },
            DeleteUsers: function (url) {
                confirmOpen(url, {});
            },
            UnDeleteUsers: function (url) {
                confirmOpen(url, {});
            }
        }
    };

    UserGroupOperations.init();
    domReady();

});

/**
 * Процессы необходимые при загрузке страницы
 */
function domReady(){
    $("#modal_confirm_group_operations_ok").on('click', function () {
        var modalConfirm = $("#modal_confirm_group_operations");
        var url = modalConfirm.data('url');
        var data = modalConfirm.data('data');

        sendRequest(url, data);
    });

    $('#close_progress').on('click', function () {
        if ($("#close_progress").closest('div.modal').find('.progress-bar').hasClass('progress-bar-success')) {
            //location.reload();
        }
    });

    $('.select-on-check-all').on('click', function () {
        var chbx = $("#userList").find('input[name*=selection]');
        var all = $(this);

        chbx.map(function () {
            $(this).prop('checked', all.prop('checked'));
        });

    });
}

/**
 * @returns {Array}
 */
function getSelections() {
    var chbx = $("#userList").find('input[name*=selection]:checked');
    var ids = [];

    chbx.map(function () {
        if (!$(this).hasClass('select-on-check-all')) {
            ids.push($(this).val());
        }
    });

    return ids;
}

/**
 *
 * @param url {string}
 * @param data {object} for merge params with defaul selections
 */
function sendRequest(url, data) {
    clearBackGroudGrid();
    window.user_group_operations_count = 0;
    var selections = getSelections();
    var group = window.performance.now().toString().replace(/\./g, '');

    for (var k in selections) {
        var defaultData = {
            id: selections[k],
            group: group
        };

        showProgress();

        $.ajax({
            url: url,
            type: 'post',
            data: typeof data === 'object' ? Object.assign(defaultData, data) : defaultData,
            error: function (response) {
                var bar = $(".progress-bar");
                bar.removeClass('progress-bar-success');
                bar.addClass('progress-bar-danger');

                $('#close_progress').prop('disabled', false);
                toastr.error(response.responseText);
            },
            success: function (response) {
                checkAnswer(response, selections.length);
            }
        })
    }
}

/**
 * @param data {object} - доп параметры (например статус)
 * @param total  {integer} - count selections
 */
function checkAnswer(data, total) {
    var response = JSON.parse(data);
    var chbx = $("input[type=checkbox][value=\"" + response.id + "\"]");

    if (response.success) {
        window.user_group_operations_count++;
        processingBar(total, window.user_group_operations_count);

        chbx.prop('checked', false);
        chbx.closest('tr').css('background-color', '#C8FFE6');
    } else {
        chbx.closest('tr').css('background-color', '#FFCECE');
        toastr.error(response.message);
        $(".group_errors").append('<br/>' + response.message);
    }
}

/**
 * @param url  {string} yii server route
 * @param data {object} - доп параметры (например статус)
 */
function confirmOpen(url, data) {
    var modalConfirm = $("#modal_confirm_group_operations");
    modalConfirm.addClass('hmodal-info');
    modalConfirm.data('url', url);
    modalConfirm.data('data', data);
    modalConfirm.modal('show');
}

function showProgress() {
    $(".in").modal('hide');
    $(".group_errors").text('');
    var bar = $(".progress-bar");
    bar.removeClass('progress-bar-danger');
    bar.addClass('progress-bar-success');
    $("#modal_progress_group_operations").modal('show');
}

/**
 * @param total count selections
 * @param current  current itteration
 */
function processingBar(total, current) {
    var bar = $(".progress-bar");
    var percent = Math.ceil((current / total) * 100);

    bar.attr('aria-valuenow', percent);
    bar.css('width', percent + '%');
    bar.text(percent + '%');

    if (percent == 100) {
        $('#close_progress').prop('disabled', false);
    }
}

function resetProgressBar(){
    var bar = $(".progress-bar");

    bar.attr('aria-valuenow', 0);
    bar.css('width', 0 + '%');
    bar.text(0 + '%');
}

function clearBackGroudGrid(){
    $("input[type=checkbox]").map(function(){
        $(this).closest('tr').css('background-color', 'white');
    });
}

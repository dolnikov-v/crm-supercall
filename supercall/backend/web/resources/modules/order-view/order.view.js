$(document).ready(function () {


});

/**
 * init main function ofr load each file
 * @param orderId
 */
function startRecordLoad(orderId)
{
    $('.tab-record-view').removeAttr('onclick');

    $.post("/order/record/start-load", {'orderId': orderId})
        .success(function(response){
            if (response.result === 'ok') {
                console.log(response.data)
                $.each(response.data, function(index, value) {
                    itemRecordLoad(value)
                });
            }
        });
}


/**
 * Load item file
 * @param value
 */
function itemRecordLoad(value)
{
    $.post("/order/record/load-file", {'path': value.path, 'id': value.id})
        .success(function(response) {
            if (response.result == 'ok') {
                $('tr.' + response.data.id).html('<td><audio src="' + response.data.url + '" controls="" preload="none"></audio></td>' +
                    '<td><a href="' + response.data.url + '">Скачать</a></td>')
            } else if (response.result == 'error') {
                toastr.error('Ошибка при скачивании файла записи. Обновите страницу или попробуйте позже');
            }
        })
        .error(function(response){
            toastr.error('Ошибка при скачивании файла записи. Обновите страницу или попробуйте позже');
        });;
}
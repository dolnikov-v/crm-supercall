$(function () {
    const ACTIVE = "1";
    const NOT_ACTIVE = "0";

    var TS = {
        container: '.container-settings',
        url_save: '/audit/transcribe-setting/edit',
        url_manage: '/audit/transcribe-setting/manage',
        url_delete: '/audit/transcribe-setting/delete',
        active_btn_save: null,
        active_btn_manage: null,
        fieldset: 'fieldset.transcribe-setting-block',
        label_fieldset: 'label.country',
        btn_add: '.transcribe-add',
        btn_save: '.transcribe-save',
        btn_manage: '.transcribe-manage',
        btn_delete: '.transcribe-delete',
        country: '[name*="TranscribeSetting[country_id]"]',
        statuses: '[name*=status]',
        stops: '[name*=TranscribeStopSet]',
        exceptions: '[name*=TranscribeExceptionSet]',
        rows: '.stop-word-groups',
        row: '.stop-word-group',
        btn_add_stops: '.add-stop-setting',
        btn_remove_stops: '.remove-stop-setting',
        init: function () {
            TS.initAddBtn();
            TS.initEvents();
            TS.manageGroupStops();
            TS.addEventsBtns();
        },
        manageGroupStops: function () {
            $(TS.rows).each(function () {
                if($(this).find(TS.row).length == 1) {
                    $(this).find(TS.btn_remove_stops).first().hide();
                }
            });
        },
        addEventAddStops: function(){
            $(TS.btn_add_stops).on('click', function (e) {
                e.stopImmediatePropagation();
                TS.addRowStops($(this));
            });
        },
        addEventRemoveStops: function(){
            $(TS.btn_remove_stops).on('click', function (e) {
                TS.removeRowStops($(this));
            });
        },
        addRowStops: function (btn) {
            var clone = $(btn).closest(TS.rows).find(TS.row).first().clone();
            $(clone).find('.select2').remove().select2();

            var uniqueid = TS.uniqueid();
            $(clone).find(TS.statuses).attr('name', 'status["'+uniqueid+'"][]');
            $(clone).find(TS.stops).attr('name', 'TranscribeStopSet["'+uniqueid+'"][]');


            $(btn).closest(TS.rows).append('<br/>');
            $(btn).closest(TS.rows).append($(clone));

            $(clone).find('select').select2();
            $(clone).find('.select2-container').css('width', 'auto');
            $(clone).find('select').val('').trigger('change');
            $(btn).closest(TS.rows).find(TS.btn_remove_stops).show();

            TS.addEventsBtns();
        },
        addEventsBtns: function(){
            TS.addEventAddStops();
            TS.addEventRemoveStops();
        },
        removeRowStops: function(btn){
            $(btn).closest(TS.row).remove();
            TS.manageGroupStops();
        },
        uniqueid: function () {
            return Math.random().toString(36).substr(2, 9);
        },
        initEvents: function () {
            TS.initCountry();
            TS.initSave();
            TS.initManage();
            TS.initDelete();
        },
        initCountry: function () {
            $(TS.country).on('change', function (e) {
                var data = $(this).select2('data');

                $(this).closest(TS.fieldset).find(TS.label_fieldset).html(data[0].text === '-' ? '' : data[0].text);
            });
        },
        initSave: function () {
            $(TS.btn_save).on('click', function (e) {
                e.stopImmediatePropagation();

                TS.save($(this).closest(TS.fieldset));
                return false;
            });
        },
        initManage: function () {
            $(TS.btn_manage).on('click', function (e) {
                e.stopImmediatePropagation();

                TS.manage($(this).closest(TS.fieldset));
                return false;
            });
        },
        initDelete: function () {
            $(TS.btn_delete).on('click', function (e) {
                e.stopImmediatePropagation();

                TS.delete($(this).closest(TS.fieldset));
                return false;
            });
        },
        getFieldsetById: function (id) {
            return $(TS.container).find('fieldset[data-id="' + id + '"]');
        },
        loadingShow: function (fieldset) {
            $(fieldset).find('select').prop('disabled', true);
            $(fieldset).find('a').attr('disabled', true);
        },
        loadingHide: function (fieldset) {
            $(fieldset).find('select').prop('disabled', false);
            $(fieldset).find('a').attr('disabled', false);
        },
        showActiveBtn: function (fieldset) {
            $(fieldset).find(TS.btn_manage).removeClass('hidden');
        },
        hideActiveBtn: function (fieldset) {
            $(fieldset).find(TS.btn_manage).addClass('hidden');
        },
        showDeleteBtn: function (fieldset) {
            $(fieldset).find(TS.btn_delete).removeClass('hidden');
        },
        hideDeleteBtn: function (fieldset) {
            $(fieldset).find(TS.btn_delete).addClass('hidden');
        },
        save: function (fieldset) {
            TS.disabledSave(fieldset);

            $.ajax({
                url: TS.url_save,
                type: 'post',
                dataType: 'json',
                data: {
                    id: TS.getId(fieldset),
                    country_id: TS.getCountry(fieldset),
                    statuses: TS.getStatuses(fieldset),
                    stops: TS.getStops(fieldset),
                    exceptions: TS.getExceptions(fieldset)
                },
                success: function (data) {
                    if (!data.success) {
                        toastr.error(data.message);
                    } else {
                        toastr.success(data.message);
                        TS.showActiveBtn(fieldset);
                        TS.showDeleteBtn(fieldset);

                        $(fieldset).attr('data-id', data.id);

                        if ($(fieldset).find(TS.btn_manage).attr('data-active') === ACTIVE) {
                            TS.activateBtnManage(fieldset);
                        }

                        TS.initAddBtn();
                    }

                    TS.enabledSave(fieldset);
                    TS.initAddBtn();
                },
                error: function (error) {
                    toastr.error(error.responseText);
                    TS.enabledSave(fieldset);
                }
            });


            return false;
        },
        manage: function (fieldset) {
            TS.disabledManage(fieldset);

            $.ajax({
                url: TS.url_manage,
                type: 'post',
                dataType: 'json',
                data: {
                    id: TS.getId(fieldset)
                },
                success: function (data) {
                    if (!data.success) {
                        toastr.error(data.message);
                    } else {
                        toastr.success(data.message);
                    }

                    TS.enabledManage(fieldset);

                    if ($(fieldset).find(TS.btn_manage).attr('data-active') === ACTIVE) {
                        TS.deactivateBtnManage(fieldset);
                    } else {
                        TS.activateBtnManage(fieldset);
                    }
                },
                error: function (error) {
                    toastr.error(error.responseText);

                    TS.enabledManage(fieldset);
                }
            });

            return false;
        },
        delete: function (fieldset) {
            TS.disabledDelete(fieldset);

            $.ajax({
                url: TS.url_delete,
                type: 'post',
                dataType: 'json',
                data: {
                    id: TS.getId(fieldset)
                },
                success: function (data) {
                    if (!data.success) {
                        toastr.error(data.message);
                        TS.enabledDelete(fieldset);
                    } else {
                        toastr.success(data.message);
                        $(fieldset).remove();
                    }

                    TS.enabledManage(fieldset);
                },
                error: function (error) {
                    toastr.error(error.responseText);

                    TS.enabledDelete(fieldset);
                }
            });

            return false;
        },
        activateBtnManage: function (fieldset) {
            var btn_manage = $(fieldset).find(TS.btn_manage);

            $(btn_manage).addClass('btn-default')
                .removeClass('btn-success')
                .text($(btn_manage).attr('data-text-deactive'))
                .attr('data-active', ACTIVE);

            $(fieldset).removeClass('muted_area');
            $(fieldset).addClass('not_muted_area');
        },
        deactivateBtnManage: function (fieldset) {
            var btn_manage = $(fieldset).find(TS.btn_manage);

            $(btn_manage).addClass('btn-success')
                .removeClass('btn-default')
                .text($(btn_manage).attr('data-text-active'))
                .attr('data-active', NOT_ACTIVE);

            $(fieldset).removeClass('not_muted_area');
            $(fieldset).addClass('muted_area');
        },
        initAddBtn: function () {
            var emptyForm = false;

            $(TS.fieldset).map(function () {
                if ($(this).attr('data-id') === '') {
                    emptyForm = true;
                }
            });

            if (emptyForm) {
                TS.disabledAdd();
            } else {
                TS.enabledAdd();
            }

            $(TS.btn_add).on('click', function (e) {
                e.stopImmediatePropagation();
                TS.add();
                return false;
            });
        },
        cloneSelect2: function (selector) {
            $(selector).closest('div').find('.select2-container').remove();
            $(selector).select2();
            $(selector).closest('div').find('.select2-container').css('width', 'auto');
            $(selector).val('').trigger('change');
        },
        add: function () {
            var clone = $(TS.container).find(TS.fieldset).first().clone();
            var cloned_fieldset = $(TS.container).find(TS.fieldset).last();

            $(clone).appendTo($(TS.container));
            $(cloned_fieldset).attr('data-id', '');

            TS.cloneSelect2($(cloned_fieldset).find(TS.country));
            TS.cloneSelect2($(cloned_fieldset).find(TS.statuses));
            TS.cloneSelect2($(cloned_fieldset).find(TS.stops));
            TS.cloneSelect2($(cloned_fieldset).find(TS.exceptions));

            $(cloned_fieldset).find(TS.label_fieldset).text($(clone).find(TS.label_fieldset).attr('data-text'));
            $(cloned_fieldset).removeClass('muted-area').addClass('not_muted_area');
            $(cloned_fieldset).find(TS.btn_delete).addClass('hidden');
            $(cloned_fieldset).find(TS.statuses + ':not(:first)').remove();
            $(cloned_fieldset).find(TS.stops + ':not(:first)').remove();

            $(cloned_fieldset).find(TS.btn_manage)
                .removeClass('btn-success')
                .addClass('btn-default')
                .attr('data-active', 1)
                .text($(TS.btn_manage).attr('data-text-deactive'))
                .addClass('hidden');


            TS.initEvents();
            TS.manageGroupStops();
            TS.addEventsBtns();

            return false;
        },
        getId: function (fieldset) {
            return $(fieldset).attr('data-id');
        },
        getCountry: function (fieldset) {
            return $(fieldset).find(TS.country).val();
        },
        getStatuses: function (fieldset) {
            var data = {};

            $(fieldset).find(TS.statuses).each(function(){
                var name = $(this).prop('name').replace(/(status|\[|\]|")/g, '');
                if(typeof data[name] == 'undefined'){
                    data[name] = null;
                }

                data[name] = $(this).val();
            });

            return data;
        },
        getStops: function (fieldset) {
            var data = {};

            $(fieldset).find(TS.stops).each(function(){
                var name = $(this).prop('name').replace(/(TranscribeStopSet|\[|\]|")/g, '');

                if(typeof data[name] == 'undefined'){
                    data[name] = [];
                }

                data[name] = $(this).val();
            });

            return data;
        },
        getExceptions: function (fieldset) {
            return $(fieldset).find(TS.exceptions).val();
        },
        disabledAdd: function () {
            $(TS.btn_add).addClass('disabled');
        },
        enabledAdd: function () {
            $(TS.btn_add).removeClass('disabled');
        },
        disabledSave: function (fieldset) {
            $(fieldset).find(TS.btn_save).addClass('disabled');
            TS.loadingShow($(fieldset));
        },
        enabledSave: function (fieldset) {
            $(fieldset).find(TS.btn_save).removeClass('disabled');
            TS.loadingHide($(fieldset));
        },
        disabledManage: function (fieldset) {
            $(fieldset).find(TS.btn_manage).addClass('disabled');
            TS.loadingShow($(fieldset));
        },
        enabledManage: function (fieldset) {
            $(fieldset).find(TS.btn_manage).removeClass('disabled');
            TS.loadingHide($(fieldset));
        },
        disabledDelete: function (fieldset) {
            $(fieldset).find(TS.btn_delete).addClass('disabled');
            TS.loadingShow($(fieldset));
        },
        enabledDelete: function (fieldset) {
            $(fieldset).find(TS.btn_delete).removeClass('disabled');
            TS.loadingHide($(fieldset));
        }
    };

    TS.init();
});
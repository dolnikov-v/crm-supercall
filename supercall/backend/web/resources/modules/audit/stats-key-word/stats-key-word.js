$(function () {
    var StatsKeyWord = {
        url: '/audit/stats-key-word/get-operators',
        loader: '.operator-loader',
        country: "[name*=country]",
        operator: "[name*=operator]",
        init: function () {
            if (parseUrl('AggregateWordSearch[operator]') !== '') {
                StatsKeyWord.getOperators();
            }

            $(StatsKeyWord.country).on('change', function () {
                StatsKeyWord.getOperators();
            });
        },
        getOperators: function () {
            StatsKeyWord.loadShow();

            $.ajax({
                url: StatsKeyWord.url,
                type: 'post',
                dataType: 'json',
                data: {
                    country_id: $(StatsKeyWord.country).val()
                },
                success: function (data) {
                    StatsKeyWord.addOptions(data);
                    StatsKeyWord.loadHide();
                },
                error: function (error) {
                    toastr.error(error.responseText);
                    StatsKeyWord.loadHide();
                }
            });
        },
        loadShow: function () {
            $(StatsKeyWord.operator).closest('div').addClass('hidden');
            $(StatsKeyWord.loader).removeClass('hidden');
            $(StatsKeyWord.country).prop('disabled', true);
        },
        loadHide: function () {
            $(StatsKeyWord.loader).addClass('hidden');
            $(StatsKeyWord.operator).closest('div').removeClass('hidden');
            $(StatsKeyWord.country).prop('disabled', false);
        },
        addOptions: function (data) {
            $(StatsKeyWord.operator).val('');
            $(StatsKeyWord.operator).find('option').remove();

            StatsKeyWord.setEmptyOption();

            for (var k in data) {
                if (data[k] !== '') {
                    var newOption = new Option(data[k], k, false, false);
                    $(StatsKeyWord.operator).append(newOption)
                }
            }

            $(StatsKeyWord.operator).val(parseUrl('AggregateWordSearch[operator]'));
            $(StatsKeyWord.operator).trigger('change');
        },
        setEmptyOption: function(){
            var newOption = new Option('-', '', false, false);
            $(StatsKeyWord.operator).append(newOption)
        }
    };

    StatsKeyWord.init();
});
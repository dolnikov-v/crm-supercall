$(function () {
    VerifiedAddress = {
        url: '/audit/verified-address/get-controllers',
        country: $("[name*=country_id]"),
        controller: $("[name*=controller_id]"),
        preset_controller_val: null,
        submit: $("[type=submit]"),
        init: function () {
            VerifiedAddress.country.on('change', VerifiedAddress.getControllers);
            VerifiedAddress.getControllers();
        },
        getControllers: function () {
            VerifiedAddress.disabledInputs();
            VerifiedAddress.clearControllers();

            $.ajax({
                url: VerifiedAddress.url,
                type: 'post',
                dataType: 'json',
                data: {
                    country_id: VerifiedAddress.country.val()
                },
                success: function (data) {
                    VerifiedAddress.controller.val('');
                    VerifiedAddress.controller.trigger('change');
                    VerifiedAddress.addControllers(data);
                    VerifiedAddress.enabledInputs();
                },
                error: function (error) {
                    VerifiedAddress.enabledInputs();
                    toastr.error(error.responseText);
                }
            });
        },
        disabledInputs: function () {
            VerifiedAddress.controller.prop('disabled', true);
            VerifiedAddress.submit.prop('disabled', true);
        },
        enabledInputs: function () {
            VerifiedAddress.controller.prop('disabled', false);
            VerifiedAddress.submit.prop('disabled', false);
        },
        clearControllers: function () {
            VerifiedAddress.controller.find('option').remove();
        },
        addControllers: function (data) {
            $(new Option('-', '', false, false)).appendTo(VerifiedAddress.controller);

            for (var k in data) {
                $(new Option(data[k], k, false, false)).appendTo(VerifiedAddress.controller);
                VerifiedAddress.controller.select2();
            }

            VerifiedAddress.controller.val(VerifiedAddress.getParameterByName('VerifiedAddressSearch[controller_id]'));
            VerifiedAddress.controller.trigger('change');
        },
        getParameterByName: function (name, url) {
            if (!url)
                url = window.location.href;

            url = decodeURIComponent(url);
            name = name.replace(/[\[\]]/g, "\\$&");

            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
            var results = regex.exec(url);

            if (!results)
                return null;
            if (!results[2])
                return '';

            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    };

    VerifiedAddress.init();
});
$(document).ready(function () {
    if ($('#queue-is_primary').prop('checked')) {
        $('[name*=country_id]').prop('disabled', true);
    }
    else {
        $('[name*=country_id]').prop('disabled', false);
    }

    // switch on preset-select
    $('#strategy-is_preset').on('click', function () {
        if ($(this).prop('checked')) {
            $('#strategy-preset').removeAttr('disabled')

            $('#strategy-is_count').prop('checked', false)
            $('#strategy-count').attr('disabled', 'disabled')
        }
        else {
            $('#strategy-preset').attr('disabled', 'disabled')
        }
    });

    // switch on count-select
    $('#strategy-is_count').on('click', function () {
        if ($(this).prop('checked')) {
            $('#strategy-count').removeAttr('disabled')

            $('#strategy-is_preset').prop('checked', false)
            $('#strategy-preset').attr('disabled', 'disabled')
        }
        else {
            $('#strategy-count').attr('disabled', 'disabled')
        }
    });

    // use primary queue
    $('#queue-is_primary').on('click', function () {
        if ($(this).prop('checked')) {
            $('#box-is_primary').show();
            $('[name*=country_id]').val('-').trigger('change');;
            $('[name*=country_id]').prop('disabled', true);
        }
        else {
            $('#box-is_primary').hide()
            $('[name*=country_id]').prop('disabled', false);
        }
    });


});

/**
 * Add new raw strategy in the form Queue
 * @param numb
 */
function addStrategy(numb) {
    $.post("/queue/control/get-strategy", {'numb': numb})
        .success(function (response) {
            if (response.result === 'ok') {
                $('#block-strategy .copy-strategy').append(response.data)
            }
        })
        .error(function (response) {

        });

    count = Number(numb)
    count++
    $('.btn-add-strategy').attr('onclick', 'addStrategy("' + count + '")');
}

/**
 * init select2-plugin
 */
function initSelect2() {
    $(".select2-form").select2();
}

/**
 * Удалить строку
 * @param numb
 */
function removeRawStrategy(numb) {
    $('.strategy-raw--' + numb).remove();
}

/**
 * final queue
 *
 * @param typeActive
 * @param typeNotActive
 */
function strategyChangeFinal(typeActive, typeNotActive) {
    var value = $('#select-' + typeActive).val()

    if (value && value != '-') {
        $('#select-' + typeActive).removeAttr('disabled')
        $('#select-' + typeNotActive).attr('disabled', 'disabled')
    } else {
        $('#select-' + typeNotActive).removeAttr('disabled')
    }
}
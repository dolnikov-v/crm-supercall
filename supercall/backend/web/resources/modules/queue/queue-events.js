$(document).ready(function () {
    var is_event = $("#is_event");
    var event_count = $('[name="Events[eventCount]"]');
    var event_type = $('[name="Events[eventType]"]');
    var event_change_status = $("#event_change_status");
    var event_change_sub_status = $("#event_change_sub_status");
    var event_change_queue = $("#event_change_queue");
    var event_blur_blockTime = $('[name*=blockTime]');

    is_event.click(function () {
        if (!$(this).prop("checked")) {
            event_count.val('');
            event_count.prop('disabled', true);

            event_change_status.val('');
            event_change_status.trigger('change');
            event_change_status.prop('disabled', true);

            event_change_queue.val('');
            event_change_queue.trigger('change');
            event_change_queue.prop('disabled', true);

            event_type.val('');
            event_type.trigger('change');
            event_type.prop('disabled', true);
        } else {
            event_change_status.prop('disabled', false);
            event_change_queue.prop('disabled', false);
            event_type.prop('disabled', false);
            event_count.prop('disabled', false);
        }
    });

    var selects = ['eventChangeSubStatus', 'changeSubStatusFromStrategy'];

    //for(var k in selects){
    //    $("[name*="+selects[k]+"]").trigger('change');
    //    getSubStatusesByStatus($(this).val(), selects[k]);
    //}

    event_change_status.change(function () {
        if ($(this).val() != '') {
            event_change_queue.val('');
            event_change_queue.trigger('change');

            getSubStatusesByStatus($(this).val(), 'eventChangeSubStatus');
        } else {
            $(this).find('option').remove();
            $("[name*=eventChangeSubStatus]").prop('disabled', true);
        }
    });

    $('#select-changeStatus').on('change', function () {
        if ($(this).val() != '') {
            getSubStatusesByStatus($(this).val(), 'changeSubStatusFromStrategy');
        } else {
            $(this).find('option').remove();
            $("[name*=changeSubStatusFromStrategy]").prop('disabled', true);
        }
    });

    event_change_queue.change(function () {
        if ($(this).val() != null) {
            event_change_status.val('');
            event_change_status.trigger('change');
            event_change_sub_status.val('');
            event_change_sub_status.trigger('change');
        }
    });

    $(document.body).on("change.queue.control", '[name="Queue[primary_partner]"]', function () {
        getQueuesByParams();
    });

    $(document.body).on("change.queue.control", '[name="Queue[primary_country][]"], [name*=country_id]', function () {
        getQueuesByParams();
    });

    event_blur_blockTime.on('blur', function () {
        if ($(this).val() != "" && $(this).val() < 30) {
            toastr.error($('.alert-block-time').text());
            $(this).val('30');
        }
    });

    function getQueuesByParams() {
        $("#event_change_queue").prop("disabled", true);
        $.ajax({
            url: '/queue/control/get-queues-by-params',
            type: 'POST',
            data: {
                partner_id: $('[name="Queue[primary_partner]"]').val(),
                country_id: $('[name="Queue[primary_country][]"]').val(),
                is_primary: $('#queue-is_primary').prop('checked') === true ? 1 : 0,
                secondary_country_id: $('[name*=country_id]').val(),
            },
            success: function (data) {
                $("#event_change_queue").html('').select2({data: JSON.parse(data)});
                if ($('#is_event').prop('checked')) {
                    $("#event_change_queue").prop("disabled", false);
                }

                $("#select-transferToQueue").empty();
                $("#select-transferToQueue").append(new Option('-', '')).val('');

                $("#select-transferToQueue").select2({data: JSON.parse(data)});
                $("#select-transferToQueue").select2();

                $("[name*=transferToQueue]").val($("[name=queue_trigger]").val());
                $("[name*=transferToQueue]").trigger('change');
                $("[name*=eventChangeQueue]").val($("[name=queue_event]").val());
                $("[name*=eventChangeQueue]").trigger('change')
            }
        });
    }

    function getSubStatusesByStatus(status, nameElem) {
        var subStatusField = $("[name*=" + nameElem + "]");
        var submit = $('.btn-success');

        submit.prop('disabled', true);
        subStatusField.prop('disabled', true);

        $.ajax({
            url: '/queue/control/get-sub-statuses-by-status',
            type: 'post',
            data: {
                status_id: status
            },
            success: function (data) {
                var response = JSON.parse(data);
                console.log('response', typeof response);
                console.log('response', response.length);
                subStatusField.prop('disabled', false);

                if (Object.keys(response).length > 0) {
                    subStatusField.find('option').remove();

                    for (var k in response) {
                        $(new Option(response[k], k, false, false)).appendTo(subStatusField);
                    }

                    subStatusField.select2();
                }

                submit.prop('disabled', false);
                subStatusField.prop('disabled', false);
            },
            error: function (error) {
                submit.prop('disabled', false);

                toastr.error(error.responseText);
            }
        });
    }
});



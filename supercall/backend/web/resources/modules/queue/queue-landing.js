$(function () {
    var container = $('.landings');

    var QLanding = {
        container: container,
        rows: '.landing',
        add_btn: '.add-landing',
        remove_btn: '.remove-landing',
        init: function () {
            if(QLanding.container.find(QLanding.rows).length === 1){
                $(QLanding.remove_btn).hide();
            }else{
                $(QLanding.remove_btn).show();
                $(QLanding.remove_btn).last().hide();

                $(QLanding.add_btn).hide();
                $(QLanding.add_btn).last().show();
            }
        },
        add: function () {
            var row = QLanding.container.find(QLanding.rows).last().clone();

            row.find("[type=text]").val('');
            row.appendTo(QLanding.container);

            $(QLanding.add_btn).off('click').on('click', QLanding.add);
            $(QLanding.remove_btn).on('click', function () {
                QLanding.remove($(this));
            });

            QLanding.init();
        },
        remove: function (btn) {
            $(btn).closest('.landing').remove();
            QLanding.init();
        }
    };

    $(QLanding.add_btn).on('click', QLanding.add);

    $(QLanding.remove_btn).on('click', function () {
        QLanding.remove($(this));
    });

    QLanding.init();
});



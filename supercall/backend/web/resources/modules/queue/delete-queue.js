$(document).ready(function () {
    var modal = $("#modal_similar_queues");
    var btn_delete = $('#change_queue');

    btn_delete.prop('disabled', true);

    btn_delete.on('click', function(){
        changeQueue(modal.attr('data-queue_id'));
        modal.modal('hide');
    });

    $('[data-action="delete"]').map(function () {
        $(this).on('click', function (e) {
            e.preventDefault();

            var is_empty = $('td[data-count-id="'+$(this).attr("data-id")+'"]').text() == '0';

            if(is_empty){
                changeQueue($(this).attr("data-id"));
            }else {

                var primary_queue = $('#primary_queues');
                var secondary_queue = $('#secondary_queues');

                modal.attr('data-queue_id', $(this).data('id'));

                blurModal(0.5);

                modal.modal('show');

                primary_queue.prop('disabled', true);
                primary_queue.css('width', '100%');
                secondary_queue.prop('disabled', true);
                secondary_queue.css('width', '100%');
                //$('#change_queue').prop('disabled', true);

                getSimilarQueues($(this).data('id'));
            }

            return false;
        });
    });

    $("#primary_queues").on('change', function () {
        var secondary_queues = $("#secondary_queues");

        if ($(this).val() !== '-') {
            console.log($(this).val())
            secondary_queues.val('-');
            secondary_queues.trigger('change');
            $("#change_queue").prop('disabled', false);
        } else {
            if (secondary_queues.val() === '-') {
                $("#change_queue").prop('disabled', true);
            }
        }
    });

    $("#secondary_queues").on('change', function () {
        var primary_queues = $("#primary_queues");

        if ($(this).val() !== '-') {
            primary_queues.val('-');
            primary_queues.trigger('change');
            $("#change_queue").prop('disabled', false);
        } else {
            if (primary_queues.val() === '-') {
                $("#change_queue").prop('disabled', true);
            }
        }
    });
});

function getSimilarQueues(queue_id) {
    $.ajax({
        url: '/queue/control/get-similar-queues',
        type: 'post',
        data: {
            id: queue_id
        },
        success: function (data) {
            blurModal(1);

            var response = JSON.parse(data);

            if (!response.success) {
                toastr.error(response.message);
            } else {
                var result = response.message;
                var primary_queues = result.primary;
                var secondary_queues = result.secondary;

                var dataPrimary = [];
                var dataSecondary = [];

                for (var k in primary_queues) {
                    var queue = primary_queues[k];
                    dataPrimary.push({id: queue.id, text: queue.name})
                }

                if (dataPrimary.length > 0) {
                    dataPrimary.push({id:'', text:'-'});
                    dataPrimary.reverse();
                    addQueueToList($("#primary_queues"), dataPrimary);
                }else{
                    addQueueToList($("#primary_queues"), [{id:'', text:'-'}]);
                }

                for (var k in secondary_queues) {
                    var queue = secondary_queues[k];
                    dataSecondary.push({id: queue.id, text: queue.name})
                }

                if (dataSecondary.length > 0) {
                    dataSecondary.push({id:'', text:'-'});
                    dataSecondary.reverse();
                    addQueueToList($("#secondary_queues"), dataSecondary);
                }else{
                    addQueueToList($("#secondary_queues"), [{id:'', text:'-'}]);
                }

                $('#change_queue').prop('disabled', false);
            }
        },
        error: function (error) {
            toastr.error(error.responseText);
            $("#modal_similar_queues").modal('hide');
        }
    });
}

function changeQueue(target_queue_id) {
    blurModal(0.5);
    //$('#change_queue').prop('disabled', true);

    var queue_id;
    var primary_queue = $('#primary_queues');
    var secondary_queue = $('#secondary_queues');

    if (primary_queue.val() === '-') {
        queue_id = secondary_queue.val();
    } else {
        queue_id = primary_queue.val();
    }

    $('[data-key="'+target_queue_id+'"]').css('opacity', 0.5);
    $('body').css('cursor', 'wait');

    $.ajax({
        url: '/queue/control/change-queue',
        data: {
            target_queue_id: target_queue_id,
            queue_id: queue_id
        },
        type: 'post',
        success: function (data) {
            var response = JSON.parse(data);

            if (!response.success) {
                toastr.error(response.message);
            } else {
                toastr.success(response.message);
                $("#modal_similar_queues").modal('hide');
            }

            blurModal(1);
            $('#change_queue').prop('disabled', false);
            $('[data-key="'+target_queue_id+'"]').hide();
            $('body').css('cursor', 'default');
        },
        error: function (error) {
            toastr.error(error.responseText);
            $("#modal_similar_queues").modal('hide');
            $('[data-key="'+target_queue_id+'"]').css('opacity', 1);
            $('body').css('cursor', 'default');
        }
    });

    $('*[data-id="'+target_queue_id+'"]').closest('ul').hide();
}

function addQueueToList(select, data) {
    select.html('').select2({data: data});
    select.select2();
    select.prop('disabled', false);
}


function blurModal(opacity) {
    $("#modal_similar_queues").find('.modal-body').css('opacity', opacity);
}
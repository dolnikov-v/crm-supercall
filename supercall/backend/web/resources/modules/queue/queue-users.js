$(document).ready(function () {
    var QueueUsers = {
        initCheckboxes : function(){
            var checkboxes = $("#attached_users").find(':checkbox');

            checkboxes.map(function () {
                $(this).on("click.queue.users", function () {
                    var action = $(this).prop('checked') ? 'queue-set' : 'queue-un-set';

                    $.ajax({
                        url: '/operator/list-operators/' + action,
                        type: 'POST',
                        data: {
                            queue_id: $(this).data('queue_id'),
                            user_id: $(this).data('user_id')
                        },
                        success: function (data) {
                            var result = JSON.parse(data);

                            if(result.success){
                                toastr.success(result.message);
                            }else{
                                toastr.error(result.message);
                            }
                        }
                    });
                });
            });
        }
    }

    QueueUsers.initCheckboxes();
    //необходимо обновлять список операторов, которые подходят для очереди по параметру "Страны"
    $(document.body).on("change.getusers",'[name="Queue[primary_country][]"], [name*=country_id]',function(){
        var queue_id = $('[name="queue_id"]').val();
        var boxContent = $("#attached_users");
        boxContent.html('Wait, there is a selection of available operators...');

        $.ajax({
            url : '/queue/control/get-users-for-queue',
            type: 'POST',
            data : {
                queue_id : $('[name="queue_id"]').val(),
                country_ids : $(this).val(),
                json : true,
                is_primary : $('#queue-is_primary').prop('checked') === true ? 1 : 0,
                secondary_country_id : $('[name*=country_id]').val(),
            },
            success : function(data){
                var result = JSON.parse(data);
                var operators = result.operators;
                var attachedOperators = result.attachedOperators;
                var content = '';

                for(var k in operators){
                    var checked = $.inArray(parseInt(k), attachedOperators)>=0 ? 'checked' : '';
                    content += '<div class="row">';
                    content += '<div class="col-lg-1"><input type="checkbox" name="attach['+k+']" '+checked+' data-queue_id="'+queue_id+'" data-user_id="'+k+'"></div>';
                    content += '<div class="col-lg-1">'+k+'</div>';
                    content += '<div class="col-lg-10">'+operators[k]+'</div>';
                    content += '</div>';
                }
                if(typeof operators == 'object') {
                    boxContent.html(Object.keys(operators).length == 0 ? 'There are no available operators' : content);
                    QueueUsers.initCheckboxes();
                }
            }
        });
    });

    $('[name*=primary_country]').trigger('change');
});
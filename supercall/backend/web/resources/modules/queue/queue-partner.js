$(function () {
    var PartnerCountry = {
        is_primary: $('[name*=is_primary]'),
        partner: $('[name*=partner_id]'),
        init: function () {
            $(PartnerCountry.is_primary).on('change', function () {
                if ($(this).prop('checked')) {
                    PartnerCountry.clearPartner();
                    PartnerCountry.disablePartner();
                }else{
                    PartnerCountry.enablePartner();
                }
            });
        },
        clearPartner: function () {
            $(PartnerCountry.partner).val('');
            $(PartnerCountry.partner).trigger('change');
        },
        disablePartner: function () {
            $(PartnerCountry.partner).prop('disabled', true);
        },
        enablePartner: function () {
            $(PartnerCountry.partner).prop('disabled', false);
        }
    };

    PartnerCountry.init();
});
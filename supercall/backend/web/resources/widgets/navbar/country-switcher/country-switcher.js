$(function () {
    $('#country_switcher_menu').on('click', NavbarCountrySwitcher.toggleCountries);
    $('#country_switcher_searcher_input').on('keydown', NavbarCountrySwitcher.searchCountry);
});

var NavbarCountrySwitcher = {
    toggleCountries: function () {
        var $input = $('#country_switcher_searcher_input');

        $input.val('');
        $input.trigger('keydown');


        setTimeout(function () {
            $input.focus();
        }, 0);
    },

    searchCountry: function (e) {
        setTimeout(function () {
            if (e.keyCode == 27) {
                $('#country_switcher_dropdown').trigger('click');
            } else {
                var search = $('#country_switcher_searcher_input').val().toLowerCase();
                var $menu = $('#country_switcher_dropdown_menu .list-group');

                if (search == '') {
                    $menu.find('.list-group-item').removeClass('no-border');
                    $menu.find('.list-group-item').removeClass('hidden');
                } else {
                    $menu.find('.list-group-item').each(function () {
                        $(this).removeClass('no-border');
                        var text = $(this).data('name').toLowerCase();

                        if (text.indexOf(search) == -1) {
                            $(this).addClass('hidden');
                        } else {
                            $(this).removeClass('hidden');
                        }
                    });
                }

                $menu.find('.list-group-item:not(.hidden):first').addClass('no-border');
            }
        }, 10);
    }
};

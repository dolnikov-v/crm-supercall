$(document).ready(function () {
    setBodySmall();

    var hpanelDefault = $('.content').find('.hpanel.panel-collapse');
    var bodyDefault = hpanelDefault.find('div.panel-body');
    var formBlockDefault = bodyDefault.find('form');
    useFilterString(formBlockDefault, true)

    $('.hide-menu').on('click', function(event){
        event.preventDefault();

        if ($(window).width() < 769) {
            $("body").toggleClass("show-sidebar");
        } else {
            $("body").toggleClass("hide-sidebar");
        }
    });

    $('#side-menu').metisMenu();

    if (false) {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });
    }

    $('.animate-panel').animatePanel();

    $('.showhide').on('click', function (event) {
        event.preventDefault();
        var hpanel = $(this).closest('div.hpanel');
        var icon = $(this).find('i:first');
        var body = hpanel.find('div.panel-body');
        var footer = hpanel.find('div.panel-footer');
        body.slideToggle(300);
        footer.slideToggle(200);

        icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        hpanel.toggleClass('').toggleClass('panel-collapse');

        // body is close,   = true
        var contentClosed = hpanel.hasClass('panel-collapse')
        var contentVisible = contentClosed ? 0 : 1;
        var formBlock = body.find('form');
        var url = formBlock.attr('action')

        // create new Url
        var urlArray = url.split('/');

        // create url - KeySession for data
        var urlShort = url.split('/');
        urlShort.splice(0, 1);
        var urlShort = urlShort.join('/');

        // update array
        urlArray[urlArray.length - 1] = 'visible-block'; // the main action in the backend/components/Controller
        var newUrl = urlArray.join('/');

        $.post( newUrl, {"keySession": urlShort, "value": contentVisible}, "json" )
          .done(function(response) {});

        // show string filter
        if (contentClosed) {
          useFilterString(formBlock, true)
        } else {
          useFilterString(formBlock, false)
        }

        setTimeout(function () {
            hpanel.resize();
            hpanel.find('[id^=map-]').resize();
        }, 50);
    });

    $('.closebox').on('click', function (event) {
        event.preventDefault();
        var hpanel = $(this).closest('div.hpanel');
        hpanel.remove();
        if($('body').hasClass('fullscreen-panel-mode')) { $('body').removeClass('fullscreen-panel-mode');}
    });

    $('.fullscreen').on('click', function() {
        var hpanel = $(this).closest('div.hpanel');
        var icon = $(this).find('i:first');
        $('body').toggleClass('fullscreen-panel-mode');
        icon.toggleClass('fa-expand').toggleClass('fa-compress');
        hpanel.toggleClass('fullscreen');
        setTimeout(function() {
            $(window).trigger('resize');
        }, 100);
    });

    $('.right-sidebar-toggle').on('click', function () {
        $('#right-sidebar').toggleClass('sidebar-open');
    });

    $('.small-header-action').on('click', function(event){
        event.preventDefault();
        var icon = $(this).find('i:first');
        var breadcrumb  = $(this).parent().find('#hbreadcrumb');
        $(this).parent().parent().parent().toggleClass('small-header');
        breadcrumb.toggleClass('m-t-lg');
        icon.toggleClass('fa-arrow-up').toggleClass('fa-arrow-down');
    });

    setTimeout(function () {
        fixWrapperHeight();
    }, 0);

    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]"
    });

    $("[data-toggle=popover]").popover();

    $('.modal').appendTo("body");

});

$(window).bind("load", function () {
    $('.splash').css('display', 'none');
});

$(window).bind("resize click", function () {
    setBodySmall();

    setTimeout(function () {
        fixWrapperHeight();
    }, 300);
});

/**
 * show filterString
 *
 * @param formBlock
 * @param flag
 */
function useFilterString(formBlock, flag) {
  // default, bllock with data of filterString is hide
  if (flag === false) {
    $('.filter-string').empty();
    return true;
  }

  var $result = [];
  formBlock.find('.form-group').each(function() {
    var $this = $(this); //this указывает на текущий .form-group
    var labelText = '&nbsp;&nbsp;<span class="label label-info">' + $this.find('label').text() + '</span>'

    var $input = $this.find('input[type=text]')
    var $select = $this.find('select')

    if ($input.val() === undefined) {
      var selectedText = $this.find('select option:selected').text()
      var selecteValue = $this.find('select option:selected').val()
      var spanText = $this.find('span.range-value').text()

      // other fields
      if (selecteValue !== undefined && selecteValue.length > 0) {
        $result.push(labelText + ' ' + selectedText)
      }
      // for datepicker range calendar
      if (spanText.length > 0) {
          $result.push(labelText + ' ' + spanText)
      }

    } else {
      if ($input.val() != '') {
        $result.push(labelText + ' ' + $input.val())
      }
    }

  })
  $result = unique($result)
  $result.join(" ")
  $('.filter-string').html($result);
}

/**
 * For find unique elements of array
 *
 * https://learn.javascript.ru/task/array-unique
 * @param arr
 * @returns {Array}
 */
function unique(arr) {
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true; // запомнить строку в виде свойства объекта
  }

  return Object.keys(obj); // или собрать ключи перебором для IE8-
}

function fixWrapperHeight() {
    var headerH = 62;
    var navigationH = $("#navigation").height();
    var contentH = $(".content").height();

    if (contentH < navigationH) {
        $("#wrapper").css("min-height", navigationH + 'px');
    }

    if (contentH < navigationH && navigationH < $(window).height()) {
        $("#wrapper").css("min-height", $(window).height() - headerH  + 'px');
    }

    if (contentH > navigationH && contentH < $(window).height()) {
        $("#wrapper").css("min-height", $(window).height() - headerH + 'px');
    }
}

function setBodySmall() {
    var $body = $('body');

    if ($(this).width() < 769) {
        $body.addClass('page-small');
    } else {
        $body.removeClass('page-small');
        $body.removeClass('show-sidebar');
    }
}

$.fn['animatePanel'] = function() {
    var element = $(this);
    var effect = $(this).data('effect');
    var delay = $(this).data('delay');
    var child = $(this).data('child');

    if(!effect) { effect = 'zoomIn'}
    if(!delay) { delay = 0.06 } else { delay = delay / 10 }
    if(!child) { child = '.row > div'} else {child = "." + child}

    var startAnimation = 0;
    var start = Math.abs(delay) + startAnimation;

    var panel = element.find(child);
    panel.addClass('opacity-0');

    panel = element.find(child);
    panel.addClass('stagger').addClass('animated-panel').addClass(effect);

    var panelsCount = panel.length + 10;
    var animateTime = (panelsCount * delay * 10000) / 10;

    panel.each(function (i, elm) {
        start += delay;
        var rounded = Math.round(start * 10) / 10;
        $(elm).css('animation-delay', rounded + 's');
        $(elm).removeClass('opacity-0');
    });

    setTimeout(function(){
        var $stagger = $('.stagger');

        $stagger.css('animation', '');
        $stagger.removeClass(effect).removeClass('animated-panel').removeClass('stagger');
    }, animateTime);
};

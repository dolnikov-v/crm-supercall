$(document).ready(function () {
    window.ATL_JQ_PAGE_PROPS = {
        "triggerFunction": function(showCollectorDialog) {
            jQuery("#sendErrorToJira").click(function(e) {
                e.preventDefault();
                showCollectorDialog();
            });
        },
        fieldValues: {
            description: window.location.href + '\n',
            components: '10208',
            email: $('#user_email').text()
        }
    };
    jQuery.ajax({
        url: $('#jira_issue_collectors_url').text() + '&collectorId=' + $("#jira_issue_collectors_language").text(),
        type: "get",
        cache: true,
        dataType: "script"
    });
});

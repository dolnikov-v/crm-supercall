$(function () {
    MainBootstrap.init();
    MainRemark.init();
});

var MainBootstrap = {
    init: function () {
        var url = document.location.toString();

        if (url.match('#')) {
            $('.nav-tabs a[href=#' + url.split('#')[1] + ']').tab('show');
        }

        $('.nav-tabs a').on('shown.bs.tab', MainBootstrap.showNavTab);

        $('.check-input-holder .checkbox-custom input').on('click', function () {
            var inp = $(this).closest('.check-input-holder').find('.form-control');
            if ($(this).is(':checked')) {
                inp.prop('disabled', false);
            }
            else {
                inp.prop('disabled', true);
                inp.val('');
            }
        });
    },

    showNavTab: function (e) {
        history.pushState({}, '', e.target.href);
    },

    addNotification: function (message, type) {
        $.notify({message: message}, {
            type: type + " dark",
            animate: {exit: 'hide'},
            z_index: 2031
        });
    },
    addNotificationDanger: function (message) {
        MainBootstrap.addNotification(message, 'danger');
    },
    addNotificationSuccess: function (message) {
        MainBootstrap.addNotification(message, 'success');
    }
};

var MainRemark = {
    init: function () {
        MainRemark.checkStatusPanel();

        $('.panel').on('shown.uikit.panel', function () {
            MainRemark.savePanelCollapse($(this).attr('id'), true);
        });

        $('.panel').on('hidden.uikit.panel', function () {
            MainRemark.savePanelCollapse($(this).attr('id'), false);
        });
    },
    savePanelCollapse: function (id, status) {
        if (id) {
            lscache.set(id, status);
        }
    },
    checkStatusPanel: function () {
        var collapse = $('[data-toggle="panel-collapse"]');

        $('.panel').each(function () {
            var attr = $(this).attr('id');
            if (attr != null) {
                if (lscache.get(attr) == null) {
                    $(this).removeClass('is-collapse');
                } else {
                    if (lscache.get(attr) == true) {
                        $(this).removeClass('is-collapse');
                        collapse.removeClass('wb-plus').addClass('wb-minus');
                    } else {
                        $(this).addClass('is-collapse');
                        collapse.removeClass('wb-minus').addClass('wb-plus');
                    }
                }
            }
        });
    }
};


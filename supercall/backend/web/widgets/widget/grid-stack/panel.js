$(function () {
    //фильтры в виджетах
    window.widgetsParams = {};
    var widgetFilterPanel = $('[data-toggle="panel-filter"]');

    widgetFilterPanel.map(function () {
        if (typeof $(this).html() != 'undefined') {
            var submit = $(this).find('.filter-submit').find('button');

            var form = $(this).closest('form');
            var id = form.closest('.panel-wrap-data').data('id');
            var widgetDom = $(this);

            window.widgetsParams[id] = {
                id: id,
                filter: form.serialize()
            }

            submit.click(function () {
                window.widgetsParams[id].filter = form.serialize();
                getRefreshDataWidget(window.widgetsParams[id], widgetDom);
            });
        }
    });

    var $widgetStack = $('#widget-stack');

    $widgetStack.find('[data-toggle="panel-close"]').click(function () {

        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            id: $(this).closest('.panel-wrap-data').data('id')
        };

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/widget/index/delete',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                $.notify({message: 'Действие выполнено.'}, {
                    type: "success dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            },
            error: function () {
                $.notify({message: 'Не удалось выполнить запрос.'}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        });

        $widgetStack.find('.grid-stack').data('gridstack').remove_widget($(this).closest('.grid-stack-item'));
    });

    $widgetStack.find('.grid-stack').on('change', function (e, items) {

        if (!items) return false;

        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            items: []
        };

        $.each(items, function () {

            var $el = $(this.el);
            var $panelWrap = $el.find('.panel-wrap-data');
            var $panel = $panelWrap.children('.panel');
            //не уловил зачем это? теперь у нас есть футер и с ним не получилось нормально
            //$panel.children('.panel-body').outerHeight(this.height * 100 - 40 - $panel.children('.panel-heading').height()- $panel.children('.panel-footer').height());

            dataAjax.items.push({
                id: $panelWrap.data('id'),
                options: {
                    height: this.height,
                    width: this.width,
                    x: this.x,
                    y: this.y
                }
            });
        });

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/widget/index/change-place',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                $.notify({message: 'Действие выполнено.'}, {
                    type: "success dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            },
            error: function () {
                $.notify({message: 'Не удалось выполнить запрос.'}, {
                    type: "danger dark",
                    animate: {exit: 'hide'},
                    z_index: 2031
                });
            }
        });

    });
});

function getRefreshDataWidget(params, widgetDom) {
    var body = widgetDom.closest(".panel").find('.panel-body').find('.innerbody');
    var footer = widgetDom.closest(".panel").find('.panel-footer');

    body.css('opacity', 0.5);
    body.css('cursor', 'wait');

    $.ajax({
        type: 'POST',
        url: '/widget/index/refresh',
        dataType: 'json',
        data: params,
        success: function (response, textStatus) {
            if (typeof response.data === 'string') {
                body.html(response.data);
                body.css('opacity', 1);
                body.css('cursor', 'default');
            }

            footer.html(response.time);
        },
        error: function () {
            $.notify({message: 'Не удалось обновить виджет.'}, {
                type: "danger dark",
                animate: {exit: 'hide'},
                z_index: 2031
            });
        }
    });

}

$(document).on('click.site.panel', '[data-toggle="panel-refresh"]', function (e) {
    var widgetDom = $(this).closest('.panel-wrap-data');
    var id = widgetDom.data('id');
    var params = {
        id: widgetDom.data('id'),
        filter: $(this).closest('form').serialize()
    };

    if (typeof window.widgetsParams == 'object' && typeof window.widgetsParams[id] == 'object') {
        params = window.widgetsParams[id];
    }

    getRefreshDataWidget(params, widgetDom);
});


function widget_refresh() {
    //исходники есть на пее.
    return;
}
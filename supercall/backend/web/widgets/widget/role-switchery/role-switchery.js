$(function () {
    $('input[data-widget="role-switchery"]').on('change', function () {
        var csrfParam = $('meta[name=csrf-param]').attr("content");
        var csrfToken = $('meta[name=csrf-token]').attr("content");

        var dataAjax = {
            type_id: $(this).closest('table').data('type-id'),
            role: $(this).closest('td').data('role'),
            value: $(this).is(':checked') ? 'on' : 'off'
        };

        dataAjax[csrfParam] = csrfToken;

        $.ajax({
            type: 'POST',
            url: '/widget/type/set-role',
            dataType: 'json',
            data: dataAjax,
            success: function (response, textStatus) {
                $.notify({message: 'Действие выполнено.'}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
            },
            error: function () {
                $.notify({message: 'Не удалось выполнить запрос.'}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
            }
        });
    });
});

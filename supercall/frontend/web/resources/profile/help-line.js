$(function () {
    const NAME_CALL_BTN = 'call';
    const NAME_HANG_BTN = 'hang';

    window.helplineReady = false;

    var HelpLine = {
        modal_window: $('#modal_call_to'),
        modal_number: $('.call_to'),
        container: $('.helpline_container'),
        zoiper_dial_number: $('#phoneNumper_dial'),
        zoiper_dial_call_btn: $('#call_btn'),
        zoiper_dial_reset_btn: $('#reset_btn'),
        call_btn: $('.call_btn'),
        hang_btn: $('.hang_btn'),
        close_btn: $('.close_btn'),
        buttons: $('.helpline'),
        init: function () {
            //до инициализации Zoiper - звонить не получиться
            HelpLine.container.find('a').prop('disabled', true).css('color', '#D4D4D4');

            //при перезагрузке страницы - слетает номер в титле и это нормально
            HelpLine.modal_window.on('shown.bs.modal', function () {
                if (HelpLine.modal_number.text() == '') {
                    //закроем модальное окно
                    HelpLine.modal_window.modal('hide');
                }
            });

            //если решили модальное окно закрыть - разговор нужно сбросить - т.к. элементы ууправления звонком - только в модальном окне
            HelpLine.modal_window.on('hide.bs.modal', function () {
                HelpLine.hang_btn.trigger('click');
            });

            //крестик там абсалютно не нужен
            HelpLine.modal_window.find('.close').hide();

            //закрывать модалку по фону - при том - что при закрытии модалки - звонок будет сброшен - не нужно. это действие может быть выполнено случайно
            HelpLine.modal_window.modal({
                backdrop: 'static',
                keyboard: false
            });

            //номер телефона в титл модалки
            HelpLine.buttons.on('click', function () {
                console.log(' window.helplineReady',  window.helplineReady);
                if(! window.helplineReady){
                    return;
                }

                HelpLine.modal_number.text($(this).data('phone'));
                HelpLine.show();
            });

            //кнопка сброса - сразу не активна
            HelpLine.hang_btn.css('opacity', 0.3);

            HelpLine.call_btn.on('click', function () {
                //call
                HelpLine.manageBtn($(this), HelpLine.hang_btn);
            });

            HelpLine.hang_btn.on('click', function () {
                //hang
                HelpLine.manageBtn($(this), HelpLine.call_btn);
            });

            return false;
        },
        show: function () {
            HelpLine.modal_window.modal('show');
        },
        hide: function () {
            HelpLine.modal_window.modal('hide');
        },
        call: function () {
            HelpLine.zoiper_dial_number.val(HelpLine.modal_number.text());
            HelpLine.zoiper_dial_call_btn.trigger('click');
        },
        hang: function () {
            HelpLine.zoiper_dial_reset_btn.trigger('click');
            HelpLine.zoiper_dial_number.val('');
        },
        manageBtn: function (btn, depend) {
            if (btn.data('click')) {
                return;
            }

            btn.data('click', !btn.data('click'));
            btn.css('opacity', !btn.data('click') ? 1 : 0.3);

            depend.css('opacity', !btn.data('click') ? 0.3 : 1);
            depend.data('click', !btn.data('click'));

            if (btn.prop('name') == NAME_CALL_BTN) {
                HelpLine.call();
            } else if (btn.prop('name') == NAME_HANG_BTN) {
                HelpLine.hang();
            }
        }
    };

    HelpLine.init();

    function IframeZoiper(command) {
        window.frames.iframeZoiper.postMessage(command, "*");
    }

    if (window.addEventListener) {
        window.addEventListener("message", listenIframe);
    } else {
        window.attachEvent("onmessage", listenIframe);
    }

    function listenIframe(msg) {
        listenEventAndAction(msg.data);
    }

    function listenEventAndAction(event) {


        switch (event) {
            case 'init' :
                console.log('HelpLine - Zoiper is ' + event);
                break;
            case 'register' :
                console.log('HelpLine - Zoiper is ' + event);
                window.helplineReady = true;
                HelpLine.container.find('a').prop('disabled', true).css('color', 'black');
                break;
            case 'unregister' :
                window.helplineReady = false;
                HelpLine.container.find('a').prop('disabled', true).css('color', '#D4D4D4');
                console.log('HelpLine - Zoiper is ' + event);
                break;
            case 'failed_register' :
                window.helplineReady = false;
                HelpLine.container.find('a').prop('disabled', true).css('color', '#D4D4D4');
                console.log('HelpLine - Zoiper is ' + event);
                break;
            case 'ring' :
                break;
            case 'hang' :
                break;
        }
    }
});



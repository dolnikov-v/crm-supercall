$(document).ready(function () {
    //30 минут в пассивном режиме в iframe - если нет keydown - разлогивание
    const TIMER_ACTIVITY = 1800;

    //!!!!!!! сброс счётчика в 0 при keydown по iframe
    //common/web/resources/modules/call/call-history/call-history.js
    /**
     *  необходимо перехватить вызов JS функции
     *  оригинал https://true-coder.ru/javascript/perexvat-vyzova-funkcii-v-javascript.html
     *  frontend\assets\widgets\navbar\OrderOnlineAsset
     *  @frontend/web/resources/widgets/navbar/order-online/order-online.js
     *  function foundedOrder()
     */
    if (typeof foundedOrder == 'function') {
        var logInterceptor = new Interceptor(listenerCalledFunction);
        foundedOrder = logInterceptor.interceptInvokes(foundedOrder);
    }

    //вызов функции отслеживания активности оператора - при вызове
    function listenerCalledFunction(callback, args) {
        //заказ найден
        if (args[0] == 'founded') {
            setTimeout(function () {
                ActivityOperator();
            }, 3000);
        } else {
            //либо заказ блокирован, либо идёт поиск
            if (typeof timerActivityId != 'undefined') {
                clearInterval(timerActivityId);
                window.ActivityOperator = {};
                window.ActivityOperator.timer = 0;
            }
        }
    }

    //Функция отслеживания активности оператора
    function ActivityOperator() {
       if (typeof timerActivityId != 'undefined') {
            clearInterval(timerActivityId);
        }

        window.ActivityOperator = {};
        window.ActivityOperator.timer = 0;

        timerActivityId = setInterval(function () {
            //10 минут
            //+20сек сохранение заказа, после чего перезапуск счётчика
            //в файлы:
            //frontend/web/resources/widgets/navbar/sipml5-slide/sipml5-wcall.js
            //frontend/web/resources/widgets/navbar/zoiper-slide-style/bootsrap.js
            // в event добавлены активности звонков parent.window.ActiveCall
            //console.log('ActiveCall', window.ActiveCall);
            if (window.ActivityOperator.timer >= TIMER_ACTIVITY) {
                //активный звонок
                if(window.ActiveCall){
                    console.log('Active Call in progress');
                    window.ActivityOperator.timer = 0;
                }else {
                    //снять готовность
                    console.log('Ahtung! Log out procedure start');
                    $("#ready-button").prop("checked", false);
                    $("#ready-button").click();
                    //разлогинить оператора
                    clearInterval(timerActivityId);
                    location.href = "/auth/logout";
                }
                return;
            } else {
                window.ActivityOperator.timer += 1;
            }
        }, 1000);
    }
});

function emptyFunction() {
}

function Interceptor(preInvoke, postInvoke) {
    var preInvoke = typeof preInvoke === 'function' ? preInvoke : emptyFunction;
    var postInvoke = typeof postInvoke === 'function' ? postInvoke : emptyFunction;

    this.preInvoke = preInvoke;
    this.postInvoke = postInvoke;
}

Interceptor.prototype = {
    constructor: Interceptor,
    interceptInvokes: function (callback) {
        var self = this;
        return function () {
            var args = Array.prototype.slice.call(arguments, 0),
                result;

            self.preInvoke.call(self, callback, args);
            result = callback.apply(self, args);
            self.postInvoke.call(self, callback, args, result);

            return result;
        };
    }
}
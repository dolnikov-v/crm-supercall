$(function () {
    var AutoCallActivity = {
        time: Math.ceil(window.performance.now()),
        modal_window: $('#modal_confirm_auto_call_activity'),
        modal_confirm_btn: $('#modal_confirm_activity_ok'),
        ready_btn: $("#ready-button"),
        limitAutoCallActivity: 600,
        seconds: 0,
        timer: {},
        init: function () {
            console.log('AutoCallActivity init');
        },
        //старт отсчёте пассивности оператора
        begin: function () {
            if ($(AutoCallActivity.ready_btn).prop("checked")) {
                AutoCallActivity.timer = setInterval(function () {
                    AutoCallActivity.seconds += 1;
                    if (AutoCallActivity.seconds > AutoCallActivity.limitAutoCallActivity) {
                        console.log('Set user not ready');
                        AutoCallActivity.modal_window.modal('show');
                        AutoCallActivity.setNotReady();
                        clearInterval(AutoCallActivity.timer);
                        AutoCallActivity.seconds = 0;
                        AutoCallActivity.timer = {};
                    }
                }, 1000);
            }
        },
        //выставить готовность оператора принимать звонки
        setReady: function () {
            $(AutoCallActivity.ready_btn).trigger('click');
        },
        //поставить оператора на паузу
        setNotReady: function () {
            $(AutoCallActivity.ready_btn).trigger('click');
        },
        //обнулять таймер, если оператор использует мышь (т.е. он находиться на рабочем месте)
        setMouseEvent: function () {
            $('body').on('mousemove click keypress', function () {
                if ($(AutoCallActivity.ready_btn).prop("checked")) {
                    AutoCallActivity.resetTimer();
                }
            });
        },
        //после подтверждения в модальном окне - запустить счётчик активноси по новой
        initConfirmActivity: function () {
            $(AutoCallActivity.modal_confirm_btn).on('click', function () {
                AutoCallActivity.modal_window.modal('hide');
                AutoCallActivity.setReady();
                AutoCallActivity.resetTimer();
            });
        },
        //сбросить счётчик на 0
        resetTimer: function () {
            AutoCallActivity.seconds = 0;
            clearInterval(AutoCallActivity.timer);
            AutoCallActivity.begin();
        }
    };

    //запуск отслеживания активности - только для auto_call = true
    //все атрибуты модели пользователя - теперь доступны на клиенте - после авторизации
    //данные на клиент попадают из frontend/widgets/aside/views/profile.php

    //Отслеживать пассивность всех операторов
    //https://2wtrade-tasks.atlassian.net/browse/WCALL-512
    //if (window.user && window.user.auto_call == 1) {
        AutoCallActivity.init();
        AutoCallActivity.begin();
        AutoCallActivity.setMouseEvent();
        AutoCallActivity.initConfirmActivity();
    //}

    //listen iframe
    if (window.addEventListener) {
        window.addEventListener("message", listenIframe);
    } else {
        window.attachEvent("onmessage", listenIframe);
    }

    //action for listing iframe
    function listenIframe(msg) {
        listenEventAndAction(msg.data);
    }

    function listenEventAndAction(event) {
        console.log('Event: ' + event);
        switch(event){
            case 'form-order-loaded' :
                $('#order-online').contents().find('body').on('mousemove click keypress', function () {
                    if ($(AutoCallActivity.ready_btn).prop("checked")) {
                        AutoCallActivity.resetTimer();
                    }
                });

                break;
        }
    }
});


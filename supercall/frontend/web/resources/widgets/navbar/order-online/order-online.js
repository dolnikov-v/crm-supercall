document.domain = $("#document_domain").text();


$(document).ready(function () {
    var timerId;
    var param_get_order;
    var sip_host,
        sip_pass,
        sip_login;

    // interval 10 s for get order
    getOrderOnline()

});

/**
 * switch on Search of Order
 * get one order for operator
 */
function getOrderOnline() {
    // Параметр для определения необходимости запроса нового заказа
    window.param_get_order = 1;
    window.param_search_form = 0;

    internalOrderOnline();

    //if ($("#user_auto_call").text() != 1) {
    window.timerId = setInterval(function () {
        internalOrderOnline()
    }, 2000);
    //}
}

// global function for order after save
function frontend_set_callback_param_get_order() {
    if (window.param_search_form == 0) {
        window.param_get_order = 1
        // проверка на вкл.флаг "Неготов после сохранения"
        if ($('#check-unready-button').prop('checked')) {
            $('#ready-button').prop('checked', false)
            changeReady('is_ready', 0);
            useDisabled();
            setTimeout(function () {
                $('#wrapper .content').html('<h2>Pause</h2>')
            }, 1000)
        }
    }
}

/**
 * internal function for searching order for Operators
 */
function internalOrderOnline() {
    if ($('#ready-button').prop('checked') && !$('#check-unready-button').prop('checked') && window.param_get_order == 1) {
        var param = $('meta[name=csrf-param]').attr("content");
        var token = $('meta[name=csrf-token]').attr("content");

        var data = {};
        data[param] = token;

        $.ajax({
            url: "/order/change/get-order-form",
            type: 'POST',
            data: data,
            statusCode: {
                500: function (error) {
                    KVS.set(KVS.ACTION_ORDER, 'error: ' + error.responseText);
                }
            },
            success: function (response, textStatus, jqXHR) {
                if (response.result === 'ok') {
                    var $height = $('#wrapper').height();

                    $('#wrapper .content').html('<iframe id="order-online" src="' +
                        response.url + '" frameborder="0"  width="100%" height="100%"></iframe>');

                    foundedOrder('founded');

                    // частный случай, вдруг случится
                    if (response.content == "Заказ заблокирован") {
                        window.param_get_order = 1
                        foundedOrder('blocked');

                    } else {
                        var iframe = $('#order-online', parent.document.body);
                        var $offset = $('#wrapper').offset().top;
                        var $diff = $height - $offset + 100;
                        iframe.height($diff);
                        $('#reset_btn').prop('disabled', false);
                        window.param_get_order = 0 // don't loading yet order
                    }

                }
                // we are searching order
                else if (response.result === 'searching') {
                    foundedOrder('search');
                    $('#wrapper .content').html('<h2>' + response.message + '</h2>')
                }
                else if (response.result === 'search_form') {
                    foundedOrder('search_form');
                    $('#wrapper .content').html(response.content);
                    SearchOrderForm.init();
                    window.param_get_order = 0;
                    window.param_search_form = 1;
                    SearchOrderForm.init();
                }
                // error request
                else if (response.result === 'error') {
                    console.log(response.message)
                }

                if (typeof KVS !== 'undefined') {
                    if (response.result == 'ok') {
                        KVS.set(KVS.ACTION_ORDER, response.order.order_id);
                    } else {
                        if(/error/.test(response)){
                            var parsedResponse = JSON.parse(response);
                            KVS.set(KVS.ACTION_ORDER, parsedResponse.result);
                        }else {
                            KVS.set(KVS.ACTION_ORDER, response.result);
                        }
                    }
                }

            }
        });

    }
}

//пустышка, когда найден заказ для оператора
//по её вызову отслеживается активность оператора
function foundedOrder(type) {

}


window.onbeforeunload = function(e) {
    var dialogText = 'Reloading of page may result in loss of order data.';
    e.returnValue = dialogText;
    return dialogText;
};
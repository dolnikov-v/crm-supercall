$(function () {
    var Monitor = {
        INTERVAL: 30000,
        WINDOW: window,
        PAUSED: 1,
        UNPAUSED: 0,
        AUTO_CALL_ON: 1,
        loader: '.loader',
        indicator: '#queue-indicator',
        modal: '#list-queue-modal',
        modalBody: '.list-queues',
        modalHeader: '.modal-header-text',
        lastUpdateQueues: '.last-update-queues',
        btn_open_modal: '.open-list-queue',
        status: '.status-queue',
        status_on: 'Queue on',
        status_off: 'Queue off',
        status_error: 'Error',
        indicator_red: 'dot-red',
        indicator_green: 'dot-green',
        token: 'd07cd74cac05dd29d1a77334c4aaa0da704a2afb',
        //url_asterisk: 'http://mcall-aster2.rtbads.com/scripts/auto-call/monitor.php',
        url: '/profile/auto-call/monitor',
        action_queue: 'queues',
        action_state: 'state',
        updateInterval: null,
        endRequest: true,
        init: function () {
            $(Monitor.btn_open_modal).on('click', Monitor.setEventShowQueues);
            $(Monitor.modal).on('show.bs.modal', Monitor.beforeShowModal);
            $(Monitor.modal).on('hide.bs.modal', Monitor.afterHideModal);

            var user = Monitor.getUserData();

            if(typeof user === 'object' && user.auto_call === Monitor.AUTO_CALL_ON) {
                setInterval(function () {
                    if (!Monitor.endRequest) {
                        return false;
                    } else {
                        Monitor.getState();
                    }
                }, Monitor.INTERVAL);
            }
        },
        getQueues: function () {
            var user = Monitor.getUserData();

            if (typeof user != 'object') {
                Monitor.setErrorModal('User data is not object');
            } else {
                var sip_data = Monitor.getSipData(user);
                Monitor.setHeaderModal(sip_data.sip_login);

                Monitor.endRequest = false;

                $.ajax({
                    url: Monitor.url,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        token: Monitor.token,
                        sip_login: sip_data.sip_login,
                        action: Monitor.action_queue
                    },
                    success: function (data) {
                        if (data.success) {
                            Monitor.setBodyModal(Monitor.createTable(data.message));
                            Monitor.loaderHide();
                        } else {
                            Monitor.setErrorModal(data.message);
                            Monitor.loaderHide();
                        }

                        Monitor.setLastUpdateQueues();

                        Monitor.endRequest = true;
                    },
                    error: function (error) {
                        console.log('error', error);
                        Monitor.setErrorModal(error.responseText);
                        Monitor.loaderHide();
                        Monitor.setLastUpdateQueues();

                        Monitor.endRequest = true;
                    }
                });
            }
        },
        setLastUpdateQueues: function () {
            $(Monitor.lastUpdateQueues).text('Last update: ' + Monitor.getTime());
        },
        renderStateQueue: function (state) {
            if (state == Monitor.PAUSED) {
                return '<span class="' + Monitor.indicator_red + '"></span>';
            } else if (state == Monitor.UNPAUSED) {
                return '<span class="' + Monitor.indicator_green + '"></span>';
            } else {
                return 'unknow state';
            }
        },
        startUpdateInterval: function () {
            console.log('start interval');
            Monitor.updateInterval = setInterval(function () {
                Monitor.getQueues()
            }, 3000);
        },
        stopUpdateInterval: function () {
            console.log('stop interval');
            clearInterval(Monitor.updateInterval);
        },
        createTable: function (data) {
            if(data.length > 0) {
                var table = $('<table/>', {
                    class: 'table table-striped'
                });

                $(table).append('<thead><tr><th>Table</th><th class="text-center">Paused</th></tr></thead>');
                $(table).append($('<tbody/>'));

                for (var k in data) {

                    $(table).find('tbody').append('<tr><td>' + data[k].queue + '</td><td class="text-center">' + Monitor.renderStateQueue(data[k].state) + '</td></tr>');
                }

                return $(table);
            }else{
                return '';
            }
        },
        setErrorModal: function (error) {
            $(Monitor.modalHeader).text('Error');
            $(Monitor.modalBody).html('<span class="text-danger">' + error + '</span>');
        },
        setHeaderModal: function (sip_login) {
            $(Monitor.modalHeader).text(sip_login);
        },
        setBodyModal: function (data) {
            $(Monitor.modalBody).html(data);
        },
        setEventShowQueues: function (e) {
            e.stopImmediatePropagation();
            $(Monitor.modal).modal('show');
        },
        beforeShowModal: function () {
            Monitor.loaderShow();
            Monitor.getQueues();
            Monitor.startUpdateInterval();
        },
        afterHideModal: function () {
            Monitor.startUpdateInterval();
        },
        loaderShow: function () {
            $(Monitor.loader).removeClass('hidden');
        },
        loaderHide: function () {
            $(Monitor.loader).addClass('hidden');
        },
        setIndicatorRed: function () {
            $(Monitor.indicator).removeClass(Monitor.indicator_green).addClass(Monitor.indicator_red);
        },
        setIndicatorGreen: function () {
            $(Monitor.indicator).removeClass(Monitor.indicator_red).addClass(Monitor.indicator_green);
        },
        setStatusOn: function () {
            $(Monitor.status).text(Monitor.status_on);
        },
        setStatusOff: function () {
            $(Monitor.status).text(Monitor.status_off);
        },
        changeStateGreen: function () {
            Monitor.setIndicatorGreen();
            Monitor.setStatusOn();
        },
        changeStateRed: function () {
            Monitor.setIndicatorRed();
            Monitor.setStatusOff();
        },
        setError: function (error) {
            Monitor.setIndicatorRed();
            $(Monitor.status).text(Monitor.status_error);
            $(Monitor.status).attr('title', error);
        },
        getTime: function () {
            return moment().format('H:mm:ss');
        },
        setLastUpdate: function () {

            $(Monitor.status).attr('title', 'Last update: ' + Monitor.getTime());
        },
        removeError: function () {
            $(Monitor.status).prop('title', '');
        },
        getUserData: function () {
            return Monitor.WINDOW.user;
        },
        getSipData: function (user) {
            return JSON.parse(user.sip);
        },
        getState: function () {
            var user = Monitor.getUserData();

            if (typeof user != 'object') {
                Monitor.setError('Monitor.WINDOW.user is not Object');
            } else {

                var sip_data = Monitor.getSipData(user);

                $.ajax({
                    url: Monitor.url,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        action: Monitor.action_state,
                        sip_login: sip_data.sip_login,
                    },
                    success: function (data) {
                        if (!data.success) {
                            Monitor.setError(data.message);
                        } else {
                            if (data.message.length > 0) {
                                Monitor.changeStateRed();
                                if (typeof KVS !== 'undefined') {
                                    KVS.set(KVS.ACTION_QUEUE_PAUSED, Monitor.PAUSED);
                                }
                            } else {
                                Monitor.changeStateGreen();
                                if (typeof KVS !== 'undefined') {
                                    KVS.set(KVS.ACTION_QUEUE_PAUSED, Monitor.UNPAUSED);
                                }
                            }

                            Monitor.setLastUpdate();
                        }
                    },
                    error: function (error) {
                        Monitor.setError(error.responseText);
                    }
                });
            }
        }
    };

    Monitor.init();
});
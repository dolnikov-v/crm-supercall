$(document).ready(function () {
    txtDisplayName = "oper1";
    txtPrivateIdentity = "oper1";
    txtPublicIdentity = "sip:oper1" + $("#asterisk_identity_tpl").text();
    txtPassword = "passwordoper1";

    txtRealm = $("#asterisk_server").text();
    txtWebsocketServerUrl = $("#asterisk_websocket").text();
    txtIceServers = $("#asterisk_ice_server").text();

    window.localStorage.setItem('org.doubango.expert.websocket_server_url', txtWebsocketServerUrl);
    window.localStorage.setItem('org.doubango.expert.ice_servers', '');
    window.localStorage.setItem('org.doubango.expert.disable_early_ims', "true");
    window.localStorage.setItem('org.doubango.expert.disable_debug', $("#asterisk_sipml5_disabled_logs").text()); //true to  more and more and more log in console
    window.localStorage.setItem('org.doubango.expert.enable_media_caching', "true");
    window.localStorage.setItem('org.doubango.expert.enable_rtcweb_breaker',"true");

    // to avoid caching
    //if (window.location.href.indexOf("svn=") == -1) {
    //    window.location.href += (window.location.href.indexOf("?") == -1 ? "svn=236" : "&svn=229");
    //}

    var sTransferNumber;
    var oRingTone, oRingbackTone;
    var oSipStack, oSipSessionRegister, oSipSessionCall, oSipSessionTransferCall;
    var videoRemote, videoLocal, audioRemote;
    var bFullScreen = false;
    var oNotifICall;
    var bDisableVideo = false;
    var viewVideoLocal, viewVideoRemote, viewLocalScreencast;

    audioRemote = document.getElementById('audio_remote');
    var oConfigCall = {
        audio_remote: audioRemote,
        events_listener: { events: '*', listener: onSipEventSession },
        sip_caps: [
            { name: '+g.oma.sip-im' },
            { name: 'language', value: '\"en,fr\"' }
        ]
    };

    var oReadyStateTimer;

    C = {
        divKeyPadWidth: 220
    };

    function ListenerEvents(event) {
        console.log('%c EVENT: '+event,  'background: #FF00AE; color: white');
        switch (event) {
            case 'register':
                break;
            case 'unregister':
            case 'disconnect':
                window.listenEventAndAction('unregister');
                break;
            case 'connected':
                window.listenEventAndAction('register');
                break;
            case 'ring' :
                window.listenEventAndAction('ring');
                break;
            case 'assept' :
                window.listenEventAndAction('accept');
                break;
            case 'reject' :
                window.listenEventAndAction('reject');
                break;
            case 'hangup' :
                break;
            case  'incoming_call':
                window.listenEventAndAction('incoming');
                break;
            case 'hold' :
                window.listenEventAndAction('hold');
                break;
            case 'unhold' :
                window.listenEventAndAction('unhold');
                break;
            case 'mute' :
                window.listenEventAndAction('mute');
                break;
            case 'unmute':
                window.listenEventAndAction('unmute');
                break;
        }
    }

    window.ListenerEvents = function(event) {
        ListenerEvents(event);
    }

    window.console && window.console.info && window.console.info("location=" + window.location);

    // set debug level
    SIPml.setDebugLevel((window.localStorage && window.localStorage.getItem('org.doubango.expert.disable_debug') == "true") ? "error" : "info");

    sipRegister();

    function postInit() {
        // check for WebRTC support
        if (!SIPml.isWebRtcSupported()) {
            // is it chrome?
            if (SIPml.getNavigatorFriendlyName() == 'chrome') {
                if (confirm("You're using an old Chrome version or WebRTC is not enabled.\nDo you want to see how to enable WebRTC?")) {
                    window.location = 'http://www.webrtc.org/running-the-demos';
                }
                else {
                    window.location = "index.html";
                }
                return;
            }
            else {
                if (confirm("webrtc-everywhere extension is not installed. Do you want to install it?\nIMPORTANT: You must restart your browser after the installation.")) {
                    window.location = 'https://github.com/sarandogou/webrtc-everywhere';
                }
                else {
                    // Must do nothing: give the user the chance to accept the extension
                    // window.location = "index.html";
                }
            }
        }

        // checks for WebSocket support
        if (!SIPml.isWebSocketSupported()) {
            if (confirm('Your browser don\'t support WebSockets.\nDo you want to download a WebSocket-capable browser?')) {
                window.location = 'https://www.google.com/intl/en/chrome/browser/';
            }
            else {
                window.location = "index.html";
            }
            return;
        }

        // FIXME: displays must be per session
        viewVideoLocal = videoLocal;
        viewVideoRemote = videoRemote;

        if (!SIPml.isWebRtcSupported()) {
            if (confirm('Your browser don\'t support WebRTC.\naudio/video calls will be disabled.\nDo you want to download a WebRTC-capable browser?')) {
                window.location = 'https://www.google.com/intl/en/chrome/browser/';
            }
        }

        btnRegister.disabled = false;
        document.body.style.cursor = 'default';
        oConfigCall = {
            audio_remote: audioRemote,
            // video_local: viewVideoLocal,
            // video_remote: viewVideoRemote,
            // screencast_window_id: 0x00000000, // entire desktop
            // bandwidth: { audio: undefined, video: undefined },
            // video_size: { minWidth: undefined, minHeight: undefined, maxWidth: undefined, maxHeight: undefined },
            events_listener: { events: '*', listener: onSipEventSession },
            sip_caps: [
                { name: '+g.oma.sip-im' },
                { name: 'language', value: '\"en,fr\"' }
            ]
        };
    }


    var getPVal = function (PName) {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) === PName) {
                return decodeURIComponent(pair[1]);
            }
        }
        return null;
    }


    var preInit = function () {
        // set default webrtc type (before initialization)
        var s_webrtc_type = getPVal("wt");
        var s_fps = getPVal("fps");
        var s_mvs = getPVal("mvs"); // maxVideoSize
        var s_mbwu = getPVal("mbwu"); // maxBandwidthUp (kbps)
        var s_mbwd = getPVal("mbwd"); // maxBandwidthUp (kbps)
        var s_za = getPVal("za"); // ZeroArtifacts
        var s_ndb = getPVal("ndb"); // NativeDebug

        if (s_webrtc_type) SIPml.setWebRtcType(s_webrtc_type);

        // initialize SIPML5
        SIPml.init(postInit);

        // set other options after initialization
        if (s_fps) SIPml.setFps(parseFloat(s_fps));
        if (s_mvs) SIPml.setMaxVideoSize(s_mvs);
        if (s_mbwu) SIPml.setMaxBandwidthUp(parseFloat(s_mbwu));
        if (s_mbwd) SIPml.setMaxBandwidthDown(parseFloat(s_mbwd));
        if (s_za) SIPml.setZeroArtifacts(s_za === "true");
        if (s_ndb == "true") SIPml.startNativeDebug();

        //var rinningApps = SIPml.getRunningApps();
        //var _rinningApps = Base64.decode(rinningApps);
        //tsk_utils_log_info(_rinningApps);
    }


    function saveCredentials() {
        if (window.localStorage) {
            window.localStorage.setItem('org.doubango.identity.display_name', txtDisplayName);
            window.localStorage.setItem('org.doubango.identity.impi', txtPrivateIdentity);
            window.localStorage.setItem('org.doubango.identity.impu', txtPublicIdentity);
            window.localStorage.setItem('org.doubango.identity.password', txtPassword);
            window.localStorage.setItem('org.doubango.identity.realm', txtRealm);
        }
    }

    function loadCredentials() {
        if (window.localStorage) {
            // IE retuns 'null' if not defined
            var s_value;
            if ((s_value = window.localStorage.getItem('org.doubango.identity.display_name'))) txtDisplayName = s_value;
            if ((s_value = window.localStorage.getItem('org.doubango.identity.impi'))) txtPrivateIdentity = s_value;
            if ((s_value = window.localStorage.getItem('org.doubango.identity.impu'))) txtPublicIdentity = s_value;
            if ((s_value = window.localStorage.getItem('org.doubango.identity.password'))) txtPassword = s_value;
            if ((s_value = window.localStorage.getItem('org.doubango.identity.realm'))) txtRealm = s_value;
        }
    }

    // sends SIP REGISTER request to login
    function sipRegister() {
        // catch exception for IE (DOM not ready)
        try {
            console.log('SipML5: trying to register');

            var o_impu = tsip_uri.prototype.Parse(txtPublicIdentity);
            if (!o_impu || !o_impu.s_user_name || !o_impu.s_host) {
                console.log('[' + txtPublicIdentity + '] is not a valid Public identity');
                return;
            }

            // enable notifications if not already done
            if (window.webkitNotifications && window.webkitNotifications.checkPermission() != 0) {
                window.webkitNotifications.requestPermission();
            }

            saveCredentials();

            oSipStack = new SIPml.Stack({
                    realm: txtRealm,
                    impi: txtPrivateIdentity,
                    impu: txtPublicIdentity,
                    password: txtPassword,
                    display_name: txtDisplayName,
                    websocket_proxy_url: (window.localStorage ? window.localStorage.getItem('org.doubango.expert.websocket_server_url') : null),
                    outbound_proxy_url: (window.localStorage ? window.localStorage.getItem('org.doubango.expert.sip_outboundproxy_url') : null),
                    ice_servers: (window.localStorage ? window.localStorage.getItem('org.doubango.expert.ice_servers') : null),
                    enable_rtcweb_breaker: (window.localStorage ? window.localStorage.getItem('org.doubango.expert.enable_rtcweb_breaker') == "true" : false),
                    events_listener: {events: '*', listener: onSipEventStack},
                    enable_early_ims: (window.localStorage ? window.localStorage.getItem('org.doubango.expert.disable_early_ims') != "true" : true), // Must be true unless you're using a real IMS network
                    enable_media_stream_cache: (window.localStorage ? window.localStorage.getItem('org.doubango.expert.enable_media_caching') == "true" : false),
                    bandwidth: (window.localStorage ? tsk_string_to_object(window.localStorage.getItem('org.doubango.expert.bandwidth')) : null), // could be redefined a session-level
                    video_size: (window.localStorage ? tsk_string_to_object(window.localStorage.getItem('org.doubango.expert.video_size')) : null), // could be redefined a session-level
                    sip_headers: [
                        {name: 'User-Agent', value: 'IM-client/OMA1.0 sipML5-v1.2016.03.04'},
                        {name: 'Organization', value: 'Doubango Telecom'}
                    ]
                }
            );
            if (oSipStack.start() != 0) {
                console.log('SipML5 : Failed to start the SIP stack');
            }
            else {
                ListenerEvents('register');
                console.log('SipML5: is registered');
                return;
            }
        }
        catch (e) {
            console.log('%c SipML5: register is failed: ' + e, 'background: #FF0000; color: white');
        }
    }

    // sends SIP REGISTER (expires=0) to logout
    function sipUnRegister() {
        if (oSipStack) {
            // shutdown all sessions
            oSipStack.stop();
            ListenerEvents('unregister');
            console.log('SipML5: is unregistered');
        }else{
            console.log('SipML5: is not registered');
        }
    }

    // makes a call (SIP INVITE)
    function sipCall(s_type, number_phone) {
        window.ListenerEvents('ring');
        console.log(s_type, number_phone);
        //console.log(1, (oSipStack && !oSipSessionCall && !tsk_string_is_null_or_empty(number_phone))); //false
        //console.log(2, oSipStack && !oSipSessionCall); //false
        //console.log(3, oSipStack);
        //console.log(4, !oSipSessionCall); // false
        //console.log(5, !tsk_string_is_null_or_empty(number_phone));

        if (oSipStack && !oSipSessionCall && !tsk_string_is_null_or_empty(number_phone)) {
            if (s_type == 'call-screenshare') {
                if (!SIPml.isScreenShareSupported()) {
                    alert('Screen sharing not supported. Are you using chrome 26+?');
                    return;
                }
                if (!location.protocol.match('https')) {
                    if (confirm("Screen sharing requires https://. Do you want to be redirected?")) {
                        sipUnRegister();
                    }
                    return;
                }
            }
            //btnCall.disabled = true;
            //btnHangUp.disabled = false;

            if (window.localStorage) {
                oConfigCall.bandwidth = tsk_string_to_object(window.localStorage.getItem('org.doubango.expert.bandwidth')); // already defined at stack-level but redifined to use latest values
                oConfigCall.video_size = tsk_string_to_object(window.localStorage.getItem('org.doubango.expert.video_size')); // already defined at stack-level but redifined to use latest values
            }

            // create call session
            oSipSessionCall = oSipStack.newSession(s_type, oConfigCall);
            // make call
            if (oSipSessionCall.call(number_phone) != 0) {
                console.log('SipML5: calling to: ' + number_phone);
                oSipSessionCall = null;
                console.log('SipML5: Failed to make call');
                return;
            }
            saveCallOptions(number_phone);
        }
        else if (oSipSessionCall) {
            console.log('SipML5: Connecting...');
            oSipSessionCall.accept(oConfigCall);
        }
    }

    window.sipCall = function(s_type, number_phone){
        sipCall(s_type, number_phone)
    }

    function saveCallOptions(number_phone) {
        if (window.localStorage) {
            window.localStorage.setItem('org.doubango.call.phone_number', number_phone);
            window.localStorage.setItem('org.doubango.expert.disable_video', bDisableVideo ? "true" : "false");
        }
    }


    // holds or resumes the call
    function sipToggleHoldResume() {
        if (oSipSessionCall) {
            var i_ret;
            //btnHoldResume.disabled = true;
            console.log('SipML5: ' + (oSipSessionCall.bHeld ? 'Resuming the call...' : 'Holding the call...'));
            i_ret = oSipSessionCall.bHeld ? oSipSessionCall.resume() : oSipSessionCall.hold();

            if (i_ret != 0) {
                console.log('SipML5: Hold / Resume failed');
                return;
            }
        }
    }

    window.sipToggleHoldResume = function(){
        sipToggleHoldResume();
    }

    // Mute or Unmute the call
    function sipToggleMute() {
        if (oSipSessionCall) {
            var i_ret;
            var bMute = !oSipSessionCall.bMute;
            console.log('SipML5: ' + (bMute ? 'Mute the call...' : 'Unmute the call...'));
            i_ret = oSipSessionCall.mute('audio'/*could be 'video'*/, bMute);
            if (i_ret != 0) {
                console.log('SipML5: Mute / Unmute failed');
                return;
            }

            oSipSessionCall.bMute = bMute;
        }
    }

    window.sipToggleMute = function(){
        sipToggleMute();
    }

    // terminates the call (SIP BYE or CANCEL)
    function sipHangUp() {

        if (oSipSessionCall) {
            console.log('SipML5: HangUp & Terminating the call...');
            ListenerEvents('hangup');
            oSipSessionCall.hangup({events_listener: {events: '*', listener: onSipEventSession}});
        }
    }

    window.sipHangUp = function(){
        sipHangUp();
    }

    function uiCallTerminated(s_description) {
        if (window.btnBFCP) window.btnBFCP.disabled = true;

        oSipSessionCall = null;

        stopRingbackTone();
        stopRingTone();

        if (oNotifICall) {
            oNotifICall.cancel();
            oNotifICall = null;
        }
    }

    function sipSendDTMF(c) {
        if (oSipSessionCall && c) {
            if (oSipSessionCall.dtmf(c) == 0) {
                try {
                    dtmfTone.play();
                } catch (e) {
                }
            }
        }
    }

    function startRingTone() {
        try {
            ringtone.play();
        }
        catch (e) {
        }
    }

    function stopRingTone() {
        try {
            ringtone.pause();
        }
        catch (e) {
        }
    }

    function startRingbackTone() {
        try {
            ringbacktone.play();
        }
        catch (e) {
        }
    }

    function stopRingbackTone() {
        try {
            ringbacktone.pause();
        }
        catch (e) {
        }
    }

    // Callback function for SIP Stacks
    function onSipEventStack(e /*SIPml.Stack.Event*/) {
        tsk_utils_log_info('==stack event = ' + e.type);
        switch (e.type) {
            case 'started': {
                // catch exception for IE (DOM not ready)
                try {
                    // LogIn (REGISTER) as soon as the stack finish starting
                    oSipSessionRegister = this.newSession('register', {
                        expires: 200,
                        events_listener: {events: '*', listener: onSipEventSession},
                        sip_caps: [
                            {name: '+g.oma.sip-im', value: null},
                            //{ name: '+sip.ice' }, // rfc5768: FIXME doesn't work with Polycom TelePresence
                            {name: '+audio', value: null},
                            {name: 'language', value: '\"en,fr\"'}
                        ]
                    });
                    oSipSessionRegister.register();
                }
                catch (e) {
                    console.log('SipML5: 1:', e);
                    //btnRegister.disabled = false;
                }
                break;
            }
            case 'stopping':
            case 'stopped':
            case 'failed_to_start':
            case 'failed_to_stop':
                var bFailure = (e.type == 'failed_to_start') || (e.type == 'failed_to_stop');
                oSipStack = null;
                oSipSessionRegister = null;
                oSipSessionCall = null;

                //uiOnConnectionEvent(false, false);

                stopRingbackTone();
                stopRingTone();

                //uiVideoDisplayShowHide(false);
                //divCallOptions.style.opacity = 0;

                console.log('%c SipML5:  ' + (bFailure ? 'Disconnected:' + e.description : 'Disconnected'), 'background: #FF0000; color: white');
                ListenerEvents('disconected');

                break;


            case 'i_new_call':
                if (oSipSessionCall) {
                    // do not accept the incoming call if we're already 'in call'
                    e.newSession.hangup(); // comment this line for multi-line support
                }
                else {
                    oSipSessionCall = e.newSession;
                    // start listening for events
                    oSipSessionCall.setConfiguration(oConfigCall);

                    console.log('SipML5: incomming call');
                    ListenerEvents('incoming_call');

                    console.log('eee', e);
                    console.log('e.newSession', e.newSession)

                    var accept = function() {e.newSession.accept()};

                    $("#zoiper_incoming_btn_accept").on('click.incomming', accept);

                    startRingTone();

                    var sRemoteNumber = (oSipSessionCall.getRemoteFriendlyName() || 'unknown');
                    console.log('SipML5: Incoming call from [' + sRemoteNumber + ']');
                }
                break;


            case 'm_permission_requested': {
                break;
            }
            case 'm_permission_accepted':
            case 'm_permission_refused': {
                if (e.type == 'm_permission_refused') {
                    console.log('Media stream permission denied');
                }
                break;
            }

            case 'starting':

            default:
                break;
        }
    }

    // Callback function for SIP sessions (INVITE, REGISTER, MESSAGE...)
    function onSipEventSession(e /* SIPml.Session.Event */) {
        tsk_utils_log_info('==session event = ' + e.type);

        switch (e.type) {
            case 'connecting':
                ListenerEvents('connecting');
            case 'connected': {
                var bConnected = (e.type == 'connected');
                if (e.session == oSipSessionRegister) {
                    //uiOnConnectionEvent(bConnected, !bConnected);
                    console.log('SipML5:', e.description);
                    ListenerEvents('connected')
                }
                else if (e.session == oSipSessionCall) {
                    //btnHangUp.value = 'HangUp';
                    //btnCall.disabled = true;
                    //btnHangUp.disabled = false;
                    //btnTransfer.disabled = false;
                    if (window.btnBFCP) window.btnBFCP.disabled = false;

                    if (bConnected) {
                        stopRingbackTone();
                        stopRingTone();

                        if (oNotifICall) {
                            oNotifICall.cancel();
                            oNotifICall = null;
                        }
                    }

                    console.log('SipML5: ', e.description);
                    //divCallOptions.style.opacity = bConnected ? 1 : 0;

                    if (SIPml.isWebRtc4AllSupported()) { // IE don't provide stream callback
                        uiVideoDisplayEvent(false, true);
                        uiVideoDisplayEvent(true, true);
                    }
                }
                break;
            } // 'connecting' | 'connected'
            case 'terminating':
            case 'terminated': {
                if (e.session == oSipSessionRegister) {
                    //uiOnConnectionEvent(false, false);

                    oSipSessionCall = null;
                    oSipSessionRegister = null;

                    console.log('SipML5: ', e.description);
                }
                else if (e.session == oSipSessionCall) {
                    //сиповский листенер - слушает hang
                    uiCallTerminated(e.description);
                }
                break;
            } // 'terminating' | 'terminated'

            case 'm_stream_video_local_added': {
                if (e.session == oSipSessionCall) {
                    uiVideoDisplayEvent(true, true);
                }
                break;
            }
            case 'm_stream_video_local_removed': {
                if (e.session == oSipSessionCall) {
                    uiVideoDisplayEvent(true, false);
                }
                break;
            }
            case 'm_stream_video_remote_added': {
                if (e.session == oSipSessionCall) {
                    uiVideoDisplayEvent(false, true);
                }
                break;
            }
            case 'm_stream_video_remote_removed': {
                if (e.session == oSipSessionCall) {
                    uiVideoDisplayEvent(false, false);
                }
                break;
            }

            case 'm_stream_audio_local_added':
            case 'm_stream_audio_local_removed':
            case 'm_stream_audio_remote_added':
            case 'm_stream_audio_remote_removed': {
                break;
            }

            case 'i_ect_new_call': {
                oSipSessionTransferCall = e.session;
                break;
            }

            case 'i_ao_request': {
                if (e.session == oSipSessionCall) {
                    var iSipResponseCode = e.getSipResponseCode();
                    if (iSipResponseCode == 180 || iSipResponseCode == 183) {
                        startRingbackTone();
                        console.log('SipML5: Remote ringing...');
                    }
                }
                break;
            }

            case 'm_early_media': {
                if (e.session == oSipSessionCall) {
                    stopRingbackTone();
                    stopRingTone();
                    console.log('SipML5: Early media started');
                }
                break;
            }

            case 'm_local_hold_ok': {
                if (e.session == oSipSessionCall) {
                    if (oSipSessionCall.bTransfering) {
                        oSipSessionCall.bTransfering = false;
                        // this.AVSession.TransferCall(this.transferUri);
                    }
                    //btnHoldResume.value = 'Resume';
                    //btnHoldResume.disabled = false;
                    console.log('SipML5: Call placed on hold');
                    oSipSessionCall.bHeld = true;
                }
                break;
            }
            case 'm_local_hold_nok': {
                if (e.session == oSipSessionCall) {
                    oSipSessionCall.bTransfering = false;
                    //btnHoldResume.value = 'Hold';
                    //btnHoldResume.disabled = false;
                    console.log('SipML5: Failed to place remote party on hold');
                }
                break;
            }
            case 'm_local_resume_ok': {
                if (e.session == oSipSessionCall) {
                    oSipSessionCall.bTransfering = false;
                    //btnHoldResume.value = 'Hold';
                    //btnHoldResume.disabled = false;
                    console.log('SipML5: Call taken off hold');
                    oSipSessionCall.bHeld = false;

                    if (SIPml.isWebRtc4AllSupported()) { // IE don't provide stream callback yet
                        uiVideoDisplayEvent(false, true);
                        uiVideoDisplayEvent(true, true);
                    }
                }
                break;
            }
            case 'm_local_resume_nok': {
                if (e.session == oSipSessionCall) {
                    oSipSessionCall.bTransfering = false;
                    //btnHoldResume.disabled = false;
                    console.log('SipML5: Failed to unhold call');
                }
                break;
            }
            case 'm_remote_hold': {
                if (e.session == oSipSessionCall) {
                    console.log('SipML5: Placed on hold by remote party');
                }
                break;
            }
            case 'm_remote_resume': {
                if (e.session == oSipSessionCall) {
                    console.log('SipML5: Taken off hold by remote party');
                }
                break;
            }
            case 'm_bfcp_info': {
                if (e.session == oSipSessionCall) {
                    console.log('SipML5: BFCP Info: ' + e.description);
                }
                break;
            }

            case 'o_ect_trying': {
                if (e.session == oSipSessionCall) {
                    console.log('SipML5: Call transfer in progress...');
                }
                break;
            }
            case 'o_ect_accepted': {
                if (e.session == oSipSessionCall) {
                    console.log('SipML5: Call transfer accepted');
                }
                break;
            }
            case 'o_ect_completed':
            case 'i_ect_completed': {
                if (e.session == oSipSessionCall) {
                    console.log('SipML5: Call transfer completed');
                    btnTransfer.disabled = false;
                    if (oSipSessionTransferCall) {
                        oSipSessionCall = oSipSessionTransferCall;
                    }
                    oSipSessionTransferCall = null;
                }
                break;
            }
            case 'o_ect_failed':
            case 'i_ect_failed': {
                if (e.session == oSipSessionCall) {
                    console.log('SipML5: Call transfer failed');
                    btnTransfer.disabled = false;
                }
                break;
            }
            case 'o_ect_notify':
            case 'i_ect_notify': {
                if (e.session == oSipSessionCall) {
                    console.log('SipML5: Call Transfer: ' + e.getSipResponseCode() + ' ' + e.description);
                    if (e.getSipResponseCode() >= 300) {
                        if (oSipSessionCall.bHeld) {
                            oSipSessionCall.resume();
                        }
                        btnTransfer.disabled = false;
                    }
                }
                break;
            }
            case 'i_ect_requested': {
                if (e.session == oSipSessionCall) {
                    var s_message = "Do you accept call transfer to [" + e.getTransferDestinationFriendlyName() + "]?";//FIXME
                    if (confirm(s_message)) {
                        console.log('SipML5: Call transfer in progress...');
                        oSipSessionCall.acceptTransfer();
                        break;
                    }
                    oSipSessionCall.rejectTransfer();
                }
                break;
            }
        }
    }
});

$(document).ready(function () {
    //для инициализации чего-либо при загрузке страницы - использовать onDomReady() - в конце кода

    //кол-во сек от начала разговора для разрыва оператором
    window.DURATION = 15;
    //кол-во сек до разрыва соединения при звонке (с начала звонка)
    window.DURATION_RING = 45; //сек
    //счётчик
    window.duration_ring = 0;
    //готовность по call-history.js
    window.statusReady = false;

    var nums = $(".caller_box .num");
    var phoneNumper = $("#phoneNumper");
    var remove_one = $("#remove_one");
    var phoneNumberDial = $("#phoneNumper_dial");
    var btn_call = $("#call_btn");
    var btn_reset = $("#reset_btn");

    // data for sipml5-core.js
    // constants for connect in sipml5-core.js
    var sip_data = $("#asterisk_user_sip_data").text();
    var sip_user = '';
    var sip_password = '';

    if(sip_data != ''){
        sip_data = JSON.parse(sip_data);
        sip_user = sip_data.sip_login;
        sip_password = sip_data.sip_pass;
        sip_host = sip_data.sip_host;
    }

    //call base functional
    onDomReady();

    function listenEventAndAction(event) {
        var phoneNumber = $('#phoneNumper');
        var reset_btn = $("#reset_btn");
        var call_btn =  $("#call_btn");

        switch (event) {
            case 'init' :
                break;
            case 'register' :
                window.statusReady = true;
                call_btn.prop('disabled', false);
                call_btn.removeClass('btn btn-warning');
                call_btn.addClass('btn btn-success');
                break;
            case 'unregister' :
                window.statusReady = false;
                break;
            case 'failed_register' :
                break;
            case 'ring' :
                console.log('set parent.window.ActiveCall to TRUE');
                parent.window.ActiveCall = true;
                //счётчик от начала разговора
                CallDurationStart();
                break;
            case 'hang' :
                console.log('set parent.window.ActiveCall to FALSE');
                parent.window.ActiveCall = false;
                clearInterval(window.durationRing);
                phoneNumber.css('background', 'white');
                if (reset_btn.prop('type') != 'undefined') {
                    btn_reset.trigger('click');
                } else {
                    $("#reset_btn", parent.document).trigger('click');
                }
                break;
            case 'mute' :
            case 'unmute' :
                window.sipToggleMute(); //in core js
                break;
            case 'hold' :
            case 'unhold' :
                window.sipToggleHoldResume();  //in core js
                break;
            case 'accept' :
                console.log('set parent.window.ActiveCall to TRUE');
                parent.window.ActiveCall = true;
                phoneNumber.css('background', '#F2FFD8');
                var i = 0;
                reset_btn.prop('disabled', true);
                AsseptDurationStart();
                break;
            case 'reject' :
                console.log('set parent.window.ActiveCall to FALSE');
                parent.window.ActiveCall = false;
                clearInterval(window.durationRing);
                phoneNumber.css('background', 'white');
                reset_btn.trigger('click');
                break;
            case 'incoming' :
                $('#zoiper_incoming_modal').modal('show');
                break;
        }

    }

    //OLD ZOIPER FUNCTIONAL

    $("span.input-group-addon").on('click', function (e) {
        //common/web/resourse/modules/call/call-history/call-history.js
    });

    nums.map(function () {
        $(this).click(function () {
            phoneNumper.val(phoneNumper.val() + $(this).text());
            phoneNumberDial.val(phoneNumberDial.val() + $(this).text());
        });
    });

    remove_one.click(function () {
        phoneNumper.val(phoneNumper.val().substring(0, phoneNumper.val().length - 1));
        phoneNumberDial.val(phoneNumberDial.val().substring(0, phoneNumberDial.val().length - 1));
    });

    btn_call.on('click.template.action', function () {
        if(window.statusReady) {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                window.sipHangUp();
                setStateReady();
                phoneNumberDial.val('');

            } else {
                $(this).addClass('active');
                window.sipCall('call-audio', phoneNumberDial.val());
            }
        }
    });

    phoneNumper.on('keyup', function(){
        phoneNumberDial.val($(this).val())
    });

    phoneNumper.on('focus', function(){
        $(this).val('');
    });

    $("#zoiper_incoming_btn_accept").on('click', function () {
        //accept  - search event "click.incomming"
        $(this).addClass('active');
        $("#zoiper_incoming_modal").modal('hide');
        ListenerEvents('incoming_accept');
    });

    btn_reset.click(function () {

        window.sipHangUp();

        console.log('kill window.intervalID');
        clearInterval(window.intervalID);
        clearInterval(window.durationRingInterval);
        $("#call_btn").addClass('btn btn-success');
        $("#call_btn").removeClass('active');

        $("#pause_btn").removeClass('active');
        setStateReady();
        phoneNumberDial.val('');
    });

    $("#pause_btn").click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            ListenerEvents('unhold');
            stateHoldToReady();
        } else {
            $(this).addClass('active');
            ListenerEvents('hold');
            stateReadyToHold();
        }
    });

    $("#settings_btn").click(function () {
        console.log('SipML5 not having settings');
    });

    $("#options_btn").click(function () {
        console.log('SipML5 not having options');
        //$("#zoiper_setting").modal('show');
    });

    $("#zoiper_security_submit").click(function() {
        var h = $("#zoiper_security").val();
        if(h.hashCode() == 751834184){
            $("#zoiper_setting").modal('hide');
            $(".modalZoiper").css('left', '10px');
        }
    });

    $("#log_btn").click(function () {
        console.log('SipML5: is not have log');
    });

    $("#volume_mute_btn").click(function () {
        if($(this).hasClass('active')){
            ListenerEvents('mute');
            $(this).removeClass('active');

        }else{
            ListenerEvents('unmute');
            $(this).addClass('active')
        }
    });

    $("#speaker_mute_btn").click(function () {

        //IframeZoiper("MuteMicToggle()");

        $(this).hasClass('active') ? $(this).removeClass('active') : $(this).addClass('active');
    });

    //for global sipml5-core.js
    window.listenEventAndAction = function(event){
        listenEventAndAction(event);
    }

    String.prototype.hashCode = function(){
        var h = 0;
        if (this.length == 0) return h;
        for (i = 0; i < this.length; i++) {
            char = this.charCodeAt(i);
            h = ((h<<5)-h)+char;
            h = h & h;
        }
        return h;
    }

    function fmtMSS(s) {
        parent.duration = s;

        return (s - (s %= 60)) / 60 + (9 < s ? ':' : ':0') + s;
    }

    //Стартовый набор операций при загрузки страницы
    function onDomReady(){
        btn_reset.prop('disabled', true);
        setStateReady();
        btn_call.prop('disabled', true);

        phoneNumper.on('focus', function(){
            $(this).val('');
        });

        phoneNumper.on('blur', function(){
            if($(this).val() == ''){
                $(this).val('READY');
            }
        });
    }

    function setStateReady(){
        phoneNumper.val('READY');
    }

    function stateReadyToHold(){
        phoneNumper.val(phoneNumper.val().replace('RING', 'HOLD'));
    }

    function stateHoldToReady(){
        phoneNumper.val(phoneNumper.val().replace('HOLD', 'RING'));
    }

    function AsseptDurationStart(){
        window.intervalID = setInterval(function () {
            //https://2wtrade-tasks.atlassian.net/browse/WCALL-9
            //15 после активного разговора - кнопка сброс не доступна
            //плагин около номера контролируется кодом в common/web/resourse/modules/call/call-history/call-history.js
            if(i>=window.DURATION){
                $("#reset_btn").prop('disabled', false);
            }

            $("#time").val(fmtMSS(i++));
        }, 1000);
    }

    function CallDurationStart(){
        window.durationRingInterval = setInterval(function(){
            window.duration_ring++;
            //
            if(window.duration_ring >= window.DURATION){
                $("#reset_btn").prop('disabled', false);
            }
        }, 1000);
    }
});

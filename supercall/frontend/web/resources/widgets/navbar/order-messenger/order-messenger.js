$(function () {
    const LOCAL = false;
    const UserRoomEvent = 'joinUserRoom';
    const OrderRoomEvent = 'joinroom';

    var serverNodeJS = LOCAL ? 'http://socket2wcall.local' : 'http://socket.2wcall.com:44377';

    var OrderMessenger = {
        container: $('.content'),
        socket: null,
        user_id: $('#user_id').text(),
        token: 'cGxhdGludW0=',
        iframe: '#order-online',
        checkOrdersFromQueueUrl: '/order/live-message/get-order-from-queue',
        getOrderFormUrl: '/order/live-message/get-order-form',
        waitOrderMessage: '<h2><i class="fa fa-circle-o-notch fa-spin" style="font-size:25px"></i> Please wait, we are looking for a new order for you</h2>',
        errorMessage: '<h2>There was an error, please refresh the page</h2>',
        newOrderInQueueMessage: '<span><small></small>A new order was placed in the queue<br/></small></span>',
        checkingQueueMessage: '<span><small>A compatibility check...</small><br/></span>',
        dosntMatchMessage: '<span><small>Order does not match the parameters<br/></small></span>',
        orderIsFoundedMessage: '<h2>Order found: #{order_id}</h2>',
        socketServer: serverNodeJS,
        init: function () {
            OrderMessenger.waitOrder();
            OrderMessenger.listenWS();
            OrderMessenger.checkOrderInWork();
        },
        checkOrderInWork: function () {
            toastr.options = {
                "positionClass": "toast-bottom-right"
            };

            $.ajax({
                url: '/order/live-message/check-order-in-work',
                type: 'post',
                data: {
                    user_id: window.user.id
                },
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        if(data.message) {
                            OrderMessenger.OrderFound({order_id: data.message});
                            console.log('Order found: ' + data.message);
                        }else{
                            console.log('Order in work not found');
                        }
                    }else{
                        toastr.error(data.message + "<br/> Please reload page.");
                    }
                },
                error: function (error) {
                    toastr.error(error.responseText + "<br/> Please reload page.");
                }
            });
        },
        listenWS: function () {
            var socket = io.connect(OrderMessenger.socketServer);
            //Заглобалим
            OrderMessenger.socket = socket;

            console.log('socket ' + serverNodeJS, socket);

            socket.emit(UserRoomEvent, OrderMessenger.user_id);

            debug('user joined in room (' + UserRoomEvent + '): #' + OrderMessenger.user_id);

            socket.on('catchorder', function (msg) {

                var iframeIsReady = true;
                if ($('iframe').is(OrderMessenger.iframe)) {
                    iframeIsReady = $(OrderMessenger.iframe).data('hidden');
                    console.log('--->', $(OrderMessenger.iframe).data())
                }

                console.log(' $(OrderMessenger.iframe)',  $(OrderMessenger.iframe).data());

                /*if (iframeIsReady) {
                 document.getElementById('order-online').remove();
                 $(OrderMessenger.iframe).remove();
                 iframeIsReady = false;
                 }*/

                debug('order catched: ');
                debug(msg);
                debug('listen: iframe is ready: ');
                debug('iframeIsReady', iframeIsReady);

                if(typeof msg.operator_id == 'undefined'){
                    msg.operator_id = windows.user.id;
                    msg.roomForOrder = windows.user.id;
                }

                if (iframeIsReady) {
                    if ($('iframe').is(OrderMessenger.iframe)) {
                        document.getElementById('order-online').remove();
                    }
                    OrderMessenger.eventFunction(msg);
                }

            });
        },
        eventFunction: function (msg) {
            console.log('MSG', msg)
            //пришла очередь заказов
            if (msg.queue) {
                debug('check orders from queue');
                console.log(msg);
                OrderMessenger.getSuitableOrderFromQueue(msg.queue);
            } else {
                //пришёл заказ
                OrderMessenger.OrderFound(msg);
            }
        },
        waitOrder: function () {
            debug('wait a order');
            $(OrderMessenger.waitOrderMessage).appendTo(OrderMessenger.container);
        },
        getSuitableOrderFromQueue: function (order_ids) {
            OrderMessenger.clearLog();
            $(OrderMessenger.newOrderInQueueMessage).appendTo(OrderMessenger.container);
            $(OrderMessenger.checkingQueueMessage).appendTo(OrderMessenger.container);

            $.ajax({
                url: OrderMessenger.checkOrdersFromQueueUrl,
                type: 'post',
                data: {
                    order_ids: order_ids,
                    user_id: OrderMessenger.user_id
                },
                success: function (data) {
                    console.log('data', data);
                    var response = JSON.parse(data);

                    if (response.success) {
                        debug('order_id from queue: ' + response.message);
                        OrderMessenger.clearLog();
                        OrderMessenger.OrderFound({order_id: response.message});
                    } else {
                        $(OrderMessenger.dosntMatchMessage).appendTo(OrderMessenger.container);

                        console.log('not found order from queue: ', response);

                        setTimeout(function () {
                            OrderMessenger.clearLog();
                        }, 3500);

                        return false;
                    }
                },
                error: function (error) {
                    console.log('error: ', error.responseText);
                    return false;
                }
            });
        },
        clearLog: function () {
            OrderMessenger.container.find('span').fadeOut('slow');
            OrderMessenger.container.find('span').remove();
        },
        OrderFound: function (data) {
            var iframeIsReady = true;
            if ($('iframe').is(OrderMessenger.iframe)) {
                iframeIsReady = $(OrderMessenger.iframe).data('hidden');
            }

            OrderMessenger.container.find('h2').hide();
            $(OrderMessenger.orderIsFoundedMessage.replace('{order_id}', data.order_id)).appendTo(OrderMessenger.container);

            debug('order found: iframe is ready: ' + iframeIsReady);

            //если форма уже загружена - то стопэ
            if (iframeIsReady) {
                if ($('iframe').is(OrderMessenger.iframe)) {
                    document.getElementById('order-online').remove();
                }
                OrderMessenger.getOrderForm(data);
            }
        },
        getOrderForm: function (data) {
            $.ajax({
                url: OrderMessenger.getOrderFormUrl,
                type: 'post',
                data: data,
                success: function (data) {
                    var parsedData = JSON.parse(data);
                    var iframe = OrderMessenger.getIframe(parsedData);

                    OrderMessenger.setIframe(iframe);
                    //меняем комнату по ордеру
                    OrderMessenger.changeRoom(OrderRoomEvent, parsedData.order_id);
                },
                error: function (error) {
                    toastr.error(error.responseText);
                    OrderMessenger.container.html(OrderMessenger.errorMessage);
                }
            });
        },
        getIframe: function (data) {
            return $('<iframe src="' + data.message + '" data-order_id ="' + data.order_id + '" id="order-online"></iframe>');
        },
        setIframe: function (iframe) {
            OrderMessenger.container.html('');
            iframe.appendTo(OrderMessenger.container);
            iframe.height(925);

            OrderMessenger.container.find('h2').remove();
        },
        afterSaveOrder: function () {
            OrderMessenger.changeRoom(UserRoomEvent, OrderMessenger.user_id);
            window.scrollTo(0, 0);

            OrderMessenger.waitOrder();
//            $(OrderMessenger.iframe).fadeOut('slow');
            if ($('iframe').is(OrderMessenger.iframe)) {
                $('#order-online').data('hidden', true);
            }

            OrderMessenger.listenWS();

        },
        changeRoom: function (event, newroom) {
            OrderMessenger.socket.emit(event, newroom.toString());

            debug('order messenger change joinned room: ' + newroom + ' event: ' + event);
        }
    };

    OrderMessenger.init();

    //Listen Iframe
    if (window.addEventListener) {
        window.addEventListener("message", listenIframe);
    } else {
        window.attachEvent("onmessage", listenIframe);
    }

    function listenIframe(msg) {
        listenEventAndAction(msg.data);
    }

    function listenEventAndAction(event) {
        console.log('Event order-messenger: ' + event);

        switch (event) {
            //from common/web/resources/modules/order/order-live-message/order-live-message.js
            case 'submit_form_ok' :
                OrderMessenger.afterSaveOrder();
                break;
        }
    }

    function debug(message) {
        console.log(message);
    }
});




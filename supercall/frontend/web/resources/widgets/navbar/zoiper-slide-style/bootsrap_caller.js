$(document).ready(function () {
    //кол-во сек от начала разговора для разрыва оператором
    window.DURATION = 15;
    //кол-во сек до разрыва соединения при звонке (с начала звонка)
    window.DURATION_RING = 45; //сек
    //счётчик
    window.duration_ring = 0;

    window.statusReady = false;

    var ZoiperManage = {
        WINDOW: window,
        FORM: '#order-online',
        FAP: '[name*=form-additional-params]',
        CHID: '#call_history_id',
        INPUT_GROUP_ADDON: '.input-group span.input-group-addon',
        CALL_BTN: '#call_btn',
        RESET_BTN: '#reset_btn',
        PAUSE_BTN: '#pause_btn',
        OPTIONS_BTN: '#options_btn',
        SETTINGS_BTN: '#settings_btn',
        REMOVE_NUMBER_BTN: '#remove_one',
        ZOIPER_SECURITY_BTN: '#zoiper_security_submit',
        ZOIPER_SECURITY: '#zoiper_security',
        ZOIPER_SETTINGS: '#zoiper_setting',
        ZOIPER_MODAL: '.modalZoiper',
        MIC_MUTE_BTN: '#speaker_mute_btn',
        VOLUME_MUTE_BTN: '#volume_mute_btn',
        LOG_BTN: '#log_btn',
        ZOIPER_A: '#ZoiperA',
        ZOIPER_INCOMING_MODAL: '#zoiper_incoming_modal',
        PHONE_NUMBER: '#phoneNumper',
        PHONE_NUMBER_DIAL: '#phoneNumper_dial',
        NUMS: '.caller_box .num',
        SPAN_ADDON: 'span.input-group-addon',
        TIME: '#time',
        USER_AUTO_CALL: '#user_auto_call',
        ZOIPER_INCOMMING_BTN_ACCEPT: '#zoiper_incoming_btn_accept',
        ZOIPER_INDICATOR: '#zoiper-indicator',
        ZOIPER_STATUS: '.status-zoiper',
        init: function () {
            ZoiperManage.IframeZoiper("init()");
            ZoiperManage.hideCallBtnAddons();
            $(ZoiperManage.NUMS).on('click', ZoiperManage.numsKeybordEvents);
            $(ZoiperManage.REMOVE_NUMBER_BTN).on('click', ZoiperManage.removeNumEvents);
            $(ZoiperManage.CALL_BTN).on('click.template.action', ZoiperManage.callBtnEvents);
            $(ZoiperManage.RESET_BTN).on('click.callbtnevents', ZoiperManage.resetBtnEvents);
            $(ZoiperManage.PAUSE_BTN).on('click', ZoiperManage.pauseBtnEvents);
            $(ZoiperManage.SETTINGS_BTN).on('click', ZoiperManage.settingsBtnEvents);
            $(ZoiperManage.OPTIONS_BTN).on('click', ZoiperManage.optionsBtnEvents);
            $(ZoiperManage.ZOIPER_SECURITY_BTN).on('click', ZoiperManage.zoiperSecurityBtnEvents);
            $(ZoiperManage.LOG_BTN).on('click', ZoiperManage.logBtnEvents);
            $(ZoiperManage.VOLUME_MUTE_BTN).on('click', ZoiperManage.volumeBtnMuteEvents);
            $(ZoiperManage.MIC_MUTE_BTN).on('click', ZoiperManage.micMuteBtnEvents);
            $(ZoiperManage.RESET_BTN).on('click', ZoiperManage.resetBtnEvents);
        },
        IframeZoiper: function (command) {
            ZoiperManage.WINDOW.frames.iframeZoiper.postMessage(command, "*");
        },
        CallDurationStart: function () {
            ZoiperManage.WINDOW.durationRingInterval = setInterval(function () {
                ZoiperManage.WINDOW.duration_ring++;
                //
                if (ZoiperManage.WINDOW.duration_ring >= ZoiperManage.WINDOW.DURATION) {
                    $(ZoiperManage.RESET_BTN).prop('disabled', false);
                }
            }, 1000);
        },
        hideCallBtnAddons: function () {
            $(ZoiperManage.FORM).contents().find(ZoiperManage.INPUT_GROUP_ADDON).css('display', 'none!important');
        },
        numsKeybordEvents: function () {
            $(ZoiperManage.PHONE_NUMBER).val($(ZoiperManage.PHONE_NUMBER).val() + $(this).text());
            $(ZoiperManage.PHONE_NUMBER_DIAL).val($(ZoiperManage.PHONE_NUMBER_DIAL).val() + $(this).text());
        },
        removeNumEvents: function () {
            $(ZoiperManage.PHONE_NUMBER).val($(ZoiperManage.PHONE_NUMBER).val().substring(0, $(ZoiperManage.PHONE_NUMBER).val().length - 1));
            $(ZoiperManage.PHONE_NUMBER_DIAL).val($(ZoiperManage.PHONE_NUMBER_DIAL).val().substring(0, $(ZoiperManage.PHONE_NUMBER_DIAL).val().length - 1));
        },
        callBtnEvents: function () {
            if (ZoiperManage.WINDOW.statusReady) {
                if ($(ZoiperManage.PAUSE_BTN).hasClass('active')) {
                    return false;
                } else {
                    $(ZoiperManage.PAUSE_BTN).addClass('active');

                    //Старый код
                    // ZoiperManage.IframeZoiper("Dial('" + $(ZoiperManage.PHONE_NUMBER_DIAL).val() + "')");
                    // $(ZoiperManage.PHONE_NUMBER).prop("disabled", true);
                    // $(ZoiperManage.PHONE_NUMBER).val('RING');
                    // $(ZoiperManage.PAUSE_BTN).addClass('active');

                    //Новый код
                    var number = $(ZoiperManage.PHONE_NUMBER_DIAL).val();
                    // if (!isEmpty(number)) {
                    //     if (!isUndefined($(ZoiperManage.FAP))) {
                    //         var form_additional_params = isUndefined($(ZoiperManage.FORM).prop('id'))
                    //             ? JSON.parse($(ZoiperManage.FAP).val())
                    //             : JSON.parse($(ZoiperManage.FORM).contents().find((ZoiperManage.FAP)).val());
                    //         //по умолчанию в форме стоит "{"sip": "[name*=user_sip]"}" - разделитель будет всегда !
                    //         //при добавлении новых направлений - на астере - этот разделитель должен учитываться!
                    //         if (Object.keys(form_additional_params).length > 0) {
                    //             number = $(ZoiperManage.PHONE_NUMBER_DIAL).val() + '|' + ZoiperManage.getCallHistoryId();
                    //         }
                    //     }
                    // }

                    if(number == ''){
                        number = $(ZoiperManage.PHONE_NUMBER).val();
                    }

                    ZoiperManage.IframeZoiper("Dial('" + number + "')");



                    $(ZoiperManage.PHONE_NUMBER).prop("disabled", true);
                    $(ZoiperManage.PHONE_NUMBER).val('RING');
                    //Конец нового кода
                }
            }
        },
        getCallHistoryId: function () {
            return isUndefined($(ZoiperManage.FORM).prop('id'))
                ? $(ZoiperManage.CHID).val()
                : $(ZoiperManage.FORM).contents().find(ZoiperManage.CHID).val()
        },
        resetBtnEvents: function () {
            ZoiperManage.IframeZoiper("Hang()");

            //console.log('kill window.intervalID');
            clearInterval(ZoiperManage.WINDOW.intervalID);
            $(ZoiperManage.CALL_BTN).addClass('btn btn-success');
            $(ZoiperManage.CALL_BTN).removeClass('active');

            $(ZoiperManage.PAUSE_BTN).removeClass('active');

            $(ZoiperManage.PHONE_NUMBER).prop("disabled", false);
            $(ZoiperManage.PHONE_NUMBER).val('READY');
            $(ZoiperManage.PHONE_NUMBER_DIAL).val('');

            clearInterval(ZoiperManage.WINDOW.durationRingInterval);
        },
        pauseBtnEvents: function () {
            if ($(ZoiperManage.PAUSE_BTN).hasClass('active')) {
                $(ZoiperManage.PAUSE_BTN).removeClass('active');

                ZoiperManage.IframeZoiper("UnHold()");

                $(ZoiperManage.PHONE_NUMBER).val($(ZoiperManage.PHONE_NUMBER).val().replace('HOLD', 'RING'));
            } else {
                //Чтобы исключить возможность вызова пустого номера
                if ($(ZoiperManage.PHONE_NUMBER_DIAL).val() == '') {
                    return false;
                }
                $(ZoiperManage.PAUSE_BTN).addClass('active');

                ZoiperManage.IframeZoiper("Hold()");

                $(ZoiperManage.PHONE_NUMBER).val($(ZoiperManage.PHONE_NUMBER).val().replace('RING', 'HOLD'));
            }
        },
        settingsBtnEvents: function () {
            ZoiperManage.IframeZoiper("ShowAudioWizard()");
        },
        optionsBtnEvents: function () {
            $(ZoiperManage.ZOIPER_SETTINGS).modal('show');
        },
        zoiperSecurityBtnEvents: function () {
            var h = $(ZoiperManage.ZOIPER_SECURITY).val();

            if (h.hashCode() == 751834184) {
                $(ZoiperManage.ZOIPER_SETTINGS).modal('hide');
                //$(ZoiperManage.ZOIPER_SETTINGS).modal('hide');
                $(ZoiperManage.ZOIPER_MODAL).css('left', '10px');
            }
        },
        logBtnEvents: function () {
            ZoiperManage.IframeZoiper("ShowLog()");
        },
        volumeBtnMuteEvents: function () {
            ZoiperManage.IframeZoiper("MuteSpeakerToggle()");

            $(ZoiperManage.VOLUME_MUTE_BTN).hasClass('active') ? $(ZoiperManage.VOLUME_MUTE_BTN).removeClass('active') : $(ZoiperManage.VOLUME_MUTE_BTN).addClass('active');
        },
        micMuteBtnEvents: function () {
            ZoiperManage.IframeZoiper("MuteMicToggle()");

            $(ZoiperManage.MIC_MUTE_BTN).hasClass('active') ? $(ZoiperManage.MIC_MUTE_BTN).removeClass('active') : $(ZoiperManage.MIC_MUTE_BTN).addClass('active');
        }
    };

    ZoiperManage.init();

    //$(ZoiperManage.SPAN_ADDON).on('click', function (e) {
    //common/web/resourse/modules/call/call-history/call-history.js
    //});

    //console.log('window.AutoCall', window.AutoCall);
    //$(ZoiperManage.ZOIPER_INCOMMING_BTN_ACCEPT).on('click', function () {
    $(window.AutoCall.button_accept).on('click', function () {
        console.log('click incomming btn');
        //$(this).addClass('active');
        ZoiperManage.IframeZoiper("Accept()");
        //$(ZoiperManage.ZOIPER_INCOMING_MODAL).modal('hide');
        window.AutoCall.hide();
    });

    $(ZoiperManage.ZOIPER_A).trigger('click');

    function fmtMSS(s) {
        parent.duration = s;

        return (s - (s %= 60)) / 60 + (9 < s ? ':' : ':0') + s;
    }

    //listen iframe
    if (window.addEventListener) {
        window.addEventListener("message", listenIframe);
    } else {
        window.attachEvent("onmessage", listenIframe);
    }

    //action for listing iframe
    function listenIframe(msg) {
        listenEventAndAction(msg.data);
    }

    function listenEventAndAction(event) {
        console.log('Event: ' + event);

        listenEventForKVS(event);

        switch (event) {
            case 'init' :
                console.log('Zoiper initialize');

                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_ZOIPER, 'register');
                }

                break;
            case 'register' :
                window.statusReady = true;
                $(ZoiperManage.CALL_BTN).prop('disabled', false);
                $(ZoiperManage.CALL_BTN).removeClass('btn btn-warning');
                $(ZoiperManage.CALL_BTN).addClass('btn btn-success');

                $(ZoiperManage.ZOIPER_INDICATOR).removeClass('dot-red').addClass('dot-green');
                $(ZoiperManage.ZOIPER_STATUS).text('Zoiper registered');

                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_ZOIPER, 'registered');
                }

                break;
            case 'unregister' :
                window.statusReady = false;

                console.groupEnd();

                $(ZoiperManage.ZOIPER_INDICATOR).removeClass('dot-green').addClass('dot-red');
                $(ZoiperManage.ZOIPER_STATUS).text('Zoiper unregistered');

                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_ZOIPER, event);
                }

                break;
            case 'failed_register' :
                console.groupEnd();

                $(ZoiperManage.ZOIPER_INDICATOR).removeClass('dot-green').addClass('dot-red');
                $(ZoiperManage.ZOIPER_STATUS).text('Zoiper failed register');

                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_ZOIPER, event);
                }

                break;
            case 'ring' :
                //console.time();
                //console.log('set parent.window.ActiveCall to TRUE');
                parent.window.ActiveCall = true;
                ZoiperManage.CallDurationStart();

                CallDurationStart();
                //console.log('typeof KVS', typeof KVS)
                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_CALL, event);
                }

                break;
            case 'hang' :
                //console.log('set parent.window.ActiveCall to FALSE');
                parent.window.ActiveCall = false;

                clearInterval(window.intervalID);

                //Закрваем окно входящего звонка - если этот звонок взял другой оператор
                if ($(ZoiperManage.USER_AUTO_CALL).text() == 1) {
                    //$(ZoiperManage.ZOIPER_INCOMING_MODAL).modal('hide');
                    window.AutoCall.hide();
                }

                //TODO непонятный баг. У некоторый пользователй не убирается подложка после закрытия модального окна
                //TODO баг понятен - все из за анимации. Подложка не успевает доанимароваться fadeIn fadeOut - вызвается модальное окно еще раз
                //$('.modal-backdrop.fade.in').remove();

                $(ZoiperManage.PHONE_NUMBER).css('background', 'white');
                if ($(ZoiperManage.RESET_BTN).prop('type') != 'undefined') {
                    $(ZoiperManage.RESET_BTN).prop('disabled', false);
                    $(ZoiperManage.CALL_BTN).removeClass('active');
                    $(ZoiperManage.RESET_BTN).trigger('click');
                    console.log('CLICK RESET_BTN 1');
                } else {
                    $(ZoiperManage.RESET_BTN, parent.document).prop('disabled', false);
                    $(ZoiperManage.CALL_BTN, parent.document).removeClass('active');
                    $(ZoiperManage.RESET_BTN, parent.document).trigger('click');
                    console.log('CLICK RESET_BTN 2');
                }

                if (typeof (SearchOrderForm) != 'undefined') {
                    //TODO использовать только для формы поиска заказа
                    //SearchOrderForm.callWasEnd();
                }

                console.timeEnd();
                console.groupEnd();

                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_CALL, event);
                }

                break;


            case 'hold' :
                console.log('Event: ' + event);

                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_CALL, event);
                }

                break;
            case 'unhold' :
                console.info('Event: ' + event);

                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_CALL, event);
                }

                break;
            case 'accept' :
                console.log('set parent.window.ActiveCall to TRUE');
                parent.window.ActiveCall = true;
                //action;
                /*if($(ZoiperManage.USER_AUTO_CALL).text() == 1){
                    getOrderOnline();
                }*/

                $(ZoiperManage.PHONE_NUMBER).css('background', '#F2FFD8');

                $(ZoiperManage.RESET_BTN).prop('disabled', false);

                var i = 0;
                window.intervalID = setInterval(function () {
                    $(ZoiperManage.TIME).val(fmtMSS(i++));
                }, 1000);
                /*
                 var i = 0;
                 setTimeout(function (el) {
                 $(ZoiperManage.RESET_BTN).removeAttr('disabled');
                 },30000);*/
                /*window.intervalID = setInterval(function () {
                 //https://2wtrade-tasks.atlassian.net/browse/WCALL-9
                 //15 после активного разговора - кнопка сброс не доступна
                 //плагин около номера контролируется кодом в common/web/resourse/modules/call/call-history/call-history.js
                 if (i >= 45) {
                 $(ZoiperManage.RESET_BTN).prop('disabled', false);
                 }

                 $(ZoiperManage.TIME).val(fmtMSS(i++));
                 }, 1000)*/

                console.info('Event: ' + event);

                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_CALL, event);
                }

                break;
            case 'reject' :
                clearInterval(window.intervalID);

                //console.log('set parent.window.ActiveCall to FALSE');
                parent.window.ActiveCall = false;

                $(ZoiperManage.PHONE_NUMBER).css('background', 'white');
                $(ZoiperManage.RESET_BTN).trigger('click');
                console.timeEnd();
                console.groupEnd();

                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_CALL, event);
                }

                break;
            case 'incoming' :
                clearInterval(window.intervalID);
                if (!$("iframe").is(ZoiperManage.FORM)) {
                    //$(ZoiperManage.ZOIPER_INCOMING_MODAL).modal('toggle');
                    window.AutoCall.show();
                }

                if (typeof KVS !== 'undefined') {
                    KVS.set(KVS.ACTION_STATUS_CALL, event);
                }

                break;
        }

    }

    //Принятия входящего звонка нажатием энтера или пробела
    document.addEventListener("keydown", function (event) {
        if (event.keyCode == 13) {
            $(ZoiperManage.ZOIPER_INCOMMING_BTN_ACCEPT).trigger('click');
        }
    });

    $(ZoiperManage.ZOIPER_INCOMING_MODAL).on('hidden.bs.modal', function () {
        $(".modal-backdrop.in").hide();
    });
    $(ZoiperManage.ZOIPER_INCOMING_MODAL).on('shown.bs.modal', function () {
        $(".modal-backdrop.in").hide();
    });

});

function CallDurationStart() {
    window.durationRingInterval = setInterval(function () {
        window.duration_ring++;
        //
        if (window.duration_ring >= window.DURATION) {
            $("#reset_btn").prop('disabled', false);
        }
    }, 1000);
}

function showModal(id) {
    $('#' + id).modal('toggle');
}

String.prototype.hashCode = function () {
    var h = 0;
    if (this.length == 0) return h;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        h = ((h << 5) - h) + char;
        h = h & h;
    }
    return h;
};

function listenEventForKVS(event) {

    if (typeof KVS !== 'undefined') {
        var val = '';
        var patternOrderStatusOpen = /status_open/;
        var patternOrderSave = /form_order_on_save/;
        var patternOrderStatusClose = /close_modal_status/;

        if (patternOrderStatusOpen.test(event) && event !== 'status_open') {
            val = event.replace('status_open-', '');

            console.log('EVENT from KVS: ', event);

            if (val !== '') {

                KVS.set(KVS.ACTION_ORDER, 'Open status: ' + val);
            }
        } else if (patternOrderSave.test(event)) {
            val = event.replace('form_order_on_save-', '');

            console.log('val', val);

            if (val !== '') {
                KVS.set(KVS.ACTION_ORDER, 'Save status: ' + val);
            }
        } else if (patternOrderStatusClose.test(event)) {
            val = event.replace('close_modal_status:', '');

            if (val !== '') {
                KVS.set(KVS.ACTION_ORDER, 'Close status: ' + val);
            }
        }
    }

    //console.log(KVS.getData(KVS.ACTION_ORDER));
}



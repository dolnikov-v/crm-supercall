var currentTime;
// init online-time function
var OrderOnline = new (function() {
  var $stopwatch, // Stopwatch element on the page
    incrementTime = 70, // Timer speed in milliseconds
    // currentTime = 0, // Current time in hundredths of a second
    updateTimer = function() {
      $stopwatch.text(formatTime(currentTime));
      currentTime += incrementTime / 10;
      $('#currentTime').text(currentTime);
    },
    init = function() {
      $stopwatch = $('#time-online-ready');
      OrderOnline.Timer = $.timer(updateTimer, incrementTime, true);
    };
  $(init);
});


$(document).ready(function () {
    getTimeReady();

    $('#ready-button').on('click', function() {
        if ($(this).prop('checked')) {
            changeReady('is_ready', 1);
            $('#check-unready-button').prop('checked', false)
            //getTimeReady();
            OrderOnline.Timer.play()
        }

        else {
          changeReady('is_ready', 0);
          useDisabled();
        }

    });

    $('#check-unready-button').on('click', function() {
        if ($(this).prop('checked'))
            changeReady('check_unready', 1)
        else
            changeReady('check_unready', 0)
    });

    window.onbeforeunload = function (e) {
        var e = e || window.event;

        // For IE and Firefox
        if (e) {
            localStorage.setItem('currentTime', $('#currentTime').text());
        }else{
            // For Safari
            localStorage.setItem('currentTime', $('#currentTime').text());
        }
    };
});

/**
 * change status "Ready/Unready" for operator
 * @param paramName
 * @param value
 */
function changeReady(paramName, value) {
    $.post("/profile/control/change-ready", {'param': paramName, 'value': value})
    .success(function(response){

        console.log('response', response);

        // location.reload();

        if (response.result === 'ok') {
            // change text in the button
            changeTextBtn(paramName, value)
        }
    })
    .error(function(response){

    });
}


function changeTextBtn(paramName, value) {
    if (paramName == 'is_ready') {
        if (value == 1) {
          $('#ready-button-box label.label-ready-button').text('Ready');
          // start serach free order for operator
            if(typeof getOrderOnline == 'function') {
                getOrderOnline();
            }
        }
        else if (value == 0) {
          $('#ready-button-box label.label-ready-button').text('Unready');
          // stop serach orders for operator
          clearInterval(window.timerId)
        }
    }
}

/**
 * get time from operator online ready
 */
function getTimeReady() {
  if ( $('#ready-button').prop('checked') ) {
      $.post("/profile/control/get-time", {})
        .success(function(response){
          if (response.result === 'ok') {
            $('#box--time-online-ready').removeClass('text-muted');

            var $timeOnlineReady = $('#time-online-ready');
            $timeOnlineReady.text(response.data)

            //if(parseInt(localStorage.getItem('currentTime')) > parseInt(response.milliseconds)){
            //    currentTime = localStorage.getItem('currentTime').substring(1,6);
            //}else{
                currentTime = response.milliseconds
            //}
          }
        })
        .error(function(response){

        });
  } else {
      OrderOnline.Timer.stop()
  }

}

/**
 * reset block in the time-operator-ready
 * @param value
 */
function useDisabled() {
    $('#box--time-online-ready').addClass('text-muted');
    OrderOnline.Timer.stop()
}

// Common functions
function pad(number, length) {
  var str = '' + number;
  while (str.length < length) {str = '0' + str;}
  return str;
}

function formatTime(time) {
  var hour = parseInt(time / 360000),
    min = parseInt(time / 6000) - (hour * 60),
    sec = parseInt(time / 100) - (min * 60) - (hour * 3600);

  return pad(hour, 2) + ":" + pad(min, 2) + ":" + pad(sec, 2);
}


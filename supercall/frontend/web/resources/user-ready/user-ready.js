$(document).ready(function(){
    setInterval(function(){
        if($("#ready-button") == null || $("#ready-button").prop("checked") == true || typeof $("#ready-button").prop('type') == 'undefined'){
            $.ajax({
                method: 'post',
                url : $("#url_backend").text() + '/call/call-history/user-ready',
                data: {
                    user_id : $("#ratchet_user").text() != '' ? $("#ratchet_user").text() : $("#user_id").text()
                }
            });
        }
    }, 60000);
});


function stopTime() {
    if($("#ready-button") == null || $("#ready-button").prop("checked") == true || typeof $("#ready-button").prop('type') == 'undefined'){
        $.ajax({
            method: 'post',
            url : $("#url_backend").text() + '/call/call-history/user-ready',
            data: {
                user_id : $("#ratchet_user").text() != '' ? $("#ratchet_user").text() : $("#user_id").text()
            }
        });
    }
}

window.addEventListener("unload", function (event) {
    stopTime();
});
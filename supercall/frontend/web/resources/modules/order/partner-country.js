$(document).ready(function () {
    $partner_select = $("#order-partner_id");
    $country_select = $("#order-country_id");
    $submit = $(".btn-success");

    $submit.prop('disabled', true);

    addSelectedEmptiOption();

    $partner_select.change(function () {
        $country_select.prop('disabled', true);

        if ($(this).val() == '-' || $country_select.val() == '-') {
            $country_select.prop('disabled', true);
            accessSubmit();
        } else {
            $.ajax({
                url: '/order/change/get-partner-countries',
                type: "POST",
                async: true,
                data: {'partner_id': $partner_select.val(), 'json': true},
                beforeSend: function () {
                },
                complete: function () {
                    $country_select.prop('disabled', false);
                    accessSubmit();
                },
                success: function (response) {
                    addSelectedEmptiOption();
                    accessSubmit();
                    for (id in response) {
                        $country_select.append(new Option(response[id], id));
                    }
                },
                dataType: 'json'
            });
        }
    });

    $country_select.change(function () {
        accessSubmit();
    });

    function addSelectedEmptiOption() {
        $country_select.empty();

        $country_select.append(new Option('-', '')).val('');
        $country_select.select2();
    }

    function accessSubmit() {
        $submit.prop('disabled', ($partner_select.val() == '-' || $country_select.val() == ''));
    }
});
$(document).ready(function () {

});

function getOrderByOperator(operatorId) {
    $.ajax({
        type: 'POST',
        url: '/order/online/form',
        dataType: 'json',
        data: {'operatorId': operatorId},
        success: function (response, textStatus) {
            $.notify({message: success}, {type: "success dark", animate: {exit: 'hide'}, z_index: 2031});
        },
        error: function () {
            $.notify({message: fail}, {type: "danger dark", animate: {exit: 'hide'}, z_index: 2031});
        }
    });
}
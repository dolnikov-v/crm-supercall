<?php
namespace frontend\controllers;

use frontend\components\web\Controller;
use Yii;

/**
 * Class ErrorsController
 * @package frontend\controllers
 */
class ErrorsController extends Controller
{
    public $layout = '@frontend/views/layouts/error';

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
}

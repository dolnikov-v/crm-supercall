<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'landing/index',
    'name' => '2WCall',
    'components' => [
        'request' => [
            'class' => 'common\components\web\Request',
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-frontend',
                'httpOnly' => true,

            ],
        ],
        'session' => [
            'class' => 'yii\web\CacheSession',
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'errors/index',
        ],
        'urlManager' => [
            'class' => 'common\components\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<module:[\w-]+>/<folder:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<folder>/<controller>/<action>',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',

                '<module:[\w-]+>/<folder:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<folder>/<controller>/<action>',
                '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
            ],
        ],
        'assetManager' => [
            'linkAssets' => YII_ENV_DEV ? true : false,
            'bundles' => [
                'yii\web\YiiAsset' => [
                    'depends' => [
                        'common\assets\vendor\JqueryAsset',
                    ],
                ],
                'yii\web\JqueryAsset' => [
                    'css' => [],
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'css' => [],
                    'js' => [],
                ],
            ],
        ],
        'view' => [
            'class' => 'common\components\web\View'
        ]
    ],
    'modules' => [
        'landing' => [
            'class' => 'frontend\modules\landing\Module',
        ],
        'auth' => [
            'class' => 'frontend\modules\auth\Module',
        ],
        'order' => [
            'class' => 'frontend\modules\order\Module',
        ],
        'docs' => [
            'class' => 'frontend\modules\docs\Module'
        ],
        'profile' => [
            'class' => 'frontend\modules\profile\Module',
        ],
        'i18n' => [
            'class' => 'backend\modules\i18n\Module',
        ],
        'wiki' => [
            'class' => 'frontend\modules\wiki\Module'
        ],
        'call' => [
            'class' => 'common\modules\call\Module'
        ],
    ],
    'params' => $params,
];

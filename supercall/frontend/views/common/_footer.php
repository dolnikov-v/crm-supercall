<footer class="footer">
    © <?= date('Y').' '.strtoupper(Yii::$app->params['companyName']);?>
</footer>
<div style="display: none;" id="AsteriskSp_prefix"><?=Yii::$app->params['AsteriskSp_prefix'];?></div>
<div style="display: none" id="document_domain"><?=yii::$app->params['document.domain'];?></div>
<div style="display: none" id="url_backend"><?=yii::$app->params['urlBackend'];?></div>
<div style="display: none" id="user_id"><?=yii::$app->user->identity->id;?></div>
<div style="display: none" id="ratchet_port"><?=yii::$app->get('ratchet')->server_port;?></div>
<div style="display: none" id="ratchet_user"><?=yii::$app->user->identity->id;?></div>

<!-- asterisk settings start-->
<div style="display: none" id="asterisk_server"><?=yii::$app->params['asterisk_server'];?></div>
<div style="display: none" id="asterisk_websocket"><?=yii::$app->params['asterisk_websocket'];?></div>
<div style="display: none" id="asterisk_ice_server"><?=yii::$app->params['asterisk_ice_server'];?></div>
<div style="display: none" id="asterisk_identity_tpl"><?=yii::$app->params['asterisk_identity_tpl'];?></div>
<div style="display: none" id="asterisk_sipml5_disabled_logs"><?=yii::$app->params['asterisk_sipml5_disabled_logs'];?></div>
<div style="display: none" id="asterisk_user_sip_data"><?=yii::$app->user->identity->sip;?></div>
<div style="display: none" id="user_auto_call"><?=yii::$app->user->identity->auto_call;?></div>
<!-- asterisk settings  end-->

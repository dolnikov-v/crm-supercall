<?php

use frontend\widgets\aside\Profile;
use frontend\widgets\aside\WikiCategory;
use frontend\modules\wiki\Module;

$css = <<<CSS
.buttons_block a{
    padding:3px!important;
}
CSS;

$this->registerCss($css);
?>

<aside id="menu">
    <div id="navigation">
        <?= Profile::widget() ?>

    </div>

    <?php if(Module::MODULE_WIKI == Yii::$app->controller->module->id):?>

    <?= WikiCategory::widget() ?>

    <?php endif;?>

</aside>





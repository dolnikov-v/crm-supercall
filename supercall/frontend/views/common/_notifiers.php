<?php
use common\components\Notifier;

$notifiers = Yii::$app->notifier->getNotifiers();
?>

<?php if (!empty($notifiers) && is_array($notifiers)) { ?>
    <?php foreach ($notifiers as $notifier) { ?>
        <script type="application/javascript">
            <?php if ($notifier['type'] == Notifier::TYPE_SUCCESS): ?>
            toastr.success('<?= $notifier['message'] ?>');
            <?php elseif ($notifier['type'] == Notifier::TYPE_ERROR): ?>
            toastr.error('<?= $notifier['message'] ?>');
            <?php elseif ($notifier['type'] == Notifier::TYPE_WARNING): ?>
            toastr.warning('<?= $notifier['message'] ?>');
            <?php elseif ($notifier['type'] == Notifier::TYPE_INFO): ?>
            toastr.info('<?= $notifier['message'] ?>');
            <?php endif; ?>
        </script>
    <?php } ?>
<?php } ?>

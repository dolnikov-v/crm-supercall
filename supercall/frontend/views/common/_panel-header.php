<?php
use common\widgets\base\Breadcrumbs;
use common\widgets\base\PanelHeader;

/** @var \yii\web\View $this */
?>

<?php if (!isset($this->params['showPanelHeader']) || $this->params['showPanelHeader'] == true): ?>
    <?= PanelHeader::widget([
        'title' => $this->title,
        'breadcrumbs' => Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ])
    ]) ?>
<?php endif; ?>

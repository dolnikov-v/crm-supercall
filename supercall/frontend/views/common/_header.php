<?php
use backend\widgets\navbar\Clock;
use backend\widgets\navbar\TimezoneSwitcher;
use backend\widgets\navbar\LanguageSwitcher;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div id="header">
    <div class="color-line"></div>
    <div id="logo" class="light-version">
        <span><?= Html::a(Yii::$app->params['companyName'], Url::to('/')) ?></span>
    </div>

    <nav role="navigation">
        <div class="header-link hide-menu">
            <i class="fa fa-bars"></i>
        </div>
    </nav>

    <div class="text-center align-middle navbar-left slogan"><?= Yii::$app->params['slogan'] ?></div>

    <div class="navbar-right">
        <ul class="nav navbar-nav no-borders">
            <?= TimezoneSwitcher::widget() ?>
            <?= LanguageSwitcher::widget() ?>
            <?= Clock::widget() ?>
        </ul>
    </div>
</div>

<?php
use common\modules\order\assets\ModalOrderFormAsset;
use common\modules\order\widgets\ModalEndWorkWithSearchForm;
use common\modules\order\widgets\ModalOrderForm;
use frontend\assets\base\ThemeAsset;
use frontend\modules\wiki\Module;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\bootstrap\Modal;
use frontend\assets\RatchetAsset;

use frontend\assets\base\TimerActivityAsset;
use frontend\assets\UserReadyAsset;

use backend\assets\base\ThemeSloganAsset;

/** @var \yii\web\View $this */
/** @var string $content */

/* base */
ThemeAsset::register($this);

ThemeSloganAsset::register($this);
//отключили
//RatchetAsset::register($this);
UserReadyAsset::register($this);
TimerActivityAsset::register($this);
ModalOrderFormAsset::register($this);


?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: <?= Yii::$app->name ?></title>
    <link rel="icon" href="<?= Yii::$app->params['urlBackend'] ?>/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?= Yii::$app->params['urlBackend'] ?>/favicon.ico" type="image/x-icon">
    <?php $this->head(); ?>
</head>
<body class="fixed-navbar fixed-sidebar <?= ArrayHelper::getValue($this->params, 'body-class'); ?>">
<?php $this->beginBody(); ?>

<?php
$this->registerJs('$("div.header-link").on("click", function(){if($("body").hasClass("hide-sidebar")){$("body").removeClass("hide-sidebar")}else{$("body").addClass("hide-sidebar")}});');
?>

<?php if (yii::$app->controller->module->id != Module::MODULE_WIKI): ?>

    <?php if (yii::$app->user->identity && !yii::$app->user->identity->webrtc): ?>

        <?= \frontend\widgets\aside\ZoiperSlide::widget() ?>

    <?php else: ?>

        <?= \frontend\widgets\aside\SipML5Slide::widget() ?>

    <?php endif; ?>

<?php endif; ?>

<?= $this->render('@frontend/views/common/_splash') ?>
<?= $this->render('@frontend/views/common/_header') ?>
<?= $this->render('@frontend/views/common/_aside') ?>

<div id="wrapper">
    <?= $this->render('@frontend/views/common/_panel-header') ?>
    <div class="content animate-panel">
        <?= $content ?>
    </div>
    <?= $this->render('@frontend/views/common/_footer') ?>
</div>

<?php $this->endBody(); ?>
<?= $this->render('@frontend/views/common/_notifiers') ?>
</body>
</html>
<?php $this->endPage(); ?>

<?php Modal::begin([
    'id' => 'ratchet_modal',
    'size' => Modal::SIZE_LARGE,
    'closeButton' => false,
    'options' => [
        'style' => 'z-index: 90000',
        'data-backdrop' => 'static',
        'data-keyboard' => false
    ],
    'header' => yii::t('common', 'Внимание'),
]); ?>

<div class="row">
    <div class="col-lg-12 align-middle"><h2 class="text-danger"><span
                    class="glyphicon glyphicon-exclamation-sign"></span> <?= yii::t('common', 'Произошёл системный сбой.'); ?>
        </h2>
        <hr/>
        <div class="row">
            <div class="col-lg-12"><?= yii::t('common', 'Обновите страницу. Если ошибка повторилась, свяжитесь с технической поддержкой.'); ?></div>
        </div>
    </div>
</div>

<?php Modal::end(); ?>


<?php Modal::begin([
    'id' => 'zoiper_setting',
    'size' => Modal::SIZE_SMALL,
    'closeButton' => ['id' => 'zoiper_modal_close_button'],
    'options' => [
        'style' => 'z-index: 90000',
        'data-backdrop' => 'static',
        'data-keyboard' => true
    ],
    'footer' => Html::button(yii::t('common', 'Подтвердить'), [
        'class' => 'btn btn-default btn-success',
        'id' => 'zoiper_security_submit'
    ]),
    'header' => yii::t('common', 'Введите пароль'),
]); ?>
<div class="row">
    <input type="password" id="zoiper_security" value="">
</div>

<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'zoiper_incoming_modal',
    'size' => Modal::SIZE_SMALL,
    'closeButton' => ['id' => 'zoiper_incoming_modal_close_button'],
    'options' => [
        'style' => 'z-index: 90000',
        'data-backdrop' => 'static',
        'data-keyboard' => false
    ],
    //'footer' => Html::button(yii::t('common', 'Принять'), ['class' => 'btn btn-default btn-success', 'id' => 'zoiper_incoming_btn_accept']),
    'header' => yii::t('common', 'Входящий звонок'),
]); ?>
<button type="button" id="zoiper_incoming_btn_accept"
        class="btn btn-success"><?= yii::t('common', 'Принять'); ?></button>
<?php Modal::end(); ?>

<?= ModalOrderForm::widget() ?>
<?= ModalEndWorkWithSearchForm::widget() ?>


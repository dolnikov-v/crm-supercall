<?php
use frontend\assets\base\ThemeAsset;
use common\assets\vendor\AnimateAsset;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var string $content */

ThemeAsset::register($this);
AnimateAsset::register($this);
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: <?= Yii::$app->name ?></title>
    <link rel="icon" href="<?= Yii::$app->params['urlFrontend'] ?>/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?= Yii::$app->params['urlFrontend'] ?>/favicon.ico" type="image/x-icon">
    <?php $this->head(); ?>
</head>
<body class="landing-page">
<?php $this->beginBody(); ?>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                    class="navbar-toggle collapsed" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/" class="navbar-brand"><img src="/resources/images/common/logo.png"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active">
                    <?= Html::a(Yii::t('common', 'Main page'), '/', ['class' => 'page-scroll']) ?>
                </li>
                <li>
                    <?= Html::a(Yii::t('common', 'Partner programm'), '/', ['class' => 'page-scroll']) ?>
                </li>
                <li>
                    <?= Html::a(Yii::t('common', 'Login'), ['/auth/login'], ['class' => 'page-scroll']) ?>
                </li>
                <li>
                    <?= Html::a(Yii::t('common', 'Registration'), ['/auth/register'], ['class' => 'page-scroll']) ?>
                </li>
            </ul>
        </div>
    </div>
</nav>

<?= $content ?>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>

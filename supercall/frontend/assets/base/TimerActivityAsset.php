<?php
namespace frontend\assets\base;

use yii\web\AssetBundle;

class TimerActivityAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/base/timer-activity';

    public $js = [
        'timer-activity.js',
    ];

    public $css = [

    ];

    public $depends = [

    ];
}
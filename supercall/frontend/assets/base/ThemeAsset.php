<?php
namespace frontend\assets\base;

use yii\web\AssetBundle;

/**
 * Class ThemeAsset
 * @package frontend\assets\base
 */
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/base/theme';

    public $js = [
        'theme.js',
    ];

    public $css = [
        'theme.css',
        'theme.custom.css',
    ];

    public $depends = [
        'common\assets\vendor\ToastrAsset',
        'common\assets\vendor\BootstrapAsset',
        'common\assets\fonts\OpenSansAsset',
        'common\assets\fonts\PeIconAsset',
    ];
}

<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class RatchetAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/ratchet/';

    public $js = [
        'ratchet.js',
    ];

    public $css = [

    ];
}
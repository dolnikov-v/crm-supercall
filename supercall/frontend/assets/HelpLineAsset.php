<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class HelpLineAsset
 * @package frontend\assets
 */
class HelpLineAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/profile/';

    public $js = [
        'help-line.js',
    ];
}
<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class UserReadyAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/user-ready/';

    public $js = [
        'user-ready.js',
    ];

    public $css = [

    ];
}
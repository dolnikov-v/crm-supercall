<?php
namespace frontend\assets\widgets\navbar;

use yii\web\AssetBundle;

class SipML5SlideAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/sipml5-slide-style';

    public $css = [
        'sipml5.css',
    ];

    public $js = [
        'sipml5-api-beatufy.js',
        'sipml5-core.js',
        'sipml5-2wcall.js'
    ];
}
<?php

namespace frontend\assets\widgets\navbar;

use yii\web\AssetBundle;

/**
 * Class MonitorAsset
 * @package frontend\assets\widgets\navbar
 */
class MonitorAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/monitor';

    public $js = [
        'monitor.js',
    ];

    public $css = [
        'monitor.css',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
        'common\assets\vendor\BootstrapAsset'
    ];
}
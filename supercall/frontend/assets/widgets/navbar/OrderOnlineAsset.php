<?php
namespace frontend\assets\widgets\navbar;

use Yii;
use yii\web\AssetBundle;

/**
 * Подключает js для запроса свободного заказа для онлайн обработки
 *
 * Class OrderOnlineAsset
 * @package backend\assets\widgets\navbar
 */
class OrderOnlineAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/order-online';

    public $js = [
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
    
    public function init()
    {
        parent::init();
        // только для Домашней страницы
        if (Yii::$app->controller->module->id === 'landing' && Yii::$app->controller->id === 'index') {
            $this->js[] = ['order-online.js'];
        }
    }
}

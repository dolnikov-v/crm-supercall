<?php
namespace frontend\assets\widgets\navbar;


use yii\web\AssetBundle;

/**
 * Class WikiCategoryAsset
 * @package frontend\assets\widgets\navbar
 */
class WikiCategoryAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/wiki-category';

    public $css = [
        'wiki-category.css'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
<?php
namespace frontend\assets\widgets\navbar;

use yii\web\AssetBundle;

/**
 * Class ProfileAsset
 * @package backend\assets\widgets\navbar
 */
class ProfileAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/profile';

    public $js = [
        'profile.js',
    ];

    public $css = [
        'profile.css',
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}

<?php
namespace frontend\assets\widgets\navbar;


use yii\web\AssetBundle;

/**
 * Class TimerAsset
 * @package backend\assets\widgets\navbar
 */
class TimerAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/timer';

    public $js = [
        'jquery.timer.js',
    ];
}

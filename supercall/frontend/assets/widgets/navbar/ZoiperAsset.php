<?php
namespace frontend\assets\widgets\navbar;

use yii\web\AssetBundle;

/**
 * Class ZoiperAsset
 * @package backend\assets\widgets\navbar
 */
class ZoiperAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/zoiper';

    public $js = [
        'zoiper.js',
    ];

}

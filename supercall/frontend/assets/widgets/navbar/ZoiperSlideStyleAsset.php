<?php
namespace frontend\assets\widgets\navbar;

use yii\web\AssetBundle;

/**
 * Class ZoiperSlideStyleAsset
 * @package backend\assets\widgets\navbar
 */
class ZoiperSlideStyleAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/zoiper-slide-style';
    
    public $css = [
        'zoiper.css',
    ];

    public $js = [
        'bootsrap_caller.js',
    ];
}

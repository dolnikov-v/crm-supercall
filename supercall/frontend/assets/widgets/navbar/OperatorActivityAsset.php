<?php
namespace frontend\assets\widgets\navbar;

use yii\web\AssetBundle;

/**
 * https://2wtrade-tasks.atlassian.net/browse/SC-67
 * разлогинивает оператора после 10 минутной пассивности
 * Class OperatorActivityAsset
 * @package frontend\assets\widgets\navbar
 */
class OperatorActivityAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/operator-activity';

    public $js = [
        'operator-activity.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
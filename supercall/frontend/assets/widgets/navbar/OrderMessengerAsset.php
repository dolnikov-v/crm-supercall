<?php

namespace frontend\assets\widgets\navbar;

use yii\web\AssetBundle;

/**
 * Class OrderMessengerAsset
 * @package frontend\assets\widgets\navbar
 */
class OrderMessengerAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/order-messenger';

    public $js = [
        'order-messenger.js',
    ];

    public $css = [
        'order-messenger.css'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
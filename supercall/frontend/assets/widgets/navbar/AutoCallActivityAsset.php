<?php

namespace frontend\assets\widgets\navbar;

use yii\web\AssetBundle;

/**
 * Class AutoCallActivityAsset
 * @package frontend\assets\widgets\navbar
 */
class AutoCallActivityAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/widgets/navbar/auto-call-activity';

    public $js = [
        'auto-call-activity.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset',
    ];
}
<?php
namespace frontend\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class BootstrapAsset
 * @package frontend\assets\vendor
 */
class BootstrapAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/vendor/bootstrap';

    public $js = [
        'bootstrap.min.js',
    ];

    public $css = [
        'bootstrap.min.css',
    ];

    public $depends = [
        'frontend\assets\vendor\JqueryAsset',
    ];
}

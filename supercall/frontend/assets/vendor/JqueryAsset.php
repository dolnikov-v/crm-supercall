<?php
namespace frontend\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class JqueryAsset
 * @package frontend\assets\vendor
 */
class JqueryAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/vendor/jquery';

    public $js = [
        'jquery.min.js',
    ];
}

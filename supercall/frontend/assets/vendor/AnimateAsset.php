<?php
namespace frontend\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class AnimateAsset
 * @package frontend\assets\vendor
 */
class AnimateAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/vendor/animate';

    public $js = [

    ];

    public $css = [
        'animate.min.css',
    ];
}

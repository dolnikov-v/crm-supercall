<?php
namespace frontend\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class OpenSansAsset
 * @package frontend\assets\fonts
 */
class OpenSansAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/fonts/open-sans';

    public $css = [
        'open-sans.css',
    ];
}

<?php
namespace frontend\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class PeIconAsset
 * @package frontend\assets\fonts
 */
class PeIconAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/fonts/pe-icon';

    public $css = [
        'pe-icon-7-stroke.css',
        'pe-icon-7-stroke.custom.css',
        'helper.css',
    ];
}

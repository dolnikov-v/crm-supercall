<?php
namespace frontend\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class GlyphiconsAsset
 * @package frontend\assets\fonts
 */
class GlyphiconsAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/fonts/glyphicons';

    public $css = [
        'glyphicons.css',
        'glyphicons.custom.css',
    ];
}

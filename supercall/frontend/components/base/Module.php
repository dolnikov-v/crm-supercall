<?php
namespace frontend\components\base;

use Yii;
use common\components\base\Module as CommonModule;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package frontend\components\base
 */
class Module extends CommonModule
{
    
    public function init()
    {
        parent::init();
        
        if (!Yii::$app->user->isGuest) {
            $rolesModel = Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id);
            $roles = ArrayHelper::getColumn($rolesModel, 'name');
            
            // показываем только операторам
            if (in_array('operator', $roles)) {
                $this->layout = '@frontend/views/layouts/main';
            }
        }
        
    }
}

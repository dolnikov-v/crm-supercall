<?php
namespace frontend\components\web;

use common\components\web\Controller as CommonController;
use Yii;

/**
 * Class Controller
 * @package frontend\components\web
 */
class Controller extends CommonController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->layout === null){
            if (!\Yii::$app->user->isGuest){
                $this->layout = '@frontend/views/layouts/main';
            }
            else{
                $this->layout = '@frontend/views/layouts/landing';
            }
        }
    }
}

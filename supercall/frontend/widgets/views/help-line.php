<?php
/** @var array $helpline_phones */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use frontend\assets\HelpLineAsset;
use frontend\assets\widgets\navbar\ZoiperAsset;

HelpLineAsset::register($this);
ZoiperAsset::register($this);

?>

<hr/>

<?php foreach ($helpline_phones as $char_code => $helpline_phone) : ?>

    <?php if (!empty($helpline_phone)) : ?>

        <div class="row form-group">
            <div class="col-lg-12"><span class="flag-icon flag-icon-<?=strtolower($char_code)?>"></span>&nbsp;&nbsp;
                <a class="helpline" data-phone="<?=strtr($helpline_phone, ['+' => '']);?>" href="#" style="text-decoration: none;">📞
                <span><?=$helpline_phone;?></span></a></div>
        </div>

    <?php endif; ?>

<?php endforeach; ?>

<hr/>

<?php Modal::begin([
    'id' => 'modal_call_to',
    'size' => Modal::SIZE_SMALL,
    'header' => '<h4>' . yii::t('common', 'Позвонить') . ' <span class="call_to"></span></h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Отмена'), ['class' => 'btn btn-danger close_btn', 'data-dismiss' => 'modal'])
]); ?>

<div class="row">
    <div class="col-lg-6">
        <img name="call" data-click="false" style="width:90px; cursor: pointer" src="/resources/images/helpline/call.png" class="img-responsive center-block call_btn"/>
    </div>
    <div class="col-lg-6">
        <img name="hang" data-click="true" style="width:90px; cursor: pointer" src="/resources/images/helpline/hang.png" class="img-responsive center-block hang_btn"/>
    </div>
</div>

<?php Modal::end(); ?>




<?php

namespace frontend\widgets;

use common\widgets\base\Widget;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class HelpLine
 * @package frontend\widgets
 */
class HelpLine extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('help-line', [
            'helpline_phones' => ArrayHelper::map(yii::$app->user->getCountries(), 'char_code', 'helpline_phone')
        ]);
    }
}
<?php
namespace frontend\widgets\aside;


use common\models\User;
use common\widgets\base\Widget;

/**
 * Class ZoiperSlide
 * @package backend\widgets\aside
 */
class ZoiperSlide extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $user = User::findOne(\Yii::$app->user->id);
        return $this->render('zoiper-slide', [
            'data' => !empty($user->sipArray) ? $user->sipArray : [],
        ]);
    }
}

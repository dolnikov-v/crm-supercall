<?php
namespace frontend\widgets\aside;


use common\models\User;
use common\widgets\base\Widget;

/**
 * Class SipML5Slide
 * @package frontend\widgets\aside
 */
class SipML5Slide extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $user = User::findOne(\Yii::$app->user->id);
        return $this->render('sipml5-slide', [
            'data' => !empty($user->sipArray) ? $user->sipArray : [],
        ]);
    }
}

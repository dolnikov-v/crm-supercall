<?php
namespace frontend\widgets\aside;


use common\widgets\base\Widget;

/**
 * Class ZoiperJs
 * @package backend\widgets\aside
 */
class ZoiperJs extends Widget
{
    public $host;
    public $login;
    public $pass;
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('zoiper-js', [
            'host' => $this->host,
            'login' => $this->login,
            'pass' => $this->pass,
        ]);
    }
}

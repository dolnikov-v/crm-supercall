<?php
    use yii\helpers\Html;
    use frontend\assets\widgets\navbar\SipML5SlideAsset;
    
    SipML5SlideAsset::register($this);
?>

<div id="slideout" style="display: block;">
    <?= Html::img('/resources/images/common/telephone.png', ['height' => '72px', 'id' => 'telephone-btn']) ?>
    <div id="slideout_inner">
        <div id="sipml5Block">

        </div>

        <br/>
        <div class="form-group">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-10 caller_box">
                    <div class="row">
                        <div class="col-lg12 text-center sipml5"><b>sipML5</b></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-7">
                            <input type="text" class="form-control" id="phoneNumper">
                            <input type="text" class="form-control hidden" id="phoneNumper_dial">
                        </div>
                        <div class="col-lg-1">
                            <button type="button" id="remove_one" class="btn btn-danger btn-md"
                                    title="<?= yii::t('common', 'Удалить') ?>">
                                <span class="glyphicon glyphicon-arrow-left"></span>
                            </button>
                        </div>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-3">
                            <input type="text" readonly class="form-control" id="time">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg12"><br/></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">1</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">2</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">3</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">4</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">5</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">6</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg12"><br/></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">7</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">8</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">9</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">0</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">#</button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="num btn btn-primary btn-xs btn-block">+</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg12"><br/></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <button disabled="disabled" type="button" id="call_btn"
                                    class="btn btn-warning btn-md btn-block"
                                    title="<?= yii::t('common', 'Звонить') ?>">
                                <span class="glyphicon glyphicon-earphone"></span>
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button type="button" id="reset_btn" class="btn btn-danger btn-md btn-block"
                                    title="<?= yii::t('common', 'Сброс') ?>">
                                <span class="glyphicon glyphicon-phone-alt"></span>
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button type="button" id="pause_btn" class="btn btn-warning btn-md btn-block"
                                    title="<?= yii::t('common', 'Удержать') ?>">
                                <span class="glyphicon glyphicon-pause"></span>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg12"><br/></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <button type="button" id="options_btn" class="btn btn-default btn-xs btn-block"
                                    data-toggle="modal" data-target="#zoiperModal"
                                    title="<?= yii::t('common', 'Настройки') ?>">
                                <span class="glyphicon glyphicon-cog"></span>
                            </button>
                        </div>
                        <div class="col-lg-3">
                            <button type="button" id="settings_btn" class="btn btn-default btn-xs btn-block"
                                    title="<?= yii::t('common', 'Конфигурация аудио') ?>">
                                <span class="glyphicon glyphicon-headphones"></span>
                            </button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" id="volume_mute_btn" class="btn btn-default btn-xs btn-block"
                                    title="<?= yii::t('common', 'Отключить динамик') ?>">
                                <span class="glyphicon glyphicon-volume-off"></span>
                            </button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" id="speaker_mute_btn" class="btn btn-default btn-xs btn-block"
                                    title="<?= yii::t('common', 'Отключить микрофон') ?>">
                                <span class="glyphicon glyphicon-remove-circle"></span>
                            </button>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" id="log_btn" class="btn btn-default btn-xs btn-block"
                                    title="<?= yii::t('common', 'Log') ?>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12"><br/><br/></div>
                </div>
            </div>
        </div>
        <audio id="audio_remote" autoplay="autoplay"> </audio>
        <audio id="ringtone" loop="" src="/resources/sounds/ringtone.wav"> </audio>
        <audio id="ringbacktone" loop="" src="/resources/sounds/ringbacktone.wav"> </audio>
        <audio id="dtmfTone" src="/resources/sounds/dtmf.wav"> </audio>
    </div>
</div>
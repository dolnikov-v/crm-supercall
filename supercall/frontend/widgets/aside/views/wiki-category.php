<?php
use common\helpers\fonts\PeIcon;
use backend\widgets\Menu;

/** @var $items */
?>

<div class="text-center"><h4 class="center"><span class="<?=PeIcon::WIKI;?>"></span> <?= yii::t('common', 'База знаний') ?></h4></div>

<?php
echo Menu::widget([
    'options' => [
        'id' => 'side-menu',
        'class' => 'nav',
    ],
    'items' => $items
]);
?>


<?php

use common\assets\base\KVSAsset;
use common\assets\base\SocketIoAsset;
use common\assets\ProfileCommonAsset;
use common\components\web\View;
use common\models\Country;
use common\models\Media;
use common\modules\order\assets\SearchOrderAsset;
use frontend\assets\widgets\navbar\AutoCallActivityAsset;
use frontend\assets\widgets\navbar\MonitorAsset;
use frontend\assets\widgets\navbar\OrderMessengerAsset;
use frontend\modules\wiki\Module;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use common\widgets\base\Dropdown;
use frontend\assets\widgets\navbar\ProfileAsset;
use frontend\assets\widgets\navbar\OrderOnlineAsset;
use frontend\assets\widgets\navbar\TimerAsset;
use frontend\assets\widgets\navbar\OperatorActivityAsset;
use backend\modules\administration\assets\JsLoggerAsset;
use frontend\assets\widgets\navbar\WikiCategoryAsset;
use common\widgets\base\GlyphIcon;
use common\assets\vendor\MomentAsset;

ProfileCommonAsset::register($this);

if (yii::$app->user->isGuest) {
    $this->registerJS('window.user = false', View::POS_READY);
} else {
    $this->registerJS('window.user=' . Json::encode(yii::$app->user->identity), \yii\web\View::POS_HEAD, 'user');
    $this->registerJS('window.user_country=' . Json::encode(array_map(function ($v) {
            return yii::t('common', $v);
        }, ArrayHelper::map(yii::$app->user->getCountries(), 'id', 'name'))), \yii\web\View::POS_HEAD, 'user_country');

    //Отслеживать пассивность всех операторов
    //https://2wtrade-tasks.atlassian.net/browse/WCALL-512
    //if (yii::$app->user->identity->auto_call) {
    AutoCallActivityAsset::register($this);
    //}

    MonitorAsset::register($this);
}
/** @var string $isWiki */
if (!$isWiki) {
    OperatorActivityAsset::register($this);
    ProfileAsset::register($this);

    if (!yii::$app->user->identity->use_messenger) {
        OrderOnlineAsset::register($this);
    } else {
        OrderMessengerAsset::register($this);
    }
    SocketIoAsset::register($this);
    TimerAsset::register($this);
    SearchOrderAsset::register($this);
} else {
    //делаем шире боковую панель
    WikiCategoryAsset::register($this);
}

MomentAsset::register($this);
JsLoggerAsset::register($this);
KVSAsset::register($this);

$avatar = \common\models\Media::findOne(['user_id' => yii::$app->user->identity->id, 'type' => \common\models\Media::TYPE_PROFILE_IMAGE]);
?>

<div class="profile-picture">
    <div class="avatar" style="max-height:150px; overflow:hidden">
    <img class="<?= $avatar ? 'img-thumbnail' : 'img-circle m-b' ?>  width-150"
         src="<?= ($avatar ? Media::getAbsolutePath(Media::TYPE_PROFILE_IMAGE) . '/' . $avatar->filename : '/resources/images/common/avatar-profile.png') ?>""
    alt="avatar"/>
    </div>
    <br/>
    <div class="stats-label text-color">
        <span class="font-extra-bold font-uppercase">
            <?= Yii::$app->user->identity->username ?>
        </span>

        <?= Dropdown::widget([
            'label' => Html::tag('small', Yii::t('common', 'Профиль')),
            'withButton' => false,
            'links' => [
                [
                    'label' => Yii::t('common', 'Профиль'),
                    'url' => Url::to('/profile/control')
                ],
                '',
                [
                    'label' => Yii::t('common', 'Выход'),
                    'url' => Url::to('/auth/logout'),
                    'style' => 'logout'
                ],
            ]
        ]) ?>

        <?php if (!$isWiki) : ?>

        <div class="text-center mt__10" style="font-size: 80%">
            <div id="box--time-online-ready" class="">
                <?= Yii::t('common', 'Время онлайн') . ':&nbsp'; ?>
                <span id="time-online-ready"><?= $time; ?></span>
                <span id="currentTime" class="hidden"></span>
            </div>
        </div>

        <div id="ready-button-box">
            <input id="ready-button" type="checkbox" <?= Yii::$app->user->getIsReady() ? 'checked' : '' ?>>
            <label class="label-ready-button" for="ready-button"><?= Yii::t('common', 'Готов'); ?></label>
        </div>
        <div class="checkbox checkbox-success">
            <input type="checkbox"
                   id="check-unready-button" <?= Yii::$app->user->getCheckUnready() ? 'checked' : '' ?>>
            <label for="check-unready-button">
                <small><?= Yii::t('common', 'перейти в "Неготов" после сохранения'); ?></small>
            </label>
        </div>

        <div class="buttons_block">
            <?= Html::a(Yii::t('common', 'Мои перезвоны'), Url::to('/order/index/recall'), [
                'class' => 'btn btn-sm btn-primary btn-block zoomed',
                'target' => '_blank',
            ]); ?>
            <?= Html::a(Yii::t('common', 'Создать заказ'), Url::to('/order/change/create-form'), [
                'class' => 'btn btn-sm btn-primary btn-block zoomed',
                'target' => '_blank',
            ]); ?>

            <?= Html::a(Yii::t('common', 'База знаний'), Url::to('/wiki/article/index'), [
                'class' => 'btn btn-sm btn-primary btn-block zoomed',
                'target' => '_blank',
            ]); ?>
        </div>
        <div>
            <p id="boxPhoneCall" style="display:none"></p>
        </div>

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <hr/>

        <?php if (yii::$app->user->identity && yii::$app->user->identity->ip): ?>

            <div class="row text-left">
                <div class="col-lg-1 "><i class="fa fa-crosshairs fa-lg text-warning"></i></div>
                <div class="col-lg-1"></div>
                <div class="col-lg-10"><span class=""><?= yii::$app->user->identity->ip; ?></span></div>
            </div>
            <br/>

        <?php endif; ?>

        <div class="row text-left">
            <div class="col-lg-1 "><span id="zoiper-indicator" class="dot-red"></span></div>
            <div class="col-lg-1"></div>
            <div class="col-lg-10"><span class="status-zoiper">Zoiper register</span></div>
        </div>
        <?php if (yii::$app->user->identity->auto_call): ?>

            <br/>
            <div class="row text-left">
                <div class="col-lg-1 "><span id="queue-indicator" class="dot-red"></span></div>
                <div class="col-lg-1"></div>
                <div class="col-lg-10"><span title="" class="status-queue">Queue off</span>&nbsp;&nbsp;&nbsp;<span
                        title="<?= yii::t('common', 'Список очередей') ?>"
                        class="<?= GlyphIcon::TASKS ?> open-list-queue"></span></div>
            </div>

        <?php endif; ?>
    </div>

    <?php endif; ?>
</div>

<?php Modal::begin([
    'id' => 'modal_confirm_auto_call_activity',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false],
    'size' => Modal::SIZE_DEFAULT,
    'header' => '<h4>' . yii::t('common', 'Подтверждение') . '</h4>',
    'options' => ['tabindex' => 2, 'disabled' => true],
    'footer' => Html::button(yii::t('common', 'Да'), ['class' => 'btn btn-success', 'id' => 'modal_confirm_activity_ok'])
]); ?>

<div class="row">
    <div class="col-lg-12"><?= yii::t('common', 'Вы не проявляете активность продолжительный срок. Вы здесь ?') ?></div>
</div>

<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'list-queue-modal',
    'size' => Modal::SIZE_DEFAULT,
    'options' => [
        'data-backdrop' => 'static',
        'data-keyboard' => false
    ],
    'footer' => '<div class="last-update-queues pull-left"></div>' . Html::button(yii::t('common', 'Закрыть'), ['class' => 'btn btn-danger close_btn', 'data-dismiss' => 'modal']),
    'header' => yii::t('common', 'Список доступных очередей оператора')
]); ?>

<div class="row">
    <div class="col-lg-12"><span class="text-uppercase modal-header-text"></span></div>
</div>
<hr>
<div class="row">
    <div class="col-lg-12 list-queues">
        <div class="loader hidden"></div>
    </div>
</div>


<?php Modal::end(); ?>

<?php
namespace frontend\widgets\aside;


use common\widgets\base\Widget;
use frontend\modules\wiki\Module;
use Yii;

/**
 * Class Profile
 * @package backend\widgets\aside
 */
class Profile extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $profile = new \frontend\modules\profile\models\Profile();

        return $this->render('profile', [
            'time' => $profile->time,
            'isWiki' => Module::MODULE_WIKI == Yii::$app->controller->module->id
        ]);
    }
}

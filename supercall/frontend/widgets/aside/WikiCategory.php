<?php
namespace frontend\widgets\aside;

use backend\modules\wiki\models\WikiArticle;
use backend\modules\wiki\models\WikiCategory as WikiCategoryModel;
use common\helpers\fonts\PeIcon;
use common\models\User;
use common\widgets\base\Widget;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class WikiCategory
 * @package frontend\widgets\aside
 */
class WikiCategory extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $items = [];
        $listArticle = [];

        $wikiCategory = ArrayHelper::map(WikiCategoryModel::find()->active()->all(), 'id', 'title');
        $articles = WikiArticle::find()
            ->where(['active' => WikiArticle::ACTIVE])
            ->andWhere(['status' => WikiArticle::STATUS_PUBLISH])
            ->all();
        $id = yii::$app->request->get('id');        $user = User::findOne(yii::$app->user->identity->id);
        $rolesByUser = ($user) ? $user->getRolesNames(true) : [];

        foreach ($articles as $article) {
            if ($article->language_id == yii::$app->user->language->id) {
                $listArticle[$article->category_id][] = [
                    'label' => yii::t('common', $article->title),
                    'icon' => PeIcon::BOOK_COLLAPSED,
                    'url' => Url::toRoute(['/wiki/article/view/', 'id' => $article->id]),
                    'active' => $id == $article->id,
                ];
            }
        }

        foreach ($wikiCategory as $id => $category) {
            $visible = false;

            $categoryModel = WikiCategoryModel::findOne(['id' => $id]);

            if ($categoryModel) {

                $categoryRoles = ArrayHelper::getColumn($categoryModel->getRoles()->all(), 'auth_item_name');

                if ($categoryModel->isOpen()) {
                    $visible = true;
                } else {
                    foreach ($rolesByUser as $role) {
                        if (in_array($role, $categoryRoles)) {
                            $visible = true;
                            break;
                        }
                    }
                }

                $items[$id] = [
                    'label' => yii::t('common', $category),
                    'icon' => PeIcon::ALBUMS,
                    'url' => '#',
                    'visible' => $visible
                ];

                if (isset($listArticle[$id])) {
                    $items[$id]['items'] = $listArticle[$id];
                }
            }
        }

        return $this->render('wiki-category', [
            'items' => $items
        ]);
    }
}
<?php
    
namespace frontend\modules\profile\models;


use common\models\Timezone;
use common\models\UserReady;
use Yii;
use common\components\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Profile extends ActiveRecord
{
    // название роли Оператора
    // Данная роль необходима для фильтрации использования стратегии Очередй
    const ROLE_OPERATOR = 'operator';
    
    public $time = false;
    public $timestamp = 0;
    
    public function init()
    {
        parent::init();

        if(!Yii::$app->user->identity){
            yii::$app->controller->redirect('/auth/login/');
            Yii::$app->end();
        }

        $sumDayDiff = 0;
        $currentTime = 0;
//        if (Yii::$app->user->getIsReady()) {
            // Выборка за текущий день
           /*$from = (new \DateTime())
                ->setTime(0,0,0)
                ->getTimestamp();
            
            $to = (new \DateTime())
                ->setTime(23,59,59)
                ->getTimestamp();
            
            $query = UserReady::find()
                ->select([
                    'sum_diff' => new Expression('SUM(diff)')
                ])
                ->where(['user_id' => Yii::$app->user->id])
                ->andWhere([
                    'between', 'start_time', $from, $to
                ])
                ->andWhere(['is not', 'stop_time', null])
                ->one();

            $sumDayDiff = 0;//$query->sum_diff ?? 0;
        
            // Если отсчет времени идет
            /*if ($query->exists()) {
                $userReady = $query->all();
                // сумма всех уже "закрытых" промежутков
                $sumDayDiff = array_sum(ArrayHelper::getColumn($userReady, 'diff'));
            }*/
        
            // Отдельно подсчитаем текущий открытый статус Готов
           /* $queryCurrentRaw = UserReady::find()
                ->where(['user_id' => Yii::$app->user->id])
                ->orderBy(['id' => SORT_DESC])->limit(1)
                ->one();

            if(!is_null($queryCurrentRaw)){
                $start_time = $queryCurrentRaw->stop_time ?? $queryCurrentRaw->start_time;
                $timezone = Timezone::DEFAULT_TIMEZONE;
                $current_time = (new \DateTime('now', new \DateTimeZone($timezone)))->getTimestamp();

                $currentTime = $current_time - $start_time;
            }
        
            if ($queryCurrentRaw->exists()) {
                $start_time = $queryCurrentRaw->one()->stop_time ?? $queryCurrentRaw->one()->start_time;
                $timezone = Timezone::DEFAULT_TIMEZONE;
                $current_time = (new \DateTime('now', new \DateTimeZone($timezone)))->getTimestamp();

                $currentTime = $current_time - $start_time;
            }*/
//        }
        
        $this->getTimeByFormat($currentTime + $sumDayDiff);
    }
    
    private function getTimeByFormat($time, $format = 'H:i:s')
    {
        $this->timestamp = $time;
        $this->time = (new \DateTime('now', new \DateTimeZone('UTC')))
            ->setTimestamp($time)
            ->format($format);
    }
}
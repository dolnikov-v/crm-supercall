<?php

namespace frontend\modules\profile\controllers;

use common\components\base\AjaxFilter;
use common\components\web\User;
use common\models\AutoCall;
use frontend\components\web\Controller;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\httpclient\Client;

/**
 * Class AutoCallController
 * @package frontend\modules\profile\controllers
 */
class AutoCallController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'monitor' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
       if (in_array($action->id, ['monitor'])) {
            $this->enableCsrfValidation = false;
       }

        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionMonitor()
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(AutoCall::URL_MONITOR)
            ->setHeaders(['Authorization' => 'Basic ' . base64_encode(User::ACCESS_TYPE_CC)])
            ->setData([
                'action' => Yii::$app->request->post('action'),
                'sip_login' => Yii::$app->request->post('sip_login')
            ])
            ->send();

        return $response->content;
    }
}
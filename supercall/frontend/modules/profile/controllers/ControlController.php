<?php

namespace frontend\modules\profile\controllers;


use common\models\Media;
use frontend\modules\profile\models\Profile;
use Yii;
use common\models\User;
use common\models\UserReady;
use frontend\components\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

class ControlController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'change-ready' => ['POST'],
                    'delete-profile-image' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->enableCsrfValidation = true;
        $model = User::findOne(Yii::$app->user->id);

        if (Yii::$app->request->isPost) {


            $model->load(Yii::$app->request->post());


            $profileImage = UploadedFile::getInstance($model, 'profile_image');
            $modelProfileImage = Media::findOne(['user_id' => $model->id, 'type' => Media::TYPE_PROFILE_IMAGE]);

            if(!$modelProfileImage){
                $modelProfileImage = new Media();
            }

            if(!is_null($profileImage)) {
                $temp = explode(".", $profileImage->name);
                $ext = end($temp);

                $data = [
                    'Media' => [
                        'filename' => Yii::$app->security->generateRandomString() . ".{$ext}",
                        'filepath' => Media::getPath(Media::TYPE_PROFILE_IMAGE),
                        'user_id' => $model->id,
                        'type' => Media::TYPE_PROFILE_IMAGE,
                        'active' => Media::ACTIVE,
                        'created_at' => time(),
                        'updated_at' => time()
                    ]
                ];

                if ($modelProfileImage->load($data)) {
                    if (!$modelProfileImage->save()) {
                        Yii::$app->notifier->addNotifierErrorByModel($modelProfileImage);
                        $error = true;
                    } else {
                        $profileImage->saveAs($data['Media']['filepath'] . '/' . $data['Media']['filename']);
                    }
                } else {
                    Yii::$app->notifier->addNotifierErrorByModel($modelProfileImage);
                    $error = true;
                }
            }

            if (!$model->save()) {
                Yii::$app->notifier->addNotifierError(Yii::t('common', 'Настройки профиля не сохранились.'));
                $this->refresh();
            }

            $this->goHome();
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }


    /**
     * Функция меняет статус is_ready в табл. {{user}}
     * При выставлении is_ready=false, необходимо в табл.{{user_ready}} проставить параметр `stop_time`
     *
     * либо
     *
     * Включает, или отключает функционал, который после сохранения лида оператором определяет
     * необходимо ли перевести оператора в режим "Неготов"
     *
     */
    public function actionChangeReady()
    {
        $post = Yii::$app->request->post();

        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        if (UserReady::change(Yii::$app->user->id, $post)) {
            $data = ['result' => 'ok'];
        } else {
            $data = ['result' => 'error'];
            Yii::$app->notifier->addNotifierError('Ошибка смены статуса "Готов"');
        }

        $response->data = $data;

        return $response;
    }

    /**
     * Функция вернет переменные SIP оператора
     *
     * @return \yii\console\Response|Response
     */
    public function actionGetSip()
    {
        if (Yii::$app->request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;

            $user = User::findOne(Yii::$app->user->id);

            $response->data = [
                'result' => 'ok',
                'data' => $user->sipArray,
            ];

            return $response;
        }
    }

    /**
     * Функция определяет общее время Оператора в онлайне (статус ГОТОВ)
     * @return \yii\console\Response|Response
     */
    public function actionGetTime()
    {
        if (Yii::$app->request->isPost) {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_JSON;

            $profile = new Profile;
            $response->data = [
                'result' => 'ok',
                'data' => $profile->time,
                'timestamp' => $profile->timestamp,
                'milliseconds' => $profile->timestamp * 100,
            ];

            return $response;
        }
    }


    /**
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDeleteProfileImage()
    {
        $profileImage = Media::findOne(['user_id' => yii::$app->request->post('user_id'), 'type' => Media::TYPE_PROFILE_IMAGE]);

        if($profileImage){
            $profileImage->delete();
        }
    }

}
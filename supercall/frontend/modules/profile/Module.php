<?php
namespace frontend\modules\profile;

use frontend\components\base\Module as CommonModule;

/**
 * Class Module
 * @package common\modules\profile
 */
class Module extends CommonModule
{
    public $controllerNamespace = 'frontend\modules\profile\controllers';

}

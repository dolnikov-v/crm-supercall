<?php

use frontend\widgets\HelpLine;
use yii\helpers\Url;
use yii\helpers\Html;
use common\widgets\ActiveForm;

/** @var \common\models\User $model */
?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['index']), 'options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="row">
    <div style="padding:10px" class="alert-info col-lg-12">
        <?= Yii::t('common', 'Для смены картинки профиля - кликните по картинке профиля, загрузите изображение и сохраните профиль.'); ?>
    </div>
</div>

<br/>

<div class="col-lg-3 place-avatar hidden"></div>

<div class="">
    <?= $this->render('@common/modules/profile/views/layout'); ?>
</div>

<div class="row helpline_container">
    <div class="col-lg-12">
        <label><?= yii::t('common', 'Телефон доверия') ?></label><br/>

        <?= HelpLine::widget(); ?>

    </div>
</div>
<div class="row">
    <?php foreach ($model->sipArray as $key => $sip): ?>
        <div class="col-lg-4">
            <div class="form-group">
                <?= Html::label($key) ?>
                <?= Html::textInput("User[sipArray][$key]", $sip, [
                    'class' => 'form-control  ',
                ]); ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= $form->submit(Yii::t('common', 'Сохранить профиль')) ?>
        <?= $form->link(Yii::t('common', 'Отмена'), Url::toRoute('index')); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>


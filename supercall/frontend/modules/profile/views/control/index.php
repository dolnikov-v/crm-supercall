<?php
    use common\widgets\base\Panel;
/** @var \common\models\User $model */
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Профиль'),
    'showButtons' => true,
    'content' => $this->render('_edit-form', [
        'model' => $model
    ])
]) ?>


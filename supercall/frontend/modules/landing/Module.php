<?php
namespace frontend\modules\landing;

use frontend\components\base\Module as FrontendModule;
use Yii;

/**
 * Class Module
 * @package frontend\modules\landing
 */
class Module extends FrontendModule
{
    public $controllerNamespace = 'frontend\modules\landing\controllers';

}

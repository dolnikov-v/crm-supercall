<?php
namespace frontend\modules\landing\controllers;

use backend\modules\access\models\AuthAssignment;
use frontend\components\web\Controller;
use yii\filters\AccessControl;
use common\models\User;
use yii;
use yii\helpers\ArrayHelper;

/**
 * Class IndexController
 * @package frontend\modules\landing\controllers
 */
class IndexController extends Controller
{
    /*
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],

                        'matchCallback' => function ($rule, $action) {
                        $listRoles = AuthAssignment::findAll(['user_id' => Yii::$app->user->id]);
                        $arrayRoles = ArrayHelper::getColumn($listRoles, 'item_name');
                        $roles = array_keys(array_flip($arrayRoles));
                            return !in_array(User::ROLE_APPLICANT, $roles);
                        }

                    ],
                ],
            ],
        ];
    }
    */

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->view->params['body-class'] = 'landing-page';
        
        
        if (\Yii::$app->user->isGuest)
            $view = 'index';
        else
            $view = 'empty';
            
        return $this->render($view);
        
    }
}

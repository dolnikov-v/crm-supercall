<?php
use yii\helpers\Html;


/** @var $this yii\web\View */

$this->title = Yii::t('common', 'Main page');

?>

<header id="page-top">
    <div class="container">
        <div class="heading">
            <h1><?= 'Welcome to ' .Yii::$app->name ?></h1>
            <span>Contrary to popular belief, Lorem Ipsum is not<br/> simply random text for print.</span>
            <p class="small">
                Lorem Ipsum is simply dummy text of the printing and tLorem Ipsum is Lorem Ipsum is simply dummy of the
                simply dummy text of the ypesetting.
            </p>
            <a href="#" class="btn btn-success btn-sm">Learn more</a>
        </div>
    </div>
</header>
<section id="features2" class="bg-light">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12">
                <h2><span class="text-success">Special icons </span>for your app</h2>
                <p>Lorem Ipsum available, but the majority have suffered alteration euismod. </p>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-3">
                <h4 class="m-t-lg"><i class="pe-7s-airplay text-success icon-big"></i></h4>
                <strong>Lorem Ipsum available</strong>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                    alteration in some form.</p>
            </div>
            <div class="col-md-3">
                <h4 class="m-t-lg"><i class="pe-7s-science text-success icon-big"></i></h4>
                <strong>Lorem Ipsum available</strong>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                    alteration in some form.</p>
            </div>
            <div class="col-md-3">
                <h4 class="m-t-lg"><i class="pe-7s-display1 text-success icon-big"></i></h4>
                <strong>Lorem Ipsum available</strong>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                    alteration in some form.</p>
            </div>
            <div class="col-md-3">
                <h4 class="m-t-lg"><i class="pe-7s-cloud-upload text-success icon-big"></i></h4>
                <strong>Lorem Ipsum available</strong>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                    alteration in some form.</p>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-3">
                <h4 class="m-t-lg"><i class="pe-7s-global text-success icon-big"></i></h4>
                <strong>Lorem Ipsum available</strong>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                    alteration in some form.</p>
            </div>
            <div class="col-md-3">
                <h4 class="m-t-lg"><i class="pe-7s-battery text-success icon-big"></i></h4>
                <strong>Lorem Ipsum available</strong>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                    alteration in some form.</p>
            </div>
            <div class="col-md-3">
                <h4 class="m-t-lg"><i class="pe-7s-users text-success icon-big"></i></h4>
                <strong>Lorem Ipsum available</strong>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                    alteration in some form.</p>
            </div>
            <div class="col-md-3">
                <h4 class="m-t-lg"><i class="pe-7s-ticket text-success icon-big"></i></h4>
                <strong>Lorem Ipsum available</strong>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered
                    alteration in some form.</p>
            </div>
        </div>

    </div>
</section>

<section id="contact" class="bg-light">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-md-offset-3">
                <h2><span
                        class="text-success"><?= Yii::t('frontend', 'Свяжитесь с нами ') ?></span> <?= Yii::t('frontend', 'в любое время') ?>
                </h2>
                <p>
                    Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites
                    still in their infancy. Various versions have evolved over the years, sometimes.
                </p>
            </div>
        </div>
        <div class="row text-center m-t-lg">
            <div class="col-md-4 col-md-offset-3">

                <form class="form-horizontal" role="form" method="post" action="#">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Your full name"
                                   value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" name="email"
                                   placeholder="user@example.com" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message" class="col-sm-2 control-label">Message</label>

                        <div class="col-sm-10">
                            <textarea class="form-control message-form-feedback" rows="3" name="message"
                                      placeholder="Your message here..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input id="submit" name="submit" type="submit" value="Send us a message"
                                   class="btn btn-success">
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-3 text-left">
                <address>
                    <strong><span class="navy">Company name, Inc.</span></strong><br/>
                    601 Street name, 123<br/>
                    New York, De 34101<br/>
                    <abbr title="Phone">P:</abbr> (123) 678-8674
                </address>
                <p class="text-color">
                    Consectetur adipisicing elit. Aut eaque, totam corporis laboriosam veritatis quis ad perspiciatis,
                    totam corporis laboriosam veritatis, consectetur adipisicing elit quos non quis ad perspiciatis,
                    totam corporis ea,
                </p>
            </div>
        </div>
    </div>
</section>

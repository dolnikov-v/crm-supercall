<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\base\View $this */
/** @var \common\models\User $user */
/** @var string $password */
?>

<?php if(!isset($activatedmail)):?>

<?= yii::t('common', 'Данные регистрации:<hr/> <b>логин:</b> {username} (или {email}) <br/><b>пароль:</b> {password}. {postfix_message}' ,
    [
        'username' => $user->username,
        'email' => $user->email,
        'password' => $password,
        'postfix_message' => yii::t('common', 'Для подтверждения аккаунта необходимо в профиле личного кабинета {personal_area} загрузить сканкопию документа, подтверждающего Вашу личность', [
            'personal_area' => yii::$app->params['urlBackend']
        ])
    ]); ?>

<?php else : ?>

    <?= yii::t('common', 'Ваш аккаунт {operator_area} одобрен:<hr/> <b>логин:</b> {username} (или {email}) <br/><b>пароль:</b> выслан Вам в письме после регистрации.',
        [
            'username' => $user->username,
            'email' => $user->email,
            'operator_area' => yii::$app->params['urlFrontend']
        ]); ?>

<?php endif;?>

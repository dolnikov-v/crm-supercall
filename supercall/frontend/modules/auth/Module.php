<?php
namespace frontend\modules\auth;

use frontend\components\base\Module as FrontendModule;

/**
 * Class Module
 * @package frontend\modules\landing
 */
class Module extends FrontendModule
{
    public $controllerNamespace = 'frontend\modules\auth\controllers';

}

<?php
namespace frontend\modules\auth\models;

use common\models\User;
use common\models\UserCountry;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use common\models\Country;

/**
 * Class RegisterForm
 * @package frontend\modules\auth\models
 */
class RegisterForm extends Model
{
    public $country_id;
    public $first_name;
    public $last_name;
    public $phone;
    public $email;
    public $username;
    public $password;
    public $password_repeat;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['country_id', 'first_name', 'last_name', 'phone', 'email'], 'required'],
            ['email', 'email'],
            ['password', 'compare'],
            [
                ['first_name', 'last_name', 'phone', 'email'], function ($attr) {
                return \HTMLPurifier::instance()->purify($this->$attr);
            }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('common', 'Страна'),
            'username' => Yii::t('common', 'Имя пользователя'),
            'password' => Yii::t('common', 'Пароль'),
            'password_repeat' => Yii::t('common', 'Повтор пароля'),
        ];
    }

    /**
     * @return bool
     */
    public function register()
    {
        $transaction = yii::$app->db->beginTransaction();

        if (!$this->validate()) {
            return false;
        }
        $generatePassword = User::generatePassword();
        $user = new User();
        $user->status = User::STATUS_APPLICANT;
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->phone = $this->phone;
        $user->email = $this->email;
        $user->username = 'empty';
        $user->password = $generatePassword;
        $user->setPassword($generatePassword);
        $user->registration_token = Yii::$app->security->generateRandomString();
        $saved = $user->save(false);

        if ($saved) {
            //сгенерировать имя пользователя
            $country = Country::find()->where(['id' => $this->country_id])->one();

            if (!$country) {
                $saved = false;
                $transaction->rollBack();
            } else {
                $user->username = strtolower($country->char_code) . '-operator-' . $user->id;

                if (!$user->save()) {
                    $saved = false;
                    $transaction->rollBack();
                } else {
                    // Сразу проставляем страну пользователю, которую он выбрал при регистрации
                    $userCountry = new UserCountry();
                    $userCountry->user_id = $user->id;
                    $userCountry->country_id = $country->id;
                    $userCountry->active = 0;
                    $userCountry->created_at = time();
                    $userCountry->updated_at = time();

                    $saved = $userCountry->save();

                    if ($saved) {
                        Yii::$app->authManager->assign(Yii::$app->authManager->getRole(User::ROLE_APPLICANT), $user->id);
                        Yii::$app->mailer
                            ->compose('@frontend/modules/auth/mail/register', ['user' => $user, 'password' => $generatePassword])
                            ->setTo($user->email)
                            ->setSubject(Yii::t('common', 'Регистрация'))
                            ->send();
                    } else {
                        $transaction->rollBack();
                    }
                }
            }
        } else {
            $transaction->rollBack();
        }

        //отправка на мыло сообщения об успешной регистрации
        if ($saved) {
            $transaction->commit();
        }

        return $saved;
    }
}

<?php
namespace frontend\modules\auth\controllers;

use common\models\Country;
use common\models\User;
use common\components\base\AjaxFilter;
use frontend\components\web\Controller;
use frontend\modules\auth\models\RegisterForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * Class RegisterController
 * @package frontend\modules\auth\controllers
 */
class RegisterController extends Controller
{
    public $layout = 'login';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'send-sms-code',
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new RegisterForm();

        $countries = Country::find()
            ->where([Country::tableName() .'.active' => 1])
            ->all();
        $countries = ArrayHelper::map($countries, 'id', 'name');
        foreach ($countries as &$item) {
            $item = Yii::t('common', $item);
        }

        if (Yii::$app->request->isPost) {

            // проверка на готовность ко второму этапу регистрации
            if (is_numeric(Yii::$app->request->post('code'))) {
                if (is_numeric(Yii::$app->request->post('code_check')) && Yii::$app->request->post('code') == Yii::$app->request->post('code_check'))
                {
                    if (is_numeric(Yii::$app->request->post('country')) && !empty(Yii::$app->request->post('phone') && !empty(Yii::$app->request->post('email'))))
                    {
                        $model->country_id = Yii::$app->request->post('country');
                        $model->phone = Yii::$app->request->post('phone');
                        $model->email = Yii::$app->request->post('email');

                        $country_name = Country::find()
                            ->where([Country::tableName() .'.id' => Yii::$app->request->post('country')])
                            ->one()->name;
                        $country_name = Yii::t('common', $country_name);

                        $data = [
                            'country_name' => $country_name,
                        ];

                        return $this->render('second-step', [
                            'model' => $model,
                            'data' => $data,
                        ]);
                    }
                }
                else {
                    Yii::$app->notifier->addNotifierError(Yii::t('common', 'Не верный проверочный код!'));
                }
            }

            // отрабатываем второй этап регистрации
            if (Yii::$app->request->post('second_step') || yii::$app->getSession()->get('second_step') === true) {
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->register()) {
                        return $this->render('info');
                    } else {
                        Yii::$app->notifier->addNotifierErrorByModel($model);
                    }
                }
            }
        }

        return $this->render('index', [
            'countries' => $countries,
        ]);
    }

    /**
     * @return null|string
     * @throws HttpException
     */
    public function actionSendSmsCode() {

        $country = Country::find()
            ->where([Country::tableName() .'.id' => Yii::$app->request->post('country_id')])
            ->one();
        if (empty($country)) {
            return json_encode([
                'success' => false,
                'message' => Yii::t('common', 'Такая страна не найдена')
            ], JSON_UNESCAPED_UNICODE);
        }

        $char_code = $country->char_code;

        $data = [
            'char_code' => $char_code,
            'phone' => Yii::$app->request->post('phone'),
            'email' => Yii::$app->request->post('email'),
        ];

        $users = User::find()
            ->where([User::tableName() .'.phone' => $data['phone']])
            ->orWhere([User::tableName() .'.email' => $data['email']])
            ->all();
        if (!empty($users)) {
            return json_encode([
                'success' => false,
                'message' => Yii::t('common', 'Пользователь с таким номером или емайл уже существует.')
            ], JSON_UNESCAPED_UNICODE);
        }

        $res = $this->sendSmsCode($data);

        if (is_null($res)) {
            return json_encode([
                'success' => false,
                'message' => Yii::t('common', 'Не удалось отправить код регистрации по SMS.')
            ], JSON_UNESCAPED_UNICODE);
        }

        return $res;
    }

    /**
     * @param $data
     * @return null|string
     */
    public function sendSmsCode($data) {

        $code = '';
        for ($i = 0; $i < 4; $i++) {
            $code .= rand(0, 9);
        }

        $smsService = Yii::$app->get('SmsService');
        $response = $smsService->sendMessage(
            $data['phone'],
            null,
            Yii::t('common', Yii::t('common', 'Registration code is: ') .$code),
            [
                'char_code' => $data['char_code'],
            ]
        );

        if ($response['status'] != 'success') {
            return json_encode([
                'success' => false,
                'message' => Yii::t('common', 'Произошла ошибка при отправки смс, повторите попытку позже.'),
                'code' => $code
            ], JSON_UNESCAPED_UNICODE);

        }

        yii::$app->getSession()->set('second_step', true);
        return json_encode([
            'success' => true,
            'message' => $code
        ], JSON_UNESCAPED_UNICODE);
    }


    public function actionConfirm($token)
    {
        /** @var User $user */
        $user = User::find()->where([
            'registration_token' => $token,
            'status' => User::STATUS_NEW
        ])->one();

        if (!$user){
            throw new HttpException(404);
        }

        $user->status = User::STATUS_DEFAULT;
        $user->registration_token = null;
        $user->save(false, ['status', 'registration_token']);

        Yii::$app->user->login($user, 0);
        return $this->redirect('/');
    }
}

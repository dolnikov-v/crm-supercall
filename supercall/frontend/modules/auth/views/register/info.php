<?php

/** @var \yii\web\View $this */

$this->title = Yii::t('common', 'Регистрация');
?>

<div class="row">
    <div class="col-md-12">
        <div class="text-center m-b-md">
            <h3><?= Yii::t('common', 'Регистрация') ?></h3>
        </div>
        <div class="hpanel">
            <div class="panel-body">
                <div class="jumbotron">
                    <h2 class="display-3 text-center" style="margin: 20px; color: #62cb31;"><strong><?= Yii::t('common', 'Ваша заявка на регистрацию успешно принята') ?></strong></h2>
                    <hr class="m-y-2">
                    <p class="lead text-justify" style="margin: 20px;"><?= Yii::t('common', 'Пожалуйста, ожидайте решения руководства по Вашей заявке. Мы обязательно уведомим Вас по указанной Вами электронной почте') ?></p>
                    <hr class="m-y-2">
                    <a class="btn btn-success btn-block" href="/" style="margin: 0 auto; width: 75%;"><?= Yii::t('common', 'Вернуться на главную') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <strong><?= strtoupper(Yii::$app->params['companyName']);?></strong> - <?= Yii::t('common', 'Панель управления') ?> <br/>
        © <?= date('Y') ?> <?= Yii::t('common', 'Все права защищены') ?>
    </div>
</div>

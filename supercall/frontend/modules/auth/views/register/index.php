<?php
use common\widgets\ActiveForm;
use yii\helpers\Html;
use common\widgets\base\Select2;
use frontend\modules\auth\models\RegisterForm;

/** @var \yii\web\View $this */
/** @var array $countries */

$this->title = Yii::t('common', 'Регистрация');
?>

<div class="row">
    <div class="col-md-12">
        <div class="text-center m-b-md">
            <h3><?= Yii::t('common', 'Registration') ?></h3>
        </div>
        <div class="hpanel">
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['action' => '']); ?>
                <?= Html::input('hidden', 'code_check', null, [
                    'id' => 'code_check',
                    'class' => 'form-control',
                ]) ?>
                <div id="hide-input" style="">
                    <div class="form-group">
                        <label class="control-label" for="country_id"><?= Yii::t('common', 'Страна') ?></label>
                        <?=
                            Select2::widget([
                                'id' => 'country_id',
                                'name' => 'country',
                                'value' => null,
                                'items' => $countries,
                            ]);
                        ?>
                        <span class="help-block small"><?= Yii::t('common', 'Страна') ?></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="phone"><?= Yii::t('common', 'Телефон') ?></label>
                        <?= Html::input('text', 'phone', null, [
                            'id' => 'phone',
                            'class' => 'form-control',
                            'placeholder' => Yii::t('common', 'Телефон'),
                        ]) ?>
                        <span class="help-block small"><?= Yii::t('common', 'Телефон') ?></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="email"><?= Yii::t('common', 'Email') ?></label>
                        <?= Html::input('text', 'email', null, [
                            'id' => 'email',
                            'class' => 'form-control',
                            'placeholder' => Yii::t('common', 'Email'),
                        ]) ?>
                        <span class="help-block small"><?= Yii::t('common', 'Email') ?></span>
                    </div>
                    <div id="get-pass-button" class="btn btn-success btn-block"><?= Yii::t('common', 'Получить пароль') ?></div>
                </div>
                <div id="finish-register" style="display: none;">
                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            Please enter here code that you recieved by sms
                        </div>
                        <hr><br>
                        <div class="col-xs-3">
                            <?= Html::input('text', 'code', null, [
                                'id' => 'code',
                                'class' => 'form-control',
                                'placeholder' => Yii::t('common', 'Code'),
                            ]) ?>
                        </div>
                        <div class="col-xs-9">
                            <button id="finish-register-button" class="btn btn-success btn-block"><?= Yii::t('common', 'Продолжить') ?></button>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <strong><?= strtoupper(Yii::$app->params['companyName']);?></strong> - <?= Yii::t('common', 'Панель управления') ?> <br/>
        © <?= date('Y') ?> <?= Yii::t('common', 'Все права защищены') ?>
    </div>
</div>

<?php
$this->registerJs("
    $('#get-pass-button').on('click', function(){
        
        $.ajax({
            url: '/auth/register/send-sms-code',
            type: 'POST',
            data: {
                country_id: $('#country_id').val(),
                phone: $('#phone').val(),
                email: $('#email').val(),
            },
            success: function (response) {
                var data = JSON.parse(response);
                
                if(data.success == false){
                    toastr.error(data.message);
                    
                    if(typeof data.code != 'undefined'){
                        $('#code_check').val(data.code);
                    }
                }else{
                    $('#code_check').val(data.message);
                    $('#finish-register').css('display', 'block');
                    $('#get-pass-button').css('display', 'none');
                } 
            },
            error: function(response) {
                toastr.error('Sorry, but error found');
            }
        });
    });
");
?>
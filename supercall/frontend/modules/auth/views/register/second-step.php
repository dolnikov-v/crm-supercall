<?php
use common\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\auth\models\RegisterForm;

/** @var \yii\web\View $this */
/** @var RegisterForm $model */
/** @var array $data */

$this->title = Yii::t('common', 'Регистрация');
?>

<div class="row">
    <div class="col-md-12">
        <div class="text-center m-b-md">
            <h3><?= Yii::t('common', 'Регистрация') ?></h3>
        </div>
        <div class="hpanel">
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['action' => '']); ?>
                <div class="form-group">
                    <?= Html::input('hidden', 'second_step', 1, [
                        'id' => 'second-step',
                        'class' => 'form-control'
                    ]) ?>
                </div>
                <div class="form-group">
                    <label class="control-label" for="country_name"><?= Yii::t('common', 'Страна') ?></label>
                    <?= Html::input('text', 'country_name', $data['country_name'], [
                        'id' => 'country_name',
                        'class' => 'form-control',
                        'placeholder' => $data['country_name'],
                        'value' => $data['country_name'],
                        'readonly' => true,
                    ]) ?>
                    <span class="help-block small"><?= Yii::t('common', 'Страна') ?></span>
                </div>
                <div class="form-group">
                    <?= Html::input('hidden', Html::getInputName($model, 'country_id'), $model->country_id, [
                        'id' => Html::getInputId($model, 'country_id'),
                        'class' => 'form-control',
                        'value' => $model->country_id,
                    ]) ?>
                </div>
                <div class="form-group">
                    <label class="control-label"
                           for="<?= Html::getInputId($model, 'phone') ?>"><?= Yii::t('common', 'Телефон') ?></label>

                    <?= Html::input('text', Html::getInputName($model, 'phone'), $model->phone, [
                        'id' => Html::getInputId($model, 'phone'),
                        'class' => 'form-control',
                        'placeholder' => $model->phone,
                        'value' => $model->phone,
                        'readonly' => true,
                    ]) ?>

                    <span
                            class="help-block small"><?= Yii::t('common', 'Телефон') ?></span>
                </div>
                <div class="form-group">
                    <label class="control-label"
                           for="<?= Html::getInputId($model, 'email') ?>"><?= Yii::t('common', 'Email') ?></label>

                    <?= Html::input('text', Html::getInputName($model, 'email'), $model->email, [
                        'id' => Html::getInputId($model, 'email'),
                        'class' => 'form-control',
                        'placeholder' => $model->email,
                        'value' => $model->email,
                        'readonly' => true,
                    ]) ?>

                    <span
                            class="help-block small"><?= Yii::t('common', 'Телефон') ?></span>
                </div>
                <div class="form-group">
                    <label class="control-label"
                           for="<?= Html::getInputId($model, 'first_name') ?>"><?= Yii::t('common', 'Имя') ?></label>

                    <?= Html::input('text', Html::getInputName($model, 'first_name'), $model->first_name, [
                        'id' => Html::getInputId($model, 'first_name'),
                        'class' => 'form-control',
                        'placeholder' => Yii::t('common', 'Имя'),
                    ]) ?>

                    <span
                            class="help-block small"><?= Yii::t('common', 'Введите ваше имя') ?></span>
                </div>
                <div class="form-group">
                    <label class="control-label"
                           for="<?= Html::getInputId($model, 'last_name') ?>"><?= Yii::t('common', 'Фамилия') ?></label>

                    <?= Html::input('text', Html::getInputName($model, 'last_name'), $model->last_name, [
                        'id' => Html::getInputId($model, 'last_name'),
                        'class' => 'form-control',
                        'placeholder' => Yii::t('common', 'Фамилия'),
                    ]) ?>

                    <span
                            class="help-block small"><?= Yii::t('common', 'Введите ваше имя') ?></span>
                </div>
                <button class="btn btn-success btn-block"><?= Yii::t('common', 'Зарегистрироваться') ?></button>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <strong><?= strtoupper(Yii::$app->params['companyName']);?></strong> - <?= Yii::t('common', 'Панель управления') ?> <br/>
        © <?= date('Y') ?> <?= Yii::t('common', 'Все права защищены') ?>
    </div>
</div>

<?php
namespace frontend\modules\docs;

use frontend\components\base\Module as FrontendModule;

/**
 * Class Module
 * @package frontend\modules\docs
 */
class Module extends FrontendModule
{
    public $controllerNamespace = 'frontend\modules\docs\controllers';
    public $defaultRoute = 'index/index';
}

<?php
namespace frontend\modules\docs\controllers;

use frontend\components\web\Controller;
use yii\helpers\Markdown;

/**
 * Class IndexController
 * @package frontend\modules\landing\controllers
 */
class IndexController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->layout = 'docs';
        $this->view->params['sections'] = $this->getSections();
        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'sections' => $this->getSections()
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionSection($type, $id)
    {
        return $this->render('section', [
            'section' => $this->getPage($type, $id)
        ]);
    }

    /**
     * @return array
     */
    protected function getSections()
    {
        $sections = [];
        if (!\Yii::$app->cache->exists('api.sections')){
            foreach (new \DirectoryIterator(\Yii::getAlias('@api/docs')) as $file) {
                if ($file->getExtension() == 'md'){
                    $sections['default'][$file->getBasename('.md')] = [
                        'id' => $file->getBasename('.md'),
                        'title' => $file->getBasename('.md'),
                        'content' => Markdown::process(file_get_contents($file->getPathname()))
                    ];
                }
            }
            foreach (new \DirectoryIterator(\Yii::getAlias('@api/docs/genesys')) as $file) {
                if ($file->getExtension() == 'md'){
                    $sections['genesys'][$file->getBasename('.md')] = [
                        'id' => $file->getBasename('.md'),
                        'title' => $file->getBasename('.md'),
                        'content' => Markdown::process(file_get_contents($file->getPathname()))
                    ];
                }
            }
            
            \Yii::$app->cache->set('api.sections', $sections);
        }
        else{
            $sections = \Yii::$app->cache->get('api.sections');
        }
        return $sections;
    }

    /**
     * @param $id
     * @return array
     */
    protected function getPage($type, $id)
    {
        return $this->getSections()[$type][$id];
    }
}

<?php
use frontend\assets\base\ThemeAsset;
use common\assets\vendor\AnimateAsset;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var string $content */

ThemeAsset::register($this);
AnimateAsset::register($this);
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: <?= Yii::$app->name ?></title>
    <link rel="icon" href="<?= Yii::$app->params['urlFrontend'] ?>/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?= Yii::$app->params['urlFrontend'] ?>/favicon.ico" type="image/x-icon">
    <?php $this->head(); ?>
</head>
<body class="landing-page">
<p>Genesys:</p>
<?= \yii\widgets\Menu::widget([
        'items' => array_map(function($section){
            return [
                    'activateItems' => true,
                    'label' => $section['title'],
                    'url' => ['/docs/index/section', 'type' => 'genesys', 'id' => $section['id']],
            ];
        }, $this->params['sections']['genesys'])
]); ?>

<p>Партнеры:</p>
<?= \yii\widgets\Menu::widget([
    'items' => array_map(function($section){
        return [
            'activateItems' => true,
            'label' => $section['title'],
            'url' => ['/docs/index/section', 'type' => 'default', 'id' => $section['id']],
        ];
    }, $this->params['sections']['default'])
]); ?>

<?php $this->beginBody(); ?>
<?= $content ?>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>

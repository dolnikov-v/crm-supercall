<?php
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $section array */

$this->title = Yii::t('frontend', $section['title']);

?>

<section class="bg-light">
    <p>
        <?= $section['content']; ?>
    </p>
</section>

<?php
namespace frontend\modules\order\assets;


use yii\web\AssetBundle;

/**
 * Class Online Asset
 */
class PartnerCountryAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/modules/order';

    public $js = [
        'partner-country.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}

<?php
namespace frontend\modules\order\assets;


use yii\web\AssetBundle;

/**
 * Class Online Asset
 */
class OnlineAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/modules/order/online';

    public $js = [
        'online.js'
    ];

    public $depends = [
        'common\assets\vendor\JqueryAsset'
    ];
}

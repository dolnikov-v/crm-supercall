<?php
use common\modules\order\models\Order;
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var bool $editable */
/** @var array $types */
/** @var array $products */
/** @var array $shippings */
/** @var array $shipping_options */
/** @var string $operator */
/** @var array $countries */
/** @var array $formAttributes */
/** @var [] $packageList */
/** @var string $packageData */
/** @var string $productPrice */
/** @var  array $operators */
/** @var  string $partnerFormAdditionalPrice */

$this->title = Yii::t('common', 'Создание заказа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => Url::to(['/order/index'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Создание заказа'),
    'content' => $this->render('@common/modules/order/views/change/create', [
        'order' => $order,
        'editable' => $editable,
        'id_param' => $order->id,
        'types' => $types,
        'countries' => $countries,
        'products' => $products,
        'shippings' => $shippings,
        'is_default_shipping' => $is_default_shipping,
        'shipping_options' => $shipping_options,
        'products_price_data' => $products_price_data,
        'operator' => $operator,
        'formAttributes' => $formAttributes,
        'packageList' => $packageList,
        'packageData' => $packageData,
        'productPrice' => $productPrice,
        'priceShipping' => $priceShipping,
        'partner_created_at' => $partner_created_at,
        'masked_phones' => $masked_phones,
        'operators' => $operators,
        'partnerFormAdditionalPrice' => $partnerFormAdditionalPrice
    ])
]); ?>
<?php
use common\modules\order\models\Order;
use common\widgets\base\Panel;
use yii\helpers\Url;
use common\assets\widgets\PhoneIcon;
use common\modules\call\assets\CallHistoryAsset;
use common\modules\order\assets\ScriptsOperatorsAsset;

/** @var \yii\web\View $this */
/** @var Order $order */
/** @var array $types */
/** @var array $products */
/** @var array $shippings */
/** @var array $shipping_options */
/** @var string $operator */
/** @var array $orderTasks */
/** @var array $vat_list */
/** @var array $lastOrderTask */
/** @var array $formAttributes */
/** @var [] $packageList */
/** @var string $packageData */
/** @var boolean $masked_phones */
/** @var string|null $partner_created_at */
/** @var string $time_now_with_timezone */
/** @var string partnerFormAdditionalPrice*/
/** @var string $productPrice */

$this->title = Yii::t('common', 'Редактирование заказа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => Url::to(['/order/index'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];

PhoneIcon::register($this);
CallHistoryAsset::register($this);

//ScriptsOperatorsAsset::register($this);
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Заказ'),
    'content' => $this->render('@common/modules/order/views/change/index', [
        'order' => $order,
        'editable' => $editable,
        'id_param' => $order->id,
        'types' => $types,
        'products' => $products,
        'shippings' => $shippings,
        'countries' => $countries,
        'is_default_shipping' => $is_default_shipping,
        'shipping_options' => $shipping_options,
        'products_price_data' => $products_price_data,
        'operator' => $operator,
        'orderTasks' => $orderTasks,
        'lastOrderTask' => $lastOrderTask,
        'vat_list' => $vat_list,
        'formAttributes' => $formAttributes,
        'packageList' => $packageList,
        'packageData' => $packageData,
        'priceShipping' => isset($priceShipping) ? $priceShipping : [],
        'disabled_status_buttons' => false,//$disabled_status_buttons
        'masked_phones' => $order->form->mask_phones,
        'partner_created_at' => $partner_created_at,
        'time_now_with_timezone' => $time_now_with_timezone,
        'operators' => $operators,
        'hide_status_buttons' => false,
        'partnerFormAdditionalPrice' => $partnerFormAdditionalPrice,
        'productPrice' => $productPrice
    ])
]); ?>

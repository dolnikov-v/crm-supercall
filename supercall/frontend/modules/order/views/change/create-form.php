<?php
    use common\widgets\base\Panel;
    use yii\helpers\Url;
    
$this->title = Yii::t('common', 'Создание заказа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => Url::to(['/order/index'])];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div id="box--create-order-form">
    <?= Panel::widget([
        'title' => Yii::t('common', 'Создание заказа'),
        'content' => $this->render('pre-create-content', [
            'order' => $order,
            'partners' => $partners,
            'countries' => $countries,
        ])
    ]); ?>
</div>


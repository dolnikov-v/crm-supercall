<?php
    use common\widgets\ActiveForm;
    use \yii\helpers\Html;
    use frontend\modules\order\assets\PartnerCountryAsset;

    PartnerCountryAsset::register($this);
?>
    
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($order, 'partner_id')->select2List($partners, ['prompt' => '-']); ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($order, 'country_id')->select2List([], ['disabled' => true]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
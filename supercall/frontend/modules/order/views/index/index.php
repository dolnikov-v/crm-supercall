<?php
use common\components\grid\ActionColumn;
use common\components\grid\DateColumn;
use common\components\grid\GridView;
use common\components\grid\IdColumn;
use common\helpers\grid\DataProvider;
use common\modules\order\models\Order;
use common\widgets\base\Panel;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \common\models\search\ProductSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var array $statuses */
/** @var array $products */
/** @var array $partners */

$this->title = Yii::t('common', 'Список заказов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_view-filter', [
        'modelSearch' => $modelSearch,
        'statuses' => $statuses,
        'products' => $products,
        'partners' => $partners,
    ])
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Таблица с заказами'),
    'actions' => DataProvider::renderSummary($dataProvider),
    'contentPadding' => false,
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => IdColumn::className(),
            ],
            'partner.name',
            [
                'attribute' => 'foreign_id',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'status',
                'enableSorting' => false,
                'content' => function ($model) {
                    return Order::getStatusesCollection()[$model->status];
                }
            ],
            [
                'attribute' => 'customer_full_name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'customer_phone',
                'enableSorting' => false,
            ],


            ['attribute' => 'created_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'attribute' => 'updated_at',
                'class' => DateColumn::className(),
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['/order/change', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('order.change.index');
                        }
                    ],
                ]
            ]
        ],
    ]),
]) ?>

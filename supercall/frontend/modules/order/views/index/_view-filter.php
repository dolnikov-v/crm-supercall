<?php
use common\widgets\ActiveForm;
use yii\helpers\Url;

/** @var \common\models\search\ProductSearch $modelSearch */
/** @var array $statuses */
/** @var array $products */
/** @var array $partners */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'partner_id')->select2List($partners, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'product')->select2List($products, [
            'prompt' => '—'
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'status')->select2List($statuses, [
            'prompt' => '—'
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->submit(Yii::t('common', 'Применить')); ?>
        <?= $form->link(Yii::t('common', 'Сбросить фильтр'), Url::toRoute('index')); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

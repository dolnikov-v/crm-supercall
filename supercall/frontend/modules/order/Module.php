<?php
namespace frontend\modules\order;

use frontend\components\base\Module as FrontendModule;

/**
 * Class Module
 * @package frontend\modules\order
 */
class Module extends FrontendModule
{
    public $controllerNamespace = 'frontend\modules\order\controllers';
}

<?php
namespace frontend\modules\order\controllers;

use common\models\Product;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderProductLog;
use common\modules\order\models\search\OrderSearch;
use common\modules\partner\models\Partner;
use frontend\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use backend\modules\order\models\OrderTask;
use backend\modules\order\controllers\OrderTaskController;


/**
 * Class IndexController
 * @package frontend\modules\order\controllers
 */
class IndexController extends \common\modules\order\controllers\IndexController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $modelProducts = $this->findProducts($model->id);

        $OrderTaskController = new OrderTaskController('order-task', yii::$app->getModule('order'));

        $lastOrderTask = $OrderTaskController->getLastTask($id, array_keys(OrderTask::getTaskStatuses()));

        return $this->render('view', [
            'model' => $model,
            'modelProducts' => $modelProducts,
            'statuses' => Order::getStatusesCollection(),
            'partners' => Partner::find()->collection(),
            'products' => Product::find()->collection(),
            'lastOrderTask' => is_null($lastOrderTask) ? false : [$lastOrderTask],
        ]);
    }

}

<?php

namespace frontend\modules\order\controllers;

use backend\modules\queue\models\QueueUser;
use common\components\ChatApiCom;
use common\components\Platinum;
use common\models\QueueOrder;
use common\models\Timezone;
use common\modules\order\models\Order;

use common\modules\order\models\OrderProduct;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * Class LiveMessageController
 * @package frontend\modules\order\controllers
 */
class LiveMessageController extends \api\modules\order\controllers\LiveMessageController
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-order-form' => ['post'],
                    'get-order-from-queue' => ['post']
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionGetOrderForm()
    {
        $order_id = yii::$app->request->post('order_id');
        $order = Order::findOne(['id' => $order_id]);

        $url = $order->generateIframeUrl(yii::$app->user->identity, false, false);

        $result = [
            'success' => true,
            'message' => $url,
            'order_id' => $order_id
        ];

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return string
     */
    public function actionGetOrderFromQueue()
    {
        $current_time = $currentTime = (new \DateTime())
            ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
            ->getTimestamp();

        $order_ids = yii::$app->request->post('order_ids');
        $user_id = yii::$app->request->post('user_id');

        $query = Order::find()
            ->leftJoin(QueueOrder::tableName(), Order::tableName() . '.id = ' . QueueOrder::tableName() . '.order_id')
            ->where(['id' => $order_ids])
            ->andWhere(['is', QueueOrder::tableName() . '.blocked_to', NULL])
            ->andWhere([Order::tableName() . '.prevent_queue_id' => ArrayHelper::getColumn(QueueUser::findAll(['user_id' => $user_id]), 'queue_id')]);

        //если у оператора прикреплены товары
        if (!empty($products = yii::$app->user->getUserProduct())) {
            $query->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id = ' . Order::tableName() . '.id');
            //товары должны совпадать с товарами заказа
            $query->andWhere(['in', OrderProduct::tableName() . '.product_id', $products]);
        }

        $orders = $query->all();

        foreach ($orders as $order) {
            if (is_null($order->blocked_to) || $order->blocked_to < $current_time) {
                $order_id = $order->id;
            }
            break;
        }

        if (isset($order_id)) {
            $result = [
                'success' => true,
                'message' => $order_id
            ];

            $block = QueueOrder::setBlockOrder(yii::$app->user->id, $order_id);

            if (!$block['success']) {
                $logger = yii::$app->get('logger');
                $logger->action = basename(__METHOD__);
                $logger->type = Platinum::LOGGER_TYPE;
                $logger->tags = [
                    'order_id' => $order_id,
                    'user_id' => yii::$app->user->id,
                ];
                $logger->error = json_encode($block['message'], 256);
                $logger->save();
            }

            $this->deleteOrderFromNodeJSQueue($order_id);
        } else {
            $result = [
                'success' => false,
                'message' => yii::t('common', 'Заказов в очереди нет')
            ];
        }

        return json_encode($result, 256);
    }

    public function deleteOrderFromNodeJSQueue($order_id)
    {
        /** @var ChatApiCom $chatApiCom */
        $chatApiCom = yii::$app->get('ChatApiCom');
        $json_data = json_encode([
            'order_id' => $order_id,
        ]);

        $ch = curl_init($chatApiCom->deleteOrderFromQueue);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data))
        );


        $res = curl_exec($ch) . "\n";
        curl_close($ch);
    }
}
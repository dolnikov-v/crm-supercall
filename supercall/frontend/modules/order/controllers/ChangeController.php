<?php

namespace frontend\modules\order\controllers;

use backend\modules\catalog\models\PriceShipping;
use backend\modules\operator\models\ListOperators;
use backend\modules\order\models\OrderBackup;
use backend\modules\order\models\OrderUserBackup;
use backend\modules\queue\models\Queue;
use backend\modules\queue\models\QueueUser;
use backend\modules\stats\models\OrderProcessingTime;
use common\components\base\AjaxFilter;
use common\models\Asterisk;
use common\models\AutoCall;
use common\models\AutoCallLog;
use common\models\Country;
use common\models\IncomingCall;
use common\models\Timezone;
use common\models\User;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use common\modules\order\models\OrderType;
use common\modules\order\models\query\OrderQuery;
use common\modules\order\models\search\SearchOrder;
use common\modules\partner\models\Package;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAdditionPrice;
use common\modules\partner\models\PartnerProduct;
use common\modules\partner\models\PartnerShipping;
use common\widgets\ActiveForm;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Query;
use yii\db\QueryBuilder;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Response;
use frontend\modules\profile\models\Profile;

/**
 * Class ChangeController
 * @package frontend\modules\order\controllers
 */
class ChangeController extends \common\modules\order\controllers\ChangeController
{
    // хранит модель заказа
    // необходимо при создании заказа
    private $orderModel;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'create-form'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'get-search-form-result',
                    'lock-order',
                    'unlock-order',
                    'continue-operator-work',
                    'shipping-list-by-broker',
                ]
            ]
        ];
    }


    /**
     * Можно использовать как с клиента, так и с бекенда
     * если с клиента - то посылать доп. параметр json = true
     * @return array|string
     */
    public function actionGetPartnerCountries()
    {
        $post = yii::$app->request->post();

        if (isset($post['partner_id'])) {
            $partner_id = $post['partner_id'];
            $json = true;
        }

        $partner_id = yii::$app->request->post('partner_id');

        if (empty($partner_id)) {
            return false;
        }

        $countries = PartnerForm::find()
            ->distinct()
            ->leftJoin(Country::tableName(), Country::tableName() . '.id=' . PartnerForm::tableName() . '.country_id')
            ->where(['partner_id' => $partner_id])
            ->all();

        $listCountries = ArrayHelper::map($countries, 'country_id', 'country.name');

        $translated_countries = [];

        foreach ($listCountries as $k => $country) {
            $translated_countries[$k] = yii::t('common', $country);
        }

        return isset($json) ? json_encode($translated_countries, JSON_UNESCAPED_UNICODE) : $translated_countries;
    }

    /**
     * Функция для создания нового заказа из админки
     *
     * Предварительно:
     * Создать роль operator
     * Назначить пользователю данную роль
     * Добавить операцию profile.control и дать права роли operator на эту операцию
     * Также этой роли дать права на след.операции:
     * home.index.index
     * profile.control
     * i18n.language
     * order.change
     * order.index
     *
     * Создать новый Тип: Созданный (created) в Справочники => Типы заказов, ID=4
     * Задать данный ID всем созданным из Админки заказам
     *
     * @return string
     * @throws \Exception
     */
    public function actionCreateForm()
    {
        $order = new Order();

        $countries = Country::find()->active()->collection('common');

        $partners = Partner::find()->active()->collection();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post('Order');
            $order->country_id = (int)$post['country_id'];
            $order->partner_id = (int)$post['partner_id'];

            $query = PartnerForm::find()->where([
                'partner_id' => $order->partner_id,
                'country_id' => $order->country_id,
                'active' => 1
            ]);
            if ($query->exists()) {
                $form = $query->one();

                $session = Yii::$app->session;
                $key = 'create_order_hash_' . Yii::$app->user->id;
                $hash = sha1(Yii::$app->user->id . $order->country_id . $order->partner_id);

                $session->set($key, (new \ArrayObject));
                $session[$key]['hash'] = $hash;
                $session[$key]['country_id'] = $order->country_id;
                $session[$key]['partner_id'] = $order->partner_id;
                $session[$key]['form_id'] = $form->id;

                return $this->redirect(Url::toRoute(['create', 'hash' => $hash]));

            } else {
                throw new InvalidParamException(Yii::t('common', 'нет такой формы'));
            }
        }

        return $this->render('create-form', [
            'countries' => $countries,
            'partners' => $partners,
            'order' => $order,
        ]);
    }


    public function actionCreate($hash)
    {
        $session = Yii::$app->session;
        $key = 'create_order_hash_' . Yii::$app->user->id;

        $order = new Order();

        if (!$session->has($key) || $session[$key]['hash'] !== $hash) {
            return $this->redirect(Url::toRoute(['create-form']));
        }

        $order->country_id = $session[$key]['country_id'];
        $order->language_id = $order->country->language_id;
        $order->partner_id = $session[$key]['partner_id'];
        $order->form_id = $session[$key]['form_id'];
        // Тип по умолчанию created или Созданный
        // Не забыть создать, иначе будет js-ошибка
        $order->type_id = 2; // тип "Созданный"
        $order->foreign_id = null;

        $orderValidate = clone $order;
        if (Yii::$app->request->isPost) {
            $order->load(Yii::$app->request->post());

            // сохранить продукты
            /** @var OrderProduct[] $products */
            $products = [];

            /// Валидация на пустой продукт и обязательные поля
            if (Yii::$app->request->post('ajax') == 'form-order') {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $post = Yii::$app->request->post();
                unset($post['Order']['address']);
                $orderValidate->load($post);
                $orderValidate->populateRelation('orderProducts', $products);
                return ActiveForm::validate($orderValidate, $orderValidate->getGeneralModel());
            }

            $order->beginSave();

            try {
                if (!$order->save()) {
                    throw new \Exception(Yii::t('common', 'Не удалось сохранить заказ'));
                }
                /** @var OrderProduct[] $existing_products */
                $existing_products = ArrayHelper::index($order->orderProducts, 'id');

                //отделить товары и promo товары
                $orderProducts = OrderProduct::selectProducts(Yii::$app->request->post('OrderProduct'));

                //Promo products
                //определить список promo товаров
                $orderPromoProducts = OrderProduct::selectPromoProducts(Yii::$app->request->post('OrderProduct'));
                //end Promo products

                // Сохраним продукты после того, как сохраним сам заказ
                foreach ($orderProducts as $item) {
                    // c пустым product_id игнорируем
                    if (!empty($item['product_id'])) {
                        $product = ArrayHelper::remove($existing_products, $item['id']);
                        if (!$product) {
                            $product = new OrderProduct();
                            $product->order_id = $order->id;
                        }
                        //явно указать текущую модель заказа в виде связи чтобы иметь доступ к иду группы для логов
                        $product->populateRelation('order', $order);
                        $product->load($item, '');
                        $products[] = $product;
                    }
                }

                //Promo products
                //охранить promo товары к заказу
                //Именно в транзакции
                OrderProduct::savePromoProducts($orderPromoProducts, $order);
                //end Promo products

                // Если нет ни одного продукта
                if (!empty($products))
                    foreach ($products as $item) {
                        if (!$item->save()) {
                            throw new \Exception(Yii::t('common', 'Не удалось сохранить заказ'));
                        }
                    }

                if (!empty($existing_products))
                    foreach ($existing_products as $existing_product) {
                        if (!$existing_product->delete()) {
                            throw new \Exception(Yii::t('common', 'Не удалось сохранить заказ'));
                        }
                    }

                $order->completeSave();

                //"Списание остатков" товаров у партнёра
                Order::withdrawProductsOfStock($order->id);

                return $this->redirect(Url::to(['change/index/' . $order->id]));
            } catch (\Exception $e) {
                $order->cancelSave([$e->getFile(), $e->getLine(), $e->getMessage()]);
                throw $e;
            }

        }

        $types = ArrayHelper::map(
            OrderType::find()->all(),
            'id',
            'name'
        );

        // Вытащим все товары с ценами, установленные Партнером
        // Вытащим все товары с ценами, установленные Партнером
        $productsWithPrice = PartnerProduct::find()
            ->select(['partner_product_id', 'product_id', 'price_data'])
            ->where(['country_id' => $order->country_id, 'partner_id' => $order->partner_id])
            ->orderBy('partner_product_id')
            ->with('product')
            ->active()
            ->all();

        // Далее, необходимо вытащить их имена и ID в нашей системе
        $products = ['' => ''] + ArrayHelper::map(
                $productsWithPrice,
                'product_id',
                'product.name'
            );

        $products_price_data = [];
        foreach ($productsWithPrice as $partnerProduct) {
            // для каждого количества
            foreach (OrderProduct::countList() as $keyProduct => $quantity) {
                $products_price_data[$partnerProduct->product_id][$keyProduct] = $this->productPrice($partnerProduct->product_id, $order, $quantity);
            }
        }


        $query = PartnerShipping::find()->where([
            'partner_id' => $order->partner_id,
            'country_id' => $order->country_id
        ])->active();

        // UN-56 Доставка по-умолчанию
        $is_default = false; // по умолчанию на параметр is_default не смотрим

        $shippings = $query->all();
        $shipping_options = ArrayHelper::map($shippings, 'shipping.id', function (PartnerShipping $shipping) {
            return [
                'data-price' => $shipping->price,
                'data-dynamic_price' => $shipping->dynamic_price ? 1 : 0,
            ];
        });
        $shippings = ['' => ''] + ArrayHelper::map($shippings, 'shipping.id', 'shipping.name');
        $countries = ['' => ''] + Country::getAllowCountries();

        $packages = Package::getPackagesByPartnerCountry($order->partner_id, $order->country_id);
        $packagesList = [];
        $packagesData = [];
        $packagesList[''] = '-';
        $productPrices = [];

        foreach ($packages as $package) {
            $full_name_package = (!empty($package['prefix'])) ? ($package['prefix'] . ' ' . $package['name']) : $package['name'];
            $packagesList[$package['id']] = $full_name_package;

            $packageGifts = [];

            if (isset($package->packageGifts) && !empty($package->packageGifts)) {
                foreach ($package->packageGifts as $gift) {
                    $packageGifts[] = [
                        'product_id' => $gift->product_id,
                        'free_amount' => $gift->free_amount,
                        'product_name' => $gift->product->name
                    ];
                }

            }


            $packagesData[$package['id']] = [
                'id' => $package['id'],
                'name' => $full_name_package,
                'partner' => $package['partner_id'],
                'country' => $package['country_id'],
                'product' => $package['product_id'],
                'paid_amount' => $package['paid_amount'],
                'gifts' => $packageGifts,
                'product_price' => $package['product_price']
            ];

            $productPrices[$package['id']] = $package['product_price'];
        }

        $priceShipping = PriceShipping::find()
            ->where(['partner_id' => $order->partner_id])
            ->where(['country_id' => $order->country_id])
            ->asArray()
            ->all();

        $partnerFormAdditionalPrice = PartnerFormAdditionPrice::findOne(['form_id' => $order->form_id]);
        if (!empty($partnerFormAdditionalPrice)) {
            $partnerFormAdditionalPrice = json::encode($partnerFormAdditionalPrice);
        }

        return $this->render('create', [
            'order' => $order,
            'editable' => $this->getOrderEditable($order),
            'types' => $types,
            'products' => $products,
            'shippings' => $shippings,
            'countries' => ['' => ''] + Country::getAllowCountries(),
            'is_default_shipping' => $is_default,
            'shipping_options' => $shipping_options,
            'products_price_data' => Json::encode($products_price_data),
            'operator' => '',
            'formAttributes' => $this->getFormAttribute($order->country_id, $order->partner_id, $order->form_id),
            'packageList' => $packagesList,
            'packageData' => json_encode($packagesData, JSON_UNESCAPED_UNICODE),
            'priceShipping' => json_encode((is_null($priceShipping) ? [] : $priceShipping), JSON_UNESCAPED_UNICODE),
            'operators' => ArrayHelper::map(ListOperators::getOperators($order->country->id), 'id', 'username'),
            'partner_created_at' => $order->created_at,
            'masked_phones' => $order->form->mask_phones,
            'partnerFormAdditionalPrice' => $partnerFormAdditionalPrice,
            'productPrice' => json_encode($productPrices)
        ]);

    }

    /**
     * Получить форму заказа асинхронно
     */
    public function actionGetOrderForm()
    {
        if (Yii::$app->request->isAjax) {
            $res = Yii::$app->response;
            $res->format = Response::FORMAT_JSON;

            $user = User::findOne(['id' => Yii::$app->user->id]);

            if (!$user) {
                $data = [
                    'result' => 'searching',
                    'message' => Yii::t('common', 'Ожидайте пожалуйста: мы ищем для вас свежий заказ'),
                    'error' => Yii::t('common', 'Юзер не найден')
                ];

                $res->data = $data;
                return $res;
            }

            // тек.время по дефолт.таймзоне с временем блокировки
            $this->current_time = (new \DateTime())
                ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
                ->getTimestamp();

            //Если у оператора стоит автодозвон - ищем заказ в табличке автодозвона
            if ($user->auto_call == User::AUTO_CALL_ON || $user->incoming_call) {
                $sipData = json_decode($user->sip, 1);

                $autoCallOrderQuery = AutoCall::find()->where([
                    'status' => '0',
                    'user_sip' => $sipData['sip_login'],
                    'call_status' => 'ANSWERED'
                ])->orderBy(['id' => SORT_DESC])->limit(1);

                //Для случаев если по какой-то причине оператор закончил работу с заказом, но из queue_order заказ не снялся с оператора
                //Например - работал с заказом и перезагрузил страницу

                //если жёстко удалять - оператор на входящем звонке может не получить форму заказа
                //if ($user->auto_call == User::AUTO_CALL_ON && !$autoCallOrderQuery->exists()) {
                //  Yii::$app->db->createCommand('DELETE FROM "queue_order" WHERE user_id = ' . $user->id)->execute();
                //}

                if ($user->auto_call == User::AUTO_CALL_ON && $autoCallOrderQuery->exists()) {
                    $autoCallOrder = $autoCallOrderQuery->one();

                    $asteriskUser = Asterisk::getSystemUser();
                    $au = $asteriskUser;
                    //TODO убрать вообще добавление в в таблицу queue_order пользователя Asterisk2::getSystemUser()
                    if (isset($user->id)) {
                        $asteriskUser = $user->id;
                    }

                    $orderData = [
                        'user_id' => $asteriskUser,
                        'order_id' => $autoCallOrder->order_id
                    ];
                    $queueOrderQuery = (new Query())
                        ->from('{{%queue_order}}')
                        ->where($orderData);

                    if (!$queueOrderQuery->exists()) {
                        $orderData['blocked_to'] = $this->current_time + 1800;
                        $insResult = Yii::$app->db->createCommand()
                            ->insert('{{%queue_order}}', $orderData)
                            ->execute();
                        //После того как добавили связку ордер-юзер удаляем из auto_call запись с этим ордером
                        Yii::$app->db->createCommand('DELETE FROM "auto_call" WHERE order_id = ' . $autoCallOrder->order_id)->execute();
                        Yii::$app->db->createCommand('DELETE FROM "queue_order" WHERE order_id = ' . $autoCallOrder->order_id . ' and user_id = ' . $au)->execute();
                    }
                } elseif ($user->incoming_call && IncomingCall::isThereForUserSip($sipData['sip_login'])) {
                    $data = [
                        'result' => 'search_form',
                        'content' => ''
                    ];

                    $phone = IncomingCall::find()
                        ->byUserSip($sipData['sip_login'])
                        ->orderBy(['id' => SORT_DESC])
                        ->select(['phone'])
                        ->scalar();
                    $searchModel = new SearchOrder();
                    $dataProvider = $searchModel->search([$searchModel->formName() => ['phone' => $phone]]);
                    if (empty($dataProvider->models)) {
                        $searchModel->phone = $phone;//TODO пока оставим так, что бы оператор знал с какого номера пришел звонок
                    }

                    $data['content'] = $this->renderPartial('@common/modules/order/views/change/search', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider
                    ]);

                    // Отметим, что по звонку кто то ответил
                    IncomingCall::updateAll([
                        'user_sip' => $sipData['sip_login'],
                        'phone' => $phone,
                        'status' => IncomingCall::STATUS_NEW,
                    ], ['status' => IncomingCall::STATUS_IN_PROGRESS]);
                    Asterisk::setUserPaused(1);
                    $res->data = $data;

                    return $res;
                } else {
                    $data = [
                        'result' => 'searching',
                        'message' => Yii::t('common', 'Ожидайте пожалуйста: мы ищем для вас свежий заказ'),
                        '$autoCallOrderQuery' => isset($autoCallOrderQuery) ? $autoCallOrderQuery->createCommand()->getRawSql() : ''
                    ];

                    $res->data = $data;
                    return $res;
                }
            } elseif (in_array(User::ROLE_LOGISTICIAN, array_keys(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)))) {
                $data = [
                    'result' => 'search_form',
                    'content' => ''
                ];

                $searchModel = new SearchOrder();
                $dataProvider = $searchModel->search([$searchModel->formName() => [
                    'country_id' => ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id'),
                    'type_id' => [OrderType::TYPE_NEW, OrderType::TYPE_CHECK_ADDRESS],
                    'status' => array_merge(array_keys(Order::getNotFinalStatuses()), [4])
                ]]);

                $data['content'] = $this->renderPartial('@common/modules/order/views/change/search', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
                ]);

                $res->data = $data;

                return $res;
            } else {
                // Найдем заказ на данного Оператора в табл. "queue_order"
                $queueOrderQuery = (new Query())
                    ->from('{{%queue_order}}')
                    ->where([
                        'user_id' => Yii::$app->user->id,
                    ]);
            }

            // Проверим наличие ID заказа в сессии
            // Если в сессии есть ID заказа, то новый не дергаем
            if (!$queueOrderQuery->exists()) {
                // Покажем надпись на странице
                // если заказ не будет найден
                $data = [
                    'result' => 'searching',
                    'message' => Yii::t('common', 'Ожидайте пожалуйста: мы ищем для вас свежий заказ')
                ];

                // Только из очередей, доступных данному Оператору
                $accessQueues = QueueUser::find()
                    ->select(['queue_id'])
                    ->where(['user_id' => \Yii::$app->user->id])
                    ->column();

                $activeQueues = Queue::find()
                    ->priority()
                    ->active()
                    ->where(['in', 'id', $accessQueues])
                    ->all();

                foreach ($activeQueues as $queue) {
                    $message = Yii::$app->amqp->basicGet($queue->id);
                    if (isset($message)) {
                        // вытащим ID заказа из очереди
                        Yii::$app->amqp->basicAck($message->delivery_info['delivery_tag']);
                        $query = Order::find()
                            ->leftJoin(OrderProduct::tableName(), OrderProduct::tableName() . '.order_id = ' . Order::tableName() . '.id')
                            ->where([Order::tableName() . '.id' => (int)$message->body])
                            ->andWhere([Order::tableName() . '.last_queue_id' => $queue->id])
                            ->andWhere([
                                'in',
                                Order::tableName() . '.country_id',
                                ArrayHelper::getColumn(yii::$app->user->getCountries(), 'id')
                            ])->andWhere([
                                'or',
                                ['is', Order::tableName() . '.blocked_to', null],
                                ['<=', Order::tableName() . '.blocked_to', $this->current_time]
                            ])
                            ->andWhere(['not in', Order::tableName() . '.status', array_keys(Order::getFinalStatusesCollection())]);

                        if (!empty($product = Yii::$app->user->getUserProduct())) {
                            $query->andWhere(['in', OrderProduct::tableName() . '.product_id', $product]);
                        }

                        if ($query->exists()) {
                            $order = $query->one();
                            //на случай если при рендере формы этот заказ кто-то взял
                            /*$checkOrderInQueueOrder =  (new Query())
                                ->from('{{%queue_order}}')
                                ->where([
                                    'order_id' => $order->id,
                                ]);

                            if($checkOrderInQueueOrder->exists()){
                                continue;
                            }*/
                            //у данного ордера может быть бекап, созданный другим оператором.
                            //этот бекап нужно использовать для данного заказа, любым оператором
                            //при условии, что этот заказ не блокирован (т.е. с ним не работает оператор, для которого был создан бекап)

                            if (yii::$app->params['useOrderBackup'] === true) {
                                $order = OrderBackup::getBackupByOrder($order);
                            }

                            //сам метод useRequestByApiUrl - имеет проверку на незаконченную работу с заказом - но в ней рассматривается
                            //бекапы созданные данным пользователем
                            $data = $order->prepareForm();
                            // выйти из цикла
                            break;
                        }
                    }
                }

                $data['rabbit'] = 'not messages';
            } // Если ID заказа есть в таблице "queue_order" и время blocked_to меньше, чем текущее время - удалим из таблицы
            else {

                $queueOrder = $queueOrderQuery->one();

                if ($queueOrder['blocked_to'] <= $this->current_time) {
                    // Удалить из таблицы данный заказ и запросить новый
                    $this->removeOrderFromQueue($queueOrder['order_id']);

                    $data = [
                        'result' => 'searching',
                        'message' => Yii::t('common', 'Ожидайте пожалуйста: мы ищем для вас свежий заказ')
                    ];
                } // Если в сессии есть ID заказа
                else {
                    // заказ все еще заблокирован и редактируется
                    $orderQuery = Order::find()->where(['id' => $queueOrder['order_id']]);

                    if ($orderQuery->exists()) {
                        if (yii::$app->params['useOrderBackup'] === true) {
                            // запросить заказ, но таблицу "queue_order" не обновлять
                            $order = OrderBackup::getBackupByOrder($orderQuery->one());
                        } else {
                            $order = $orderQuery->one();
                        }
                        $data = $order->prepareForm($isExists = true);
                    } else
                        $data = [];
                }
            }

            $res->data = $data;


            if (isset($data['order'])) {
                $order = Order::findOne(['id' => $data['order']]);

                OrderProcessingTime::saveData([
                    'order_id' => $order->id,
                    'status' => $order->status
                ]);

                Asterisk::setUserPaused(1);
                AutoCall::deleteAll(['order_id' => $data['order']]);
            }

            return $res;
        }
    }


    /**
     * @return array
     */
    public function actionGetSearchFormResult()
    {
        $response = [
            'status' => 'success',
            'content' => ''
        ];

        $searchModel = new SearchOrder();
        $dataProvider = $searchModel->search(Yii::$app->request->post());

        $response['content'] = $this->renderPartial('@common/modules/order/views/change/search/_search-content', [
            'dataProvider' => $dataProvider
        ]);

        return $response;
    }
}

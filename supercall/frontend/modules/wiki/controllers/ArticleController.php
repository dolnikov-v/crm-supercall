<?php
namespace frontend\modules\wiki\controllers;

use yii\filters\AccessControl;

/**
 * Class ArticleController
 * @package frontend\modules\wiki\controllers
 */
class ArticleController extends \backend\modules\wiki\controllers\ArticleController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionView($id)
    {
        return parent::actionView($id);
    }
}
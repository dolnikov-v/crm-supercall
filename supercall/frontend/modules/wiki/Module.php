<?php
namespace frontend\modules\wiki;

use frontend\components\base\Module as CommonModule;

/**
 * Class Module
 * @package frontend\modules\wiki
 */
class Module extends CommonModule
{
    const MODULE_WIKI = 'wiki';

    public $controllerNamespace = 'frontend\modules\wiki\controllers';

}

<?php
/** @var \backend\modules\wiki\models\WikiArticle $model */
?>
<?= Yii::$app->view->renderFile('@backend/modules/wiki/views/article/view.php', ['model' => $model]);?>
<?php
use console\components\db\Migration;

/**
 * Class m161214_215130_partner_form_attr_time
 */
class m161214_215130_partner_form_attr_time extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_form_attribute}}', 'created_at', $this->integer());
        $this->addColumn('{{%partner_form_attribute}}', 'updated_at', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_form_attribute}}', 'created_at');
        $this->dropColumn('{{%partner_form_attribute}}', 'updated_at');
    }
}

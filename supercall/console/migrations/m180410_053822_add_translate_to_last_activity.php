<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180410_053822_add_translate_to_last_activity
 */
class m180410_053822_add_translate_to_last_activity extends Migration
{
    public $phrases = [
        'Последняя активность' => 'Last activity',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

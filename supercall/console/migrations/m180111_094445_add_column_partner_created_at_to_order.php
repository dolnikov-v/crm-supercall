<?php
use console\components\db\Migration;
use common\modules\order\models\Order;

/**
 * Class m180111_094445_add_column_partner_created_at_to_order
 */
class m180111_094445_add_column_partner_created_at_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'partner_created_at', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'partner_created_at');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170331_132526_correct_cost
 */
class m170331_132526_correct_cost extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //Пересчитываем стиомость касячных заказов
        $query = (new \yii\db\Query())->select('op.id, op.quantity, op.price')
            ->from('{{%order_product}} op')
            ->innerJoin('{{%order}} o', 'o.id = op.order_id')
            ->where('o.status=4 AND op.cost IS NULL');

        foreach ($query->batch(10) as $ops) {
            foreach ($ops as $op) {
                $cost =  $this->calcPrice($op['price'], $op['quantity']);
                $this->update('{{%order_product}}', [
                    'cost' => $cost,
                ], 'id=' . $op['id']);
            }
        }
    }

    protected function calcPrice($price, $quantity)
    {
        switch ((int)$quantity) {
            case 1:
                $priceAll = 1*$price;
                break;
            case 2:
                $priceAll = 2*$price;
                break;
            case 3:
                $priceAll = 2*$price;
                break;
            case 4:
                $priceAll = 4*$price;
                break;
            case 5:
                $priceAll = 3*$price;
                break;
            case 6:
                $priceAll = 6*$price;
                break;
            case 7:
                $priceAll = 4*$price;
                break;
            case 8:
                $priceAll = 8*$price;
                break;
            case 9:
                $priceAll = 5*$price;
                break;
            case 10:
                $priceAll = 10*$price;
                break;
            default:
                $priceAll = $price*$quantity;
                break;
        }

        return $priceAll;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return true;
    }
}

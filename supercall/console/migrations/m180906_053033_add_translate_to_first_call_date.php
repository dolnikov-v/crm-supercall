<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180906_053033_add_translate_to_first_call_date
 */
class m180906_053033_add_translate_to_first_call_date extends Migration
{

    public $phrases = [
        'Время до первого звонка' => 'Time to the first call'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

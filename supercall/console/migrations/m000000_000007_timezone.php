<?php
use common\models\Timezone;
use console\components\db\Migration;

/**
 * Class m000000_000007_timezone
 */
class m000000_000007_timezone extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%timezone}}', [
            'id' => $this->primaryKey(),
            'time_offset' => $this->integer(11)->notNull(),
            'timezone_id' => $this->string(100)->defaultValue(null),
            'name' => $this->string(512)->notNull(),
        ]);

        foreach ($this->fixtures() as $timezone) {
            $tz = new Timezone($timezone);
            $tz->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%timezone}}');
    }

    /**
     * @return array
     */
    private function fixtures()
    {
        return [
            [
                'time_offset' => -39600,
                'name' => '(GMT -11:00) Тихий океан/Мидуэй',
                'timezone_id' => 'Pacific/Midway'
            ],
            [
                'time_offset' => -39600,
                'name' => '(GMT -11:00) Тихий океан/Ниуэ',
                'timezone_id' => 'Pacific/Niue'
            ],
            [
                'time_offset' => -39600,
                'name' => '(GMT -11:00) Тихий океан/Паго-Паго',
                'timezone_id' => 'Pacific/Pago_Pago'
            ],
            [
                'time_offset' => -36000,
                'name' => '(GMT -10:00) Тихий океан/Гавайское время',
                'timezone_id' => 'Pacific/Honolulu'
            ],
            [
                'time_offset' => -36000,
                'name' => '(GMT -10:00) Тихий океан/Джонстон (атолл)',
                'timezone_id' => 'Pacific/Johnston'
            ],
            [
                'time_offset' => -36000,
                'name' => '(GMT -10:00) Тихий океан/Раротонга',
                'timezone_id' => 'Pacific/Rarotonga'
            ],
            [
                'time_offset' => -36000,
                'name' => '(GMT -10:00) Тихий океан/Таити',
                'timezone_id' => 'Pacific/Tahiti'
            ],
            [
                'time_offset' => -34200,
                'name' => '(GMT -09:30) Тихий океан/Маркизские острова',
                'timezone_id' => 'Pacific/Marquesas'
            ],
            [
                'time_offset' => -32400,
                'name' => '(GMT -09:00) Америка/Время Аляски',
                'timezone_id' => 'America/Anchorage'
            ],
            [
                'time_offset' => -32400,
                'name' => '(GMT -09:00) Тихий океан/Гамбир',
                'timezone_id' => 'Pacific/Gambier'
            ],
            [
                'time_offset' => -28800,
                'name' => '(GMT -08:00) Америка/Тихоокеанское время',
                'timezone_id' => 'America/Los_Angeles'
            ],
            [
                'time_offset' => -28800,
                'name' => '(GMT -08:00) Америка/Тихоокеанское время – Тихуана',
                'timezone_id' => 'America/Tijuana'
            ],
            [
                'time_offset' => -28800,
                'name' => '(GMT -08:00) Америка/Тихоокеанское время – Ванкувер',
                'timezone_id' => 'America/Vancouver'
            ],
            [
                'time_offset' => -28800,
                'name' => '(GMT -08:00) Америка/Тихоокеанское время – Уайтхорс',
                'timezone_id' => 'America/Whitehorse'
            ],
            [
                'time_offset' => -28800,
                'name' => '(GMT -08:00) Тихий океан/Питкэрн',
                'timezone_id' => 'Pacific/Pitcairn'
            ],
            [
                'time_offset' => -25200,
                'name' => '(GMT -07:00) Америка/Горное время – Доусон Крик',
                'timezone_id' => 'America/Dawson_Creek'
            ],
            [
                'time_offset' => -25200,
                'name' => '(GMT -07:00) Америка/Горное время – Денвер',
                'timezone_id' => 'America/Denver'
            ],
            [
                'time_offset' => -25200,
                'name' => '(GMT -07:00) Америка/Горное время – Эдмонтон',
                'timezone_id' => 'America/Edmonton'
            ],
            [
                'time_offset' => -25200,
                'name' => '(GMT -07:00) Америка/Горное время – Эрмосильо',
                'timezone_id' => 'America/Hermosillo'
            ],
            [
                'time_offset' => -25200,
                'name' => '(GMT -07:00) Америка/Горное время – Чиуауа, Мазатлан',
                'timezone_id' => 'America/Mazatlan'
            ],
            [
                'time_offset' => -25200,
                'name' => '(GMT -07:00) Америка/Горное время – Аризона',
                'timezone_id' => 'America/Phoenix'
            ],
            [
                'time_offset' => -25200,
                'name' => '(GMT -07:00) Америка/Горное время – Йеллоунайф',
                'timezone_id' => 'America/Yellowknife'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Америка/Белиз',
                'timezone_id' => 'America/Belize'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Америка/Центральное время',
                'timezone_id' => 'America/Chicago'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Америка/Коста-Рика',
                'timezone_id' => 'America/Costa_Rica'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Америка/Сальвадор',
                'timezone_id' => 'America/El_Salvador'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Америка/Гватемала',
                'timezone_id' => 'America/Guatemala'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Америка/Манагуа',
                'timezone_id' => 'America/Managua'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Америка/Центральное время – Мехико',
                'timezone_id' => 'America/Mexico_City'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Америка/Центральное время – Реджайна',
                'timezone_id' => 'America/Regina'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Америка/Центральное время (Америка/Тегусигальпа)',
                'timezone_id' => 'America/Tegucigalpa'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Америка/Центральное время – Виннипег',
                'timezone_id' => 'America/Winnipeg'
            ],
            [
                'time_offset' => -21600,
                'name' => '(GMT -06:00) Тихий океан/Галапагос',
                'timezone_id' => 'Pacific/Galapagos'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Богота',
                'timezone_id' => 'America/Bogota'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Каймановы острова',
                'timezone_id' => 'America/Cayman'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Гуаякиль',
                'timezone_id' => 'America/Guayaquil'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Гавана',
                'timezone_id' => 'America/Havana'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Восточное время – Икалуит',
                'timezone_id' => 'America/Iqaluit'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Ямайка',
                'timezone_id' => 'America/Jamaica'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Лима',
                'timezone_id' => 'America/Lima'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Восточное время – Монреаль',
                'timezone_id' => 'America/Montreal'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Нассау',
                'timezone_id' => 'America/Nassau'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Восточное время',
                'timezone_id' => 'America/New_York'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Панама',
                'timezone_id' => 'America/Panama'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Порт-о-Пренс',
                'timezone_id' => 'America/Port-au-Prince'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Риу-Бранку',
                'timezone_id' => 'America/Rio_Branco'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -05:00) Америка/Восточное время – Торонто',
                'timezone_id' => 'America/Toronto'
            ],
            [
                'time_offset' => -18000,
                'name' => '(GMT -06:00) Тихий океан/Остров Пасхи',
                'timezone_id' => 'Pacific/Easter'
            ],
            [
                'time_offset' => -16200,
                'name' => '(GMT -04:30) Америка/Каракас',
                'timezone_id' => 'America/Caracas'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Ангилья',
                'timezone_id' => 'America/Anguilla'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Антигуа',
                'timezone_id' => 'America/Antigua'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Аруба',
                'timezone_id' => 'America/Aruba'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Барбадос',
                'timezone_id' => 'America/Barbados'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Боа-Виста',
                'timezone_id' => 'America/Boa_Vista'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Кюрасао',
                'timezone_id' => 'America/Curacao'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Доминика',
                'timezone_id' => 'America/Dominica'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Гранд Турк',
                'timezone_id' => 'America/Grand_Turk'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Гренада',
                'timezone_id' => 'America/Grenada'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Гваделупа',
                'timezone_id' => 'America/Guadeloupe'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Гайана',
                'timezone_id' => 'America/Guyana'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Атлантическое время – Галифакс',
                'timezone_id' => 'America/Halifax'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Ла-Пас',
                'timezone_id' => 'America/La_Paz'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Манаус',
                'timezone_id' => 'America/Manaus'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Мартиника',
                'timezone_id' => 'America/Martinique'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Монсеррат',
                'timezone_id' => 'America/Montserrat'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Порт-оф-Спейн',
                'timezone_id' => 'America/Port_of_Spain'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Порто-Велью',
                'timezone_id' => 'America/Porto_Velho'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Пуэрто-Рико',
                'timezone_id' => 'America/Puerto_Rico'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Санто-Доминго',
                'timezone_id' => 'America/Santo_Domingo'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Сент-Китс',
                'timezone_id' => 'America/St_Kitts'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Сент-Люсия',
                'timezone_id' => 'America/St_Lucia'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Сент-Томас',
                'timezone_id' => 'America/St_Thomas'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Сент-Винсент',
                'timezone_id' => 'America/St_Vincent'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Тули',
                'timezone_id' => 'America/Thule'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Америка/Тортола',
                'timezone_id' => 'America/Tortola'
            ],
            [
                'time_offset' => -14400,
                'name' => '(GMT -04:00) Атлантика/Бермуды',
                'timezone_id' => 'Atlantic/Bermuda'
            ],
            [
                'time_offset' => -12600,
                'name' => '(GMT -03:30) Америка/Ньюфаундлендское время – Сент-Джонс',
                'timezone_id' => 'America/St_Johns'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Арагуайна',
                'timezone_id' => 'America/Araguaina'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Буэнос-Айрес',
                'timezone_id' => 'America/Argentina/Buenos_Aires'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Асунсьон',
                'timezone_id' => 'America/Asuncion'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Сальвадор',
                'timezone_id' => 'America/Bahia'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Белен',
                'timezone_id' => 'America/Belem'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Кампу-Гранди',
                'timezone_id' => 'America/Campo_Grande'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Кайенна',
                'timezone_id' => 'America/Cayenne'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Куяба',
                'timezone_id' => 'America/Cuiaba'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Форталеза',
                'timezone_id' => 'America/Fortaleza'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Годхоб',
                'timezone_id' => 'America/Godthab'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Масейо',
                'timezone_id' => 'America/Maceio'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Микелон',
                'timezone_id' => 'America/Miquelon'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Парамарибо',
                'timezone_id' => 'America/Paramaribo'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Ресифи',
                'timezone_id' => 'America/Recife'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Америка/Сантьяго',
                'timezone_id' => 'America/Santiago'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Антарктида/Палмер',
                'timezone_id' => 'Antarctica/Palmer'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Антарктида/Ротера',
                'timezone_id' => 'Antarctica/Rothera'
            ],
            [
                'time_offset' => -10800,
                'name' => '(GMT -03:00) Атлантика/Стэнли',
                'timezone_id' => 'Atlantic/Stanley'
            ],
            [
                'time_offset' => -7200,
                'name' => '(GMT -03:00) Америка/Монтевидео',
                'timezone_id' => 'America/Montevideo'
            ],
            [
                'time_offset' => -7200,
                'name' => '(GMT -02:00) Америка/Норонха',
                'timezone_id' => 'America/Noronha'
            ],
            [
                'time_offset' => -7200,
                'name' => '(GMT -03:00) Америка/Сан-Пауло',
                'timezone_id' => 'America/Sao_Paulo'
            ],
            [
                'time_offset' => -7200,
                'name' => '(GMT -02:00) Атлантика/Южная Георгия',
                'timezone_id' => 'Atlantic/South_Georgia'
            ],
            [
                'time_offset' => -3600,
                'name' => '(GMT -01:00) Америка/Скорсби',
                'timezone_id' => 'America/Scoresbysund'
            ],
            [
                'time_offset' => -3600,
                'name' => '(GMT -01:00) Атлантика/Азорские острова',
                'timezone_id' => 'Atlantic/Azores'
            ],
            [
                'time_offset' => -3600,
                'name' => '(GMT -01:00) Атлантика/Острова Зеленого мыса',
                'timezone_id' => 'Atlantic/Cape_Verde'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Абиджан',
                'timezone_id' => 'Africa/Abidjan'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Аккра',
                'timezone_id' => 'Africa/Accra'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Бамако',
                'timezone_id' => 'Africa/Bamako'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Банжул',
                'timezone_id' => 'Africa/Banjul'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Бисау',
                'timezone_id' => 'Africa/Bissau'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Касабланка',
                'timezone_id' => 'Africa/Casablanca'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Конакри',
                'timezone_id' => 'Africa/Conakry'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Дакар',
                'timezone_id' => 'Africa/Dakar'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Эль-Аюн',
                'timezone_id' => 'Africa/El_Aaiun'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Фритаун',
                'timezone_id' => 'Africa/Freetown'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Ломе',
                'timezone_id' => 'Africa/Lome'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Монровия',
                'timezone_id' => 'Africa/Monrovia'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Нуакшот',
                'timezone_id' => 'Africa/Nouakchott'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Уагадугу',
                'timezone_id' => 'Africa/Ouagadougou'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Африка/Сан-Томе',
                'timezone_id' => 'Africa/Sao_Tome'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Америка/Данмаркшавн',
                'timezone_id' => 'America/Danmarkshavn'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Атлантика/Канарские острова',
                'timezone_id' => 'Atlantic/Canary'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Атлантика/Фарерские острова',
                'timezone_id' => 'Atlantic/Faroe'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Атлантика/Рейкьявик',
                'timezone_id' => 'Atlantic/Reykjavik'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Атлантика/Остров Святой Елены',
                'timezone_id' => 'Atlantic/St_Helena'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Время по Гринвичу (без перехода на летнее время)',
                'timezone_id' => 'Etc/GMT'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Европа/Дублин',
                'timezone_id' => 'Europe/Dublin'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Европа/Лиссабон',
                'timezone_id' => 'Europe/Lisbon'
            ],
            [
                'time_offset' => 0,
                'name' => '(GMT +00:00) Европа/Лондон',
                'timezone_id' => 'Europe/London'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Алжир',
                'timezone_id' => 'Africa/Algiers'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Банги',
                'timezone_id' => 'Africa/Bangui'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Браззавиль',
                'timezone_id' => 'Africa/Brazzaville'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Сеута',
                'timezone_id' => 'Africa/Ceuta'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Дуала',
                'timezone_id' => 'Africa/Douala'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Киншаса',
                'timezone_id' => 'Africa/Kinshasa'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Лагос',
                'timezone_id' => 'Africa/Lagos'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Либревиль',
                'timezone_id' => 'Africa/Libreville'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Луанда',
                'timezone_id' => 'Africa/Luanda'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Малабо',
                'timezone_id' => 'Africa/Malabo'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Нджамена',
                'timezone_id' => 'Africa/Ndjamena'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Ниамей',
                'timezone_id' => 'Africa/Niamey'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Порто-Ново',
                'timezone_id' => 'Africa/Porto-Novo'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Африка/Тунис',
                'timezone_id' => 'Africa/Tunis'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Амстердам',
                'timezone_id' => 'Europe/Amsterdam'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Андорра',
                'timezone_id' => 'Europe/Andorra'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Центральноевропейское время (Европа/Белград)',
                'timezone_id' => 'Europe/Belgrade'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Берлин',
                'timezone_id' => 'Europe/Berlin'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Брюссель',
                'timezone_id' => 'Europe/Brussels'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Будапешт',
                'timezone_id' => 'Europe/Budapest'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Копенгаген',
                'timezone_id' => 'Europe/Copenhagen'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Гибралтар',
                'timezone_id' => 'Europe/Gibraltar'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Люксембург',
                'timezone_id' => 'Europe/Luxembourg'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Мадрид',
                'timezone_id' => 'Europe/Madrid'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Мальта',
                'timezone_id' => 'Europe/Malta'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Монако',
                'timezone_id' => 'Europe/Monaco'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Осло',
                'timezone_id' => 'Europe/Oslo'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Париж',
                'timezone_id' => 'Europe/Paris'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Центральноевропейское время (Европа/Прага)',
                'timezone_id' => 'Europe/Prague'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Рим',
                'timezone_id' => 'Europe/Rome'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Стокгольм',
                'timezone_id' => 'Europe/Stockholm'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Тирана',
                'timezone_id' => 'Europe/Tirane'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Вадуц',
                'timezone_id' => 'Europe/Vaduz'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Вена',
                'timezone_id' => 'Europe/Vienna'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Варшава',
                'timezone_id' => 'Europe/Warsaw'
            ],
            [
                'time_offset' => 3600,
                'name' => '(GMT +01:00) Европа/Цюрих',
                'timezone_id' => 'Europe/Zurich'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Блантайр',
                'timezone_id' => 'Africa/Blantyre'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Бужумбура',
                'timezone_id' => 'Africa/Bujumbura'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Каир',
                'timezone_id' => 'Africa/Cairo'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Габороне',
                'timezone_id' => 'Africa/Gaborone'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Хараре',
                'timezone_id' => 'Africa/Harare'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Йоханнесбург',
                'timezone_id' => 'Africa/Johannesburg'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Кигали',
                'timezone_id' => 'Africa/Kigali'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Лубумбаши',
                'timezone_id' => 'Africa/Lubumbashi'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Лусака',
                'timezone_id' => 'Africa/Lusaka'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Мапуту',
                'timezone_id' => 'Africa/Maputo'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Масеру',
                'timezone_id' => 'Africa/Maseru'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Мбабане',
                'timezone_id' => 'Africa/Mbabane'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Триполи',
                'timezone_id' => 'Africa/Tripoli'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Африка/Виндхук',
                'timezone_id' => 'Africa/Windhoek'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Азия/Амман',
                'timezone_id' => 'Asia/Amman'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Азия/Бейрут',
                'timezone_id' => 'Asia/Beirut'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Азия/Дамаск',
                'timezone_id' => 'Asia/Damascus'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Азия/Газа',
                'timezone_id' => 'Asia/Gaza'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Азия/Тель-Авив',
                'timezone_id' => 'Asia/Jerusalem'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Азия/Никосия (Азия/Nicosia)',
                'timezone_id' => 'Asia/Nicosia'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/Афины',
                'timezone_id' => 'Europe/Athens'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/Бухарест',
                'timezone_id' => 'Europe/Bucharest'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/Кишинев',
                'timezone_id' => 'Europe/Chisinau'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/Хельсинки',
                'timezone_id' => 'Europe/Helsinki'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/Стамбул',
                'timezone_id' => 'Europe/Istanbul'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/Калининград',
                'timezone_id' => 'Europe/Kaliningrad'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/Киев',
                'timezone_id' => 'Europe/Kiev'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/Рига',
                'timezone_id' => 'Europe/Riga'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/София',
                'timezone_id' => 'Europe/Sofia'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/Таллинн',
                'timezone_id' => 'Europe/Tallinn'
            ],
            [
                'time_offset' => 7200,
                'name' => '(GMT +02:00) Европа/Вильнюс',
                'timezone_id' => 'Europe/Vilnius'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Африка/Аддис-Абеба',
                'timezone_id' => 'Africa/Addis_Ababa'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Африка/Асмера',
                'timezone_id' => 'Africa/Asmara'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Африка/Дар-эс-Салам',
                'timezone_id' => 'Africa/Dar_es_Salaam'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Африка/Джибути',
                'timezone_id' => 'Africa/Djibouti'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Африка/Кампала',
                'timezone_id' => 'Africa/Kampala'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Африка/Хартум',
                'timezone_id' => 'Africa/Khartoum'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Африка/Могадишо',
                'timezone_id' => 'Africa/Mogadishu'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Африка/Найроби',
                'timezone_id' => 'Africa/Nairobi'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Антарктида/Сиова',
                'timezone_id' => 'Antarctica/Syowa'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Азия/Аден',
                'timezone_id' => 'Asia/Aden'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Азия/Багдад',
                'timezone_id' => 'Asia/Baghdad'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Азия/Бахрейн',
                'timezone_id' => 'Asia/Bahrain'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Азия/Кувейт',
                'timezone_id' => 'Asia/Kuwait'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Азия/Катар',
                'timezone_id' => 'Asia/Qatar'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Азия/Эр-Рияд',
                'timezone_id' => 'Asia/Riyadh'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Европа/Минск',
                'timezone_id' => 'Europe/Minsk'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Европа/Москва',
                'timezone_id' => 'Europe/Moscow'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Индия/Антананариву',
                'timezone_id' => 'Indian/Antananarivo'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Индия/Коморские острова',
                'timezone_id' => 'Indian/Comoro'
            ],
            [
                'time_offset' => 10800,
                'name' => '(GMT +03:00) Индия/Майорка',
                'timezone_id' => 'Indian/Mayotte'
            ],
            [
                'time_offset' => 12600,
                'name' => '(GMT +03:30) Азия/Тегеран',
                'timezone_id' => 'Asia/Tehran'
            ],
            [
                'time_offset' => 14400,
                'name' => '(GMT +04:00) Азия/Баку',
                'timezone_id' => 'Asia/Baku'
            ],
            [
                'time_offset' => 14400,
                'name' => '(GMT +04:00) Азия/Дубай',
                'timezone_id' => 'Asia/Dubai'
            ],
            [
                'time_offset' => 14400,
                'name' => '(GMT +04:00) Азия/Мускат',
                'timezone_id' => 'Asia/Muscat'
            ],
            [
                'time_offset' => 14400,
                'name' => '(GMT +04:00) Азия/Тбилиси',
                'timezone_id' => 'Asia/Tbilisi'
            ],
            [
                'time_offset' => 14400,
                'name' => '(GMT +04:00) Азия/Ереван',
                'timezone_id' => 'Asia/Yerevan'
            ],
            [
                'time_offset' => 14400,
                'name' => '(GMT +04:00) Европа/Самара',
                'timezone_id' => 'Europe/Samara'
            ],
            [
                'time_offset' => 14400,
                'name' => '(GMT +04:00) Индия/Маэ',
                'timezone_id' => 'Indian/Mahe'
            ],
            [
                'time_offset' => 14400,
                'name' => '(GMT +04:00) Индия/Маврикий',
                'timezone_id' => 'Indian/Mauritius'
            ],
            [
                'time_offset' => 14400,
                'name' => '(GMT +04:00) Индия/Реюньон',
                'timezone_id' => 'Indian/Reunion'
            ],
            [
                'time_offset' => 16200,
                'name' => '(GMT +04:30) Азия/Кабул',
                'timezone_id' => 'Asia/Kabul'
            ],
            [
                'time_offset' => 18000,
                'name' => '(GMT +05:00) Антарктида/Моусон',
                'timezone_id' => 'Antarctica/Mawson'
            ],
            [
                'time_offset' => 18000,
                'name' => '(GMT +05:00) Азия/Актау',
                'timezone_id' => 'Asia/Aqtau'
            ],
            [
                'time_offset' => 18000,
                'name' => '(GMT +05:00) Азия/Актобе',
                'timezone_id' => 'Asia/Aqtobe'
            ],
            [
                'time_offset' => 18000,
                'name' => '(GMT +05:00) Азия/Ашгабат',
                'timezone_id' => 'Asia/Ashgabat'
            ],
            [
                'time_offset' => 18000,
                'name' => '(GMT +05:00) Азия/Душанбе',
                'timezone_id' => 'Asia/Dushanbe'
            ],
            [
                'time_offset' => 18000,
                'name' => '(GMT +05:00) Азия/Карачи',
                'timezone_id' => 'Asia/Karachi'
            ],
            [
                'time_offset' => 18000,
                'name' => '(GMT +05:00) Азия/Ташкент',
                'timezone_id' => 'Asia/Tashkent'
            ],
            [
                'time_offset' => 18000,
                'name' => '(GMT +05:00) Азия/Екатеринбург',
                'timezone_id' => 'Asia/Yekaterinburg'
            ],
            [
                'time_offset' => 18000,
                'name' => '(GMT +05:00) Индия/Кергелен',
                'timezone_id' => 'Indian/Kerguelen'
            ],
            [
                'time_offset' => 18000,
                'name' => '(GMT +05:00) Индия/Мальдивы',
                'timezone_id' => 'Indian/Maldives'
            ],
            [
                'time_offset' => 19800,
                'name' => '(GMT +05:30) Азия/Индийское время',
                'timezone_id' => 'Asia/Calcutta'
            ],
            [
                'time_offset' => 19800,
                'name' => '(GMT +05:30) Азия/Коломбо',
                'timezone_id' => 'Asia/Colombo'
            ],
            [
                'time_offset' => 20700,
                'name' => '(GMT +05:45) Азия/Катманду',
                'timezone_id' => 'Asia/Katmandu'
            ],
            [
                'time_offset' => 21600,
                'name' => '(GMT +06:00) Антарктида/Восток',
                'timezone_id' => 'Antarctica/Vostok'
            ],
            [
                'time_offset' => 21600,
                'name' => '(GMT +06:00) Азия/Алматы',
                'timezone_id' => 'Asia/Almaty'
            ],
            [
                'time_offset' => 21600,
                'name' => '(GMT +06:00) Азия/Бишкек',
                'timezone_id' => 'Asia/Bishkek'
            ],
            [
                'time_offset' => 21600,
                'name' => '(GMT +06:00) Азия/Дхака',
                'timezone_id' => 'Asia/Dhaka'
            ],
            [
                'time_offset' => 21600,
                'name' => '(GMT +06:00) Азия/Омск, Новосибирск',
                'timezone_id' => 'Asia/Omsk'
            ],
            [
                'time_offset' => 21600,
                'name' => '(GMT +06:00) Азия/Тхимпху',
                'timezone_id' => 'Asia/Thimphu'
            ],
            [
                'time_offset' => 21600,
                'name' => '(GMT +06:00) Индия/Чагос',
                'timezone_id' => 'Indian/Chagos'
            ],
            [
                'time_offset' => 23400,
                'name' => '(GMT +06:30) Азия/Рангун',
                'timezone_id' => 'Asia/Rangoon'
            ],
            [
                'time_offset' => 23400,
                'name' => '(GMT +06:30) Индия/Кокосовые острова',
                'timezone_id' => 'Indian/Cocos'
            ],
            [
                'time_offset' => 25200,
                'name' => '(GMT +07:00) Антарктида/Davis',
                'timezone_id' => 'Antarctica/Davis'
            ],
            [
                'time_offset' => 25200,
                'name' => '(GMT +07:00) Азия/Бангкок',
                'timezone_id' => 'Asia/Bangkok'
            ],
            [
                'time_offset' => 25200,
                'name' => '(GMT +07:00) Азия/Ховд',
                'timezone_id' => 'Asia/Hovd'
            ],
            [
                'time_offset' => 25200,
                'name' => '(GMT +07:00) Азия/Джакарта',
                'timezone_id' => 'Asia/Jakarta'
            ],
            [
                'time_offset' => 25200,
                'name' => '(GMT +07:00) Азия/Красноярск',
                'timezone_id' => 'Asia/Krasnoyarsk'
            ],
            [
                'time_offset' => 25200,
                'name' => '(GMT +07:00) Азия/Пномпень',
                'timezone_id' => 'Asia/Phnom_Penh'
            ],
            [
                'time_offset' => 25200,
                'name' => '(GMT +07:00) Азия/Ханой',
                'timezone_id' => 'Asia/Saigon'
            ],
            [
                'time_offset' => 25200,
                'name' => '(GMT +07:00) Азия/Вьентьян',
                'timezone_id' => 'Asia/Vientiane'
            ],
            [
                'time_offset' => 25200,
                'name' => '(GMT +07:00) Индия/Рождественские острова',
                'timezone_id' => 'Indian/Christmas'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Антарктида/Кейси',
                'timezone_id' => 'Antarctica/Casey'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Бруней',
                'timezone_id' => 'Asia/Brunei'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Чойбалсан',
                'timezone_id' => 'Asia/Choibalsan'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Гонконг',
                'timezone_id' => 'Asia/Hong_Kong'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Иркутск',
                'timezone_id' => 'Asia/Irkutsk'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Куала-Лумпур',
                'timezone_id' => 'Asia/Kuala_Lumpur'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Макау',
                'timezone_id' => 'Asia/Macau'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Макасар',
                'timezone_id' => 'Asia/Makassar'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Манила',
                'timezone_id' => 'Asia/Manila'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Китайское время – Пекин',
                'timezone_id' => 'Asia/Shanghai'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Сингапур',
                'timezone_id' => 'Asia/Singapore'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Тайбэй',
                'timezone_id' => 'Asia/Taipei'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Азия/Улан-Батор',
                'timezone_id' => 'Asia/Ulaanbaatar'
            ],
            [
                'time_offset' => 28800,
                'name' => '(GMT +08:00) Австралия/Западное время – Перт',
                'timezone_id' => 'Australia/Perth'
            ],
            [
                'time_offset' => 32400,
                'name' => '(GMT +09:00) Азия/Дили',
                'timezone_id' => 'Asia/Dili'
            ],
            [
                'time_offset' => 32400,
                'name' => '(GMT +09:00) Азия/Джапура',
                'timezone_id' => 'Asia/Jayapura'
            ],
            [
                'time_offset' => 32400,
                'name' => '(GMT +09:00) Азия/Пхеньян',
                'timezone_id' => 'Asia/Pyongyang'
            ],
            [
                'time_offset' => 32400,
                'name' => '(GMT +09:00) Азия/Сеул',
                'timezone_id' => 'Asia/Seoul'
            ],
            [
                'time_offset' => 32400,
                'name' => '(GMT +09:00) Азия/Токио',
                'timezone_id' => 'Asia/Tokyo'
            ],
            [
                'time_offset' => 32400,
                'name' => '(GMT +09:00) Азия/Якутск',
                'timezone_id' => 'Asia/Yakutsk'
            ],
            [
                'time_offset' => 32400,
                'name' => '(GMT +09:00) Тихий океан/Палау',
                'timezone_id' => 'Pacific/Palau'
            ],
            [
                'time_offset' => 34200,
                'name' => '(GMT +09:30) Австралия/Центральное время – Дарвин',
                'timezone_id' => 'Australia/Darwin'
            ],
            [
                'time_offset' => 36000,
                'name' => '(GMT +10:00) Антарктида/Дюмон-Дюрвиль',
                'timezone_id' => 'Antarctica/DumontDUrville'
            ],
            [
                'time_offset' => 36000,
                'name' => '(GMT +10:00) Азия/Магадан',
                'timezone_id' => 'Asia/Magadan'
            ],
            [
                'time_offset' => 36000,
                'name' => '(GMT +10:00) Азия/Владивосток, Южно-Сахалинск',
                'timezone_id' => 'Asia/Vladivostok'
            ],
            [
                'time_offset' => 36000,
                'name' => '(GMT +10:00) Австралия/Восточное время – Брисбен',
                'timezone_id' => 'Australia/Brisbane'
            ],
            [
                'time_offset' => 36000,
                'name' => '(GMT +10:00) Тихий океан/Гуам',
                'timezone_id' => 'Pacific/Guam'
            ],
            [
                'time_offset' => 36000,
                'name' => '(GMT +10:00) Тихий океан/Порт-Морсби',
                'timezone_id' => 'Pacific/Port_Moresby'
            ],
            [
                'time_offset' => 36000,
                'name' => '(GMT +10:00) Тихий океан/Сайпан',
                'timezone_id' => 'Pacific/Saipan'
            ],
            [
                'time_offset' => 36000,
                'name' => '(GMT +10:00) Тихий океан/Трук',
                'timezone_id' => 'Pacific/Truk'
            ],
            [
                'time_offset' => 37800,
                'name' => '(GMT +10:30) Австралия/Центральное время – Аделаида',
                'timezone_id' => 'Australia/Adelaide'
            ],
            [
                'time_offset' => 39600,
                'name' => '(GMT +11:00) Австралия/Восточное время – Хобарт',
                'timezone_id' => 'Australia/Hobart'
            ],
            [
                'time_offset' => 39600,
                'name' => '(GMT +11:00) Австралия/Восточное время – Мельбурн, Сидней',
                'timezone_id' => 'Australia/Sydney'
            ],
            [
                'time_offset' => 39600,
                'name' => '(GMT +11:00) Тихий океан/Эфате',
                'timezone_id' => 'Pacific/Efate'
            ],
            [
                'time_offset' => 39600,
                'name' => '(GMT +11:00) Тихий океан/Гвадалканал',
                'timezone_id' => 'Pacific/Guadalcanal'
            ],
            [
                'time_offset' => 39600,
                'name' => '(GMT +11:00) Тихий океан/Кусаие',
                'timezone_id' => 'Pacific/Kosrae'
            ],
            [
                'time_offset' => 39600,
                'name' => '(GMT +11:00) Тихий океан/Нумеа',
                'timezone_id' => 'Pacific/Noumea'
            ],
            [
                'time_offset' => 39600,
                'name' => '(GMT +11:00) Тихий океан/Понапе',
                'timezone_id' => 'Pacific/Ponape'
            ],
            [
                'time_offset' => 41400,
                'name' => '(GMT +11:30) Тихий океан/Норфолк',
                'timezone_id' => 'Pacific/Norfolk'
            ],
            [
                'time_offset' => 43200,
                'name' => '(GMT +12:00) Азия/Петропавловск-Камчатский',
                'timezone_id' => 'Asia/Kamchatka'
            ],
            [
                'time_offset' => 43200,
                'name' => '(GMT +12:00) Тихий океан/Фунафути',
                'timezone_id' => 'Pacific/Funafuti'
            ],
            [
                'time_offset' => 43200,
                'name' => '(GMT +12:00) Тихий океан/Кваджелейн',
                'timezone_id' => 'Pacific/Kwajalein'
            ],
            [
                'time_offset' => 43200,
                'name' => '(GMT +12:00) Тихий океан/Маджуро',
                'timezone_id' => 'Pacific/Majuro'
            ],
            [
                'time_offset' => 43200,
                'name' => '(GMT +12:00) Тихий океан/Науру',
                'timezone_id' => 'Pacific/Nauru'
            ],
            [
                'time_offset' => 43200,
                'name' => '(GMT +12:00) Тихий океан/Тарава',
                'timezone_id' => 'Pacific/Tarawa'
            ],
            [
                'time_offset' => 43200,
                'name' => '(GMT +12:00) Тихий океан/Остров Вэйк',
                'timezone_id' => 'Pacific/Wake'
            ],
            [
                'time_offset' => 43200,
                'name' => '(GMT +12:00) Тихий океан/Уоллис',
                'timezone_id' => 'Pacific/Wallis'
            ],
            [
                'time_offset' => 46800,
                'name' => '(GMT +13:00) Тихий океан/Оклэнд',
                'timezone_id' => 'Pacific/Auckland'
            ],
            [
                'time_offset' => 46800,
                'name' => '(GMT +13:00) Тихий океан/Острова Эндербери',
                'timezone_id' => 'Pacific/Enderbury'
            ],
            [
                'time_offset' => 46800,
                'name' => '(GMT +13:00) Тихий океан/Факаофо',
                'timezone_id' => 'Pacific/Fakaofo'
            ],
            [
                'time_offset' => 46800,
                'name' => '(GMT +13:00) Тихий океан/Фиджи',
                'timezone_id' => 'Pacific/Fiji'
            ],
            [
                'time_offset' => 46800,
                'name' => '(GMT +13:00) Тихий океан/Тонгатапу',
                'timezone_id' => 'Pacific/Tongatapu'
            ],
            [
                'time_offset' => 50400,
                'name' => '(GMT +14:00) Тихий океан/Апия',
                'timezone_id' => 'Pacific/Apia'
            ],
            [
                'time_offset' => 50400,
                'name' => '(GMT +14:00) Тихий океан/Киритимати',
                'timezone_id' => 'Pacific/Kiritimati'
            ]
        ];
    }
}

<?php
use console\components\db\Migration;
use common\modules\order\models\Order;

/**
 * Class m170929_081544_add_column_google_zip_to_order
 */
class m170929_081544_add_column_google_zip_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'google_zip', $this->string()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'google_zip');
    }
}

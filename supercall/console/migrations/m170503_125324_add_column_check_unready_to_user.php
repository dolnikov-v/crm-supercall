<?php
use console\components\db\Migration;

/**
 * Class m170503_125324_add_column_check_unready_to_user
 */
class m170503_125324_add_column_check_unready_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'check_unready', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'check_unready');
    }
}

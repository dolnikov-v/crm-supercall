<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180824_090603_add_translate_link_download
 */
class m180824_090603_add_translate_link_download extends Migration
{
    public $phrases = [
        'Скачать' => 'Download'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

    }
}

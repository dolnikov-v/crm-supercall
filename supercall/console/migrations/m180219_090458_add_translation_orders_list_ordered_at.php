<?php
    
    use console\components\db\Migration;
    use backend\models\i18n_source_message;
    
    /**
     * Class m180219_090458_add_translation_orders_list_ordered_at
     */
    class m180219_090458_add_translation_orders_list_ordered_at extends Migration
    {
        public $phrases = [
            'Дата создания в партнера' => 'Date of creation in partner',
        ];
        
        /**
         * @inheritdoc
         */
        public function safeUp()
        {
            i18n_source_message::addTranslate($this->phrases, true);
        }
        
        /**
         * @inheritdoc
         */
        public function safeDown()
        {
            i18n_source_message::removeTranslate($this->phrases);
        }
    }

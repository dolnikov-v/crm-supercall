<?php
use console\components\db\Migration;
use common\models\Product;

/**
 * Class m171114_024645_add_column_active_to
 */
class m171114_024645_add_column_active_to extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Product::tableName(), 'active', $this->boolean()->defaultValue(true));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Product::tableName(), 'active');
    }
}

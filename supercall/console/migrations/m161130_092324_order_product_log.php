<?php
use console\components\db\Migration;

/**
 * Class m161130_092324_order_product_log
 */
class m161130_092324_order_product_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_product_log}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'action' => $this->string()->notNull(),
            'price' => $this->double()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, '{{%order_product_log}}', 'order_id', 'order', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%order_product_log}}', 'product_id', 'product', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, '{{%order_product_log}}', 'action');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_product_log}}');
    }
}

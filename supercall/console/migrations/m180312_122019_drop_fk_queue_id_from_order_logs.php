<?php
use console\components\db\Migration;

/**
 * Class m180312_122019_drop_fk_queue_id_from_order_logs
 */
class m180312_122019_drop_fk_queue_id_from_order_logs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-order_logs-queue_id', '{{%order_logs}}');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addForeignKey('fk-order_logs-queue_id', '{{%order_logs}}', 'queue_id', '{{%queue}}', 'id', self::RESTRICT, self::RESTRICT);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m161207_194137_partner_form
 */
class m161207_194137_partner_form extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_form}}', [
            'id' => $this->primaryKey(),
            'partner_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'active' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addForeignKey(null, '{{%partner_form}}', 'partner_id', '{{%partner}}', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_form}}');
    }
}

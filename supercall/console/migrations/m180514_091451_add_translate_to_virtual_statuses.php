<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180514_091451_add_translate_to_virtual_statuses
 */
class m180514_091451_add_translate_to_virtual_statuses extends Migration
{
    public $phrases = [
        'Заказ доставлен' => 'Delivered',
        'Ожидает заказ' => 'Waiting for order',
        'Клиент отказался' => 'Reject'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

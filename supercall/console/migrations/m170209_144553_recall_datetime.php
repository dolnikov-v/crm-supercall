<?php
use console\components\db\Migration;

/**
 * Class m170209_144553_recall_datetime
 */
class m170209_144553_recall_datetime extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('{{%order}}', 'recall_date', $this->dateTime());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('{{%order}}', 'recall_date', $this->date());
    }
}

<?php

use common\models\User;
use console\components\db\Migration;
use yii\helpers\ArrayHelper;

/**
 * Class m180911_061005_add_role_logistician
 */
class m180911_061005_add_role_logistician extends Migration
{
    /**
     * @inheritdoc
     */
    public $permissions = [
        "profile.control",
        "home.index.index",
        "order.index.index",
        "order.change.index",
        "order.index.view",
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = $this->authManager;
        $role = $auth->createRole(User::ROLE_LOGISTICIAN);
        $role->description = 'Логист';
        $auth->add($role);

        foreach ($this->permissions as $permission) {
            echo $permission.PHP_EOL;
            if (!in_array($permission, ArrayHelper::getColumn($auth->getPermissionsByRole(User::ROLE_LOGISTICIAN), 'name'))) {
                $this->insert($this->authManager->itemChildTable, [
                    'parent' => User::ROLE_LOGISTICIAN,
                    'child' => $permission
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_LOGISTICIAN]);
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }

        $auth = $this->authManager;
        $role = $auth->getRole(User::ROLE_LOGISTICIAN);
        $auth->remove($role);
    }
}

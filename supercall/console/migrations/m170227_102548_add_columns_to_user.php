<?php
use console\components\db\Migration;

/**
 * Class m170227_102548_add_columns_to_user
 */
class m170227_102548_add_columns_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%user_partner}}', [
            'user_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'active' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    
        $this->addPrimaryKey(null, '{{%user_partner}}', ['user_id', 'partner_id']);
        $this->addForeignKey(null, '{{%user_partner}}', 'user_id', '{{%user}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%user_partner}}', 'partner_id', '{{%partner}}', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_partner}}');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m000000_000002_auth_item_child
 */
class m000000_000002_auth_item_child extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%auth_item_child}}', [
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
            $this->includePrimaryKey(['parent', 'child']),
        ]);

        $this->addForeignKey(null, $this->authManager->itemChildTable, 'parent', $this->authManager->itemTable, 'name', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, $this->authManager->itemChildTable, 'child', $this->authManager->itemTable, 'name', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%auth_item_child}}');
    }
}

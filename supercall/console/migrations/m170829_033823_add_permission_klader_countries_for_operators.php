<?php
use console\components\db\Migration;
use common\models\User;


/**
 * Class m170829_033823_add_permission_klader_countries_for_operators
 */
class m170829_033823_add_permission_klader_countries_for_operators extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => 'catalog.kladercountries.getdata',
            'type' => '2',
            'description' => 'catalog.kladercountries.getdata',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_OPERATOR,
            'child' => 'catalog.kladercountries.getdata'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.kladercountries.getdata', 'parent' => User::ROLE_OPERATOR]);

        $this->delete('{{%auth_item}}', ['name' => 'catalog.kladercountries.getdata']);

    }
}

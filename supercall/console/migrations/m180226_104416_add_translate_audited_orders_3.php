<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180226_104416_add_translate_audited_orders_3
 */
class m180226_104416_add_translate_audited_orders_3 extends Migration
{
    public $phrases = [
        'Завершить аудит' => 'Complete audit',
        'Закрыть' => 'Close'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m180821_064244_add_column_uniqueid_to_task_queue_order
 */
class m180821_064244_add_column_uniqueid_to_task_queue_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%task_queue_order}}', 'uniqueid', $this->string()->after('order_id'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%task_queue_order}}', 'uniqueid');
    }
}

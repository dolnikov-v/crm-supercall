<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180226_083101_add_translate_to_audited_order_2
 */
class m180226_083101_add_translate_to_audited_order_2 extends Migration
{

    public $phrases = [
        'Результат проверки' => 'Result of checking'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m180316_135934_add_translate_to_auto_call_log
 */
class m180316_135934_add_translate_to_auto_call_log extends Migration
{
    public $phrases = [
        'Код очереди' => 'Queue code',
        'Код звонка'  => 'CDR code',
        'Сип логин'   => 'User Sip',
        'Логи автодозвона' => 'Autocall logs'
    ];
    
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }
    
    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

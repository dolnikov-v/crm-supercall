<?php

use common\modules\partner\models\Package;
use console\components\db\Migration;

/**
 * Class m180420_061615_delete_column_free_amount_on_package
 */
class m180420_061615_delete_column_free_amount_on_package extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(Package::tableName(), 'free_amount');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn(Package::tableName(), 'free_amount', $this->integer());
    }
}

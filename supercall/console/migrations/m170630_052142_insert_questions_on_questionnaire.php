<?php
use console\components\db\Migration;
use backend\modules\operator\models\QuestionnaireManagement;
use common\modules\order\models\Order;

/**
 * Class m170630_052142_insert_questions_on_questionnaire
 */
class m170630_052142_insert_questions_on_questionnaire extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete(QuestionnaireManagement::tableName());

        $mask = [
            'yes_no' => json_encode(['type'=>'options', 'mask' => 'yes_no',  'answers' => ['Да', 'Нет']]),
            'yes_no_na' => json_encode(['type'=>'options', 'mask' => 'yes_no_na',  'answers' => ['Да', 'Нет', 'не определено']]),
            '1_3' => json_encode(['type'=>'options', 'mask' => '1_3',  'answers' => [
                'Совершенно неприемлемо', 'Удовлетворительно', 'Отлично'
            ]]),
            'text' => json_encode(['type' => 'text', 'mask' => false])
        ];

        $questions = [
            [//2.1
                'question_text' => 'Did the agent apply a local greeting?',
                'answers' => $mask['yes_no'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//2.2
                'question_text' => 'Did the agent mention the company name?',
                'answers' => $mask['yes_no'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//2.3
                'question_text' => 'Did the agent mention his/ her name?',
                'answers' => $mask['yes_no'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//3.1
                'question_text' => "Did the agent ask for / confirm the customer's name?",
                'answers' => $mask['yes_no'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//3.2
                'question_text' => "Did the agent ask for / confirm the customer's address?",
                'answers' => $mask['yes_no'],
                'order_status' => json_encode([Order::STATUS_APPROVED]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//3.3
                'question_text' => 'Did the agent note in comments additional info from customer?',
                'answers' => $mask['yes_no'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//4.1
                'question_text' => " Full details of the call were obtained and understood by the agent",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//4.2
                'question_text' => "The agent asked all appropriate questions (followed script)",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//4.3
                'question_text' => "The agent answered customer questions correctly",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//4.4
                'question_text' => "The agent needs to improve",
                'answers' => $mask['text'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 0,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//5.1
                'question_text' => "Agent did not interrupt or talk over the customer",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//5.2
                'question_text' => "Agent avoided long silences during the call",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//5.3
                'question_text' => "The agent needs to improve",
                'answers' => $mask['text'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 0,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//6.1
                'question_text' => "Agent sounded friendly, polite and welcoming",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//6.2
                'question_text' => "Agent displayed a professional manner throughout the call",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//6.3
                'question_text' => "Agent used effective questioning skills",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//6.4
                'question_text' => "Agent demonstrated active listening",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//6.5
                'question_text' => "Agent sounded clear and confident throughout call",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//6.6
                'question_text' => "Agent refrained from using jargon throughout call",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//6.8
                'question_text' => "The agent needs to improve",
                'answers' => $mask['text'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 0,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//7.1
                'question_text' => "Agent dealt with all the issues raised",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//7.2
                'question_text' => "The customer was satisfied",
                'answers' => $mask['1_3'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//7.4
                'question_text' => "The agent needs to improve",
                'answers' => $mask['text'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 0,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//8.1
                'question_text' => "Correct order status",
                'answers' => $mask['yes_no'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//8.2
                'question_text' => "Correct address",
                'answers' => $mask['yes_no_na'],
                'order_status' => json_encode([Order::STATUS_APPROVED]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//8.3
                'question_text' => "Correct goods and quantity",
                'answers' => $mask['yes_no_na'],
                'order_status' => json_encode([Order::STATUS_APPROVED]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//8.4
                'question_text' => "Correct comment",
                'answers' => $mask['yes_no'],
                'order_status' => json_encode([Order::STATUS_APPROVED, Order::STATUS_REJECTED, Order::STATUS_TRASH]),
                'required' => 1,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//91
                'question_text' => "Any comments for the call",
                'answers' => $mask['text'],
                'order_status' => json_encode([Order::STATUS_APPROVED], Order::STATUS_REJECTED, Order::STATUS_TRASH),
                'required' => 0,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
            [//9.2
                'question_text' => "Any comments or suggestions for this form improvement based on the call",
                'answers' => $mask['text'],
                'order_status' => json_encode([Order::STATUS_APPROVED], Order::STATUS_REJECTED, Order::STATUS_TRASH),
                'required' => 0,
                'sort' => 0,
                'created_at' => time(),
                'updated_at' => time()
            ],
        ];

        foreach($questions as $q){
            $this->insert(QuestionnaireManagement::tableName(), $q);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(QuestionnaireManagement::tableName());
    }
}

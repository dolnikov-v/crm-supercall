<?php
use common\modules\partner\models\PartnerProduct;
use console\components\db\Migration;
use backend\modules\catalog\models\PriceShipping;

/**
 * Class m171214_032751_modify_table_price_shipping_on_task_WCALL87
 */
class m171214_032751_modify_table_price_shipping_on_task_WCALL87 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_price_shipping_partner_product_id', PriceShipping::tableName());
        $this->dropColumn(PriceShipping::tableName(), 'partner_product_id');
        $this->addColumn(PriceShipping::tableName(), 'param_shipping', 'jsonb');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn(PriceShipping::tableName(), 'partner_product_id', $this->integer()->notNull());
        $this->dropColumn(PriceShipping::tableName(), 'param_shipping');
        $this->addForeignKey(null, PriceShipping::tableName(), 'partner_product_id', PartnerProduct::tableName(), 'id', self::CASCADE, self::RESTRICT);

    }
}

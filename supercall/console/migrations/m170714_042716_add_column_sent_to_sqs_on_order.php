<?php
use console\components\db\Migration;
use common\modules\order\models\Order;
/**
 * Class m170714_042716_add_column_sent_to_sqs_on_order
 */
class m170714_042716_add_column_sent_to_sqs_on_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'sent_to_sqs', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'sent_to_sqs');
    }
}

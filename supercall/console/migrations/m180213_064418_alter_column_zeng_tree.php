<?php
use backend\modules\operator\models\ZingTree;
use console\components\db\Migration;

/**
 * Class m180213_064418_alter_column_zeng_tree
 */
class m180213_064418_alter_column_zeng_tree extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(ZingTree::tableName(), 'country_id');
        $this->dropColumn(ZingTree::tableName(), 'product_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn(ZingTree::tableName(), 'country_id', 'jsonb');
        $this->addColumn(ZingTree::tableName(), 'product_id', 'jsonb');
    }
}

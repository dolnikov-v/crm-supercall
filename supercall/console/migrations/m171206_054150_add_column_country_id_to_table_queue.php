<?php
use console\components\db\Migration;
use backend\modules\queue\models\Queue;

/**
 * Class m171206_054150_add_column_country_id_to_table_queue
 */
class m171206_054150_add_column_country_id_to_table_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Queue::tableName(), 'country_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Queue::tableName(), 'country_id');
    }
}

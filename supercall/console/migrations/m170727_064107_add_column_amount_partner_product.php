<?php
use console\components\db\Migration;
use common\modules\partner\models\PartnerProduct;

/**
 * Class m170727_064107_add_column_amount_partner_product
 */
class m170727_064107_add_column_amount_partner_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(PartnerProduct::tableName(), 'amount', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(PartnerProduct::tableName(), 'amount');
    }
}

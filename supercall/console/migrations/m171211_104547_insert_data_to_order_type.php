<?php
use console\components\db\Migration;
use common\modules\order\models\OrderType;

/**
 * Class m171211_104547_insert_data_to_order_type
 */
class m171211_104547_insert_data_to_order_type extends Migration
{
    public $statuses = [
        OrderType::TYPE_NEW_RETURN => 'Повторный обзвон',
        OrderType::TYPE_ORDER_RETURN => 'Очередь 200',
        OrderType::TYPE_ORDER_ANALYSIS_TRASH => 'Треш',
        OrderType::TYPE_ORDER_STILL_WAITING => 'Проверка спроса (после задержки)',
        OrderType::TYPE_RETURN_NO_PROD => 'Перепроверка товаров',
        OrderType::TYPE_CHECK_UNDELIVERY => 'Невыкуп'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->statuses as $type => $name) {
            $this->insert(OrderType::tableName(), ['name' => $name, 'created_at' => time(), 'updated_at' => time()]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        OrderType::deleteAll(['>', 'id', 5]);
    }
}

<?php
use console\components\db\Migration;
use yii\db\Query;
use backend\models\i18n_source_message;

/**
 * Class m171013_025651_translate_scripts_operator
 */
class m171013_025651_translate_scripts_operator extends Migration
{
    public $phrases = [
        'Скрипты для операторов' => 'Scripts for operators',
        'Список скриптов для операторов' => 'List of scripts for operators',
        'Таблица со скриптами операторов' => 'Table with operator scripts',
        'Активность' => 'Activity',
        'Партнёр' => 'Partner',
        'Продукт' => 'Product',
        'Создан пользователем' => 'Created by',
        'Обновлен пользователем' => 'Updated by',
        'Дата обновления' => 'Date of update',
        'Создать скрипт для оператора' => 'Create a script for the operator',
        'Активировать' => 'Activate',
        'Редактирование скрипта для оператор' => 'Editing a script for an operator',
        'Содержание' => 'Content',
        'Сохранить скрипт для оператора' => 'Save the script for the operator',
        'Добавить скрипт для оператора' => 'Add a script for the operator',
        'Скрипты оператора' => 'Operator scripts',
        'Скрипт не найден.' => 'The script was not found.',
        'Скрипт для оператора успешно отредактирован.' => 'The script for the operator has been successfully edited.',
        'Скрипт для оператора успешно добавлен.' => 'The script for the operator has been successfully added.',
        'Для данного товара и партнёра в указанной стране уже есть активный скрипт. Для активации текущего скрипта, необходимо, деактивировать его.' => 'For this product and partner in the specified country there is already an active script. To activate the current script, you must deactivate it.',
        'Скрипт для оператора успешно удалён.' => 'The script for the operator was successfully deleted.',
        'Скрипт для оператора успешно активирован.' => 'The script for the operator has been successfully activated.',
        'Скрипт для оператора успешно деактивирован.' => 'The script for the operator has been successfully deactivated.',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

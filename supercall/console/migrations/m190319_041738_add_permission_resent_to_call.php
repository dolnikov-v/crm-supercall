<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;


/**
 * Class m190319_041738_add_permission_resent_to_call
 */
class m190319_041738_add_permission_resent_to_call extends Migration
{

    public $phrases = [
        'Cтатус выставлен стратегией очереди' => 'Status set by queue strategy',
        'Заказ #{id} должен находиться в финальном статусе' => 'Order #{id} must be in final status',
        'Заказ #{id}. Не выбрана первичная очередь' => 'Order #{id}. No primary queue selected',
        'Заказ #{id}. Ошибка создания дубликата для отправки в очередь: {error}' => 'Order #{id}. Error creating duplicate to send to queue: {error}',
        'По заказу #{id} создан дубликат #{duplicate_id} и отправлен в первичную очередь' => 'Order # {id} was duplicated #{duplicate id} and sent to the primary queue',
        'В выбранной стране нет первичной очереди' => 'There is no primary queue in selected country',
        'Должна быть указана одна страна' => 'One country must be specified',
        'Отправить заказы на повторный обзвон' => 'Send orders for re-call',
        'Выбор очереди' => 'Queue selection',
        'Отправить повторно на обзвон' => 'Resend to call',
        'Внимание! При повторной отправки заказов на обзвон, данные заказы будут созданы в системе как новые, получат новые номера и будут отправлены в первичную очередь.' => 'Attention! When re-sending orders to call, these orders will be created in the system as new, will receive new numbers and will be sent to the primary queue.',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        $this->addPermission(['order.groupoperations.resendtocall' => 'Сменить стоимость товаров в заказе']);
        $this->addPermissionToRole('order.groupoperations.resendtocall', User::ROLE_ADMINISTRATOR);
        $this->addPermissionToRole('order.groupoperations.resendtocall', User::ROLE_CURATOR);
        $this->addPermissionToRole('order.groupoperations.resendtocall', User::ROLE_TEAM_LEAD);
        $this->addPermissionToRole('order.groupoperations.resendtocall', User::ROLE_AUDITOR);
        $this->addPermissionToRole('order.groupoperations.resendtocall', User::ROLE_SALES_DIRECTOR);
        $this->addPermissionToRole('order.groupoperations.resendtocall', User::ROLE_SUPERVISOR);
        $this->addPermissionToRole('order.groupoperations.resendtocall', User::ROLE_LOGISTICIAN);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->deletePermissionByRole('order.groupoperations.resendtocall', User::ROLE_ADMINISTRATOR);
        $this->deletePermissionByRole('order.groupoperations.resendtocall', User::ROLE_CURATOR);
        $this->deletePermissionByRole('order.groupoperations.resendtocall', User::ROLE_TEAM_LEAD);
        $this->deletePermissionByRole('order.groupoperations.resendtocall', User::ROLE_AUDITOR);
        $this->deletePermissionByRole('order.groupoperations.resendtocall', User::ROLE_SALES_DIRECTOR);
        $this->deletePermissionByRole('order.groupoperations.resendtocall', User::ROLE_SUPERVISOR);
        $this->deletePermissionByRole('order.groupoperations.resendtocall', User::ROLE_LOGISTICIAN);
        $this->deletePermission('order.groupoperations.resendtocall');
    }
}

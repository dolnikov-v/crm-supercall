<?php
use console\components\db\Migration;

/**
 * Class m170206_075032_tbl_queue_user
 */
class m170206_075032_tbl_queue_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%queue_user}}', [
            'id'         => $this->primaryKey(),
            'queue_id'   => $this->integer(),
            'user_id'    => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk-queue_user-queue_id', '{{%queue_user}}', 'queue_id', '{{%queue}}', 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey('fk-queue_user-user_id', '{{%queue_user}}', 'user_id', '{{%user}}', 'id', self::CASCADE, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-queue_user-user_id', '{{%queue_user}}');
        $this->dropForeignKey('fk-queue_user-queue_id', '{{%queue_user}}');

        $this->dropTable('{{%queue_user}}');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170216_181235_add_sort_to_partner_form_attr
 */
class m170216_181235_add_sort_to_partner_form_attr extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_form_attribute}}', 'sort', $this->integer()->defaultValue(10));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_form_attribute}}', 'sort');
    }
}

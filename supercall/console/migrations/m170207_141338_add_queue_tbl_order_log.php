<?php
use console\components\db\Migration;

/**
 * Class m170207_141338_add_queue_tbl_order_log
 */
class m170207_141338_add_queue_tbl_order_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_log}}', 'queue_id', $this->integer()->null());
        $this->addForeignKey('fk-order_log-queue_id', '{{%order_log}}', 'queue_id', '{{%queue}}', 'id', self::CASCADE, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-order_log-queue_id', '{{%order_log}}');
        $this->dropColumn('{{%order_log}}', 'queue_id');
    }
}

<?php
use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180207_045528_add_description_on_auth_item_and_translate
 */
class m180207_045528_add_description_on_auth_item_and_translate extends Migration
{
    public $replacement_list = [
        'catalog.kladercountries.delete' => 'Справочники   КЛАДР стран / Удалить',
        'catalog.kladercountries.index' => 'Справочники   КЛАДР стран',
        'operator.qualitycontrol' => 'Операторы   Контроль качества',
        'operator.quantitycontrol.audit' => 'Операторы / Контроль качества / Редактировать',
        'operator.quantitycontrol.confirm' => 'Операторы / Контроль качества / Проверено',
        'partner.control.edit' => 'Партнеры / Управление / Добавление партнера',
        'queue.control.edit' => 'Очереди   Управление   Добавление очереди',
        'queue.control.getQueuesByParams' => 'Очереди / Управление / Редактирование очереди / Получение списка доступных очередей для смены',
        'queue.control.getusersforqueue' => 'Очереди / Управление / Редактирование очереди / Получение списка операторов для назначения',
        'queue.user' => 'Настройка доступа к очереди / Настройка доступа к очереди',
    ];

    public $phrases = [
        'Справочники   КЛАДР стран / Удалить' => 'Directories KLADR countries / Delete',
        'Справочники   КЛАДР стран' => 'Directories KLADR countries',
        'Операторы   Контроль качества' => 'Quality Control Operators',
        'Операторы / Контроль качества / Редактировать' => 'Operators / Quality Control / Edit',
        'Операторы / Контроль качества / Проверено' => 'Operators / Quality Control / Verified',
        'Партнеры / Управление / Добавление партнера' => 'Partners / Management / Adding a partner',
        'Очереди / Управление  / Добавление очереди' => 'Queues / Manage / Add Queue',
        'Очереди / Управление / Редактирование очереди / Получение списка доступных очередей для смены' => 'Queues / Manage / Edit queue / Get a list of available queues for changing',
        'Очереди / Управление / Редактирование очереди / Получение списка операторов для назначения' => 'Queues / Management / Queue Editing / Getting the list of operators for the assignment',
        'Настройка доступа к очереди / Настройка доступа к очереди' => 'Configuring access to the queue / Setting up access to the queue',
    ];

    public $old_permissions = [
        'operator.control',
        'operator.quantitycontrol.control.confirm',
        'operator.quantitycontrol.control.audit',
        'operator.qualitycontrol.control'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach($this->old_permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_SUPERVISOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_CURATOR]);
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }

        foreach($this->replacement_list as $origin=>$replacement){
            $rename = yii::$app->db->createCommand("update auth_item set description = :description_replace where description=:description_origin")
                ->bindValues([
                    ':description_origin' => $origin,
                    ':description_replace' => $replacement
                ])->execute();

            if($rename){
                echo 'replace : '.$origin.' => '.$replacement.PHP_EOL;
                var_dump($rename).PHP_EOL;
            }else{
                echo 'not replace : '.$origin.' => '.$replacement.PHP_EOL;
                var_dump($rename).PHP_EOL;
            }
        }

        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->replacement_list as $origin=>$replacement){
            $rename = yii::$app->db->createCommand("update {{%auth_item}} set description = :description_origin where description=:description_replace")
                ->bindValues([
                    ':description_origin' => $origin,
                    ':description_replace' => $replacement
                ])->execute();

            if($rename){
                echo 'replace : '.$origin.' => '.$origin.PHP_EOL;
                var_dump($rename).PHP_EOL;
            }else{
                echo 'not replace : '.$origin.' => '.$origin.PHP_EOL;
                var_dump($rename).PHP_EOL;
            }
        }

        i18n_source_message::removeTranslate($this->phrases);
    }
}

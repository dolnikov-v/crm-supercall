<?php
use common\models\Currency;
use console\components\db\Migration;

/**
 * Class m000000_000008_currency
 */
class m000000_000008_currency extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'num_code' => $this->string(3)->notNull(),
            'char_code' => $this->string(3)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        foreach ($this->fixtures() as $key => $fixture) {
            $currency = new Currency($fixture);
            $currency->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }

    /**
     * @return array
     */
    private function fixtures()
    {
        return [
            [
                'name' => 'Доллар США',
                'num_code' => 840,
                'char_code' => 'USD',
            ],
            [
                'name' => 'Индийская рупия',
                'num_code' => 356,
                'char_code' => 'INR',
            ],
            [
                'name' => 'Тайский бат',
                'num_code' => 764,
                'char_code' => 'THB',
            ],
            [
                'name' => 'Мексиканский песо',
                'num_code' => 484,
                'char_code' => 'MXN',
            ],
        ];
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170627_051329_alter_auth_integer
 */
class m170627_051329_alter_auth_integer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('alter table {{%auth_assignment}} alter column user_id TYPE integer USING (user_id::integer);');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('{{%auth_assignment}}', 'user_id', $this->string(64));
    }
}

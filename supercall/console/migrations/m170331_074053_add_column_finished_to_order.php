<?php
use console\components\db\Migration;

/**
 * Class m170331_074053_add_column_finished_to_order
 */
class m170331_074053_add_column_finished_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'finished', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'finished');
    }
}

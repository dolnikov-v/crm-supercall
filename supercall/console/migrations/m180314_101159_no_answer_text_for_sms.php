<?php
use console\components\db\Migration;

/**
 * Class m180314_101159_no_answer_text_for_sms
 */
class m180314_101159_no_answer_text_for_sms extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('partner_form', 'no_answer_notification_text', $this->text());
        $this->addColumn('partner_form', 'no_answer_notification_sender', $this->string(15));

        $this->createTable('no_answer_notification_request', [
            'order_id' => $this->integer(),
            'group_id' => $this->string(255),
            'status' => $this->string(50),
            'phone' => $this->string(50),
            'response' => "JSONB",
            'sent_at' => $this->integer(),
            'created_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addPrimaryKey($this->getPkName('no_answer_notification_request', [
            'order_id',
            'group_id'
        ]), 'no_answer_notification_request', ['order_id', 'group_id']);
        $this->addForeignKey($this->getFkName('no_answer_notification_request', 'order_id'), 'no_answer_notification_request', 'order_id', 'order', 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('partner_form', 'no_answer_notification_text');
        $this->dropColumn('partner_form', 'no_answer_notification_sender');
        $this->dropTable('no_answer_notification_request');
    }
}

<?php

/**
 * Создание роли (с проверкой на наличие)
 * $role = [name => description]
 * $this->addRole(array $role)
 *
 * Удаление роли
 * $this-deleteRole(string $role)
 *
 * Проверка наличия роли
 * $this->existsRole(string $role_name);
 *
 * Создание пермишена (с проверкой на наличие)
 * $permission = [name => description]
 * $this->addPermission(array $permission)
 *
 * Удаление пермишена (автоматом удаляются все связки по ролям)
 * $this->deletePermission(string $permission);
 *
 * Проверка наличия пермишена
 * $this->>existsPermission(string $permission_name)
 *
 * Добавление пермишена к роли
 * $this->addPermissionToRole(string $permission, string $role)
 *
 * Удаления пермишена у роли
 * $this->deletePermissionByRole(string $permission, string $role)
 *
 * Добавление перевода (удаление перед добавление уже внутри метода)
 * i18n_source_message::addTranslate($this->phrases, true);
 *
 * Удаление переводов
 * i18n_source_message::removeTranslate($this->phrases);
 */

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m190207_114808_add_permission_queue_control_viewhistory
 */
class m190207_114808_add_permission_queue_control_viewhistory extends Migration
{

    public $phrases = [
        'Просмотр логов редактирование очереди' => 'View logs edit the queue',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
        $this->addPermission(['queue.control.viewhistory' => 'Просмотр логов редактирование очереди']);
        $this->addPermissionToRole('queue.control.viewhistory', User::ROLE_ADMINISTRATOR);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
        $this->deletePermissionByRole('queue.control.viewhistory', User::ROLE_ADMINISTRATOR);
        $this->deletePermission('queue.control.viewhistory');
    }
}

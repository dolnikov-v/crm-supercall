<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171019_095309_translate_applicants
 */
class m171019_095309_translate_applicants extends Migration
{
    public $phrases = [

        'Произошла ошибка при отправки смс, повторите попытку позже.' => 'An error occurred while sending sms, please try again later.',
        'Пользователь с таким номером или емайл уже существует.' => 'A user with this number or email already exists.',
        'Данные регистрации:<hr/> <b>логин:</b> {username} (или {email}) <br/><b>пароль:</b> {password}' => 'Login details: <hr /> <b> login: </b> {username} (or {email}) <br/> <b> password: </b> {password}',
        'Заявитель' => 'Applicant',
        'Список заявок' => 'List of applications',
        'Таблица с заявками' => 'Table with applications',
        'Подтвердить' => 'Confirm',
        'Отклонить' => 'Decline',
        'Заблокировать' => 'Block',
        'Заблокированный / Отклонённый' => 'Blocked / Rejected',
        'Ожидает активации' => 'Waiting for activation',
        'Разблокировать' => 'Unlock',
        'По умолчанию' => 'Default',
        'Заблокирован' => 'Blocked',
        'Удален' => 'Removed',
        'Новый' => 'New',
        'Активация аккаунта 2wcall.com' => 'Activate account 2wcall.com',
        'Ваш аккаунт одобрен:<hr/> <b>логин:</b> {username} (или {email}) <br/><b>пароль:</b> выслан Вам в письме после регистрации' => 'Your account is approved: <hr /> <b> login: </b> {username} (or {email}) <br/> <b> password: </b> sent to you in the email after registration',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

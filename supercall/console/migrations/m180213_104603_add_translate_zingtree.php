<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180213_104603_add_translate_zingtree
 */
class m180213_104603_add_translate_zingtree extends Migration
{
    public $phrases = [
        'Оператор / ZingTree' => 'Operator / ZingTree',
        'Оператор / ZingTree / Создание (редактирование)' => 'Operator / ZingTree / Creating (editing)',
        'Оператор / ZingTree / Удаление' => 'Operator / ZingTree / Delete',
        'Оператор / ZingTree / Активация (деактивация)' => 'Operator / ZingTree / Activation (deactivation)',
        'Источник' => 'Source',
        'Активность' => 'Activity',
        'Создано' => 'Created',
        'Обновлено' => 'Updated',
        'Активные' => 'Active',
        'Не активные' => 'Not active',
        'Таблица с ZingTree' => 'Table with ZingTree',
        'ZingTree не найден.' => 'ZingTree not found.',
        'ZingTree успешно активирован.' => 'ZingTree has been successfully activated.',
        'ZingTree успешно деактивирован.' => 'ZingTree has been successfully deactivated.',
        'ZingTree успешно удален.' => 'ZingTree was successfully deleted.',
        'Добавить ZingTree' => 'Add ZingTree',
        'ZingTree не найдены для данной страны' => 'ZingTree not found for this country',
        'Для страны будет использоваться последний добавленный активный ZingTree' => 'For the country, the last added active ZingTree will be used'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

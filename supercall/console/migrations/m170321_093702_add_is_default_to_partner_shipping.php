<?php
use console\components\db\Migration;

/**
 * Class m170321_093702_add_is_default_to_partner_shipping
 */
class m170321_093702_add_is_default_to_partner_shipping extends Migration
{
    /**
     * Возможность установить Курьера по умолчанию
     * Работает только в рамках 1 партнера и 1 страны
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_shipping}}', 'is_default', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_shipping}}', 'is_default');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m000000_000005_i18n_message
 */
class m000000_000005_i18n_message extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%i18n_message}}', [
            'id' => $this->integer(),
            'language' => $this->string(16),
            'translation' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey(null, 'i18n_message', ['id', 'language']);
        $this->addForeignKey(null, 'i18n_message', 'id', 'i18n_source_message', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%i18n_message}}');
    }
}

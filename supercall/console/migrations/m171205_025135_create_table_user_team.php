<?php
use console\components\db\Migration;
use common\models\User;
use common\models\Teams;
use common\models\UserTeam;

/**
 * Class m171205_025135_create_table_user_team
 */
class m171205_025135_create_table_user_team extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(UserTeam::tableName(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'team_id' => $this->integer()->notNull(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, UserTeam::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, UserTeam::tableName(), 'team_id', Teams::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(UserTeam::tableName());
    }
}

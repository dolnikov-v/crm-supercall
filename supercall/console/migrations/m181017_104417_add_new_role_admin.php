<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use common\models\User;
use console\components\db\Migration;
use yii\helpers\ArrayHelper;

/**
 * Class m181017_104417_add_new_role_admin
 */
class m181017_104417_add_new_role_admin extends Migration
{
    public $permissions = [
        "profile.control",
        "home.index.index",
        'operator.listoperators.queue',
        'operator.listoperators.queueset',
        'operator.listoperators.queueunset',
        'order.groupoperations.changequeue',
        'queue.control.activate',
        'queue.control.clear',
        'queue.control.deactivate',
        'queue.control.delete',
        'queue.control.edit',
        'queue.control.getqueuesbyparams',
        'queue.control.getusersforqueue',
        'queue.control.index',
        'queue.edit.configuration',
        'queue.user',
        'stats.queue.index',
        'operator.auditedorders.audit',
        'operator.auditedorders.index',
        'operator.auditedorders.orderlog',
        'operator.questionnaire.orders',
        'order.addressverification.confirm',
        'order.addressverification.edit',
        'order.addressverification.index',
        'order.change',
        'order.change.addproduct',
        'order.change.index',
        'order.change.productprice',
        'order.groupoperations.changequeue',
        'order.groupoperations.changestatus',
        'order.groupoperations.senttosqs',
        'order.index',
        'order.index.exportcallrecords',
        'order.index.index',
        'order.index.view',
        'order.index.viewcontacts',
        'stats.log.details',
        'stats.log.general',
        'stats.log.list',
        'stats.operator.index',
        'stats.order.index',
        'stats.orderprocessingtime.dates',
        'stats.orderprocessingtime.orders',
        'stats.orderprocessingtime.products',
        'stats.orderprocessingtime.statuses',
        'stats.product.index',
        'stats.queue.index',
        'access.team.edit',
        'access.team.index',
        'access.team.manageuserofteam',
        'access.user.index',
        'access.user.setproduct',
        'access.usergroupoperations.activateusers',
        'access.usergroupoperations.blockusers',
        'access.usergroupoperations.deleteusers',
        'access.usergroupoperations.offautocall',
        'access.usergroupoperations.offwebrtc',
        'access.usergroupoperations.onautocall',
        'access.usergroupoperations.onwebrtc',
        'access.usergroupoperations.undeleteusers'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addRole([User::ROLE_ADMINISTRATOR => 'Администратор']);

        foreach ($this->permissions as $permission) {
            echo $permission.PHP_EOL;

            $this->addPermission([$permission => $permission]);
            $this->addPermissionToRole($permission, User::ROLE_ADMINISTRATOR);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_LOGISTICIAN]);
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }

        $auth = $this->authManager;
        $role = $auth->getRole(User::ROLE_LOGISTICIAN);
        $auth->remove($role);
    }
}

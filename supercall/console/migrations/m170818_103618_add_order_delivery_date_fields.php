<?php
use console\components\db\Migration;
use common\modules\order\models\Order;
/**
 * Class m170818_103618_add_order_delivery_date_fields
 */
class m170818_103618_add_order_delivery_date_fields extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'delivery_from', $this->integer());
        $this->addColumn(Order::tableName(), 'delivery_to', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'delivery_from');
        $this->dropColumn(Order::tableName(), 'delivery_to');
    }
}

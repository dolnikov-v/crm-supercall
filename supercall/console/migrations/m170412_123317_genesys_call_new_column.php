<?php
use console\components\db\Migration;

/**
 * Class m170412_123317_genesys_call_new_column
 */
class m170412_123317_genesys_call_new_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%genesys_call}}', 'PRODUCT_TIMESTAMP', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%genesys_call}}', 'PRODUCT_TIMESTAMP');
    }
}

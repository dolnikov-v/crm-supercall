<?php
use console\components\db\Migration;
use common\modules\partner\models\PartnerForm;

/**
 * Class m180609_051551_add_new_column_min_delivery_days
 */
class m180609_051551_add_new_column_min_delivery_days extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(PartnerForm::tableName(), 'min_delivery_days', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(PartnerForm::tableName(), 'min_delivery_days');
    }
}

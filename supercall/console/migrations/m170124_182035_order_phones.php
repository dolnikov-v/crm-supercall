<?php
use console\components\db\Migration;

/**
 * Class m170124_182035_order_phones
 */
class m170124_182035_order_phones extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'order_phones', $this->string());
        $this->dropColumn('{{%order}}', 'genesys_id');
        $this->createTable('{{%order_genesys}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'genesys_id' => $this->integer(),
            'index' => $this->integer(),
            'phone' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addForeignKey('fk__order_genesys__order',
            '{{%order_genesys}}', 'order_id',
            '{{%order}}', 'id',
            'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_genesys}}');
        $this->dropColumn('{{%order}}', 'order_phones');
        $this->addColumn('{{%order}}', 'genesys_id', $this->integer());
    }
}

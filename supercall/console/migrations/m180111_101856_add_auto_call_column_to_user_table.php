<?php

use yii\db\Migration;

/**
 * Handles adding auto_call to table `user`.
 */
class m180111_101856_add_auto_call_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $table = Yii::$app->db->schema->getTableSchema('user');
        if (!isset($table->columns['auto_call'])) {
            $this->addColumn('user', 'auto_call', $this->integer(1)->defaultValue(0)->notNull());
        }
        $this->createIndex('idx-user-auto_call','{{%user}}', 'auto_call');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'auto_call');
    }
}

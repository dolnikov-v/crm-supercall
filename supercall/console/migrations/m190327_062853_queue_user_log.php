<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m190327_062853_queue_user_log
 */
class m190327_062853_queue_user_log extends Migration
{
    public $phrases = [
        'Логи очередей по операторам' => 'Operator queues logs',
        'Просмотр списка логов очередей по операторам' => 'Operator queues logs index'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        $this->addPermission(['administration.queueuserlog.index' => 'Просмотр списка логов очередей по операторам']);
        $this->addPermissionToRole('administration.queueuserlog.index', User::ROLE_ADMINISTRATOR);

        $this->createTable('{{%queue_user_log}}', [
            'id' => $this->primaryKey(),
            'queue_id' => $this->integer()->notNull(),
            'operator_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'action' => $this->string(6)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, '{{%queue_user_log}}', 'queue_id', '{{%queue}}', 'id', Migration::CASCADE, Migration::RESTRICT);
        $this->addForeignKey(null, '{{%queue_user_log}}', 'operator_id', '{{%user}}', 'id', Migration::CASCADE, Migration::RESTRICT);
        $this->addForeignKey(null, '{{%queue_user_log}}', 'user_id', '{{%user}}', 'id', Migration::CASCADE, Migration::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->deletePermissionByRole('administration.queueuserlog.index', User::ROLE_ADMINISTRATOR);

        $this->dropTable('{{%queue_user_log}}');
    }
}

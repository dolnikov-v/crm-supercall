<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180905_091743_add_translate_to_address_filter_order
 */
class m180905_091743_add_translate_to_address_filter_order extends Migration
{
    public $phrases = [
        'У формы #{form_id} не найдены поля с атрибутом "из КЛАДРа"' => 'Form # {form_id} did not find fields with the attribute "from the KLADER"',
        'Для данной страны не найдена форма с типом заказов: {type}' => 'For a given country, a form with the order form type was not found: {type}',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;
use backend\modules\order\models\OrderProductBackup;
use common\models\Product;

/**
 * Class m171115_044114_create_table_order_product_backup
 */
class m171115_044114_create_table_order_product_backup extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(OrderProductBackup::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'price' => $this->double()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);


        $this->addColumn(OrderProductBackup::tableName(), 'package_id', $this->integer()->defaultValue(null));
        $this->addColumn(OrderProductBackup::tableName(), 'cost', $this->double());
        $this->addColumn(OrderProductBackup::tableName(), 'gift', $this->boolean());
        $this->addColumn(OrderProductBackup::tableName(), 'fixed_price', $this->boolean());

        $this->addForeignKey(null, OrderProductBackup::tableName(), 'product_id', Product::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(OrderProductBackup::tableName());
    }
}

<?php

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180820_115933_add_permission_for_export_call_records
 */
class m180820_115933_add_permission_for_export_call_records extends Migration
{
    public $phrases = [
        'Экспорт записей разговоров' => 'Exporting call records',
        'Заказы / Список заказов / Экспорт записей разговоров' => 'Orders / List of orders / Export of records of calls'
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        $this->insert('{{%auth_item}}', array(
            'name' => 'order.index.exportcallrecords',
            'type' => '2',
            'description' => 'Заказы / Список заказов / Экспорт записей разговоров',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'order.index.exportcallrecords'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_CONTROLLER,
            'child' => 'order.index.exportcallrecords'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.exportcallrecords', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.index.exportcallrecords', 'parent' => User::ROLE_CONTROLLER]);

        $this->delete('{{%auth_item}}', ['name' => 'order.index.exportcallrecords']);
    }
}

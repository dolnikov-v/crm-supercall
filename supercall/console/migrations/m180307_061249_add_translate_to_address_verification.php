<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180307_061249_add_translate_to_address_verification
 */
class m180307_061249_add_translate_to_address_verification extends Migration
{
    public $phrases = [
        'Редактирование адреса заказа' => 'Editing the order address',
        'Сохранить и подтвердить' => 'Save and confirm'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

    }
}

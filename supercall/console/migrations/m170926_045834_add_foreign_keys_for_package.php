<?php
use console\components\db\Migration;
use common\modules\partner\models\Package;
use common\models\Country;
use common\modules\partner\models\Partner;
use common\models\Product;

/**
 * Class m170926_045834_add_foreign_keys_for_package
 */
class m170926_045834_add_foreign_keys_for_package extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(null, Package::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, Package::tableName(), 'partner_id', Partner::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, Package::tableName(), 'product_id', Product::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName(Package::tableName(), 'country_id'), Package::tableName());
        $this->dropForeignKey($this->getFkName(Package::tableName(), 'partner_id'), Package::tableName());
        $this->dropForeignKey($this->getFkName(Package::tableName(), 'product_id'), Package::tableName());
    }
}

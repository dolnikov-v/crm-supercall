<?php
use console\components\db\Migration;
use common\modules\order\models\Order;
use common\modules\partner\models\Package;

/**
 * Class m170920_082542_add_column_package_id_in_table_order
 */
class m170920_082542_add_column_package_id_in_table_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'package_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(null, Order::tableName(), 'package_id', Package::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'package_id');
    }
}

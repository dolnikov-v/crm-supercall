<?php

use common\models\Country;
use common\models\Product;
use common\models\User;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use console\components\db\Migration;

/**
 * Class m180912_050144_create_table_stats_key_word
 */
class m180912_050144_create_table_stats_key_word extends Migration
{
    public $table_aggregate = '{{%aggregate_word}}';
    public $table_aggregate_base = '{{%aggregate_word_base}}';
    public $table_aggregate_stat = '{{%aggregate_word_stat}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->table_aggregate, [
            'id' => $this->primaryKey(),
            'word_id' => $this->integer()->notNull(),
            'call_history_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->defaultValue(NULL),
            'product_id' => $this->integer()->notNull(),
            'total' => $this->integer()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, $this->table_aggregate, 'call_history_id', CallHistory::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, $this->table_aggregate, 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, $this->table_aggregate, 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, $this->table_aggregate, 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, $this->table_aggregate, 'product_id', Product::tableName(), 'id', self::CASCADE, self::RESTRICT);

        $this->createIndex(null, $this->table_aggregate, ['call_history_id', 'word_id']);
        $this->createIndex(null, $this->table_aggregate, ['order_id', 'word_id']);
        $this->createIndex(null, $this->table_aggregate, ['user_id', 'word_id']);
        $this->createIndex(null, $this->table_aggregate, ['status', 'word_id']);
        $this->createIndex(null, $this->table_aggregate, ['product_id', 'word_id']);

        $this->createIndex(null, $this->table_aggregate, ['word_id', 'call_history_id', 'order_id', 'product_id', 'status', 'country_id', 'user_id'], true);


        $this->createTable($this->table_aggregate_base, [
            'id' => $this->primaryKey(),
            'word' => $this->string()->notNull(),
            'total' => $this->integer()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->createIndex(null, $this->table_aggregate_base, ['word'], true);

        $this->addForeignKey(null, $this->table_aggregate, 'word_id', $this->table_aggregate_base, 'id', self::CASCADE, self::RESTRICT);

        $this->createTable($this->table_aggregate_stat, [
            'id' => $this->primaryKey(),
            'word_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'total' => $this->integer()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->createIndex(null, $this->table_aggregate_stat, ['word_id', 'country_id', 'product_id'], true);

        $this->addForeignKey(null, $this->table_aggregate_stat, 'word_id', $this->table_aggregate_base, 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, $this->table_aggregate_stat, 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, $this->table_aggregate_stat, 'product_id', Product::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->table_aggregate);
        $this->dropTable($this->table_aggregate_stat);
        $this->dropTable($this->table_aggregate_base);
    }
}

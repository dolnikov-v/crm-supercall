<?php
use console\components\db\Migration;
use common\models\Teams;
use common\models\Country;
use common\models\TeamsCountry;

/**
 * Class m171205_114043_create_table_teams_country
 */
class m171205_114043_create_table_teams_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(TeamsCountry::tableName(), [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(null, TeamsCountry::tableName(), 'team_id', Teams::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, TeamsCountry::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(TeamsCountry::tableName());
    }
}

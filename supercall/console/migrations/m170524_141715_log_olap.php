<?php
use console\components\db\Migration;

/**
 * Class m170524_141715_log_olap
 */
class m170524_141715_log_olap extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_log_olap_approved}}', [
            'period_type' => $this->smallInteger(1)->notNull(),
            'period' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),

            'progress' => $this->integer()->notNull() . ' DEFAULT 0',
            'buyout' => $this->integer()->notNull() . ' DEFAULT 0',
            'nobuyout' => $this->integer()->notNull() . ' DEFAULT 0',
            'quantity' => $this->integer()->notNull() . ' DEFAULT 0',
        ]);
        $this->addPrimaryKey('pk_oloap', '{{%order_log_olap_approved}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
        ]);
        $this->addKeys('{{%order_log_olap_approved}}', 'oloap');


        $this->createTable('{{%order_log_olap_reject}}', [
            'period_type' => $this->smallInteger(1)->notNull(),
            'period' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),

            'expensive' => $this->integer()->notNull() . ' DEFAULT 0',
            'changed_mind' => $this->integer()->notNull() . ' DEFAULT 0',
            'medical' => $this->integer()->notNull() . ' DEFAULT 0',
            'no_comments' => $this->integer()->notNull() . ' DEFAULT 0',
            'auto' => $this->integer()->notNull() . ' DEFAULT 0',
            'quantity' => $this->integer()->notNull() . ' DEFAULT 0',
        ]);
        $this->addPrimaryKey('pk_olor', '{{%order_log_olap_reject}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
        ]);
        $this->addKeys('{{%order_log_olap_reject}}', 'olor');


        $this->createTable('{{%order_log_olap_trash}}', [
            'period_type' => $this->smallInteger(1)->notNull(),
            'period' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),

            'wrong_number' => $this->integer()->notNull() . ' DEFAULT 0',
            'double' => $this->integer()->notNull() . ' DEFAULT 0',
            'unaware' => $this->integer()->notNull() . ' DEFAULT 0',
            'consulting' => $this->integer()->notNull() . ' DEFAULT 0',
            'competitor' => $this->integer()->notNull() . ' DEFAULT 0',
            'return' => $this->integer()->notNull() . ' DEFAULT 0',
            'denied' => $this->integer()->notNull() . ' DEFAULT 0',
            'quantity' => $this->integer()->notNull() . ' DEFAULT 0',
        ]);
        $this->addPrimaryKey('pk_olot', '{{%order_log_olap_trash}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
        ]);
        $this->addKeys('{{%order_log_olap_trash}}', 'olot');
    }

    protected function addKeys($table, $prefix)
    {
        $this->addForeignKey(
            'fk_'.$prefix.'_product',
            $table,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_'.$prefix.'_type',
            $table,
            'type_id',
            '{{%order_type}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_'.$prefix.'_partner',
            $table,
            'partner_id',
            '{{%partner}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_'.$prefix.'_country',
            $table,
            'country_id',
            '{{%country}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_log_olap_trash}}');
        $this->dropTable('{{%order_log_olap_reject}}');
        $this->dropTable('{{%order_log_olap_approved}}');
    }
}

<?php
use common\modules\order\models\OrderType;
use console\components\db\Migration;

/**
 * Class m180307_090036_add_order_type_old_crm
 */
class m180307_090036_add_order_type_old_crm extends Migration
{
    public $statuses = [
        OrderType::TYPE_OLD_CRM => 'Заказы из старого КЦ'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->statuses as $type => $name) {
            $this->insert(OrderType::tableName(), ['name' => $name, 'created_at' => time(), 'updated_at' => time()]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        OrderType::deleteAll(['>', 'id', 5]);
    }
}

<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m181207_035723_add_permission_and_translate_to_operators_online_stats
 */
class m181207_035723_add_permission_and_translate_to_operators_online_stats extends Migration
{
    public $permission = 'stats.operatoronline.index';

    public $phrases = [
        'Статистика операторов по времени онлайн' => 'Online operators statistics',
        'Список операторов ({count})' => 'List of operators ({count})'

    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        $this->insert('{{%auth_item}}', array(
            'name' => $this->permission,
            'type' => '2',
            'description' => 'Статистика операторов по времени онлайн',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => $this->permission
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_CURATOR,
            'child' => $this->permission
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_ADMINISTRATOR,
            'child' => $this->permission
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->delete($this->authManager->itemChildTable, ['child' => $this->permission, 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => $this->permission, 'parent' => User::ROLE_CURATOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => $this->permission, 'parent' => User::ROLE_ADMINISTRATOR]);

        $this->delete('{{%auth_item}}', ['name' => $this->permission]);
    }
}

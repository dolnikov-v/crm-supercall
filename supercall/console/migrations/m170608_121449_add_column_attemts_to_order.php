<?php
use console\components\db\Migration;

/**
 * Class m170608_121449_add_column_attemts_to_order
 */
class m170608_121449_add_column_attemts_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'attempts', $this->smallInteger()->defaultValue('0'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'attempts');
    }
}

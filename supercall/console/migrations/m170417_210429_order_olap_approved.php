<?php
use console\components\db\Migration;

/**
 * Class m170417_210429_order_olap_approved
 */
class m170417_210429_order_olap_approved extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_olap_approved}}', [
            'period_type' => $this->smallInteger(1)->notNull(),
            'period' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),

            'progress' => $this->integer()->notNull() . ' DEFAULT 0',
            'buyout' => $this->integer()->notNull() . ' DEFAULT 0',
            'nobuyout' => $this->integer()->notNull() . ' DEFAULT 0',
            'quantity' => $this->integer()->notNull() . ' DEFAULT 0',
        ]);

        $this->addForeignKey(
            'fk_ooap_product',
            '{{%order_olap_approved}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_ooap_type',
            '{{%order_olap_approved}}',
            'type_id',
            '{{%order_type}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_ooap_partner',
            '{{%order_olap_approved}}',
            'partner_id',
            '{{%partner}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_ooap_country',
            '{{%order_olap_approved}}',
            'country_id',
            '{{%country}}',
            'id',
            'CASCADE'
        );

        $this->addPrimaryKey('pk_ooap', '{{%order_olap_approved}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_olap_approved}}');
    }
}

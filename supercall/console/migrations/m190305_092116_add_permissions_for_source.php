<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m190305_092116_add_permissions_for_source
 */
class m190305_092116_add_permissions_for_source extends Migration
{
    public $phrases = [
        'Просмотр списка' => 'Sources list index',
        'Редактирование и создание списка источников' => 'Edit and create sources list',
        'Активация источника' => 'Activate source',
        'Деактивация источника' => 'Deactivate source'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        $this->addPermission(['catalog.source.index' => 'Просмотр списка источников']);
        $this->addPermissionToRole('catalog.source.index', User::ROLE_ADMINISTRATOR);

        $this->addPermission(['catalog.source.edit' => 'Редактирование и создание списка источников']);
        $this->addPermissionToRole('catalog.source.edit', User::ROLE_ADMINISTRATOR);

        $this->addPermission(['catalog.source.activate' => 'Активация источника']);
        $this->addPermissionToRole('catalog.source.activate', User::ROLE_ADMINISTRATOR);

        $this->addPermission(['catalog.source.deactivate' => 'Деактивация источника']);
        $this->addPermissionToRole('catalog.source.deactivate', User::ROLE_ADMINISTRATOR);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->deletePermission('catalog.source.index');
        $this->deletePermission('catalog.source.edit');
        $this->deletePermission('catalog.source.activate');
        $this->deletePermission('catalog.source.deactivate');
    }
}

<?php
use console\components\db\Migration;
use backend\modules\operator\models\QuestionnaireData;
use backend\modules\operator\models\QuestionnaireOrder;
use backend\modules\operator\models\QuestionnaireManagement;

/**
 * Class m170630_044014_create_table_questionnaire_data
 */
class m170630_044014_create_table_questionnaire_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(QuestionnaireData::tableName(), [
            'id' => $this->primaryKey(),
            'questionnaire_order_id' => $this->integer()->notNull(),
            'questionnaire_id' => $this->integer()->notNull(),
            'question_text' => $this->string()->notNull(),
            'answer_text' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, QuestionnaireData::tableName(), 'questionnaire_order_id', QuestionnaireOrder::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, QuestionnaireData::tableName(), 'questionnaire_id', QuestionnaireManagement::tableName(), 'id', self::CASCADE, self::RESTRICT);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(QuestionnaireData::tableName());
    }
}

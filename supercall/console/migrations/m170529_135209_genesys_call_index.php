<?php
use console\components\db\Migration;

/**
 * Class m170529_135209_genesys_call_index
 */
class m170529_135209_genesys_call_index extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('idx_genesys_call_product_id_call_time', '{{%genesys_call}}', ['PRODUCT_ID', 'CALL_START']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('idx_genesys_call_product_id_call_time', '{{%genesys_call}}');
    }
}

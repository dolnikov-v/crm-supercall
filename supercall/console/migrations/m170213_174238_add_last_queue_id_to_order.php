<?php
use console\components\db\Migration;

/**
 * Class m170213_174238_add_last_queue_id_to_order
 * Данное поле необходимо, чтобы определять к какой очереди реббита
 * последний раз находился заказ
 */
class m170213_174238_add_last_queue_id_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'last_queue_id', $this->integer()->null());
        $this->addForeignKey('fk-order-last_queue_id', '{{%order}}', 'last_queue_id', '{{%queue}}', 'id', self::SET_NULL, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-order-last_queue_id', '{{%order}}');
        $this->dropColumn('{{%order_log}}', 'queue_id');
    }
}

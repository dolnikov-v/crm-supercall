<?php
use console\components\db\Migration;

/**
 * Class m170526_094031_add_column_mode_queue
 */
class m170526_094031_add_column_mode_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%queue}}', 'mode_auto', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%queue}}', 'mode_auto');
    }
}

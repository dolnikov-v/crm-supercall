<?php

use common\models\User;
use console\components\db\Migration;

/**
 * Class m180604_040736_add_column_use_messendger_to_user_table
 */
class m180604_040736_add_column_use_messendger_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'use_messenger', $this->boolean()->after('webrtc')->defaultValue(User::USE_MESSENGER_OFF));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(User::tableName(), 'use_messenger');
    }
}

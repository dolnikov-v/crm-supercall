<?php
use console\components\db\Migration;

/**
 * Class m170605_203148_order_logs_price_data_jsonb
 */
class m170605_203148_order_logs_price_data_jsonb extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('alter table order_logs alter column product set data type jsonb using product::jsonb');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('alter table order_logs alter column product set data type json using product::json');
    }
}

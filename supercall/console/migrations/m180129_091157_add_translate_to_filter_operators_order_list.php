<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180129_091157_add_translate_to_filter_operators_order_list
 */
class m180129_091157_add_translate_to_filter_operators_order_list extends Migration
{
    public $phrases = [
        'Список операторов обновлен' => 'List of operators updated',
        'Ошибка обновления списка операторов' => 'Error updating list of operators'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

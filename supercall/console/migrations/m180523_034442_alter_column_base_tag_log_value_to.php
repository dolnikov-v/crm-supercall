<?php

use common\models\LoggerTag;
use console\components\db\Migration;

/**
 * Class m180523_034442_alter_column_base_tag_log_value_to
 */
class m180523_034442_alter_column_base_tag_log_value_to extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(LoggerTag::tableName(), 'value', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(LoggerTag::tableName(), 'value', $this->string());
    }
}

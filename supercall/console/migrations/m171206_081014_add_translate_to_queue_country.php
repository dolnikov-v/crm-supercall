<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171206_081014_add_translate_to_queue_country
 */
class m171206_081014_add_translate_to_queue_country extends Migration
{
    public $phrases = [
        'Укажите страну/страны для команды' => 'Enter the country / countries for the team',
        'Имя команды уже используется' => 'Team name is already in use',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

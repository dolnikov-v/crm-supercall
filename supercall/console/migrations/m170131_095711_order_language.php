<?php
use console\components\db\Migration;

/**
 * Class m170131_095711_order_language
 */
class m170131_095711_order_language extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%country}}', 'language_id', $this->integer());
        $this->addForeignKey('fk__country__language', '{{%country}}', 'language_id', '{{%i18n_language}}', 'id', 'restrict', 'cascade');

        $this->addColumn('{{%order}}', 'language_id', $this->integer());
        $this->addForeignKey('fk__order__language', '{{%order}}', 'language_id', '{{%i18n_language}}', 'id', 'restrict', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk__country__language', '{{%country}}');
        $this->dropColumn('{{%country}}', 'language_id');

        $this->dropForeignKey('fk__order__language', '{{%order}}');
        $this->dropColumn('{{%order}}', 'language_id');
    }
}

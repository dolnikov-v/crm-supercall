<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m181123_115206_add_translate_partner_form
 */
class m181123_115206_add_translate_partner_form extends Migration
{
    public $phrases = [
        'Использовать брокер служб доставок партнера' => 'Use Partner Delivery Services Broker',
        'Псевдо статусы' => 'Pseudo statuses',
        'Показывать доставку' => 'Show delivery',
        'Показывать страну' => 'Show country',
        'Кнопки' => 'Buttons',
        'Сохранить форму' => 'Save form',
        'Использовать Messenger' => 'Use messenger',
        'Быстрая смена очереди' => 'Fast change queue',
        'Дополнительная цена' => 'Additional price',
        'Тип дополнительной цены' => 'Additional price type',
        'Условие' => 'Condition',
        'Атрибуты (основные)' => 'Attributes (main)',
        'Добавить аттрибут' => 'Add attribute',
        'Обязательно' => 'Required field',
        'Регулярное выражение' => 'Regular expression',
        'Число' => 'Number',
        'Строка' => 'String',
        'Координата' => 'Coordinate'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

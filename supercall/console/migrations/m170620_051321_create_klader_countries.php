<?php
use console\components\db\Migration;
use backend\modules\catalog\models\KladerCountries;

/**
 * Class m170620_051321_create_klader_countries
 */
class m170620_051321_create_klader_countries extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;

        $this->createTable(KladerCountries::tableName(), [
            'id' => $this->primaryKey(),
            'path' => $this->string(255),
            'depth' => $this->integer()->notNull()->defaultValue(0),
            'sort' => $this->integer()->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            //for root
            'country_id' => $this->integer()->defaultValue(null)
        ], $tableOptions);

        $this->createIndex('path', KladerCountries::tableName(), ['path']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(KladerCountries::tableName());
    }
}

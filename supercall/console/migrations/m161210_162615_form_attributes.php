<?php
use console\components\db\Migration;

/**
 * Class m161210_162615_form_attributes
 */
class m161210_162615_form_attributes extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'form_id', $this->integer()->null());
        $this->addForeignKey('fk__order__form', '{{%order}}', 'form_id', '{{%partner_form}}', 'id', 'restrict', 'cascade');

        $this->createTable('{{%partner_form_attribute}}', [
            'id' => $this->primaryKey(),
            'form_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'label' => $this->string()->notNull(),
            'required' => $this->boolean()->notNull(),
            'type' => $this->string()->notNull()
        ]);
        $this->addForeignKey('fk__form_attribute__form', '{{%partner_form_attribute}}', 'form_id', '{{%partner_form}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_form_attribute}}');
        $this->dropForeignKey('fk__order__form', '{{%order}}');
        $this->dropColumn('{{%order}}', 'form_id');
    }
}

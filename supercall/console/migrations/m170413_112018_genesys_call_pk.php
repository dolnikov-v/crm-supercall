<?php
use console\components\db\Migration;

/**
 * Class m170413_112018_genesys_call_pk
 */
class m170413_112018_genesys_call_pk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addPrimaryKey('pk__genesys_call', 'genesys_call', 'CALLID');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropPrimaryKey('pk_genesys_call', 'genesys_call');
    }
}

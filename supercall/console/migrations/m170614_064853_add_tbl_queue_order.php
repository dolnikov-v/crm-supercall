<?php
use console\components\db\Migration;

/**
 * Class m170614_064853_add_tbl_queue_order
 */
class m170614_064853_add_tbl_queue_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%queue_order}}', [
            'user_id'    => $this->integer(),
            'order_id'   => $this->integer(),
            'blocked_to' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%queue_order}}');
    }
}

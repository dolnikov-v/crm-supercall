<?php
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180213_043153_add_permissions_for_zing_tree
 */
class m180213_043153_add_permissions_for_zing_tree extends Migration
{
    public $permissions = [
        'operator.zingtree.index' => 'Оператор / ZingTree',
        'operator.zingtree.edit' => 'Оператор / ZingTree / Создание (редактирование)',
        'operator.zingtree.delete' => 'Оператор / ZingTree / Удаление',
        'operator.zingtree.activate' => 'Оператор / ZingTree / Активация (деактивация)',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach($this->permissions as $name=>$desc) {
            $this->insert('{{%auth_item}}', array(
                'name' => $name,
                'type' => '2',
                'description' => $desc,
                'created_at' => time(),
                'updated_at' => time()
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_SUPERVISOR,
                'child' => $name
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->permissions as $name=>$desc) {
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => User::ROLE_SUPERVISOR]);
            $this->delete('{{%auth_item}}', ['name' => $name]);
        }
    }
}

<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m180314_085846_add_translations_to_logger_2
 */
class m180314_085846_add_translations_to_logger_2 extends Migration
{
    public $phrases = [
        'Старые поля' => 'Old fields',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

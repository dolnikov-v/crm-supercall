<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180214_120015_add_translate_to_wiki
 */
class m180214_120015_add_translate_to_wiki extends Migration
{
    public $phrases = [
        'Статья не найдена' => 'Article not found',
        'В черновики' => 'In drafts',
        'Сохранить как черновик' => 'Save as draft',
        'Опубликовать' => 'Publish',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180827_043032_add_translate_to_queue_fro_earliest_lead_date
 */
class m180827_043032_add_translate_to_queue_fro_earliest_lead_date extends Migration
{
    public $phrases = [
        'Дата самого раннего лида' => 'Date of the earliest lead'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

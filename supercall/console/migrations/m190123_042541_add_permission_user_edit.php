<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use common\models\User;
use console\components\db\Migration;

/**
 * Class m190123_042541_add_permission_user_edit
 */
class m190123_042541_add_permission_user_edit extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $permission = ['access.user.edit' => 'Редактирование пользователей'];
        $this->addPermission($permission);

        $this->addPermissionToRole('access.user.edit', User::ROLE_ADMINISTRATOR);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->deletePermissionByRole('access.user.edit', User::ROLE_ADMINISTRATOR);
        $this->deletePermission('access.user.edit');
    }
}

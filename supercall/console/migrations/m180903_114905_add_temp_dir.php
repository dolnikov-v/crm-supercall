<?php
use console\components\db\Migration;
use yii\helpers\FileHelper;

/**
 * Class m180903_114905_add_temp_dir
 */
class m180903_114905_add_temp_dir extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        FileHelper::createDirectory(yii::getAlias('@backend').DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'temp');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        FileHelper::removeDirectory(yii::getAlias('@backend').DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'temp');
    }
}

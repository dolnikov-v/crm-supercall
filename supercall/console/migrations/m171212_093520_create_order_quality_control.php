<?php
use console\components\db\Migration;
use backend\modules\operator\models\QualityControl;
use common\models\User;
use common\modules\order\models\Order;

/**
 * Class m171212_093520_create_order_quality_control
 */
class m171212_093520_create_order_quality_control extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(QualityControl::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'operator_id' => $this->integer()->notNull(),
            'controller_id' => $this->integer()->notNull(),
            'edit' => $this->boolean()->defaultValue(false),
            'created_at' => $this->integer()->defaultValue(time()),
            'updated_at' => $this->integer()->defaultValue(time())
        ]);

        $this->addForeignKey(null, QualityControl::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, QualityControl::tableName(), 'operator_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, QualityControl::tableName(), 'controller_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(QualityControl::tableName());
    }
}

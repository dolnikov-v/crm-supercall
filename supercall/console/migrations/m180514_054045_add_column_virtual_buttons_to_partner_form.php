<?php

use common\modules\partner\models\PartnerForm;
use console\components\db\Migration;

/**
 * Class m180514_054045_add_column_virtual_buttons_to_partner_form
 */
class m180514_054045_add_column_virtual_buttons_to_partner_form extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(PartnerForm::tableName(), 'virtual_buttons', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(PartnerForm::tableName(), 'virtual_buttons');
    }
}

<?php
use console\components\db\Migration;
use backend\modules\queue\models\Queue;

/**
 * Class m170911_064002_add_event_to_queue
 */
class m170911_064002_add_event_to_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Queue::tableName(), 'event', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Queue::tableName(), 'event');
    }
}

<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m181114_102620_add_translate_promo_product
 */
class m181114_102620_add_translate_promo_product extends Migration
{
    public $phrases = [
        'Акционные товары' => 'Promotional goods',
        'Список товаров зависит от партнёра и страны, указанного в форме. При смене партнёра или страны формы - список акционных товаров будет сброшен' => 'List of products depends on the partner and the country specified in the form. If you change the partner or country of the form - the list of promotional items will be reset to',
        'Выбрать все доступные товары партнёра' => 'Select all available partner products',
        'Сохранить список акционных товаров' => 'Save the list of promotional goods',
        'Промо продукты' => 'Promotional goods'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

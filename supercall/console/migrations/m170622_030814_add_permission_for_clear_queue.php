<?php
use console\components\db\Migration;

/**
 * Class m170622_030814_add_permission_for_clear_queue
 */
class m170622_030814_add_permission_for_clear_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'queue.control.clear',
            'type' => '2',
            'description' => 'queue.control.clear',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'superadmin',
            'child' => 'queue.control.clear'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'queue.control.clear', 'parent' => 'superadmin']);
        $this->delete('{{%auth_item}}', ['name' => 'queue.control.clear']);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m000000_000014_order
 */
class m000000_000014_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'sub_status' => $this->integer()->defaultValue(null),
            'partner_id' => $this->integer()->notNull(),
            'foreign_id' => $this->integer()->notNull(),
            'customer_full_name' => $this->string(200)->notNull(),
            'customer_phone' => $this->string(20)->notNull(),
            'customer_mobile' => $this->string(20)->defaultValue(null),
            'customer_address' => $this->text()->defaultValue(null),
            'customer_ip' => $this->string(15)->defaultValue(null),
            'customer_components' => $this->text()->defaultValue(null),
            'comment' => $this->text(),
            'init_price' => $this->double()->defaultValue(0),
            'final_price' => $this->double()->defaultValue(0),
            'shipping_price' => $this->double()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, '{{%order}}', 'country_id', '{{%country}}', 'id', self::RESTRICT, self::RESTRICT);

        $this->createIndex(null, '{{%order}}', 'country_id');
        $this->createIndex(null, '{{%order}}', 'type');
        $this->createIndex(null, '{{%order}}', 'status');
        $this->createIndex(null, '{{%order}}', ['partner_id', 'foreign_id'], true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order}}');
    }
}

<?php
use console\components\db\Migration;
use common\models\User;

/**
 * Class m170918_090439_add_permission_for_operator_report
 */
class m170918_090439_add_permission_for_operator_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => 'stats.operator.index',
            'type' => '2',
            'description' => 'stats.product.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'stats.operator.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_CURATOR,
            'child' => 'stats.operator.index'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->delete($this->authManager->itemChildTable, ['child' => 'stats.operator.index', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => 'stats.operator.index', 'parent' => User::ROLE_CURATOR]);

        $this->delete('{{%auth_item}}', ['name' => 'stats.operator.index']);
    }
}

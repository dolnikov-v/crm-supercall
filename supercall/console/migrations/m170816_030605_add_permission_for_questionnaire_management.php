<?php
use console\components\db\Migration;
use common\models\User;

/**
 * Class m170816_030605_add_permission_for_questionnaire_management
 */
class m170816_030605_add_permission_for_questionnaire_management extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => 'operator.questionnairemanagement.index',
            'type' => '2',
            'description' => 'operator.questionnairemanagement.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'curator',
            'child' => 'operator.questionnairemanagement.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_CONTROLLER,
            'child' => 'operator.questionnairemanagement.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'operator.questionnairemanagement.index', 'parent' => 'curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'operator.questionnairemanagement.index', 'parent' => User::ROLE_CONTROLLER]);

        $this->delete('{{%auth_item}}', ['name' => 'operator.questionnairemanagement.index']);
    }
}

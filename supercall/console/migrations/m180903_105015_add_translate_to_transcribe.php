<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180903_105015_add_translate_to_transcribe
 */
class m180903_105015_add_translate_to_transcribe extends Migration
{

    public $phrases = [
            'Запросить транскрибацию' => 'Request transcription',
            'В процессе: {status}' => 'In progress: {status}',
            'Сведения о записи разговора не найдены' => 'Information about the recording of the conversation was not found',
            'Задание для получение транскрибации успешно создано' => 'The task for obtaining transcription was successfully created',
            'Запись разговора в папке Temp' => 'Recording a conversation in the Temp folder',
            'Запись разговора на S3' => 'Recording a conversation on S3',
            'Имя задачи' => 'Task name',
            'Параметры задачи' => 'Task parameters',
            'Файл транскрибации на S3' => 'Transcription file on S3',
            'Ошибка' => 'Error',
            'Начало обработки' => 'Processing start',
            'Успешный запрос записи с сервера Asterisk' => 'Successful write request from the Asterisk server',
            'Ошибка запроса записи с сервера Asterisk' => 'Error requesting entry from the Asterisk server',
            'Сохранение записи на сервере выполнено' => 'Saving the record on the server is complete',
            'Ошибка сохранение записи на сервере' => 'Error saving record on server',
            'Отправка записи на S3 сервис выполнена' => 'Sending an entry to the S3 service is complete',
            'Ошибка отправка записи на S3 сервис' => 'Error sending a record to S3 service',
            'Задача на транскрибацию успешно выполнена' => 'Transcription task completed successfully',
            'Ошибка создание задачи для транскрибации' => 'Error creating a task for transcription',
            'Ожидание готовности задачи' => 'Waiting for the task to be ready',
            'Успешное получение транскрибации, идет обработка' => 'Successful receipt of transcription, is processing',
            'Ошибка получения транскрибации' => 'Error obtaining transcription',
            'Статус не определён' => 'Status not defined',
        ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

    }
}

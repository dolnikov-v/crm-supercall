<?php
use console\components\db\Migration;
use backend\modules\administration\models\JsLogger;
use common\modules\order\models\Order;
use common\models\User;

/**
 * Class m171017_051512_add_table_js_logger
 */
class m171017_051512_add_table_js_logger extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(JsLogger::tableName(),[
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->defaultValue(null),
            'user_id' => $this->integer()->defaultValue(null),
            'script' => $this->string()->notNull(),
            'type' => $this->string(),
            'stack' => $this->text(),
            'message' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey('js_logger_order_id_order_id', JsLogger::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey('js_logger_user_id_user_id', JsLogger::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(JsLogger::tableName());
    }
}

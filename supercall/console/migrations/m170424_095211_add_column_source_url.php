<?php
use console\components\db\Migration;

/**
 * Class m170424_095211_add_column_source_url
 */
class m170424_095211_add_column_source_url extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'source_uri', 'VARCHAR');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'source_uri');
    }
}

<?php
use console\components\db\Migration;
use backend\modules\catalog\models\KladerCountries;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
/**
 * Class m170821_113142_add_column_partner_form_id_to_klader_countries
 */
class m170821_113142_add_column_partner_form_id_to_klader_countries extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(KladerCountries::tableName(), 'partner_form_id', $this->integer());
        $this->addColumn(KladerCountries::tableName(), 'partner_form_attribute_id', $this->integer());

        $this->addForeignKey(null, KladerCountries::tableName(), 'partner_form_id', PartnerForm::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, KladerCountries::tableName(), 'partner_form_attribute_id', PartnerFormAttribute::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(KladerCountries::tableName(), 'partner_form_id');
        $this->dropColumn(KladerCountries::tableName(), 'partner_form_attribute_id');

    }
}

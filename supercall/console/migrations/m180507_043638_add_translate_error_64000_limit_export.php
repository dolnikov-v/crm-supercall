<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180507_043638_add_translate_error_64000_limit_export
 */
class m180507_043638_add_translate_error_64000_limit_export extends Migration
{
    public $phrases = [
        'Установлен лимит в 64 000 строки при экспорте в Excel. Для экспорта большего кол-ва строк используйте формат CSV' => 'A limit of 64,000 lines is set when exporting to Excel. To export a larger number of rows, use the CSV format',
        '' => '',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

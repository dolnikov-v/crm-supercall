<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171116_055436_tranlate_order_types_on_filter
 */
class m171116_055436_tranlate_order_types_on_filter extends Migration
{
    public $phrases = [
        'Новый' => 'New',
        'Доставка' => 'Delivery',
        'Невыкуп' => 'Non-repurchase',
        'Созданный' => 'Created',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

    }
}

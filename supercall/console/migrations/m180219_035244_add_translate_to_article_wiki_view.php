<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180219_035244_add_translate_to_article_wiki_view
 */
class m180219_035244_add_translate_to_article_wiki_view extends Migration
{
    public $phrases = [
        'Список статей' => 'List of articles',
        'Изменить' => 'Edit',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

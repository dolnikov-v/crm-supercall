<?php
use console\components\db\Migration;
use common\models\User;

/**
 * Class m171019_084417_add_permission_for_applicants
 */
class m171019_084417_add_permission_for_applicants extends Migration
{
    public $permissions = [
        'operator.request.confirm',
        'coperator.request.reject',
        'operator.request.unblock',
        'operator.request.index',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach($this->permissions as $permission) {
            $this->insert('{{%auth_item}}', array(
                'name' => $permission,
                'type' => '2',
                'description' => $permission,
                'created_at' => time(),
                'updated_at' => time()
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_CURATOR,
                'child' => $permission
            ));
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->permissions as $permission){
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_CURATOR]);
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }
    }
}

<?php
use console\components\db\Migration;
use common\modules\order\models\OrderType;

/**
 * Class m171222_091850_add_order_type_check_address
 */
class m171222_091850_add_order_type_check_address extends Migration
{
    public $statuses = [
        OrderType::TYPE_CHECK_ADDRESS => 'Проверка адреса'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->statuses as $type => $name) {
            $this->insert(OrderType::tableName(), ['name' => $name, 'created_at' => time(), 'updated_at' => time()]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        OrderType::deleteAll(['>', 'id', 5]);
    }
}

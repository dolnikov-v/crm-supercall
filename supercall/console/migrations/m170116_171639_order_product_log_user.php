<?php
use console\components\db\Migration;

/**
 * Class m170116_171639_order_product_log_user
 */
class m170116_171639_order_product_log_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_product_log}}', 'user_id', $this->integer());
        $this->addColumn('{{%order_product_log}}', 'group_id', $this->string());
        $this->addForeignKey('fk__order_product_log__user', '{{%order_product_log}}', 'user_id', '{{%user}}', 'id', 'set null', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk__order_product_log__user', '{{%order_product_log}}');
        $this->dropColumn('{{%order_product_log}}', 'user_id');
        $this->dropColumn('{{%order_product_log}}', 'group_id');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170419_235637_order_olap_trash
 */
class m170419_235637_order_olap_trash extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_olap_trash}}', [
            'period_type' => $this->smallInteger(1)->notNull(),
            'period' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),

            'wrong_number' => $this->integer()->notNull() . ' DEFAULT 0',
            'double' => $this->integer()->notNull() . ' DEFAULT 0',
            'unaware' => $this->integer()->notNull() . ' DEFAULT 0',
            'consulting' => $this->integer()->notNull() . ' DEFAULT 0',
            'competitor' => $this->integer()->notNull() . ' DEFAULT 0',
            'return' => $this->integer()->notNull() . ' DEFAULT 0',
            'denied' => $this->integer()->notNull() . ' DEFAULT 0',
            'quantity' => $this->integer()->notNull() . ' DEFAULT 0',
        ]);

        $this->addForeignKey(
            'fk_oot_product',
            '{{%order_olap_trash}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_oot_type',
            '{{%order_olap_trash}}',
            'type_id',
            '{{%order_type}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_oot_partner',
            '{{%order_olap_trash}}',
            'partner_id',
            '{{%partner}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_oot_country',
            '{{%order_olap_trash}}',
            'country_id',
            '{{%country}}',
            'id',
            'CASCADE'
        );

        $this->addPrimaryKey('pk_oot', '{{%order_olap_trash}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_olap_trash}}');
    }
}

<?php
use common\models\User;
use common\models\UserWorkTime;
use console\components\db\Migration;

/**
 * Class m180727_105138_user_work_time
 */
class m180727_105138_user_work_time extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(UserWorkTime::tableName(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'time' => $this->integer()->defaultValue(0),
            'date' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->createIndex('user_work_time_user_id', UserWorkTime::tableName(), 'user_id');
        $this->addForeignKey(null, UserWorkTime::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(UserWorkTime::tableName());
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170511_163905_order_product_fixed_price
 */
class m170511_163905_order_product_fixed_price extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_product}}', 'fixed_price', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order_product}}', 'fixed_price');
    }
}

<?php

use backend\modules\queue\models\Queue;
use common\modules\order\models\Order;
use console\components\db\Migration;

/**
 * Class m180609_095445_add_column_prevent_queue_id_for_order
 */
class m180609_095445_add_column_prevent_queue_id_for_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'prevent_queue_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(null, Order::tableName(), 'prevent_queue_id', Queue::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'prevent_queue_id');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m180920_084416_remove_fk_from_prevent_queue_id_table_order
 */
class m180920_084416_remove_fk_from_prevent_queue_id_table_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey($this->getFkName('{{%order}}', 'prevent_queue_id'), '{{%order}}');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        //не трабуется
    }
}

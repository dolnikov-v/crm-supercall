<?php
use console\components\db\Migration;
use backend\modules\widget\models\WidgetRole;
use backend\modules\widget\models\WidgetType;
use common\models\User;
use backend\models\i18n_source_message;

/**
 * Class m171114_052134_add_widget_stat_queue_order
 */
class m171114_052134_add_widget_stat_queue_order extends Migration
{
    public $phrases = [
        'Статистика очередей' => 'Queue Statistics',
        'Всего' => 'Total',
        'В обработке' => 'In processing'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'stat-queue-order',
            'name' => 'Статистика очередей',
            'status' => WidgetType::STATUS_ACTIVE,
            'no_cache' => WidgetType::NO_CACHE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'stat-queue-order'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => User::ROLE_CURATOR,
            'type_id' => $type->id
        ]);

        $this->insert(WidgetRole::tableName(), [
            'role' => User::ROLE_CONTROLLER,
            'type_id' => $type->id
        ]);

        $this->insert(WidgetRole::tableName(), [
            'role' => User::ROLE_SUPERVISOR,
            'type_id' => $type->id
        ]);

        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'stat-queue-order'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => User::ROLE_CURATOR,
            'type_id' => $type->id
        ]);

        $this->delete(WidgetRole::tableName(), [
            'role' => User::ROLE_CONTROLLER,
            'type_id' => $type->id
        ]);

        $this->delete(WidgetRole::tableName(), [
            'role' => User::ROLE_SUPERVISOR,
            'type_id' => $type->id
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'stat-queue-order']);

        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use common\modules\order\models\OrderType;
use console\components\db\Migration;

/**
 * Class m181211_113601_add_order_type_information
 */
class m181211_113601_add_order_type_information extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%order_type}}', ['id' => OrderType::TYPE_INFORMATION, 'name' => OrderType::TYPE_CC_INFORMATION, 'created_at' => time(), 'updated_at' => time()]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $typeInformation = OrderType::findOne(['name' => OrderType::TYPE_CC_INFORMATION]);

        if($typeInformation){
            $typeInformation->delete();
        }
    }
}

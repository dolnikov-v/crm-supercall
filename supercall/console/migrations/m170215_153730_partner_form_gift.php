<?php
use console\components\db\Migration;

/**
 * Class m170215_153730_partner_form_gift
 */
class m170215_153730_partner_form_gift extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_form}}', 'allow_gifts', $this->boolean());
        $this->addColumn('{{%order_product}}', 'gift', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_form}}', 'allow_gifts');
        $this->dropColumn('{{%order_product}}', 'gift');
    }
}

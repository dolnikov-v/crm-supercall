<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use common\modules\order\models\Order;
use common\modules\order\models\OrderSubStatus;
use common\modules\order\models\OrderSubStatusGroup;
use console\components\db\Migration;

/**
 * Class m181015_180910_add_new_trash_sub_status_other_reason
 */
class m181015_180910_add_new_trash_sub_status_other_reason extends Migration
{

    public $phrases = [
        'Другая причина' => 'Other reason'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $groupReject = OrderSubStatusGroup::findOne(['id' => Order::SUB_STATUS_GROUP_TRASH]);

        $this->insert(OrderSubStatus::tableName(), [
            'code' => Order::SUB_STATUS_TRASH_REASON_OTHER_REASON,
            'group_id' => $groupReject->id,
            'name' => 'Другая причина',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}

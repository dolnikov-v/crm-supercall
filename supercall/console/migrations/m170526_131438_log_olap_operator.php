<?php
use console\components\db\Migration;

/**
 * Class m170526_131438_log_olap_operator
 */
class m170526_131438_log_olap_operator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_log_olap_general}}', 'operator', $this->string()->notNull()->defaultValue('')->notNull()->defaultValue(''));
        $this->dropPrimaryKey('pk_olog', '{{%order_log_olap_general}}');
        $this->addPrimaryKey('pk_olog', '{{%order_log_olap_general}}', [
            'period_type',
            'country_id',
            'type_id',
            'product_id',
            'partner_id',
            'status',
            'period',
            'operator',
        ]);

        $this->addColumn('{{%order_log_olap_approved}}', 'operator', $this->string()->notNull()->defaultValue(''));
        $this->dropPrimaryKey('pk_oloap', '{{%order_log_olap_approved}}');
        $this->addPrimaryKey('pk_oloap', '{{%order_log_olap_approved}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
            'operator',
        ]);

        $this->addColumn('{{%order_log_olap_reject}}', 'operator', $this->string()->notNull()->defaultValue(''));
        $this->dropPrimaryKey('pk_olor', '{{%order_log_olap_reject}}');
        $this->addPrimaryKey('pk_olor', '{{%order_log_olap_reject}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
            'operator',
        ]);

        $this->addColumn('{{%order_log_olap_trash}}', 'operator', $this->string()->notNull()->defaultValue(''));
        $this->dropPrimaryKey('pk_olot', '{{%order_log_olap_trash}}');
        $this->addPrimaryKey('pk_olot', '{{%order_log_olap_trash}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
            'operator',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order_log_olap_general}}', 'operator');
        $this->dropPrimaryKey('pk_olog', '{{%order_log_olap_general}}');
        $this->addPrimaryKey('pk_olog', '{{%order_log_olap_general}}', [
            'period_type',
            'country_id',
            'type_id',
            'product_id',
            'partner_id',
            'status',
            'period',
        ]);

        $this->dropColumn('{{%order_log_olap_approved}}', 'operator');
        $this->dropPrimaryKey('pk_oloap', '{{%order_log_olap_approved}}');
        $this->addPrimaryKey('pk_oloap', '{{%order_log_olap_approved}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
        ]);

        $this->dropColumn('{{%order_log_olap_reject}}', 'operator');
        $this->dropPrimaryKey('pk_olor', '{{%order_log_olap_reject}}');
        $this->addPrimaryKey('pk_olor', '{{%order_log_olap_reject}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
        ]);

        $this->dropColumn('{{%order_log_olap_trash}}', 'operator');
        $this->dropPrimaryKey('pk_olot', '{{%order_log_olap_trash}}');
        $this->addPrimaryKey('pk_olot', '{{%order_log_olap_trash}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
        ]);
    }
}

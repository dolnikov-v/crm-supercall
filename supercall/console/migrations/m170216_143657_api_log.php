<?php
use console\components\db\Migration;

/**
 * Class m170216_143657_api_order_create_log
 */
class m170216_143657_api_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%api_log}}', [
            'id' => $this->primaryKey(),
            'url' => $this->string(),
            'auth' => $this->text(),
            'request' => $this->text(),
            'response' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%api_log}}');
    }
}

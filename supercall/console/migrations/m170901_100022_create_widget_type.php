<?php
use console\components\db\Migration;
use backend\modules\widget\models\WidgetType;

/**
 * Class m170901_100022_create_widget_type
 */
class m170901_100022_create_widget_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(WidgetType::tableName(), [
            'id' => $this->primaryKey(),
            'code' => $this->string(30)->unique(),
            'name' => $this->string(255),
            'status' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->createIndex(null, 'widget_type', 'code');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(WidgetType::tableName());
    }

}

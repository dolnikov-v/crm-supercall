<?php
use console\components\db\Migration;

/**
 * Class m170306_134412_partner_product_price_country
 */
class m170306_134412_partner_product_price_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('insert into "partner_product_price" (select nextval(\'partner_product_price_id_seq\'), id, 13, price_data, created_at, null from partner_product where partner_id = 2)');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}

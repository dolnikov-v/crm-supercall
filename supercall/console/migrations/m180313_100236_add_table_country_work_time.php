<?php

use common\models\Country;
use common\models\CountryWorkTime;
use common\modules\partner\models\Partner;
use console\components\db\Migration;

/**
 * Class m180313_100236_add_table_country_work_time
 */
class m180313_100236_add_table_country_work_time extends Migration
{
    public function safeUp()
    {
        $this->createTable(CountryWorkTime::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer(),
            'partner_id' => $this->integer(),
            'begin_time_hour' => $this->integer(),
            'begin_time_minute' => $this->integer(),
            'end_time_hour' => $this->integer(),
            'end_time_minute' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk-country_work_time_country_id', CountryWorkTime::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::NO_ACTION);
        $this->addForeignKey('fk-country_work_time_partner_id', CountryWorkTime::tableName(), 'partner_id', Partner::tableName(), 'id', self::CASCADE, self::NO_ACTION);

    }
    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-country_work_time_country_id', CountryWorkTime::tableName());
        $this->dropForeignKey('fk-country_work_time_partner_id', CountryWorkTime::tableName());

        $this->dropTable(CountryWorkTime::tableName());
    }
}

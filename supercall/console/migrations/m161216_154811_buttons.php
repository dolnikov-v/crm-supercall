<?php
use console\components\db\Migration;

/**
 * Class m161216_154811_buttons
 */
class m161216_154811_buttons extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'reject_reason', $this->string());
        $this->addColumn('{{%order}}', 'recall_date', $this->date());

        $this->addColumn('{{%partner_form}}', 'buttons', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_form}}', 'buttons');

        $this->dropColumn('{{%order}}', 'recall_date');
        $this->dropColumn('{{%order}}', 'reject_reason');
    }
}

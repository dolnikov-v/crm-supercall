<?php
use console\components\db\Migration;

/**
 * Class m161229_135715_order_customer_email
 */
class m161229_135715_order_customer_email extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'customer_email', $this->string(200));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'customer_email');
    }
}

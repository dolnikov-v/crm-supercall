<?php
use console\components\db\Migration;
use common\models\CountryPhoneCode;

/**
 * Class m180330_114341_add_columncheck_phone_to_country_phone_code
 */
class m180330_114341_add_columncheck_phone_to_country_phone_code extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CountryPhoneCode::tableName(), 'check_phone', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CountryPhoneCode::tableName(), 'check_phone');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m180920_094648_add_status_to_transcribe_stop_set
 */
class m180920_094648_add_status_to_transcribe_stop_set extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%transcribe_stop_set}}', 'status', $this->integer()->after('transcribe_setting_id'));
        $this->createIndex(null, '{{%transcribe_stop_set}}', ['status']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%transcribe_stop_set}}', 'status');
    }
}

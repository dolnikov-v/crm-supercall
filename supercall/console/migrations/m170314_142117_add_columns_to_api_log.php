<?php
use console\components\db\Migration;

/**
 * Class m170314_142117_add_columns_to_api_log
 */
class m170314_142117_add_columns_to_api_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%api_log}}', 'operator', $this->string()->null());
        $this->addColumn('{{%api_log}}', 'order_id', $this->integer()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%api_log}}', 'order_id');
        $this->dropColumn('{{%api_log}}', 'operator');
    }
}

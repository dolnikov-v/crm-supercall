<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m180313_085548_add_translate_to_logger
 */
class m180313_085548_add_translate_to_logger extends Migration
{
    public $phrases = [
        'Поля' => 'Fields',
        'Новые поля' => 'New fields',
    ];
    
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

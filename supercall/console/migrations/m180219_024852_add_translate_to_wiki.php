<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180219_024852_add_translate_to_wiki
 */
class m180219_024852_add_translate_to_wiki extends Migration
{
    public $phrases = [
        'Wiki категории' => 'Wiki category',
        'Заголовок' => 'Title',
        'Добавление категории' => 'Add category',
        'Текст статьи' => 'Article text',
        'Сохранить категорию' => 'Save category',
        ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

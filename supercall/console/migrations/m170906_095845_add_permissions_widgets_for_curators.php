<?php
use console\components\db\Migration;
use common\models\User;

/**
 * Class m170906_095845_add_permissions_widgets_for_curators
 */
class m170906_095845_add_permissions_widgets_for_curators extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', [
            'name'=> 'widget.index.add',
            'type' => '2',
            'description' => 'widget.index.add',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('{{%auth_item}}', [
            'name'=> 'widget.index.delete',
            'type' => '2',
            'description' => 'widget.index.delete',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert('{{%auth_item}}', [
            'name'=> 'widget.index.changeplace',
            'type' => '2',
            'description' => 'widget.index.changeplace',
            'created_at' => time(),
            'updated_at' => time()
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => User::ROLE_CURATOR,
            'child' => 'widget.index.add'
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => User::ROLE_CURATOR,
            'child' => 'widget.index.delete'
        ]);

        $this->insert($this->authManager->itemChildTable, [
            'parent' => User::ROLE_CURATOR,
            'child' => 'widget.index.changeplace'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, [
            'parent' => User::ROLE_CURATOR,
            'child' => 'widget.index.add'
        ]);

        $this->delete($this->authManager->itemChildTable, [
            'parent' => User::ROLE_CURATOR,
            'child' => 'widget.index.delete'
        ]);

        $this->delete($this->authManager->itemChildTable, [
            'parent' => User::ROLE_CURATOR,
            'child' => 'widget.index.changeplace'
        ]);


        $this->delete('{{%auth_item}}', ['name' => 'widget.index.add']);
        $this->delete('{{%auth_item}}', ['name' => 'widget.index.delete']);
        $this->delete('{{%auth_item}}', ['name' => 'widget.index.changeplace']);
    }
}

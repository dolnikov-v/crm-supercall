<?php

use backend\models\i18n_source_message;
use backend\modules\operator\models\ZingTree;
use backend\modules\operator\models\ZingTreeOrderType;
use common\modules\order\models\OrderType;
use console\components\db\Migration;

/**
 * Class m180615_063532_add_zing_tree_order_type_table
 */
class m180615_063532_add_zing_tree_order_type_table extends Migration
{
    public $phrases = [
        'Тип заказа' => 'Order type',
        'Если не будет указан не один тип заказа, значение тип заказа будет "Новый"' => 'If no more than one order type is specified, the value of the order type is "New"',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        $this->createTable(ZingTreeOrderType::tableName(), [
            'id' => $this->primaryKey(),
            'order_type_id' => $this->integer()->notNull(),
            'zing_tree_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, ZingTreeOrderType::tableName(), 'order_type_id', OrderType::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, ZingTreeOrderType::tableName(), 'zing_tree_id', ZingTree::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
        $this->dropTable(ZingTreeOrderType::tableName());
    }
}

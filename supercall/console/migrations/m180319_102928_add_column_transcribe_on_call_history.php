<?php

use backend\models\i18n_source_message;
use common\modules\call\models\CallHistory;
use console\components\db\Migration;

/**
 * Class m180319_102928_add_column_transcribe_on_call_history
 */
class m180319_102928_add_column_transcribe_on_call_history extends Migration
{
    public $phrases = [
        'Распознование речи' => 'Speech recognition'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallHistory::tableName(), 'transcribe', $this->text());
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallHistory::tableName(), 'transcribe');
        i18n_source_message::removeTranslate($this->phrases);
    }
}

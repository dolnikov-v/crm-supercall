<?php
use console\components\db\Migration;
use common\modules\partner\models\Partner;

/**
 * Class m170717_054335_add_column_queue_url_to_partner
 */
class m170717_054335_add_column_queue_url_to_partner extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Partner::tableName(), 'url_queue', $this->string()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Partner::tableName(), 'url_queue');
    }
}

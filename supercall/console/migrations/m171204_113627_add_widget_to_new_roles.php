<?php
use common\models\User;
use console\components\db\Migration;
use backend\modules\widget\models\WidgetType;

/**
 * Class m171204_113627_add_widget_to_new_roles
 */
class m171204_113627_add_widget_to_new_roles extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $types = WidgetType::find()
            ->where(['code' => [
                "main-orders",
                "main-calls",
                "operators-online",
                "stat-queue-order"
            ]])
            ->asArray()
            ->all();

        foreach($types as $type){
            $this->insert('{{%widget_role}}', [
                'role' => User::ROLE_SENIOR_OPERATOR,
                'type_id' => $type['id'],
            ]);

            $this->insert('{{%widget_role}}', [
                'role' => User::ROLE_TEAM_LEAD,
                'type_id' => $type['id'],
            ]);

        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $types = WidgetType::find()
            ->where(['code' => [
                "main-orders",
                "main-calls",
                "operators-online",
                "stat-queue-order"
            ]])
            ->asArray()
            ->all();

        foreach($types as $type) {
            $this->delete('{{%widget_role}}', [
                'role' => User::ROLE_SENIOR_OPERATOR,
                'type_id' => $type['id'],
            ]);

            $this->delete('{{%widget_role}}', [
                'role' => User::ROLE_TEAM_LEAD,
                'type_id' => $type['id'],
            ]);
        }
    }
}

<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180206_034130_add_translate_to_mask_phones_partner_phorm
 */
class m180206_034130_add_translate_to_mask_phones_partner_phorm extends Migration
{
    public $phrases = [
        'Скрывать телефоны' => 'Mask the phones'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

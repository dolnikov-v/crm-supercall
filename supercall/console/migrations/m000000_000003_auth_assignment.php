<?php
use console\components\db\Migration;

/**
 * Class m000000_000003_auth_assignment
 */
class m000000_000003_auth_assignment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%auth_assignment}}', [
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->string(64)->notNull(),
            'created_at' => $this->integer()->notNull(),
            $this->includePrimaryKey(['item_name', 'user_id']),
        ]);

        $this->addForeignKey(null, $this->authManager->assignmentTable, 'item_name', $this->authManager->itemTable, 'name', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%auth_assignment}}');
    }
}

<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180822_032159_add_translate_to_downloads
 */
class m180822_032159_add_translate_to_downloads extends Migration
{
    public $phrases = [
        'Задание на экспорт успешно удалено' => 'Export task was successfully deleted',
        'Ошибка передачи списка заказов для экспорта записей' => 'Error sending order list for exporting records',
        'Экспорт записей разговоров' => 'Export call records',
        'Некорректный параметр $place: {place}' => 'Incorrect parameter $place: {place}',
        'Вы действительно собираетесь выполнить данную операцию по экспорту записей разговоров?' => 'Are you really going to perform this operation on exporting conversation records?',
        'Таблица с загрузками' => 'Table with downloads',
        'Ссылка	' => 'Link',
        'Ожидает обработку' => 'ready',
        'В обработке' => 'in progress',
        'Готов' => 'completed',
        'Ошибка' => 'Error',
        'По ID' => 'By ID',
        'По запросу' => 'By query',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m180313_030515_call_history_group_id
 */
class m180313_030515_call_history_group_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%call_history}}', 'group_id', $this->string(255)->after('order_id'));
        $this->addColumn('{{%call_history}}', 'user_sip', $this->string(255));
        $this->createIndex($this->getIdxName('{{%call_history}}', ['order_id', 'group_id']), '{{%call_history}}', ['order_id', 'group_id']);
        $this->createIndex($this->getIdxName('{{%call_history}}', ['user_sip', 'phone']), '{{%call_history}}', ['user_sip', 'phone']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->getIdxName('{{%call_history}}', ['order_id', 'group_id']), '{{%call_history}}');
        $this->dropIndex($this->getIdxName('{{%call_history}}', ['user_sip', 'phone']), '{{%call_history}}');
        $this->dropColumn('{{%call_history}}', 'group_id');
        $this->dropColumn('{{%call_history}}', 'user_sip');
    }
}

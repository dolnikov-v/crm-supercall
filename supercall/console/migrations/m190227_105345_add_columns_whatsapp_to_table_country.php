<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use console\components\db\Migration;

/**
 * Class m190227_105345_add_columns_whatsapp_to_table_country
 */
class m190227_105345_add_columns_whatsapp_to_table_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%country}}', 'whatsapp_number', $this->string());
        $this->addColumn('{{%country}}', 'whatsapp_url', $this->string());
        $this->addColumn('{{%country}}', 'whatsapp_token', $this->string());
        $this->addColumn('{{%country}}', 'whatsapp_phone', $this->string());
        $this->addColumn('{{%country}}', 'whatsapp_description', $this->string());
        $this->addColumn('{{%country}}', 'whatsapp_message_template', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%country}}', 'whatsapp_number');
        $this->dropColumn('{{%country}}', 'whatsapp_url');
        $this->dropColumn('{{%country}}', 'whatsapp_token');
        $this->dropColumn('{{%country}}', 'whatsapp_phone');
        $this->dropColumn('{{%country}}', 'whatsapp_description');
        $this->dropColumn('{{%country}}', 'whatsapp_message_template');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m161227_093218_vat
 */
class m161227_093218_vat extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_vat}}', [
            'id' => $this->primaryKey(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'value' => $this->double()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addForeignKey('fk__partner_vat__partner', '{{%partner_vat}}', 'partner_id', '{{%partner}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk__partner_vat__country', '{{%partner_vat}}', 'country_id', '{{%country}}', 'id', 'restrict', 'cascade');

        $this->createTable('{{%partner_product_vat}}', [
            'id' => $this->primaryKey(),
            'partner_product_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'value' => $this->double()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addForeignKey('fk__partner_product_vat__partner_product',
            '{{%partner_product_vat}}', 'partner_product_id',
            '{{%partner_product}}', 'id',
            'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_product_vat}}');
        $this->dropTable('{{%partner_vat}}');
    }
}

<?php
use console\components\db\Migration;

use backend\models\i18n_source_message;

/**
 * Class m180112_102412_add_translate_report_filters
 */
class m180112_102412_add_translate_report_filters extends Migration
{
    public $phrases = [
        'Дата создания у партнёра' => 'Date created from the partner',
        'Игнорировать' => 'Ignore',
        'Учитывать часовой пояс' => 'Include time zone',
        'Дата создания в CRM' => 'Creation date in CRM',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

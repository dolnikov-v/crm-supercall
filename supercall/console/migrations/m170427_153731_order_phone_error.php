<?php
use console\components\db\Migration;

/**
 * Class m170427_153731_order_phone_error
 */
class m170427_153731_order_phone_error extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'phone_error', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'phone_error');
    }
}

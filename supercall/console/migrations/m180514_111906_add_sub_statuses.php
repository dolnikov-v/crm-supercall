<?php

use common\modules\order\models\Order;
use common\modules\order\models\OrderSubStatus;
use common\modules\order\models\OrderSubStatusGroup;
use console\components\db\Migration;

/**
 * Class m180514_111906_add_sub_statuses
 */
class m180514_111906_add_sub_statuses extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $groupBuyOut = OrderSubStatusGroup::findOne(['id' => Order::SUB_STATUS_GROUP_BUYOUT]);
        $groupReject = OrderSubStatusGroup::findOne(['id' => Order::SUB_STATUS_GROUP_REJECT]);

        if($groupBuyOut){
            $this->insert(OrderSubStatus::tableName(), [
                'code' => Order::SUB_STATUS_REJECT_STILL_WAITING,
                'group_id' => $groupBuyOut->id,
                'name' => 'Ожидает заказ',
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }

        if($groupReject){
            $this->insert(OrderSubStatus::tableName(), [
                'code' => Order::SUB_STATUS_ALREADY_RECEIVED,
                'group_id' => $groupReject->id,
                'name' => 'Заказ доставлен',
                'created_at' => time(),
                'updated_at' => time()
            ]);

            $this->insert(OrderSubStatus::tableName(), [
                'code' => Order::SUB_STATUS_REJECT_NO_LONGER_NEEDED,
                'group_id' => $groupReject->id,
                'name' => 'Клиент отказался',
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(OrderSubStatus::tableName(), [
            'code' => Order::SUB_STATUS_REJECT_STILL_WAITING
        ]);

        $this->delete(OrderSubStatus::tableName(), [
            'code' => Order::SUB_STATUS_REJECT_NO_LONGER_NEEDED
        ]);

        $this->delete(OrderSubStatus::tableName(), [
            'code' => Order::SUB_STATUS_ALREADY_RECEIVED
        ]);
    }
}

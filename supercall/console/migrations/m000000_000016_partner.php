<?php
use console\components\db\Migration;

/**
 * Class m000000_000016_partner
 */
class m000000_000016_partner extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%partner}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'active' => $this->smallInteger()->notNull(),
            'api_key' => $this->string()->defaultValue(null),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, '{{%order}}', 'partner_id', '{{%partner}}', 'id', self::RESTRICT, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('{{%order}}', 'partner_id'), '{{%order}}');

        $this->dropTable('{{%partner}}');
    }
}

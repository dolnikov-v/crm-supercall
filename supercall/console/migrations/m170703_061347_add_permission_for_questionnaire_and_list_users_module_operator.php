<?php
use console\components\db\Migration;
use common\models\User;

/**
 * Class m170703_061347_add_permission_for_questionnaire_and_list_users_module_operator
 */
class m170703_061347_add_permission_for_questionnaire_and_list_users_module_operator extends Migration
{
    public $permissions = [
        'operator.questionnaire.index',
        'operator.questionnaire.orders',
        'operator.questionnaire.finishoff',
        'operator.questionnaire.addbase',
        'operator.questionnaire.cancelcompletion',
        'operator.questionnaire.evaluation',
        'operator.listoperators.index',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {
            $this->insert('{{%auth_item}}', array(
                'name' => $permission,
                'type' => '2',
                'description' => $permission,
                'created_at' => time(),
                'updated_at' => time()
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'curator',
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_CONTROLLER,
                'child' => $permission
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => 'curator']);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_CONTROLLER]);

            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }
    }
}

<?php
use backend\modules\order\models\AddressVerification;
use common\modules\order\models\Order;
use console\components\db\Migration;

/**
 * Class m180116_043106_add_column_confirm_address_to_order_table
 */
class m180116_043106_add_column_confirm_address_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'confirm_address', $this->integer()->defaultValue(AddressVerification::ADDRESS_CONFIRM_DEFAULT));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'confirm_address');
    }
}

<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;
use common\models\User;

/**
 * Class m180215_025704_add_roles_sales_director_quality_manager
 */
class m180215_025704_add_roles_sales_director_quality_manager extends Migration
{
    public $phrases = [
        'Директор продаж' => 'Sales director',
        'Менеджер качества' => 'Quality manager',
        'Операторы / Контроль качества' => 'Operators / Quality Control',
    ];

    public $permissions = [
        "operator.qualitycontrol.index",
        "home.index.index",
        "order.addressverification.index",
        "order.addressverification.confirm",
        "order.addressverification.edit",
        "operator.questionnaire.index",
        "operator.questionnaire.orders",
        "operator.questionnaire.finishoff",
        "operator.questionnaire.addbase",
        "operator.questionnaire.cancelcompletion",
        "operator.questionnaire.evaluation",
        "operator.questionnairemanagement.index",
        "wiki.default.index",
        "wiki.category.index",
        "wiki.article.index",
        "wiki.category.edit",
        "wiki.category.delete",
        "wiki.article.edit",
        "wiki.article.delete",
        "profile.control",
        "stats.order.index",
        "stats.log.general",
        "stats.log.list",
        "stats.log.details",
        "stats.orderprocessingtime.dates",
        "stats.orderprocessingtime.orders",
        "stats.orderprocessingtime.statuses",
        "stats.orderprocessingtime.products",
        "stats.product.index",
        "stats.operator.index",
    ];

    public $director = [
        "queue.control.clear",
        "queue.control.index",
        "order.change",
        "order.index",
        "order.change.productprice",
        "order.index.view",
        "order.index.index",
        "order.change.index",
        "order.change.addproduct",

    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => "operator.qualitycontrol.index",
            'type' => '2',
            'description' => "operator.qualitycontrol.index",
            'created_at' => time(),
            'updated_at' => time()
        ));

        $auth = $this->authManager;

        $role = $auth->createRole(User::ROLE_SALES_DIRECTOR);
        $role->description = 'Директор продаж';
        $auth->add($role);

        $role = $auth->createRole(User::ROLE_QUALITY_MANAGER);
        $role->description = 'Менеджер качества';
        $auth->add($role);

        foreach($this->permissions as $permission) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_SALES_DIRECTOR,
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_QUALITY_MANAGER,
                'child' => $permission
            ));
        }

        foreach($this->director as $director) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_SALES_DIRECTOR,
                'child' => $director
            ));
        }

        i18n_source_message::addTranslate($this->phrases, true);


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = $this->authManager;

        $role = $auth->getRole(USER::ROLE_SALES_DIRECTOR);
        $auth->remove($role);

        $role = $auth->getRole(USER::ROLE_QUALITY_MANAGER);
        $auth->remove($role);

        foreach($this->permissions as $permission){
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_SALES_DIRECTOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_QUALITY_MANAGER]);
        }

        foreach($this->director as $director){
            $this->delete($this->authManager->itemChildTable, ['child' => $director, 'parent' => User::ROLE_SALES_DIRECTOR]);
        }


        i18n_source_message::removeTranslate($this->phrases);

        $this->delete('{{%auth_item}}', ['name' => "operator.qualitycontrol.index"]);

    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170117_205417_order_trash_reason
 */
class m170117_205417_order_trash_reason extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'trash_reason', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'trash_reason');
    }
}

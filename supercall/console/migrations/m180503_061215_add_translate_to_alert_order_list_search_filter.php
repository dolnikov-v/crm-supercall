<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180503_061215_add_translate_to_alert_order_list_search_filter
 */
class m180503_061215_add_translate_to_alert_order_list_search_filter extends Migration
{
    public $phrases = [
        '<b>Внимание!</b><br/> При использовании фильтра <i>"Оператор"</i> совместно с фильтром <i>"Тип даты" = Дата обновления</i> в столбце статуса заказа будут отражены исторические данные (статусы заказов на период)<br/>Если совместно с фильтром <i>"Оператор"</i> будет использоваться фильтр <i>"Тип даты" = Дата создания в CRM</i>, то в столбце будут отражены текущие (актуальные) статусы заказов<br><br><b>Новое: </b>Поиск по полям "Number" и "Index by partner" теперь поддерживает массовый ввод. Используйте "," для разделения данных и "Enter" для ввода (например: 8236585, 1407507)'
        => '<b> Warning: </b> <br/> If you use the filter <i> "Operator" </i> together with the filter <i> "Date type" = Update date </i>, the historical order column will reflect historical data (status of orders for a period) <br/> If the filter <i> "Date type" = Creation date in CRM </i> is used in conjunction with the <i> "Operator" </i> filter, the column will reflect current (current) order statuses <br> <br> <b> New: </b> Search by fields "Number" and "Index by partner" now supports bulk input. Use "," to separate data and "Enter" to enter (for example: 8236585, 1407507)'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;
use common\modules\order\models\CountOrdersData;
use common\modules\order\models\CountOrdersOnPhone;

/**
 * Class m170817_093414_addIndexForCountOrdersOnPhone
 */
class m170817_093414_addIndexForCountOrdersOnPhone extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex($this->idxPrefix.'__count_orders_on_phone__phone', CountOrdersOnPhone::tableName(), 'phone');
        $this->createIndex($this->idxPrefix.'__count_orders_data__count_orders_on_phone_id__order_id', CountOrdersData::tableName(), ['count_orders_on_phone_id', 'order_id'], true);
        $this->createIndex($this->idxPrefix.'__count_orders_data__order_id', CountOrdersData::tableName(), 'order_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex($this->idxPrefix.'__count_orders_on_phone__phone', CountOrdersOnPhone::tableName());
        $this->dropIndex($this->idxPrefix.'__count_orders_data__count_orders_on_phone_id__order_id', CountOrdersData::tableName());
        $this->dropIndex($this->idxPrefix.'__count_orders_data__order_id', CountOrdersData::tableName());
    }
}

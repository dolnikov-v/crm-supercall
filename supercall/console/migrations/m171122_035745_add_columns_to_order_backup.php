<?php
use console\components\db\Migration;
use backend\modules\order\models\OrderBackup;

/**
 * Class m171122_035745_add_columns_to_order_backup
 */
class m171122_035745_add_columns_to_order_backup extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OrderBackup::tableName(), 'ordered_at', $this->integer());
        $this->addColumn(OrderBackup::tableName(), 'source_uri', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderBackup::tableName(), 'ordered_at');
        $this->dropColumn(OrderBackup::tableName(), 'source_uri');
    }
}

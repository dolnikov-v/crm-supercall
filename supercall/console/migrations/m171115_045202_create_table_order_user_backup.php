<?php
use console\components\db\Migration;
use backend\modules\order\models\OrderUserBackup;
use common\models\User;
use common\modules\order\models\Order;

/**
 * Class m171115_045202_create_table_order_user_backup
 */
class m171115_045202_create_table_order_user_backup extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(OrderUserBackup::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'url' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(null, OrderUserBackup::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, OrderUserBackup::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(OrderUserBackup::tableName());
    }
}

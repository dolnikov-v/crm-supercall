<?php
use console\components\db\Migration;

/**
 * Class m180904_113221_modify_column_transcribe_file_s3
 */
class m180904_113221_modify_column_transcribe_file_s3 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE {{%transcribe_queue}} ALTER COLUMN transcribe_file_s3 TYPE TEXT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('ALTER TABLE {{%transcribe_queue}} ALTER COLUMN transcribe_file_s3 TYPE VARCHAR(255)');
    }
}

<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use console\components\db\Migration;

/**
 * Class m190129_042042_change_column_validate_type
 */
class m190129_042042_change_column_validate_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn(\common\models\CountryPhoneCode::tableName(), 'validate_type', 'use_zorra_validate');
        
        yii::$app->db->createCommand('ALTER TABLE {{%country_phone_code}} ALTER COLUMN use_zorra_validate DROP DEFAULT')->execute();
        yii::$app->db->createCommand('ALTER TABLE {{%country_phone_code}} ALTER use_zorra_validate TYPE bool USING case when use_zorra_validate = 1 then false else true end')->execute();
        yii::$app->db->createCommand('ALTER TABLE {{%country_phone_code}} ALTER COLUMN use_zorra_validate SET DEFAULT false')->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn(\common\models\CountryPhoneCode::tableName(), 'use_zorra_validate', 'validate_type');

        yii::$app->db->createCommand('ALTER TABLE {{%country_phone_code}} ALTER COLUMN validate_type DROP DEFAULT')->execute();
        yii::$app->db->createCommand('ALTER TABLE {{%country_phone_code}} ALTER validate_type TYPE bool USING case when validate_type = false then 1 else 2 end')->execute();
        yii::$app->db->createCommand('ALTER TABLE {{%country_phone_code}} ALTER COLUMN validate_type SET DEFAULT 1')->execute();

    }
}

<?php

use common\models\User;
use console\components\db\Migration;

/**
 * Class m180410_053405_add_column_last_activity_to_user_table
 */
class m180410_053405_add_column_last_activity_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'last_activity', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(User::tableName(), 'last_activity');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170419_114635_alter_approve_components_jsonb
 */
class m170419_114635_alter_approve_components_jsonb extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('{{%order}}', 'approve_components');
        $this->execute('ALTER TABLE {{%order}} ADD approve_components JSONB;');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'approve_components');
        $this->addColumn('{{%order}}', 'approve_components', $this->text()->after('customer_components'));
    }
}

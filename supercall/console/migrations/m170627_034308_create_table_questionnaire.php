<?php
use console\components\db\Migration;
use backend\modules\operator\models\QuestionnaireManagement;

/**
 * Class m170627_034308_create_table_questionnaire
 */
class m170627_034308_create_table_questionnaire extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(QuestionnaireManagement::tableName(), [
            'id' => $this->primaryKey(),
            'question_text' => $this->text()->notNull(),
            'answers' => "JSON NOT NULL",
            'order_status' => "JSON NOT NULL", //вопрос может использоваться в нескольких статусах заказа
            'required' => $this->integer()->defaultValue(1),
            'sort' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(QuestionnaireManagement::tableName());
    }
}

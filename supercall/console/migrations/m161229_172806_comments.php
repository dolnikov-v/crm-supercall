<?php
use console\components\db\Migration;

/**
 * Class m161229_172806_comments
 */
class m161229_172806_comments extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('{{%order}}', 'comment');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('{{%order}}', 'comment', $this->text());
    }
}

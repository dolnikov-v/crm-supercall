<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180207_054642_rename_auth_item_description
 */
class m180207_054642_rename_auth_item_description extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        yii::$app->db->createCommand("update auth_item set description = :description_replace where description=:description_origin")
            ->bindValues([
                ':description_origin' => 'Очереди Управление Добавление очереди',
                ':description_replace' => 'Очереди / Управление'
            ])->execute();

        i18n_source_message::addTranslate(['Очереди / Управление' => 'Queues / Managing'], true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        yii::$app->db->createCommand("update {{%auth_item}} set description = :description_origin where description=:description_replace")
            ->bindValues([
                ':description_origin' =>'Очереди / Управление',
                ':description_replace' => 'Очереди Управление Добавление очереди'
            ])->execute();

        i18n_source_message::removeTranslate(['Очереди / Управление' => 'Queues / Managing']);
    }
}

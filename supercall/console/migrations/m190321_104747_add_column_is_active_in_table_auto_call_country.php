<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m190321_104747_add_column_is_active_in_table_auto_call_country
 */
class m190321_104747_add_column_is_active_in_table_auto_call_country extends Migration
{
    /**
     * @var array
     */
    public $phrases = [
        'Автодозвон стран' => 'Auto call by country',
        'Таблица с настройками автодозвона по странам' => 'Table with auto call settings by country',
        'Настройка автодозвона страны' => 'Setting auto call by country',
        'Добавить настройку автодозвона' => 'Add auto call setting',
        'Добавление настройки автодозвона страны' => 'Adding country auto call setting',
        'Сохранить настройку автодозвона' => 'Save auto call setting',
        'Настройка автодозвона страны не найдена.' => 'Country auto call setting not found.',
        'Настройка автодозвона успешно деактивирована.' => 'Auto call setting successfully deactivated.',
        'Настройка автодозвона успешно активирована.' => 'Auto call setting successfully activated.',
        'Настройка автодозвона страны успешно отредактирована.' => 'The country auto call setting has been successfully edited.',
        'Настройка автодозвона страны успешно добавлены' => 'Country auto call setting successfully added',
        'Редактирование настройки автодозвона страны' => 'Editing the country auto call setting',
        'Выбранная страна уже добавлена' => 'The selected country has already been added.',
        'Количество заказов на оператора' => 'Number of orders for operator',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
        $this->addPermission([
            'administration.autocallcountry.index' => 'Настройка автодозвона страны',
            'administration.autocallcountry.edit' => 'Редактирование автодозвона страны'
        ]);
        $this->addColumn('{{%auto_call_country}}', 'is_active', $this->boolean()->defaultValue(true));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
        $this->dropColumn('{{%auto_call_country}}', 'is_active');
        $this->deletePermission('administration.autocallcountry.index');
        $this->deletePermission('administration.autocallcountry.edit');
    }
}

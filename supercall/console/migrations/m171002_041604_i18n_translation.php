<?php
use console\components\db\Migration;
/**
 * Class m171002_041604_i18n_translation
 */
class m171002_041604_i18n_translation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $messages = (new \yii\db\Query())
            ->select(['id', 'message'])
            ->from('i18n_source_message')
            ->where(['category' => 'common'])
            ->all();
        $i = 1;
        foreach ($this->messages() as $source_message => $translation_message) {
            if($messages){
                $found = false;
                foreach ($messages as $messageInDb){
                    //Изем в источникам
                    if($messageInDb['message'] == $source_message){
                        $found = true;
                        //Изем по этом ИД в переводах
                        $messagesTranslateInDb = (new \yii\db\Query())
                            ->select(['id', 'translation'])
                            ->from('i18n_message')
                            ->where(['language' => 'en-US', 'id' => $messageInDb['id']])
                            ->one();
                        //Если не нашли то, добавляем
                        if(!$messagesTranslateInDb){
                            $this->insert('i18n_message', [
                                'id' => $messageInDb['id'],
                                'language' => 'en-US',
                                'translation' => $translation_message,
                                'created_at' => time(),
                                'updated_at' => time(),
                            ]);
                        }
                    }
                }
                if(!$found){
                    $this->insert('i18n_source_message', [
                        'category' => 'common',
                        'message' => $source_message,
                        'created_at' => time(),
                        'updated_at' => time(),
                    ]);

                    $lastId= (new \yii\db\Query())
                        ->select(['id'])
                        ->from('i18n_source_message')
                        ->where(['message' => $source_message])
                        ->one();

                    if($lastId['id'] > 0){
                        $this->insert('i18n_message', [
                            'id' => $lastId['id'],
                            'language' => 'en-US',
                            'translation' => $translation_message,
                            'created_at' => time(),
                            'updated_at' => time(),
                        ]);
                    }
                }
            }
            else{
                echo 'vot '.$i+4;
                $this->insert('i18n_source_message', [
                    'category' => 'common',
                    'message' => $source_message,
                    'created_at' => time(),
                    'updated_at' => time(),
                ]);

                if($this->db->lastInsertID > 0){
                    echo 'vot '.$i+5;
                    $this->insert('i18n_message', [
                        'id' => $this->db->lastInsertID,
                        'language' => 'en-US',
                        'translation' => $translation_message,
                        'created_at' => time(),
                        'updated_at' => time(),
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

    public function messages()
    {
        return [
            "Профиль" => "Profile",
            "Готов" => "Ready",
            "перейти в 'Неготов' после сохранения" => "set 'Unready' after save",
            "Мои перезвоны" => "My recalls",
            "Создать заказ" => "Create order",
            "Форма заказа" => "Form of order",
            "История заказа" => "History of Order",
            "Похожие заказы" => "Similar orders",
            "очередь" => "query",
            "Перечень заказов" => "List of orders",
            "Дата создания" => "Date of creation",
            "Информация о клиенте" => "Customer info",
            "Мобильный" => "Mobile phone",
            "Адрес" => "Address",
            "Адрес присланный клиентом" => "Address sent by the client",
            "Комментарий" => "Comment",
            "Телефоны" => "Telephones",
            "Добавить" => "Add",
            "Товары" => "Good's",
            "Итого" => "Total",
            "Одобрен" => "Approve",
            "Перезвон" => "Recall",
            "Недозвон" => "No answer",
            "Отклонен" => "Reject",
            "Треш" => "Trash",
            "Общее" => "Common",
            "Статусы" => "Statuses",
            "Продукты" => "Products",
            "Время онлайн" => "Online time",
            "Название" => "Name",
            "Номер у партнера" => "Index by partner",
            "Телефон" => "Telephone",
            "Дата изменения" => "Date of change",
            "Выход" => "Exit",
            "Ожидайте пожалуйста: мы ищем для вас свежий заказ" => "Please wait: we searching new order for you",
            "Имя пользователя" => "Login",
            "Пароль" => "Password",
            "Запомнить меня" => "Remember me",
            "Войти" => "Enter",
            "Панель управления" => "Control panel",
            "Все права защищены" => "All rights reserved",
            "Авторизация" => "Authorization",
            "Введите ваше имя пользователя" => "Input your login",
            "Введите ваш пароль" => "Input your password",
            "Просмотр" => "View",
            "Редактировать" => "Edit",
            "Фильтр поиска" => "Search filter",
            "Список заказов" => "Order's list",
            "Заказы" => "Orders",
            "Не готов" => "Unready",
            "Создание заказа" => "Create order",
            "Партнер" => "Partner",
            "Страна" => "Country",
            "Сохранить" => "Save",
            "Заказы для перезвона" => "Order's for recall",
            "ID заказа у партнера" => "Order id by partner",
            "Имя клиента" => "Client's name",
            "Действия" => "Actions",
            "Номер заказа" => "Order ID",
            "Итоговая цена" => "Total price",
            "Статус" => "Status",
            "Поле" => "Field",
            "Старое значение" => "Old value",
            "Новое значение" => "New value",
            "Старый статус" => "Old status",
            "Новый статус" => "New status",
            "Оператор" => "Operator",
            "Данные отсутствуют" => "No data",
            "Действие" => "Action",
            "Цена" => "Price",
            "Количество" => "Quantity",
            "Изменение #{number}" => "Change #{number}",
            "Данные заказа" => "Order info",
            "Комментарии" => "Comments",
            "Таски" => "Tasks",
            "Форма" => "Form",
            "Просмотр заказа" => "Order's view",
            "Дополнительные данные" => "Additional data",
            "Цена начальная" => "Start price",
            "Цена конечная" => "Finish price",
            "Цена доставки" => "Shipping price",
            "Дата повторного звонка" => "Date of recall",
            "Имя" => "First name",
            "Фамилия" => "Second name",
            "Отчество" => "Middle name",
            "Служба доставки" => "Delivery",
            "Очередь" => "Query",
            "Заказ" => "Order",
            "Стоимость" => "Cost",
            "Время" => "Time",
            "Список тасков" => "Task's list",
            "Таски отсутствуют" => "No tasks",
            "Оценка работы" => "Evaluation of work",
            "Планы оценки" => "Evaluation plans",
            "Операторы" => "Operators",
            "Список операторов" => "List of operators",
            "Планы оценки работы" => "Work evaluation plans",
            "Управление опросами операторов" => "Operator survey management",
            "Список пользователей" => "List of users",
            "Таблица с пользователями" => "User table",
            "Роли" => "Roles",
            "Дата добавления" => "Date added",
            "Управление товарами" => "Product Management",
            "Оценить работу" => "Rate work",
            "Статус заказа" => "Order status",
            "Продолжительность разговора" => "Duration of conversation",
            "Закончено" => "Finished",
            "Оценить" => "Rate",
            "Оценка" => "Evaluation",
            "Записи разговоров" => "Call records",
            "* - ответ на данный вопрос обязателен" => "* - the answer to this question is mandatory",
            "Продолжительность разговора:" => "Call duration:",
            "Вопрос:" => "Question:",
            "Ответ:" => "Answer:",
            "Да" => "Yes",
            "Нет" => "No",
            "Варианты ответoв" => "Variants of answers",
            "Ответ обязателен" => "The answer is mandatory",
            "Совершенно неприемлемо" => "Completely unacceptable",
            "Удовлетворительно" => "Satisfactorily",
            "Отлично" => "Excellent",
            "Удалить" => "Delete",
            "Создан" => "Created by",
            "Таблица с вопросами" => "Table with questions",
            "Обновлен" => "Updated",
            "Добавить вопрос" => "Add a question",
            "Контролер" => "Controller",
            "Добавление вопроса" => "Adding a question",
            "Вопрос" => "Question",
            "Тип ответа на вопрос" => "Type of answer to the question",
            "Произвольный ответ" => "Arbitrary answer",
            "Варианты ответов" => "Variants of answers",
            "Все доступные" => "All available",
            "Редактирование вопроса" => "Editing a question",
            "Завершить" => "Complete",
            "Перейти" => "Go to",
            "Вы ответили не на все вопросы" => "You did not answer all questions",
            "Неверное значение поля 'Продолжительность разговора'" => "Invalid value for the 'Call duration' field",
            "Ошибка сохранения ответа на вопрос '{question_text}'" => "Error saving answer to question '{question_text}'",
            "Некорректные данные в ответе вопроса '{question_text}'" => "Incorrect data in the question answer '{question_text}'",
            "Оценка по заказу #{order_id}" => "Order estimate #",
            "Да / Нет" => "Yes / No",
            "Да / Нет / Не определено" => "Yes / No / Undefined",
            "Свой вариант" => "Custom Option",
            "Введите варианты ответов, разделяя их запятыми или нажатием на клавишу ENTER" => "Type your answers, separated by commas or by pressing ENTER",
            "не определено" => "undefined",
            "Главная" => "Main page",
            "Партнерская программа" => "Partner program",
            "Вход" => "Login",
            "Добро пожаловать на" => "Welcome to",
            "Добавить товары" => "Add good's",
            "Подарки" => "Gifts",
            "Добавить подарки" => "add gifts",
            "Информация о доставке" => "Shipping's info",
            "с" => "from",
            "по" => "to",
            "Создано" => "Created",
            "Передумал" => "Change mind",
            "Без комментариев" => "No comments",
            "Автоматическое отклонение" => "Auto deviation",
            "Несуществующий номер" => "Wrong number",
            "Клиент ничего не знает о заказе" => "Client does not know anything about the order",
            "Заказано у конкурентов" => "Ordered from competitors",
            "Возврат" => "Return",
            "Консультация" => "Inquiry",
            "Не отправляем туда" => "Out of delivery zone",
            "Слишком дорого" => "Product is too expensive",
            "Медицинские противопоказания" => "Doctor recommendation",
            "Русский" => "Russian",
            "Английский" => "English",
            "Испанский" => "Spanish",
            "Французский" => "French",
            "Немецкий" => "German",
            "Словацкий" => "Slovak",
            "Румынский" => "Romanian",
            "Чешский" => "Czech",
            "Португальский" => "Portuguese",
            "Греческий" => "Greek",
            "Болгарский" => "Bulgarian",
            "Итальянский" => "Italian",
            "Китайский" => "Chinese",
            "Польский" => "Polish",
            "Казахский" => "Kazakh",
            "Венгерский" => "Hungarian",
            "Сохранить профиль" => "Save profile",
            "Отмена" => "Cancel",
            "Дубль" => "Duplicate order",
            "Не правильный формат телефона" => "Number is incorrect format",
            "Шуточный заказ" => "Joke order",
            "Не известный продукт" => "Product is unknown ",
            "Совершил покупку в другом магазине" => "Already bought it at another shop",
            "Плохие отзывы в интернете" => "Negative feedback from internet/friend",
            "Личная причина не хочет обсуждать" => "Personal reason, does not want discuss",
            "Не может позволить себе" => "Cannot afford it",
            "Невероятно" => "Unbelievable"
        ];
    }
}

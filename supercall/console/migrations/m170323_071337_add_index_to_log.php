<?php
use console\components\db\Migration;

/**
 * Class m170323_071337_add_index_to_log
 */
class m170323_071337_add_index_to_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('idx_order_log_order_id_comment', '{{%order_log}}', ['order_id', 'comment']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('idx_order_log_order_id_comment', '{{%order_log}}');
    }
}

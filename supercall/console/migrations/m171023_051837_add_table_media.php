<?php
use console\components\db\Migration;
use common\models\Media;
use common\models\User;

/**
 * Class m171023_051837_add_table_media
 */
class m171023_051837_add_table_media extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(Media::tableName(), [
            'id' => $this->primaryKey(),
            'filename' => $this->string()->notNull(),
            'filepath' => $this->string()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'active' => $this->boolean()->defaultValue(Media::ACTIVE),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('foreign_media_user_id_user_id', Media::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(Media::tableName());
    }
}

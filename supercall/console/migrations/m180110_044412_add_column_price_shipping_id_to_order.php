<?php
use console\components\db\Migration;
use common\modules\order\models\Order;

/**
 * Class m180110_044412_add_column_price_shipping_id_to_order
 */
class m180110_044412_add_column_price_shipping_id_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'price_shipping_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'price_shipping_id');
    }
}

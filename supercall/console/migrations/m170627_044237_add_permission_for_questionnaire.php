<?php
use console\components\db\Migration;

/**
 * Class m170627_044237_add_permission_for_questionnaire
 */
class m170627_044237_add_permission_for_questionnaire extends Migration
{
    public $permissions = [
        'catalog.questionnaire.index',
        'catalog.questionnaire.edit',
        'catalog.questionnaire.delete'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {
            $this->insert('{{%auth_item}}', array(
                'name' => $permission,
                'type' => '2',
                'description' => $permission,
                'created_at' => time(),
                'updated_at' => time()
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'curator',
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'superadmin',
                'child' => $permission
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => 'curator']);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => 'superadmin']);
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }
    }
}

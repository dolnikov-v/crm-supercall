<?php
use console\components\db\Migration;

/**
 * Class m170503_171505_add_column_diff_to_user_ready
 */
class m170503_171505_add_column_diff_to_user_ready extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_ready}}', 'diff', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_ready}}', 'diff');
    }
}

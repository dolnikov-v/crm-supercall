<?php

use backend\models\TaskQueue;
use console\components\db\Migration;

/**
 * Class m180828_054110_add_column_queue_to_task_queue
 */
class m180828_054110_add_column_queue_to_task_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(TaskQueue::tableName(), 'query', $this->text()->after('status')->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(TaskQueue::tableName(), 'query');
    }
}

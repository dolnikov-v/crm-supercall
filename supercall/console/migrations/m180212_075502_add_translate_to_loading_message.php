<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180212_075502_add_translate_to_loading_message
 */
class m180212_075502_add_translate_to_loading_message extends Migration
{
    public $phrases = [
        'Пожалуйста, подождите, идет загрузка страницы...' => 'Please wait while loading the page ...'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

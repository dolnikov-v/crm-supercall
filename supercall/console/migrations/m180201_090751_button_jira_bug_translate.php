<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180201_090751_button_jira_bug_translate
 */
class m180201_090751_button_jira_bug_translate extends Migration
{
    public $phrases = [
        'Нашли ошибку?' => 'Report a bug',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

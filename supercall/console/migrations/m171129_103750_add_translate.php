<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;


/**
 * Class m171129_103750_add_translate
 */
class m171129_103750_add_translate extends Migration
{
    public $phrases = [
        'Вы уверены, что хотите изменить статус на {status} ?' => 'Are you sure you want to change the status to {status}?',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;
use common\modules\order\models\asterisk\Records;
use backend\models\i18n_source_message;

/**
 * Class m171116_114742_add_columns_on_records
 */
class m171116_114742_add_columns_on_records extends Migration
{
    public $phrases = [
        'Время звонка',
        'Заказ'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        $this->addColumn(Records::tableName(), 'order_id', $this->integer()->notNull());
        $this->addColumn(Records::tableName(), 'user_id', $this->integer()->notNull());
        $this->addColumn(Records::tableName(), 'created_at', $this->integer());
        $this->addColumn(Records::tableName(), 'updated_at', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->dropColumn(Records::tableName(), 'order_id');
        $this->dropColumn(Records::tableName(), 'user_id');
        $this->dropColumn(Records::tableName(), 'created_at');
        $this->dropColumn(Records::tableName(), 'updated_at');
    }
}
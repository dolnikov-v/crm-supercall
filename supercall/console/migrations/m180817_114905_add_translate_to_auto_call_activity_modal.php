<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180817_114905_add_translate_to_auto_call_activity_modal
 */
class m180817_114905_add_translate_to_auto_call_activity_modal extends Migration
{
    public $phrases = [
        'Вы не проявляете активность продолжительный срок. Вы здесь ?' => 'You do not show activity for a long time. Are you here ?'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

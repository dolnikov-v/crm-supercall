<?php

use common\models\User;
use console\components\db\Migration;

/**
 * Class m180912_031134_add_permissions_to_roles_from_transcribe_settings
 */
class m180912_031134_add_permissions_to_roles_from_transcribe_settings extends Migration
{
    public $permissions = [
        'audit.transcribesetting.index',
        'audit.transcribesetting.edit',
        'audit.transcribesetting.manage',
        'audit.transcribesetting.delete',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_SUPERVISOR,
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_AUDITOR,
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_QUALITY_MANAGER,
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_CURATOR,
                'child' => $permission
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_SUPERVISOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_AUDITOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_QUALITY_MANAGER]);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_CURATOR]);
        }
    }
}

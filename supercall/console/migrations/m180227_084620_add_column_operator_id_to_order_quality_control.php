<?php
use backend\modules\operator\models\QualityControl;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180227_084620_add_column_operator_id_to_order_quality_control
 */
class m180227_084620_add_column_operator_id_to_order_quality_control extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(QualityControl::tableName(), 'operator_id', $this->integer());
        yii::$app->db->createCommand("update order_quality_control set operator_id = 22")->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(QualityControl::tableName(), 'operator_id');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170702_205559_alter_jsonb_to_fileds_order_logs
 */
class m170702_205559_alter_jsonb_to_fileds_order_logs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE order_logs ALTER COLUMN fields SET DATA TYPE jsonb USING fields::jsonb');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}

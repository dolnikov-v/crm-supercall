<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\modules\queue\models\Queue;
use console\components\db\Migration;

/**
 * Class m181102_043123_set_partner_for_secondary_queues
 */
class m181102_043123_set_partner_for_secondary_queues extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Queue::tableName(),'partner_id',$this->integer());
        $queues = Queue::updateAll(['partner_id' => 1], ['is_primary' => false]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $queues = Queue::updateAll(['partner_id' => null], ['is_primary' => false]);
        $this->dropColumn(Queue::tableName(),'partner_id');
    }
}

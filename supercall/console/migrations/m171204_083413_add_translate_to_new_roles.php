<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171204_083413_add_translate_to_new_roles
 */
class m171204_083413_add_translate_to_new_roles extends Migration
{
    public $phrases = [
        'Старший оператор' => 'Senior operator',
        'Тимлид' => 'Team lead',
        'Супервизор' => 'Supervisor',
        'Куратор' => 'Curator',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

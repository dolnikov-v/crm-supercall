<?php
use console\components\db\Migration;

/**
 * Class m000000_000015_order_product
 */
class m000000_000015_order_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_product}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'price' => $this->double()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, '{{%order_product}}', 'order_id', '{{%order}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%order_product}}', 'product_id', '{{%product}}', 'id', self::CASCADE, self::RESTRICT);

        $this->createIndex(null, '{{%order_product}}', 'order_id');
        $this->createIndex(null, '{{%order_product}}', 'product_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_product}}');
    }
}

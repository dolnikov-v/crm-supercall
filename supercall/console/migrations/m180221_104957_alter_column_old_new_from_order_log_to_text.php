<?php
use console\components\db\Migration;
use common\modules\order\models\OrderLog;
use backend\modules\administration\models\BaseLogger;
/**
 * Class m180221_104957_alter_column_old_new_from_order_log_to_text
 */
class m180221_104957_alter_column_old_new_from_order_log_to_text extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(OrderLog::tableName(), 'old', $this->text());
        $this->alterColumn(OrderLog::tableName(), 'new', $this->text());
        $this->alterColumn(BaseLogger::tableName(), 'tags', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(OrderLog::tableName(), 'old', $this->string());
        $this->alterColumn(OrderLog::tableName(), 'new', $this->string());
        $this->alterColumn(BaseLogger::tableName(), 'tags', $this->string());
    }
}

<?php
use console\components\db\Migration;

use common\models\Logger;
use common\models\LoggerTag;

/**
 * Class m171025_041304_add_log_tags_table
 */
class m171025_041304_add_log_tags_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->createTable(LoggerTag::tableName(), [
            'id' => $this->primaryKey(),
            'log_id' => $this->integer()->notNull(),
            'tag' => $this->string(),
            'value' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey('base_tag_log_id', LoggerTag::tableName(), 'log_id', Logger::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(LoggerTag::tableName());
    }
}

<?php
use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;
use yii\db\Query;

/**
 * Class m180205_064701_edit_description_auth_items
 */
class m180205_064701_edit_description_auth_items extends Migration
{
    public $replacement_list = [
        "access.user.setproduct" => "Операторы / Список операторов / Назначить товар оператору",
        "profile.control" => 'Профиль',
        "order.change" => 'Изменение заказа',
        "order.index" => 'Просмотр заказов',
        "i18n.language" => 'Интернационализация / Список языков',
        "catalog.priceshipping.index" => 'Справочники / Стоимость доставки',
        "catalog.priceshipping.edit" => 'Справочники / Стоимость доставки / Добавление (редактирование) стоимости доставки',
        "catalog.priceshipping.delete" => 'Справочники / Стоимость доставки / Удаление стоимости доставки',
        "operator.questionnaire.index" => 'Операторы / Оценка работы',
        "operator.questionnaire.orders" => 'Операторы / Планы оценки / Перечень заказов',
        "operator.questionnaire.finishoff" => 'Операторы / Оценка работы / Завершить',
        "operator.questionnaire.addbase" => 'Операторы / Список операторов / Оценить работу (Создание нового плана)',
        "operator.questionnaire.cancelcompletion" => 'Операторы / Планы оценки / Отмена завершения планак оценки',
        "operator.questionnaire.evaluation" => 'Операторы / Планы оценки /  Перечень заказов / Оценка по заказу',
        "operator.listoperators.index" => 'Операторы / Список операторов',
        "catalog.country.index" => 'Справочники / Страны',
        "queue.control.clear" => 'Очереди / Очередь / Очистить',
        "queue.control.index" => 'Очереди / Очередь',
        "partner.product.index" => 'Партнеры / Товары',
        "partner.product.edit" => 'Партнеры / Товары / Редактирование',
        "operator.questionnairemanagement.index" => 'Справочники / Управление опросами операторов',
        "catalog.kladercountries.getdata" => 'Получение данных справочника КЛАДР стран по Ajax',
        "widget.index.add" => 'Добавление виджета на главной странице',
        "widget.index.delete" => 'Удаление виджета с главной страницы',
        "widget.index.changeplace" => 'Перемещение виджета на главной странице',
        "catalog.scriptsoperators.index" => 'Справочники / Список скриптов для операторов',
        "operator.listoperators.setproduct" => 'Операторы / Список операторов / Управление товарами',
        "operator.listoperators.queue" => 'Операторы / Список операторов / Доступы к очередям',
        "operator.listoperators.queueset" => 'Операторы / Список операторов / Добавить очередь',
        "operator.listoperators.queueunset" => 'Операторы / Список операторов / Снять с очереди',
        "stats.product.index" => 'Статистика   Статистика по продуктам',
        "partner.package.index" => 'Партнеры / Акции',
        "partner.package.edit" => 'Партнеры / Акции / Редактирование (Добавление)',
        "partner.package.activate" => 'Партнеры / Акции / Активировать акцию',
        "partner.package.deactivate" => 'Партнеры / Акции / Деактивировать акцию',
        "partner.package.delete" => 'Партнеры / Акции / Удалить',
        "catalog.sub-status.index" => 'Справочники  / Список субстатусов',
        "catalog.sub-status.edit" => 'Справочники  / Список субстатусов / Редактирование',
        "catalog.sub-status.delete" => 'Справочники  / Список субстатусов / Удаление',
        "catalog.scriptsoperators.edit" => 'Справочники / Список скриптов для операторов / Редактирование (Создание)',
        "catalog.scriptsoperators.delete" => 'Справочники / Список скриптов для операторов / Удаление',
        "catalog.scriptsoperators.activate" => 'Справочники / Список скриптов для операторов / Активировать',
        "catalog.scriptsoperators.deactivate" => 'Справочники / Список скриптов для операторов / Деактивировать',
        "operator.request.confirm" => 'Операторы / Список заявок / Подтвердить',
        "coperator.request.reject" => 'Операторы / Список заявок / Отклонить',
        "operator.request.unblock" => 'Операторы / Список заявок / Разблокировать',
        "operator.request.index" => 'Операторы / Список заявок',
        "Статистика / Затраты времени по датам" => 'Статистика / Затраты времени по датам',
        "Статистика / Затраты времени по заказам" => 'Статистика / Затраты времени по заказам',
        "Статистика / Затраты времени по статусам заказов" => 'Статистика / Затраты времени по статусам заказов',
        "Статистика / Затраты времени по товарам заказов" => 'Статистика / Затраты времени по товарам заказов',
        "Заказа / Подтверждение адреса" => 'Заказы / Подтверждение адреса',
        "Заказа / Подтверждение адреса / Подтвердить адреса" => 'Заказы / Подтверждение адреса / Подтвердить адреса',
        "Заказа / Подтверждение адреса / Редактировать адреса" => 'Заказы / Подтверждение адреса / Редактировать адреса',
    ];

    public $phrases = [
        'Профиль' => 'Profile',
        'Изменение заказа' => 'Change order',
        'Просмотр заказов' => 'View orders',
        'Интернационализация / Список языков' => 'Internationalization / List of languages',
        'Справочники / Стоимость доставки' => 'Directories / Cost of delivery',
        'Справочники / Стоимость доставки / Добавление (редактирование) стоимости доставки' => 'Directories / Cost of delivery / Addition (editing) of the cost of delivery',
        'Справочники / Стоимость доставки / Удаление стоимости доставки' => 'Directories / Cost of delivery / Removal of shipping costs',
        'Операторы / Оценка работы' => 'Operators / Performance Evaluation',
        'Операторы / Планы оценки / Перечень заказов' => 'Operators / Evaluation plans / List of orders',
        'Операторы / Оценка работы / Завершить' => 'Operators / Evaluation of work / Completion',
        'Операторы / Список операторов / Оценить работу (Создание нового плана)' => 'Operators / List of operators / Estimate work (Create a new plan)',
        'Операторы / Планы оценки / Отмена завершения планак оценки' => 'Operators / Evaluation plans / Termination of completion of assessment plots',
        'Операторы / Планы оценки /  Перечень заказов / Оценка по заказу' => 'Operators / Evaluation Plans / List of Orders / Order Evaluation',
        'Операторы / Список операторов' => 'Operators / List of Operators',
        'Справочники / Страны' => 'Directories / Countries',
        'Очереди / Очередь / Очистить' => 'Queues / Queue / Clear',
        'Очереди / Очередь' => 'Queue / Queue',
        'Партнеры / Товары' => 'Partners / Products',
        'Партнеры / Товары / Редактирование' => 'Partners / Products / Editing',
        'Справочники / Управление опросами операторов' => 'Directories / Managing interrogations of operators',
        'Получение данных справочника КЛАДР стран по Ajax' => 'Obtaining data from the KLADR of countries  AJAX  directory',
        'Добавление виджета на главной странице' => 'Adding a widget to the main page',
        'Удаление виджета с главной страницы' => 'Deleting a widget from the main page',
        'Перемещение виджета на главной странице' => 'Moving the widget on the main page',
        'Справочники / Список скриптов для операторов' => 'Directories / List of scripts for operators',
        "Операторы / Список операторов / Назначить товар оператору" => 'Operators / List of operators / Assign the goods to the operator',
        'Операторы / Список операторов / Управление товарами' => 'Operators / List of operators / Goods management',
        'Операторы / Список операторов / Доступы к очередям' => 'Operators / List of operators / Access to queues',
        'Операторы / Список операторов / Добавить очередь' => 'Operators / List of operators / Add queue',
        'Операторы / Список операторов / Снять с очереди' => 'Operators / List of operators / Remove from queue',
        'Статистика   Статистика по продуктам' => 'Statistics by Product Statistics',
        'Партнеры / Акции' => 'Partners / Promotions',
        'Партнеры / Акции / Редактирование (Добавление)' => 'Partners / Promotions / Editing (Addition)',
        'Партнеры / Акции / Активировать акцию' => 'Partners / Actions / Activate share',
        'Партнеры / Акции / Деактивировать акцию' => 'Partners / Actions / Deactivate share',
        'Партнеры / Акции / Удалить' => 'Partners / Promotions / Delete',
        'Справочники  / Список субстатусов' => 'Reference books / List of sub-statuses',
        'Справочники  / Список субстатусов / Редактирование' => 'Directories / List of Sub-statuses / Editing',
        'Справочники  / Список субстатусов / Удаление' => 'Directories / List of Sub-items / Deletion',
        'Справочники / Список скриптов для операторов / Редактирование (Создание)' => 'Directories / List of scripts for operators / Editing (Creating)',
        'Справочники / Список скриптов для операторов / Удаление' => 'Directories / List of scripts for operators / Removal',
        'Справочники / Список скриптов для операторов / Активировать' => 'Directories / List of scripts for operators / Activate',
        'Справочники / Список скриптов для операторов / Деактивировать' => 'Directories / List of scripts for operators / Deactivate',
        'Операторы / Список заявок / Подтвердить' => 'Operators / List of applications / Confirm',
        'Операторы / Список заявок / Отклонить' => 'Operators / List of applications / Reject',
        'Операторы / Список заявок / Разблокировать' => 'Operators / List of applications / Unblock',
        'Операторы / Список заявок' => 'Operators / List of applications',
        'Статистика / Затраты времени по датам' => 'Statistics / Time Spent by Date',
        'Статистика / Затраты времени по заказам' => 'Statistics / Time costs for orders',
        'Статистика / Затраты времени по статусам заказов' => 'Statistics / Time costs by order status',
        'Статистика / Затраты времени по товарам заказов' => 'Statistics / Costs of time for order goods',
        'Заказы / Подтверждение адреса' => 'Orders / Address Confirmation',
        'Заказы / Подтверждение адреса / Подтвердить адреса' => 'Orders / Address verification / Confirm addresses',
        'Заказы / Подтверждение адреса / Редактировать адреса' => 'Orders / Address verification / Edit addresses',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //old
        $old_permissions = [
            'catalog.productprice.edit',
            'catalog.productprice.index',
            'catalog.questionnaire.delete',
            'catalog.questionnaire.edit',
            'catalog.questionnaire.index',
        ];
        foreach($old_permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_SUPERVISOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_CURATOR]);
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }

        $this->insert('{{%auth_item}}', array(
            'name' => 'catalog.scriptsoperators.index',
            'type' => '2',
            'description' => 'catalog.scriptsoperators.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'catalog.scriptsoperators.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_CURATOR,
            'child' => 'catalog.scriptsoperators.index'
        ));

        foreach($this->replacement_list as $origin=>$replacement){
            $rename = yii::$app->db->createCommand("update auth_item set description = :description_replace where description=:description_origin")
                ->bindValues([
                    ':description_origin' => $origin,
                    ':description_replace' => $replacement
                ])->execute();

                if($rename){
                    echo 'replace : '.$origin.' => '.$replacement.PHP_EOL;
                    var_dump($rename).PHP_EOL;
                }else{
                    echo 'not replace : '.$origin.' => '.$replacement.PHP_EOL;
                    var_dump($rename).PHP_EOL;
                }
        }

        i18n_source_message::addTranslate($this->phrases, true);

        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.scriptsoperators.index', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.scriptsoperators.index', 'parent' => User::ROLE_CURATOR]);
        $this->delete('{{%auth_item}}', ['name' => 'catalog.scriptsoperators.index']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->replacement_list as $origin=>$replacement){
            $rename = yii::$app->db->createCommand("update {{%auth_item}} set description = :description_origin where description=:description_replace")
                ->bindValues([
                    ':description_origin' => $origin,
                    ':description_replace' => $replacement
                ])->execute();

            if($rename){
                echo 'replace : '.$origin.' => '.$origin.PHP_EOL;
                var_dump($rename).PHP_EOL;
            }else{
                echo 'not replace : '.$origin.' => '.$origin.PHP_EOL;
                var_dump($rename).PHP_EOL;
            }
        }

        i18n_source_message::removeTranslate($this->phrases);

        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.scriptsoperators.index', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.scriptsoperators.index', 'parent' => User::ROLE_CURATOR]);
        $this->delete('{{%auth_item}}', ['name' => 'catalog.scriptsoperators.index']);

    }
}

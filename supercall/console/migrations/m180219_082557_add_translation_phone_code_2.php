<?php
    
    use console\components\db\Migration;
    use backend\models\i18n_source_message;
    
    /**
     * Class m180219_082557_add_translation_phone_code_2
     */
    class m180219_082557_add_translation_phone_code_2 extends Migration
    {
        public $phrases = [
            'Мин. длина номера' => 'Min phone length',
            'Макс. длина номера' => 'Max phone length',
        ];
    
        /**
         * @inheritdoc
         */
        public function safeUp()
        {
            i18n_source_message::addTranslate($this->phrases, true);
        }
        
        /**
         * @inheritdoc
         */
        public function safeDown()
        {
            i18n_source_message::removeTranslate($this->phrases);
        }
    }

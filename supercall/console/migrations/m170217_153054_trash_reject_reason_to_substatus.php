<?php
use console\components\db\Migration;
use yii\db\Expression;

/**
 * Class m170217_153054_trash_reject_reason_to_substatus
 */
class m170217_153054_trash_reject_reason_to_substatus extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update('{{%order}}', ['sub_status' => new Expression('cast(reject_reason as integer) + 501')], 'status = 5 and reject_reason is not null');
        $this->update('{{%order}}', ['sub_status' => new Expression('cast(trash_reason as integer) + 601')], 'status = 6 and reject_reason is not null');
        $this->dropColumn('{{%order}}', 'trash_reason');
        $this->dropColumn('{{%order}}', 'reject_reason');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('{{%order}}', 'trash_reason', $this->string());
        $this->addColumn('{{%order}}', 'reject_reason', $this->string());
    }
}

<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m190228_030932_add_permission_whatsapp_and_translate
 */
class m190228_030932_add_permission_whatsapp_and_translate extends Migration
{
    public $phrases = [
        'Редактирование Whatsapp настроек страны' => 'Editing whatsapp country settings',
        'Просмотр дополнительных данных аккаунта Whatsapp' => 'View Additional Whatsapp Account Information',
        'Номер аккаунта chat-api.com' => 'Account number chat-api.com',
        'Мобильный для аккаунта chat-api.com' => 'Mobile for chat-api.com account',
        'Токен chat-api.com' => 'Chat-api.com token',
        'Api URL chat-api.com' => 'Api URL chat-api.com',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addPermission(['catalog.country.whatsappedit' => 'Редактирование Whatsapp настроек страны']);
        $this->addPermission(['catalog.country.whatsappviewdescription' => 'Просмотр дополнительных данных аккаунта Whatsapp']);

        $this->addPermissionToRole('catalog.country.whatsappedit', User::ROLE_ADMINISTRATOR);
        $this->addPermissionToRole('catalog.country.whatsappviewdescription', User::ROLE_ADMINISTRATOR);

        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->deletePermission('catalog.country.whatsappedit');
        $this->deletePermission('catalog.country.whatsappviewdescription');

        i18n_source_message::removeTranslate($this->phrases);
    }
}

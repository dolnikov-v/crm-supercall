<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180119_075426_add_translate_to_audit_cc
 */
class m180119_075426_add_translate_to_audit_cc extends Migration
{
    public $phrases = [
        'Аудит КЦ' => 'Audit CC'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170221_154627_log_operator
 */
class m170221_154627_log_operator extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_log}}', 'operator', $this->string());
        $this->addColumn('{{%order_product_log}}', 'operator', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order_log}}', 'operator');
        $this->dropColumn('{{%order_product_log}}', 'operator');
    }
}

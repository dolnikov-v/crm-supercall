<?php
use console\components\db\Migration;

/**
 * Class m170626_040640_add_permission_access_user_product
 */
class m170626_040640_add_permission_access_user_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'access.user.setproduct',
            'type' => '2',
            'description' => 'access.user.setproduct',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'operator',
            'child' => 'access.user.setproduct'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'access.user.setproduct', 'parent' => 'operator']);
        $this->delete('{{%auth_item}}', ['name' => 'access.user.setproduct']);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m161205_120454_partner_shipping
 */
class m161205_120454_partner_shipping extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_shipping}}', [
            'id' => $this->primaryKey(),
            'partner_id' => $this->integer()->notNull(),
            'partner_shipping_id' => $this->integer()->notNull(),
            'shipping_id' => $this->integer()->notNull(),
            'active' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, '{{%partner_shipping}}', 'partner_id', '{{%partner}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%partner_shipping}}', 'shipping_id', '{{%shipping}}', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_shipping}}');
    }
}

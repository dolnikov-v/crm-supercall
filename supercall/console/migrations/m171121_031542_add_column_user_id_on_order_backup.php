<?php
use console\components\db\Migration;
use backend\modules\order\models\OrderBackup;
use common\models\User;

/**
 * Class m171121_031542_add_column_user_id_on_order_backup
 */
class m171121_031542_add_column_user_id_on_order_backup extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(OrderBackup::tableName(), 'user_id', $this->integer());
        $this->addForeignKey(null, OrderBackup::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderBackup::tableName(), 'user_id');
    }
}

<?php
use console\components\db\Migration;
use common\modules\order\models\Order;

/**
 * Class m170829_094650_add_column_coords_to_order
 */
class m170829_094650_add_column_coords_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'latitude', $this->string()->defaultValue(null));
        $this->addColumn(Order::tableName(), 'longitude', $this->string()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'latitude');
        $this->dropColumn(Order::tableName(), 'longitude');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m161229_141018_messages
 */
class m161229_141018_messages extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_message}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'type' => $this->string(200)->notNull(),
            'operator' => $this->string(200)->notNull(),
            'to' => $this->string(200)->notNull(),
            'text' => $this->text()->notNull(),
            'status' => $this->string(200)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addForeignKey('fk__order_message__order', '{{%order_message}}', 'order_id', '{{%order}}', 'id', 'restrict', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_message}}');
    }
}

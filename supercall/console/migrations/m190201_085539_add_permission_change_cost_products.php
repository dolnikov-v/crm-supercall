<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m190201_085539_add_permission_change_cost_products
 */
class m190201_085539_add_permission_change_cost_products extends Migration
{
    public $phrases = [
        'Сменить стоимость товаров в заказе' => 'Change the cost of goods in the order',
        'Чтобы не изменять стоимость товара - оставьте поле с ценой пустым.' => 'In order not to change the value of the goods - leave the field with the price empty.',
        'Заказ №{id} находятся в финальном статусе' => 'Order №{id} are in final status',
        'Заказ №{id} в работе' => 'Order №{id} in work',
        'Ошибка при изменении финальной стоимости заказа №{id} : {error}' => 'Error when changing the final price of the order № {id}: {error}',
        'Ошибка транзакции при обновлении заказа №{id} : {error}' => 'Transaction error when updating order No. {id}: {error}',
        'Заказ №{id} успешно изменен' => 'Order №{id} successfully changed',
        'Заказа №{id} обновлен, но не отправлен партнеру. Необходимо отправить заказ в очередь в ручном режиме сомощю груповой операции' => 'Order №{id} updated but not sent to partner. You must send the order to the queue manually by using a group operation',
        'Заказа №{id} обновлен, данные отправлены партнеру.' => 'Order №{id} updated, data sent to the partner.',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        $this->addPermission(['order.groupoperations.changecostgoods' => 'Сменить стоимость товаров в заказе']);
        $this->addPermissionToRole('order.groupoperations.changecostgoods', User::ROLE_ADMINISTRATOR);
        $this->addPermissionToRole('order.groupoperations.changecostgoods', User::ROLE_CURATOR);
        $this->addPermissionToRole('order.groupoperations.changecostgoods', User::ROLE_TEAM_LEAD);
        $this->addPermissionToRole('order.groupoperations.changecostgoods', User::ROLE_AUDITOR);
        $this->addPermissionToRole('order.groupoperations.changecostgoods', User::ROLE_SALES_DIRECTOR);
        $this->addPermissionToRole('order.groupoperations.changecostgoods', User::ROLE_SUPERVISOR);
        $this->addPermissionToRole('order.groupoperations.changecostgoods', User::ROLE_LOGISTICIAN);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->deletePermissionByRole('order.groupoperations.changecostgoods', User::ROLE_ADMINISTRATOR);
        $this->deletePermissionByRole('order.groupoperations.changecostgoods', User::ROLE_CURATOR);
        $this->deletePermissionByRole('order.groupoperations.changecostgoods', User::ROLE_TEAM_LEAD);
        $this->deletePermissionByRole('order.groupoperations.changecostgoods', User::ROLE_AUDITOR);
        $this->deletePermissionByRole('order.groupoperations.changecostgoods', User::ROLE_SALES_DIRECTOR);
        $this->deletePermissionByRole('order.groupoperations.changecostgoods', User::ROLE_SUPERVISOR);
        $this->deletePermissionByRole('order.groupoperations.changecostgoods', User::ROLE_LOGISTICIAN);
        $this->deletePermission('order.groupoperations.changecostgoods');
    }
}

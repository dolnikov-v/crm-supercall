<?php
use console\components\db\Migration;

/**
 * Class m000000_000001_auth_item
 */
class m000000_000001_auth_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%auth_item}}', [
            'name' => $this->string(64)->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'rule_name' => $this->string(64),
            'data' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            $this->includePrimaryKey('name'),
        ]);

        $this->createIndex(null, $this->authManager->itemTable, 'type');
        $this->addForeignKey(null, $this->authManager->itemTable, 'rule_name', $this->authManager->ruleTable, 'name', self::SET_NULL, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%auth_item}}');
    }
}

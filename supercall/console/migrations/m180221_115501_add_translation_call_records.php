<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180221_115501_add_translation_call_records
 */
class m180221_115501_add_translation_call_records extends Migration
{
    public $phrases = [
        'Запись разговора' => 'Call record',
        'Запись отсутвует' => 'No record',
        'Общее время звонка' => 'Total call duration',
        'Время разговора' => 'Conversation time',
        'Статус звонка' => 'Call status',
        'Дата звонка' => 'Date of call',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

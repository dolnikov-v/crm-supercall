<?php
use console\components\db\Migration;
use backend\modules\order\models\OrderTask;
use common\modules\order\models\Order;
use common\models\User;

/**
 * Class m170704_025640_create_table_order_task
 */
class m170704_025640_create_table_order_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(OrderTask::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'status' => $this->integer()->defaultValue(1),
            'order_old' => "JSON",
            'order_new' => "JSON",
            'product_old' => "JSON",
            'product_new' => "JSON",
            'operator' => $this->integer()->notNull(),
            'curator' => $this->integer()->defaultValue(null),
            'curator_username' => $this->string()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->defaultValue(0)
        ]);

        $this->addForeignKey(null, OrderTask::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::CASCADE);
        $this->addForeignKey(null, OrderTask::tableName(), 'operator', User::tableName(), 'id', self::CASCADE, self::CASCADE);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(OrderTask::tableName());
    }
}

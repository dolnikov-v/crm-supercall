<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auto_call`.
 */
class m180112_131228_create_auto_call_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('auto_call', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'order_phone' => $this->string(15),
            'cdr_id' => $this->string(20)->defaultValue(''),
            'call_status' => $this->string(10)->defaultValue(''),
            'queue_name' => $this->string(50),
            'status' => $this->integer(1)->defaultValue(0),
            'user_sip' => $this->string(15)->defaultValue(''),
            'call_time' => $this->integer(),
            'country_id' => $this->integer()->notNull()
        ]);
        
        $this->createIndex('idx-auto_call-order_id','auto_call','order_id');
        $this->createIndex('idx-auto_call-order_phone','auto_call','order_phone');
        $this->createIndex('idx-auto_call-status','auto_call','status');
        $this->createIndex('idx-auto_call-call_status','auto_call','call_status');
        $this->createIndex('idx-auto_call-cdr_id','auto_call','cdr_id');
        $this->createIndex('idx-auto_call-queue_name','auto_call','queue_name');
        $this->createIndex('idx-auto_call-country_id','auto_call','country_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('auto_call');
    }
}

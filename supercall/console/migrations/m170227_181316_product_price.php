<?php
use console\components\db\Migration;

/**
 * Class m170227_181316_product_price
 */
class m170227_181316_product_price extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_product_price}}', [
            'id' => $this->primaryKey(),
            'partner_product_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'value' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()
        ]);
        $this->addForeignKey('fk__partner_product_price__partner_product',
            '{{%partner_product_price}}', 'partner_product_id',
            '{{%partner_product}}', 'id',
            'cascade', 'cascade');
        $this->addForeignKey('fk__partner_product_price__country',
            '{{%partner_product_price}}', 'country_id',
            '{{%country}}', 'id',
            'restrict', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_product_price}}');
    }
}

<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171024_100427_add_translate_action_and_request
 */
class m171024_100427_add_translate_action_and_request extends Migration
{
    public $phrases = [
        'Ваш аккаунт {operator_area} одобрен:<hr/> <b>логин:</b> {username} (или {email}) <br/><b>пароль:</b> выслан Вам в письме после регистрации.' => 'Your account {operator_area} is approved: <hr /> <b> login: </ b> {username} (or {email}) <br/> <b> password: </ b> sent to you in the email after registration.',
        'Для подтверждения аккаунта необходимо в профиле личного кабинета {personal_area} загрузить сканкопию документа, подтверждающего Вашу личность' => 'To verify your account, you need to upload a scanned copy of your identity document in the personal account {personal_area} profile',
        'Если вы не хотите менять пароль, то необходимо оставить поле «Пароль» пустым.' => 'If you do not want to change the password, you must leave the "Password" field blank.',
        'Акции' => 'Promotions',
        'Таблица с акциями' => 'Table with shares',
        'Добавить акцию' => 'Add stock',
        'Префикс' => 'Prefix',
        'Товар' => 'Product',
        'Подарок' => 'Gift',
        'Добавление акции' => 'Add stock',
        'Сохранить акцию' => 'Save share',
        '' => '',
        '' => '',
        '' => '',
        '' => '',
        '' => '',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

    }
}

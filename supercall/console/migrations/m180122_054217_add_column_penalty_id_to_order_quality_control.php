<?php
use backend\modules\operator\models\QualityControl;
use console\components\db\Migration;

/**
 * Class m180122_054217_add_column_penalty_id_to_order_quality_control
 */
class m180122_054217_add_column_penalty_id_to_order_quality_control extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(QualityControl::tableName(), 'penalty_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(QualityControl::tableName(), 'penalty_id');
    }
}

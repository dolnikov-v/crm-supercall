<?php
use console\components\db\Migration;

/**
 * Class m170505_070914_add_column_ip_to_api_log
 */
class m170505_070914_add_column_ip_to_api_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%api_log}}', 'ip', "INET");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%api_log}}', 'ip');
    }
}

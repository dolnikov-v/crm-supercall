<?php
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180117_034819_add_permission_to_address_verification
 */
class m180117_034819_add_permission_to_address_verification extends Migration
{
    public $permissions = [
        'order.addressverification.index' => 'Заказа / Подтверждение адреса',
        'order.addressverification.confirm' => 'Заказа / Подтверждение адреса / Подтвердить адреса',
        'order.addressverification.edit' => 'Заказа / Подтверждение адреса / Редактировать адреса'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach($this->permissions as $name=>$desc) {
            $this->insert('{{%auth_item}}', array(
                'name' => $name,
                'type' => '2',
                'description' => $desc,
                'created_at' => time(),
                'updated_at' => time()
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_CURATOR,
                'child' => $name
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_SUPERVISOR,
                'child' => $name
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_TEAM_LEAD,
                'child' => $name
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->permissions as $name=>$desc) {
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => User::ROLE_CURATOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => User::ROLE_SUPERVISOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => User::ROLE_TEAM_LEAD]);
            $this->delete('{{%auth_item}}', ['name' => $name]);
        }
    }
}

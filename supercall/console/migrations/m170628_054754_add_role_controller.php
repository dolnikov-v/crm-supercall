<?php
use console\components\db\Migration;

/**
 * Class m170628_054754_add_role_controller
 */
class m170628_054754_add_role_controller extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = $this->authManager;
        $role = $auth->createRole('controller');
        $role->description = 'Контролер';
        $auth->add($role);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = $this->authManager;
        $role = $auth->getRole('controller');
        $auth->remove($role);
    }
}

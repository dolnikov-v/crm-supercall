<?php

use common\models\LiveMessage;
use common\models\User;
use common\modules\order\models\Order;
use console\components\db\Migration;

/**
 * Class m180529_103234_create_table_live_message
 */
class m180529_103234_create_table_live_message extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(LiveMessage::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->defaultValue(null),
            'is_new' => $this->boolean()->defaultValue(LiveMessage::IS_NEW),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->createIndex('live_messsage_order_id', LiveMessage::tableName(), 'order_id');
        $this->addForeignKey(null, LiveMessage::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(LiveMessage::tableName());
    }
}

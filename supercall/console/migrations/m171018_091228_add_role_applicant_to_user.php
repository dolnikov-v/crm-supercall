<?php
use console\components\db\Migration;
use common\models\User;

/**
 * Class m171018_091228_add_role_applicant_to_user
 */
class m171018_091228_add_role_applicant_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = $this->authManager;
        $role = $auth->createRole(User::ROLE_APPLICANT);
        $role->description = 'Заявитель';
        $auth->add($role);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = $this->authManager;
        $role = $auth->getRole(User::ROLE_APPLICANT);
        $auth->remove($role);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m161226_161054_partner_shipping_prices
 */
class m161226_161054_partner_shipping_prices extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete('{{%partner_shipping}}');
        $this->addColumn('{{%partner_shipping}}', 'country_id', $this->integer()->notNull());
        $this->addColumn('{{%partner_shipping}}', 'price', $this->float()->notNull());
        $this->addForeignKey('fk__partner_shipping__country', '{{%partner_shipping}}', 'country_id', '{{%country}}', 'id', 'restrict', 'cascade');

        $this->addColumn('{{%order}}', 'shipping_id', $this->integer());
        $this->addForeignKey('fk__order__shipping', '{{%order}}', 'shipping_id', '{{%shipping}}', 'id', 'restrict', 'cascade');

        $this->addColumn('{{%partner_form}}', 'show_shipping', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_form}}', 'show_shipping');

        $this->dropColumn('{{%order}}', 'shipping_id');

        $this->dropColumn('{{%partner_shipping}}', 'price');
        $this->dropColumn('{{%partner_shipping}}', 'country_id');
    }
}

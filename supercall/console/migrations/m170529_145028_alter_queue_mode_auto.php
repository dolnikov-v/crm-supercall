<?php
use console\components\db\Migration;

/**
 * Class m170529_145028_alter_queue_mode_auto
 */
class m170529_145028_alter_queue_mode_auto extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('{{%queue}}', 'mode_auto');
        $this->addColumn('{{%queue}}', 'mode_auto', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}

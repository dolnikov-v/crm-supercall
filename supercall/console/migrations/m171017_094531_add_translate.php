<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171017_094531_add_translate
 */
class m171017_094531_add_translate extends Migration
{
    public $phrases = [
        'Поиск скриптов' => 'Scripts search',
        'Скрипт' => 'Script',
        'Тип' => 'Type',
        'Текст' => 'Text',
        'JS логгер' => 'JS logger',
        'Ошибка' => 'Error',
        'Стек' => 'Stack',
        'Просмотр лога' => 'View Log',
        'Отменить' => 'Cancel'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

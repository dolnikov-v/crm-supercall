<?php

use backend\modules\queue\models\Queue;
use backend\modules\queue\models\QueueLanding;
use console\components\db\Migration;

/**
 * Class m180412_090252_create_table_queue_landing
 */
class m180412_090252_create_table_queue_landing extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(QueueLanding::tableName(), [
            'id' => $this->primaryKey(),
            'queue_id' => $this->integer()->notNull(),
            'url_landing' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, QueueLanding::tableName(), 'queue_id', Queue::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(QueueLanding::tableName());
    }
}

<?php
use console\components\db\Migration;
use common\modules\partner\models\PartnerForm;

/**
 * Class m180206_031247_add_column_mask_phones_to_partner_form
 */
class m180206_031247_add_column_mask_phones_to_partner_form extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(PartnerForm::tableName(), 'mask_phones', $this->boolean()->defaultValue(true));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(PartnerForm::tableName(), 'mask_phones');
    }
}

<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180530_103205_add_translate_to_live_message
 */
class m180530_103205_add_translate_to_live_message extends Migration
{
    public $phrases = [
        'Сообщения (<span class="old-messages">{old}</span>/<span class="new-messages">{new}</span>)' => 'Messages (<span class = "old-messages">{old}</span>/<span class = "new-messages">{new}</span>)',
        'С данным клиентом диалога ещё не было' => 'With this client, the dialogue was not there yet.',
        'Отправить' => 'Send',
        'Сообщение' => 'Message',
        'Цитировать' => 'Quote',
        'Сообщение клиента' => 'Customer Message',
        'Сообщение оператора' => 'Operator message'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

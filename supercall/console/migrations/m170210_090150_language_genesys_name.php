<?php
use console\components\db\Migration;

/**
 * Class m170210_090150_language_genesys_name
 */
class m170210_090150_language_genesys_name extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%i18n_language}}', 'genesys_name', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%i18n_language}}', 'genesys_name');
    }
}

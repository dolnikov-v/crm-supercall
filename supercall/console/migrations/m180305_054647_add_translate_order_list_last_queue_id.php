<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180305_054647_add_translate_order_list_last_queue_id
 */
class m180305_054647_add_translate_order_list_last_queue_id extends Migration
{
    public $phrases = [
        'Очередь' => 'Queue',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

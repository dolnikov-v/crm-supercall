<?php
use console\components\db\Migration;
use backend\modules\operator\models\QualityControl;

/**
 * Class m180125_054751_add_column_confirm_to_order_quality_control
 */
class m180125_054751_add_column_confirm_to_order_quality_control extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(QualityControl::tableName(), 'confirm', $this->boolean()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(QualityControl::tableName(), 'confirm');
    }
}

<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180122_080554_add_translate_on_release
 */
class m180122_080554_add_translate_on_release extends Migration
{
    public $old_phrases = [
        'Дата создания в CRM' => 'Creation date in CRM',
        'Дата создания у партнёра' => 'Date created from the partner'
    ];

    public $phrases = [
        'Дата создания в CRM' => 'Date of creation in CRM',
        'Подтверждение адреса' => 'Address verification',
        'Дата создания у партнёра' => 'Date of creation by partner'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->old_phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

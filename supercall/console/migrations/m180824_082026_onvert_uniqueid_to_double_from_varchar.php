<?php
use console\components\db\Migration;

/**
 * Class m180824_082026_onvert_uniqueid_to_double_from_varchar
 */
class m180824_082026_onvert_uniqueid_to_double_from_varchar extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('{{%call_history}}', 'uniqueid', $this->string(32));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('{{%call_history}}', 'uniqueid', $this->double());
    }
}

<?php
use backend\modules\operator\models\ZingTree;
use common\models\Country;
use console\components\db\Migration;

/**
 * Class m180213_065057_create_table_zeng_tree_country
 */
class m180213_065057_create_table_zeng_tree_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%zing_tree_country}}', [
            'id' => $this->primaryKey(),
            'zing_tree_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(null, '{{%zing_tree_country}}', 'zing_tree_id', ZingTree::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%zing_tree_country}}', 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%zing_tree_country}}');
    }
}

<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m190207_040927_add_column_klader_in_all_string_to_county
 */
class m190207_040927_add_column_klader_in_all_string_to_county extends Migration
{

    public $phrases = [
        'При работе с КЛАДР искать совпадения по всей строке' => 'When working with KLADR, search for matches throughout the line'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
        $this->addColumn('{{%country}}', 'klader_in_all_string', $this->boolean()->defaultValue(false));

        //Для Philippines выставляем значение true
        $this->update('{{%country}}', ['klader_in_all_string' => true], 'id=22');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
        $this->dropColumn('{{%country}}', 'klader_in_all_string');
    }
}
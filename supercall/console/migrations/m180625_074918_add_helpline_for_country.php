<?php

use backend\models\i18n_source_message;
use common\models\Country;
use console\components\db\Migration;

/**
 * Class m180625_074918_add_helpline_for_country
 */
class m180625_074918_add_helpline_for_country extends Migration
{
    public $phrases = [
        'Телефон доверия' => 'Helpline',
    ];


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Country::tableName(), 'helpline_phone', $this->string()->defaultValue(null));

        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Country::tableName(), 'helpline_phone');

        i18n_source_message::removeTranslate($this->phrases);

    }
}

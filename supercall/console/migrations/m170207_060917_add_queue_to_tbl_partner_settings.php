<?php
use console\components\db\Migration;

/**
 * Class m170207_060917_add_queue_to_tbl_partner_settings
 */
class m170207_060917_add_queue_to_tbl_partner_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_settings}}', 'queue_id', $this->integer()->null());
        $this->addForeignKey('fk-partner_settings-queue_id', '{{%partner_settings}}', 'queue_id', '{{%queue}}', 'id', self::CASCADE, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-partner_settings-queue_id', '{{%partner_settings}}');
        $this->dropColumn('{{%partner_settings}}', 'queue_id');
    }
}

<?php
use console\components\db\Migration;

use common\modules\call\models\CallHistory;
use common\models\User;
use common\modules\order\models\Order;
/**
 * Class m170901_031905_create_table_call_history
 */
class m170901_031905_create_table_call_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(CallHistory::tableName(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'start_time' => $this->integer()->notNull(),
            'end_time' => $this->integer(),
            'start_status' => $this->integer()->notNull(),
            'end_status' => $this->integer(),
            'duration' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, CallHistory::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CallHistory::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(CallHistory::tableName());
    }
}

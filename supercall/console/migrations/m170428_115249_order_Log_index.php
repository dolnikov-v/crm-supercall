<?php
use console\components\db\Migration;

/**
 * Class m170428_115249_order_Log_index
 */
class m170428_115249_order_Log_index extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('idx_order_log_group_id', '{{%order_log}}', 'group_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('idx_order_log_group_id', '{{%order_log}}');
    }
}

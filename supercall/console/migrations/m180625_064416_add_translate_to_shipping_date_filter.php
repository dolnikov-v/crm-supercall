<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180625_064416_add_translate_to_shipping_date_filter
 */
class m180625_064416_add_translate_to_shipping_date_filter extends Migration
{
    public $phrases = [
        'Дата доставки' => 'Shipping date'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use console\components\db\Migration;

/**
 * Class m181218_053913_order_buyout
 */
class m181218_053913_order_buyout extends Migration
{
    public $tableName = '{{%order_buyout}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_buyout}}',[
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'buyout' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'partner_id' => $this->integer(),
        ]);

        $this->addForeignKey(null, $this->tableName, 'order_id', '{{%order}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, $this->tableName, 'partner_id', '{{%partner}}', 'id', self::CASCADE, self::RESTRICT);
        $this->createIndex(null, $this->tableName, ['order_id']);
        $this->createIndex(null, $this->tableName, ['buyout']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170123_123614_order_genesys_id
 */
class m170123_123614_order_genesys_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'genesys_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'genesys_id');
    }
}

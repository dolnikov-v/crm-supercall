<?php
use console\components\db\Migration;
use common\models\GmapKey;

/**
 * Class m171013_102112_add_gmap_key
 */
class m171013_102112_add_gmap_key extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(GmapKey::tableName(), [
            'email' => 'igor.vasinsky@gmail.com',
            'key' => 'AIzaSyC9f4OsO56tNFkoBE6xbyfWqcA1EKchInk',
            'active' => true,
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(GmapKey::tableName(), ['email' => 'igor.vasinsky@gmail.com']);
    }
}

<?php

/**
 * Создание роли (с проверкой на наличие)
 * $role = [name => description]
 * $this->addRole(array $role)
 *
 * Удаление роли
 * $this-deleteRole(string $role)
 *
 * Проверка наличия роли
 * $this->existsRole(string $role_name);
 *
 * Создание пермишена (с проверкой на наличие)
 * $permission = [name => description]
 * $this->addPermission(array $permission)
 *
 * Удаление пермишена (автоматом удаляются все связки по ролям)
 * $this->deletePermission(string $permission);
 *
 * Проверка наличия пермишена
 * $this->>existsPermission(string $permission_name)
 *
 * Добавление пермишена к роли
 * $this->addPermissionToRole(string $permission, string $role)
 *
 * Удаления пермишена у роли
 * $this->deletePermissionByRole(string $permission, string $role)
 *
 * Добавление перевода (удаление перед добавление уже внутри метода)
 * i18n_source_message::addTranslate($this->phrases, true);
 *
 * Удаление переводов
 * i18n_source_message::removeTranslate($this->phrases);
 */

use console\components\db\Migration;
use yii\db\Query;
use yii\helpers\Json;

/**
 * Class m190321_051711_change_block_time_for_queue
 */
class m190321_051711_change_block_time_for_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $query = (new Query())->select('id, strategy')
            ->from('{{%queue}}');


        foreach ($query->batch(10) as $queues) {
            foreach ($queues as $queue) {
                $strategyChanged = false;

                if (!is_null($queue['strategy'])) {
                    $strategys = Json::decode($queue['strategy']);

                    foreach ($strategys as $key => $strategy) {

                        if (isset($strategy['blockTime']) && $strategy['blockTime'] < 30) {
                            $strategy['blockTime'] = 30;
                            $strategyChanged = true;
                        }

                        $newStrategy[$key] = $strategy;
                    }

                    if ($strategyChanged) {
                        $this->update('{{%queue}}', [
                            'strategy' => Json::encode($newStrategy),
                        ], 'id=' . $queue['id']);
                    }

                }
            }
        }

    }

}

<?php
use console\components\db\Migration;

/**
 * Class m171214_113749_add_role_auditor
 */
class m171214_113749_add_role_auditor extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = $this->authManager;
        $role = $auth->createRole('auditor');
        $role->description = 'Аудитор';
        $auth->add($role);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = $this->authManager;
        $role = $auth->getRole('auditor');
        $auth->remove($role);
    }
}

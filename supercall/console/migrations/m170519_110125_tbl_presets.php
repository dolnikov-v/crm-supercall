<?php
use console\components\db\Migration;

/**
 * Class m170519_110125_tbl_presets
 */
class m170519_110125_tbl_presets extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%presets}}', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(),
            'attempts'   => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%presets}}');
    }
}

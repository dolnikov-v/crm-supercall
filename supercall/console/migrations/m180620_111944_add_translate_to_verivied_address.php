<?php

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180620_111944_add_translate_to_verivied_address
 */
class m180620_111944_add_translate_to_verivied_address extends Migration
{
    public $phrases = [
        'Проверенные адреса' => 'Verified address',
        'Верификатор' => 'Verificator',
        'Нуждается в проверке' => 'Need verification',
        'Подтверждён' => 'Confirmed',
        'Неподтверждён' => 'Unconfirmed',
        'Таблица с проверенными адресами' => 'Table with verified addresses',
        'Аудит / Проверенные адреса' => 'Audit / Verified address',
        'Сводная статистика' => 'Summary Statistics',
        'Проверка адреса' => 'Address Verification',
        'Номер у партнёра' => 'Index by partner',
        'Период' => 'Date',
        'Тип подтверждения' => 'Confirmation Type'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        $this->insert('{{%auth_item}}', array(
            'name' => 'audit.verifiedaddress.index',
            'type' => '2',
            'description' => 'Аудит / Проверенные адреса',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_AUDITOR,
            'child' => 'audit.verifiedaddress.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_QUALITY_MANAGER,
            'child' => 'audit.verifiedaddress.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->delete($this->authManager->itemChildTable, ['child' => 'audit.verifiedaddress.index', 'parent' => User::ROLE_AUDITOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => 'audit.verifiedaddress.index', 'parent' => User::ROLE_QUALITY_MANAGER]);
        $this->delete('{{%auth_item}}', ['name' => 'audit.verifiedaddress.index']);
    }

}

<?php
use console\components\db\Migration;

/**
 * Class m161206_132921_partner_product_price
 */
class m161206_132921_partner_product_price extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_product}}', 'price_data', 'JSON');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_product}}', 'price_data');
    }
}

<?php
use console\components\db\Migration;
use backend\modules\widget\models\WidgetRole;

/**
 * Class m170901_112614_create_widget_role
 */
class m170901_112614_create_widget_role extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(WidgetRole::tableName(), [
            'id' => $this->primaryKey(),
            'role' => $this->string(64)->notNull(),
            'type_id' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'widget_role', 'type_id', 'widget_type', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(WidgetRole::tableName());
    }

}

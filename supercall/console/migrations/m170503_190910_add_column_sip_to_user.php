<?php
use console\components\db\Migration;

/**
 * Class m170503_190910_add_column_sip_to_user
 */
class m170503_190910_add_column_sip_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'sip', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'sip');
    }
}

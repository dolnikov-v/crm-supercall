<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\modules\catalog\models\Source;
use common\models\Country;
use common\modules\partner\models\Partner;
use console\components\db\Migration;

/**
 * Class m190305_090343_create_table_source_partner_country
 */
class m190305_090343_create_table_source_partner_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%source_partner_country}}', [
            'id' => $this->primaryKey(),
            'source_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, '{{%source_partner_country}}', 'source_id', Source::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%source_partner_country}}', 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%source_partner_country}}', 'partner_id', Partner::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%source_partner_country}}');
    }
}

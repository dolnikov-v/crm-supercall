<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180207_110234_add_translate_release_operator_from_order_form
 */
class m180207_110234_add_translate_release_operator_from_order_form extends Migration
{
    public $phrases = [
        'Вы действительно хотите снять оператора на этого заказа ?' => 'Are you sure you want to remove the operator for this order?',
        'Снять' => 'Release',
        'Оператор снят с заказа.' => 'The operator is removed from the order.',
        'Произошла ошибка при попытки освободить заказ.' => 'An error occurred while trying to free the order.',
        'Список операторов обновлен' => 'List of agents updated',
        'Ошибка обновления списка операторов' => 'Error updating list of operators',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;
use backend\modules\queue\models\Queue;

/**
 * Class m180226_200354_add_column_display_position_to_queue
 */
class m180226_200354_add_column_display_position_to_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Queue::tableName(), 'display_position', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Queue::tableName(), 'display_position');
    }
}

<?php
use console\components\db\Migration;

use backend\models\i18n_source_message;

/**
 * Class m171109_091334_translate_153_addon
 */
class m171109_091334_translate_153_addon extends Migration
{
    public $phrases = [
        'Среднеее время, мин.' => 'Average time, min.',
        'Максимальное время, мин.' => 'The maximum time, min.',
        'Индия' => 'India',
        'Таиланд' => 'Thailand',
        'Мексика' => 'Mexico',
        'Швейцария' => 'Switzerland',
        'Китай' => 'China',
        'Латвия' => 'Latvia',
        'Россия' => 'Russia',
        'Франция' => 'France',
        'Португалия' => 'Portugal',
        'Испания' => 'Spain',
        'Болгария' => 'Bulgaria',
        'Румыния' => 'Romania',
        'Италия' => 'Italy',
        'Германия' => 'Germany',
        'Греция' => 'Greece',
        'Словакия' => 'Slovakia',
        'Чехия' => 'Czech Republic',
        'Венгрия' => 'Hungary',
        'Польша' => 'Poland',
        'Австрия' => 'Austria',
        'Кипр' => 'Cyprus',
        'Индонезия' => 'Indonesia'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

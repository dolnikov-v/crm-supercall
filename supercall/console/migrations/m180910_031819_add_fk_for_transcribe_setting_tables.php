<?php

use console\components\db\Migration;

/**
 * Class m180910_031819_add_fk_for_transcribe_setting_tables
 */
class m180910_031819_add_fk_for_transcribe_setting_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(null, '{{%transcribe_setting}}', 'country_id', '{{%country}}', 'id', self::CASCADE, self::RESTRICT);

        $this->addForeignKey(null, '{{%transcribe_status_set}}', 'transcribe_setting_id', '{{%transcribe_setting}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%transcribe_exception_set}}', 'transcribe_setting_id', '{{%transcribe_setting}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%transcribe_stop_set}}', 'transcribe_setting_id', '{{%transcribe_setting}}', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->getFkName('{{%transcribe_status_set}}', 'transcribe_setting_id'), '{{%transcribe_status_set}}');
        $this->dropForeignKey($this->getFkName('{{%transcribe_exception_set}}', 'transcribe_setting_id'), '{{%transcribe_exception_set}}');
        $this->dropForeignKey($this->getFkName('{{%transcribe_stop_set}}', 'transcribe_setting_id'), '{{%transcribe_stop_set}}');
    }
}

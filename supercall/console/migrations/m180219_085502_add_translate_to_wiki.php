<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180219_085502_add_translate_to_wiki
 */
class m180219_085502_add_translate_to_wiki extends Migration
{
    public $phrases = [
        'Заголовок (на английском языке)' => 'Title (in English)',
        'Язык перевода' => 'Language of translation',
        'Произошла ошибка при добавлении перевода категории: {error}' => 'An error occurred while adding the category translation: {error}',
        'Добавить перевод категории' => 'Add a translation of the category',
        'Редактирование категории' => 'Edit category',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

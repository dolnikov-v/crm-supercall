<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m181119_050624_add_permission_for_order_stats_export
 */
class m181119_050624_add_permission_for_order_stats_export extends Migration
{
    public $phrases = [
        'Экспорт статистики заказов' => 'Export Order Statistics',
        'Тренер продаж' => 'Sales trainer'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        $permission = ['stats.order.export' => 'Экспорт статистики заказов'];
        $this->addPermission($permission);

        $this->addRole([User::ROLE_SALES_TRAINER => 'Тренер продаж']);

        $this->addPermissionToRole('stats.order.export' , User::ROLE_SUPERVISOR);
        $this->addPermissionToRole('stats.order.export' , User::ROLE_CONTROLLER);
        $this->addPermissionToRole('stats.order.export' , User::ROLE_TEAM_LEAD);
        $this->addPermissionToRole('stats.order.export' , User::ROLE_SALES_DIRECTOR);
        $this->addPermissionToRole('stats.order.export' , User::ROLE_QUALITY_MANAGER);
        $this->addPermissionToRole('stats.order.export' , User::ROLE_ADMINISTRATOR);
        $this->addPermissionToRole('stats.order.export' , User::ROLE_SALES_TRAINER);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
        $this->deletePermission('stats.order.export');
        $this->deleteRole(User::ROLE_SALES_TRAINER);
    }
}

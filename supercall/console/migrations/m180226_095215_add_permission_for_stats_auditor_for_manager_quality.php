<?php
use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180226_095215_add_permission_for_stats_auditor_for_manager_quality
 */
class m180226_095215_add_permission_for_stats_auditor_for_manager_quality extends Migration
{
    public $phrases = [
        'Аудит / Статистика по аудиторам' => 'Audit CC / Statistics of auditors',
        'Статистика по аудиторам' => 'Statistics of auditors'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => 'operator.statisticsofauditors.index',
            'type' => '2',
            'description' => 'Аудит / Статистика по аудиторам',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_QUALITY_MANAGER,
            'child' => 'operator.statisticsofauditors.index'
        ));

        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'operator.statisticsofauditors.index', 'parent' => User::ROLE_QUALITY_MANAGER]);
        $this->delete('{{%auth_item}}', ['name' => 'operator.statisticsofauditors.index']);

        i18n_source_message::removeTranslate($this->phrases);
    }
}

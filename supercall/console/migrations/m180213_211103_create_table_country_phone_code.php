<?php
use console\components\db\Migration;
use common\models\Country;
use common\models\CountryPhoneCode;
use yii\helpers\ArrayHelper;

/**
 * Class m180213_211103_create_table_country_phone_code
 */
class m180213_211103_create_table_country_phone_code extends Migration
{
    public $phone_codes = [
        'AT' => ['43',10],
        'BH' => ['973',12,13],
        'BD' => ['880',11,12],
        'BG' => ['359',10,11],
        'CL' => ['56',10],
        'CN' => ['86',11,12],
        'CO' => ['57',10],
        'CY' => ['357',11,12],
        'CZ' => ['420',11,12],
        'FR' => ['33',11],
        'DE' => ['49',11,12],
        'GR' => ['30',11,12],
        'GT' => ['502',11],
        'HK' => ['852',11],
        'HU' => ['36',10],
        'IN' => ['91',10],
        'ID' => ['62',11],
        'IT' => ['39',10],
        'JO' => ['962',11],
        'LV' => ['371',11],
        'MY' => ['60',11,12],
        'MX' => ['52',11],
        'OM' => ['968',10],
        'PK' => ['92',11],
        'PE' => ['51',10],
        'PH' => ['63',11],
        'PL' => ['48',11,12],
        'PT' => ['351',11,12],
        'QA' => ['974',11],
        'RO' => ['40',11],
        'RU' => ['7',12],
        'SA' => ['966',11],
        'SG' => ['65',10],
        'SK' => ['421',12],
        'ZA' => ['27',11],
        'ES' => ['34',11],
        'LK' => ['94',11],
        'CH' => ['41',11],
        'TW' => ['886',11],
        'TH' => ['66',10],
        'TN' => ['216',11],
        'AE' => ['971',10,11],
        'VN' => ['84',10,11]];
    
    
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('country_phone_code', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'phone_code' => $this->integer()->notNull(),
            'phone_length_min' => $this->integer()->notNull(),
            'phone_length_max' => $this->integer()->notNull()
        ]);
        
        $this->createIndex('idx-country_phone_code-phone_code','country_phone_code','phone_code');
        $this->createIndex('idx-country_phone_code-country_id','country_phone_code','country_id');
        
        $this->addForeignKey(null, CountryPhoneCode::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        
        $countries_codes = ArrayHelper::map(Country::find()->asArray()->all(),'char_code','id');
        
        foreach ($countries_codes as $country_code => $country_id) {
            if (!empty($this->phone_codes[$country_code])) {
                $codeData = $this->phone_codes[$country_code];
                $this->insert('country_phone_code', ['country_id' => $country_id,
                                                     'phone_code' => $codeData[0],
                                                     'phone_length_min' => $codeData[1],
                                                     'phone_length_max' => isset($codeData[2]) ? $codeData[2] : $codeData[1]]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('country_phone_code');
    }
    
    
}

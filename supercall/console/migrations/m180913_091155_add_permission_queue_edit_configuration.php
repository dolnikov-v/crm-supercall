<?php
use console\components\db\Migration;

/**
 * Class m180913_091155_add_permission_queue_edit_configuration
 */
class m180913_091155_add_permission_queue_edit_configuration extends Migration
{
    public $permissions = [
        'queue.edit.configuration' => 'Возможность редактирование настроек очереди'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission => $description) {
            $this->insert('{{%auth_item}}', array(
                'name' => $permission,
                'type' => '2',
                'description' => $description,
                'created_at' => time(),
                'updated_at' => time()
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }
    }
}

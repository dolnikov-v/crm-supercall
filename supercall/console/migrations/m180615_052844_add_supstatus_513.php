<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;
use common\modules\order\models\Order;
use common\modules\order\models\OrderSubStatus;
use common\modules\order\models\OrderSubStatusGroup;

/**
 * Class m180615_052844_add_supstatus_513
 */
class m180615_052844_add_supstatus_513 extends Migration
{
    public $phrases = [
        'КС не связалась с клиентом' => "Delivery didn't contacted customer",
        'С клиентом не связались' => "Delivery didn't contacted customer"
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $groupReject = OrderSubStatusGroup::findOne(['id' => Order::SUB_STATUS_GROUP_REJECT]);

        if($groupReject){
            $this->insert(OrderSubStatus::tableName(), [
                'code' => Order::SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER,
                'group_id' => $groupReject->id,
                'name' => 'КС не связалась с клиентом',
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }

        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete(OrderSubStatus::tableName(), [
            'code' => Order::SUB_STATUS_REJECT_DELIVERY_NOT_CONTACTED_CUSTOMER
        ]);

        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;


/**
 * Class m170622_095857_add_column_country_id_in_partner_product_price
 */
class m170622_095857_add_column_country_id_in_partner_product_price extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_product}}', 'country_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_product}}', 'country_id');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170112_125439_order_log_user
 */
class m170112_125439_order_log_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_log}}', 'user_id', $this->integer());
        $this->addForeignKey('fk__order_log__user', '{{%order_log}}', 'user_id', '{{%user}}', 'id', 'set null', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk__order_log__user', '{{%order_log}}');
        $this->dropColumn('{{%order_log}}', 'user_id');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170126_084927_partner_settings
 */
class m170126_084927_partner_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_settings}}', [
            'id' => $this->primaryKey(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'genesys_sync_on_save' => $this->boolean()
        ]);
        $this->addForeignKey('fk__partner_settings__partner', '{{%partner_settings}}', 'partner_id', '{{%partner}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk__partner_settings__country', '{{%partner_settings}}', 'country_id', '{{%country}}', 'id', 'restrict', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_settings}}');
    }
}

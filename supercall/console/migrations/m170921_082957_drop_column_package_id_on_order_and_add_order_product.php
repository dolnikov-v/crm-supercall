<?php
use console\components\db\Migration;
use common\modules\order\models\Order;
use common\modules\order\models\OrderProduct;
use common\modules\partner\models\Package;

/**
 * Class m170921_082957_drop_column_package_id_on_order_and_add_order_product
 */
class m170921_082957_drop_column_package_id_on_order_and_add_order_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(Order::tableName(), 'package_id');
        $this->addColumn(OrderProduct::tableName(), 'package_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(null, OrderProduct::tableName(), 'package_id', Package::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(OrderProduct::tableName(), 'package_id');
        $this->addColumn(Order::tableName(), 'package_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(null, Order::tableName(), 'package_id', Package::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }
}

<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180220_112448_add_translation_queues
 */
class m180220_112448_add_translation_queues extends Migration
{
    public $phrases = [
        'Очереди' => 'Queues'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

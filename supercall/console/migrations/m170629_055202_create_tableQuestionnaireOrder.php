<?php
use console\components\db\Migration;
use backend\modules\operator\models\QuestionnaireOrder;
use backend\modules\operator\models\QuestionnaireBase;
use common\modules\order\models\Order;

/**
 * Class m170629_055202_create_tableQuestionnaireOrder
 */
class m170629_055202_create_tableQuestionnaireOrder extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(QuestionnaireOrder::tableName(), [
            'id' => $this->primaryKey(),
            'questionnaire_base_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'order_status' => $this->integer()->notNull(),
            'duration_time' => $this->integer(),
            'is_finished' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, QuestionnaireOrder::tableName(), 'questionnaire_base_id', QuestionnaireBase::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, QuestionnaireOrder::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(QuestionnaireOrder::tableName());
    }
}

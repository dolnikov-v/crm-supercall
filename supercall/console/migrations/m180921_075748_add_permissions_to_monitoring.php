<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180921_075748_add_permissions_to_monitoring
 */
class m180921_075748_add_permissions_to_monitoring extends Migration
{
    public $phrases = [
        'Администрирование / Мониторинг' => 'Administration / Monitoring',
        'Мониторинг' => 'Monitoring',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        $this->insert('{{%auth_item}}', array(
            'name' => 'administration.monitoring.index',
            'type' => '2',
            'description' => 'Администрирование / Мониторинг',
            'created_at' => time(),
            'updated_at' => time()
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding show_countries to table `partner_form`.
 */
class m170525_184707_add_show_countries_column_to_partner_form_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('partner_form', 'show_countries', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('partner_form', 'show_countries');
    }
}

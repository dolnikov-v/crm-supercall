<?php
use console\components\db\Migration;

/**
 * Class m170523_101017_add_column_queue
 */
class m170523_101017_add_column_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%queue}}', 'is_primary', $this->boolean());
        $this->execute('ALTER TABLE {{%queue}} ADD primary_components JSONB;');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%queue}}', 'primary_components');
        $this->dropColumn('{{%queue}}', 'is_primary');
    }
}

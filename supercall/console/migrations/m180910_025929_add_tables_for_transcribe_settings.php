<?php

use console\components\db\Migration;

/**
 * Class m180910_025929_add_tables_for_transcribe_settings
 */
class m180910_025929_add_tables_for_transcribe_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $date_fields = [
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ];

        $this->createTable('{{%transcribe_setting}}', array_merge([
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'active' => $this->boolean()->defaultValue(true)
        ], $date_fields));

        $this->createTable('{{%transcribe_status_set}}', array_merge([
            'id' => $this->primaryKey(),
            'transcribe_setting_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()
        ], $date_fields));

        $this->createTable('{{%transcribe_exception_set}}', array_merge([
            'id' => $this->primaryKey(),
            'transcribe_setting_id' => $this->integer()->notNull(),
            'text' => $this->string()
        ], $date_fields));

        $this->createTable('{{%transcribe_stop_set}}', array_merge([
            'id' => $this->primaryKey(),
            'transcribe_setting_id' => $this->integer()->notNull(),
            'text' => $this->string()
        ], $date_fields));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%transcribe_setting}}');
        $this->dropTable('{{%transcribe_status_set}}');
        $this->dropTable('{{%transcribe_stop_set}}');
        $this->dropTable('{{%transcribe_exception_set}}');
    }
}

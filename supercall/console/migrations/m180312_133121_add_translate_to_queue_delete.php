<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180312_133121_add_translate_to_queue_delete
 */
class m180312_133121_add_translate_to_queue_delete extends Migration
{
    public $phrases = [
        'Смена очереди' => 'Change of queue',
        'Первичные очереди' => 'Primary queues',
        'Вторичные очереди' => 'Secondary queues',
        'Исходная очередь №{id} не найдена' => 'Initial queue No. {id} not found',
        'Очередь №{id} удалена' => 'Queue # {id} was deleted',
        'Очередь №{id} не удалена: {error}' => 'Queue # {id} has not been deleted: {error}',
        'Очередь №{id} не найдена' => 'Queue # {id} not found',
        'Очередь удалена, заказы перемещены. Обновление списка очередей ...' => 'The queue is deleted, orders are moved. Updating the queue list ...',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

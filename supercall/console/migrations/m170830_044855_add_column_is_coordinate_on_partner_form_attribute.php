<?php
use console\components\db\Migration;
use common\modules\partner\models\PartnerFormAttribute;

/**
 * Class m170830_044855_add_column_is_coordinate_on_partner_form_attribute
 */
class m170830_044855_add_column_is_coordinate_on_partner_form_attribute extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(PartnerFormAttribute::tableName(), 'is_coordinate', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(PartnerFormAttribute::tableName(), 'is_coordinate');
    }
}

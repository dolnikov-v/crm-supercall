<?php
use common\modules\order\models\OrderType;
use console\components\db\Migration;

/**
 * Class m180119_043823_replace_order_type_11_replace_4
 */
class m180119_043823_replace_order_type_11_replace_4 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $orderType11 = OrderType::findOne(['name' => "Отправить на проверку в КЦ (Невыкуп)"]);
        if($orderType11) {
            $orderType11->delete();
        }

        $query = yii::$app->db->createCommand("update queue set primary_components = cast(REPLACE(primary_components::varchar, '\"primary_type\": \"".OrderType::TYPE_CHECK_UNDELIVERY."\"', '\"primary_type\": \"".OrderType::TYPE_NON_PURCHASE."\"') as jsonb)  where primary_components::varchar like '%\"primary_type\": \"".OrderType::TYPE_CHECK_UNDELIVERY."\"%'");
        $query->execute();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}

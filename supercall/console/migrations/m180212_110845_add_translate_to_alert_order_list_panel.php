<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180212_110845_add_translate_to_alert_order_list_panel
 */
class m180212_110845_add_translate_to_alert_order_list_panel extends Migration
{
    public $phrases = [
        '<b>Внимание!</b><br/> При использовании фильтра <i>"Оператор"</i> совместно с фильтром <i>"Тип даты" = Дата обновления</i> в столбце статуса заказа будут отражены исторические данные (статусы заказов на период)<br/>Если совместно с фильтром <i>"Оператор"</i> будет использоваться фильтр <i>"Тип даты" = Дата создания в CRM</i>, то в столбце будут отражены текущие (актуальные) статусы заказов'
        => '<b> Warning: </b> <br/> If you use the <i> "Operator" </i> filter along with the <i> Date Type = Update Date</i> filter, the historical status column will display historical data (status of orders for a period) <br/> If the filter <i> "Date type" = Date of creation in the CRM </i> is used in conjunction with the <i> "Operator" filter </i>, the column will display current (actual) statuses of orders'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true, 'common');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

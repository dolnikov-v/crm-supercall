<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180313_095923_translate_modal_address_verification
 */
class m180313_095923_translate_modal_address_verification extends Migration
{
    public $phrases = [
        'Сохранить и подтвердить' => 'Save and confirm',
        'Отмена' => 'Cancel',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

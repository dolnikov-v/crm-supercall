<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180213_205226_add_translate_modal_incoming
 */
class m180213_205226_add_translate_modal_incoming extends Migration
{
    public $phrases = [
        'Входящий звонок' => 'Incoming call',
        'Принять' => 'Accept',
    ];
  
    /**
    * @inheritdoc
    */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }
  
    /**
    * @inheritdoc
    */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

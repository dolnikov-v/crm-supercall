<?php
use console\components\db\Migration;

/**
 * Class m170206_072730_tbl_queue
 */
class m170206_072730_tbl_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%queue}}', [
            'id'                 => $this->primaryKey(),
            'name'               => $this->string()->notNull(),
            'description'        => $this->text(),
            'priority'           => $this->integer()->defaultValue(1),
            'priority_selection' => $this->smallInteger()->defaultValue(1),
            'active'             => $this->smallInteger(1)->defaultValue(1),
            'strategy'           => $this->text(),
            'created_at'         => $this->integer(),
            'updated_at'         => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%queue}}');
    }
}

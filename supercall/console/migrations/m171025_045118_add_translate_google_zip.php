<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171025_045118_add_translate_google_zip
 */
class m171025_045118_add_translate_google_zip extends Migration
{
    public $phrases = [

        'Сервис Google по данному адресу определил индекс как <b>{google_zip}</b>, но вы использовали индекс <b>{cusomer_zip}</b>, заменить указанный вами индекс ?' =>
        'Google service at this address defined the index as <b> {google_zip} </b>, but you used the <b> {cusomer_zip} </b> index, replace the index you specified?',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

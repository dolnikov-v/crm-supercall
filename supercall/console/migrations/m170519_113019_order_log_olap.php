<?php
use console\components\db\Migration;

/**
 * Class m170519_113019_order_log_olap
 */
class m170519_113019_order_log_olap extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_log_olap_general}}', [
            'period_type' => $this->smallInteger(1)->notNull(),
            'period' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull() . ' DEFAULT 0',
            'price_data' => 'jsonb'
        ]);
        $this->addKeys('{{%order_log_olap_general}}', 'olog');
    }

    protected function addKeys($table, $prefix)
    {
        $this->addForeignKey(
            'fk_'.$prefix.'_product',
            $table,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_'.$prefix.'_type',
            $table,
            'type_id',
            '{{%order_type}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_'.$prefix.'_partner',
            $table,
            'partner_id',
            '{{%partner}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_'.$prefix.'_country',
            $table,
            'country_id',
            '{{%country}}',
            'id',
            'CASCADE'
        );

        $this->addPrimaryKey('pk_'.$prefix, $table, [
            'period_type',
            'country_id',
            'type_id',
            'product_id',
            'partner_id',
            'status',
            'period',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_log_olap_general}}');
    }
}

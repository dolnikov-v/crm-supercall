<?php
use console\components\db\Migration;

/**
 * Class m000000_000009_currency_rate
 */
class m000000_000009_currency_rate extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%currency_rate}}', [
            'id' => $this->primaryKey(),
            'currency_id' => $this->integer()->notNull(),
            'rate' => $this->decimal(10, 4)->defaultValue(null),
            'ask' => $this->decimal(10, 4)->defaultValue(null),
            'bid' => $this->decimal(10, 4)->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, '{{%currency_rate}}', 'currency_id', '{{%currency}}', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency_rate}}');
    }
}

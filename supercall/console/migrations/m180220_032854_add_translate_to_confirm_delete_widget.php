<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180220_032854_add_translate_to_confirm_delete_widget
 */
class m180220_032854_add_translate_to_confirm_delete_widget extends Migration
{
    public $phrases = [
        'Подтверждение удаления' => 'Confirm removal',
        'Вы действительно хотите удалить данную запись?' => 'Are you sure you want to delete this entry?',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

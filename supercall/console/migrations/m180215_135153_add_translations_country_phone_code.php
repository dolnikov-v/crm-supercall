<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m180215_135153_add_translations_country_phone_code
 */
class m180215_135153_add_translations_country_phone_code extends Migration
{
    public $phrases = [
        'ID страны' => 'Country ID',
        'Телефонный код' => 'Phone code',
        'Минимальная длина номера' => 'Minimal phone length',
        'Максимальная длина номера' => 'Maximal phone length',
        ''
    ];
    
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }
    
    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

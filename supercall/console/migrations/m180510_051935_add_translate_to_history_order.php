<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m180510_051935_add_translate_to_history_order
 */
class m180510_051935_add_translate_to_history_order extends Migration
{
    public $phrases = [
        'Пользователь' => 'User',

    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

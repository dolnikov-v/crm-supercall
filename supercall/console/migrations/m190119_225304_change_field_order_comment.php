<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use common\modules\order\models\Order;
use console\components\db\Migration;

/**
 * Class m190119_225304_change_field_order_comment
 */
class m190119_225304_change_field_order_comment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(Order::tableName(), 'order_comment', $this->string(1500));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(Order::tableName(), 'order_comment', $this->string(255));
    }
}

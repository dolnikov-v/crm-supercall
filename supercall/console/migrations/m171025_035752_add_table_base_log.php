<?php
use console\components\db\Migration;
use common\models\Logger;

/**
 * Class m171025_035752_add_table_base_log
 */
class m171025_035752_add_table_base_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(Logger::tableName(), [
            'id' => $this->primaryKey(),
            'route' => $this->string(),
            'action' => $this->string(),
            'type' => $this->string(),
            'tags' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(Logger::tableName());
    }
}

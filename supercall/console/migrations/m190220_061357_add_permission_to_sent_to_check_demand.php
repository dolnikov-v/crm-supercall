<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use console\components\db\Migration;

/**
 * Class m190220_061357_add_permission_to_sent_to_check_demand
 */
class m190220_061357_add_permission_to_sent_to_check_demand extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $permission = ['order.groupoperations.senttocheckdemand' => 'Групповая операция "отправить в очередь Check demand"'];
        $this->addPermission($permission);

        $this->addPermissionToRole('order.groupoperations.senttocheckdemand', \common\models\User::ROLE_ADMINISTRATOR);
        $this->addPermissionToRole('order.groupoperations.senttocheckdemand', \common\models\User::ROLE_CURATOR);
        $this->addPermissionToRole('order.groupoperations.senttocheckdemand', \common\models\User::ROLE_CONTROLLER);
        $this->addPermissionToRole('order.groupoperations.senttocheckdemand', \common\models\User::ROLE_LOGISTICIAN);
        $this->addPermissionToRole('order.groupoperations.senttocheckdemand', \common\models\User::ROLE_SUPERVISOR);
        $this->addPermissionToRole('order.groupoperations.senttocheckdemand', \common\models\User::ROLE_TEAM_LEAD);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->deletePermission('order.groupoperations.senttocheckdemand');
    }
}

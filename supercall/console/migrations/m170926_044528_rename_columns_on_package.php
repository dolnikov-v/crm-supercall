<?php
use console\components\db\Migration;
use common\modules\partner\models\Package;

/**
 * Class m170926_044528_rename_columns_on_package
 */
class m170926_044528_rename_columns_on_package extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn(Package::tableName(), 'country', 'country_id');
        $this->renameColumn(Package::tableName(), 'partner', 'partner_id');
        $this->renameColumn(Package::tableName(), 'product', 'product_id');

        $this->dropForeignKey($this->getFkName(Package::tableName(), 'country'), Package::tableName());
        $this->dropForeignKey($this->getFkName(Package::tableName(), 'partner'), Package::tableName());
        $this->dropForeignKey($this->getFkName(Package::tableName(), 'product'), Package::tableName());


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn(Package::tableName(), 'country_id', 'country');
        $this->renameColumn(Package::tableName(), 'partner_id', 'partner');
        $this->renameColumn(Package::tableName(), 'product_id', 'product');
    }
}

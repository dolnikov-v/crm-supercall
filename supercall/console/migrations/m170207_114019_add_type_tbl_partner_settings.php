<?php
use console\components\db\Migration;

/**
 * Class m170207_114019_add_type_tbl_partner_settings
 */
class m170207_114019_add_type_tbl_partner_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_settings}}', 'type_id', $this->integer()->null());
        $this->addForeignKey('fk-partner_settings-type_id', '{{%partner_settings}}', 'type_id', '{{%order_type}}', 'id', self::CASCADE, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-partner_settings-type_id', '{{%partner_settings}}');
        $this->dropColumn('{{%partner_settings}}', 'type_id');
    }
}

<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m180111_035704_translate_from_price_shipping
 */
class m180111_035704_translate_from_price_shipping extends Migration
{
    public $phrases = [
        'Список курьерских служб обновлен' => 'List of courier services has been updated',
        'Список доступных курьерских служб' => 'List of available courier services',
        'Тариф доставки' => 'Delivery rate',
        'по стране' => 'around the country'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

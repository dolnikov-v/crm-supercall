<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180221_032217_add_translate_to_wiki
 */
class m180221_032217_add_translate_to_wiki extends Migration
{
    public $phrases = [
        'Статья' => 'Article',
        'Редактирование статьи' => 'Editing an article',
        'Сохранить статью' => 'Save article',
        'Добавление статьи' => 'Adding an article',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

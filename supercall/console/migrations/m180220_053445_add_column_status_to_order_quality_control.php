<?php
use console\components\db\Migration;
use backend\modules\operator\models\QualityControl;

/**
 * Class m180220_053445_add_column_status_to_order_quality_control
 */
class m180220_053445_add_column_status_to_order_quality_control extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(QualityControl::tableName(), 'operator_status', $this->integer());
        $this->addColumn(QualityControl::tableName(), 'auditor_status', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(QualityControl::tableName(), 'operator_status');
        $this->dropColumn(QualityControl::tableName(), 'auditor_status');
    }
}

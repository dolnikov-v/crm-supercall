<?php
use console\components\db\Migration;

/**
 * Class m170531_153023_olap_product_id
 */
class m170531_153023_olap_product_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_oog_product', '{{%order_olap_general}}');
        $this->dropForeignKey('fk_ooa_product', '{{%order_olap_attempts}}');
        $this->dropForeignKey('fk_ooap_product', '{{%order_olap_approved}}');
        $this->dropForeignKey('fk_oor_product', '{{%order_olap_reject}}');
        $this->dropForeignKey('fk_oot_product', '{{%order_olap_trash}}');

        $this->dropForeignKey('fk_olog_product', '{{%order_log_olap_general}}');
        $this->dropForeignKey('fk_oloap_product', '{{%order_log_olap_approved}}');
        $this->dropForeignKey('fk_olor_product', '{{%order_log_olap_reject}}');
        $this->dropForeignKey('fk_olot_product', '{{%order_log_olap_trash}}');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addForeignKey(
            'fk_oog_product',
            '{{%order_olap_general}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_ooa_product',
            '{{%order_olap_attempts}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_ooap_product',
            '{{%order_olap_approved}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_oor_product',
            '{{%order_olap_reject}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_oot_product',
            '{{%order_olap_trash}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );


        $this->addForeignKey(
            'fk_olog_product',
            '{{%order_log_olap_general}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_oloap_product',
            '{{%order_log_olap_approved}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_olor_product',
            '{{%order_log_olap_reject}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_olor_product',
            '{{%order_log_olap_trash}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
    }
}

<?php
use console\components\db\Migration;
use common\models\User;

/**
 * Class m170919_114920_add_permission_for_partner_packages
 */
class m170919_114920_add_permission_for_partner_packages extends Migration
{
    public $permissions = [
        'partner.package.index',
        'partner.package.edit',
        'partner.package.activate',
        'partner.package.deactivate',
        'partner.package.delete'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {
            $this->insert('{{%auth_item}}', array(
                'name' => $permission,
                'type' => '2',
                'description' => $permission,
                'created_at' => time(),
                'updated_at' => time()
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_SUPERVISOR,
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_CURATOR,
                'child' => $permission
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->permissions as $permission){
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_SUPERVISOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_CURATOR]);

            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }
    }
}

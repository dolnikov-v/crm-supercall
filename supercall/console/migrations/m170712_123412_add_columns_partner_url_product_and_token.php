<?php
use console\components\db\Migration;
use common\modules\partner\models\Partner;

/**
 * Class m170712_123412_add_columns_partner_url_product_and_token
 */
class m170712_123412_add_columns_partner_url_product_and_token extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Partner::tableName(), 'url_product', $this->string()->defaultValue(''));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Partner::tableName(), 'url_product');
    }
}

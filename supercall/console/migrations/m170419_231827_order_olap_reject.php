<?php
use console\components\db\Migration;

/**
 * Class m170419_231827_order_olap_reject
 */
class m170419_231827_order_olap_reject extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_olap_reject}}', [
            'period_type' => $this->smallInteger(1)->notNull(),
            'period' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),

            'expensive' => $this->integer()->notNull() . ' DEFAULT 0',
            'changed_mind' => $this->integer()->notNull() . ' DEFAULT 0',
            'medical' => $this->integer()->notNull() . ' DEFAULT 0',
            'no_comments' => $this->integer()->notNull() . ' DEFAULT 0',
            'auto' => $this->integer()->notNull() . ' DEFAULT 0',
            'quantity' => $this->integer()->notNull() . ' DEFAULT 0',
        ]);

        $this->addForeignKey(
            'fk_oor_product',
            '{{%order_olap_reject}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_oor_type',
            '{{%order_olap_reject}}',
            'type_id',
            '{{%order_type}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_oor_partner',
            '{{%order_olap_reject}}',
            'partner_id',
            '{{%partner}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_oor_country',
            '{{%order_olap_reject}}',
            'country_id',
            '{{%country}}',
            'id',
            'CASCADE'
        );

        $this->addPrimaryKey('pk_oor', '{{%order_olap_reject}}', [
            'period_type',
            'period',
            'type_id',
            'product_id',
            'partner_id',
            'country_id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_olap_reject}}');
    }
}

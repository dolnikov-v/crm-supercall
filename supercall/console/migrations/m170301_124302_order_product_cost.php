<?php
use console\components\db\Migration;

/**
 * Class m170301_124302_order_product_cost
 */
class m170301_124302_order_product_cost extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_product}}', 'cost', $this->double());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order_product}}', 'cost');
    }
}

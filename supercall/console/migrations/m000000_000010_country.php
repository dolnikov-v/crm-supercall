<?php
use common\models\Country;
use console\components\db\Migration;

/**
 * Class m000000_000010_country
 */
class m000000_000010_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%country}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull(),
            'slug' => $this->string(3)->notNull(),
            'char_code' => $this->string(3)->defaultValue(null),
            'active' => $this->smallInteger()->defaultValue(null),
            'timezone_id' => $this->integer()->defaultValue(null),
            'currency_id' => $this->integer()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, '{{%country}}', 'timezone_id', '{{%timezone}}', 'id', Migration::CASCADE, Migration::RESTRICT);
        $this->addForeignKey(null, '{{%country}}', 'currency_id', '{{%currency}}', 'id', Migration::CASCADE, Migration::RESTRICT);

        foreach ($this->fixtures() as $key => $fixture) {
            $country = new Country($fixture);
            $country->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%country}}');
    }

    /**
     * @return array
     */
    private function fixtures()
    {
        return [
            [
                'name' => 'Индия',
                'slug' => 'in',
                'char_code' => 'IN',
                'active' => 1,
            ],
            [
                'name' => 'Таиланд',
                'slug' => 'th',
                'char_code' => 'TH',
                'active' => 1,
            ],
            [
                'name' => 'Мексика',
                'slug' => 'mx',
                'char_code' => 'MX',
                'active' => 1,
            ],
        ];
    }
}

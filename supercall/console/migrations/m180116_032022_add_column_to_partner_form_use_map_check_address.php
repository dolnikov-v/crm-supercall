<?php
use console\components\db\Migration;
use common\modules\partner\models\PartnerForm;

/**
 * Class m180116_032022_add_column_to_partner_form_use_map_check_address
 */
class m180116_032022_add_column_to_partner_form_use_map_check_address extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(PartnerForm::tableName(), 'use_map', $this->boolean()->defaultValue(true));
        $this->addColumn(PartnerForm::tableName(), 'check_address', $this->boolean()->defaultValue(true));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(PartnerForm::tableName(), 'use_map');
        $this->dropColumn(PartnerForm::tableName(), 'check_address');
    }
}

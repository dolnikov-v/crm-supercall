<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180326_094840_add_broker_flag_to_form
 */
class m180326_094840_add_broker_flag_to_form extends Migration
{
    public $phrases = [
        'Доставка доступна с {delivery_from}' => 'Delivery available from {delivery_from}',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('partner_form', 'use_foreign_delivery_broker', $this->boolean());
        $this->addColumn('partner', 'url_foreign_delivery_broker', $this->string(255));

        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('partner_form', 'use_foreign_delivery_broker');
        $this->dropColumn('partner', 'url_foreign_delivery_broker');
        i18n_source_message::removeTranslate($this->phrases);
    }
}

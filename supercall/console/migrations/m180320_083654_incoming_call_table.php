<?php
use console\components\db\Migration;

/**
 * Class m180320_083654_incoming_call_table
 */
class m180320_083654_incoming_call_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user', 'incoming_call', $this->boolean()->defaultValue(false));
        $this->createTable('incoming_call', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(15),
            'user_sip' => $this->string(15),
            'status' => $this->integer(2),
            'cdr_id' => $this->double(),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'incoming_call');
        $this->dropTable('incoming_call');
    }
}

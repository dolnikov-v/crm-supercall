<?php
use console\components\db\Migration;
use backend\modules\administration\models\BaseLogger;

/**
 * Class m180312_081508_add_column_old_tags_model_to_base_log
 */
class m180312_081508_add_column_old_tags_model_to_base_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(BaseLogger::tableName(), 'model', $this->string(255));
        $this->addColumn(BaseLogger::tableName(), 'old_tags', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(BaseLogger::tableName(), 'model');
        $this->dropColumn(BaseLogger::tableName(), 'old_tags');
    }
}

<?php
use console\components\db\Migration;
use common\models\User;

/**
 * Class m171204_062025_add_new_roles
 */
class m171204_062025_add_new_roles extends Migration
{

    public $permissions = [
        User::ROLE_SENIOR_OPERATOR => [
            "profile.control",
            "home.index.index",
            "order.index.index",
            "order.change.index",
            "order.index.view",
            "stats.order.index",
            "stats.log.general",
            "stats.log.list",
            "stats.log.details",
            "stats.product.index",
            "stats.operator.index",
            "stats.orderprocessingtime.dates",
            "stats.orderprocessingtime.orders",
            "stats.orderprocessingtime.statuses",
            "stats.orderprocessingtime.products"
        ],
        User::ROLE_TEAM_LEAD => [
            "profile.control",
            "home.index.index",
            "order.index.index",
            "order.change.index",
            "order.index.view",
            "stats.order.index",
            "stats.log.general",
            "stats.log.list",
            "stats.log.details",
            "stats.product.index",
            "stats.operator.index",
            "stats.orderprocessingtime.dates",
            "stats.orderprocessingtime.orders",
            "stats.orderprocessingtime.statuses",
            "stats.orderprocessingtime.products",
            "queue.control.clear",
            "queue.control.index",
            "operator.listoperators.queue",
            "operator.listoperators.queueset",
            "operator.listoperators.queueunset"
        ]
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $auth = $this->authManager;
        $role = $auth->createRole(User::ROLE_SENIOR_OPERATOR);
        $role->description = 'Старший оператор';
        $auth->add($role);

        $auth = $this->authManager;
        $role = $auth->createRole(User::ROLE_TEAM_LEAD);
        $role->description = 'Тимлид';
        $auth->add($role);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->permissions as $permission){
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_SENIOR_OPERATOR]);

            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }

        $auth = $this->authManager;
        $role = $auth->getRole(User::ROLE_SENIOR_OPERATOR);
        $auth->remove($role);

        foreach($this->permissions as $permission){
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_TEAM_LEAD]);

            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }

        $auth = $this->authManager;
        $role = $auth->getRole(User::ROLE_TEAM_LEAD);
        $auth->remove($role);
    }
}

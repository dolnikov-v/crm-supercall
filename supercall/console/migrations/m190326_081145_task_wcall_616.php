<?php

/**
 * Создание роли (с проверкой на наличие)
 * $role = [name => description]
 * $this->addRole(array $role)
 *
 * Удаление роли
 * $this-deleteRole(string $role)
 *
 * Проверка наличия роли
 * $this->existsRole(string $role_name);
 *
 * Создание пермишена (с проверкой на наличие)
 * $permission = [name => description]
 * $this->addPermission(array $permission)
 *
 * Удаление пермишена (автоматом удаляются все связки по ролям)
 * $this->deletePermission(string $permission);
 *
 * Проверка наличия пермишена
 * $this->>existsPermission(string $permission_name)
 *
 * Добавление пермишена к роли
 * $this->addPermissionToRole(string $permission, string $role)
 *
 * Удаления пермишена у роли
 * $this->deletePermissionByRole(string $permission, string $role)
 *
 * Добавление перевода (удаление перед добавление уже внутри метода)
 * i18n_source_message::addTranslate($this->phrases, true);
 *
 * Удаление переводов
 * i18n_source_message::removeTranslate($this->phrases);
 */

use backend\models\i18n_source_message;
use backend\modules\queue\models\Queue;
use common\models\Country;
use common\models\Currency;
use common\models\Product;
use common\models\Timezone;
use common\modules\order\models\Order;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use common\modules\partner\models\PartnerProduct;
use console\components\db\Migration;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;

/**
 * Class m190326_081145_task_wcall_616
 */
class m190326_081145_task_wcall_616 extends Migration
{

    public $phrases = [
        'Новая Зеландия' => 'New Zealand',
        'Канада' => 'Canada',
        'Австралия' => 'Australia'
    ];

    public $products = [
        'NZ' => [
            [
                'name' => 'Black latte ',
                'partner_product_id' => 134,
                'price' => 45
            ],
        ],
        'CA' => [
            [
                'name' => 'Black latte ',
                'partner_product_id' => 134,
                'price' => 45
            ],
        ],
        'AU' => [
            [
                'name' => 'Black latte ',
                'partner_product_id' => 134,
                'price' => 45
            ],
        ]
    ];

    /**
     * @var Partner
     */
    public $partner;

    public $listNewCountries = [
        'NZ' => 'Новая Зеландия',
        'CA' => 'Канада',
        'AU' => 'Австралия'
    ];

    public $listTimezonesCountries = [
        'NZ' => '+12:00',
        'CA' => '-07:00',
        'AU' => '+08:00'
    ];

    /** @var  Currency */
    public $currency;

    /** @var  array */
    public $language;

    public $addressAttributes = [
        [
            'name' => 'customer_state',
            'label' => 'State',
            'required' => true,
            'is_klader' => true,
        ],
        [
            'name' => 'customer_city',
            'label' => 'City',
            'required' => true,
            'is_klader' => true,
        ],
        [
            'name' => 'customer_district',
            'label' => 'District',
            'required' => false,
        ],
        [
            'name' => 'customer_street',
            'label' => 'Street',
            'required' => false,
        ],
        [
            'name' => 'customer_house_number',
            'label' => 'House number',
            'required' => false,
        ],
        [
            'name' => 'customer_zip',
            'label' => 'Postal code',
            'required' => true,
            'is_klader' => true,
        ],

    ];

    public function init()
    {
        parent::init();

        $this->currency = Currency::findOne(['char_code' => 'USD']);
        $this->language = yii::$app->db->createCommand("SELECT * FROM i18n_language WHERE locale='en-US'")->queryOne();
        $this->partner = Partner::findOne(['name' => '2wtrade']);
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        $productData = [];

        //create countries
        foreach (array_keys($this->listNewCountries) as $char_code) {
            $partnerProductIds = [];
            $country = Country::findOne(['char_code' => $char_code]);

            if (!$country) {
                $timezone_string = "(GMT {$this->listTimezonesCountries[$char_code]})%";
                $timezone = Timezone::find()->where("name like '{$timezone_string}'")->one();

                if (!$timezone) {
                    $timezone = Timezone::findOne("name like '(GMT +00:00)'%");
                }

                //создать страну - если её нет
                $country = new Country([
                    'name' => $this->listNewCountries[$char_code],
                    'slug' => strtolower($char_code),
                    'char_code' => $char_code,
                    'active' => Country::ACTIVE,
                    'timezone_id' => $timezone->id,
                    'currency_id' => $this->currency->id,
                    'language_id' => $this->language['id'],
                    'order_hold_verification' => false,
                    'klader_in_all_string' => false,
                ]);

                $country->save();
            }

            //создать формы для новыйх заказов
            $form = new PartnerForm([
                'partner_id' => $this->partner->id,
                'name' => $country->name,
                'active' => true,
                'country_id' => $country->id,
                'type' => OrderType::TYPE_NEW,
                'buttons' => [
                    Order::STATUS_RECALL,
                    Order::STATUS_FAIL,
                    Order::STATUS_APPROVED,
                    Order::STATUS_REJECTED,
                    Order::STATUS_TRASH,
                    Order::STATUS_DOUBLE
                ],
                'show_shipping' => false,
                'allow_gifts' => true,
                'show_countries' => true,
                'use_map' => true,
                'check_address' => true,
                'use_foreign_delivery_broker' => true,
                'disabled_buttons' => true
            ]);

            if ($form->save()) {
                //дополнить форму адресными атрибутами
                foreach ($this->addressAttributes as $k => $attribute) {
                    $attribute['sort'] = $k++;
                    $attribute['type'] = 'string';
                    $attribute['scope'] = 'address';
                    $partnerFormAttribute = new PartnerFormAttribute($attribute);
                    $partnerFormAttribute->link('form', $form);
                }

                $partnerProductIds[$country->id] = [];


                //создать товары
                if (isset($this->products[$char_code])) {

                    foreach ($this->products[$char_code] as $product) {

                        //найти или создать товар
                        $productNew = Product::findOne(['name' => $product['name']]);

                        if (!$productNew && !isset($productData[$product['name']])) {
                            $productNew = new Product([
                                'name' => $product['name'],
                                'active' => Product::ACTIVE
                            ]);

                            $productNew->save();

                            Console::output('Product: ' . $product['name'] . 'created: id ' . $productNew->id);
                        } else {
                            Console::output('Product: ' . $product['name'] . 'founded: id ' . ($productNew ? $productNew->id : $productData[$product['name']]));
                        }

                        $productData[$product['name']] = ($productNew ? $productNew->id : $productData[$product['name']]);

                        //найти или создать партнёрский товар
                        $partnerProduct = PartnerProduct::findOne([
                            'partner_id' => $this->partner->id,
                            'partner_product_id' => $product['partner_product_id'],
                            'product_id' => $productData[$product['name']],
                            'country_id' => $country->id
                        ]);

                        if (!$partnerProduct) {
                            $partnerProduct = new PartnerProduct([
                                'partner_id' => $this->partner->id,
                                'partner_product_id' => $product['partner_product_id'],
                                'product_id' => $productData[$product['name']],
                                'country_id' => $country->id,
                                'active' => PartnerProduct::ACTIVE,
                                'price_data' => [
                                    'type' => PartnerProduct::PRICE_DATA_TYPE_STATIC,
                                    'price' => $product['price']
                                ]
                            ]);

                            $partnerProduct->save();
                        }

                        $partnerProductIds[$country->id][] = $partnerProduct->id;

                    }
                }

                //создать первичную очередь с минимальными настройками
                $queue = new Queue([
                    'name' => yii::t('common', $country->name) . ' test queue',
                    'priority' => 1,
                    'priority_selection' => 1,
                    'strategy' => '{"3":{"strategyCount":3,"blockTime":30,"changeStatus":5}}',
                    'is_primary' => true,
                    'primary_type' => strval(OrderType::TYPE_NEW),
                    'primary_country' => $country->id,
                    'primary_partner' => strval($this->partner->id),
                    'primary_product' => array_map('strval', array_values($productData))
                ]);

                $queue->save();

            }
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $countries = Country::findAll(['char_code' => array_keys($this->listNewCountries)]);
        $country_ids = ArrayHelper::getColumn($countries, 'id');

        PartnerForm::deleteAll(['country_id' => $country_ids]);
        Country::deleteAll(['char_code' => array_keys($this->listNewCountries)]);

        foreach ($country_ids as $country_id) {
            $queues = Queue::find()->byCountry($country_id, false)->all();
            foreach ($queues as $queue) {
                $queue->delete();
            }
        }
    }
}

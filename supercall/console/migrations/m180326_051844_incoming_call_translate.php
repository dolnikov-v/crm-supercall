<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180326_051844_incoming_call_translate
 */
class m180326_051844_incoming_call_translate extends Migration
{
    public $phrases = [
        'Результаты поиска' => 'Searching results',
        'Полное имя заказчика' => 'Full name client',
        'Завершить работу с заказом' => 'Finish work with the order',
        'Завершить работу с заказом?' => 'Finish work with the order?',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

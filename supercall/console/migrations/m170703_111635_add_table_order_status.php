<?php
use console\components\db\Migration;

/**
 * Class m170703_111635_add_table_order_status
 */
class m170703_111635_add_table_order_status extends Migration
{
    /**
     * @inheritdoc
     */

    public function safeUp()
    {
        $this->createTable('{{%order_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ], $this->tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('{{%order_status}}');
    }

}
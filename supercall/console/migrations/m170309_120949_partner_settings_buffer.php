<?php
use console\components\db\Migration;

/**
 * Class m170309_120949_partner_settings_buffer
 */
class m170309_120949_partner_settings_buffer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_settings}}', 'buffer', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_settings}}', 'buffer');
    }
}

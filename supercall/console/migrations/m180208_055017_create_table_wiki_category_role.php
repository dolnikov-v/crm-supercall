<?php
use backend\modules\wiki\models\WikiCategory;
use console\components\db\Migration;
use backend\modules\wiki\models\WikiCategoryRole;

/**
 * Class m180208_055017_create_table_wiki_category_role
 */
class m180208_055017_create_table_wiki_category_role extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(WikiCategoryRole::tableName(), [
            'id' => $this->primaryKey(),
            'wiki_category_id' => $this->integer()->notNull(),
            'auth_item_name' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, WikiCategoryRole::tableName(), 'wiki_category_id', WikiCategory::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(WikiCategoryRole::tableName());
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170413_143841_add_settings_start_finish_date
 */
class m170413_143841_add_settings_start_finish_date extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_settings}}', 'daily_from', $this->integer()->defaultValue(0)->notNull());
        $this->addColumn('{{%partner_settings}}', 'daily_till', $this->integer()->defaultValue(0)->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_settings}}', 'daily_till');
        $this->dropColumn('{{%partner_settings}}', 'daily_from');
    }
}

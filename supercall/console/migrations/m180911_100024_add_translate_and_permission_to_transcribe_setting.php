<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180911_100024_add_translate_and_permission_to_transcribe_setting
 */
class m180911_100024_add_translate_and_permission_to_transcribe_setting extends Migration
{
    public $permissions = [
        [
            'name' => 'audit.transcribesetting.index',
            'description' => 'Aудит КЦ / Настройка транскрибации (просмотр)',
        ],
        [
            'name' => 'audit.transcribesetting.edit',
            'description' => 'Aудит КЦ / Настройка транскрибации (добавление, редактирование)',
        ],
        [
            'name' => 'audit.transcribesetting.manage',
            'description' => 'Aудит КЦ / Настройка транскрибации (включение / отключение)',
        ],
        [
            'name' => 'audit.transcribesetting.delete',
            'description' => 'Aудит КЦ / Настройка транскрибации (удаление)',
        ],
    ];

    public $phrases = [
        'Aудит КЦ / Настройка транскрибации (просмотр)' => 'Audit CC / Transcription setup (viewing)',
        'Aудит КЦ / Настройка транскрибации (добавление, редактирование)' => 'Audit CC / Setting up transcription (adding, editing)',
        'Aудит КЦ / Настройка транскрибации (включение / отключение)' => 'Audit CC / Transcription setup (on / off)',
        'Aудит КЦ / Настройка транскрибации (удаление)' => 'Audit CC / Transcription setup (delete)',
        'Настройка с идентификатором №{id} не найдены' => 'Setup with id # {id} not found',
        'Настройка {action}' => 'Setting up {action}',
        'включены' => 'on',
        'выключены' => 'off',
        'Настройка с идентификатором №{id} не найдена' => 'Setting with id # {id} not found',
        'Настройка удалена' => 'Setup deleted',
        'Настройка успешно сохранена' => 'Setup successfully saved',
        'Необходимо указать страну' => 'Country is required',
        'Необходимо указать статусы заказа' => 'You must specify the order statuses',
        'Неизвестные статусы заказа: {status}' => 'Unknown order statuses: {status}',
        'Необходимо указать "Стоп" слова' => 'You must specify "Stop" words',
        'Для страны "{country}" уже существует настройка транскрибации' => 'Country "{country}" already has a transcription setting',
        'Ошибка сохранения статусов' => 'Error saving statuses',
        'Ошибка сохранения "Стоп" слов' => 'Error storing "Stop" words',
        '"Стоп"  слова' => '"Stop" words',
        'Слова "Исключения"' => 'he words "Exceptions"',
        'Настройка транскрибации' => 'Setting up transcription',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        foreach ($this->permissions as $permission) {

            $this->insert('{{%auth_item}}', array(
                'name' => $permission['name'],
                'type' => '2',
                'description' => $permission['description'],
                'created_at' => time(),
                'updated_at' => time()
            ));

        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        foreach($this->permissions as $permission){
            $this->delete('{{%auth_item}}', ['name' => $permission['name']]);
        }
    }
}

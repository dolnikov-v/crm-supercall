<?php
use common\models\User;
use console\components\db\Migration;

/**
 * Class m000000_000011_user
 */
class m000000_000011_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(64)->notNull(),
            'password' => $this->string(64)->notNull(),
            'timezone_id' => $this->integer()->defaultValue(null),
            'email' => $this->string(64)->defaultValue(null),
            'auth_key' => $this->string(32)->defaultValue(null),
            'status' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(null, '{{%user}}', 'username', true);
        $this->addForeignKey(null, '{{%user}}', 'timezone_id', '{{%timezone}}', 'id', Migration::CASCADE, Migration::RESTRICT);

        $user = new User();
        $user->username = 'superadmin';
        $user->setPassword('superadmin');
        $user->save(false);

        $role = $this->authManager->createRole(User::ROLE_SUPERADMIN);
        $role->description = 'Суперадминистратор';
        $this->authManager->add($role);
        $this->authManager->assign($role, $user->id);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}

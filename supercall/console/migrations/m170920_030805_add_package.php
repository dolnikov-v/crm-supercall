<?php
use console\components\db\Migration;
use common\modules\partner\models\Package;
use common\modules\partner\models\Partner;
use common\models\Country;
use common\models\Product;

/**
 * Class m170920_030805_add_package
 */
class m170920_030805_add_package extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(Package::tableName(), [
            'id' => $this->primaryKey(),
            'prefix' => $this->string(),
            'name' => $this->string()->notNull(),
            'partner' => $this->integer()->notNull(),
            'country' => $this->integer()->notNull(),
            'product' => $this->integer()->notNull(),
            'paid_amount' => $this->integer()->notNull(),
            'free_amount' => $this->integer()->notNull(),
            'active' => $this->integer()->defaultValue(0),
            'history' => 'jsonb',
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, Package::tableName(), 'partner', Partner::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, Package::tableName(), 'country', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, Package::tableName(), 'product', Product::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(Package::tableName());
    }
}

<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180209_080937_add_translate_to_wiki_frontend
 */
class m180209_080937_add_translate_to_wiki_frontend extends Migration
{
    public $phrases = [
        'База знаний' => 'Training room',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

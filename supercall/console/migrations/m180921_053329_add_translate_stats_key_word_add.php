<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180921_053329_add_translate_stats_key_word_add
 */
class m180921_053329_add_translate_stats_key_word_add extends Migration
{
    public $phrases = [
        'Слова "Исключения"' => 'The words "Exceptions"',
        'Указан неизвестный статус: {status}' => 'Unknown status: {status}',
        'Для статуса: {status} "Стоп"-слова уже укзаны' => 'For status: {status} "Stop" words are already registered',
        'Для статуса: {status} не указаны "Стоп"-слова' => 'For status: {status} no "Stop" word is specified',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

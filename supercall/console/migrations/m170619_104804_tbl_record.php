<?php
use console\components\db\Migration;

/**
 * Class m170619_104804_tbl_record
 */
class m170619_104804_tbl_record extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%records}}', [
            'id'    => $this->primaryKey(),
            'phone'   => $this->string(32),
            'datetime' => $this->integer(),
            'path' => $this->text(),
            'saved' => $this->boolean()->defaultValue(false),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%records}}');
    }
}

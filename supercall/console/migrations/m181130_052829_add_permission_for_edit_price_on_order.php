<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m181130_052829_add_permission_for_edit_price_on_order
 */
class m181130_052829_add_permission_for_edit_price_on_order extends Migration
{
    public $phrases = [
        'Изменение цены товаров заказа' => 'Change the price of goods order'
    ];

    public $permission = 'order.edit.productprice';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $permission = [$this->permission => 'Изменение цены товаров заказа'];
        $this->addPermission($permission);

        $this->addPermissionToRole($this->permission, User::ROLE_ADMINISTRATOR);

        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->deletePermission($this->permission);
        i18n_source_message::removeTranslate($this->phrases);
    }
}

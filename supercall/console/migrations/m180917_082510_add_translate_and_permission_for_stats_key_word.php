<?php

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180917_082510_add_translate_and_permission_for_stats_key_word
 */
class m180917_082510_add_translate_and_permission_for_stats_key_word extends Migration
{
    public $permission = [
        'name' => 'audit.statskeyword.index',
        'description' => 'Аудит КЦ / Статистика распознанных ключевых слов',
    ];

    public $roles = [
        User::ROLE_CURATOR,
        User::ROLE_SUPERVISOR,
        User::ROLE_QUALITY_MANAGER,
        User::ROLE_AUDITOR,
        User::ROLE_TEAM_LEAD,
        User::ROLE_QUALITY_MANAGER
    ];

    public $phrases = [
        'Слово' => 'Key-word',
        'По слову' => 'By key-word',
        'По звонку' => 'On call',
        'По всем звонкам заказа' => 'For all calls to the order',
        'Все слова' => 'All the words',
        'Только "Стоп" слова' => 'Just "Stop" the words',
        'Статистика распознанных ключевых слов' => 'Recognized key-word statistics',
        'Статистика слов' => 'Statistics of words',
        'Аудит КЦ / Статистика распознанных ключевых слов' => 'Audit CC / Recognized key-word statistics',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        $exists = $this->authManager->getPermission($this->permission);

        if(!$exists) {
            $this->insert('{{%auth_item}}', array(
                'name' => $this->permission['name'],
                'type' => '2',
                'description' => $this->permission['description'],
                'created_at' => time(),
                'updated_at' => time()
            ));
        }



        foreach($this->roles as $role) {
            $rolePermissions = $this->authManager->getPermissionsByRole($role);


            if(!in_array($this->permission['name'], array_keys($rolePermissions))) {
                $this->insert($this->authManager->itemChildTable, array(
                    'parent' => $role,
                    'child' => $this->permission['name']
                ));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        foreach($this->roles as $role) {
            $this->delete($this->authManager->itemChildTable, ['child' => $this->permission['name'], 'parent' => $role]);
        }

        $this->delete('{{%auth_item}}', ['name' => $this->permission['name']]);
    }
}

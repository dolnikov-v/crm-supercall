<?php
use console\components\db\Migration;

/**
 * Class m170515_114108_add_blocked_to_order
 */
class m170515_114108_add_blocked_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'blocked_to', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'blocked_to');
    }
}

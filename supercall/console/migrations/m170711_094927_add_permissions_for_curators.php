<?php
use console\components\db\Migration;
use common\models\User;
use backend\modules\access\models\AuthItem;
use yii\db\Query;

/**
 * Class m170711_094927_add_permissions_for_curators
 */
class m170711_094927_add_permissions_for_curators extends Migration
{
    public $permissions = [
        'catalog.country.index',
        'queue.control.clear',
        'queue.control.index',
        'partner.product.index',
        'partner.product.edit',

    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->permissions as $permission) {

            if (!AuthItem::find()->where(['name' => $permission])->exists()) {
                $this->insert(AuthItem::tableName(), array(
                    'name' => $permission,
                    'type' => '2',
                    'description' => $permission,
                    'created_at' => time(),
                    'updated_at' => time()
                ));
            }

            $item = (new Query())->select('*')->from($this->authManager->itemChildTable)
                ->where(['parent' => 'curator'])
                ->andWhere(['child' => $permission])
                ->one();

            if (is_null($item)) {
                $this->insert($this->authManager->itemChildTable, array(
                    'parent' => 'curator',
                    'child' => $permission
                ));
            }

        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => 'curator']);

            $this->delete(AuthItem::tableName(), ['name' => $permission]);
        }
    }
}

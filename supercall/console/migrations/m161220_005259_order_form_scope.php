<?php
use console\components\db\Migration;

/**
 * Class m161220_005259_order_form_scope
 */
class m161220_005259_order_form_scope extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_form_attribute}}', 'scope', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_form_attribute}}', 'scope');
    }
}

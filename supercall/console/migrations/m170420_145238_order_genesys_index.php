<?php
use console\components\db\Migration;

/**
 * Class m170420_145238_order_genesys_index
 */
class m170420_145238_order_genesys_index extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('idx_order_genesys_order_id', '{{%order_genesys}}', 'order_id');
        $this->createIndex('idx_order_logs_order_id', '{{%order_logs}}', 'order_id');
        $this->createIndex('idx_order_product_log_order_id', '{{%order_product_log}}', 'order_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('idx_order_genesys_order_id', '{{%order_genesys}}');
        $this->dropIndex('idx_order_product_log_order_id', '{{%order_product_log}}');
        $this->dropIndex('idx_order_logs_order_id', '{{%order_logs}}');
    }
}

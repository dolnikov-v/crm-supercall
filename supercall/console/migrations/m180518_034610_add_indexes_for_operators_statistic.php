<?php

use backend\modules\access\models\AuthAssignment;
use common\models\Country;
use common\models\User;
use common\modules\order\models\OrderLog;
use console\components\db\Migration;

/**
 * Class m180518_034610_add_indexes_for_operators_statistic
 */
class m180518_034610_add_indexes_for_operators_statistic extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('id_user_index', User::tableName(), 'id');
        $this->createIndex('field_order_log_index', OrderLog::tableName(), 'field');
        $this->createIndex('created_at_order_log_index', OrderLog::tableName(), 'created_at');
        $this->createIndex('item_name_auth_assignment_index', AuthAssignment::tableName(), 'item_name');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('id_user_index', User::tableName());
        $this->dropIndex('field_order_log_index', OrderLog::tableName());
        $this->dropIndex('created_at_order_log_index', OrderLog::tableName());
        $this->dropIndex('item_name_auth_assignment_index', AuthAssignment::tableName());
    }
}

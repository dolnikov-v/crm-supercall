<?php
    use console\components\db\Migration;
    use common\models\AutoCallLog;
    
    /**
     * Class m180316_130940_add_table_auto_call_log_fixed
     */
    class m180316_130940_add_table_auto_call_log_fixed extends Migration
    {
        /**
         * @inheritdoc
         */
        public function safeUp()
        {
            $this->dropTable(AutoCallLog::tableName());
            $this->createTable(AutoCallLog::tableName(), [
                'id' => $this->primaryKey(),
                'order_phone' => $this->string()->notNull(),
                'order_id' => $this->integer()->notNull(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                'country_id' => $this->integer(),
                'queue_name' => $this->string(255),
                'cdr_id' => $this->string(30),
                'user_sip' => $this->string(20),
                'call_status' => $this->string(20),
            ]);
            
            $this->createIndex('idx_order_id', AutoCallLog::tableName(), ['order_id']);
            $this->createIndex('idx_country_id', AutoCallLog::tableName(), ['country_id']);
            $this->createIndex('idx_order_phone', AutoCallLog::tableName(), ['order_phone']);
            $this->createIndex('idx_queue_name', AutoCallLog::tableName(), ['queue_name']);
            $this->createIndex('idx_call_status', AutoCallLog::tableName(), ['call_status']);
            $this->createIndex('idx_user_sip', AutoCallLog::tableName(), ['user_sip']);
            
        }
        
        /**
         * @inheritdoc
         */
        public function safeDown()
        {
        
        }
    }

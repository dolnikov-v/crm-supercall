<?php
use console\components\db\Migration;

/**
 * Class m170203_124001_dynamic_shipping
 */
class m170203_124001_dynamic_shipping extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_shipping}}', 'dynamic_price', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_shipping}}', 'dynamic_price');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170110_153639_order_ordered_at
 */
class m170110_153639_order_ordered_at extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'ordered_at', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'ordered_at');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170621_101340_add_permission_for_klader_countries
 */
class m170621_101340_add_permission_for_klader_countries extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}',array(
            'name'=>'catalog.kladercountries.index',
            'type' => '2',
            'description' => 'catalog.kladercountries.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'curator',
            'child' => 'catalog.kladercountries.index'
        ));

        $this->insert('{{%auth_item}}',array(
            'name'=>'catalog.kladercountries.delete',
            'type' => '2',
            'description' => 'catalog.kladercountries.delete',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'curator',
            'child' => 'catalog.kladercountries.delete'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.kladercountries.index', 'parent' => 'curator']);
        $this->delete('{{%auth_item}}', ['name' => 'catalog.kladercountries.index']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.kladercountries.delete', 'parent' => 'curator']);
        $this->delete('{{%auth_item}}', ['name' => 'catalog.kladercountries.delete']);
    }
}

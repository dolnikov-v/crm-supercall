<?php
use console\components\db\Migration;

/**
 * Class m170428_140606_phone_length
 */
class m170428_140606_phone_length extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('{{%order}}', 'customer_phone', $this->string(32));
        $this->alterColumn('{{%order}}', 'customer_mobile', $this->string(32));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('{{%order}}', 'customer_phone', $this->string(20));
        $this->alterColumn('{{%order}}', 'customer_mobile', $this->string(20));
    }
}

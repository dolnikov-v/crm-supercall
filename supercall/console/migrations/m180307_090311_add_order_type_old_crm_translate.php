<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180307_090311_add_order_type_old_crm_translate
 */
class m180307_090311_add_order_type_old_crm_translate extends Migration
{
    public $phrases = [
        'Заказы из старого КЦ' => 'Order from old CRM',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m180205_023137_add_queue_counter_to_order
 */
class m180205_023137_add_queue_counter_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(\common\modules\order\models\Order::tableName(), 'queue_counter', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(\common\modules\order\models\Order::tableName(), 'queue_counter');
    }
}
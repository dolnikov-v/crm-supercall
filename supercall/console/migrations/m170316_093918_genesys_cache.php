<?php
use console\components\db\Migration;

/**
 * Class m170316_093918_genesys_cache
 */
class m170316_093918_genesys_cache extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%genesys_call}}', [
            'CALLID' => $this->string(),
            'CALL_START' => $this->string(),
            'DNIS' => $this->string(),
            'CPD_RESULT' => $this->string(),
            'CPD_RESULT_DESC' => $this->string(),
            'CALL_RESULT' => $this->string(),
            'CALL_RESULT_DESC' => $this->string(),
            'PRODUCT_LANGUAGE' => $this->string(),
            'PRODUCT_ID' => $this->string(),
            'CUSTOMER_NAME' => $this->string(),
            'LAST_ATTEMPT' => $this->string(),
            'TIMEZONE' => $this->string(),
            'CALL_DURATION' => $this->string(),
            'TIME_ALERTING' => $this->string(),
            'TIME_CONNECTED' => $this->string(),
            'TIME_IN_QUEUE' => $this->string(),
            'TIME_ON_HOLD' => $this->string(),
            'RECORD' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%genesys_call}}');
    }
}

<?php
use console\components\db\Migration;
use common\modules\call\models\CallHistory;

/**
 * Class m180216_095844_add_new_field_in_call_history
 */
class m180216_095844_add_new_field_in_call_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //uniqueid - это ИД записи в баще астера. Будем хранить для быстрого поиска
        $this->addColumn(CallHistory::tableName(), 'uniqueid', $this->float()->null());
        //где лежит на астере. Полный путь не храним. Храним его в параметрах
        $this->addColumn(CallHistory::tableName(), 'recordingfile', $this->string());
        //Статус звонка на астере
        $this->addColumn(CallHistory::tableName(), 'disposition', $this->string());
        //Время реального разговора с клиентом
        $this->addColumn(CallHistory::tableName(), 'billsec', $this->integer()->defaultValue(null)->null());

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallHistory::tableName(), 'uniqueid');
        $this->dropColumn(CallHistory::tableName(), 'recordingfile');
        $this->dropColumn(CallHistory::tableName(), 'disposition');
        $this->dropColumn(CallHistory::tableName(), 'billsec');
    }
}

<?php
use console\components\db\Migration;

/**
 * Для хранения в формате json данных в формате
 *  {
 *      "code": 1,
 *      "tracker": 45645ghgfjh7,
 *  }
 *
 * 1 - заказ в процессе оформления
 * 2 - в доставке
 * 3 - доставлен
 * 4 - недоставлен
 * Class m170314_085318_add_column_to_order
 */
class m170314_085318_add_column_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'approve_components', $this->text()->after('customer_components'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'approve_components');
    }
}

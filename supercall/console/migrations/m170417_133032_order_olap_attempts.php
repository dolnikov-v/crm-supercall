<?php
use console\components\db\Migration;

/**
 * Class m170417_133032_order_olap_attempts
 */
class m170417_133032_order_olap_attempts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_olap_attempts}}', [
            'period_type' => $this->smallInteger(1)->notNull(),
            'period' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'attempts' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull() . ' DEFAULT 0',
        ]);

        $this->addForeignKey(
            'fk_ooa_product',
            '{{%order_olap_attempts}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_ooa_type',
            '{{%order_olap_attempts}}',
            'type_id',
            '{{%order_type}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_ooa_partner',
            '{{%order_olap_attempts}}',
            'partner_id',
            '{{%partner}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_ooa_country',
            '{{%order_olap_attempts}}',
            'country_id',
            '{{%country}}',
            'id',
            'CASCADE'
        );

        $this->addPrimaryKey('pk_ooa', '{{%order_olap_attempts}}', [
            'period_type',
            'country_id',
            'type_id',
            'product_id',
            'partner_id',
            'period',
            'attempts',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_olap_attempts}}');
    }
}

<?php
use console\components\db\Migration;
use common\modules\call\models\CallHistory;

/**
 * Class m171129_033922_add_column_phone_to_call_history
 */
class m171129_033922_add_column_phone_to_call_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(CallHistory::tableName(), 'phone', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(CallHistory::tableName(), 'phone');
    }
}

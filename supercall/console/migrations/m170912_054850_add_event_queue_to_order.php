<?php
use console\components\db\Migration;
use common\modules\order\models\Order;

/**
 * Class m170912_054850_add_event_queue_to_order
 */
class m170912_054850_add_event_queue_to_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Order::tableName(), 'event', 'jsonb');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Order::tableName(), 'event');
    }
}

<?php
use console\components\db\Migration;


/**
 * Class m170830_133154_add_permission_for_substatus
 */
class m170830_133154_add_permission_for_substatus extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => 'catalog.sub-status.index',
            'type' => '2',
            'description' => 'catalog.sub-status.index',
            'created_at' => time(),
            'updated_at' => time(),
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'curator',
            'child' => 'catalog.sub-status.index'
        ));

        $this->insert('{{%auth_item}}', array(
            'name' => 'catalog.sub-status.edit',
            'type' => '2',
            'description' => 'catalog.sub-status.edit',
            'created_at' => time(),
            'updated_at' => time(),
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'curator',
            'child' => 'catalog.sub-status.edit'
        ));

        $this->insert('{{%auth_item}}', array(
            'name' => 'catalog.sub-status.delete',
            'type' => '2',
            'description' => 'catalog.sub-status.delete',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => 'curator',
            'child' => 'catalog.sub-status.delete',
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.sub-status.index', 'parent' => 'curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.sub-status.edit', 'parent' => 'curator']);
        $this->delete($this->authManager->itemChildTable, ['child' => 'catalog.sub-status.delete', 'parent' => 'curator']);

        $this->delete('{{%auth_item}}', ['name' => 'catalog.sub-status.index']);
        $this->delete('{{%auth_item}}', ['name' => 'catalog.sub-status.edit']);
        $this->delete('{{%auth_item}}', ['name' => 'catalog.sub-status.delete']);
    }
}

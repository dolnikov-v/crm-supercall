<?php
use console\components\db\Migration;

/**
 * Class m161130_092314_order_log
 */
class m161130_092314_order_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_log}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'group_id' => $this->string()->notNull(),
            'field' => $this->string()->notNull(),
            'old' => $this->string()->notNull(),
            'new' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, '{{%order_log}}', 'order_id', '{{%order}}', 'id', self::NO_ACTION, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_log}}');
    }
}

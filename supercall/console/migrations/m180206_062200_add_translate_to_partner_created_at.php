<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180206_062200_add_translate_to_partner_created_at
 */
class m180206_062200_add_translate_to_partner_created_at extends Migration
{
    public $phrases = [
        'Время от создания' => 'Time from creation'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

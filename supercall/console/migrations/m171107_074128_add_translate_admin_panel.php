<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171107_074128_add_translate_admin_panel
 */
class m171107_074128_add_translate_admin_panel extends Migration
{
    public $phrases = [
        'Статистика' => 'Statistics',
        'Статистика заказов' => 'Order statistics',
        'Статистика обработки заказов' => 'Order processing statistics',
        'Статистика по товарам' => 'Products statistics',
        'Статистика по операторам' => 'Statistics of operators',
        'Статистика по заказам' => 'Order statistics',
        'Статистика одобренных' => 'Approve statistic',
        'Статистика отказов' => 'Reject statistic',
        'Статистика трешей' => 'Trash statistic',
        'История изменений' => 'History of changes',
        'Применить' => 'Apply',
        'Сбросить фильтр' => 'Reset filter',
        'Группировка' => 'Grouping',
        'День' => 'Day',
        'Час' => 'Hour',
        'Месяц' => 'Month',
        'Все' => 'All',
        'Дата' => 'Date',
        'Тип заказа' => 'Order type',
        'Всего' => 'Total',
        'Всего:' => 'Total:',
        'В процессе' => 'During',
        'Выкуп' => 'Buyout',
        'Невыкуп' => 'Non-payment',
        'Новый' => 'New',
        'Доставка' => 'Delivery',
        'Созданный' => 'Created',
        'Средний чек' => 'Average check',
        'Онлайн, мин.' => 'Online, min.',
        'Среднее время, мин.' => 'Average time, min.',
        'Количество заказов' => 'Count of orders',
        'Минимальное время, мин.' => 'Minimum time, min.',
        'Среднеее время, мин.' => 'Average time, min.',
        'Максимальное время, мин.' => 'Maximum time, min.',
        'Суммарное время, мин.' => 'Total time, min.',
        'Общее время, мин.' => 'Total time, min.',
        'Интернационализация' => '',
        'Филиппины' => 'Philippines',
        'Список языков' => 'List of languages',
        'Список фраз' => 'List of phrases',
        'Список переводов' => 'List of translations',
        'Панель языков' => 'Language bar',
        'Локаль' => 'Local',
        'Добавить язык' => 'Add language',
        'Таблица с фразами' => 'Phrase Table',
        'Категория' => 'Category',
        'Фраза' => 'Phrase',
        'Добавить перевод' => 'Add translation',
        'Таблица с переводами' => 'Table of translations',
        'Язык' => 'Language',
        'Перевод' => 'Translation',
        'Добавить еще перевод' => 'Add translation',
        'Доступы' => 'Access',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}



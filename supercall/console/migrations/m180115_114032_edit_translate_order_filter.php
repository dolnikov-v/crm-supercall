<?php
use console\components\db\Migration;

use backend\models\i18n_source_message;

/**
 * Class m180115_114032_edit_translate_order_filter
 */
class m180115_114032_edit_translate_order_filter extends Migration
{
    public $phrases = [
        'Дата создания у партнёра' => 'Date of creation by partner',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }
}

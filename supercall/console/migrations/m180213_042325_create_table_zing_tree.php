<?php
use backend\modules\operator\models\ZingTree;
use console\components\db\Migration;

/**
 * Class m180213_042325_create_table_zing_tree
 */
class m180213_042325_create_table_zing_tree extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(ZingTree::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'url' => $this->string(),
            'country_id' => 'jsonb',
            'product_id' => 'jsonb',
            'active' => $this->integer()->defaultValue(ZingTree::ACTIVE),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(ZingTree::tableName());
    }
}

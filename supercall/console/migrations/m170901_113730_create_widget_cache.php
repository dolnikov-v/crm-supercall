<?php
use console\components\db\Migration;
use backend\modules\widget\models\WidgetCache;
use backend\modules\widget\models\WidgetType;
use backend\modules\widget\models\WidgetUser;
use common\models\Country;


/**
 * Class m170901_113730_create_widget_cache
 */
class m170901_113730_create_widget_cache extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(WidgetType::tableName(), 'by_country', $this->smallInteger());
        $this->addColumn(WidgetType::tableName(), 'no_cache', $this->smallInteger());

        $this->createTable(WidgetCache::tableName(), [
            'id' => $this->primaryKey(),
            'widgetuser_id' => $this->integer(),
            'country_id' => $this->integer(),
            'data' => 'TEXT',
            'cached_at' => $this->integer(),
        ], $this->tableOptions);

        $this->addForeignKey(null, WidgetCache::tableName(), 'widgetuser_id', WidgetUser::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, WidgetCache::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(WidgetType::tableName(), 'by_country');
        $this->dropColumn(WidgetType::tableName(), 'no_cache');
        $this->dropTable(WidgetCache::tableName());
    }
}

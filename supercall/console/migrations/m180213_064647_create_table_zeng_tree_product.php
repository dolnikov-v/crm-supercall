<?php
use backend\modules\operator\models\ZingTree;
use common\models\Product;
use console\components\db\Migration;

/**
 * Class m180213_064647_create_table_zeng_tree_product
 */
class m180213_064647_create_table_zeng_tree_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%zing_tree_product}}', [
            'id' => $this->primaryKey(),
            'zing_tree_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(null, '{{%zing_tree_product}}', 'zing_tree_id', ZingTree::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%zing_tree_product}}', 'product_id', Product::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%zing_tree_product}}');
    }
}

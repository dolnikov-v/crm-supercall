<?php

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180307_050557_add_permission_for_group_operations
 */
class m180307_050557_add_permission_for_group_operations extends Migration
{
    public $phrases = [
        'Доступы / Список пользователей / Блокировка пользователей (групповые операции)' => 'Security / List of users/ Blocking users (group operations)',
        'Доступы / Список пользователей / Разблокировка пользователей (групповые операции)' => 'Security / List of users/ Unlocking users (group operations)',
        'Доступы / Список пользователей / Включить WebRTC (групповые операции)' => 'Security / List of users/ Enable WebRTC (group operations)',
        'Доступы / Список пользователей / Выключить WebRTC (групповые операции)' => 'Security / List of users/ Disable WebRTC (group operations)',
        'Доступы / Список пользователей / Выключить автодозвон (групповые операции)' => 'Security / List of users/ Disable auto-call (group operations)',
        'Доступы / Список пользователей / Включить автодозвон (групповые операции)' => 'Security / List of users/ Enable auto-call (group operations)',
        'Доступы / Список пользователей / Удалить пользователей (групповые операции)' => 'Security / List of users/ Delete users (group operations)',
        'Доступы / Список пользователей / Восстановить пользователей (групповые операции)' => 'Security / List of users/ Restore users (group operations)',
        'Заказы / Список заказов/ Сменить статус (групповые операции)' => 'Orders / Orders list / Change status (group operations)',
        'Заказы / Список заказов/ Отправить в очередь (групповые операции)' => 'Orders / Orders list / Send to the queue (group operations)',
        'Подтверждение' => 'Confirmation',
        'Вы действительно собираетесь выполнить данную операцию ?' => 'Are you really going to perform this operation?',
        'Смена статуса' => 'Change status',
        'Заказы не выбраны' => 'No orders selected',
        'Прогресс' => 'Progress',
        'Для заказа №{id} указан несуществующий статус: {status}' => 'Order #{id} has a non-existent status: {status}',
        'Заказ №{id} не найден' => 'Order No.{id} was not found',
        'Ошибка при смене статуса заказа №{id} : {error}' => 'Error changing order status No.{id}: {error}',
        'Отправка в очередь' => 'Send to queue',
        'Пользователь №{id} не найден' => 'User No.{id} was not found',
        'Запрос запрещён' => 'Request denied',
        'Блокировать' => 'Block',
        'Разблокировать' => 'Unlock',
        'Включить Автодозвон' => 'Enable Auto Call',
        'Включить WebRTC' => 'Enable WebRTC',
        'Отключить Автодозвон' => 'Disable Auto Call',
        'Отключить WebRTC' => 'Disable WebRTC',
        'Сменить статус' => 'Change status',
        'Восстановить' => 'Restore',
        'Групповые операции' => 'Group Operations'
    ];

    public $permissions = [
        'access.usergroupoperations.blockusers' => 'Доступы / Список пользователей / Блокировка пользователей (групповые операции)',
        'access.usergroupoperations.activateusers' => 'Доступы / Список пользователей / Разблокировка пользователей (групповые операции)',
        'access.usergroupoperations.offwebrtc' => 'Доступы / Список пользователей / Включить WebRTC (групповые операции)',
        'access.usergroupoperations.onwebrtc' => 'Доступы / Список пользователей / Выключить WebRTC (групповые операции)',
        'access.usergroupoperations.offautocall' => 'Доступы / Список пользователей / Выключить автодозвон (групповые операции)',
        'access.usergroupoperations.onautocall' => 'Доступы / Список пользователей / Включить автодозвон (групповые операции)',
        'access.usergroupoperations.deleteusers' => 'Доступы / Список пользователей / Удалить пользователей (групповые операции)',
        'access.usergroupoperations.undeleteusers' => 'Доступы / Список пользователей / Восстановить пользователей (групповые операции)',
        'order.groupoperations.changestatus' => 'Заказы / Список заказов/ Сменить статус (групповые операции)',
        'order.groupoperations.senttosqs' => 'Заказы / Список заказов/ Отправить в очередь (групповые операции)',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        foreach($this->permissions as $name=>$desc) {
            $this->insert('{{%auth_item}}', array(
                'name' => $name,
                'type' => '2',
                'description' => $desc,
                'created_at' => time(),
                'updated_at' => time()
            ));
        }

        foreach ($this->permissions as $name=>$description){
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_SUPERVISOR,
                'child' => $name
            ));
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        foreach($this->permissions as $name=>$desc) {
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => User::ROLE_SUPERVISOR]);
        }

        foreach ($this->permissions as $name => $desctiption){
            $this->delete('{{%auth_item}}', ['name' => $name]);
        }

    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170228_041757_tbl_wiki
 */
class m170228_041757_tbl_wiki extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%wiki_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'title' => $this->string()->notNull()->unique(),
            'description' => $this->text()->null(),
            'sort' => $this->integer()->defaultValue(10),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
        
        $this->addForeignKey('fk-wiki_category-created_by', '{{%wiki_category}}', 'created_by', '{{%user}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey('fk-wiki_category-updated_by', '{{%wiki_category}}', 'updated_by', '{{%user}}', 'id', self::CASCADE, self::RESTRICT);
        
        $this->createTable('{{%wiki_article}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'title' => $this->string()->notNull()->unique(),
            'text' => $this->text()->null(),
            'category_id' => $this->integer(),
            'sort' => $this->integer()->defaultValue(10),
            'active' => $this->smallInteger()->defaultValue(1),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
    
        $this->addForeignKey('fk-wiki_article-category_id', '{{%wiki_article}}', 'category_id', '{{%wiki_category}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey('fk-wiki_article-created_by', '{{%wiki_article}}', 'created_by', '{{%user}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey('fk-wiki_article-updated_by', '{{%wiki_article}}', 'updated_by', '{{%user}}', 'id', self::CASCADE, self::RESTRICT);
    
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-wiki_article-updated_by');
        $this->dropForeignKey('fk-wiki_article-created_by');
        $this->dropForeignKey('fk-wiki_article-category_id');
    
        $this->dropTable('{{%wiki_article}}');
        
        $this->dropForeignKey('fk-wiki_category-updated_by');
        $this->dropForeignKey('fk-wiki_category-created_by');
    
        $this->dropTable('{{%wiki_category}}');
    }
}

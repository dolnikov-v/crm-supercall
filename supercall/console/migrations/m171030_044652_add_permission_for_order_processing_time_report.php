<?php
use console\components\db\Migration;
use common\models\User;
use backend\models\i18n_source_message;

/**
 * Class m171030_044652_add_permission_for_order_processing_time_report
 */
class m171030_044652_add_permission_for_order_processing_time_report extends Migration
{
    public $permissions = [
        'stats.orderprocessingtime.dates' => 'Статистика / Затраты времени по датам',
        'stats.orderprocessingtime.orders' => 'Статистика / Затраты времени по заказам',
        'stats.orderprocessingtime.statuses' => 'Статистика / Затраты времени по статусам заказов',
        'stats.orderprocessingtime.products' => 'Статистика / Затраты времени по товарам заказов',
    ];

    public $phrases = [
        'Статистика / Затраты времени по датам' => 'Statistics / Time spent by date',
        'Статистика / Затраты времени по заказам' => 'Statistics / Time consumed by orders',
        'Статистика / Затраты времени по статусам заказов' => 'Statistics / Time-based order statuses',
        'Статистика / Затраты времени по товарам заказов' => 'Statistics / Time consumed by order goods',
        'Затраты времени по датам' => 'Time spent by date',
        'Затраты времени по заказам' => 'Time spent on orders',
        'Затраты времени по статусам заказов' => 'Time costs for order statuses',
        'Затраты времени по товарам заказов' => 'Time consumed by order goods',
        'Затраты времени по статусам' => 'Time expenditure by status',
        'Затраты времени по товарам' => 'Time consumed by goods',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach($this->permissions as $name=>$desc) {
            $this->insert('{{%auth_item}}', array(
                'name' => $name,
                'type' => '2',
                'description' => $desc,
                'created_at' => time(),
                'updated_at' => time()
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_CURATOR,
                'child' => $name
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_SUPERVISOR,
                'child' => $name
            ));
        }

        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->permissions as $name=>$desc) {
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => User::ROLE_CURATOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => User::ROLE_SUPERVISOR]);
            $this->delete('{{%auth_item}}', ['name' => $name]);
        }

        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;
use backend\modules\widget\models\WidgetType;

/**
 * Class m170906_065115_fix_migration_widget_name
 */
class m170906_065115_fix_migration_widget_name extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $type = WidgetType::find()
            ->where(['code' => 'main-statistic'])
            ->one();

        if(!is_null($type)) {
            $type->code = 'main-orders';
            $type->name = 'Заказы';
            $type->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}

<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171027_075519_add_translate_with_test
 */
class m171027_075519_add_translate_with_test extends Migration
{
    public $phrases = [
        'количество заказов на данный номер телефона' => 'number of orders for this phone number',
        'Назначение оператора' => 'Operator Assignment',
        'Звонить' => 'Call',
        'Сброс' => 'Reset',
        'Удержать' => 'Hold',
        'Настройки' => 'Settings',
        'Конфигурация аудио' => 'Audio Configuration',
        'Отключить динамик' => 'Disable speaker',
        'Отключить микрофон' => 'Mute microphone',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

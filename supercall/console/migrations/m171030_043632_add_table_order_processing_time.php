<?php
use console\components\db\Migration;
use common\modules\order\models\Order;
use common\models\User;
use backend\modules\stats\models\OrderProcessingTime;

/**
 * Class m171030_043632_add_table_order_processing_time
 */
class m171030_043632_add_table_order_processing_time extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(OrderProcessingTime::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'begin_date' => $this->integer()->notNull(),
            'end_date' => $this->integer(),
            'begin_status' => $this->integer()->notNull(),
            'end_status' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('order_processing_time__order_id__order__id', OrderProcessingTime::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey('order_processing_time__user_id__user__id', OrderProcessingTime::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(OrderProcessingTime::tableName());
    }
}

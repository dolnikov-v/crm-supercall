<?php

/**
 * Создание роли (с проверкой на наличие)
 * $role = [name => description]
 * $this->addRole(array $role)
 *
 * Удаление роли
 * $this-deleteRole(string $role)
 *
 * Проверка наличия роли
 * $this->existsRole(string $role_name);
 *
 * Создание пермишена (с проверкой на наличие)
 * $permission = [name => description]
 * $this->addPermission(array $permission)
 *
 * Удаление пермишена (автоматом удаляются все связки по ролям)
 * $this->deletePermission(string $permission);
 *
 * Проверка наличия пермишена
 * $this->>existsPermission(string $permission_name)
 *
 * Добавление пермишена к роли
 * $this->addPermissionToRole(string $permission, string $role)
 *
 * Удаления пермишена у роли
 * $this->deletePermissionByRole(string $permission, string $role)
 *
 * Добавление перевода (удаление перед добавление уже внутри метода)
 * i18n_source_message::addTranslate($this->phrases, true);
 *
 * Удаление переводов
 * i18n_source_message::removeTranslate($this->phrases);
 */

use backend\models\i18n_source_message;
use backend\modules\queue\models\Queue;
use common\models\Country;
use common\models\Currency;
use common\models\Timezone;
use common\modules\order\models\Order;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use console\components\db\Migration;
use yii\helpers\ArrayHelper;

/**
 * Class m190329_042104_wcall_618_create_ln_and_bg
 */
class m190329_042104_wcall_618_create_ln_and_bg extends Migration
{
    /**
     * @var Partner
     */
    public $partner;

    /** @var  Currency */
    public $currency;

    /** @var  array */
    public $language;

    public $phrases = [
        'Нидерланды' => 'Netherlands',
        'Бельгия' => 'Belgium'
    ];

    public $listNewCountries = [
        'NL' => 'Нидерланды',
        'BE' => 'Бельгия'
    ];

    public $listTimezonesCountries = [
        'NL' => '+01:00',
        'BE' => '+01:00'
    ];

    public $addressAttributes = [
        [
            'name' => 'customer_city',
            'label' => 'City',
            'required' => true,
            'is_klader' => true,
            'is_coordinate' => true
        ],
        [
            'name' => 'customer_street',
            'label' => 'Street',
            'required' => true,
            'is_coordinate' => true
        ],
        [
            'name' => 'customer_house_number',
            'label' => 'House number',
            'required' => false,
            'is_coordinate' => true
        ],
        [
            'name' => 'customer_zip',
            'label' => 'Postal code',
            'required' => true,
            'is_klader' => true,
            'is_coordinate' => true
        ],

    ];

    public function init()
    {
        parent::init();

        $this->currency = Currency::findOne(['char_code' => 'EUR']);
        $this->language = yii::$app->db->createCommand("SELECT * FROM i18n_language WHERE locale='en-US'")->queryOne();
        $this->partner = Partner::findOne(['name' => '2wtrade']);
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        //create countries
        foreach (array_keys($this->listNewCountries) as $char_code) {
            $partnerProductIds = [];
            $country = Country::findOne(['char_code' => $char_code]);

            if (!$country) {
                $timezone_string = "(GMT {$this->listTimezonesCountries[$char_code]})%";
                $timezone = Timezone::find()->where("name like '{$timezone_string}'")->one();

                if (!$timezone) {
                    $timezone = Timezone::findOne("name like '(GMT +00:00)'%");
                }

                //создать страну - если её нет
                $country = new Country([
                    'name' => $this->listNewCountries[$char_code],
                    'slug' => strtolower($char_code),
                    'char_code' => $char_code,
                    'active' => Country::ACTIVE,
                    'timezone_id' => $timezone->id,
                    'currency_id' => $this->currency->id,
                    'language_id' => $this->language['id'],
                    'order_hold_verification' => false,
                    'klader_in_all_string' => false,
                ]);

                $country->save();
            }

            //создать формы для новыйх заказов
            $form = new PartnerForm([
                'partner_id' => $this->partner->id,
                'name' => $country->name,
                'active' => true,
                'country_id' => $country->id,
                'type' => OrderType::TYPE_NEW,
                'buttons' => [
                    Order::STATUS_RECALL,
                    Order::STATUS_FAIL,
                    Order::STATUS_APPROVED,
                    Order::STATUS_REJECTED,
                    Order::STATUS_TRASH,
                    Order::STATUS_DOUBLE
                ],
                'show_shipping' => true,
                'allow_gifts' => true,
                'show_countries' => true,
                'use_map' => true,
                'check_address' => true,
                'use_foreign_delivery_broker' => true,
                'disabled_buttons' => true
            ]);

            if ($form->save()) {
                //дополнить форму адресными атрибутами
                foreach ($this->addressAttributes as $k => $attribute) {
                    $attribute['sort'] = $k++;
                    $attribute['type'] = 'string';
                    $attribute['scope'] = 'address';
                    $partnerFormAttribute = new PartnerFormAttribute($attribute);
                    $partnerFormAttribute->link('form', $form);
                }
            }

            //создать первичную очередь с минимальными настройками
            $queue = new Queue([
                'name' => yii::t('common', $country->name) . ' test queue',
                'priority' => 1,
                'priority_selection' => 1,
                'strategy' => '{"3":{"strategyCount":3,"blockTime":30,"changeStatus":5}}',
                'is_primary' => true,
                'primary_type' => strval(OrderType::TYPE_NEW),
                'primary_country' => $country->id,
                'primary_partner' => strval($this->partner->id),
                'primary_product' => []
            ]);

            $queue->save();
        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $countries = Country::findAll(['char_code' => array_keys($this->listNewCountries)]);
        $country_ids = ArrayHelper::getColumn($countries, 'id');

        PartnerForm::deleteAll(['country_id' => $country_ids]);
        Country::deleteAll(['char_code' => array_keys($this->listNewCountries)]);

        foreach ($country_ids as $country_id) {
            $queues = Queue::find()->byCountry($country_id, false)->all();
            foreach ($queues as $queue) {
                $queue->delete();
            }
        }
    }
}

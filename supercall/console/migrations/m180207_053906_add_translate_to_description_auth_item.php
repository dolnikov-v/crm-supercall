<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180207_053906_add_translate_to_description_auth_item
 */
class m180207_053906_add_translate_to_description_auth_item extends Migration
{
    public $phrases = [
        'ЛК' => 'My Account (Main)',
        'Добавление продукта при редактировании заказа' => 'Adding a product when editing an order',
        'Редактирование заказа' => 'Editing an order',
        'Пересчет стоимости заказа' => 'Conversion of order value',
        'Партнеры, форма, все действия' => 'Partners, form, all actions',
        'Партнеры, НДС, все действия' => 'Partners, VAT, all actions',
        'Очереди / Управление Добавление очереди' => 'Queues / Managing',
        'Деталицазия статистики операторов' => 'Details of statistics of operators',
        'История изменений в обработке заказов' => 'Change history in order processing',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

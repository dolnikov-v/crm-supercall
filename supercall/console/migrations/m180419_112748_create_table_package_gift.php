<?php

use common\models\Product;
use common\modules\partner\models\Package;
use common\modules\partner\models\PackageGift;
use common\modules\partner\models\PartnerProduct;
use console\components\db\Migration;

/**
 * Class m180419_112748_create_table_package_gift
 */
class m180419_112748_create_table_package_gift extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(PackageGift::tableName(), [
            'id' => $this->primaryKey(),
            'package_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'free_amount' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, PackageGift::tableName(), 'package_id', Package::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, PackageGift::tableName(), 'product_id', Product::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(PackageGift::tableName());
    }
}

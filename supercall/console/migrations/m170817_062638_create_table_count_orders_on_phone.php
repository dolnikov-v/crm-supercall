<?php
use console\components\db\Migration;
use common\modules\order\models\CountOrdersOnPhone;
/**
 * Class m170817_062638_create_table_count_orders_on_phone
 */
class m170817_062638_create_table_count_orders_on_phone extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(CountOrdersOnPhone::tableName(), [
            'id' => $this->primaryKey(),
            'phone' => $this->string()->notNull(),
            'count_orders' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(CountOrdersOnPhone::tableName());
    }
}

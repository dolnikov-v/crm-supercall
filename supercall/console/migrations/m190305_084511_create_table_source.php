<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use backend\modules\catalog\models\Source;
use console\components\db\Migration;

/**
 * Class m190305_084511_create_table_source
 */
class m190305_084511_create_table_source extends Migration
{
    public $phrases = [
        'Источники' => 'Sources',
        'Список источников' => 'Sources list',
        'Таблица с источниками' => 'Sources table',
        'Добавить источник' => 'Add source',
        'Добавление источника' => 'Add source',
        'Редактирование источника' => 'Edit source',
        'Данные источника' => 'Source data',
        'Сохранить источник' => 'Save source',
        'Источник успешно отредактирован.' => 'The source has been successfully edited.',
        'Источник успешно сохранен.' => 'The source has been successfully saved.',
        'Источник успешно добавлен.' => 'The source has been successfully added.',
        'Источник успешно активирован.' => 'The source has been successfully activated.',
        'Источник успешно деактивирован.' => 'The source has been successfully deactivated.'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        $this->createTable(Source::tableName(), [
            'id' => $this->primaryKey(),
            'code' => $this->integer()->notNull()->unique(),
            'name' => $this->string(255)->notNull(),
            'active' => $this->boolean()->defaultValue(true),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->dropTable(Source::tableName());
    }
}

<?php
use console\components\db\Migration;
use backend\modules\widget\models\WidgetRole;
use backend\modules\widget\models\WidgetType;
use common\models\User;

/**
 * Class m170904_031114_add_widget_main_statistic
 */
class m170904_031114_add_widget_main_statistic extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'main-orders',
            'name' => 'Заказы',
            'status' => WidgetType::STATUS_ACTIVE,
            'no_cache' => WidgetType::NO_CACHE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'main-orders'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => User::ROLE_CURATOR,
            'type_id' => $type->id
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'main-orders'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
           'role' => User::ROLE_CURATOR,
            'type_id' => $type->id
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'main-orders']);
    }
}

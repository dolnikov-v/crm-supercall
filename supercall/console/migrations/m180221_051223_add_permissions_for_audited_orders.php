<?php
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180221_051223_add_permissions_for_audited_orders
 */
class m180221_051223_add_permissions_for_audited_orders extends Migration
{
    public $new_permissions = [
        'operator.auditedorders.index' => 'Аудит / Проверенные заказы / Просмотр',
        'operator.auditedorders.orderlog' => 'Аудит / Проверенные заказы / Просмотр изменений',
        'operator.auditedorders.audit' => 'Аудит / Проверенные заказы / Просмотр аудита',
    ];

    public $list_permissions = [
        'operator.auditedorders.index',
        'operator.auditedorders.orderlog',
        'operator.auditedorders.audit'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach($this->new_permissions as $name=>$desc) {
            $this->insert('{{%auth_item}}', array(
                'name' => $name,
                'type' => '2',
                'description' => $desc,
                'created_at' => time(),
                'updated_at' => time()
            ));
        }

        foreach ($this->list_permissions as $name){
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_QUALITY_MANAGER,
                'child' => $name
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->new_permissions as $name=>$desc) {
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => User::ROLE_QUALITY_MANAGER]);
        }

        foreach ($this->list_permissions as $name){
            $this->delete('{{%auth_item}}', ['name' => $name]);
        }

    }
}

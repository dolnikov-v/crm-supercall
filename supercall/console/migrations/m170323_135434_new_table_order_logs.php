<?php
use console\components\db\Migration;

/**
 * Class m170323_135434_new_table_order_logs
 */
class m170323_135434_new_table_order_logs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_logs}}', [
            'id'         => $this->primaryKey(),
            'order_id'   => $this->integer(),
            'old'        => $this->smallInteger(),
            'new'        => $this->smallInteger(),
            'comment'    => $this->text(),
            'queue_id'   => $this->integer(),
            'user_id'    => $this->integer(),
            'operator'   => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->execute('ALTER TABLE "order_logs" ADD COLUMN "fields" json');
        $this->execute('ALTER TABLE "order_logs" ADD COLUMN "product" json');
    
        $this->addForeignKey('fk-order_logs-order_id', '{{%order_logs}}', 'order_id', '{{%order}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey('fk-order_logs-queue_id', '{{%order_logs}}', 'queue_id', '{{%queue}}', 'id', self::RESTRICT, self::RESTRICT);
        $this->addForeignKey('fk-order_logs-user_id', '{{%order_logs}}', 'user_id', '{{%user}}', 'id', self::RESTRICT, self::RESTRICT);
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-order_logs-user_id', '{{%order_logs}}');
        $this->dropForeignKey('fk-order_logs-queue_id', '{{%order_logs}}');
        $this->dropForeignKey('fk-order_logs-order_id', '{{%order_logs}}');
    
        $this->dropTable('{{%order_logs}}');
    }
}

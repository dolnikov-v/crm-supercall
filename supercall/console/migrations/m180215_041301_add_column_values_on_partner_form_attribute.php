<?php
use console\components\db\Migration;
use common\modules\partner\models\PartnerFormAttribute;

/**
 * Class m180215_041301_add_column_values_on_partner_form_attribute
 */
class m180215_041301_add_column_values_on_partner_form_attribute extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(PartnerFormAttribute::tableName(), 'values', 'jsonb');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(PartnerFormAttribute::tableName(), 'values');
    }
}

<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180220_090549_add_translate_to_quality_control
 */
class m180220_090549_add_translate_to_quality_control extends Migration
{
    public $phrases = [
        'Статус оператора' => 'Operator status',
        'Статус аудитора' => 'Auditor status',
        'Система' => 'System',
        'Ошибка открытия заказ №{order_id} для аудита: {error}' => 'Error opening order # {order_id} for audit: {error}',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

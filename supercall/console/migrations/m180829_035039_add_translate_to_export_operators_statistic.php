<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180829_035039_add_translate_to_export_operators_statistic
 */
class m180829_035039_add_translate_to_export_operators_statistic extends Migration
{
    public $phrases = [
        'Ошибка создания директории для экспорта: {path}' => 'Error creating directory for export: {path}',
        'Ошибка сохранения файла: {filePath}' => 'Error saving file: {filePath}'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;
use backend\modules\operator\models\QuestionnaireBase;
use common\models\Country;
use common\models\User;

/**
 * Class m170628_065605_crerate_table_queestionnaire_base
 */
class m170628_065605_crerate_table_queestionnaire_base extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(QuestionnaireBase::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'controller_id' => $this->integer()->notNull(),
            'operator_id' => $this->integer()->notNull(),
            'is_finished' => $this->integer()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(null, QuestionnaireBase::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, QuestionnaireBase::tableName(), 'controller_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, QuestionnaireBase::tableName(), 'operator_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(QuestionnaireBase::tableName());
    }
}

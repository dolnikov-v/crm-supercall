<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180424_044553_add_translate_to_package_alert
 */
class m180424_044553_add_translate_to_package_alert extends Migration
{
    public $phrases = [
        'Не указаны товары по акции' => 'No products listed for shares',
        'Товары партнёра в данной стране не найдены' => 'Partner goods not found in this country',
        'Наименование акции составляется автоматически и включает в себя перфикс + продукт + продукты по акции' => 'The name of the package is compiled automatically and includes the suffix + product + products by shares',
        //'' => '',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;
use common\modules\partner\models\PartnerFormAttribute;
/**
 * Class m170821_100232_add_column_to_partner_form_attribute
 */
class m170821_100232_add_column_to_partner_form_attribute extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(PartnerFormAttribute::tableName(), 'is_klader', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(PartnerFormAttribute::tableName(), 'is_klader');
    }
}

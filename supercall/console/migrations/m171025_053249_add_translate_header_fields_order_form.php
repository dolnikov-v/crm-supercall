<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171025_053249_add_translate_header_fields_order_form
 */
class m171025_053249_add_translate_header_fields_order_form extends Migration
{
    public $phrases = [
        'Наименование' => 'Name',
        'Акция' => 'Promotion',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

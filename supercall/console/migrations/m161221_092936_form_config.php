<?php
use console\components\db\Migration;

/**
 * Class m161221_092936_form_config
 */
class m161221_092936_form_config extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_form_attribute}}', 'regex', $this->string());
        $this->addColumn('{{%partner_form_attribute}}', 'length', $this->integer());
        $this->addColumn('{{%partner_form}}', 'extra_price', $this->float());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%partner_form}}', 'extra_price');
        $this->dropColumn('{{%partner_form_attribute}}', 'regex');
        $this->dropColumn('{{%partner_form_attribute}}', 'length');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m000000_000017_partner_product
 */
class m000000_000017_partner_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%partner_product}}', [
            'id' => $this->primaryKey(),
            'partner_id' => $this->integer()->notNull(),
            'partner_product_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'active' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, '{{%partner_product}}', 'partner_id', '{{%partner}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%partner_product}}', 'product_id', '{{%product}}', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%partner_product}}');
    }
}

<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180212_024823_add_translate_change_operator_order_form
 */
class m180212_024823_add_translate_change_operator_order_form extends Migration
{
    public $phrases = [
        'Вы действительно хотите назначить данного оператора на этот заказ ?' => 'Are you sure you want to assign this operator to this order ?',
        'Оператор успешно назначен' => 'The operator was successfully assigned',
        'Список операций' => 'List of operations'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

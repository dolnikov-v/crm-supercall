<?php
use console\components\db\Migration;
use common\modules\order\models\CountOrdersData;
use common\modules\order\models\CountOrdersOnPhone;
use common\modules\order\models\Order;
/**
 * Class m170817_064810_create_table_count_orders_data
 */
class m170817_064810_create_table_count_orders_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(CountOrdersData::tableName(), [
            'id' => $this->primaryKey(),
            'count_orders_on_phone_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(null, CountOrdersData::tableName(), 'count_orders_on_phone_id', CountOrdersOnPhone::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, CountOrdersData::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(CountOrdersData::tableName());
    }
}

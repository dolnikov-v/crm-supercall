<?php
use console\components\db\Migration;
use common\modules\order\models\Order;

/**
 * Class m180117_062348_alter_column_customer_id_in_order_set_varchar_255
 */
class m180117_062348_alter_column_customer_id_in_order_set_varchar_255 extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(Order::tableName(), 'customer_ip', $this->string(42));
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(Order::tableName(), 'customer_ip', $this->string(15));
    }
}

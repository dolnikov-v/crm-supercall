<?php
use console\components\db\Migration;

/**
 * Class m181212_031730_add_country_phone_code_validate_type
 */
class m181212_031730_add_country_phone_code_validate_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%country_phone_code}}','validate_type', $this->integer()->notNull()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%country_phone_code}}','validate_type');
    }
}

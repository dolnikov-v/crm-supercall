<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180226_024932_add_translate_to_audited_orders
 */
class m180226_024932_add_translate_to_audited_orders extends Migration
{
    public $phrases = [
        'Проверенные заказы' => 'Audited orders',
        'Аудитор' => 'Auditor',
        'Просмотр изменений' => 'View changes',
        'Просмотр заказа' => 'View order',
        'Просмотр аудита' => 'View audit',
        'Редактирование заказа' => 'Order editing',
        'Аудит не найден' => 'Audit not found',
        'Заказ не найден' => 'Order not found',
        'При попытке получить заказ по аудиту произошла ошибка' => 'An error occurred while trying to receive an audit order',
        'Таблица с аудитом' => 'Table with audit',
        'Аудит' => 'Audit',
        'Аудит / Проверенные заказы / Просмотр' => 'Audit / Proved Orders / Review',
        'Аудит / Проверенные заказы / Просмотр изменений' => 'Audit / Proved Orders / Reviewing Changes',
        'Аудит / Проверенные заказы / Просмотр аудита' => 'Audit / Audited Orders / Audit Review',
        'Не найдено изменений данного заказа аудитором' => 'No changes were found to this order by the auditor',
        'По данным полям заказа не найдено изменений операторами' => 'No ordering information found for order fields',
        'Форма заказа не определена' => 'Order form is not defined',
        'Изменение заказа' => 'Order changing',
        'Изменение товара' => 'Product changing',
        'Не определён' => 'Indefined',
        'Старое значение' => 'Old value',
        'Новое значение' => 'New value',
        'Данных нет' => 'No data',
        'Действие' => 'Action',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

    }
}

<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180920_113547_add_translate_all_country
 */
class m180920_113547_add_translate_all_country extends Migration
{
    public $phrases = [
        'Индия' => 'India',
        'Таиланд' => 'Thailand',
        'Мексика' => 'Mexico',
        'Румыния' => 'Romania',
        'Австрия' => 'Austria',
        'Болгария' => 'Bulgaria',
        'Швейцария' => 'Switzerland',
        'Китай' => 'China',
        'Кипр' => 'Cyprus',
        'Чехия' => 'Czech Republic',
        'Германия' => 'Germany',
        'Испания' => 'Spain',
        'Франция' => 'France',
        'Греция' => 'Greece',
        'Венгрия' => 'Hungary',
        'Италия' => 'Italy',
        'Латвия' => 'Latvia',
        'Польша' => 'Poland',
        'Португалия' => 'Portugal',
        'Россия' => 'Russia',
        'Словакия' => 'Slovakia',
        'Филиппины' => 'Philippines',
        'Индонезия' => 'Indonesia',
        'Малайзия' => 'Malaysia',
        'Сингапур' => 'Singapore',
        'Чили' => 'Chile',
        'Гватемала' => 'Guatemala',
        'Колумбия' => 'Colombia',
        'Тунис' => 'Tunisia',
        'Шри Ланка' => 'Sri Lanka',
        'ЮАР' => 'South Africa',
        'Оман' => 'Oman',
        'Бахрейн' => 'Bahrain',
        'Иордания' => 'Jordan',
        'Саудовская Аравия' => 'Saudi Arabia',
        'ОАЭ' => 'United Arab Emirates',
        'Катар' => 'Qatar',
        'Вьетнам' => 'Vietnam',
        'Перу' => 'Peru',
        'Тайвань' => 'Taiwan',
        'Гонконг' => 'Hong Kong',
        'Пакистан' => 'Pakistan',
        'Бангладеш' => 'Bangladesh',
        'Камбоджа' => 'Cambodia',
        'Лаос' => 'Laos'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {


        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170321_201353_add_index_order
 */
class m170321_201353_add_index_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('idx_order_partner_id', '{{%order}}', 'partner_id');
        $this->createIndex('idx_order_foreign_id', '{{%order}}', 'foreign_id', true);
        $this->createIndex('idx_order_created_at', '{{%order}}', 'created_at');
        $this->createIndex('idx_order_updated_at', '{{%order}}', 'updated_at');
        $this->createIndex('idx_order_customer_phone', '{{%order}}', 'customer_phone');
        $this->createIndex('idx_order_customer_mobile', '{{%order}}', 'customer_mobile');
        $this->createIndex('idx_order_customer_last_name_customer_first_name_customer_middle_name',
            '{{%order}}',
            ['customer_last_name', 'customer_first_name', 'customer_middle_name']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    
        $this->dropIndex('idx_order_partner_id', '{{%order}}');
        $this->dropIndex('idx_order_foreign_id', '{{%order}}');
        $this->dropIndex('idx_order_created_at', '{{%order}}');
        $this->dropIndex('idx_order_updated_at', '{{%order}}');
        $this->dropIndex('idx_order_customer_phone', '{{%order}}');
        $this->dropIndex('idx_order_customer_mobile', '{{%order}}');
        $this->dropIndex('idx_order_customer_last_name_customer_first_name_customer_middle_name', '{{%order}}');
    }
}

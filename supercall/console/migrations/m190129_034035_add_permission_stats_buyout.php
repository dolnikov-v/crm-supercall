<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use console\components\db\Migration;

/**
 * Class m190129_034035_add_permission_stats_buyout
 */
class m190129_034035_add_permission_stats_buyout extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addPermission(['stats.orderbuyout.index' => 'Статистика по выкупам']);
        $this->addPermissionToRole('stats.orderbuyout.index', \common\models\User::ROLE_ADMINISTRATOR);
        $this->addPermissionToRole('stats.orderbuyout.index', \common\models\User::ROLE_CURATOR);
        $this->addPermissionToRole('stats.orderbuyout.index', \common\models\User::ROLE_AUDITOR);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->deletePermissionByRole('stats.orderbuyout.index', \common\models\User::ROLE_ADMINISTRATOR);
        $this->deletePermissionByRole('stats.orderbuyout.index', \common\models\User::ROLE_CURATOR);
        $this->deletePermissionByRole('stats.orderbuyout.index', \common\models\User::ROLE_AUDITOR);
        $this->deletePermission('stats.orderbuyout.index');
    }
}

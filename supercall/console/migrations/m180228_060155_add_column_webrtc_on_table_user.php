<?php
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180228_060155_add_column_webrtc_on_table_user
 */
class m180228_060155_add_column_webrtc_on_table_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'webrtc', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(User::tableName(), 'webrtc');
    }
}

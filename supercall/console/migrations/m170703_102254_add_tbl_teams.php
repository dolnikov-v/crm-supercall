<?php
use console\components\db\Migration;

/**
 * Class m170703_102254_add_tbl_teams
 */
class m170703_102254_add_tbl_teams extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%team}}', [
            'id'    => $this->primaryKey(),
            'name'   => $this->string(),
            'active'   => $this->boolean()->defaultValue(true),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addColumn('{{%user}}', 'team_id', $this->smallInteger());
        $this->addForeignKey('fk__user__team_id', '{{%user}}', 'team_id', '{{%team}}', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk__user__team_id', '{{%user}}');
        $this->dropColumn('{{%user}}', 'team_id');
        $this->dropTable('{{%team}}');
    }
}

<?php
use console\components\db\Migration;
use backend\modules\widget\models\WidgetUser;

/**
 * Class m170901_113023_create_widget_user
 */
class m170901_113023_create_widget_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(WidgetUser::tableName(), [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'options' => $this->string(2000),
        ], $this->tableOptions);

        $this->addForeignKey(null, 'widget_user', 'user_id', 'user', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, 'widget_user', 'type_id', 'widget_type', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(WidgetUser::tableName());
    }
}

<?php
use console\components\db\Migration;
use common\modules\order\models\Order;


/**
 * Class m170331_105649_order_correction
 */
class m170331_105649_order_correction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //откатываем статусы которые были сначала Approved а потом получили другой статус
        $query = (new \yii\db\Query())->select('*')
            ->from('{{%order_log}}')
            ->where("field='status' and old = '4'");

        $ids = [];
        foreach ($query->batch(10) as $ols) {
            foreach ($ols as $ol) {
                $ids[] = $ol['id'];
                $this->update('{{%order}}', [
                    'status' => Order::STATUS_APPROVED,
                    'sub_status' => Order::SUB_STATUS_BUYOUT
                ], 'id=' . $ol['order_id']);
            }
        }

        $this->delete('{{%order_log}}', ['id' => $ids]);

    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return true;
    }
}

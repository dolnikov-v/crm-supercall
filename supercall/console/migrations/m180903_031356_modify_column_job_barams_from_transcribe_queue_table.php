<?php
use console\components\db\Migration;

/**
 * Class m180903_031356_modify_column_job_barams_from_transcribe_queue_table
 */
class m180903_031356_modify_column_job_barams_from_transcribe_queue_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE {{%transcribe_queue}} ALTER COLUMN job_params TYPE JSONB USING(job_params::jsonb)');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('ALTER TABLE {{%transcribe_queue}} ALTER COLUMN job_params TYPE VARCHAR(255)  USING(job_params::text)');
    }
}

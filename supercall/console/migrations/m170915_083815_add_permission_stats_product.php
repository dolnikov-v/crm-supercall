<?php
use console\components\db\Migration;
use common\models\User;

/**
 * Class m170915_083815_add_permission_stats_product
 */
class m170915_083815_add_permission_stats_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => 'stats.product.index',
            'type' => '2',
            'description' => 'stats.product.index',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'stats.product.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_CURATOR,
            'child' => 'stats.product.index'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->delete($this->authManager->itemChildTable, ['child' => 'stats.product.index', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => 'stats.product.index', 'parent' => User::ROLE_CURATOR]);

        $this->delete('{{%auth_item}}', ['name' => 'stats.product.index']);
    }
}

<?php
use common\models\AutoCallCountry;
use common\models\Country;
use console\components\db\Migration;

/**
 * Class m180718_055350_add_table_auto_call_country
 */
class m180718_055350_add_table_auto_call_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(AutoCallCountry::tableName(), [
                'id' => $this->primaryKey(),
                'country_id' => $this->integer()->notNull(),
                'count' => $this->integer()->defaultValue(1),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer()
            ]
        );

        $this->addForeignKey(null, AutoCallCountry::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(AutoCallCountry::tableName());
    }
}

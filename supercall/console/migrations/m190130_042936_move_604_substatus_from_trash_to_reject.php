<?php

/**
 * Создание роли (с проверкой на наличие)
 * $role = [name => description]
 * $this->addRole(array $role)
 *
 * Удаление роли
 * $this-deleteRole(string $role)
 *
 * Проверка наличия роли
 * $this->existsRole(string $role_name);
 *
 * Создание пермишена (с проверкой на наличие)
 * $permission = [name => description]
 * $this->addPermission(array $permission)
 *
 * Удаление пермишена (автоматом удаляются все связки по ролям)
 * $this->deletePermission(string $permission);
 *
 * Проверка наличия пермишена
 * $this->>existsPermission(string $permission_name)
 *
 * Добавление пермишена к роли
 * $this->addPermissionToRole(string $permission, string $role)
 *
 * Удаления пермишена у роли
 * $this->deletePermissionByRole(string $permission, string $role)
 *
 * Добавление перевода (удаление перед добавление уже внутри метода)
 * i18n_source_message::addTranslate($this->phrases, true);
 *
 * Удаление переводов
 * i18n_source_message::removeTranslate($this->phrases);
 */

use common\modules\order\models\Order;
use common\modules\order\models\OrderSubStatus;
use console\components\db\Migration;

/**
 * Class m190130_042936_move_604_substatus_from_trash_to_reject
 */
class m190130_042936_move_604_substatus_from_trash_to_reject extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $subStatus514 = new OrderSubStatus();
        $subStatus514->group_id = Order::SUB_STATUS_GROUP_REJECT;
        $subStatus514->code = Order::SUB_STATUS_REJECT_REASON_CONSULTING;
        $subStatus514->name = "Консультация";
        $subStatus514->created_at = time();
        $subStatus514->updated_at = time();
        $subStatus514->save(false);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $subStatus514 = OrderSubStatus::findOne(['code' => Order::SUB_STATUS_REJECT_REASON_CONSULTING]);

        if ($subStatus514) {
            $subStatus514->delete();
        }

        $subStatus604 = new OrderSubStatus();
        $subStatus604->group_id = Order::SUB_STATUS_GROUP_TRASH;
        $subStatus604->code = Order::SUB_STATUS_TRASH_REASON_CONSULTING;
        $subStatus604->name = "Консультация";
        $subStatus604->created_at = time();
        $subStatus604->updated_at = time();
        $subStatus604->save(false);

    }
}

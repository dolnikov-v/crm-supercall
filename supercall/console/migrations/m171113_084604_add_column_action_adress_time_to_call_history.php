<?php
use console\components\db\Migration;
use common\modules\call\models\CallHistory;
use backend\models\i18n_source_message;

/**
 * Class m171113_084604_add_column_action_adress_time_to_call_history
 */
class m171113_084604_add_column_action_adress_time_to_call_history extends Migration
{
    public $phrases = [
        'Заполнение адреса, сек.' => 'Address filling, sec.'
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        i18n_source_message::addTranslate($this->phrases, true);
        $this->addColumn(CallHistory::tableName(), 'action_address_time', $this->integer()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        i18n_source_message::removeTranslate($this->phrases);
        $this->dropColumn(CallHistory::tableName(), 'action_address_time');
    }
}

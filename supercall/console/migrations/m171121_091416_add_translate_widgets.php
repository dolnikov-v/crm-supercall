<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171121_091416_add_translate_widgets
 */
class m171121_091416_add_translate_widgets extends Migration
{
    public $phrases = [
        'Период создания' => 'Creation period',
        'Процент' => 'Percent',
        'Товары' => 'Goods',
        'Количество онлайн' => 'Number of online',
        'Операторы онлайн' => 'Operators online',
        'Очередь/Страна' => 'Queue / Country',
        'Звонки' => 'Calls',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170629_051656_add_colums_order
 */
class m170629_051656_add_colums_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'queue_type', $this->string()->defaultValue(''));
        $this->addColumn('{{%order}}', 'order_comment', $this->string()->defaultValue(''));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'queue_type');
        $this->dropColumn('{{%order}}', 'order_comment');
    }
}

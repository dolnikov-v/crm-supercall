<?php

use common\models\UserReady;
use console\components\db\Migration;

/**
 * Class m180531_060614_add_index_user_id_to_user_ready
 */
class m180531_060614_add_index_user_id_to_user_ready extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('idx_user_ready_user_id',  UserReady::tableName(), 'user_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('idx_user_ready_user_id', UserReady::tableName());
    }
}

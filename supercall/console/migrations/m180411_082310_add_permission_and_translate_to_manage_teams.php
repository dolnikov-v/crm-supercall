<?php

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180411_082310_add_permission_and_translate_to_manage_teams
 */
class m180411_082310_add_permission_and_translate_to_manage_teams extends Migration
{
    public $phrases = [
        'Редактирование команд пользователя "{username}"' => 'Editing user teams "{username}"',
        'Сохранить команды' => 'Save teams',
        'Управление командами пользователя' => 'Team Management',
        'Ошибка добавления пользователя в команды' => 'Error adding user to teams',
        'Команды пользователя успешно обновлены' => 'User teams updated successfully',
        'Исключенные пользователи' => 'Excluded Users',
        'Проверка совместимости пользователей' => 'Checking user compatibility',
        'Операторы / Список операторов / Управление командами пользователя' => 'Operators / List of operators / Managing user commands',
        'Доступы / Список команд / Редактирование (управление пользователями)' => 'Accesses / List of commands / Editing (user management)'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);

        $this->insert('{{%auth_item}}', array(
            'name' => 'access.team.manageuserofteam',
            'type' => '2',
            'description' => 'Доступы / Список команд / Редактирование (управление пользователями)',
            'created_at' => time(),
            'updated_at' => time()
        ));


        $this->insert('{{%auth_item}}', array(
            'name' => 'operator.listoperators.manageteam',
            'type' => '2',
            'description' => 'Операторы / Список операторов / Управление командами пользователя',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'access.team.manageuserofteam'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'operator.listoperators.manageteam'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);


        $this->delete($this->authManager->itemChildTable, ['child' => 'access.team.manageuserofteam', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => 'operator.listoperators.manageteam', 'parent' => User::ROLE_SUPERVISOR]);

        $this->delete('{{%auth_item}}', ['name' => 'access.team.manageuserofteam']);
        $this->delete('{{%auth_item}}', ['name' => 'operator.listoperators.manageteam']);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170502_093604_tbl_user_ready
 */
class m170502_093604_tbl_user_ready extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%user_ready}}', [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer(),
            'start_time' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
            'stop_time'  => 'TIMESTAMP DEFAULT NULL',
        ]);
        
        $this->addForeignKey('fk-user_ready-user_id', '{{%user_ready}}', 'user_id', '{{%user}}', 'id', self::CASCADE, self::RESTRICT);
        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_ready}}');
    }
}

<?php

use backend\models\i18n_message;
use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180528_040515_delete_permission_change_status
 */
class m180528_040515_delete_permission_change_status extends Migration
{
    public $phrases = [
        'Отправка в NPay' => 'Send to NPay',
        'Тип заказа' => 'Order type',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        $this->update(i18n_message::tableName(), ['translation' => 'Non-buyouts'], ['translation' => 'Non-payment']);


        $this->delete($this->authManager->itemChildTable,[
            'parent' => User::ROLE_CURATOR,
            'child' => 'order.groupoperations.changestatus'
        ]);

        $this->delete($this->authManager->itemChildTable,[
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'order.groupoperations.changestatus'
        ]);

        $this->delete($this->authManager->itemChildTable,[
            'parent' => User::ROLE_CURATOR,
            'child' => 'order.groupoperations.senttosqs'
        ]);

        $this->delete($this->authManager->itemChildTable,[
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'order.groupoperations.senttosqs'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use common\models\User;
use console\components\db\Migration;
use backend\modules\operator\models\QualityControl;

/**
 * Class m180124_061920_alter_colom_operator_id_on_order_quality_control_to_default_null
 */
class m180124_061920_alter_colom_operator_id_on_order_quality_control_to_default_null extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(QualityControl::tableName(), 'operator_id');
        $this->dropColumn(QualityControl::tableName(), 'penalty_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn(QualityControl::tableName(), 'operator_id', $this->integer()->notNull());
        $this->addColumn(QualityControl::tableName(), 'penalty_id', $this->integer()->defaultValue(null));
        $this->addForeignKey(null, QualityControl::tableName(), 'operator_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170110_133528_order_last_name_notnull
 */
class m170110_133528_order_last_name_notnull extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('alter table {{%order}} alter column customer_last_name drop not null');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->execute('alter table {{%order}} alter column customer_last_name set not null');
    }
}

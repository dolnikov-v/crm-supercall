<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180412_045517_add_permission_for_view_contacts_clients
 */
class m180412_045517_add_permission_for_view_contacts_clients extends Migration
{
    public $translate = [
        'Заказа / Список заказов' => 'Orders / Orders list'
    ];
    public $permission = [
        'name' => 'order.index.viewcontacts',
        'description' => 'Заказа / Список заказов'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->translate, true);

        $this->insert('{{%auth_item}}', array(
            'name' => $this->permission['name'],
            'type' => '2',
            'description' => $this->permission['description'],
            'created_at' => time(),
            'updated_at' => time()
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        i18n_source_message::removeTranslate($this->translate);
        $this->delete('{{%auth_item}}', ['name' => $this->permission['name']]);
    }
}

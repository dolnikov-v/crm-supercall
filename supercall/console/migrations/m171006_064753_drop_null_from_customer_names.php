<?php
use console\components\db\Migration;
use common\modules\order\models\Order;
/**
 * Class m171006_064753_drop_null_from_customer_names
 */
class m171006_064753_drop_null_from_customer_names extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(Order::tableName(), 'customer_first_name', 'DROP NOT NULL');
        $this->alterColumn(Order::tableName(), 'customer_first_name', 'SET DEFAULT NULL');

        $this->alterColumn(Order::tableName(), 'customer_full_name', 'DROP NOT NULL');
        $this->alterColumn(Order::tableName(), 'customer_full_name', 'SET DEFAULT NULL');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}

<?php

use console\components\db\Migration;
use backend\modules\catalog\models\ScriptsOperators;
use common\models\Country;
use common\modules\partner\models\Partner;
use common\models\Product;
/**
 * Handles the creation of table `scripts_operators`.
 */
class m171011_045451_create_scripts_operators_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(ScriptsOperators::tableName(), [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'content' => $this->text(),
            'created_user_id' => $this->integer()->notNull(),
            'updated_user_id' => $this->integer()->defaultValue(null),
            'active' => $this->boolean()->defaultValue(false),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, ScriptsOperators::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, ScriptsOperators::tableName(), 'partner_id', Partner::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, ScriptsOperators::tableName(), 'product_id', Product::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(ScriptsOperators::tableName());
    }
}

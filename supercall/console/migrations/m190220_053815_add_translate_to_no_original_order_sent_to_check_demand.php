<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m190220_053815_add_translate_to_no_original_order_sent_to_check_demand
 */
class m190220_053815_add_translate_to_no_original_order_sent_to_check_demand extends Migration
{
    public $phrases = [
        'Не оригинал' => 'Not original',
        'Заказ #{id}. ошибка создания дубликата для отправки в очередь: {error}' => 'Order #{id}. Failed to create a duplicate to send to the queue: {error}',
        'По заказу #{id} создан дубликат #{duplicate_id} и отправлен в очередь' => 'By request #{id} duplicate #{duplicate_id} was created and sent to the queue',
        'Отправить в очередь Check demand' => 'Send to queue Check demand',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

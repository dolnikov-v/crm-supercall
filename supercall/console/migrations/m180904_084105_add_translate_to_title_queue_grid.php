<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180904_084105_add_translate_to_title_queue_grid
 */
class m180904_084105_add_translate_to_title_queue_grid extends Migration
{

    public $phrases = [
        'Доступны сейчас -> Доступны через 1 час' => 'Available now -> Available after 1 hour',
        'Всего заказов | Необработанных заказов' => 'Total orders | Unprocessed orders'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170519_072409_alter_user_ready
 */
class m170519_072409_alter_user_ready extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-user_ready-user_id', '{{%user_ready}}');
        $this->dropTable('{{%user_ready}}');
    
        $this->createTable('{{%user_ready}}', [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer(),
            'start_time' => $this->integer()->notNull(),
            'stop_time'  => $this->integer(),
            'diff'       => $this->integer(),
        ]);
    
        $this->alterColumn('{{%user_ready}}', 'stop_time', 'DROP NOT NULL');
        $this->alterColumn('{{%user_ready}}', 'stop_time', 'SET DEFAULT NULL');
    
        $this->addForeignKey('fk-user_ready-user_id', '{{%user_ready}}', 'user_id', '{{%user}}', 'id', self::CASCADE, self::RESTRICT);
    
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}

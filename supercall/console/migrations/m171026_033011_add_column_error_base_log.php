<?php
use console\components\db\Migration;
use common\models\Logger;

/**
 * Class m171026_033011_add_column_error_base_log
 */
class m171026_033011_add_column_error_base_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Logger::tableName(), 'error', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Logger::tableName(), 'error');
    }
}

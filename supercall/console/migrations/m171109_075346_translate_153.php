<?php
use console\components\db\Migration;

use backend\models\i18n_source_message;

/**
 * Class m171109_075346_translate_153
 */
class m171109_075346_translate_153 extends Migration
{
    public $phrases = [
        'ID заказа' => 'Order ID',
        'Тип даты' => 'Date type',
        'Все' => 'All',
        'Ошибки в телефоне' => 'Errors in the phone',
        'Таблица с заказами' => 'Order table',
        'Номер' => 'Number',
        'Дата' => 'Date',
        'Добро пожаловать в 2WCall' => 'Welcome to 2WCall',
        'Одобрено с попыток' => 'Approved with attempts',
        'Попыток' => 'Try',
        'Заказов' => 'Orders',
        'Выкуплено' => 'Redeemed',
        'Не выкуплено' => 'Not redeemed',
        'Опреатор' => 'Operator',
        'Команда' => 'Team',
        'Таблица с данными' => 'Data table',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

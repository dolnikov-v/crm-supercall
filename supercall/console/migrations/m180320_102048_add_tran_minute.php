<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180320_102048_add_tran_minute
 */
class m180320_102048_add_tran_minute extends Migration
{
    public $phrases = [
        'Минута' => 'Minute',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

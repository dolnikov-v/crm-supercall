<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180126_112543_add_translate_number_of_operators
 */
class m180126_112543_add_translate_number_of_operators extends Migration
{
    public $phrases = [
        'Количество операторов' => 'Number of operators'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use common\models\Product;
use console\components\db\Migration;

/**
 * Class m000000_000013_product
 */
class m000000_000013_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        foreach ($this->fixtures() as $key => $fixture) {
            $country = new Product($fixture);
            $country->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }

    /**
     * @return array
     */
    private function fixtures()
    {
        return [
            ['name' => 'Goji cream'],
            ['name' => 'Titan Gel'],
            ['name' => 'Goji berry'],
            ['name' => 'Don Vigaron'],
            ['name' => 'Brutalin'],
            ['name' => 'Vigaron hammer'],
            ['name' => 'Green coffee'],
            ['name' => 'Power balance'],
            ['name' => 'Valgustep'],
            ['name' => 'Cruz de Toretto'],
            ['name' => 'Corazon de Ocean'],
            ['name' => 'Provocative Gel'],
            ['name' => 'Hammer of Thor'],
            ['name' => 'Carrot mask'],
            ['name' => 'White mask'],
            ['name' => 'Snore clip'],
            ['name' => 'Antihrap'],
            ['name' => 'Eye mask'],
            ['name' => 'Teeth whitening'],
            ['name' => 'Turboslim'],
            ['name' => 'Goji Cream Hendel'],
            ['name' => 'Miracle glow'],
        ];
    }
}

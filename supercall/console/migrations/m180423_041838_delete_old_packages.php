<?php

use common\modules\partner\models\Package;
use console\components\db\Migration;

/**
 * Class m180423_041838_delete_old_packages
 */
class m180423_041838_delete_old_packages extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        yii::$app->db->createCommand('TRUNCATE TABLE ' . Package::tableName() . ' CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}

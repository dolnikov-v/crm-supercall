<?php
use console\components\db\Migration;

/**
 * Class m170517_182341_alter_foreign_order
 */
class m170517_182341_alter_foreign_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('{{%order}}', 'foreign_id', 'DROP NOT NULL');
        $this->alterColumn('{{%order}}', 'foreign_id', 'SET DEFAULT NULL');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}

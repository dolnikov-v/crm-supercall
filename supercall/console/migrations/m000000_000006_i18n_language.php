<?php
use common\models\Language;
use console\components\db\Migration;

/**
 * Class m000000_000006_i18n_language
 */
class m000000_000006_i18n_language extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%i18n_language}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull(),
            'icon' => $this->string(20)->notNull(),
            'locale' => $this->string(8)->notNull(),
            'source_language' => $this->smallInteger()->notNull()->defaultValue(0),
            'active' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        foreach ($this->fixtures() as $fixture) {
            $language = new Language($fixture);
            $language->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%i18n_language}}');
    }

    /**
     * @return array
     */
    private function fixtures()
    {
        return [
            [
                'name' => 'Русский',
                'icon' => 'flag-icon-ru',
                'locale' => 'ru-RU',
                'source_language' => 1,
                'active' => 1,
            ],
            [
                'name' => 'English',
                'icon' => 'flag-icon-gb',
                'locale' => 'en-US',
                'source_language' => 0,
                'active' => 1,
            ],
        ];
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170323_054432_add_comment_to_log
 */
class m170323_054432_add_comment_to_log extends Migration
{
    /**
     * Комментарии к заказу привязываем к таблице order_log
     *
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_log}}', 'comment', $this->text()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order_log}}', 'comment');
    }
}

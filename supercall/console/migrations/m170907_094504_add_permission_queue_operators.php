<?php
use console\components\db\Migration;
use common\models\User;

/**
 * Class m170907_094504_add_permission_queue_operators
 */
class m170907_094504_add_permission_queue_operators extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%auth_item}}', array(
            'name' => User::ROLE_SUPERVISOR,
            'type' => '1',
            'description' => 'Супервизор',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert('{{%auth_item}}', array(
            'name' => 'operator.listoperators.setproduct',
            'type' => '2',
            'description' => 'operator.listoperators.setproduct',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert('{{%auth_item}}', array(
            'name' => 'operator.listoperators.queue',
            'type' => '2',
            'description' => 'operator.listoperators.queue',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert('{{%auth_item}}', array(
            'name' => 'operator.listoperators.queueset',
            'type' => '2',
            'description' => 'operator.listoperators.queueset',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert('{{%auth_item}}', array(
            'name' => 'operator.listoperators.queueunset',
            'type' => '2',
            'description' => 'operator.listoperators.queueunset',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'operator.listoperators.index'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'operator.listoperators.queue'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'operator.listoperators.queueset'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'operator.listoperators.queueunset'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'operator.listoperators.setproduct'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'home.index.index'
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('{{%auth_item}}', ['name' =>  User::ROLE_SUPERVISOR]);

        $this->delete($this->authManager->itemChildTable, ['child' => 'operator.listoperators.queue', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete('{{%auth_item}}', ['name' => 'operator.listoperators.queue']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'operator.listoperators.queueset', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete('{{%auth_item}}', ['name' => 'operator.listoperators.queueset']);

        $this->delete($this->authManager->itemChildTable, ['child' => 'operator.listoperators.queueunset', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete('{{%auth_item}}', ['name' => 'operator.listoperators.queueunset']);


        $this->delete($this->authManager->itemChildTable, ['child' => 'operator.listoperators.setproduct', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => 'operator.listoperators.index', 'parent' => User::ROLE_SUPERVISOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => 'home.index.index', 'parent' => User::ROLE_SUPERVISOR]);
    }
}

<?php
use console\components\db\Migration;
use backend\modules\operator\models\ProductOperator;
use common\models\User;
use common\models\Product;

/**
 * Class m170626_063818_create_table_product_operator
 */
class m170626_063818_create_table_product_operator extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(ProductOperator::tableName(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(null, ProductOperator::tableName(), 'user_id', User::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, ProductOperator::tableName(), 'product_id', Product::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(ProductOperator::tableName());
    }
}

<?php

use console\components\db\Migration;
use backend\modules\catalog\models\PriceShipping;
use common\modules\partner\models\Partner;
use common\models\Shipping;
use common\models\Country;
use common\modules\partner\models\PartnerProduct;

/**
 * Handles the creation of table `price_shipping`.
 */
class m170623_044130_create_price_shipping_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(PriceShipping::tableName(), [
            'id' => $this->primaryKey(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'shipping_id' => $this->integer()->notNull(),
            'partner_product_id' => $this->integer()->notNull(),
            'price_shipping' =>$this->double(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(null, PriceShipping::tableName(), 'partner_id', Partner::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, PriceShipping::tableName(), 'shipping_id', Shipping::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, PriceShipping::tableName(), 'country_id', Country::tableName(), 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, PriceShipping::tableName(), 'partner_product_id', PartnerProduct::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(PriceShipping::tableName());
    }
}

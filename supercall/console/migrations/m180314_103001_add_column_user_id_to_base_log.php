<?php
use console\components\db\Migration;
use backend\modules\administration\models\BaseLogger;

/**
 * Class m180314_103001_add_column_user_id_to_base_log
 */
class m180314_103001_add_column_user_id_to_base_log extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(BaseLogger::tableName(), 'user_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(BaseLogger::tableName(), 'user_id');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170326_112854_stats_olap
 */
class m170326_112854_stats_olap extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_olap_general}}', [
            'period_type' => $this->smallInteger(1)->notNull(),
            'period' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull() . ' DEFAULT 0',
        ]);

        $this->execute('ALTER TABLE {{%order_olap_general}} ADD price_data JSONB;');

        $this->addForeignKey(
            'fk_oog_product',
            '{{%order_olap_general}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_oog_type',
            '{{%order_olap_general}}',
            'type_id',
            '{{%order_type}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_oog_partner',
            '{{%order_olap_general}}',
            'partner_id',
            '{{%partner}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_oog_country',
            '{{%order_olap_general}}',
            'country_id',
            '{{%country}}',
            'id',
            'CASCADE'
        );

        $this->addPrimaryKey('pk_oog', '{{%order_olap_general}}', [
            'period_type',
            'country_id',
            'type_id',
            'product_id',
            'partner_id',
            'status',
            'period',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_olap_general}}');
    }
}

<?php
use console\components\db\Migration;
use common\modules\partner\models\Partner;

/**
 * Class m170719_102757_add_column_token_to_partner
 */
class m170719_102757_add_column_token_to_partner extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(Partner::tableName(), 'token', $this->string()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(Partner::tableName(), 'token');
    }
}

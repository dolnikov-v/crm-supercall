<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180525_115712_add_translate_and_permission_group_operation_change_queue
 */
class m180525_115712_add_translate_and_permission_group_operation_change_queue extends Migration
{
    public $phrases = [
        'Неудачная смена очереди заказа №{id}' => 'Unsuccessful order queue change # {id}',
        'Ошибка смены очереди у заказа #{id}. Страна заказа отличается от страны очереди' => 'Queue change error for order # {id}. Country of order differs from the country of the queue',
        'Заказы / Групповые операции / Смена очереди' => 'Orders / Group Operations / Queue Change'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        $this->insert('{{%auth_item}}', array(
            'name' => 'order.groupoperations.changequeue',
            'type' => '2',
            'description' => 'Заказы / Групповые операции / Смена очереди',
            'created_at' => time(),
            'updated_at' => time()
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
        $this->delete('{{%auth_item}}', ['name' => 'order.groupoperations.changequeue']);
    }
}

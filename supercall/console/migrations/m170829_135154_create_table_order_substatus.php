<?php
use console\components\db\Migration;

/**
 * Class m170829_135154_create_table_order_substatus
 */
class m170829_135154_create_table_order_substatus extends Migration
{
    const SUB_STATUS_BUYOUT = 402;
    const SUB_STATUS_NO_BUYOUT = 403;

    const SUB_STATUS_REJECT_REASON_EXPENSIVE = 501;
    const SUB_STATUS_REJECT_REASON_CHANGED_MIND = 502;
    const SUB_STATUS_REJECT_REASON_MEDICAL = 503;
    const SUB_STATUS_REJECT_REASON_NO_COMMENTS = 504;
    const SUB_STATUS_REJECT_REASON_AUTO = 505;
    const SUB_STATUS_REJECT_REASON_PRODUCT_UNKNOWN = 506;
    const SUB_STATUS_REJECT_REASON_ANOTHER_SHOP = 507;
    const SUB_STATUS_REJECT_REASON_BAD_FEEDBACK = 508;
    const SUB_STATUS_REJECT_REASON_PERSONAL_REASON = 509;
    const SUB_STATUS_REJECT_REASON_AFFORD = 510;

    const SUB_STATUS_TRASH_REASON_WRONG_NUMBER = 601;
    const SUB_STATUS_TRASH_REASON_DOUBLE = 602;
    const SUB_STATUS_TRASH_REASON_UNAWARE = 603;
    const SUB_STATUS_TRASH_REASON_CONSULTING = 604;
    const SUB_STATUS_TRASH_REASON_COMPETITOR = 605;
    const SUB_STATUS_TRASH_REASON_RETURN = 606;
    const SUB_STATUS_TRASH_REASON_DENIED = 607;
    const SUB_STATUS_TRASH_REASON_INCORRECT_FORMAT = 608;
    const SUB_STATUS_TRASH_REASON_JOKE_ORDER = 609;

    //Статусы для очереди "заказ не доставлен"
    const SUB_STATUS_CHECK_UNDELIVERED_DELIVERED = 2001;
    const SUB_STATUS_CHECK_UNDELIVERED_CLIENT_NOT_RESPOND = 2002;
    const SUB_STATUS_CHECK_UNDELIVERED_NUMBER_BUSY = 2003;
    const SUB_STATUS_CHECK_UNDELIVERED_NUMBER_NOT_SERVICED = 2004;
    const SUB_STATUS_CHECK_UNDELIVERED_AUTORESPONDER = 2005;
    const SUB_STATUS_CHECK_UNDELIVERED_NOT_ENOUGH_MONEY = 2006;
    const SUB_STATUS_CHECK_UNDELIVERED_CHANGED_MIND = 2007;
    const SUB_STATUS_CHECK_UNDELIVERED_NOT_ORDERING = 2008;
    const SUB_STATUS_CHECK_UNDELIVERED_BAD_REVIEWS = 2009;
    const SUB_STATUS_CHECK_UNDELIVERED_INCORRECT_ADDRESS = 2010;

    public static function getRejectReasonCollection()
    {
        return [
            self::SUB_STATUS_REJECT_REASON_EXPENSIVE => Yii::t('common', 'Слишком дорого'),
            self::SUB_STATUS_REJECT_REASON_CHANGED_MIND => Yii::t('common', 'Передумал'),
            self::SUB_STATUS_REJECT_REASON_MEDICAL => Yii::t('common', 'Медицинские противопоказания'),
            self::SUB_STATUS_REJECT_REASON_NO_COMMENTS => Yii::t('common', 'Без комментариев'),
            self::SUB_STATUS_REJECT_REASON_AUTO => Yii::t('common', 'Автоматическое отклонение'),
            self::SUB_STATUS_REJECT_REASON_PRODUCT_UNKNOWN => Yii::t('common', 'Не известный продукт'),
            self::SUB_STATUS_REJECT_REASON_ANOTHER_SHOP => Yii::t('common', 'Совершил покупку в другом магазине'),
            self::SUB_STATUS_REJECT_REASON_BAD_FEEDBACK => Yii::t('common', 'Плохие отзывы в интернете'),
            self::SUB_STATUS_REJECT_REASON_PERSONAL_REASON => Yii::t('common', 'Личная причина не хочет обсуждать'),
            self::SUB_STATUS_REJECT_REASON_AFFORD => Yii::t('common', 'Не может позволить себе'),
        ];
    }

    public static function getTrashReasonCollection()
    {
        return [
            self::SUB_STATUS_TRASH_REASON_WRONG_NUMBER => Yii::t('common', 'Несуществующий номер'),
            self::SUB_STATUS_TRASH_REASON_DOUBLE => Yii::t('common', 'Дубль'),
            self::SUB_STATUS_TRASH_REASON_UNAWARE => Yii::t('common', 'Клиент ничего не знает о заказе'),
            self::SUB_STATUS_TRASH_REASON_CONSULTING => Yii::t('common', 'Консультация'),
            self::SUB_STATUS_TRASH_REASON_COMPETITOR => Yii::t('common', 'Заказано у конкурентов'),
            self::SUB_STATUS_TRASH_REASON_RETURN => Yii::t('common', 'Возврат'),
            self::SUB_STATUS_TRASH_REASON_DENIED => Yii::t('common', 'Не отправляем туда'),
            self::SUB_STATUS_TRASH_REASON_INCORRECT_FORMAT => Yii::t('common', 'Не правильный формат телефона'),
            self::SUB_STATUS_TRASH_REASON_JOKE_ORDER => Yii::t('common', 'Шуточный заказ'),
        ];
    }

    public static function getUndeliveredReasonCollection()
    {
        return [
            self::SUB_STATUS_CHECK_UNDELIVERED_DELIVERED => Yii::t('common', 'Доставлено'),
            self::SUB_STATUS_CHECK_UNDELIVERED_CLIENT_NOT_RESPOND => Yii::t('common', 'Клиент не отвечает'),
            self::SUB_STATUS_CHECK_UNDELIVERED_NUMBER_BUSY => Yii::t('common', 'Номе занят'),
            self::SUB_STATUS_CHECK_UNDELIVERED_NUMBER_NOT_SERVICED => Yii::t('common', 'Номер не обслуживается'),
            self::SUB_STATUS_CHECK_UNDELIVERED_AUTORESPONDER => Yii::t('common', 'Автоответчик'),
            self::SUB_STATUS_CHECK_UNDELIVERED_NOT_ENOUGH_MONEY => Yii::t('common', 'Не хватает денег'),
            self::SUB_STATUS_CHECK_UNDELIVERED_CHANGED_MIND => Yii::t('common', 'Передумал'),
            self::SUB_STATUS_CHECK_UNDELIVERED_NOT_ORDERING => Yii::t('common', 'Не заказывал'),
            self::SUB_STATUS_CHECK_UNDELIVERED_BAD_REVIEWS => Yii::t('common', 'Плохой обзор'),
            self::SUB_STATUS_CHECK_UNDELIVERED_INCORRECT_ADDRESS =>Yii::t('common', 'Ошибочный адрес'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order_substatus_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->insert('order_substatus_group', [
            'name' => Yii::t('common', 'Выкупы'),
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('order_substatus_group', [
            'name' => Yii::t('common', 'Отказы'),
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('order_substatus_group', [
            'name' => Yii::t('common', 'Мусор'),
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('order_substatus_group', [
            'name' => Yii::t('common', 'Не доставленные'),
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->createTable('order_substatus', [
            'id' => $this->primaryKey(),
            'group_id' => $this->integer(11)->notNull(),
            'code' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->insert('order_substatus', [
            'code' => self::SUB_STATUS_BUYOUT,
            'group_id' => 1,
            'name' => Yii::t('common', 'Выкуплено'),
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        $this->insert('order_substatus', [
            'code' => self::SUB_STATUS_NO_BUYOUT,
            'group_id' => 1,
            'name' => Yii::t('common', 'Не выкуплено'),
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        foreach ($this->getRejectReasonCollection() as $key => $reason) {
            $this->insert('order_substatus', [
                'code' => $key,
                'group_id' => 2,
                'name' => $reason,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }

        foreach ($this->getTrashReasonCollection() as $key => $reason) {
            $this->insert('order_substatus', [
                'code' => $key,
                'group_id' => 3,
                'name' => $reason,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }

        foreach ($this->getUndeliveredReasonCollection() as $key => $reason) {
            $this->insert('order_substatus', [
                'code' => $key,
                'group_id' => 4,
                'name' => $reason,
                'created_at' => time(),
                'updated_at' => time(),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('order_substatus');
        $this->dropTable('order_substatus_group');
    }
}

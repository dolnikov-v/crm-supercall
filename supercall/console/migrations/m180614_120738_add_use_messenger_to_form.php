<?php

use common\modules\partner\models\PartnerForm;
use console\components\db\Migration;

/**
 * Class m180614_120738_add_use_messenger_to_form
 */
class m180614_120738_add_use_messenger_to_form extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(PartnerForm::tableName(), 'use_messenger', $this->boolean()->after('use_map')->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(PartnerForm::tableName(), 'use_messenger');
    }
}

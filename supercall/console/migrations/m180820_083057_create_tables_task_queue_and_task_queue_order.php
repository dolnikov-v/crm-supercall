<?php

use common\models\User;
use console\components\db\Migration;

/**
 * Class m180820_083057_create_tables_task_queue_and_task_queue_order
 */
class m180820_083057_create_tables_task_queue_and_task_queue_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%task_queue}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'type' => $this->string(),
            'user_id' => $this->integer(),
            'path' => $this->string(),
            'status' => $this->string(),
            'error' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, '{{%task_queue}}', 'user_id', User::tableName(), 'id', self::SET_NULL, self::CASCADE);

        $this->createTable('{{%task_queue_order}}', [
            'id' => $this->primaryKey(),
            'task_queue_id' => $this->integer(),
            'order_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(null, '{{%task_queue_order}}', 'task_queue_id', '{{%task_queue}}', 'id', self::NO_ACTION, self::NO_ACTION);
        $this->addForeignKey(null, '{{%task_queue_order}}', 'order_id', '{{%order}}', 'id', self::NO_ACTION, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%task_queue_order}}');
        $this->dropTable('{{%task_queue}}');
    }
}

<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m190306_093133_add_permission_for_forecast_orders
 */
class m190306_093133_add_permission_for_forecast_orders extends Migration
{
    public $permission = 'stats.forecastorders.index';

    public $phrases = [
        'Прогнозирование количества заказов' => 'Forecasting the number of orders'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addPermission([$this->permission => 'Прогнозирование количества заказов']);
        $this->addPermissionToRole($this->permission, \common\models\User::ROLE_SUPERVISOR);
        $this->addPermissionToRole($this->permission, \common\models\User::ROLE_ADMINISTRATOR);
        $this->addPermissionToRole($this->permission, \common\models\User::ROLE_CURATOR);
        $this->addPermissionToRole($this->permission, \common\models\User::ROLE_TEAM_LEAD);

        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->deletePermission($this->permission);
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use console\components\db\Migration;
use backend\modules\widget\models\WidgetType;
use backend\modules\widget\models\WidgetRole;
use common\models\User;

/**
 * Class m170906_085346_add_widget_operators_online
 */
class m170906_085346_add_widget_operators_online extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'operators-online',
            'name' => 'Операторы онлайн',
            'status' => WidgetType::STATUS_ACTIVE,
            'no_cache' => WidgetType::NO_CACHE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'operators-online'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => User::ROLE_CURATOR,
            'type_id' => $type->id
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'operators-online'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => User::ROLE_CURATOR,
            'type_id' => $type->id
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'operators-online']);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m161213_172552_order_form_country_type
 */
class m161213_172552_order_form_country_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_form}}', 'country_id', $this->integer()->notNull());
        $this->addColumn('{{%partner_form}}', 'type', $this->integer()->notNull());
        $this->addForeignKey('fk__partner_form_country__country', '{{%partner_form}}', 'country_id', '{{%country}}', 'id', 'restrict', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk__partner_form_country__country', '{{%partner_form}}');
        $this->dropColumn('{{%partner_form}}', 'country_id');
        $this->dropColumn('{{%partner_form}}', 'type');
    }
}

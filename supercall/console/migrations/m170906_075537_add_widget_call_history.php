<?php
use console\components\db\Migration;
use backend\modules\widget\models\WidgetType;
use backend\modules\widget\models\WidgetRole;
use common\models\User;

/**
 * Class m170906_075537_add_widget_call_history
 */
class m170906_075537_add_widget_call_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(WidgetType::tableName(), [
            'code' => 'main-calls',
            'name' => 'Звонки',
            'status' => WidgetType::STATUS_ACTIVE,
            'no_cache' => WidgetType::NO_CACHE
        ]);

        $type = WidgetType::find()
            ->where(['code' => 'main-calls'])
            ->one();

        $this->insert(WidgetRole::tableName(), [
            'role' => User::ROLE_CURATOR,
            'type_id' => $type->id
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $type = WidgetType::find()
            ->where(['code' => 'main-calls'])
            ->one();

        $this->delete(WidgetRole::tableName(), [
            'role' => User::ROLE_CURATOR,
            'type_id' => $type->id
        ]);

        $this->delete(WidgetType::tableName(), ['code' => 'main-calls']);
    }
}

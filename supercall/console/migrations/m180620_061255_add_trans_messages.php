<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180620_061255_add_trans_messages
 */
class m180620_061255_add_trans_messages extends Migration
{
    public $phrases = [
        'Сообщения' => 'Messages',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

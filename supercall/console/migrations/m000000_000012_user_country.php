<?php
use console\components\db\Migration;

/**
 * Class m000000_000012_user_country
 */
class m000000_000012_user_country extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%user_country}}', [
            'user_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'active' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey(null, '{{%user_country}}', ['user_id', 'country_id']);
        $this->addForeignKey(null, '{{%user_country}}', 'user_id', '{{%user}}', 'id', self::CASCADE, self::RESTRICT);
        $this->addForeignKey(null, '{{%user_country}}', 'country_id', '{{%country}}', 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_country}}');
    }
}

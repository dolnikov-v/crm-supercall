<?php
use common\models\Language;
use console\components\db\Migration;
use backend\modules\wiki\models\WikiArticle;

/**
 * Class m180208_075628_add_column_langiage_id_to_wiki_article
 */
class m180208_075628_add_column_langiage_id_to_wiki_article extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(WikiArticle::tableName(), 'language_id', $this->integer()->defaultValue(WikiArticle::DEFAULT_LANGUAGE_ARTICLE));

        $this->addForeignKey(null, WikiArticle::tableName(), 'language_id', Language::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(WikiArticle::tableName(), 'language_id');
    }
}

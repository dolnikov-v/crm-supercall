<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180117_035510_add_translate_to_address_verification
 */
class m180117_035510_add_translate_to_address_verification extends Migration
{
    public $phrases = [
        'Контроль качества' => 'Quality control',
        'Подтверждения адреса' => 'Address verification',
        'Подтвердить' => 'Confirm',
        'Данные адреса' => 'Address data',
        'Остаток блокировки, мин.' => 'The rest of the lock, min.',
        'Отсутсвуют данные адреса в customer_components заказа' => 'There are no address data in the customer_components of the order',
        'Атрибуты формы не определены, у заказа не указан form_id' => 'Form attributes are not defined, the order does not have a form_id',
        'Список заказов содержит только заказы в статусе APPROVE, адреса которых, требуют подтверждения.' => 'The order list contains only orders in the status APPROVE, which addresses require of confirmation.',
        'Заказ не найден' => 'Order not found',
        'Ошибка смены значения confirm_address признака заказа #{order_id}' => 'Error changing value of confirm_address of order feature #{order_id}',
        'Форма заказа не определена' => 'Order form is not defined',
        'Отсутствуют данные адреса в customer_components' => 'There is no address data in customer_components',
        'Адрес заказа #{order_id} подтверждён и добавлен в КЛАДР' => 'Order address #{order_id} is confirmed and added to KLADER',
        'Вы действительно хотите подтвердить адрес данного заказа ?' => 'Do you want to confirm the address of the order?',
        'Подтверждение' => 'Сonfirmation',
        'Отмена' => 'Cancel',
        'Редактирование адреса заказа' => 'Editing of the order address',
        'Заказ #{order_id} не найден.' => 'Order #{order_id} not found.',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

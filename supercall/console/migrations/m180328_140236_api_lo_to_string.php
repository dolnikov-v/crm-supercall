<?php
use console\components\db\Migration;

/**
 * Class m180328_140236_api_lo_to_string
 */
class m180328_140236_api_lo_to_string extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(\api\models\ApiLog::tableName(), 'url', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170213_081109_order_form_attr_gmap
 */
class m170213_081109_order_form_attr_gmap extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%partner_form_attribute}}', 'gmap_address_component', $this->string());
        $this->createTable('{{%gmap_key}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'key' => $this->string()->notNull(),
            'active' => $this->boolean(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%gmap_key}}');
        $this->dropColumn('{{%partner_form_attribute}}', 'gmap_address_component');
    }
}

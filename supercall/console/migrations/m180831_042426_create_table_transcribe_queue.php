<?php

use common\modules\call\models\CallHistory;
use console\components\db\Migration;

/**
 * Class m180831_042426_create_table_transcribe_queue
 */
class m180831_042426_create_table_transcribe_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%transcribe_queue}}', [
            'id' => $this->primaryKey(),
            'call_history_id' => $this->integer(),
            'media_file_temp' => $this->string(),
            'media_file_s3' => $this->string(),
            'job_name' => $this->string(),
            'job_params' => $this->string(),
            'transcribe_file_s3' => $this->string(),
            'transcribe_json' => "jsonb",
            'status' => $this->string(),
            'error' => $this->text()
        ]);

        $this->addForeignKey(null, '{{%transcribe_queue}}', 'call_history_id', '{{%call_history}}', 'id', self::SET_NULL, self::NO_ACTION);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%transcribe_queue}}');
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m161222_162340_customer_name
 */
class m161222_162340_customer_name extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->delete('{{%order_log}}');
        $this->delete('{{%order}}');
        $this->dropColumn('{{%order}}', 'customer_full_name');
        $this->addColumn('{{%order}}', 'customer_first_name', $this->string(200)->notNull());
        $this->addColumn('{{%order}}', 'customer_last_name', $this->string(200)->notNull());
        $this->addColumn('{{%order}}', 'customer_middle_name', $this->string(200));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('{{%order}}');
        $this->addColumn('{{%order}}', 'customer_full_name', $this->string(200)->notNull());
        $this->dropColumn('{{%order}}', 'customer_first_name');
        $this->dropColumn('{{%order}}', 'customer_last_name');
        $this->dropColumn('{{%order}}', 'customer_middle_name');
    }
}

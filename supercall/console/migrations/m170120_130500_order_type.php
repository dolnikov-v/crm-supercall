<?php
use console\components\db\Migration;

/**
 * Class m170120_130500_order_type
 */
class m170120_130500_order_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%order_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->insert('{{%order_type}}', ['name' => Yii::t('common', 'Заказ')]);
        $this->renameColumn('{{%order}}', 'type', 'type_id');
        $this->execute('alter table {{%order}} alter column type_id drop not null');
        $this->addForeignKey('fk__order__order_type', '{{%order}}', 'type_id', '{{%order_type}}', 'id', 'set null', 'cascade');

        $this->addColumn('{{%timezone}}', 'genesys_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%timezone}}', 'genesys_id');

        $this->dropForeignKey('fk__order__order_type', '{{%order}}');
        $this->execute('alter table {{%order}} alter column type_id set not null');
        $this->renameColumn('{{%order}}', 'type_id', 'type');
        $this->dropTable('{{%order_type}}');
    }
}

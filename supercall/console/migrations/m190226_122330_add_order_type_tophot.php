<?php

/**
* Создание роли (с проверкой на наличие)
* $role = [name => description]
* $this->addRole(array $role)
*
* Удаление роли
* $this-deleteRole(string $role)
*
* Проверка наличия роли
* $this->existsRole(string $role_name);
*
* Создание пермишена (с проверкой на наличие)
* $permission = [name => description]
* $this->addPermission(array $permission)
*
* Удаление пермишена (автоматом удаляются все связки по ролям)
* $this->deletePermission(string $permission);
*
* Проверка наличия пермишена
* $this->>existsPermission(string $permission_name)
*
* Добавление пермишена к роли
* $this->addPermissionToRole(string $permission, string $role)
*
* Удаления пермишена у роли
* $this->deletePermissionByRole(string $permission, string $role)
*
* Добавление перевода (удаление перед добавление уже внутри метода)
* i18n_source_message::addTranslate($this->phrases, true);
*
* Удаление переводов
* i18n_source_message::removeTranslate($this->phrases);
*/

use common\modules\order\models\OrderType;
use console\components\db\Migration;

/**
 * Class m190226_122330_add_order_type_tophot
 */
class m190226_122330_add_order_type_tophot extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert(OrderType::tableName(), ['id' => OrderType::TYPE_TOPHOT, 'name' => OrderType::TYPE_CC_TOPHOT, 'created_at' => time(), 'updated_at' => time()]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $queue_type = OrderType::findOne(['name' =>  OrderType::TYPE_CC_TOPHOT]);

        if($queue_type){
            $queue_type->delete();
        }
    }
}

<?php
use console\components\db\Migration;
use \common\modules\order\models\Order;

/**
 * Class m170711_062743_alter_table_order_first_name_change_IS_NULL
 */
class m170711_062743_alter_table_order_first_name_change_IS_NULL extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(Order::tableName(), 'customer_first_name', 'DROP NOT NULL');
        $this->alterColumn(Order::tableName(), 'customer_first_name', 'SET DEFAULT NULL');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}

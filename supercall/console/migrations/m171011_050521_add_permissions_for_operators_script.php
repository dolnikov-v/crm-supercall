<?php
use console\components\db\Migration;
use common\models\User;
/**
 * Class m171011_050521_add_permissions_for_operators_script
 */
class m171011_050521_add_permissions_for_operators_script extends Migration
{
    public $permissions = [
        'catalog.scriptsoperators.edit',
        'catalog.scriptsoperators.delete',
        'catalog.scriptsoperators.activate',
        'catalog.scriptsoperators.deactivate',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach($this->permissions as $permission) {
            $this->insert('{{%auth_item}}', array(
                'name' => $permission,
                'type' => '2',
                'description' => $permission,
                'created_at' => time(),
                'updated_at' => time()
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_SUPERVISOR,
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_CURATOR,
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_CONTROLLER,
                'child' => $permission
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach($this->permissions as $permission){
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_SUPERVISOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_CURATOR]);
            $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => User::ROLE_CONTROLLER]);

            $this->delete('{{%auth_item}}', ['name' => $permission]);
        }
    }
}

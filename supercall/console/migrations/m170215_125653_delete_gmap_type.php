<?php
use console\components\db\Migration;

/**
 * Class m170215_125653_delete_gmap_type
 */
class m170215_125653_delete_gmap_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update('{{%partner_form_attribute}}', ['type' => 'string'], ['type' => 'gmap_address']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}

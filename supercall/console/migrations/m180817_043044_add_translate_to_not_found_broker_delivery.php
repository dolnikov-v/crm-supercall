<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180817_043044_add_translate_to_not_found_broker_delivery
 */
class m180817_043044_add_translate_to_not_found_broker_delivery extends Migration
{
    public $phrases = [
        'Ни одна из курьерских служб не подходит для данного зип кода' => 'None of the courier services is suitable for this zip code',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

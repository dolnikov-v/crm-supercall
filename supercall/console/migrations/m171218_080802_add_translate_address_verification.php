<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;


/**
 * Class m171218_080802_add_translate_address_verification
 */
class m171218_080802_add_translate_address_verification extends Migration
{
    /**
     * @inheritdoc
     */
    public $phrases = [
        'Проверка адреса' => 'Address verification'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180619_060433_add_translate_audited_orders_filter
 */
class m180619_060433_add_translate_audited_orders_filter extends Migration
{
    public $phrases = [
        'Адрес' => 'Address',
        'Информация о доставке' => 'Shipping Info',
        'Товары' => 'Goods',
        'Статус' => 'Status',
        'Вид изменения' => 'Modification type',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

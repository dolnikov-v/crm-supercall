<?php
use console\components\db\Migration;

/**
 * Class m170502_094246_add_column_ready_to_user
 */
class m170502_094246_add_column_ready_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'is_ready', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'is_ready');
    }
}

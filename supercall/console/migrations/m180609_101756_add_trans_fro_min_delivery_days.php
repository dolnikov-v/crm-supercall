<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180609_101756_add_trans_fro_min_delivery_days
 */
class m180609_101756_add_trans_fro_min_delivery_days extends Migration
{
    public $phrases = [
        'Минимальное количество дней доставки' => 'Min delivery days',
        'Мин. кол-во дней доставки' => 'Min delivery days'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php

use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180413_062824_translate_to_queue_landing
 */
class m180413_062824_translate_to_queue_landing extends Migration
{
    public $phrases = [
        'URL лендинга' => 'Landing URL',
        'Лендинги' => 'Landings',
        'Основные настройки' => 'Basic settings',
        'Лендинги ({count})' => 'Landing ({count})'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

<?php
use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180208_092105_add_permission_for_wiki
 */
class m180208_092105_add_permission_for_wiki extends Migration
{
    public $permissions = [
        'wiki.default.index' => 'Доступ к Wiki разделу',
        'wiki.category.index' => 'Категории / Wiki категории',
        'wiki.article.index' => 'Статьи / Wiki статьи',
        'wiki.category.edit' => 'Статьи / Wiki категории / Редактирование',
        'wiki.category.delete' => 'Статьи / Wiki категории / Удаление',
        'wiki.article.edit' => 'Статьи / Wiki статьи / Редактирование',
        'wiki.article.delete' => 'Статьи / Wiki статьи / Удаление',
    ];

    public $phrases = [
        'Доступ к Wiki разделу' => 'Access to the Wiki section',
        'Категории / Wiki категории' => 'Categories / Wiki category',
        'Статьи / Wiki статьи' => 'Articles / Wiki article',
        'Статьи / Wiki категории / Редактирование' => 'Articles / Wiki category / Editing',
        'Статьи / Wiki категории / Удаление' => 'Articles / Wiki category / Deletion',
        'Статьи / Wiki статьи / Редактирование' => 'Articles / Wiki articles / Editing',
        'Статьи / Wiki статьи / Удаление' => 'Articles / Wiki articles / Removing',
    ];

    public $mortalRoles = [
        User::ROLE_OPERATOR,
        User::ROLE_CONTROLLER,
        User::ROLE_CURATOR,
        User::ROLE_APPLICANT,
        User::ROLE_AUDITOR,
        User::ROLE_SENIOR_OPERATOR,
        User::ROLE_TEAM_LEAD
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $editPermissions = [
            'wiki.category.edit',
            'wiki.article.edit',
        ];

        $viewPermissions = [
            'wiki.default.index',
            'wiki.category.index',
            'wiki.article.index',
        ];

        foreach ($this->permissions as $name => $desc) {
            $this->insert('{{%auth_item}}', array(
                'name' => $name,
                'type' => '2',
                'description' => $desc,
                'created_at' => time(),
                'updated_at' => time()
            ));

            //всемогущий суперадмин
            $this->insert($this->authManager->itemChildTable, array(
                'parent' => User::ROLE_SUPERADMIN,
                'child' => $name
            ));

            //супервизоп - читает и редактирует
            if (in_array($name, $viewPermissions) || in_array($name, $editPermissions)) {
                $this->insert($this->authManager->itemChildTable, array(
                    'parent' => User::ROLE_SUPERVISOR,
                    'child' => $name
                ));
            }

            //остальные только смотрят
            if (in_array($name, $viewPermissions)) {
                foreach ($this->mortalRoles as $role) {
                    $this->insert($this->authManager->itemChildTable, array(
                        'parent' => $role,
                        'child' => $name
                    ));
                }
            }
        }

        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->permissions as $name => $desc) {
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => User::ROLE_SUPERADMIN]);
            $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => User::ROLE_SUPERVISOR]);

            foreach ($this->mortalRoles as $role) {
                $this->delete($this->authManager->itemChildTable, ['child' => $name, 'parent' => $role]);
                $this->delete('{{%auth_item}}', ['name' => $name]);
            }
        }

        i18n_source_message::removeTranslate($this->phrases);
    }
}

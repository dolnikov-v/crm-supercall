<?php

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180507_032018_add_permission_and_translate_to_order_export
 */
class m180507_032018_add_permission_and_translate_to_order_export extends Migration
{
    public $phrases = [
        'Доставка с' => 'Delivery from',
        'Доставка по' => 'Delivery to',
        'Цена начальная' => 'Price initial',
        'Список заказов / Экспорт' => 'Order list / Export',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        $this->insert('{{%auth_item}}', array(
            'name' => 'order.orderexport.getexportcolumns',
            'type' => '2',
            'description' => 'Список заказов / Экспорт',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_CURATOR,
            'child' => 'order.orderexport.getexportcolumns'
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => 'order.orderexport.getexportcolumns'
        ));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->delete($this->authManager->itemChildTable, ['child' => 'order.orderexport.getexportcolumns', 'parent' => User::ROLE_CURATOR]);
        $this->delete($this->authManager->itemChildTable, ['child' => 'order.orderexport.getexportcolumns', 'parent' => User::ROLE_SUPERVISOR]);

        $this->delete('{{%auth_item}}', ['name' => 'order.orderexport.getexportcolumns']);
    }
}

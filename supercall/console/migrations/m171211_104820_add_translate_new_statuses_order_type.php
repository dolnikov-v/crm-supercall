<?php
use console\components\db\Migration;
use backend\models\i18n_source_message;

/**
 * Class m171211_104820_add_translate_new_statuses_order_type
 */
class m171211_104820_add_translate_new_statuses_order_type extends Migration
{
    public $phrases = [
        'Повторный обзвон' => 'Resend to call (new queue)',
        'Очередь 200' => 'Queue 200',
        'Треш' => 'Analyze the trash',
        'Проверка спроса (после задержки)' => 'Still waiting',
        'Перепроверка товаров' => 'Recheck goods',
        'Невыкуп' => 'Send to check in CC (Non-repurchase)',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

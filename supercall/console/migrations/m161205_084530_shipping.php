<?php
use console\components\db\Migration;

/**
 * Class m161205_084530_shipping
 */
class m161205_084530_shipping extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%shipping}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%shipping}}');
    }
}

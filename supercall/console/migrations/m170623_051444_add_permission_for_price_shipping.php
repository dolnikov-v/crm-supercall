<?php
use console\components\db\Migration;
use backend\modules\access\models\AuthItem;

/**
 * Class m170623_051444_add_permission_for_price_shipping
 */
class m170623_051444_add_permission_for_price_shipping extends Migration
{
    /**
     * @var array
     */
    public $permissions = [
        'catalog.priceshipping.index',
        'catalog.priceshipping.edit',
        'catalog.priceshipping.delete'
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        foreach ($this->permissions as $permission) {
            $this->insert('{{%auth_item}}', array(
                'name' => $permission,
                'type' => '2',
                'description' => $permission,
                'created_at' => time(),
                'updated_at' => time(),
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'curator',
                'child' => $permission
            ));

            $this->insert($this->authManager->itemChildTable, array(
                'parent' => 'superadmin',
                'child' => $permission
            ));
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        foreach ($this->permissions as $permission){
            $this->delete($this->authManager->itemChildTable,[
                'parent' => 'curator',
                'child' => $permission
            ]);

            $this->delete($this->authManager->itemChildTable,[
                'parent' => 'superadmin',
                'child' => $permission
            ]);

            $this->delete(AuthItem::tableName(), ['name' => $permission]);

        }
    }
}

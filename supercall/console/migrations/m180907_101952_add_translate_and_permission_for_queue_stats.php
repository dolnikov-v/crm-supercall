<?php

use backend\models\i18n_source_message;
use common\models\User;
use console\components\db\Migration;

/**
 * Class m180907_101952_add_translate_and_permission_for_queue_stats
 */
class m180907_101952_add_translate_and_permission_for_queue_stats extends Migration
{
    public $phrases = [
        'Статистика по очередям' => 'Queue statistics',
        'Дата создания заказа у партнёра' => 'Date of order creation from the partner',
        'Дата создания заказа в КЦ' => 'Date of order creation in Call Center',
        'Таблица статистика' => 'Table statistics',
        'Статистика / Статистика по очередям' => 'Statistics / Statistics by queue'
    ];

    public $permission = 'stats.queue.index';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::removeTranslate($this->phrases);
        i18n_source_message::addTranslate($this->phrases, true);

        $this->insert('{{%auth_item}}', array(
            'name' => $this->permission,
            'type' => '2',
            'description' => 'Статистика / Статистика по очередям',
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_CURATOR,
            'child' => $this->permission
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SALES_DIRECTOR,
            'child' => $this->permission
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_TEAM_LEAD,
            'child' => $this->permission
        ));

        $this->insert($this->authManager->itemChildTable, array(
            'parent' => User::ROLE_SUPERVISOR,
            'child' => $this->permission
        ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);

        $this->delete($this->authManager->itemChildTable, ['child' => $this->permission]);
        $this->delete('{{%auth_item}}', ['name' => $this->permission]);

    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170327_175027_drop_index_from_order
 */
class m170327_175027_drop_index_from_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropIndex('idx_order_foreign_id', '{{%order}}');
        $this->dropIndex('uni_order_partner_idforeign_id', '{{%order}}');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->createIndex('uni_order_partner_idforeign_id', '{{%order}}', ['partner_id', 'foreign_id'], true);
        $this->createIndex('idx_order_foreign_id', '{{%order}}', ['foreign_id'], true);
    }
}

<?php
use console\components\db\Migration;

/**
 * Class m170629_094904_add_order_customer_full_name
 */
class m170629_094904_add_order_customer_full_name extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'customer_full_name', $this->string()->defaultValue(''));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'customer_full_name');
    }
}

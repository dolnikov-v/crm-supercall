<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180320_115518_add_other_translate
 */
class m180320_115518_add_other_translate extends Migration
{
    public $phrases = [
        'Добавить Очередь' => 'Add Queue',
        'Указывать кол-во вручную' => 'Add quantity of attempts manually ',
        'Блокировка между попытками, в мин' => 'Blocking between attempts, in a min',
        'Или' => 'Or',
        'Укажите цифрой' => 'specify a digital',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

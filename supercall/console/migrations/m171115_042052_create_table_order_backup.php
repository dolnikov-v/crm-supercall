<?php
use console\components\db\Migration;
use backend\modules\order\models\OrderBackup;
use common\modules\order\models\Order;

/**
 * Class m171115_042052_create_table_order_backup
 */
class m171115_042052_create_table_order_backup extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(OrderBackup::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'country_id' => $this->integer(),
            'type' => $this->integer(),
            'status' => $this->integer(),
            'sub_status' => $this->integer()->defaultValue(null),
            'partner_id' => $this->integer()->notNull(),
            'foreign_id' => $this->integer()->notNull(),
            'customer_full_name' => $this->string(200),
            'customer_phone' => $this->string(20),
            'customer_mobile' => $this->string(20)->defaultValue(null),
            'customer_address' => $this->text()->defaultValue(null),
            'customer_ip' => $this->string(15)->defaultValue(null),
            'customer_components' => $this->text()->defaultValue(null),
            'comment' => $this->text(),
            'init_price' => $this->double()->defaultValue(0),
            'final_price' => $this->double()->defaultValue(0),
            'shipping_price' => $this->double()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);


        $this->addColumn(OrderBackup::tableName(), 'sent_to_sqs', $this->boolean()->defaultValue(false));
        $this->addColumn(OrderBackup::tableName(), 'delivery_from', $this->integer());
        $this->addColumn(OrderBackup::tableName(), 'delivery_to', $this->integer());
        $this->addColumn(OrderBackup::tableName(), 'latitude', $this->string()->defaultValue(null));
        $this->addColumn(OrderBackup::tableName(), 'longitude', $this->string()->defaultValue(null));
        $this->addColumn(OrderBackup::tableName(), 'event', 'jsonb');
        $this->addColumn(OrderBackup::tableName(), 'package_id', $this->integer()->defaultValue(null));
        $this->addColumn(OrderBackup::tableName(), 'google_zip', $this->string()->defaultValue(null));
        $this->addColumn(OrderBackup::tableName(), 'recall_date', $this->dateTime());
        $this->addColumn(OrderBackup::tableName(), 'order_phones', $this->string());
        $this->addColumn(OrderBackup::tableName(), 'form_id', $this->integer()->null());
        $this->addColumn(OrderBackup::tableName(), 'customer_email', $this->string(200));
        $this->addColumn(OrderBackup::tableName(), 'type_id', $this->integer());

        $this->addForeignKey(null, OrderBackup::tableName(), 'order_id', Order::tableName(), 'id', self::CASCADE, self::RESTRICT);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable(OrderBackup::tableName());
    }
}

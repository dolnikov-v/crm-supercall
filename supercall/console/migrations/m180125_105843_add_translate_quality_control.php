<?php
use backend\models\i18n_source_message;
use console\components\db\Migration;

/**
 * Class m180125_105843_add_translate_quality_control
 */
class m180125_105843_add_translate_quality_control extends Migration
{
    public $phrases = [
        'Изменён' => 'Modified',
        'Штраф' => 'Penalty',
        'Создан' => 'Created by',
        'Обновлен' => 'Updated',
        'Проверено' => 'Verified',
        'Ошибка получения данных' => 'Error retrieving data',
        'Записи не найдены' => 'No records found',
        'Ошибка запроса' => 'The request failed',
        'Оператор не определён' => 'The operator is not determined',
        'Сохранить изменения' => 'Save changes',
        'Получение записей' => 'Getting records',
        'Не определён' => 'Not determined',
        'Некорректный адрес' => 'Invalid address',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        i18n_source_message::addTranslate($this->phrases, true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        i18n_source_message::removeTranslate($this->phrases);
    }
}

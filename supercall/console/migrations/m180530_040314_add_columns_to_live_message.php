<?php

use common\models\LiveMessage;
use console\components\db\Migration;

/**
 * Class m180530_040314_add_columns_to_live_message
 */
class m180530_040314_add_columns_to_live_message extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(LiveMessage::tableName(), 'type', $this->string()->after('user_id'));
        $this->addColumn(LiveMessage::tableName(), 'content', $this->string()->after('type'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(LiveMessage::tableName(), 'type');
        $this->dropColumn(LiveMessage::tableName(), 'content');
    }
}

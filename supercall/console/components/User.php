<?php

namespace console\components;

use common\models\Country;
use common\models\Timezone;

/**
 * @property Timezone $timezone
 */
class User extends \yii\web\User
{
    public function getId()
    {
        return null;
    }

    protected $_timezone;
    public function getTimezone()
    {
        if (!$this->_timezone) {
            $this->_timezone = Timezone::find()->where(['timezone_id' => 'Europe/Moscow'])->one();
        }
        return $this->_timezone;
    }

    protected $_countries;
    public function getCountries()
    {
        if (!$this->_countries) {
            $this->_countries = Country::find()->all();
        }
        return $this->_countries;
    }
}
<?php
namespace console\components\db;

use Yii;
use yii\db\Migration as YiiMigration;
use yii\helpers\Console;

/**
 * Class Migration
 * @package console\components\db
 */
class Migration extends YiiMigration
{
    const CASCADE = 'CASCADE';
    const RESTRICT = 'RESTRICT';
    const SET_NULL = 'SET NULL';
    const NO_ACTION = 'NO ACTION';

    /**
     * @var null|string
     */
    protected $tableOptions = null;

    /**
     * @var \yii\rbac\DbManager
     */
    protected $authManager = null;

    /**
     * @var string
     */
    protected $pkPrefix = 'PK';

    /**
     * @var string
     */
    protected $idxPrefix = 'IDX';

    /**
     * @var string
     */
    protected $uniPrefix = 'UNI';

    /**
     * @var string
     */
    protected $fkPrefix = 'FK';

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->db->enableSchemaCache = false;

        $this->authManager = Yii::$app->getAuthManager();
    }

    /**
     * @param string|null $name
     * @param string $table
     * @param string|array $columns
     */
    public function addPrimaryKey($name, $table, $columns)
    {
        if (is_null($name)) {
            $name = $this->getPkName($table, $columns);
        }

        parent::addPrimaryKey($name, $table, $columns);
    }

    /**
     * @param $table
     * @param $columns
     * @return string
     */
    public function getPkName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);

        return strtolower(sprintf(
            '%s_%s',
            $this->pkPrefix,
            $table . '_' . implode('', (array)$columns)
        ));
    }

    /**
     * @param $columns
     * @return string
     */
    public function includePrimaryKey($columns)
    {
        $columns = is_array($columns) ? implode(',', $columns) : $columns;

        return ('PRIMARY KEY (' . $columns . ')');
    }

    /**
     * @param string|null $name
     * @param string $table
     * @param array|string $columns
     * @param boolean|false $unique
     */
    public function createIndex($name, $table, $columns, $unique = false)
    {
        if (is_null($name)) {
            if ($unique) {
                $name = $this->getUniName($table, $columns);
            } else {
                $name = $this->getIdxName($table, $columns);
            }
        }

        parent::createIndex($name, $table, $columns, $unique);
    }

    /**
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getUniName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);

        return strtolower(sprintf(
            '%s_%s',
            $this->uniPrefix,
            $table . '_' . implode('', (array)$columns)
        ));
    }

    /**
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getIdxName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);

        return strtolower(sprintf(
            '%s_%s',
            $this->idxPrefix,
            $table . '_' . implode('', (array)$columns)
        ));
    }

    /**
     * @param string|null $name
     * @param string $table
     * @param string $columns
     * @param array|string $refTable
     * @param string $refColumns
     * @param null $delete
     * @param null $update
     */
    public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        if (is_null($name)) {
            $name = $this->getFkName($table, $columns);
        }

        parent::addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    /**
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getFkName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);

        return strtolower(substr(sprintf(
            '%s_%s_%s',
            $this->fkPrefix,
            $table,
            $columns
        ), 0, 64));
    }


    /**
     * @param array $role
     * @comment  array [name=>description]
     */
    public function addRole(array $role)
    {
        if(!is_array($role)){
            Console::output(PHP_EOL);
            Console::error("!!! Role not created: {$role}");
            Console::output("!!! Migration not applied");
            Console::output('!!! For adding new role: $this->addRole([name => description])');
            Console::output(PHP_EOL);
            exit;
        }

        $name = array_keys($role);
        $desc = array_values($role);

        if(!$this->existsRole($name[0])){
            $addRole = $this->authManager->createRole($name[0]);
            $addRole->description = $desc[0];
            $this->authManager->add($addRole);

            Console::output("add role: {$name[0]} => {$desc[0]}");
        }else{
            Console::output("exists role: {$name[0]} => {$desc[0]}");
        }
    }

    /**
     * @param string $role
     */
    public function deleteRole(string $role)
    {
        $findRole = $this->authManager->getRole($role);

        if($role){
            $this->authManager->remove($findRole);
            Console::output("Delete role: {$role}");
        }
    }

    /**
     * @param string $role_name
     * @return null|\yii\rbac\Item|\yii\rbac\Role
     */
    public function existsRole(string $role_name)
    {
        return $this->authManager->getRole($role_name);
    }

    /**
     * @param array $permission
     * @comment  array [name=>description]
     */
    public function addPermission(array $permission)
    {
        if(!is_array($permission)){
            Console::output(PHP_EOL);
            Console::error("!!! Permission not created: {$permission}");
            Console::output("!!! Migration not applied");
            Console::output('!!! For adding new permission: $this->addPermission([name => description])');
            Console::output(PHP_EOL);
            exit;
        }

        $name = array_keys($permission);
        $desc = array_values($permission);
        if(!$this->existsPermission($name[0])){
            $this->insert('{{%auth_item}}', [
                'name' => $name[0],
                'type' => 2,
                'description' => $desc[0],
                'created_at' => time(),
                'updated_at' => time()
            ]);

            Console::output("add permission: {$name[0]}");
        }else{
            Console::output("exists permission: {$name[0]}");
        }
    }

    /**
     * @param string|array $permission
     */
    public function deletePermission(string $permission)
    {
        $this->delete('{{%auth_item}}', ['name' => $permission]);
        $this->delete($this->authManager->itemChildTable, ['child' => $permission]);

        Console::output("Delete permission: {$permission}");
    }

    /**
     * @param string $permission_name
     * @return null|\yii\rbac\Item|\yii\rbac\Permission
     */
    public function existsPermission(string $permission_name)
    {
        return $this->authManager->getPermission($permission_name);
    }

    /**
     * @param string $permission
     * @param string $role
     */
    public function addPermissionToRole(string $permission, string $role)
    {
        if(!$this->existsPermission($permission)){
            Console::error(PHP_EOL);
            Console::error("!!! Permission not found: {$permission}");
            Console::output("!!! Migration not applied");
            Console::output('!!! For adding new permission: $this->addPermission([name => description])');
            Console::error(PHP_EOL);
            exit;
        }elseif(!$this->existsRole($role)){
            Console::error(PHP_EOL);
            Console::error("!!! Role not found: {$role}");
            Console::output("!!! Migration not applied");
            Console::output('!!! For adding new role: $this->addRole([name => description])');
            Console::error(PHP_EOL);
            exit;
        }else{
            $rolePermissions = array_keys($this->authManager->getPermissionsByRole($role));

            if(!in_array($permission, $rolePermissions)) {
                $this->insert($this->authManager->itemChildTable, array(
                    'parent' => $role,
                    'child' => $permission
                ));

                Console::output("Add permission: {$permission} to role: {$role}");
            }else{
                Console::output("Exists permission: {$permission} to role: {$role}");
            }
        }
    }

    /**
     * @param string $permission
     * @param string $role
     */
    public function deletePermissionByRole(string $permission, string $role)
    {
        $this->delete($this->authManager->itemChildTable, ['child' => $permission, 'parent' => $role]);
        Console::output("Delete permission: {$permission} by role: {$role}");
    }
}

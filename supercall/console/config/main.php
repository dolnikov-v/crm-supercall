<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'templateFile' => '@console/migrations/views/migration.php',
        ],
        'auto-call' => [
            'class' => 'console\controllers\AutoCallController',
            'amqp' => 'amqp',
            'logstash' => 'logstash'
        ]
    ],
    'components' => [
        'ratchet' => require(yii::getAlias('@common') . '/config/ratchet.php'),
        'user' => [
            'class' => \console\components\User::className(),
            'identityClass' => \common\models\User::className()
        ],
        'logger' => [
            'class' => 'common\components\Logger',
            'table_log_class' => 'common\models\Logger',
            'table_tag_log_class' => 'common\models\LoggerTag'
        ],
        'telegram' => [
            'class' => 'common\components\Telegram',
            'urlApi' => 'https://api.telegram.org/bot',
            'token' => '373344284:AAFE-LtGvtqqCHk5lp70aYXe1rgdW9jSN1A',
            'urlTelegram' => 't.me/Wwcall_bot',
            'bot' => 'Wwcall_bot',
            'name' => '2Wcall'
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/profile.log',
                    'levels' => ['profile'],
                    'categories' => ['application']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/stats.log',
                    'logVars' => [],
                    'categories' => ['stats']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/auto_call.log',
                    'logVars' => [],
                    'categories' => ['auto_call'],
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 10,
                ],
            ],
        ],
        'GoogleSpeech' => [
            'class' => 'backend\components\GoogleSpeech',
            'key' => 'AIzaSyCzOhcnaanNNyd7mh7d1BGe7PSWV3i9beU',
            'api' => 'https://speech.googleapis.com/v1/speech:recognize?fields=results&key=',
            'getCropedByAsteriskUrl' => 'http://mcall-aster2.rtbads.com/transcribe/manage-crop-files.php',
            'deleteCropFromAsteriskUrl' => 'http://mcall-aster2.rtbads.com/transcribe/manage-crop-files.php',
            'path' => '/var/recording/in/',
            'path_asterisk' => 'http://136.243.130.196/acdr/download.php?audio='
        ],
        'Asterisk2' => require(yii::getAlias('@backend') . '/config/asterisk2.php'),
        'Wtrade' => [
            'class' => 'backend\components\Wtrade',
            'webHookPayUrl' => 'http://2wtrade-pay.com/webhook/external-source-role/update'
        ],
        'AwsS3' => [
            'class' => 'backend\components\AwsS3',
            'region' => 'eu-west-1',
            'profile' => 'default',
            'version' => 'latest',
            'credentials' => require(yii::getAlias('@backend') . '/config/aws.php')
        ],
        'AwsTranscribe' => [
            'class' => 'backend\components\AwsTranscribe',
            'region' => 'eu-west-1',
            'version' => 'latest',
            'credentials' => require(yii::getAlias('@backend') . '/config/aws.php'),
        ]
    ],
    'params' => $params,
];

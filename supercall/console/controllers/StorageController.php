<?php
/**
 * Created by PhpStorm.
 * User: PCWORK
 * Date: 27.07.2017
 * Time: 12:56
 */

namespace console\controllers;

use api\models\ApiLog;
use common\models\Country;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerProduct;
use yii\console\Controller;
use yii\httpclient\Client;
use yii\helpers\ArrayHelper;
use yii;

/**
 * Class Storage
 * @package console\controllers
 */
class StorageController extends Controller
{
    public $log;

    public function actionGetProducts2wtrade()
    {
        $char_codes = $this->getCharCodes();
        $partner = Partner::find()->where(['name' => Partner::PARTNER_2WTRADE])->one();

        foreach ($char_codes as $char_code) {

            //for partner 2wtrade-pay
            if ($partner) {
                $url = yii::$app->request->params[0];
                $auth = ['Authorization' => 'Bearer ' . yii::$app->params['payApiToken']];
                $response = $this->getFromApiPay($char_code, $auth);

                if (is_array($response) && isset($response['data'])) {
                    if ($response['data']) {
                        foreach ($response['data'] as $product) {

                            if (isset($product['amount'], $product['id'])) {

                                if (!$this->updateAmount($partner->id, $product['id'], $char_code, $product['amount'])) {

                                    $this->log = new ApiLog();
                                    $this->log->url = $url;
                                    $this->log->auth = $auth['Authorization'];
                                    $this->log->request = json_encode(['product' => $product], JSON_UNESCAPED_UNICODE);
                                    $this->log->response = json_encode(['error' => 'not saved'], JSON_UNESCAPED_UNICODE);
                                    $this->log->save();
                                }
                            }
                        }
                    }
                } else {
                    $this->log = new ApiLog();
                    $this->log->url = $url;
                    $this->log->auth = $auth['Authorization'];
                    $this->log->request = json_encode(['country' => $char_code], JSON_UNESCAPED_UNICODE);
                    $this->log->response = json_encode($response, JSON_UNESCAPED_UNICODE);
                    $this->log->save();
                }
            }
            //end 2wtarde storage products

        }
    }

    /**
     * @return array|object
     */
    public function getCharCodes()
    {
        $char_codes = Country::find()->asArray()->all();

        return ArrayHelper::map($char_codes, 'id', 'char_code', null);
    }

    /**
     * @param $char_code
     * @return bool|yii\httpclient\Response
     */
    public function getFromApiPay($char_code, $auth)
    {
        $client = new Client();

        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl(yii::$app->params['payApiStorageProductsUrl'])
            ->addHeaders($auth)
            ->setData(['country' => $char_code])
            ->send();

        if ($response->isOk) {
            return $response->data;
        } else {
            return $response;
        }
    }

    /**
     * @param integer $partner_id
     * @param integer $partner_product_id
     * @param integer $amount
     * @return bool|PartnerProduct
     */
    public function updateAmount($partner_id, $partner_product_id, $country_char_code, $amount)
    {
        $country = Country::findOne(['char_code' => $country_char_code]);

        $partner_product = PartnerProduct::find()
            ->where(['partner_id' => $partner_id])
            ->andWhere(['partner_product_id' => $partner_product_id])
            ->andWhere(['country_id' => $country->id])
            ->one();


        if (!is_null($partner_product)) {
            $partner_product->amount = (int)$amount;
                        if (!$partner_product->save()) {
                            return false;
                        }

            return $partner_product;
        } else {
            return false;
        }
    }
}
<?php
namespace console\controllers;

use api\models\ApiLog;
use backend\modules\stats\models\olap\LogApproved;
use backend\modules\stats\models\olap\LogGeneral;
use backend\modules\stats\models\olap\LogReject;
use backend\modules\stats\models\olap\LogTrash;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLogs;
use common\modules\partner\helpers\PartnerProduct;
use yii\console\Controller;
use yii\db\Transaction;
use yii\helpers\Json;

/**
 * Class LogStatsController
 * @package console\controllers
 */
class LogStatsController extends Controller
{
    protected $lock_file = '';

    public function init()
    {
        $this->lock_file = \Yii::getAlias('@runtime/stats-olap.lock');;
        parent::init();
    }

    protected function lock()
    {
        if (file_exists($this->lock_file)){
            throw new \Exception('Обнаружен lock-файл');
        }
        touch($this->lock_file);
    }

    protected function unlock()
    {
        unlink($this->lock_file);
    }

    /**
     * расчитывает OLAP куб
     * @return bool
     * @throws \Exception
     */
    public function actionOlap()
    {
        $this->lock();

        $transaction = \Yii::$app->db->beginTransaction();
        \Yii::$app->db->schema->setTransactionIsolationLevel(Transaction::SERIALIZABLE);

        $createLogQuery = ApiLog::find()->innerJoinWith('order')->where(['url' => '/v1/order/create']);
        $changeLogQuery = OrderLogs::find();
        try {
            LogGeneral::deleteAll();
            LogApproved::deleteAll();
            LogReject::deleteAll();
            LogTrash::deleteAll();
            foreach ($createLogQuery->batch(1000) as $logs) {
                echo '.';
                /** @var ApiLog $api_log */
                foreach ($logs as $api_log) {
                    $products = array_map(function ($product) use($api_log) {
                        $product['product_id'] = PartnerProduct::get($api_log->order->partner_id, $product['id']);
                        return $product;
                    }, Json::decode($api_log->request)['products']);

                    $log = [
                        'created_at' => $api_log->created_at,
                        'new' => Order::STATUS_NEW,
                        'fields' => Json::encode(['sub_status' => null]),
                        'product' => Json::encode($products),
                    ];

                    $data = LogGeneral::collectDataFromOrder($api_log->order, $log);
                    foreach ($data as $row) {
                        if (!LogGeneral::incrementWithHierarchy($row)) {
                            throw new \Exception('Не удалось обновить OLAP');
                        }
                    }
                }
            }
            foreach ($changeLogQuery->batch(1000) as $logs) {
                echo '.';
                /** @var OrderLogs $log */
                foreach ($logs as $log) {

                    //general
                    $data = LogGeneral::collectDataFromOrder($log->order, $log);
                    foreach ($data as $row) {
                        if (!LogGeneral::incrementWithHierarchy($row)) {
                            throw new \Exception('Не удалось обновить OLAP');
                        }
                    }

                    //approved
                    if ($log->new == Order::STATUS_APPROVED) {
                        $data = LogApproved::collectDataFromOrder($log->order, $log);
                        foreach ($data as $row) {
                            if (!LogApproved::incrementWithHierarchy($row)) {
                                throw new \Exception('Не удалось обновить OLAP');
                            }
                        }
                    }

                    //reject
                    if ($log->new == Order::STATUS_REJECTED) {
                        $data = LogReject::collectDataFromOrder($log->order, $log);
                        foreach ($data as $row) {
                            if (!LogReject::incrementWithHierarchy($row)) {
                                throw new \Exception('Не удалось обновить OLAP');
                            }
                        }
                    }

                    //trash
                    if ($log->new == Order::STATUS_TRASH) {
                        $data = LogTrash::collectDataFromOrder($log->order, $log);
                        foreach ($data as $row) {
                            if (!LogTrash::incrementWithHierarchy($row)) {
                                throw new \Exception('Не удалось обновить OLAP');
                            }
                        }
                    }

                }
            }
            echo PHP_EOL;
            $transaction->commit();
            $this->unlock();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $this->unlock();
            throw $e;
        }
        return true;
    }
}

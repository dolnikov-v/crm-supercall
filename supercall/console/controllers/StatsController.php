<?php
namespace console\controllers;

use backend\modules\stats\models\olap\OrderApproved;
use backend\modules\stats\models\olap\OrderAttempts;
use backend\modules\stats\models\olap\OrderGeneral;
use backend\modules\stats\models\olap\OrderReject;
use backend\modules\stats\models\olap\OrderTrash;
use common\components\Amqp;
use common\modules\order\models\Order;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;
use yii\console\Controller;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class StatsController
 * @package console\controllers
 */
class StatsController extends Controller
{
    protected $lock_file = '';

    public function init()
    {
        $this->lock_file = \Yii::getAlias('@runtime/stats-olap.lock');;
        parent::init();
    }

    protected function lock()
    {
        if (file_exists($this->lock_file)){
            throw new \Exception('Обнаружен lock-файл');
        }
        touch($this->lock_file);
    }

    protected function unlock()
    {
        unlink($this->lock_file);
    }

    public function actionOlapGeneralAsync()
    {
        $queue = ArrayHelper::getValue(\Yii::$app->params, 'statsRecalcQueueName', 'stats.recalc');
        /** @var Amqp $amqp */
        $amqp = \Yii::$app->amqp;
        $amqp->declareQueue($queue);

        $amqp->getChannel()->basic_consume(
            $queue,
            '', false, false, false, false,
            function (AMQPMessage $msg) use ($amqp) {
                try {
                    $this->actionOlapGeneral();
                } catch (\Exception $e) {
                }
                $amqp->getChannel()->basic_ack($msg->delivery_info['delivery_tag']);

            }
        );

        $end = time() + 300;
        while ($amqp->getChannel()->callbacks && time() < $end) {
            try {
                $amqp->getChannel()->wait(null, false, 5);
            } catch (AMQPTimeoutException $e) {
            }
        }

        $amqp->closeConnection();
    }

    /**
     * расчитывает OLAP куб
     * @return bool
     */
    public function actionOlap()
    {
        $this->lock();

        $transaction = \Yii::$app->db->beginTransaction();
        \Yii::$app->db->schema->setTransactionIsolationLevel(Transaction::SERIALIZABLE);

        $ordersQuery = Order::find();//->where(['>', 'created_at', 1495929600]);
        try {
            OrderGeneral::deleteAll();
            OrderApproved::deleteAll();
            OrderAttempts::deleteAll();
            OrderReject::deleteAll();
            OrderTrash::deleteAll();
            foreach ($ordersQuery->batch(1000) as $orders) {
                echo '.';
                /** @var Order $order */
                foreach ($orders as $order) {
                    //general
                    $log = [
                        'new' => $order->status,
                        'product' => Json::encode(ArrayHelper::toArray($order->orderProducts))
                    ];
                    $data = OrderGeneral::collectDataFromOrder($order, $log);
                    foreach ($data as $row) {
                        if (!OrderGeneral::incrementWithHierarchy($row)) {
                            throw new \Exception('Не удалось обновить OLAP');
                        }
                    }
                    //approved & attempts
                    if ($order->status == Order::STATUS_APPROVED){
                        $log = [
                            'new' => $order->status,
                            'fields' => Json::encode(['sub_status' => $order->sub_status]),
                            'product' => Json::encode(ArrayHelper::toArray($order->orderProducts))
                        ];
                        $data = OrderApproved::collectDataFromOrder($order, $log);
                        foreach ($data as $row) {
                            if (!OrderApproved::incrementWithHierarchy($row)) {
                                throw new \Exception('Не удалось обновить OLAP');
                            }
                        }

                        $log = [
                            'created_at' => time(),
                            'new' => $order->status,
                            'product' => Json::encode(ArrayHelper::toArray($order->orderProducts))
                        ];
                        $data = OrderAttempts::collectDataFromOrder($order, $log);
                        foreach ($data as $row) {
                            if (!OrderAttempts::incrementWithHierarchy($row)) {
                                throw new \Exception('Не удалось обновить OLAP');
                            }
                        }
                    }
                    //reject
                    if ($order->status == Order::STATUS_REJECTED){
                        $log = [
                            'new' => $order->status,
                            'fields' => Json::encode(['sub_status' => $order->sub_status]),
                            'product' => Json::encode(ArrayHelper::toArray($order->orderProducts))
                        ];
                        $data = OrderReject::collectDataFromOrder($order, $log);
                        foreach ($data as $row) {
                            if (!OrderReject::incrementWithHierarchy($row)) {
                                $transaction->rollBack();
                                return false;
                            }
                        }
                    }
                    //trash
                    if ($order->status == Order::STATUS_TRASH){
                        $log = [
                            'new' => $order->status,
                            'fields' => Json::encode(['sub_status' => $order->sub_status]),
                            'product' => Json::encode(ArrayHelper::toArray($order->orderProducts))
                        ];
                        $data = OrderTrash::collectDataFromOrder($order, $log);
                        foreach ($data as $row) {
                            if (!OrderTrash::incrementWithHierarchy($row)) {
                                $transaction->rollBack();
                                return false;
                            }
                        }
                    }
                }
            }
            echo PHP_EOL;
            $transaction->commit();
            $this->unlock();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $this->unlock();
            throw $e;
        }
        return true;
    }
}

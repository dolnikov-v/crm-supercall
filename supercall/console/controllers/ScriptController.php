<?php

namespace console\controllers;

use backend\modules\catalog\components\MaterializedPathBehavior;
use backend\modules\catalog\models\KladerCountries;
use common\models\Country;
use common\models\LeadSQS;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use http\Exception;
use PHPExcel_CachedObjectStorageFactory;
use PHPExcel_IOFactory;
use PHPExcel_Settings;
use Samples\Sample14\ChunkReadFilter;
use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Class ScriptController
 * @package console\controllers
 */
class ScriptController extends Controller
{
    const DEBUG = false;

    public $fileImport;
    public $char_code;
    //кол-во колонок excel файла
    public $size_data;
    //offset колонки в excel файле
    public $offset_excel = 0;
    //1 - это шапка файла
    public $startLine = 2;
    public $root;


    public function setCharCode($charCode)
    {
        $this->char_code = $charCode;
    }

    public function setFileImport($fileImport)
    {
        $this->fileImport = $fileImport;
    }

    public function setSizeData($size)
    {
        $this->size_data = $size;
    }

    public function setOffset($offset)
    {
        $this->offset_excel = $offset;
    }

    public function setStartLine($startLine)
    {
        $this->startLine = $startLine;
    }

    /**
     * @return bool|string
     */
    public function getFileImport()
    {
        $filePath = yii::getAlias('@console-import') . DIRECTORY_SEPARATOR . $this->fileImport;

        if (file_exists($filePath)) {
            return $filePath;
        } else {
            return false;
        }
    }

    /**
     * @return null|Country
     */
    public function getCountry()
    {
        return Country::findOne(['char_code' => $this->char_code]);
    }

    /**
     * @param $country
     * @return null|PartnerForm
     */
    public function getForm($country)
    {
        return PartnerForm::find()->where(['country_id' => $country->id])->orderBy(['id' => SORT_ASC])->one();
    }

    /**
     * @return array|bool
     */
    public function getFormAttributes()
    {
        if (!$country = $this->getCountry()) {
            Console::error('Country for form not found');
            return false;
        }

        if (!$form = $this->getForm($country)) {
            Console::error('Form not found');
            return false;
        }

        if (!$formAttributes = PartnerFormAttribute::find()->where(['form_id' => $form->id, 'is_klader' => true])->orderBy(['sort' => SORT_ASC])->all()) {
            Console::error('Attributes not found');
            return false;
        }

        return ArrayHelper::map($formAttributes, 'id', 'name');
    }

    /**
     * @param $country
     * @return bool
     */
    public function clearKladrByCountry($country)
    {
        /** @var MaterializedPathBehavior $oldRoot */
        $oldRoot = KladerCountries::findOne(['country_id' => $country->id]);

        if ($oldRoot) {
            $oldRoot->deleteWithChildren();
        }

        return true;
    }

    /**
     * @param Country $country
     * @return MaterializedPathBehavior|KladerCountries|bool
     */
    public function createRoot($country)
    {
        $form = $this->getForm($country);

        $model = new KladerCountries();
        $model->name = $country->name;
        $model->path = (string)$country->id;
        $model->partner_form_id = $form->id;
        $model->country_id = $country->id;

        if ($model->validate()) {
            /** @var MaterializedPathBehavior $model */
            $model->makeRoot()->save();

            return $model;
        } else {
            Console::error(Json::encode($model->getErrors()));
            return false;
        }
    }

    /**
     * @param $root
     * @param $form
     * @param $attributes
     * @param $data
     * @return null
     */
    public function putInKladr($root, $form, $attributes, $data)
    {
        $this->root = $root;

        $i = 0;

        foreach ($attributes as $attribute_id => $attr) {
            $num = $i++;


            $name = ($num == 3 && $form->country->char_code == 'PE' && intval($data[$num]) < 100000) ? '0' . trim($data[$num]) : trim($data[$num]);
            $name = trim($name);

            $path = new KladerCountries();
            $path->name = $name;
            $path->partner_form_id = $form->id;
            $path->partner_form_attribute_id = $attribute_id;
            $path->country_id = $form->country_id;

            $exists = KladerCountries::findOne([
                'name' => $name,
                'country_id' => $form->country_id,
                'partner_form_attribute_id' => $attribute_id
            ]);
            echo '-' . $path->name . PHP_EOL;
            if ($path->name == '') {
                echo 'EMPTY NAME. BREAK' . PHP_EOL;
                return null;
            }

            if ($exists) {
                $this->root = $exists;
                echo "FIND  {$path->name}" . $exists->id . ' ' . $name . ' ' . $exists->path . ' i=' . $i . ' form_id=' . $form->id . ' partner_form_attribute_id=' . $attribute_id . PHP_EOL;
            } else {
                if ($path->prependTo($this->root)->save(false)) {
                    echo $path->id . ' | ' . $path->path . ' | ' . $path->name . ' | ' . $path->partner_form_attribute_id . PHP_EOL;
                    echo 'parth by root ' . $this->root->path . PHP_EOL;
                    $this->root = $path;
                } else {
                    echo 'error ' . $name . PHP_EOL;
                }
            }

//            if ($this->root->getChildren()->where(['name' => $name, 'partner_form_attribute_id' => $attribute_id])->exists()) {
//                $parentPath = $this->root->getRoot()->where(['name' => $name, 'partner_form_attribute_id' => $attribute_id])->one();
//                $this->root = $parentPath;
//                //echo "FIND " . $name . ' i=' . $i . ' form_id=' . $form->id . ' partner_form_attribute_id=' . $attribute_id . PHP_EOL;
//                continue;
//            } else {
//
//            }
//
//            if ($path->prependTo($this->root)->save()) {
//                $this->root = $path;
//                //echo $path->id . ' | ' . $path->path . ' | ' . $path->name . ' | ' . $path->partner_form_attribute_id . PHP_EOL;
//            }
        }

        return true;
    }

    /**
     * Импорт в кладр данных из файла excel
     * При каждом запуске автоматически чистит кладр по стране
     * @return bool
     */
    public function actionImportKladr($charCode, $form_id, $fileName, $size)
    {

        $this->setCharCode($charCode);
        $this->setFileImport($fileName);
        $this->setSizeData($size);

        $country = $this->getCountry();
        $attributes = array_slice($this->getFormAttributes(), 0, $this->size_data, true);

        if (count($attributes) < $this->size_data) {
            $this->size_data = count($attributes);
        }

        if (!$country) {
            Console::error('Country not found');
            return false;
        }

        $file = $this->getFileImport();

        if (!$file) {
            Console::error('File does not exist: ' . $this->fileImport);
            return false;
        }

        Console::output('File exists: ' . $file);
        //PHPExcel_Settings::setZipClass(PHPExcel_Settings::ZIPARCHIVE);
        $inputFileType = PHPExcel_IOFactory::identify($file);
        $this->clearKladrByCountry($country);

        if (!$root = $this->createRoot($country)) {
            Console::error('Cant create root');
        } else {
            Console::error(Json::encode($root->getAttributes()));
        }

        $transaction = yii::$app->db->beginTransaction();

        $form = PartnerForm::find()->active()->where(['id' => $form_id])->one();

        if (!$form) {
            Console::error('Form ' . $form_id . ' not found or not active');
            return false;
        }

        try {
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            for ($row = 0; $row <= $highestRow; $row++) {
                if ($row >= $this->startLine) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                        NULL,
                        TRUE,
                        FALSE);

                    $neededData = array_slice($rowData[0], $this->offset_excel, $this->size_data);

                    if (is_null($this->putInKladr($root, $form, $attributes, $neededData))) {
                        break;
                    }

                    if (self::DEBUG) {
                        if ($row >= 10) {
                            exit;
                        }
                    }
                }
            }

            $transaction->commit();

        } catch (Exception $e) {
            $transaction->rollBack();
            die('Error loading file "' . pathinfo($inputFileType, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

    }

    public function actionChangeQueue()
    {
        $sql = <<<SQL
            SELECT order_id, queue_id FROM (
                SELECT o.id AS order_id , o.type_id, o.country_id, last_queue_id, q.id, q.primary_country_id, need_q.id  AS queue_id, need_q.country_id, need_q.type_id
                    FROM "order" o
                LEFT JOIN (
                  SELECT id, country_id, replace(replace(primary_components->>'primary_country', '["', ''), '"]', '') AS primary_country_id FROM queue
                ) q ON q.id = o.last_queue_id
                LEFT JOIN (
                  SELECT id, replace(replace(primary_components->>'primary_country', '["', ''), '"]', '')  AS country_id, primary_components->>'primary_type' AS type_id FROM queue WHERE is_primary = TRUE
                ) need_q ON need_q.country_id = o.country_id::INTEGER AND o.type_id = need_q.type_id::INTEGER
                WHERE o.status = 1 AND  o.country_id = 2
                AND o.country_id <> q.primary_country_id::INTEGER
                AND need_q.id <> last_queue_id
            ) AS t
SQL;

        $data = yii::$app->db->createCommand($sql)->queryAll();

        echo '<pre>' . print_r($data, 1) . '</pre>';
    }


    /**
     *  тестирование апи зорра
     */
    public function actionTestZorra()
    {
        $zorra = yii::$app->get('ZorraPhoneValidator');

        $data = [
            12992 => '79133876745',
            12993 => '9133876745',
            12994 => '133876745'
        ];

        var_dump($zorra->checkPhone($data));
    }

    public function actionClearDoubleOrderLogs()
    {
        $statuses = array_keys(Order::getFinalStatusesCollection());

        $sql = "
            SELECT min(id), count(id), order_id
            FROM order_log 
            WHERE field='status'
            AND new::INTEGER IN(" . implode(', ', $statuses) . ")
            AND (\"order_log\".\"created_at\" BETWEEN '1546300800' AND  " . time() . ")
            --AND (\"order_log\".\"created_at\" BETWEEN '1546300800' AND  '1548028800')
            GROUP BY order_id
            HAVING count(id) > 1
        ";

        $data = yii::$app->db->createCommand($sql)->queryAll();

        $orderIds = ArrayHelper::getColumn($data, 'order_id');
        $orderLogIds = ArrayHelper::getColumn($data, 'min');

        if(!empty($orderIds)) {
            $findSql = "
              SELECT id 
              FROM order_log 
              WHERE order_id IN(" . implode(',', $orderIds) . ") 
              AND id NOT IN (" . implode(',', $orderLogIds) . ") 
              AND field = 'status' 
              AND new::INTEGER IN (" . implode(', ', $statuses) . ")
            ";

            $logs = yii::$app->db->createCommand($findSql)->queryAll();

            $logIds = ArrayHelper::getColumn($logs, 'id');

            if (!empty($logIds)) {
                OrderLog::deleteAll(['id' => $logIds]);
            }
        }
    }

    public function actionTestSendLeadStatus()
    {
        $callHistory = CallHistory::find()->where(['is not', 'recordingfile', null])->limit(20)->all();

        $orderIds = ArrayHelper::getColumn($callHistory, 'order_id');

        $orders = Order::findAll(['id' => $orderIds]);
        
        $leadSqs = new LeadSQS();
        
        foreach($orders as $order){
            $leadSqs->sendLeadStatus($order);
        }
    }
}
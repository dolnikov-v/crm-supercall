<?php

namespace console\controllers;


use common\models\Timezone;
use common\modules\order\models\asterisk\Records;
use Yii;
use yii\helpers\Console;

/**
 * Сохранить запись разговора на сервер
 * запускается по крону, предварительно через каждую минуту
 * @package console\controllers
 */
class RecordController extends \yii\console\Controller
{
    /**
     * Выполняет запись файла на сервера, и при успешной записи, по ссылке удаляет на сервере
     * @throws \ErrorException
     * @param $id - идентификатор записи в табл.`records`
     */
    public function actionLoad($id = null)
    {
        $currentTime = (new \DateTime())
            ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
            ->add(new \DateInterval('PT15M')) // интервал забора записи в 15 минут
            ->getTimestamp();

        $recordQuery = Records::find()
                ->where(['saved' => false]);

        if ($id) {
            $recordQuery->andWhere(['id' => $id]);
        } else {
            $recordQuery->andWhere(['<=', 'datetime', $currentTime]);
        }


        if ($recordQuery->exists()) {
            $count = $recordQuery->count();
            Console::startProgress(0, $count);

            $item = 0;
            foreach ($recordQuery->orderBy(['datetime' => SORT_DESC])->batch(10) as $batch) {
                foreach ($batch as $record) {
                    $item++;
                    // вытащить запись
                    Records::loading($record->path);

                    if ($count > 0) {
                        Console::updateProgress($item, $count);
                    }
                }
            }
            Console::endProgress("records is done" . PHP_EOL);

        }
    }
}
<?php
namespace console\controllers;

use \common\models\User;
use common\modules\call\models\CallHistory;
use function MongoDB\BSON\fromJSON;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\VarDumper;
use yii;
use common\components\base\Module;

/*
* @property bool $reset_pass;
*/

class AsteriskController extends Controller
{
    public $reset_pass = false;
    public $recreate = false;
    public $sip_prefix;

    public function __construct($id, $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->sip_prefix = yii::$app->params['AsteriskSp_prefix'];
    }


    public function options($actionID)
    {
        return array_merge(parent::options($actionID), [
            'reset_pass','recreate'
        ]);
    }
    /**
     * синхронизирует SIP аккаунты операторов с asterisk
     * @reset_pass сбрасывает пароли сувществующим аккам
     */
    public function actionSync()
    {
        $users = User::find()->orderBy(['id'=>SORT_DESC]);
        Console::stdout('Reset pass: '.$this->reset_pass.PHP_EOL);
        Console::stdout('Rcreate SIP: '.$this->recreate.PHP_EOL);
        foreach ($users->batch(10) as $rows) {
            foreach ($rows as $user) {
                $config = $user->sipArray;
                if (empty($config['sip_host'])){
                    $config['sip_host'] = '136.243.130.196';
                }

                if($this->recreate){
                    $config['sip_login'] = '';
                    $config['sip_pass'] = '';
                }
                $pass = $this->getSipPass();
                $login = $this->sip_prefix.$user->id;
                $reset_pass = $this->reset_pass;
                $insert = false;
                $sql = false;

                if (empty($config['sip_login'])){
                    $config['sip_login'] = $login;
                    $insert = true;
                    $reset_pass = true;
                }
                if ($reset_pass || empty($config['sip_pass'])){
                    $config['sip_pass'] = $pass;
                    $reset_pass = 1;
                }

                if ($reset_pass == 1 && !$insert){
                    $sql = "UPDATE sippeers SET secret ='".$config['sip_pass']."' WHERE `name`='".$config['sip_login']."'";
                } elseif($insert){
                    $sql = "INSERT INTO sippeers (name,defaultuser,type,host,secret,context)
                            VALUES ('" . $config['sip_login'] . "', '".$config['sip_login']."','friend','dynamic','" . $config['sip_pass'] . "','internal')";
                }
                $user->sipArray = $config;
                if($sql){
                    if(\Yii::$app->aster->createCommand($sql)->execute()){
                        $user->save();
                        Console::stdout('User '.$config['sip_login'].' is synced. Pass: '.$config['sip_pass'].', login: '.$login.PHP_EOL);
                    }
                }
            }
        }
    }

    /**
     * генерация пароля для SIP аккаунта
     */
    private function getSipPass()
    {
        $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $max = 10;
        $size = strlen($chars) - 1;
        $pass = null;
        while ($max--) {
            $pass .= $chars[rand(0, $size)];
        }
        return $pass;
    }

    public function actionClearEmptyRecords()
    {
        $query = CallHistory::find()
            ->where(['duration' => 0])
            ->andWhere(['=', 'disposition', CallHistory::DISPOSITION_WAITING])
            ->andWhere(['>', 'updated_at', time()-7200]);

        if($query){
            $models = $query->all();
            $ids = ArrayHelper::getColumn($models, 'id');
            Console::output('Count: ' . count($ids));
            CallHistory::updateAll(['disposition' => CallHistory::DISPOSITION_FAIL], ['id' => $ids]);
        }
    }
}
<?php
namespace console\controllers;

use backend\modules\queue\models\Queue;
use backend\modules\queue\models\QueueUser;
use common\components\ChatApiCom;
use common\models\Country;
use common\models\LiveMessage;
use common\models\QueueOrder;
use common\models\Timezone;
use common\models\UserReady;
use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use Yii;
use yii\console\Controller;
use yii\console\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;

/**
 * Class LiveMessengerController
 * @package console\controllers
 */
class LiveMessengerController extends Controller
{
    /**
     * Time to wait for the order sent back in queue
     */
    const WAIT_TIME = 28800;

    public $responseData;

    /***
     * @var Order;
     */
    public $ordersForUpdate;

    /**
     * whatsapp Для возвращения заказов обратно в очередь на обзвон
     * @return bool
     */
    public function actionReturnToQueue()
    {
        $this->stdout('START "RETURN TO QUEUE"', Console::BG_CYAN);
        $this->stdout(PHP_EOL);

        $currentTime = (new \DateTime())
                ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
                ->getTimestamp() - (60 * 60 * 8);

        $this->stdout('CURRENT TIME ' . $currentTime . PHP_EOL);

        //Так как эти заказы могут только измениться когда по ним приходит сообщение и на это сообщение отвечают
        //Берем дату изменения у ордера
        //todo но все равно лучше смотреть по дате последнего сообщения
        $orders = ArrayHelper::getColumn(Order::find()->select('id')
            ->where(['<=', Order::tableName() . '.updated_at', $currentTime])
            ->andWhere(['in', Order::tableName() . '.status', array_keys(Order::getNotFinalStatuses())])
            ->andWhere(['IS ', Order::tableName() . '.last_queue_id', null])
            ->andWhere(['IS NOT', Order::tableName() . '.prevent_queue_id', null])
            ->asArray()->all(), 'id');

        $this->stdout('FIND ORDERS  ' . count($orders) . PHP_EOL);

        if (empty($orders)) {
            $this->stdout('STOP "RETURN TO QUEUE"' . count($orders), Console::BG_YELLOW);
            $this->stdout(PHP_EOL);
            return true;
        }

        $this->ordersForUpdate = Order::find()->where(['id' => $orders])->all();
        $countUpdateOrder = 0;

        foreach ($this->ordersForUpdate as $order) {
            $this->stdout('RETURN BACK ORDER ');
            $this->stdout($order->id, Console::BG_GREEN);
            $this->stdout(' TO QUEUE ');
            $this->stdout($order->prevent_queue_id, Console::BG_CYAN);
            $this->stdout('  START' . ' ORDER UPDATED ' . $countUpdateOrder . "\r");

            $order->last_queue_id = $order->prevent_queue_id;
            $order->save();
            $countUpdateOrder++;
            $this->stdout('RETURN BACK ORDER ');
            $this->stdout($order->id, Console::BG_GREEN);
            $this->stdout(' TO QUEUE ');
            $this->stdout($order->prevent_queue_id, Console::BG_CYAN);
            $this->stdout('  DONE   ' . ' ORDER UPDATED ' . $countUpdateOrder . "\r");
        }

        $this->stdout(PHP_EOL);
        $this->stdout('DONE "RETURN TO QUEUE"', Console::BG_CYAN);
        $this->stdout("\r");
        $this->stdout(PHP_EOL);
    }

    /**
     *  whatsapp Отправка сообщений при инициации чата whatsapp
     */
    public function actionSendMessages()
    {
        $messages = LiveMessage::find()
            ->where(['is_new' => LiveMessage::IS_NEW])
            ->andWhere(['is not', 'user_id', null])
            ->andWhere(['is', 'queue_number', null])
            ->all();

        /** @var ChatApiCom $chatApiCom */
        $chatApiCom = yii::$app->get('ChatApiCom');

        foreach ($messages as $message) {
            $this->stdout('SENT' . $message->id . ' to ' . $message->phone, Console::BG_GREEN);
            $this->stdout(PHP_EOL);

            $content = $chatApiCom->createMessageFromOrder($message->order);
            $responses = $chatApiCom->initChat($content, $message->order);

            foreach ($responses as $k => $response) {
                if (isset($response['result']) && !isset($response['result']['error'])) {
                    $messageByPhone = LiveMessage::find()
                        ->where(['order_id' => $message->order->id])
                        ->andWhere(['is', 'content', null])
                        ->andWhere(['is', 'chat_id', null])
                        ->andWhere(['phone' => $response['phone']])
                        ->orderBy(['id' => SORT_ASC])
                        ->one();

                    if ($messageByPhone) {
                        $this->stdout('MESSAGE' . $messageByPhone->id . ' found', Console::BG_GREEN);
                        $this->stdout(PHP_EOL);

                        $messageByPhone->is_new = LiveMessage::IS_NOT_NEW;
                        $messageByPhone->content = $response['message'];
                        $messageByPhone->queue_number = $response['result']['queueNumber'];
                        $messageByPhone->chat_id = $response['result']['id'];

                        $saved = $messageByPhone->save(false, ['is_new', 'content', 'queue_number', 'chat_id']);

                        $this->stdout('MESSAGE' . $messageByPhone->id . ($saved ? '' : ' not') . ' saved', $saved ? Console::BG_GREEN : Console::BG_RED);
                        $this->stdout(PHP_EOL);
                    } else {
                        $this->stdout('MESSAGE not found for order_id' . $message->order->id, Console::BG_RED);
                        $this->stdout(PHP_EOL);
                    }
                } else {
                    $this->stdout('ERROR ' . $response['result']['error'], Console::BG_RED);
                    $this->stdout(PHP_EOL);
                }
            }
        }
    }

    /**
     * whatsapp получение сообщений whatsapp у chat api com
     */
    public function actionGetMessages()
    {
        $query = Country::find()->where(['is not', 'whatsapp_token', null]);

        /** @var ChatApiCom $chatApiCom */
        $chatApiCom = Yii::$app->get('ChatApiCom');

        $order_id_by_phone = [];

        try {
            foreach ($query->batch(5) as $countries) {
                foreach ($countries as $country) {
                    /** @var Country $country */
                    $response = $chatApiCom->getMessages($country->whatsapp_phone, $country->whatsapp_url, $country->whatsapp_token);
                    //для теста, если упала апи
                    //$response = '{"messages":[{"id":"false_79963772721@c.us_3A377F9243370C83E4A5","body":"Оло","fromMe":false,"self":0,"author":"79963772721@c.us","time":1541583144,"chatId":"79963772721@c.us","messageNumber":1,"type":"chat","senderName":"Евгений","chatName":"+7 996 377-27-21"},{"id":"false_79963772721@c.us_3A1E2F7B4235C8FE6921","body":"Приём","fromMe":false,"self":0,"author":"79963772721@c.us","time":1541583147,"chatId":"79963772721@c.us","messageNumber":2,"type":"chat","senderName":"Евгений","chatName":"+7 996 377-27-21"},{"id":"false_79963772721@c.us_3A7B61010393CE555444","body":"Рооллотиито","fromMe":false,"self":0,"author":"79963772721@c.us","time":1541583152,"chatId":"79963772721@c.us","messageNumber":3,"type":"chat","senderName":"Евгений","chatName":"+7 996 377-27-21"},{"id":"false_79137428350@c.us_ACC9DFBA44420EC9C929F7EA2DD27446","body":"Хэлоу","fromMe":false,"self":0,"author":"79137428350@c.us","time":1539077537,"chatId":"79137428350@c.us","messageNumber":4,"type":"chat","senderName":"Александр","chatName":"+7 913 742-83-50"},{"id":"true_79137428350@c.us_13A2EB6F9ED4A3EC7C156C56C3217569","body":"Ваш телефон спорта теперь у меня","fromMe":true,"self":0,"author":"79513667788@c.us","time":1539083963,"chatId":"79137428350@c.us","messageNumber":5,"type":"chat","senderName":"Александр","chatName":"+7 913 742-83-50"},{"id":"true_79137428350@c.us_4B985B590A1FC1FC1B93C419EE5E3F78","body":"Рппппп","fromMe":true,"self":0,"author":"79513667788@c.us","time":1541583100,"chatId":"79137428350@c.us","messageNumber":6,"type":"chat","senderName":"Александр","chatName":"+7 913 742-83-50"},{"id":"true_79618709218@c.us_3EB05AB355A8B79E42A9","body":"Hello Alex F!\r\nYour order presently in work, number 252349. The order contains the following products: \r\n\r\nTitan Gel \r\n\r\nCan you please confirm the order details?","fromMe":true,"self":0,"author":"79513667788@c.us","time":1552368904,"chatId":"79618709218@c.us","messageNumber":7,"type":"chat","senderName":"","chatName":"+7 961 870-92-18"},{"id":"false_79618709218@c.us_01AC2A96D14EA5211F402F763F24F9A4","body":"Hello, thank you for you feedback.","fromMe":false,"self":0,"author":"79618709218@c.us","time":1552369014,"chatId":"79618709218@c.us","messageNumber":8,"type":"chat","senderName":"tumanovfedorandreevich","chatName":"+7 961 870-92-18"},{"id":"true_79618709218@c.us_3EB00D197DDD215D5FA5","body":"Hello Alex F!\r\nYour order presently in work, number 252349. The order contains the following products: \r\n\r\nTitan Gel \r\n\r\nCan you please confirm the order details?","fromMe":true,"self":0,"author":"79513667788@c.us","time":1552372540,"chatId":"79618709218@c.us","messageNumber":9,"type":"chat","senderName":"tumanovfedorandreevich","chatName":"+7 961 870-92-18"},{"id":"false_79618709218@c.us_D1BF61C582DB0BAB3507DE684373AFE0","body":"Not bad","fromMe":false,"self":0,"author":"79618709218@c.us","time":1552374911,"chatId":"79618709218@c.us","messageNumber":10,"type":"chat","senderName":"tumanovfedorandreevich","chatName":"+7 961 870-92-18"},{"id":"true_79615238147@c.us_3EB064C797C1A5B3EA79","body":"Chat api system test","fromMe":true,"self":0,"author":"79513667788@c.us","time":1552466420,"chatId":"79615238147@c.us","messageNumber":11,"type":"chat","senderName":"","chatName":"+7 961 523-81-47"},{"id":"false_79618709218@c.us_6D53B1E5A111C73BD2EA5F36D8A934E4","body":"Test message 13.03","fromMe":false,"self":0,"author":"79618709218@c.us","time":1552470677,"chatId":"79618709218@c.us","messageNumber":13,"type":"chat","senderName":"tumanovfedorandreevich","chatName":"+7 961 870-92-18"},{"id":"false_79618709218@c.us_391FA239A64CFF49A63BBCD7412C261E","body":"Another Test message 13.03","fromMe":false,"self":0,"author":"79618709218@c.us","time":1552471183,"chatId":"79618709218@c.us","messageNumber":14,"type":"chat","senderName":"tumanovfedorandreevich","chatName":"+7 961 870-92-18"},{"id":"false_79618709218@c.us_7D2B4EA06554266D1D3F39210BCE3291","body":"Yet anoter Test message 13.03","fromMe":false,"self":0,"author":"79618709218@c.us","time":1552471264,"chatId":"79618709218@c.us","messageNumber":15,"type":"chat","senderName":"tumanovfedorandreevich","chatName":"+7 961 870-92-18"}],"lastMessageNumber":15}';

                    $response = Json::decode($response);

                    if (isset($response['messages'])) {
                        $dataMessages = [];

                        foreach ($response['messages'] as $message) {
                            //var_dump($message);
                            if (!$message['fromMe']) {
                                $phone = strtr($message['author'], [ChatApiCom::END => '']);

                                $exists = LiveMessage::find()->where([
                                    'content' => $message['body'],
                                    'phone' => $phone,
                                    'chat_id' => $message['id'],
                                    'number' => $message['messageNumber']
                                ])->exists();

                                Console::output(Json::encode([
                                    'content' => $message['body'],
                                    'phone' => $phone,
                                    'chat_id' => $message['id'],
                                    'number' => $message['messageNumber']
                                ]));

                                if (!$exists) {
                                    if (!isset($order_id_by_phone[$phone])) {
                                        $lastMessage = LiveMessage::find()->where(['phone' => $phone])->orderBy(['id' => SORT_DESC])->one();

                                        if ($lastMessage) {
                                            $order_id_by_phone[$phone] = $lastMessage->order_id;
                                        }
                                    }

                                    if (isset($order_id_by_phone[$phone])) {
                                        $dataMessages[] = [
                                            'is_new' => true,
                                            'order_id' => $order_id_by_phone[$phone],
                                            'created_at' => time(),
                                            'updated_at' => time(),
                                            'type' => LiveMessage::TYPE_WHATSAPP,
                                            'content' => $message['body'],
                                            'phone' => $phone,
                                            'queue_number' => null,
                                            'chat_id' => $message['id'],
                                            'number' => $message['messageNumber']
                                        ];
                                    }
                                }
                            }
                        }

                        if (!empty($dataMessages)) {
                            Console::output('Messages: ' . Json::encode($dataMessages));

                            /** @var ChatApiCom $chatApiCom */
                            $chatApiCom = yii::$app->get('ChatApiCom');

                            foreach ($dataMessages as $dataMessage) {

                                $result = $chatApiCom->setAnswer($dataMessage);

                                if (!$result['success']) {
                                    $error = $result['message'];

                                    Console::error('Error: ' . $error);

                                } else {
                                    Console::output('CreatedMessage: ' . Json::encode($result['message']));
                                    Console::output('$mappedMessage: ' . Json::encode($dataMessage));

                                    //оператор уже работает с заказом
                                    if ($queue_order = QueueOrder::find()->where(['order_id' => $dataMessage['order_id']])->one()) {
                                        $this->sentOrderToOperator($queue_order->user_id, $dataMessage['order_id']);
                                        $sendToNodeJS = $chatApiCom->sentToNodeJS($chatApiCom->nodeJSserver, $result['message']);

                                        Console::output('1' . Json::encode($sendToNodeJS));

                                    } else {
                                        //поиск оператора
                                        //$operator_id = 26;
                                        $operator_id = $this->getOperatorForOrder($dataMessage['order_id']);

                                        if ($operator_id) {
                                            //отправка оператору заказа
                                            $this->sentOrderToOperator($operator_id, $dataMessage['order_id']);
                                            $sendToNodeJS = $chatApiCom->sentToNodeJS($chatApiCom->nodeJSserver, $result['message']);

                                            Console::output('2' . Json::encode($sendToNodeJS));

                                            $this->setBlockToOrder($operator_id, $dataMessage['order_id']);

                                        } else {
                                            //отправить в очередь на NODE JS
                                            $toQeueuNodeJS = $this->sentOrderToQueueNodeJS($dataMessage['order_id']);

                                            Console::output('4' . Json::encode($toQeueuNodeJS));
                                        }
                                    }

                                }
                            }
                        } else {
                            Console::output('Empty messages');
                        }

                    }
                }
            }
        } catch (Exception $e) {
            Console::error($e->getMessage());
        }
    }


    /**
     * Подобрать оператора по очереди заказа
     * @param $noBusyOperators
     * @return bool|int
     */
    public function getOptimalOperator($noBusyOperators, $order_id)
    {

        $order = Order::findOne(['id' => $order_id]);

        $queue = Queue::findOne(['id' => $order->prevent_queue_id]);

        if ($queue) {
            $accessOperators = ArrayHelper::getColumn(QueueUser::find()->where(['queue_id' => $queue->id])->asArray()->all(), 'user_id');
            $operators = array_intersect($accessOperators, $noBusyOperators);
            $sortOperators = OrderLog::find()->select(['user_id'])->where(['user_id' => $operators])->orderBy(['id' => SORT_DESC])->one();

            if (is_null($sortOperators)) {
                $operatorId = array_shift($operators);
            } else {
                $operatorId = $sortOperators->user_id;
            }

            return $operatorId;
        }

        return false;
    }

    /**
     * Подобрать оператора на заказ
     * @return mixed
     */
    public function getOperatorForOrder($order_id)
    {
        $optimalOperator = false;

        $operators = UserReady::getUsersOnline(['use_messenger' => true]);

        if (!empty($operators)) {
            $optimalOperator = $this->getOptimalOperator($operators, $order_id);
        }

        return $optimalOperator;
    }

    /**
     * @param $message
     */
    protected function addMessage($message)
    {
        if (!isset($this->responseData['data']['messages'])) {
            $this->responseData['data']['messages'] = [];
        }

        $this->responseData['data']['messages'][] = $message;
    }

    /**
     * @return array
     */
    public function mapping()
    {
        return [
            'text' => 'content',
            'order' => 'order_id'
        ];
    }

    /**
     * @param $operator_id
     * @param $order_id
     * @return string
     */
    public function sentOrderToOperator($operator_id, $order_id)
    {
        /** @var ChatApiCom $chatApiCom */
        $chatApiCom = yii::$app->get('ChatApiCom');
        $json_data = json_encode([
            'order_id' => $order_id,
            'operator_id' => $operator_id,
            'roomForOrder' => $operator_id
        ]);

        $ch = curl_init($chatApiCom->openOrder);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data))
        );


        $res = curl_exec($ch) . "\n";
        curl_close($ch);

        return $res;
    }

    /**
     * @param $operator_id
     * @param $order_id
     * @return bool
     */
    public function setBlockToOrder($operator_id, $order_id)
    {
        $result = QueueOrder::setBlockOrder($operator_id, $order_id);

        if (!$result['success']) {
            $logger = yii::$app->get('logger');
            $logger->action = basename(__METHOD__);
            $logger->type = ChatApiCom::LOGGER_TYPE;
            $logger->tags = [
                'order_id' => $order_id,
                'user_id' => $operator_id,
            ];
            $logger->error = json_encode($result['message'], 256);
            $logger->save();

            return false;
        }

        return true;
    }

    /**
     * @param integer $order_id
     * @return string
     */
    public function sentOrderToQueueNodeJS($order_id)
    {
        /** @var ChatApiCom $chatApiCom */
        $chatApiCom = yii::$app->get('ChatApiCom');
        $json_data = json_encode([
            'order_id' => $order_id,
        ]);

        $ch = curl_init($chatApiCom->toQueue);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_data))
        );

        $res = curl_exec($ch) . "\n";
        curl_close($ch);

        return $res;
    }
}
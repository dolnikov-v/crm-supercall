<?php
namespace console\controllers;

use common\models\User;
use common\models\UserReady;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class UserReadyController
 * @package console\controllers
 */
class UserReadyController extends Controller
{
    /**
     * метод найдёт всех операторов, не вышедших из системы, и выставит им is_ready = false
     * операторы не выходят из системы самостоятельно. будем делать это за них.
     */
    public function actionCheckIsReady()
    {
        $nonClosedIndervalsQuery = UserReady::find()
        ->limit(500)
        ->orderBy(['id' => SORT_DESC]);

        /** @var UserReady $userReady */
        foreach($nonClosedIndervalsQuery->batch(100) as $usersReady){
           foreach ($usersReady as $userReady) {
               if ($userReady->user instanceof  User && $userReady->stop_time == null && time() - $userReady->start_time > 60 && $userReady->user->isReady === true) {
                   $userReady->stop_time = $userReady->start_time + 60;
                   $userReady->diff = 60;
                   $userReady->save(false);

                   $userReady->user->is_ready = false;
                   $userReady->user->save(false);

                   Console::output($userReady->user->username . ' is out from ' . \Yii::$app->formatter->asDatetime($userReady->start_time)
                       . 'time is now ' . \Yii::$app->formatter->asDatetime(time()));
               }
           }
        }

        //echo $nonClosedIndervals->createCommand()->getRawSql();
    }
}
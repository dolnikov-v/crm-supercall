<?php
namespace console\controllers;

use common\modules\order\models\genesys\Call;
use common\modules\order\models\Order;
use yii\console\Controller;
use yii\helpers\Console;

class GenesysController extends Controller
{
    public function actionRefresh()
    {
        $call_attributes = array_flip((new Call())->attributes());

        $last_date = gmdate('Y-m-d H:i:s', strtotime('now - 1 day '.date_default_timezone_get()));

        $calls = \Yii::$app->genesys->db2
            ->createCommand('select * from gen_agr.ob_campaign_calls where CALL_START > :last_id')
            ->bindValue('last_id', $last_date)
            ->query();
        while ($call_data = $calls->read()){
            echo $call_data['CALL_START'], ' ', $call_data['CALLID'], ' ';

            $call = Call::find()->select(['CALLID'])->where(['CALLID' => $call_data['CALLID']])->one();
            if (!$call){
                \Yii::$app->db->createCommand()->insert('{{%genesys_call}}', array_intersect_key($call_data, $call_attributes))->execute();
                echo 'new';
            }
            else {
                echo 'skipped';
            }

            echo PHP_EOL;
        }
    }

    /**
     * синхронизирует все заказы с БД генезиса
     * Заказы с нефинальным статусом и не принадлежащие очередям
     * Запрещен update в БД Genesys, только INSERT возможен
     */
    public function actionSync()
    {
        $orders = Order::find()
            // Если нефинальный статус
            ->where(['in', 'status', array_keys(Order::getNotFinalStatuses())])
            // Если не используется очередями rabbit
            ->andWhere(['is not', 'last_queue_id', null]);

        foreach ($orders->batch(10) as $rows) {
            foreach ($rows as $order) {
                try {
                    \Yii::$app->genesys->syncOrder($order);
                    echo 'synced: ' . $order->id . PHP_EOL;
                } catch (\Exception $e) {
                    echo $e;
                }
            }
        }
    }

    /**
     * Синхронизация конкретного заказа по ID
     * @param $id ид заказа для индивидуальной синхронизации
     */
    public function actionSyncById($id)
    {
        $orderQuery = Order::find()->where(['id' => $id]);
        if ($orderQuery->exists()) {
            try {
                \Yii::$app->genesys->syncOrder($orderQuery->one(), $allowedUpdate = true);
                echo 'synced: ' . $id . PHP_EOL;
            } catch (\ErrorException $e) {
                echo $e;
            }
        }
    }

    public function actionUpdate($id = null)
    {
        $orders = Order::find()
            ->where(['status' => array_keys(Order::getNotFinalStatuses()),'last_queue_id' => null])
            ->andFilterWhere(['id' => $id]);
        foreach ($orders->batch(100) as $rows) {
            foreach ($rows as $order) {
                try {
                    $updated = \Yii::$app->genesys->updateOrder($order);
                    if ($updated) {
                        sleep(1);
                    }
                } catch (\Exception $e) {
                    echo 'order #', $order->id, ' ', $e->getMessage(), PHP_EOL;
                    \Yii::error($e);
                }
            }
        }
    }

    /**
     * проверка экспорта заказов в генезис
     * @param bool $sync флаг, экспортировать ненайденые заказы
     * @param int $id ид заказа для индивидуальной проверки
     */
    public function actionCheck($sync = false, $id = null)
    {
        $orders = Order::find()
            ->orderBy(['id' => SORT_ASC])
            ->filterWhere([
                'id' => $id,
                'partner_id' => 2,
                'country_id' => 13,
            ]);

        foreach ($orders->batch(10) as $batch) {
            foreach ($batch as $order) {
                try {
                    $genesys_id = \Yii::$app->genesys->db
                        ->createCommand('select RECORD_ID from CALLINGLIST_FR WHERE PRODUCT_ID=' . $order->id)
                        ->queryScalar();

                    if (!$genesys_id || $id) {
                        echo str_pad($order->id, 10, ' ', STR_PAD_RIGHT), ' ',
                        date('Y-m-d H:i:s', $order->created_at), '  ',
                        str_pad($genesys_id, 10, ' ', STR_PAD_RIGHT), '  ';
                    }

                    if (!$genesys_id && $sync) {
                        \Yii::$app->genesys->syncOrder(Order::findOne($id));
                        $genesys_id = \Yii::$app->genesys->db
                            ->createCommand('select RECORD_ID from CALLINGLIST_FR WHERE PRODUCT_ID=' . $order->id)
                            ->queryScalar();
                        if ($genesys_id) {
                            echo 'synced  ', $genesys_id;
                        } else {
                            echo 'failed';
                        }
                    }

                    echo PHP_EOL;
                } catch (\Exception $e) {
                    echo $e;
                }
            }
        }
    }
    
    /**
     * Функция проходит по финальным заказам, которые не были записаны в табл.GEN_OCS.PREDIAL
     * и повторно записывает данные в эту таблицу
     */
    public function actionCheckPredial()
    {
        $orders = Order::find()
            ->orderBy(['id' => SORT_DESC])
            ->filterWhere(['finished' => false])
            ->andFilterWhere(['in', 'status', array_keys(Order::getFinalStatusesCollection())]);
        
        foreach ($orders->batch(10) as $batch) {
            foreach ($batch as $order) {
                // необходимо убедиться, что в табл. PREDIAL строка записалась
                $orderModel = Order::findOne($order->id);
                if (\Yii::$app->genesys->syncPredial($orderModel)) {
                    // укажем, что сохраняем finished, чтобы избежать запуска события на
                    // проверку данного поля в бд
                    \Yii::$app->db->createCommand()
                        ->update('{{%order}}', ['finished' => true], ['id' => $order->id])
                        ->execute();
                    echo Console::stdout('order='.$order->id.' is done');
                }
                else {
                    echo Console::stdout('order='.$order->id.' dont save finished attr');
                }
            }
        }
    }
}
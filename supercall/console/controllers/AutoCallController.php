<?php

namespace console\controllers;

use backend\modules\queue\models\Queue;
use backend\modules\queue\models\QueueUser;
use common\components\Amqp;
use common\components\Logstash;
use common\helpers\Utils;
use common\models\Asterisk;
use common\components\web\User as UserWeb;
use common\models\AutoCall;
use common\models\AutoCallLog;
use common\models\Country;
use common\models\CountryPhoneCode;
use common\models\QueueOrder;
use common\models\Timezone;
use common\modules\call\models\CallHistory;
use common\modules\order\models\Order;
use console\components\User;
use common\models\User as UserModel;
use yii\console\Controller;
use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;
use yii\httpclient\Client;


/**
 * Формирование списка заказов для отправки на астериск, на автодозвон
 * @package console\controllers
 */
class AutoCallController extends Controller
{
    /**
     * @var Amqp
     */
    public $amqp;

    /**
     * @var Logstash
     */
    public $logstash;

    public function init()
    {
        parent::init();
        $this->amqp = yii\di\Instance::ensure($this->amqp, Amqp::class);
        $this->logstash = yii\di\Instance::ensure($this->logstash, Logstash::class);
    }

    /**
     *  не используется
     *  отправляет данные о доступных операторах автодозвона на сервер астериска
     */
    public function actionSendOperatorsOnline()
    {
        $autoCall = new AutoCall();
        $usersOnline = $autoCall->getUsersOnline();
        $this->sendOpersOnline($usersOnline);
    }

    /**
     * снять операторов с паузы если - они по какой-то причине в пацзе остались висеть
     */
    public function actionUnPauseOperators()
    {
        $autoCall = new AutoCall();
        /** @var \common\models\User [] $usersOnline */
        $usersOnline = $autoCall->getUsersOnline();

        $usersWithoutWork = [];

        foreach ($usersOnline as $user) {
            $queueOrder = QueueOrder::findOne(['user_id' => $user->id]);

            if (!$queueOrder) {
                $sipData = $user->sipArray;
                $usersWithoutWork[] = $sipData['sip_login'];
            }
        }

        if (!empty($usersWithoutWork)) {
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(AutoCall::UNPAUSE_OPERATORS)
                ->setHeaders(['Authorization' => 'Basic ' . base64_encode(UserWeb::ACCESS_TYPE_CC)])
                ->setData([
                    'operators' => $usersWithoutWork
                ])
                ->send();

            //var_dump($response->content);
        }
    }

    /**
     * Метод получает список заказов (подобранных для операторов) и список операторов в нужном приоритете
     * Задача - вычеслить операторов, которые будут работать с заказами из одной очереди и распределить звонки на группы операторов,
     * тем самым - обеспечить конкретное чилсо входящих звонков, а не отдавать входящий звонок всем операторам
     * @param  array $orders
     * @param array $ratedOperators
     * @return array
     */
    public function setlimitOrder($orders, $ratedOperators)
    {
        //система уже подобрала нужное кол-во заказов для оператора согласно настройки страны автокла - по проценту дозвона
        $countOrdersByOperator = $this->getCountOrdersByUser($orders);

        $queueOrders = [];
        $queueUsers = [];

        //разобрать заказы и операторов по очередям
        foreach ($orders as $user_id => $data) {
            foreach ($data as $order) {
                $queueOrders[$order['queue_name']][] = $order;

                if (!isset($queueUsers[$order['user_sip']])) {
                    $queueUsers[$order['user_sip']] = [];
                    $queueUsers[$order['user_sip']][] = $order['queue_name'];
                } else {
                    if (!in_array($order['queue_name'], $queueUsers[$order['user_sip']])) {
                        $queueUsers[$order['user_sip']][] = $order['queue_name'];
                    }
                }
            }
        }

        //сортировка согласно рейтингу операторов
        $ratedQueueUser = $this->resortQueueUsers($ratedOperators, $queueUsers);

        //$ratedOperators - список операторов по рейтингу
        //$queueOrders - список заказов во очередям
        //$queueUsers - список операторов по очередям
        //$countOrdersByOperator - кол-во заказов на конкретного оператора

        $listOrdersForOperators = [];
        //лимиты для операторов
        $limitOperatorsCount = [];
        //лимиты для заказов
        $limitOrdersCount = [];

        foreach ($queueOrders as $queueName => $ordersData) {
            foreach ($ordersData as $order) {
                //найти для очереди оператора
                foreach ($ratedQueueUser as $user_sip => $queues) {
                    if (in_array($queueName, $queues)) {
                        $limitOrders = isset($countOrdersByOperator[$user_sip]) ? $countOrdersByOperator[$user_sip] : 0;

                        if ($limitOrders == 0) {
                            continue;
                        } else {
                            //не привышен ли лимит для оператора
                            $limitOperatorsCount[$user_sip] = isset($limitOperatorsCount[$user_sip]) ? $limitOperatorsCount[$user_sip] : 0;
                            if ($limitOperatorsCount[$user_sip] >= $limitOrders) {
                                continue;
                            } else {
                                $limitOperatorsCount[$user_sip]++;
                            }

                            //не привышен ли лимит на заказ
                            $limitOrdersCount[$order['order_id']] = isset($limitOrdersCount[$order['order_id']]) ? $limitOrdersCount[$order['order_id']] : 0;
                            if ($limitOrdersCount[$order['order_id']] >= AutoCall::MAX_COUNT_CALL_OPERATORS) {
                                continue;
                            } else {
                                $limitOrdersCount[$order['order_id']]++;
                            }

                            $order['user_sip'] = $user_sip;
                            $listOrdersForOperators[$order['order_id']][] = [
                                'order_id' => $order['order_id'],
                                'phone' => $order['order_phone'],
                                'country_id' => $order['country_id'],
                                'country_char_code' => $order['country_char_code'],
                                'user_sip' => $user_sip,
                                'queue_name' => $queueName
                            ];
                        }
                    }
                }
            }
        }

        return $listOrdersForOperators;
    }

    /**
     * метод для отладки логики создание связей на asterweb.2wcall.com \app\modules\v1\controllers\AutoCallController::setOperatorsInQueues
     */
    public function actionTest()
    {
        $orders = [
            [
                'order_id' => 173614,
                'order_phone' => 79133869812,
                'queue_name' => 'cc_35_1',
                'country_id' => 3,
                'country_char_code' => 'MX',
                'user_id' => 445,
                'user_sip' => '2wcall_acall1'
            ],
            [
                'order_id' => 173614,
                'order_phone' => 79133869812,
                'queue_name' => 'cc_35_1',
                'country_id' => 3,
                'country_char_code' => 'MX',
                'user_id' => 445,
                'user_sip' => '2wcall_acall2'
            ],
            [
                'order_id' => 173614,
                'order_phone' => 79133869812,
                'queue_name' => 'cc_35_1',
                'country_id' => 3,
                'country_char_code' => 'MX',
                'user_id' => 445,
                'user_sip' => '2wcall_acall3'
            ],
            [
                'order_id' => 173614,
                'order_phone' => 79133869812,
                'queue_name' => 'cc_31_1',
                'country_id' => 3,
                'country_char_code' => 'MX',
                'user_id' => 445,
                'user_sip' => '2wcall_acall1'
            ],
        ];

        $dataFromQmt = [
            [
                'uniqueid' => '12',
                'membername' => '2wcall_acall3',
                'queue_name' => 'cc_35_1',
                'paused' => 0
            ],
            [
                'uniqueid' => '13',
                'membername' => '2wcall_acall3',
                'queue_name' => 'cc_35_2',
                'paused' => 0
            ],
            [
                'uniqueid' => '14',
                'membername' => '2wcall_acall3',
                'queue_name' => 'cc_35_3',
                'paused' => 0
            ],
            [
                'uniqueid' => '15',
                'membername' => '2wcall_acall3',
                'queue_name' => 'cc_30_1',
                'paused' => 0
            ],
            [
                'uniqueid' => '16',
                'membername' => '2wcall_acall3',
                'queue_name' => 'cc_30_2',
                'paused' => 0
            ],
            [
                'uniqueid' => '17',
                'membername' => '2wcall_acall2',
                'queue_name' => 'cc_30_3',
                'paused' => 1
            ],
            [
                'uniqueid' => '18',
                'membername' => '2wcall_acall2',
                'queue_name' => 'cc_35_1',
                'paused' => 1
            ],
            [
                'uniqueid' => '19',
                'membername' => '2wcall_acall2',
                'queue_name' => 'cc_35_2',
                'paused' => 1
            ],
            [
                'uniqueid' => '20',
                'membername' => '2wcall_acall2',
                'queue_name' => 'cc_35_3',
                'paused' => 1
            ],
            [
                'uniqueid' => '21',
                'membername' => '2wcall_acall1',
                'queue_name' => 'cc_35_1',
                'paused' => 0
            ],
            [
                'uniqueid' => '22',
                'membername' => '2wcall_acall1',
                'queue_name' => 'cc_35_2',
                'paused' => 0
            ],
            [
                'uniqueid' => '23',
                'membername' => '2wcall_acall1',
                'queue_name' => 'cc_35_3',
                'paused' => 0
            ],
            [
                'uniqueid' => '24',
                'membername' => '2wcall_acall1',
                'queue_name' => 'cc_30_1',
                'paused' => 0
            ],

        ];

        $qmtList = ArrayHelper::index($dataFromQmt, 'uniqueid');

        $qmtListForDelete = [];
        $statusPauseForUsers = [];

        foreach ($qmtList as $uniqueid => $qmt) {
            $delete = true;

            foreach ($orders as $order) {
                //актуальные связи должны остаться
                if ($order['user_sip'] . ' - ' . $order['queue_name'] == $qmt['membername'] . ' - ' . $qmt['queue_name']) {
                    $delete = false;
                }
            }

            //актуальные связи оператор - очередь
            if ($delete) {
                $qmtListForDelete[] = $uniqueid;
            }
        }

        $qmtListForInsert = [];

        foreach ($orders as $order) {
            $absent = true;

            foreach ($qmtList as $qmt) {

                $statusPauseForUsers[$qmt['membername']] = $qmt['paused'];

                //нам нужны только новые связи
                if ($order['user_sip'] . ' - ' . $order['queue_name'] == $qmt['membername'] . ' - ' . $qmt['queue_name']) {
                    $absent = false;
                }
            }

            if ($absent) {
                $qmtListForInsert[] = [
                    'membername' => $order['user_sip'],
                    'queue_name' => $order['queue_name'],
                    'interface' => 'SIP/' . $order['user_sip'],
                    'paused' => $statusPauseForUsers[$order['user_sip']] ?? 0
                ];
            }
        }

        echo '<pre>' . print_r($qmtListForDelete, 1) . '</pre>';
        echo '<pre>' . print_r($qmtListForInsert, 1) . '</pre>';
    }

    /**
     *  Воркер для генерации звонков автодозвона
     */
    public function actionWorkerGenerate()
    {
        sleep(20);
        $this->actionGenerate();
    }

    /**
     * Создание и отправка номеров телефонов для автодозвона
     * используется либо самостоятельно, либо через воркер actionWorkerGenerate
     * @return bool
     */
    public function actionGenerate()
    {
        try {
            $autoCall = new AutoCall();

            $orders = $autoCall->generateOrders(false);

            /*
            $orders = [
                445 => [
                    [
                        'order_id' => 173614,
                        'order_phone' => 79133869812,
                        'queue_name' => 'cc_35',
                        'country_id' => 3,
                        'country_char_code' => 'MX',
                        'user_id' => 445,
                        'user_sip' => '2wcall_acall1'
                    ],
                ],
                448 => [
                    [
                        'order_id' => 173614,
                        'order_phone' => 79133869812,
                        'queue_name' => 'cc_35',
                        'country_id' => 3,
                        'country_char_code' => 'MX',
                        'user_id' => 445,
                        'user_sip' => '2wcall_acall2'
                    ],
                ],
                449 => [
                    [
                        'order_id' => 173614,
                        'order_phone' => 79133869812,
                        'queue_name' => 'cc_' . rand(30, 35),
                        'country_id' => 3,
                        'country_char_code' => 'MX',
                        'user_id' => 445,
                        'user_sip' => '2wcall_acall3'
                    ],
                ]
            ];
            */

            //для теста - отсортированные операторы по рейтингу
            $ratedOperators = $this->getRatedOperators($orders);

            //если нет операторов - тут нам больше нечего делать
            if (empty($ratedOperators)) {
                return true;
            }

            //заспределить звонки между операторами
            $limitedOrders = $this->setlimitOrder($orders, $ratedOperators);
            //нужен простой массив заказов
            $listOrders = $this->convertOrderListToSendOrderList($limitedOrders);

            if (empty($listOrders)) {
                Console::stdout(Yii::$app->formatter->asDatetime(time()) . ': empty orders' . PHP_EOL);
            } else {
                ob_start();
                echo '<pre>' . print_r($listOrders, 1) . '</pre>' . PHP_EOL;
                $buffer = ob_get_contents();
                ob_end_clean();

                Console::stdout($buffer);
            }

            //отправим данные для подготовки диалпланов, очередей и связей очередь-оператор
            $sentOperatorsInQueues = $this->sentOperatorsInQueues($listOrders);

            $resultSentOperators = Json::decode($sentOperatorsInQueues);

            if (isset($resultSentOperators['success']) && $resultSentOperators['success']) {

                AutoCall::deleteAll(['call_status' => [CallHistory::DISPOSITION_LOST]]);

                $this->stdout('Sent orders');
                //звоним на номера заказов
                $response = $this->sendOrders($listOrders);

                $result = Json::decode($response['message']);

                $this->logstash->saveLog(['sendOrders' => $result]);
            } else {
                $this->logstash->saveLog(['sentOperatorsInQueues' => $resultSentOperators]);
            }

            $this->stdout('End GENERATE orders' . PHP_EOL, Console::BG_BLUE);
        }catch (yii\base\Exception $e){
            $this->logstash->saveLog(['autoCallCatch' => $e->getMessage()]);
        }

        return true;
    }

    /**
     * после распределения заказов на операторов - список выглядит как многомерный асс массив - заказ => операторы (телефон, очередь)
     * для удобства - нам нужен лишь список заказов ввиде простого массива
     * @param $limitedOrders
     * @return array
     */
    public function convertOrderListToSendOrderList($limitedOrders)
    {
        //для каждой очереди свои группы queue_name_1,queue_name_2,queue_name_3,queue_name_4,queue_name_5 и тл
        $groupedQueue = [];

        $listOrders = [];

        $i = 0;

        foreach ($limitedOrders as $order_id => $orders) {

            $group = ++$i;

            foreach ($orders as $callData) {
                if (!in_array($callData['queue_name'], $groupedQueue)) {
                    $groupedQueue[] = $callData['queue_name'];
                    $i = 0;
                    $group = ++$i;
                }

                $callData['original_queue_name'] = $callData['queue_name'];
                $callData['queue_name'] = $callData['queue_name'] . '_' . $group;
                $callData['queue_group'] = $group;
                $listOrders[] = $callData;
            }
        }

        return $listOrders;
    }

    /**
     * операторы по рейтингу
     * @param $orders
     * @return array
     */
    public function getRatedOperators($orders)
    {
        $ratedOperators = [];

        foreach ($orders as $user_id => $listOrders) {
            foreach ($listOrders as $order) {
                $ratedOperators[$order['user_sip']] = $order['user_sip'];
            }
        }

        sort($ratedOperators);

        return $ratedOperators;
    }

    /**
     * метод отсортирует список оператор - очереди согласно рейтингу операторов
     * @param $ratedOperators
     * @param $queueUsers
     * @return array
     */
    public function resortQueueUsers($ratedOperators, $queueUsers)
    {
        $ratedQueueUsers = [];

        foreach ($ratedOperators as $user_sip) {
            if (isset($queueUsers[$user_sip])) {
                $ratedQueueUsers[$user_sip] = $queueUsers[$user_sip];
            }
        }

        return $ratedQueueUsers;
    }

    /**
     * вернёт массив вида user_id => count($orders)
     * @param $orders
     * @return array
     */
    public function getCountOrdersByUser($orders)
    {
        $data = [];

        foreach ($orders as $user_id => $listOrders) {
            foreach ($listOrders as $order) {
                if (!isset($data[$order['user_sip']])) {
                    $data[$order['user_sip']] = 1;
                } else {
                    $data[$order['user_sip']]++;
                }
            }
        }

        return $data;
    }

    /**
     * метод отправит данные о заказах с распределением операторов на звонки
     * @param $orders
     * @return array|yii\httpclient\Response
     */
    public function sentOperatorsInQueues($orders)
    {
        $client = new Client();

        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(AutoCall::URL_SET_OPERATORS_IN_QUEUES)
            ->setHeaders(['Authorization' => 'Basic ' . base64_encode(UserWeb::ACCESS_TYPE_CC)])
            ->setData(['orders' => $orders])
            ->send();

        return $response->content;
    }

    /**
     * @param $orders
     * @return array
     */
    public function sendOrders($orders)
    {
        $result = [
            'success' => true,
            'message' => 'not sent empty orders'
        ];
        $usedOrders = [];

        if (count($orders)) {
            foreach ($orders as $order) {

                if (in_array($order['order_id'], $usedOrders)) {
                    continue;
                } else {
                    $usedOrders[] = $order['order_id'];
                }

                $time = (new \DateTime('now', new \DateTimeZone(Timezone::DEFAULT_TIMEZONE)))->getTimestamp();

                $autoCall = new AutoCall();
                $autoCall->order_id = $order['order_id'];
                $autoCall->order_phone = $order['phone'];
                $autoCall->queue_name = $order['original_queue_name'];
                $autoCall->group_queue = $order['queue_name'];
                $autoCall->country_id = $order['country_id'];
                $autoCall->call_time = $time;
                if ($autoCallSave = $autoCall->save(false)) {
                    //var_dump('$autoCallSave saved');
                    $AutoLog = new AutoCallLog();
                    $AutoLog->id = $autoCall->id;
                    $AutoLog->order_id = $order['order_id'];
                    $AutoLog->order_phone = strval($order['phone']);
                    $AutoLog->queue_name = $order['original_queue_name'];
                    $AutoLog->group_queue = $order['queue_name'];
                    $AutoLog->country_id = $order['country_id'];

                    $autoLogSave = $AutoLog->save(false);

                    //create call history
                    $callHistory = new CallHistory([
                        'order_id' => $autoCall->order_id,
                        'start_time' => time(),
                        'user_id' => Asterisk::getSystemUser(),
                        'start_status' => $autoCall->order->status,
                        'phone' => $autoCall->order_phone,
                        'group_id' => Utils::uid($autoCall->order_id . time() . mt_rand())
                    ]);

                    $callHistorySave = $callHistory->save(false);

                    if ($callHistorySave) {
                        $this->stdout('CH saved: ' . $callHistory->id);
                        $this->stdout(Json::encode($callHistory->getAttributes()));
                    } else {
                        $this->stdout('CH not saved: ');
                        $this->stdout(Json::encode($callHistory->getErrors()));
                    }
                }
            }

            $client = new Client();
            //одной пачкой все заказы
            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(AutoCall::URL_ASTER_AUTO_CALL)
                ->setHeaders(['Authorization' => 'Basic ' . base64_encode(UserWeb::ACCESS_TYPE_CC)])
                ->setData(['orders' => $orders])
                ->send();

            $result = [
                'success' => $response->isOk,
                'message' => $response->content
            ];
        }

        return $result;
    }

    /**
     * @param $opers
     * @return bool|string
     */
    public function sendOpersOnline($opers)
    {
        $sips = [];

        foreach ($opers as $oper) {
            $sipData = Json::decode($oper->sip);

            if (!empty($sipData['sip_login'])) {
                $sips[$oper->id] = $sipData['sip_login'];
            }
        }

        $this->stdout('Send Operatorss: ' . count($sips) . PHP_EOL, Console::BG_GREEN);

        if (count($sips)) {
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(AutoCall::URL_REFRESH_OPERS_ACTIVITY)
                ->setHeaders(['Authorization' => 'Basic ' . base64_encode(\common\components\web\User::ACCESS_TYPE_CC)])
                ->setData(['operators' => $sips])
                ->send();

            return $response->content;
        }

        return false;
    }

    /**
     * проверка номера телефона по id заказа
     * @param integer $id
     */
    public function actionCheckPhone($id)
    {
        $order = Order::findOne($id);

        echo $order->customer_phone . PHP_EOL;
        echo CountryPhoneCode::checkPhone($order->customer_phone, $order->country_id, 1) . PHP_EOL;
        echo PHP_EOL;
    }

    /**
     * Список активных autocall операторов
     */
    public function actionGetOpersOnline()
    {
        $autoCall = new AutoCall();
        $autoCall->getUsersOnline();

        if (empty($autoCall->usersOnline)) {
            $this->stdout('No online operators' . PHP_EOL . "\r" . Console::FG_CYAN);
        }

        $users = UserModel::find()->where(['id' => $autoCall->usersOnline])->all();

        /** @var User $user */
        foreach ($users as $user) {
            $countryName = '';
            $lengthLine = 14;

            if (!empty($user->countries)) {
                foreach ($user->countries as $country) {
                    $countryName .= $country->name . ' ';
                }
            }

            $lastActivity = ceil(((time() - $user->last_activity) / 60));

            if (strlen($countryName) > $lengthLine) {
                $lengthLine = strlen($countryName);
            } else if (strlen($user->username) > $lengthLine) {
                $lengthLine = strlen($user->username);
            }

            $this->stdout(str_repeat('=', $lengthLine) . PHP_EOL);
            $this->stdout($user->username . PHP_EOL, Console::FG_CYAN);

            $this->stdout('Last activity ');
            $this->stdout($lastActivity, $lastActivity < 10 ? Console::FG_GREEN : $lastActivity < 15 ? Console::FG_YELLOW : Console::FG_RED);
            $this->stdout(' minutes' . PHP_EOL);

            $this->stdout('Countries: ' . PHP_EOL);

            $this->stdout($countryName . PHP_EOL, Console::FG_GREEN);
            $this->stdout(str_repeat('=', $lengthLine) . PHP_EOL);
        }

    }

    public function actionUpdateUsersQueue()
    {

        //отключил, т.к. на астере операторы распределяются на подочереди в php yii auto-call/generate
//        $autoCall = new AutoCall();
//        $autoCall->getUsersOnline();
//        $users = $autoCall->usersOnline;
//        if (!empty($users)) {
//            foreach ($users as $userId) {
//                $queue = QueueUser::find()->select('queue_id')->where(['user_id' => $userId])->asArray()->all();
//                $this->stdout('User ID: ' . $userId . PHP_EOL, Console::BG_GREEN);
//
//                if (!empty($queue)) {
//                    $queue = ArrayHelper::getColumn($queue, 'queue_id');
//                    $this->stdout('Queue_User ID: ' . implode(',', $queue) . PHP_EOL, Console::BG_GREEN);
//                    Queue::updateAsteriskMemberQueues($userId, $queue);
//                }
//            }
//        }
    }

    /**
     * что ты такое????
     */
    public function actionAddAllQueue()
    {
        $countries = Country::find()->active()->asArray()->all();

        $this->stdout("ADDING ALL QUEUES\n", Console::FG_GREEN);

        foreach ($countries as $country) {
            $country['id'];
        }
    }

}

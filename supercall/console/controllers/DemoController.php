<?php
namespace console\controllers;

use common\modules\order\models\Order;
use common\modules\order\models\OrderLog;
use yii\console\Controller;

/**
 * Class DemoController
 * @package console\controllers
 */
class DemoController extends Controller
{
    /**
     * генерирует логи
     */
    public function actionLogs()
    {
        foreach (Order::find()->with('logs')->batch(10) as $batch) {
            foreach ($batch as $order) {
                echo $order->id, PHP_EOL;
                $time = $order->created_at;
                $total = 0;
                if (count($order->logs) > 0){
                    $time = $order->logs[0]->created_at;
                    $total = count($order->logs);
                }
                $new_status = $order->status;
                while ($total < 10){
                    echo ' ', $total;
                    $time += rand(1800, 3600*10);
                    $new_status = rand(1,7);
                    $log = new OrderLog();
                    $log->order_id = $order->id;
                    $log->group_id = uniqid();
                    $log->field = 'status';
                    $log->old = (string)$order->status;
                    $log->new = (string)$new_status;
                    $log->user_id = rand(1,2);
                    $log->created_at = $time;
                    $log->updated_at = $time;
                    $log->detachBehavior('timestamp');
                    $log->save(false);
                    $order->status = $new_status;
                    $total++;
                    echo "\033[2K\r";
                }
                $order->updateAttributes(['status' => $new_status]);
            }
        }
    }
}

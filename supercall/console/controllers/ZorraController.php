<?php
/**
 * Created by PhpStorm.
 * User: vasinsky
 * Date: 29.01.19
 * Time: 16:10
 */

namespace console\controllers;

use common\components\ZorraHLR;
use common\models\Country;
use common\models\CountryPhoneCode;
use common\models\LeadSQS;
use common\models\QueueOrder;
use common\modules\order\models\Order;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\Json;

class ZorraController extends Controller
{
    public function actionCheckPhones()
    {
        sleep(2);

        /** @var ZorraHLR $zorra */
        $zorra = yii::$app->get('ZorraPhoneValidator');

        if (!$zorra->global_off) {

            if ($zorra->console) {
                Console::output('global_off: ' . ($zorra->global_off ? 'true' : 'false'));
            }

            $orders = Order::find()
                ->leftJoin(QueueOrder::tableName(), QueueOrder::tableName() . '.order_id=' . Order::tableName() . '.id')
                ->leftJoin(CountryPhoneCode::tableName(), CountryPhoneCode::tableName() . '.country_id=' . Order::tableName() . '.country_id')
                ->where(['status' => Order::STATUS_NEW])
                ->andWhere(['use_zorra_validate' => true])
                ->andWhere(['is not', 'last_queue_id', NULL])
                ->andWhere(['>=', 'created_at', (time() - 3600 * 24)])
                ->andWhere(['is', 'order_id', NULL])
                ->andWhere(['check_zorra' => false])
                ->limit(20);

            if ($zorra->console) {
                Console::output('count orders: ' . $orders->count());
            }
            $countryCodePhones = new CountryPhoneCode();

            if ($orders) {
                foreach ($orders->batch($zorra->batch_size_to_check) as $orders) {
                    $data = [];

                    foreach ($orders as $order) {
                        /** @var Order $order */
                        $data[$order->id] = $countryCodePhones->checkPhone($order->customer_phone, $order->country->id);
                    }

                    $resultCheckZorra = $zorra->checkPhone($data);

                    if ($zorra->console) {
                        Console::output('count answers: ' . count($resultCheckZorra));
                    }

                    if (!empty($resultCheckZorra)) {

                        $orders_ids = array_keys($resultCheckZorra);

                        if ($zorra->console) {
                            Console::output('list orders: ' . implode(', ', $orders_ids));
                        }

                        foreach ($orders_ids as $order_id) {

                            if ($zorra->console) {
                                Console::output('order_id ' . $order_id . ' update check_zorra');
                            }

                            $order = Order::findOne(['id' => $order_id]);

                            if($order){
                                Console::output('Order: ' . $order_id .' country: ' . $order->country->name.' strategy: ' . Json::encode($order->country->strategy));

                                if(isset($order->country->strategy['type'])){

                                    if($order->country->strategy['type'] == Country::STRATEGY_TYPE_STATUS){
                                        Console::output('Change status ' . $order->status .' => ' . $order->country->strategy['value']);

                                        if (!$zorra->test) {
                                            $order->status = $order->country->strategy['value'];
                                        }

                                    }elseif($order->country->strategy['type'] == Country::STRATEGY_TYPE_QUEUE){
                                        Console::output('Change queue ' . $order->last_queue_id .' => ' . $order->country->strategy['value']);

                                        if (!$zorra->test) {
                                            $order->prevent_queue_id = $order->last_queue_id;
                                            $order->last_queue_id = $order->country->strategy['value'];
                                        }
                                    }

                                    $save = $order->save(false);

                                    Console::output('Save: ' . ($save ? 'true' : 'false'));
                                }
                            }
                        }
                    }

                    if (isset($orders_ids)) {
                        $leadSqs = new LeadSQS();

                        //отправить в амазон с новыми статусами
                        $ordersList = Order::findAll(['id' => $orders_ids]);

                        foreach ($ordersList as $updatedOrder) {
                            if ($zorra->console) {
                                Console::output('sent to sqs order_id: ' . $updatedOrder->id . ' status: ' . $updatedOrder->status);
                                Console::output(PHP_EOL);
                            }

                            if (!$zorra->test) {
                                $leadSqs->sendLeadStatus($updatedOrder);
                            }
                        }
                    }
                }
            }
        }
    }
}
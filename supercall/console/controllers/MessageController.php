<?php

namespace console\controllers;

use common\modules\order\models\OrderMessage;
use yii\console\Controller;

/**
 * Отправка недоставленных сообщений
 * @package console\controllers
 */
class MessageController extends Controller
{
    public function actionSend()
    {
        $query = OrderMessage::find()
            ->where([
                'not',
                ['status' => [OrderMessage::STATUS_SENT, OrderMessage::STATUS_SENDING]]
            ]);
        foreach ($query->batch() as $message) {
            $message->send();
        }
    }
}
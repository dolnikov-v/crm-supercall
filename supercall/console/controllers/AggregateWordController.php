<?php

namespace console\controllers;

use backend\modules\audit\models\TranscribeSetting;
use common\models\User;
use common\modules\call\models\CallHistory;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderProductLog;
use Yii;
use yii\console\Controller;
use backend\modules\audit\models\AggregateWord;
use backend\modules\audit\models\AggregateWordBase;
use backend\modules\audit\models\AggregateWordStat;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class AggregateWordController
 * @package console\controllers
 */
class AggregateWordController extends Controller
{
    const DEBUG = false;

    const BATCH_SIZE_AGGREGATE_WORD = 15;
    const BATCH_SIZE_AGGREGATE_WORD_OTHER = 10;
    const BATCH_SIZE_UPDATE_TOTAL = 50;

    /** @var  Query */
    public $query;

    /** @var array */
    public $logData = [];

    /** @var array */
    public $words = [];

    /**
     * Подсчёт слов в справочник aggregate_word_base
     * @param array $wordBaseDataDuplicate
     */
    public function updateWordTotal($wordBaseDataDuplicate)
    {
        Console::output('-- start updateWordTotal() ' . yii::$app->formatter->asTime(time()));

        $updates = [];

        foreach ($wordBaseDataDuplicate as $word => $count) {
            $updates[] = "UPDATE aggregate_word_base SET total = aggregate_word_base.total + " . $count . " WHERE word = '" . $word . "'";
        }

        $update_chunks = array_chunk($updates, self::BATCH_SIZE_UPDATE_TOTAL);

        foreach ($update_chunks as $chunk) {
            yii::$app->db->pdo->exec(implode(";", $chunk));
        }

        if (self::DEBUG) {
            Console::output('update total data: ');
            Console::output(implode(PHP_EOL, $updates));
        }

        Console::output('-- end updateWordTotal() ' . yii::$app->formatter->asTime(time()));
    }

    /**
     * Получить ассоц. массив sip_login => user_id (не используется)
     * @param $call_histories
     * @return array
     */
    public function getUserIdByCallHistory($call_histories)
    {
        Console::output('start getUserIdByCallHistory() ' . yii::$app->formatter->asTime(time()));

        $user_sips = ArrayHelper::getColumn($call_histories, 'user_sip');

        foreach ($user_sips as $k => $sip) {
            if (is_null($sip)) {
                unset($user_sips[$k]);
            }
        }

        Console::output('end getUserIdByCallHistory()');

        if (empty($user_sips)) {
            return [];
        } else {
            return ArrayHelper::map(User::find()
                ->select([
                    'id',
                    '_sip_login' => new Expression("sip::jsonb->>'sip_login'")
                ])
                ->where("sip::jsonb->>'sip_login' in (" . implode(",", array_map(function ($v) {
                        return "'" . $v . "'";
                    }, $user_sips)) . ")")
                ->all(), '_sip_login', 'id');
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsedCallHistoryId()
    {
        return AggregateWord::find();
    }

    /**
     * @param $transcribe
     * @return array
     */
    public function getWordsByTranscribe($transcribe)
    {
        $clear_transcribe = preg_replace("#[^a-z0-9]{1,}#i", " ", $transcribe);
        return explode(" ", $clear_transcribe);
    }

    /**
     * Сбор данных для aggregate_word_stat и aggregate_word
     * @return array
     */
    public function aggregateOtherData()
    {
        Console::output('-- start aggregateOtherData() ' . yii::$app->formatter->asTime(time()));

        $wordAggregateData = [];
        $wordAggregateDuplicate = [];

        $wordAggregateStatData = [];
        $wordAggregateStatDuplicate = [];

        $wordSearch = ArrayHelper::map(AggregateWordBase::findAll(['word' => $this->words]), 'word', 'id');

        foreach ($this->query->batch(self::BATCH_SIZE_AGGREGATE_WORD) as $call_histories) {
            foreach ($call_histories as $call_history) {
                /** @var CallHistory $call_history */
                $order_logs = $call_history->getOrderLogs()->where(['field' => 'status'])->all();
                $order_product_logs = $call_history->getOrderProductLogs()->where(['>', 'price', 0])->all();

                foreach ($order_logs as $order_log) {

                    foreach ($order_product_logs as $order_product_log) {

                        if ($order_log->new || $call_history->end_status) {
                            $words = $this->getWordsByTranscribe($call_history->transcribe);

                            foreach ($words as $word) {
                                $word = mb_strtolower(trim($word));

                                if (in_array($word, array_flip($wordSearch))) {
                                    $word_id = $wordSearch[$word];
                                    $status = !is_null($call_history->end_status) ? $call_history->end_status : $order_log->new;
                                    $product_id = $order_product_log->product_id;
                                    $user_id = $order_log->user_id;

                                    /** @var TranscribeSetting $setting */
                                    if ($setting = $call_history->order->country->transcribeSetting) {
                                        $settingStatuses = ArrayHelper::getColumn($setting->transcribeStatusSets, 'status');
                                    } else {
                                        $settingStatuses = [$status];
                                    }

                                    if (in_array($status, $settingStatuses)) {

                                        $hashAggregate = sha1(implode('-', [
                                            $word_id,
                                            $call_history->id,
                                            $call_history->order_id,
                                            $status,
                                            $call_history->order->country_id,
                                            $user_id,
                                            $product_id,
                                        ]));

                                        $wordAggregateDataArray = [
                                            'word_id' => $word_id,
                                            'call_history_id' => $call_history->id,
                                            'order_id' => $call_history->order_id,
                                            'status' => $status,
                                            'country_id' => $call_history->order->country_id,
                                            'user_id' => $user_id,
                                            'product_id' => $product_id,
                                            'created_at' => time(),
                                            'updated_at' => time(),
                                        ];


                                        if (!isset($wordAggregateData[$hashAggregate])) {
                                            $wordAggregateData[$hashAggregate] = $wordAggregateDataArray;
                                        } else {
                                            unset($wordAggregateDataArray['updated_at']);

                                            if (!isset($wordAggregateDuplicate[$hashAggregate])) {
                                                $wordAggregateDuplicate[$hashAggregate] = array_merge($wordAggregateData[$hashAggregate], ['total' => 1]);
                                            } else {
                                                $wordAggregateDuplicate[$hashAggregate]['total'] += 1;
                                            }
                                        }

                                        $hashAggregateStat = sha1(implode('-', [
                                            $word_id,
                                            $call_history->order->country_id,
                                            $product_id,

                                        ]));


                                        $wordAggregateStatDataArray = [
                                            'word_id' => $word_id,
                                            'country_id' => $call_history->order->country_id,
                                            'product_id' => $product_id,
                                            'created_at' => time(),
                                            'updated_at' => time(),
                                        ];


                                        if (!isset($wordAggregateStatData[$hashAggregateStat])) {
                                            $wordAggregateStatData[$hashAggregateStat] = $wordAggregateStatDataArray;
                                        } else {
                                            unset($wordAggregateStatDataArray['updated_at']);

                                            if (!isset($wordAggregateStatDuplicate[$hashAggregateStat])) {
                                                $wordAggregateStatDuplicate[$hashAggregateStat] = array_merge($wordAggregateStatData[$hashAggregateStat], ['total' => 1]);
                                            } else {
                                                $wordAggregateStatDuplicate[$hashAggregateStat]['total'] += 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //только основной товар
                        break;
                    }
                }
            }
        }

        Console::output('-- end aggregateOtherData() ' . yii::$app->formatter->asTime(time()));

        return [
            'wordAggregateData' => $wordAggregateData,
            'wordAggregateDuplicate' => $wordAggregateDuplicate,
            'wordAggregateStatData' => $wordAggregateStatData,
            'wordAggregateStatDuplicate' => $wordAggregateStatDuplicate,
        ];
    }

    /**
     * Сбор данных для aggregate_word
     * @param array $wordAggregateData
     * @param array $wordAggregateDuplicate
     */
    public function saveAggregateData($wordAggregateData, $wordAggregateDuplicate)
    {
        Console::output('-- start saveAggregateData() ' . yii::$app->formatter->asTime(time()));

        $uniqueue = '(word_id, call_history_id, order_id, status, country_id, user_id, product_id)';

        if (!empty($wordAggregateData)) {
            $sql = yii::$app->db->createCommand()->batchInsert(AggregateWord::tableName(), [
                    'word_id',
                    'call_history_id',
                    'order_id',
                    'status',
                    'country_id',
                    'user_id',
                    'product_id',
                    'created_at',
                    'updated_at'
                ], $wordAggregateData)->getRawSql()
                . " ON CONFLICT " . $uniqueue . " DO UPDATE SET total = aggregate_word.total + 1, updated_at = " . time() . ";";

            yii::$app->db->createCommand($sql)->execute();
        } else {
            Console::output('empty data');
        }

        $inserts = [];

        foreach ($wordAggregateDuplicate as $key => $data) {
            $inserts[] = "INSERT INTO aggregate_word (word_id, call_history_id, order_id, status, country_id, user_id, product_id, total, created_at) 
                              VALUES 
                              (
                                {$data['word_id']}, 
                                {$data['call_history_id']}, 
                                {$data['order_id']}, 
                                {$data['status']}, 
                                {$data['country_id']}, 
                                {$data['user_id']}, 
                                {$data['product_id']}, 
                                {$data['total']}, 
                                {$data['created_at']}
                              )  ON CONFLICT " . $uniqueue . " DO UPDATE SET total = aggregate_word.total + " . $data['total'] . ", updated_at = " . time();

        }

        $insert_chunks = array_chunk($inserts, self::BATCH_SIZE_AGGREGATE_WORD_OTHER);
        $this->saveByChunks($insert_chunks);

        if (self::DEBUG) {
            Console::output("Insered aggregate data:");
            Console::output(implode(PHP_EOL, $inserts));
        }

        Console::output('-- end saveAggregateData() ' . yii::$app->formatter->asTime(time()));
    }

    /**
     * Сбор данных для aggregate_word_stat
     * @param array $wordAggregateStatData
     * @param array $wordAggregateStatDuplicate
     */
    public function saveAggregateStatData($wordAggregateStatData, $wordAggregateStatDuplicate)
    {
        Console::output('-- start saveAggregateStatData() ' . yii::$app->formatter->asTime(time()));

        $uniqueue = '(word_id, country_id, product_id)';

        if (!empty($wordAggregateStatData)) {
            $sql = yii::$app->db->createCommand()->batchInsert(AggregateWordStat::tableName(), [
                    'word_id',
                    'country_id',
                    'product_id',
                    'created_at',
                    'updated_at'
                ], $wordAggregateStatData)->getRawSql()
                . " ON CONFLICT " . $uniqueue . " DO UPDATE SET total = " . AggregateWordStat::tableName() . ".total + " . AggregateWordStat::tableName() . ".total + 1, updated_at = " . time() . ";";

            yii::$app->db->createCommand($sql)->execute();
        } else {
            Console::output('empty data');
        }

        $inserts = [];

        foreach ($wordAggregateStatDuplicate as $key => $data) {

            $inserts[] = "INSERT INTO aggregate_word_stat (word_id, country_id, product_id, total, created_at) 
                              VALUES 
                              (
                                {$data['word_id']}, 
                                {$data['country_id']}, 
                                {$data['product_id']}, 
                                {$data['total']}, 
                                {$data['created_at']}
                              )  ON CONFLICT " . $uniqueue . " DO UPDATE SET total = aggregate_word_stat.total + " . $data['total'] . ", updated_at = " . time();
        }

        $insert_chunks = array_chunk($inserts, self::BATCH_SIZE_AGGREGATE_WORD_OTHER);

        if (self::DEBUG) {
            Console::output("Insered aggregate stat_data:");
            Console::output(implode(PHP_EOL, $inserts));
        }

        $this->saveByChunks($insert_chunks);

        Console::output('-- end saveAggregateStatData() ' . yii::$app->formatter->asTime(time()));
    }

    /**
     * Сохранение aggregate_word_stat и aggregate_word с помощью multi query
     * @param array $chunks
     */
    public function saveByChunks($chunks)
    {
        Console::output('-- -start saveByChunks() ' . yii::$app->formatter->asTime(time()));

        foreach ($chunks as $chunk) {
            $sql = implode(';', $chunk);
            yii::$app->db->pdo->exec($sql);
        }

        Console::output('-- -end saveByChunks() ' . yii::$app->formatter->asTime(time()));
    }

    /**
     * Метод заполнения таблицы aggregate_word, aggregate_word_stat, aggregate_word
     */
    public function actionAggregate()
    {
        Console::output('start actionAggregate() ' . yii::$app->formatter->asTime(time()) . yii::$app->formatter->asDateFullTime(time()));

        $transaction = yii::$app->db->beginTransaction();

        $this->words = [];
        $this->logData = [];

        try {
            $used_call_history_ids = ArrayHelper::getColumn($this->getUsedCallHistoryId()->all(), 'call_history_id');
            $used_call_history_ids = array_unique($used_call_history_ids);

            $this->query = CallHistory::find()
                ->where(['is not', 'transcribe', null])
                ->andWhere(['is not', 'group_id', null])
                ->andWhere(['<>', 'group_id', ''])
                ->andWhere(['not in', 'id', $used_call_history_ids])
                ->orderBy(['id' => SORT_DESC]);

            $wordBaseData = [];
            $wordBaseDataDuplicate = [];

            foreach ($this->query->batch(self::BATCH_SIZE_AGGREGATE_WORD) as $call_histories) {
                foreach ($call_histories as $call_history) {
                    /** @var CallHistory $call_history */
                    $order_logs = $call_history->getOrderLogs()->where(['field' => 'status'])->all();
                    $order_product_logs = $call_history->getOrderProductLogs()->where(['>', 'price', 0])->all();
                    $count_products = count($order_product_logs);

                    foreach ($order_logs as $order_log) {
                        if ($order_log->new) {
                            $status = !is_null($call_history->end_status) ? $call_history->end_status : $order_log->new;

                            /** @var TranscribeSetting $setting */
                            if ($setting = $call_history->order->country->transcribeSetting) {
                                $settingStatuses = ArrayHelper::getColumn($setting->transcribeStatusSets, 'status');
                            } else {
                                $settingStatuses = [$status];
                            }

                            if (in_array($status, $settingStatuses)) {
                                $words = $this->getWordsByTranscribe($call_history->transcribe);

                                foreach ($words as $word) {
                                    $word = mb_strtolower(trim($word));

                                    if (!empty($word)) {
                                        $this->words[] = $word;

                                        $wordBaseData[] = $word;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $wordBaseDataByCount = array_count_values($wordBaseData);

            $wordBaseDataForInsert = [];

            foreach ($wordBaseDataByCount as $word => $count) {
                if (!empty($word)) {
                    if ($count > 1) {
                        $wordBaseDataDuplicate[$word] = $count - 1;
                    }

                    $wordBaseDataForInsert[] = [
                        'word' => $word,
                        'created_at' => time(),
                        'updated_at' => time()
                    ];
                }
            }

            if (!empty($wordBaseDataForInsert)) {
                $sql = yii::$app->db->createCommand()->batchInsert(AggregateWordBase::tableName(), [
                        'word',
                        'created_at',
                        'updated_at'
                    ], $wordBaseDataForInsert)->getRawSql() . ' ON CONFLICT (word) DO UPDATE SET total = '
                    . AggregateWordBase::tableName() . '.total + 1, updated_at = ' . time() . ';';

                yii::$app->db->createCommand($sql)->execute();

                if (self::DEBUG) {
                    Console::output('aggegate_word_base aggregate ' . count($wordBaseDataByCount) . ' records');
                }

                //update aggregate_word_base
                $this->updateWordTotal($wordBaseDataDuplicate);

                $aggregatedOtherData = $this->aggregateOtherData();

                //save in aggregate_date_stat
                $this->saveAggregateStatData($aggregatedOtherData['wordAggregateStatData'], $aggregatedOtherData['wordAggregateStatDuplicate']);

                //save in aggregate_data
                $this->saveAggregateData($aggregatedOtherData['wordAggregateData'], $aggregatedOtherData['wordAggregateDuplicate']);
            }

            if ($transaction) {
                $transaction->commit();
            }

            Console::output('end actionAggregate() ' . yii::$app->formatter->asTime(time()));

        } catch (Exception $e) {
            Console::error($e->getMessage());
            $transaction->rollBack();
        }
    }
}
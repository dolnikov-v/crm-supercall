<?php

namespace console\controllers;

use api\models\ApiLog;
use backend\modules\queue\models\Queue;
use backend\modules\stats\models\olap\LogApproved;
use backend\modules\stats\models\olap\LogGeneral;
use backend\modules\stats\models\olap\LogReject;
use backend\modules\stats\models\olap\LogTrash;
use backend\modules\stats\models\olap\OrderApproved;
use backend\modules\stats\models\olap\OrderAttempts;
use backend\modules\stats\models\olap\OrderGeneral;
use backend\modules\stats\models\olap\OrderReject;
use backend\modules\stats\models\olap\OrderTrash;
use backend\modules\stats\models\report\LogReport;
use backend\modules\stats\models\report\OrderReport;
use common\components\Amqp;
use common\models\LeadSQS;
use common\models\Timezone;
use common\modules\order\models\Order;
use common\modules\order\models\OrderBuyout;
use common\modules\order\models\OrderGenesys;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use function JmesPath\search;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;
use yii\httpclient\Client;
use Yii;

/**
 * Class OrderController
 * @package console\controllers
 */
class OrderController extends Controller
{
    public function actionPhones()
    {
        $query = Order::find()
            ->where(['status' => Order::STATUS_REJECTED, 'sub_status' => Order::SUB_STATUS_REJECT_REASON_AUTO])
            ->orWhere(['status' => Order::STATUS_TRASH]);

        $error_file = \Yii::getAlias('@runtime/phone-reject.csv');
        $correct_file = \Yii::getAlias('@runtime/phone-correct.csv');
        @unlink($error_file);
        @unlink($correct_file);
        $f_error = fopen($error_file, 'w');
        $f_correct = fopen($correct_file, 'w');

        $util = PhoneNumberUtil::getInstance();

        foreach ($query->batch(100) as $orders) {
            /** @var Order $order */
            foreach ($orders as $order) {
                /** @var OrderGenesys $record */
                $record = $order->getGenesysRecords()->where(['index' => 1])->one();
                if (!$record) {
                    continue;
                }

                $region = $order->country->char_code;
                $phone = $util->parse($record->phone, $region);
                if ($util->isValidNumber($phone)) {
                    $order->phone_error = false;
                    $order->customer_phone = ltrim($util->format($phone, PhoneNumberFormat::E164), '+');
                } else {
                    $order->phone_error = true;
                }
                $order->save(false);

                if ($order->phone_error) {
                    fputcsv($f_error, [$order->id, $order->customer_phone, $order->order_phones]);
                } else {
                    fputcsv($f_correct, [$order->id, $order->customer_phone, $order->order_phones]);
                }
            }
        }

        fclose($f_error);
        fclose($f_correct);
    }

    public function actionFix()
    {
        foreach (Order::find()->where(['partner_id' => 2, 'country_id' => 13, 'status' => Order::STATUS_REJECTED])->batch(10) as $batch) {
            /** @var Order $order */
            foreach ($batch as $order) {
                echo $order->id, PHP_EOL;
                /** @var OrderLog[] $logs */
                $logs = $order->getStatusLogs()->where("old = '5' and new = '5'")->all();
                foreach ($logs as $log) {
                    $extra_logs = $order->getLogs()
                        ->where(['group_id' => $log->group_id])
                        ->andWhere(['<>', 'field', 'status'])
                        ->one();
                    if ($extra_logs) {
                        continue;
                    }
                    $log->delete();
                }
            }
        }
    }

    public function actionRecalc()
    {
        foreach (Order::find()->with('orderProducts')->where(['partner_id' => 2, 'country_id' => 13, 'status' => Order::STATUS_APPROVED])->batch(10) as $batch) {
            /** @var Order $order */
            foreach ($batch as $order) {
                foreach ($order->orderProducts as $orderProduct) {
                    $orderProduct->cost = $this->calc_price($orderProduct->quantity, $orderProduct->price);
                    $orderProduct->save(false);
                    if ($orderProduct->cost != $orderProduct->price * $orderProduct->quantity) {
                        echo $order->id, ' ', $orderProduct->id, ' ', $orderProduct->price * $orderProduct->quantity, ' -> ', $orderProduct->cost, PHP_EOL;
                    }
                }
            }
        }
    }

    protected function calc_price($quantity, $price)
    {
        $cost = 0;

        switch (intval($quantity)) {
            case 1:
                $cost = 1 * $price;
                break;
            case 2:
                $cost = 2 * $price;
                break;
            case 3:
                $cost = 2 * $price;
                break;
            case 4:
                $cost = 4 * $price;
                break;
            case 5:
                $cost = 3 * $price;
                break;
            case 6:
                $cost = 6 * $price;
                break;
            case 7:
                $cost = 4 * $price;
                break;
            case 8:
                $cost = 8 * $price;
                break;
            case 9:
                $cost = 5 * $price;
                break;
            case 10:
                $cost = 10 * $price;
                break;
            default:
                $cost = $price * $quantity;
                break;
        }

        return $cost;
    }

    public function checkStats($old = null, $new = null, $return = false)
    {
        $order_totals = [
            'total' => 0,
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
            '5' => 0,
            '6' => 0,
            '7' => 0,
        ];

        $data = \Yii::$app->db
            ->createCommand('select "status", count(*) as "count" from "order" group by "status";')
            ->queryAll();
        foreach ($data as $row) {
            $order_totals['total'] += $row['count'];
            $order_totals[$row['status']] += $row['count'];
        }

        if ($new) {
            $order_totals[$new]--;
            if ($old) {
                $order_totals[$old]++;
            } else {
                $order_totals['total']--;
            }
        }


        $order_stat_totals = [
            'total' => 0,
            '1' => 0,
            '2' => 0,
            '3' => 0,
            '4' => 0,
            '5' => 0,
            '6' => 0,
            '7' => 0,
        ];

        $model = new OrderReport();
        $model->date_start = '01.01.2016 00:00';
        $model->date_end = '01.01.2018 00:00';
        $model->group = OrderReport::GROUP_MONTH;
        $model->type = null;
        $model->product = null;
        $model->partner = null;
        $data = $model->getGeneral()['dataProvider']->allModels;
        foreach ($data as $row) {
            $order_stat_totals['total'] += $row['total'];
            foreach (range(1, 7) as $status) {
                $order_stat_totals[$status] += $row['status_quantity_' . $status];
            }
        }


        $log_stat_totals = [
            'total' => 0,
            '1' => -8,
            '2' => 0,
            '3' => 0,
            '4' => 8,
            '5' => 0,
            '6' => 0,
            '7' => 0,
        ];

        $model = new LogReport();
        $model->date_start = '01.01.2016 00:00';
        $model->date_end = '01.01.2018 00:00';
        $model->group = LogReport::GROUP_MONTH;
        $model->type = null;
        $model->product = null;
        $model->partner = null;
        $data = $model->getGeneral()['dataProvider']->allModels;
        foreach ($data as $row) {
            $log_stat_totals['total'] += $row['total'];
            foreach (range(1, 7) as $status) {
                $log_stat_totals[$status] += $row['status_quantity_' . $status];
            }
        }

        $correct = $order_totals['total'] == $order_stat_totals['total']
            && $order_totals['total'] == $log_stat_totals['total'];
        foreach (range(1, 7) as $status) {
            $correct = $correct && $order_totals[$status] == $order_stat_totals[$status]
                && $order_totals[$status] == $log_stat_totals[$status];
        }

        $result = json_encode($order_totals) . PHP_EOL . json_encode($order_stat_totals) . PHP_EOL . json_encode($log_stat_totals) . PHP_EOL;
        return $return ? $result : $correct;
    }

    public function actionCheck()
    {
        echo $this->checkStats(null, null, true);
        var_dump($this->checkStats());
    }

    public function actionUpdate($discard = '')
    {
//        $end = time() + 290;
//
//        $discard = (boolean)$discard;
//
//        /** @var Amqp $amqp */
//        $amqp = \Yii::$app->amqp;
//        $amqp->declareQueue(ArrayHelper::getValue(\Yii::$app->params, 'orderQueueName', 'order.update'));
//
//        $i = 1;
//        echo $this->checkStats(null, null, true);
//        var_dump($this->checkStats());
//
//        $amqp->getChannel()->basic_consume(
//            ArrayHelper::getValue(\Yii::$app->params, 'orderQueueName', 'order.update'),
//            '', false, false, false, false,
//            function (AMQPMessage $msg) use ($amqp, $discard, $end, &$i) {
//                echo PHP_EOL, 'received ', print_r($msg->body, true), PHP_EOL;
//                echo $i++, PHP_EOL;
//
//                if (file_exists(\Yii::getAlias('@runtime/stats-olap.lock'))) {
//                    echo 'stats calculation in progress, aborting', PHP_EOL;
//                    $amqp->getChannel()->basic_reject($msg->delivery_info['delivery_tag'], true);
//                    exit;
//                }
//
//                if ($discard) {
//                    echo 'discarded', PHP_EOL;
//                    $amqp->getChannel()->basic_ack($msg->delivery_info['delivery_tag']);
//                    return;
//                }
//
//                $transaction = \Yii::$app->db->beginTransaction();
//                try {
//                    /** @var array $msg_data */
//                    $msg_data = Json::decode($msg->body);
//
//                    /** @var Order $order */
//                    $order = Order::findOne($msg_data['id']);
//                    echo $order->id, PHP_EOL;
//                    echo ArrayHelper::getValue($msg_data, 'old.new'), '->', $order->status, PHP_EOL;
//
//                    $correct_before = $this->checkStats(ArrayHelper::getValue($msg_data, 'old.new'), $order->status);
//                    $stats_before = $this->checkStats(ArrayHelper::getValue($msg_data, 'old.new'), $order->status, true);
//                    echo $stats_before;
//                    var_dump($correct_before);
//
//                    $general_data = $this->updateOrderGeneral($order, $msg_data);
//                    $this->updateOrderAttempts($order, $msg_data);
//                    $this->updateOrderApproved($order, $msg_data);
//                    $this->updateOrderReject($order, $msg_data);
//                    $this->updateOrderTrash($order, $msg_data);
//
//                    $correct_after = $this->checkStats();
//                    $stats_after = $this->checkStats(null, null, true);
//                    echo $stats_after;
//                    var_dump($correct_after);
//
////                    if ($correct_before && !$correct_after) {
////                        $file = __DIR__.'/../runtime/dump.psql';
////                        if (!file_exists($file)) {
////                            exec('/usr/pgsql-9.6/bin/pg_dump -U postgres -F c -f dump.psql unicall_db');
////                            file_put_contents(__DIR__.'/../runtime/stat.report', VarDumper::dumpAsString([
////                                'message' => $msg_data,
////                                'before' => $stats_before,
////                                'after' => $stats_after,
////                                'general_data' => $general_data
////                            ]));
////                        }
////                    }
//
//                    $transaction->commit();
//                    $amqp->getChannel()->basic_ack($msg->delivery_info['delivery_tag']);
//                    echo PHP_EOL, PHP_EOL;
//                } catch (\Exception $e) {
//                    $transaction->rollBack();
//                    throw $e;
//                }
//            }
//        );
//
//
//        while ($amqp->getChannel()->callbacks && time() < $end) {
//            try {
//                $amqp->getChannel()->wait(null, false, 5);
//            } catch (AMQPTimeoutException $e) {
//                echo '.';
//            }
//        }
//
//        $amqp->closeConnection();
    }

    protected function updateOrderGeneral(Order $order, array $msg_data)
    {
        $return = [];

        $data = OrderGeneral::collectDataFromOrder($order, $msg_data['new']);
        $return['order'] = $data;
        foreach ($data as $row) {
            if (!OrderGeneral::incrementWithHierarchy($row)) {
                throw new \Exception("Не удалось добавить данные в куб");
            }
        }

        $data = LogGeneral::collectDataFromOrder($order, $msg_data['new']);
        $return['log'] = $data;
        foreach ($data as $row) {
            if (!LogGeneral::incrementWithHierarchy($row)) {
                throw new \Exception("Не удалось добавить данные в куб");
            }
        }

        return $return;
    }

    protected function updateOrderAttempts(Order $order, array $msg_data)
    {
        if ($order->status != Order::STATUS_APPROVED) {
            return;
        }
        $data = OrderAttempts::collectDataFromOrder($order, $msg_data['new']);
        foreach ($data as $row) {
            if (!OrderAttempts::incrementWithHierarchy($row)) {
                throw new \Exception("Не удалось добавить данные в куб");
            }
        }
    }

    protected function updateOrderApproved(Order $order, array $msg_data)
    {
        if ($order->status != Order::STATUS_APPROVED) {
            return;
        }

        $data = OrderApproved::collectDataFromOrder($order, $msg_data['new']);
        foreach ($data as $row) {
            if (!OrderApproved::incrementWithHierarchy($row)) {
                throw new \Exception("Не удалось добавить данные в куб");
            }
        }

        $data = LogApproved::collectDataFromOrder($order, $msg_data['new']);
        foreach ($data as $row) {
            if (!LogApproved::incrementWithHierarchy($row)) {
                throw new \Exception("Не удалось добавить данные в куб");
            }
        }
    }

    protected function updateOrderReject(Order $order, array $msg_data)
    {
        if ($order->status != Order::STATUS_REJECTED) {
            return;
        }

        $data = OrderReject::collectDataFromOrder($order, $msg_data['new']);
        foreach ($data as $row) {
            if (!OrderReject::incrementWithHierarchy($row)) {
                throw new \Exception("Не удалось добавить данные в куб");
            }
        }

        $data = LogReject::collectDataFromOrder($order, $msg_data['new']);
        foreach ($data as $row) {
            if (!LogReject::incrementWithHierarchy($row)) {
                throw new \Exception("Не удалось добавить данные в куб");
            }
        }
    }

    protected function updateOrderTrash(Order $order, array $msg_data)
    {
        if ($order->status != Order::STATUS_TRASH) {
            return;
        }

        $data = OrderTrash::collectDataFromOrder($order, $msg_data['new']);
        foreach ($data as $row) {
            if (!OrderTrash::incrementWithHierarchy($row)) {
                throw new \Exception("Не удалось добавить данные в куб");
            }
        }

        $data = LogTrash::collectDataFromOrder($order, $msg_data['new']);
        foreach ($data as $row) {
            if (!LogTrash::incrementWithHierarchy($row)) {
                throw new \Exception("Не удалось добавить данные в куб");
            }
        }
    }

    /**
     * Функция для всех заказов со статусом = APPROVE запрашивает Трекер доставки со статуом
     *  Статусы (status) доставки:
     *  0 - не  найден
     *  1 - заказ в процессе оформления // продолжаем их дергать
     *  2 - в доставке // продолжаем их дергать
     *  3 - доставлен
     *  4 - недоставлен
     *  5 - отказ,дубль,треш (косяк) // с таким статусом отобразить в админке
     *
     *  Ответ SUCCESS:
     * {"success":1,"data":{"50333":{"partner_id":"50333","id":"1137875","status":"3","tracker":"SWS40554808","service_status":"COD"}}}
     *
     *  Ответ ERROR:
     * {"error":"Empty ids"}
     *
     * Лимит в 1000 заказов
     * Заказы со статусом 1 и 2 (в БД Unicall аттрибут "code") необходимо обновлять
     */
    public function actionFindTrackerByIds()
    {
        // учитывать буфер + 30 минут
        $after_time = (60 + 30) * 60;
        $current_time = time();
        $queryIds = \Yii::$app->db->createCommand('SELECT id FROM "order"
                      WHERE ((updated_at + :after_time) <=:current_time) AND 
                          ((status=:status AND approve_components IS NULL)
                          OR (approve_components @>:approve_1) 
                          OR (approve_components @>:approve_2)
                          OR (approve_components @>:approve_5))
                      ORDER BY id')
            ->bindValues([
                ':status' => Order::STATUS_APPROVED,
                ':approve_1' => '{"code":1}',
                ':approve_2' => '{"code":2}',
                ':approve_5' => '{"code":5}',
                ':after_time' => $after_time,
                ':current_time' => $current_time,
            ])
            ->queryColumn();

        // разбить на части по 1000 ID
        $arrayQuery = array_chunk($queryIds, 1000);

        foreach ($arrayQuery as $arrayIds) {
            $this->findTracker($arrayIds);
        }

    }


    private function findTracker(array $arrayIds)
    {
        $key = '2ErP6c7wcUYWfkn';
        $url = 'http://europay.co/api/trackers/get';

        // key ключ
        // $ids '52410,50333,83675,62070,93639,93387'  id unicall
        $ids = implode(',', $arrayIds);
        $data = [
            'key' => md5($key . strlen($ids)),
            'ids' => $ids,
        ];

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl($url)
            ->setData($data)
            ->setOptions([
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 60,
            ])
            ->send();

        // Обработаем ответ
        if ($response->isOk) {
            $result = $response->data;
            $savedIds = [];
            // if good
            if (key_exists('success', $result) && $result['success']) {
                foreach ($result['data'] as $orderId => $value) {
                    $jsonData = Json::encode([
                        'code' => (int)$value['status'],
                        'tracker' => $value['tracker'],
                    ]);

                    // Сохраним трекеры в БД unicall
                    $savedIds[] = $orderId;
                    if (Order::updateByParams([
                        'approve_components' => $jsonData],
                        ['id' => $orderId, 'foreign_id' => $value['id']])) {
                        Console::output($orderId . '=' . $jsonData);
                    } // Если трекер не сохранился
                    else {
                        Console::output('Не сохранился трекер c ID=' . $jsonData);
                        $this->updateOrder($orderId, 5);
                    }
                }
            }

            // Если не найдены заказы, выберем эти ID
            $diffOrderIds = array_diff($arrayIds, $savedIds);
            foreach ($diffOrderIds as $keyId => $orderId) {
                $this->updateOrder($orderId, 0);
            }
        } else {
            \Yii::error('Ошибка запроса трекинга http://europay.co/api/trackers/get, для IDs=' . $ids);
        }
    }


    /**
     * Функция обновит approve_components с определенным кодом
     *
     * @param int $orderId
     * @param int $code
     */
    private function updateOrder(int $orderId, int $code)
    {
        Console::output('Заказ с ошибкой, статус= ' . $code . ', ID=' . $orderId);
        // Сохраним в БД unicall code = $code
        Order::updateByParams([
            'approve_components' => Json::encode(['code' => (int)$code])
        ], ['id' => $orderId]);
    }

    public function actionSendToAmazon()
    {
        //TODO ID брать из файла. а не вот это вот все.
        $orderIds = [];
        $leadSQS = new LeadSQS();
        foreach ($orderIds as $id) {
            echo 'ID: ' . $id . PHP_EOL;
            $order = new Order();

            $order->useUtc = true;

            $order->findOne(['foreign_id' => $id]);

            if (isset($order->id)) {
                if (!$leadSQS->sendLeadStatus(Order::findOne($order->id))) {
                    echo 'FAIL ID: ' . $id . PHP_EOL;
                } else {
                    echo 'OK ID: ' . $id . PHP_EOL;
                }
            } else {
                echo 'NOT FIND ID: ' . $id . PHP_EOL;
            }
        }
    }

    public function actionReSend($createdAt = false)
    {
        if (!$createdAt) {
            $createdAt = strtotime(date('Y-m-d 00:00:00'));
        } else {
            $createdAt = strtotime($createdAt);
        }

        $query = Order::find()
            ->where(['>=', 'created_at', $createdAt])
            ->andWhere(['partner_id' => 1])
            //->andWhere(['!=', 'type_id', 2])
            ->andWhere(['>', 'status', 1])
            //->andWhere(['>', 'foreign_id', 0])
            ->all();

        if (!is_array($query)) {
            return;
        }

        $leadSQS = new LeadSQS();
        foreach ($query as $order) {
            if (isset($order->id)) {
                echo 'ID:' . $order->id;

                if (is_null($order->foreign_id)) {
                    if (!$leadSQS->sendNewLead($order)) {
                        echo 'FAIL ID: ' . $order->id . PHP_EOL;
                    } else {
                        Yii::$app->db->createCommand()->update('{{%order}}', ['sent_to_sqs' => true], ['id' => $order->id])->execute();
                    }
                } else {
                    if (!$leadSQS->sendLeadStatus($order)) {
                        echo 'FAIL ID: ' . $order->id . PHP_EOL;
                    } else {
                        //echo 'OK ID: '.$order->id.PHP_EOL;
                        Yii::$app->db->createCommand()->update('{{%order}}', ['sent_to_sqs' => true], ['id' => $order->id])->execute();
                    }
                }
            } else {
                echo 'NOT FIND ID: ' . $order->id . PHP_EOL;
            }

        }
    }

    /**
     * Метод для поиска заказов без очереди и создания очереди и присвоение заказам очередь
     *
     * @return bool
     */
    public function actionFindOrderWithOutQueue()
    {
        $orders = Order::find()
            ->where(
                [
                    'status' => 1,
                    'last_queue_id' => null
                ]
            )
            ->all();

        if (empty($orders)) {
            return true;
        }

        foreach ($orders as $order) {
            if (!$queue = Queue::checkExistsQueueByType($order->country_id, $order->type_id, $order->partner_id)) {
                if (!$queueId = Queue::createDefaultQueue($order->partner_id, $order->country_id, $order->type_id)) {
                    return false;
                }
            } else {
                $queueId = $queue->id;
            }

            $order->last_queue_id = $queueId;
            $order->saveWithoutLogs();
        }
    }

    public function actionGetBuyout()
    {


        $partners = Partner::find()->active()->all();  //Берем всех активных партнеров

        if (!$partners) {
            return false;
        }

        foreach ($partners as $partner) {
            if (!empty($partner->buyout_url)) {
                $orders = OrderBuyout::find()
                    ->select(['order_id'])
                    ->where(
                        [
                            'partner_id' => $partner->id,
                            'buyout' => 0
                        ]
                    );

                if ($orders) {
                    $orderIds = [];
                    foreach ($orders->each(1000) as $order) {
                        $orderIds[] = $order->order_id;
                    }
                }

                if (!empty($partner->header_auth)) {
                    $auth = ['Authorization' => $partner->header_auth];

                    if($result = $this->getFromApiPay($orderIds, $auth, $partner->buyout_url)){
                        foreach ($result as $id => $response){
                            $order->setBuyoutFromAPIResponse($response);
                        }
                    }
                }

            }
        }
    }

    public function getFromApiPay($ids, $auth, $url)
    {
        $client = new Client();

        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl($url)
            ->addHeaders($auth)
            ->setData(['ids' => $ids])
            ->send();

        if ($response->isOk) {
            if ($response->data['status'] == 'success') {
                return $response->data['data'];
            }
        }

        $log = new ApiLog();
        $log->response = $response;
        $log->save();

        return false;
    }
}



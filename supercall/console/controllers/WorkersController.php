<?php
/**
 * Created by PhpStorm.
 * User: paraf_000
 * Date: 29.06.2017
 * Time: 14:02
 */

namespace console\controllers;

use api\models\ApiLog;
use api\modules\v1\models\Order;
use backend\modules\order\models\AddressVerification;
use backend\modules\order\models\OrderTask;
use backend\modules\queue\models\Queue;
use common\components\ChatApiCom;
use common\components\Logstash;
use common\components\Platinum;
use common\models\Country;
use common\models\LeadSQS;
use common\models\User;
use common\models\UserCountry;
use common\models\UserPartner;
use common\models\UserSQS;
use common\models\UserWorkTime;
use common\modules\call\models\CallHistory;
use common\modules\order\components\notification\NoAnswerNotificationQueue;
use common\modules\order\models\asterisk\Records;
use common\modules\order\models\Order as OrderModel;
use common\modules\order\models\OrderLog;
use common\modules\order\models\OrderType;
use common\modules\partner\models\Partner;
use common\modules\partner\models\PartnerForm;
use common\modules\partner\models\PartnerFormAttribute;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use yii;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\Console;
use yii\httpclient\Client;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * Class WorkersController
 * @package console\controllers
 *
 * Работа с очередями AmazonSQS
 */
class WorkersController extends Controller
{
    public $log;
    public $order_id;
    public $user_id;
    public $sip_login;

    /**
     *
     */
    public function actionLead()
    {
        $start = microtime(true);

        $LeadSQS = new LeadSQS();
        $messages = $LeadSQS->getLeads();

        $this->stdout("LEAD START\n", Console::FG_GREEN);

        //Если у нас нет сообщений в очереди то, ждем 1 минуту. Считаем, что другие воркеры справляются с нагрузкой
        if (empty($messages)) {
            $this->stdout("MESSAGES NOT FOUND\n", Console::FG_YELLOW);
            sleep(60);
        } else {
            $this->stdout("CREATE ORDER MODEL\n", Console::FG_CYAN);

            //Создаем объект ордера
            $order = new Order();
            $order->scenario = Order::SCENARIO_CREATE;

            $request = Yii::$app->request;

            //Сюда складываем ИД сообщений которые успешнл создали
            foreach ($messages as $messageId => $message) {
                $message = json_decode($message, true);
                print_r($message);
                $this->stdout('Country ' . $message['country'] . "\n", Console::BOLD);

                $partner = Partner::getPartnerByToken($message['partner']);

                //Подставляем ИД партнера которого нашли
                if ($partner) {
                    $message['partner_id'] = $partner->id;
                    $this->stdout("Partner ID " . $message['id'] . "\n");
                } else { //если не нашли то, пишем лог с ошибкой и переходим к следующему сообщению
                    $this->stdout("AUTH ERROR\n", Console::FG_RED);

                    $this->log = new ApiLog();
                    $this->log->order_id = $message['id'];
                    $this->log->url = $request->params[0];
                    $this->log->request = json_encode($message, JSON_UNESCAPED_UNICODE);
                    $this->log->response = 'Dont identificate partner by token';
                    $this->log->save();

                    $errorMessage = json_encode([
                        'order_id' => $message['id'],
                        'message' => $message,
                        'errors' => "UNKNOW PARTNER",
                        'error_type' => $LeadSQS::ERROR_TYPE_UNKNOWN_PARTNER
                    ]);
                    //Отправляем сообщение о ошибке в очередь с ошибками
                    $LeadSQS->addItemErrorQueue($errorMessage);

                    continue;
                }

                //Проверяем наличие в сообщение типа ордера
                if (isset($message['queue_type'])) {
                    $mappedType = OrderType::getMappedType($message['queue_type']);

                    if (!$mappedType) {
                        $this->stdout("MAPPING TYPE {" . $message['queue_type'] . "} ERROR\n", Console::FG_RED);

                        $this->log = new ApiLog();
                        $this->log->order_id = $message['id'];
                        $this->log->url = $request->params[0];
                        $this->log->request = json_encode($message, JSON_UNESCAPED_UNICODE);
                        $this->log->response = "Mapping type order {" . $message['queue_type'] . "} error";
                        $this->log->save();

                        $errorMessage = json_encode([
                            'order_id' => $message['id'],
                            'message' => $message,
                            'errors' => "UNKNOW QUEUE TYPE " . $message['queue_type'],
                            'error_type' => $LeadSQS::ERROR_TYPE_UNKNOWN_QUEUE
                        ]);
                        //Отправляем сообщение о ошибке в очередь с ошибками
                        $LeadSQS->addItemErrorQueue($errorMessage);

                        continue;
                    } else {
                        $message['type'] = $mappedType;
                    }
                } else {
                    $this->stdout("Mapping type don`t find in order\n", Console::FG_RED);

                    $this->log = new ApiLog();
                    $this->log->order_id = $message['id'];
                    $this->log->url = $request->params[0];
                    $this->log->request = json_encode($message, JSON_UNESCAPED_UNICODE);
                    $this->log->response = "Mapping type not fined on order";
                    $this->log->save();
                    $errorMessage = json_encode([
                        'order_id' => $message['id'],
                        'message' => $message,
                        'errors' => $order->getErrors()
                    ]);
                    //Отправляем сообщение о ошибке в очередь с ошибками
                    $LeadSQS->addItemErrorQueue($errorMessage);
                    continue;
                }


                //Делаем поиск этого заказа по id заказа и внешего ИД
                $checkOrder = OrderModel::findAll([
                    'foreign_id' => $message['id'],
                    'partner_id' => $message['partner_id']
                ]);

                $orderExist = false;

                //Если не нашли то, пробуем преобразовать данные о клиенте в json формат под подходящую форму у партнера в этой стране
                if (empty($checkOrder)) {
                    $this->stdout("NEW LEAD\n", Console::FG_CYAN);

                    $customer_components = $this->generateCustomerComponents($message);

                    if (!$customer_components) {
                        //записать в заказы-косяки
                        $message['approve_components'] = json_encode([
                            "code" => 5,
                            "tracker" => null
                        ], JSON_UNESCAPED_UNICODE);
                    } else {
                        $customer_components_completed = [];
                        foreach ($customer_components as $scope => $attribute) {
                            foreach ($attribute as $field => $value) {
                                $customer_components_completed[$scope][$field] = isset($message[$field]) ? $message[$field] : null;
                            }
                        }
                        $message['customer_components'] = json_encode($customer_components_completed, JSON_UNESCAPED_UNICODE);
                    }
                } else { //если нашли такой заказ то, считаем, что у нас есть оригинал такого заказа
                    $this->stdout("DUBBED LEAD\n", Console::FG_GREY);
                    $orderExist = true;

                    //Проверяем, нет ли у нас уже дубля заказа с этим типом очереди
                    foreach ($checkOrder as $_order) {
                        //Проверяем на вторичную отправку заказа
                        if ($_order->type_id == $message['type'] && $message['is_second_sent'] && in_array($_order->status, array_keys(OrderModel::getNotFinalStatuses()))) {
                            $LeadSQS->sendLeadStatus($_order);
                            continue 2;
                        }

                        if ($message['type'] == OrderType::TYPE_NEW) {
                            $this->stdout("DUBBED LEAD FOR ORDER WITH TYPE NEW " . count($checkOrder) . "  \n", Console::FG_RED);

                            $this->log = new ApiLog();
                            $this->log->order_id = $message['id'];
                            $this->log->url = $request->params[0];
                            $this->log->request = json_encode($message, JSON_UNESCAPED_UNICODE);
                            $this->log->response = "DUBBED LEAD FOR ORDER WITH TYPE NEW";
                            $this->log->save();

                            $LeadSQS->sendLeadStatus($_order);

                            //После того как отправили в ошибку и записали в логи, берем следующий заказ из очереди сообщений
                            continue 2;
                        }

                        if ($_order->type_id == $message['type'] && in_array($_order->status, array_keys(OrderModel::getNotFinalStatuses()))) {
                            $this->stdout("DUBBED LEAD IS NOT IN FINAL STATUSES  " . count($checkOrder) . "  \n", Console::FG_RED);

                            $this->log = new ApiLog();
                            $this->log->order_id = $message['id'];
                            $this->log->url = $request->params[0];
                            $this->log->request = json_encode($message, JSON_UNESCAPED_UNICODE);
                            $this->log->response = "DUBBED LEAD IS NOT IN FINAL STATUSES";
                            $this->log->save();

                            /*$errorMessage = json_encode([
                                'order_id' => $message['id'],
                                'original_order_id' => $_order->id,
                                'message' => $message,
                                'errors' => "DUBBED LEAD IS NOT IN FINAL STATUSES"
                            ]);*/
                            //Отправляем сообщение о ошибке в очередь с ошибками
                            //$LeadSQS->addItemErrorQueue($errorMessage);
                            $LeadSQS->sendLeadStatus($_order);

                            //После того как отправили в ошибку и записали в логи, берем следующий заказ из очереди сообщений
                            continue 2;
                        }

                        //По умолчанию записываем в этот заказ данные из оригинального заказа
                        if ($_order->type_id == OrderType::TYPE_NEW) {
                            $message['customer_components'] = $_order->customer_components;
                        }
                    }
                }

                $this->stdout('ORDER TYPE: ' . $message['queue_type'] . '(' . $message['type'] . ')' . PHP_EOL);

                $order->load($message, '');
                $order->order_comment = $message['order_comment'];

                //Определяем есть ли подарки в заказе
                foreach ($order->products as &$product) {
                    if ($product['price'] == 0) {
                        $product['gift'] = true;
                    } else {
                        $product['gift'] = false;
                    }
                }

                if (!$order_id = $order->create($orderExist)) {
                    $this->stdout("CREATE FAIL\n", Console::FG_RED);
                    $this->stdout(print_r($order->getErrors(), 1) . "\n\n");
                    $errorMessage = json_encode([
                        'order_id' => $message['id'],
                        'message' => $message,
                        'errors' => $order->getErrors()
                    ]);
                    //Отправляем сообщение о ошибке в очередь с ошибками
                    $LeadSQS->addItemErrorQueue($errorMessage);

                    //логирование ошибки
                    $this->log = new ApiLog();
                    $this->log->order_id = $message['id'];
                    $this->log->url = $request->params[0];
                    $this->log->request = json_encode($message, JSON_UNESCAPED_UNICODE);
                    $this->log->response = $errorMessage;
                    $this->log->save();

                } else {
                    $this->stdout("CREATE OK\n", Console::FG_GREEN);
                    if ($order_id) {
                        $this->stdout("SENT STATUS TO ERP\n", Console::FG_GREEN);
                        if ($LeadSQS->sendLeadStatus(OrderModel::find()->where(['id' => $order_id])->one())) {
                            $this->stdout("SENT STATUS IS OK\n", Console::FG_GREEN);
                        } else {
                            $this->stdout("SENT STATUS ERROR\n\n", Console::FG_RED);
                        }
                    }

                }
            }

            if (!empty($LeadSQS->handlers)) {
                foreach ($LeadSQS->handlers as $handlers) {
                    $this->stdout("DELETE MESSAGE FROM SQS \n", Console::FG_GREY);
                    //Удаляем из очереди обработанные сообщения
                    $LeadSQS->deleteMessage($handlers, LeadSQS::AMAZON_LEADS_QUEUE);
                }
            }

            $end = microtime(true) - $start;
            //Если скрипт выполнялся менее 3х секунд
            if ($end < 3) {
                sleep(3 - $end);
            }
        }
    }

    /**
     * @param array $order
     * @return array|boolean
     */
    public function generateCustomerComponents($order)
    {
        $customer_components = [];

        if ($form = $this->getPartnerForm($order['country'], $order['partner_id'])) {
            //поиск атрибутов формы
            if (!$form) {
                return false;
            }

            $formAttributes = $this->getFormAttributes($form->one()->id);
            if ($formAttributes->exists()) {
                foreach ($formAttributes->all() as $attribute) {
                    if (!isset($customer_components[$attribute->scope])) {
                        $customer_components[$attribute->scope] = [];
                    }

                    if (!isset($customer_components[$attribute->scope][$attribute->name])) {
                        $customer_components[$attribute->scope][$attribute->name] = null;
                    }
                }
            }
        }

        return empty($customer_components) ? false : $customer_components;
    }

    /**
     * @param string $country_charcode
     * @param integer $partner_id
     * @return bool|yii\db\Query PartnerForm
     */
    public function getPartnerForm($country_charcode, $partner_id)
    {
        $country = Country::find()->where(['char_code' => $country_charcode, 'active' => 1]);;

        if ($country->exists()) {
            $partnerForm = PartnerForm::find()
                ->where(['country_id' => $country->one()->id, 'active' => 1])
                ->andWhere(['partner_id' => $partner_id]);
            return $partnerForm;
        }

        return false;

    }

    /**
     * @param $form_id
     * @return yii\db\Query PartnerFormAttribute
     */
    public function getFormAttributes($form_id)
    {
        $formAttributes = PartnerFormAttribute::find()
            ->where(['form_id' => $form_id]);

        return $formAttributes;
    }

    /**
     *  Отправка тасков в статусе 5-pending
     */
    public function actionSendTasks()
    {
        $start = microtime(true);

        $orderTasks = OrderTask::find()->where(['status' => OrderTask::TASK_STATUS_PENDING])->all();

        if (!empty($orderTasks)) {
            $orderTaskModel = new OrderTask();

            foreach ($orderTasks as $orderTask) {
                $orderTaskModel->sendOrderTaskToPay($orderTask);
            }
        }

        $end = microtime(true) - $start;
        //Если скрипт выполнялся менее 3х секунд
        if ($end < 3) {
            sleep(3 - $end);
        }

    }

    public function actionStartRatchetServer()
    {
        $ratchet = yii::$app->get('ratchet');

        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    $ratchet
                )
            ),
            $ratchet->server_port
        );

        $server->run();
    }

    public function actionAddUser()
    {
        $users = [];
        foreach ($users as $u) {
            $transaction = yii::$app->db->beginTransaction();
            $generatePassword = User::generatePassword();
            $userLastId = User::find()->orderBy('id DESC');

            $user = new User();
            $user->status = User::STATUS_DEFAULT;
            $user->status = User::STATUS_DEFAULT;
            $user->first_name = $u['first_name'];
            $user->last_name = $u['last_name'];
            $user->username = $u['user_name'] . '-' . ($userLastId->one()->id + 1);
            $user->password = $generatePassword;
            $user->setPassword($generatePassword);

            if (!$user->save(false)) {
                $transaction->rollBack();
            } else {
                $data = ['username' => $user->username, 'password' => $generatePassword];
                $this->addToAsterisk($data);

                $userPartner = new UserPartner();
                $userPartner->user_id = $user->id;
                $userPartner->partner_id = 1;
                $userPartner->save();
                if (is_array($u['char_code']) && !empty($u['char_code'])) {
                    foreach ($u['char_code'] as $code) {
                        $country = Country::find()->where(['char_code' => $code]);
                        $userCountry = new UserCountry();
                        $userCountry->user_id = $user->id;
                        $userCountry->country_id = $country->one()->id;
                        $userCountry->save();
                    }
                } else {
                    $country = Country::find()->where(['char_code' => $u['char_code']]);
                    $userCountry = new UserCountry();
                    $userCountry->user_id = $user->id;
                    $userCountry->country_id = $country->one()->id;
                    $userCountry->save();
                }

                $authRole = Yii::$app->authManager->getRole(User::ROLE_OPERATOR);
                Yii::$app->authManager->assign($authRole, $user->id);
                echo $u['first_name'] . ' ' . $u['last_name'] . ' ' . $user->username . ' => ' . $generatePassword . PHP_EOL;
                $transaction->commit();
            }
        }
    }

    public function addToAsterisk($data)
    {
        $user = User::findOne(['username' => $data['username']]);
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(yii::$app->params['AsteriskSp_adduser'])
            ->setData([
                'login' => yii::$app->params['AsteriskSp_prefix'] . $user->id,
                'pass' => $data['password']
            ])
            ->send();

        if ($response->isOk) {
            $answer = json_decode($response->content, 1);
            if (isset($answer['status']) && $answer['status'] === true) {
                $parseUrl = parse_url(yii::$app->params['AsteriskSp_adduser']);

                $sip = [
                    'sip_host' => $parseUrl['host'],
                    'sip_login' => yii::$app->params['AsteriskSp_prefix'] . $user->id,
                    'sip_pass' => $data['password']
                ];

                $user->sipArray = $sip;

                if (!$user->save()) {
                    $this->log = new ApiLog();
                    $this->log->request = 'insert sip data to User (id = ' . $user->id . ') table . ' . json_encode($sip, JSON_UNESCAPED_UNICODE);
                    $this->log->response = json_encode($user->getErrors(), JSON_UNESCAPED_UNICODE);
                    $this->log->save();
                }

            } else {
                $this->log = new ApiLog();
                $this->log->request = 'insert sip data to Asterisk2 (id = ' . $user->id . ') . ' . json_encode([
                        'login' => yii::$app->params['AsteriskSp_prefix'] . $user->id,
                        'pass' => $data['password']
                    ], JSON_UNESCAPED_UNICODE);
                $this->log->response = $response->content;
                $this->log->save();
            }
        } else {
            $this->log = new ApiLog();
            $this->log->request = 'insert sip data to Asterisk2 (id = ' . $user->id . ') . ' . json_encode([
                    'login' => yii::$app->params['AsteriskSp_prefix'] . $user->id,
                    'pass' => $data['password']
                ], JSON_UNESCAPED_UNICODE);
            $this->log->response = $response->content;
            $this->log->save();
        }
    }

    /**
     * Метод подтянет все записи с сервера астериск и уложит их по полочкам для заказов - в таблицу Record и файлы по папочкам.
     *
     * php yii workers/get-records order_id  123123,1231541,155231
     * php yii workers/get-records limit  10
     *
     * @param string $type limit|order_id|user_id|sip_login
     * @param string $list
     */
    public function actionGetRecords($type = null, $list = null)
    {
        if (!is_null($type) && !is_null($list)) {
            echo PHP_EOL . PHP_EOL;
            echo '* Start get-records' . PHP_EOL;
            echo $this->setLine();
            echo 'Type: ' . $type . PHP_EOL;
            echo 'List: ' . $list . PHP_EOL;
            echo $this->setLine();
            $list = explode(',', $list);

            if ($type == 'order_id') {
                $order_id = array_map('trim', $list);
                echo '<pre>' . print_r($order_id, 1) . '</pre>';
            }

            if ($type == 'user_id') {
                $user_id = array_map('trim', $list);
                echo '<pre>' . print_r($user_id, 1) . '</pre>';
            }

            if ($type == 'sip_login') {
                $sip_login = array_map('trim', $list);
                echo '<pre>' . print_r($sip_login, 1) . '</pre>';
            }

            if ($type == 'limit') {
                $limit = $list[0];
            }
        }

        $query = CallHistory::find()
            ->select([
                'user_id' => CallHistory::tableName() . '.user_id',
                'sip_login' => new Expression(User::tableName() . ".sip::jsonb->>'sip_login'"),
                'order_id' => CallHistory::tableName() . '.order_id',
                'phone' => new Expression("case when " . OrderModel::tableName() . ".customer_phone is null then " . OrderModel::tableName() . ".customer_mobile else customer_phone end")
            ])
            ->leftJoin(User::tableName(), User::tableName() . '.id=' . CallHistory::tableName() . '.user_id')
            ->leftJoin(OrderModel::tableName(), OrderModel::tableName() . '.id=' . CallHistory::tableName() . '.order_id')
            ->where([
                'like',
                User::tableName() . '.sip',
                's'
            ])
            ->groupBy([
                CallHistory::tableName() . '.user_id',
                CallHistory::tableName() . '.order_id',
                new Expression(User::tableName() . ".sip::jsonb->>'sip_login'"),
                OrderModel::tableName() . '.customer_phone',
                OrderModel::tableName() . '.customer_mobile'
            ]);

        if (isset($order_id)) {
            $query->andFilterWhere(['in', CallHistory::tableName() . '.order_id', $order_id]);
        }

        if (isset($user_id)) {
            $query->andFilterWhere(['in', CallHistory::tableName() . '.user_id', $user_id]);
        }

        if (isset($sip_login)) {
            $query->andFilterWhere(['=', "sip::jsonb->>'sip_login'", $sip_login]);
        }

        if (isset($limit)) {
            $query->limit($limit);
        }

        echo '* SQL: ' . $query->createCommand()->getRawSql() . PHP_EOL;
        echo $this->setLine();

        echo '* Count All: ' . $query->count() . PHP_EOL;
        echo $this->setLine();

        if ($query->count() > 0) {
            $data = $query->asArray()->all();
            $dataRecords = [];

            foreach ($data as $k => $v) {
                echo PHP_EOL . 'sip_login : ' . $v['sip_login'] . ' user_id : ' . $v['user_id'] . ' order_id: ' . $v['order_id'] . ' phone: ' . $v['phone'] . PHP_EOL;
                //данные с сервера астериска
                $recordsFromAsterisk = Records::getDataFromAsterisk($v['sip_login'], [$v['phone']]);

                $dataRecords[] = [
                    'data' => $recordsFromAsterisk,
                    'user_id' => $v['user_id']
                ];
                //кол-во записеий на колле в бд
                $countAsterisk = count($recordsFromAsterisk);

                $countRecords = Records::find()->where(['order_id' => $v['order_id']])->count();


                echo '* Count Asterisk2: ' . $countAsterisk . PHP_EOL;
                echo '* Count Records: ' . $countRecords . PHP_EOL;
                echo $this->setLine();

                if ($countAsterisk != $countRecords) {
                    echo '* Get to records' . PHP_EOL;

                    echo json_encode($recordsFromAsterisk, JSON_UNESCAPED_UNICODE) . PHP_EOL;

                    $orderQuery = OrderModel::find()->where(['id' => $v['order_id']]);

                    if ($orderQuery->exists()) {
                        $order = $orderQuery->one();
                        Records::downloadRecords($dataRecords, $order);
                    }
                }

                //echo '<pre>' . print_r($dataRecords, 1) . '</pre>';
            }
            #$dataFromAsterisk = Records::getDataFromAsterisk();
        }
    }

    //метод для снятия хода с заказов с признаками confirm_address = 1 - если в течении часа ТМ не провел проверку адреса
    //данные адреса таких заказов в КЛАДР не добавляются, но холд заказов снимается за счет смены значения признака confirm_address = 3
    //раздел для подтверждения адреса /order/address-verification/index
    //олд заказа выставляется на 1 час (и только для апрува - 4) - т.е. now() - updated_at не должно быть более 60 минут

    public function setLine()
    {
        return '----------------------' . PHP_EOL;
    }

    public function actionCheckAddressVerification()
    {
        $orders = OrderModel::find()
            ->where(['confirm_address' => AddressVerification::ADDRESS_NEED_CONFIRM])
            ->andWhere([
                '<=',
                'updated_at',
                new Expression("(EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE 'now'::timestamp - interval '1 hour'))")
            ])
            ->andWhere(['status' => OrderModel::STATUS_APPROVED])
            ->all();

        foreach ($orders as $order) {
            echo $order->id . PHP_EOL;
            $order->confirm_address = AddressVerification::ADDRESS_NOT_CONFIRMED;

            try {
                yii::$app->db->createCommand('UPDATE "order" SET confirm_address = 3 WHERE id = ' . $order->id)
                    ->execute();

                $leadSQSModel = new LeadSQS();
                $result = $leadSQSModel->sendLeadStatus($order);

                if ($result) {
                    //в случае успешной отправки - обновит статус отправки
                    Yii::$app->db->createCommand()
                        ->update('{{%order}}', ['sent_to_sqs' => true], ['id' => $order->id])
                        ->execute();
                } else {
                    $this->log = new ApiLog();
                    $this->log->order_id = $order->id;
                    $this->log->url = 'workers/check-address-verification';
                    $this->log->request = '$leadSQSModel->sendLeadStatus($order)';
                    $this->log->response = json_encode($result, JSON_UNESCAPED_UNICODE);
                    $this->log->save();
                }

            } catch (yii\db\Exception $e) {
                echo 'ID: ' . $order->id . ' confirm_address = ' . AddressVerification::ADDRESS_NOT_CONFIRMED . ' ERROR : ' . json_encode($e->getMessage(), JSON_UNESCAPED_UNICODE);

                $this->log = new ApiLog();
                $this->log->order_id = $order->id;
                $this->log->url = 'workers/check-address-verification';
                $this->log->request = 'UPDATE "order" SET confirm_address = 3 WHERE id = ' . $order->id;
                $this->log->response = json_encode($e->getMessage(), JSON_UNESCAPED_UNICODE);
                $this->log->save();
            }
        }
    }

    /**
     * Получение данных от партнёров для действия с пользователями CRM
     */
    public function actionActionsWithUsers()
    {
        $UserSQS = new UserSQS();
        $messages = $UserSQS->getActions(10);

        if ($messages) {
            foreach ($messages as $messageId => $message) {
                $message = json_decode($message, 1);

                $logger = yii::$app->get('logger');
                $logger->action = 'actions-with-users';
                $logger->route = $this->route;
                $logger->type = UserSqs::LOGGER_TYPE;

                if (!isset($message['action'])) {
                    $logger->tags = ['error' => 'action not sended'];
                    $logger->error = json_encode($message, JSON_UNESCAPED_UNICODE);
                    echo 'ACTION NOT SENDED :' . json_encode($message, JSON_UNESCAPED_UNICODE) . PHP_EOL;
                } else {

                    switch ($message['action']) {
                        //увольнение
                        case $UserSQS::ACTION_DISMISSAL :
                            $this->BlockUsersWithQueue($message, $logger);
                            break;
                    }

                    $logger->save();
                }
            }
        } else {
            echo 'ACTIONS USER QUEUE IS EMPTY' . PHP_EOL;
            sleep(60);
        }

        if (!empty($UserSQS->handlers)) {
            foreach ($UserSQS->handlers as $handler) {
                $UserSQS->deleteMessage($handler, $UserSQS::AMAZON_ACTIONS_USER_FROM_ERP);
            }
        }

    }

    /**
     * @param $message
     * @param $logger
     */
    public function BlockUsersWithQueue($message, $logger)
    {
        if (!isset($message['user_id'])) {
            $logger->tags = ['error' => 'user not sended'];
            $logger->error = json_encode($message, JSON_UNESCAPED_UNICODE);
            echo 'USER NOT SENDED :' . json_encode($message, JSON_UNESCAPED_UNICODE) . PHP_EOL;
        } elseif (!is_array($message['user_id'])) {
            $logger->tags = ['error' => 'user is not array'];
            $logger->error = json_encode($message, JSON_UNESCAPED_UNICODE);
        } else {
            foreach ($message['user_id'] as $user_id) {
                $dismissal = User::blockUser($user_id);

                if (!$dismissal['success']) {
                    $logger->tags = $message;
                    $logger->error = json_encode($dismissal['message'], JSON_UNESCAPED_UNICODE);
                    echo 'DISMISSAL FAILED :' . json_encode([
                            'error' => $dismissal['message'],
                            'request' => $message
                        ], JSON_UNESCAPED_UNICODE) . PHP_EOL;
                } else {
                    $logger->tags = $message;
                    $logger->error = '';
                    echo 'DISMISSAL SUCCESS :' . json_encode($message, JSON_UNESCAPED_UNICODE) . PHP_EOL;
                }
            }
        }
    }

    /**
     * for actionActionsWithUsers()
     * php yii workers/test-send-to-amazon-actions-user
     */
    public function actionTestSendToAmazonActionsUser()
    {
        $UserSQS = new UserSQS();
        $queue = $UserSQS::AMAZON_ACTIONS_USER_FROM_ERP;

        $messages = [
            ['action' => 'dismissal', 'user_id' => [999, 12]],
            ['user_id' => [999]],
            ['action' => 'dismissal'],
            ['action' => 'dismissal', 'user_id' => [2]]
        ];

        foreach ($messages as $message) {
            echo 'SENT: ' . json_encode($message, JSON_UNESCAPED_UNICODE) . PHP_EOL;

            $UserSQS->sendMessage([
                'MessageBody' => json_encode($message, JSON_UNESCAPED_UNICODE),
                'QueueUrl' => $queue
            ]);
        }
    }

    /**
     * Подтягивать транскрибацию руками
     * php yii workers/get-transcribe order_id  123123,1231541,155231
     * php yii workers/get-transcribe limit  10
     * @param null $type
     * @param null $list
     */
    public function actionGetTranscribe($type = null, $list = null)
    {
        if (!is_null($type) && !is_null($list)) {
            echo PHP_EOL . PHP_EOL;
            echo '* Start get-transcribe' . PHP_EOL;
            echo $this->setLine();
            echo 'Type: ' . $type . PHP_EOL;
            echo 'List: ' . $list . PHP_EOL;
            echo $this->setLine();
            $list = explode(',', $list);

            if ($type == 'order_id') {
                $order_id = array_map('trim', $list);
                echo '<pre>' . print_r($order_id, 1) . '</pre>';
            }

            if ($type == 'limit') {
                $limit = $list[0];
            }

            $query = CallHistory::find()->where(['is not', 'recordingfile', null])->andWhere(['disposition' => 'ANSWERED'])->orderBy(['id' => SORT_DESC]);

            if ($type == 'limit') {
                $query->limit($limit);
            } else {
                $query->andWhere(['order_id' => $list]);
            }

            if (!$query->exists()) {
                echo 'No call records found!' . $this->setLine();
            } else {
                $records = $query->all();

                foreach ($records as $record) {
                    $googleSpeech = yii::$app->get('GoogleSpeech');
                    $googleSpeech->setOrder(OrderModel::find()->where(['id' => $record->order_id])->one());
                    $googleSpeech->call_history_id = $record->id;
                    $googleSpeech->file = $record->recordingfile;
                    $googleSpeech->getRecordTranscribe();
                }


                foreach ($records as $record) {
                    if ($record->transcribe) {
                        echo 'order_id: ' . $record->order_id . ' is completed' . PHP_EOL;
                    } else {
                        echo 'order_id: ' . $record->order_id . ' is empty' . PHP_EOL;
                    }
                }
            }
        }
    }

    /**
     * Отправка клиентам уведомлений о недозвоне
     */
    public function actionSendNoAnswerNotification()
    {
        $queue = new NoAnswerNotificationQueue();
        $queue->sendNotifications();
        sleep(2);
    }

    /**
     *  Обновление списка ролей на пее у стороннего источника
     */
    public function actionUpdateRoles()
    {
        $component = yii::$app->get('Wtrade');
        $component->sentData();
    }

    /**
     * ['OrderID' => "Order - 7888",  'Phone' => '+7999999999999', 'Product' =>  'Phone'  , 'ClientName' => 'Bob',  'Geo'=>'MY', 'MessageToClient' => $text]
     */
    public function actionPlatinumLiveMessageSendTest()
    {
        /** @var Platinum $platinum */
        $platinum = yii::$app->get(Platinum::NAME);
        $order = OrderModel::find()->orderBy(['id' => SORT_DESC])->limit(1)->all();

        $messages = $platinum->createMessageFromOrder($order[0]);

        foreach ($messages as $message) {
            if (!empty($message['Phone'])) {
                $data = $platinum->sendToPlatinum($message);
                $result = $platinum->addToChat($data);

                Console::stdout('Result: ' . ($result['success'] === true ? 'OK' : 'ERROR') . PHP_EOL);
                echo '<pre>' . print_r($result['message'], 1) . '</pre>';
            }
        }


    }

    public function actionAggregateWorkTime($allTime = false, $debug = false)
    {
        $this->stdout('START Aggregate Work Time ' . PHP_EOL, Console::BG_CYAN);
        $this->stdout(PHP_EOL);
        UserWorkTime::aggregate($allTime, $debug);
        $this->stdout('DONE Aggregate Work Time ' . PHP_EOL, Console::BG_CYAN);
        $this->stdout(PHP_EOL);
    }

    /**
     * Отправка статусов принятых заказов в очередь амазон
     * https://2wtrade-tasks.atlassian.net/browse/WCALL-505
     */
    public function actionSentStatusesFromCallCenterToNpay($lifetime = 86400)
    {
        $is_dev = yii::$app->params['is_dev'];

        $LeadSQS = new LeadSQS();

        if ($is_dev) {
            $queue = yii::$app->params['AmazonCallCenterCheckingStatusDev'];
        } else {
            $queue = yii::$app->params['AmazonCallCenterCheckingStatus'];
        }
        $launchTime = time();
        while (((time() - $launchTime) < $lifetime)) {
            $messages = $LeadSQS->getMessagesFromQueue($queue);

            Console::output('Queue: ' . $queue);
            //foreign-id наш ID
            //order_id - пея
            if ($messages) {
                if (is_array($messages) && count($messages) > 0) {

                    foreach ($messages as $messagesJson) {
                        $messagesDecoded = Json::decode($messagesJson);

                        $order_ids = ArrayHelper::getColumn($messagesDecoded, 'foreign_id');

                        //для локального теста
                        //$orders = OrderModel::find()->limit(10)->all();

                        $orders = ArrayHelper::index(OrderModel::find()->where(['id' => $order_ids])->all(), 'id');

                        $dataForCheck = [];

                        foreach ($orders as $order) {
                            $dataForCheck[$order->id] = hash('sha256', $LeadSQS->generateMessageBodyJson($order));
                        }

                        foreach ($messagesDecoded as $message) {
                            //ордер не найден у КЦ - отправим в очередь ошибок
                            if (!isset($dataForCheck[$message['foreign_id']])) {
                                Console::output('Order #' . $message['foreign_id'] . ' (foreign_id: ' . $message['order_id'] . ') not found');
                                $this->sentInError($LeadSQS, $message);
                            } else {
                                if ($dataForCheck[$message['foreign_id']] != $message['hash']) {
                                    //hash не совпадает - нужно отправит свежие данные по статусу
                                    Console::output('Order #' . $message['foreign_id'] . ' hash not equal');
                                    Console::output('npay hash: ' . $message['hash']);
                                    Console::output('wcall hash: ' . $dataForCheck[$message['foreign_id']]);

                                    if (!$is_dev) {
                                        $LeadSQS->sendLeadStatus($orders[$message['foreign_id']]);
                                    } else {
                                        /** @var OrderModel $orders [$message['order_id']] */
                                        echo '<pre>' . print_r($orders[$message['foreign_id']]->getAttributes(), 1) . '</pre>';
                                    }
                                } else {
                                    Console::output('Order #' . $message['foreign_id'] . ' hash is equal');
                                    Console::output('npay hash: ' . $message['hash']);
                                    Console::output('wcall hash: ' . $dataForCheck[$message['foreign_id']]);
                                }
                            }
                        }
                    }

                    if (!$is_dev) {
                        if (!empty($LeadSQS->handlers)) {
                            foreach ($LeadSQS->handlers as $handlers) {
                                $this->stdout("DELETE MESSAGE FROM SQS \n", Console::FG_GREY);
                                //Удаляем из очереди обработанные сообщения
                                $LeadSQS->deleteMessage($handlers, $queue);
                            }
                        }
                    }
                } else {
                    Console::output('Queue empty');
                    sleep(60);
                }
            }
        }
    }

    /**
     * @param LeadSQS $LeadSQS
     * @param array $message
     */
    public function sentInError($LeadSQS, $message)
    {
        $errorMessage = json_encode([
            'order_id' => $message['order_id'],
            'foreign_id' => $message['foreign_id'],
            'message' => $message,
            'errors' => "ORDER NOT FOUND",
            'error_type' => LeadSQS::ERROR_TYPE_ORDER_NOT_FOUND
        ]);
        //Отправляем сообщение о ошибке в очередь с ошибками
        if (yii::$app->params['is_dev']) {
            $LeadSQS->addItemDevErrorQueue($errorMessage);
        } else {
            $LeadSQS->addItemErrorQueue($errorMessage);
        }
    }

    /**
     *  воркер проверяет сообщения на chat-api.com (не используется новый live-messenger/get-messages)
     */
    public function actionGetLiveMessages()
    {
        $query = Country::find()->where(['is not', 'whatsapp_token', null]);

        $phonesCountry = ArrayHelper::getColumn($query->all(), function($data){return $data['whatsapp_phone'] . ChatApiCom::END;});

        foreach ($query->batch(5) as $countries) {
            foreach ($countries as $country) {
                /** @var Country $country */
                /** @var ChatApiCom $chatApiCom */
                $chatApiCom = yii::$app->get('ChatApiCom');

                $number = 10000;
                $response = $chatApiCom->getMessages($number, $country->whatsapp_url, $country->whatsapp_token);
                $result = Json::decode($response);
                $messages = [];

                if (isset($result['messages'])) {
                    foreach ($result['messages'] as $message) {
                        //из истории сообщений - нам нужны только ответы клиента
                        if ($message['fromMe'] && (in_array($message['author'], $phonesCountry) || in_array($message['chatId'], $phonesCountry))) {
                            $messages[] =  $message;
                        }
                    }
                }
            }
        }
    }


    /**
     *  метод найдёт все заказы в финальных статусах и очистит у них очередь, если она не была очищена ранее
     *  запуск кроном каждую минуту
     */
    public function actionClearLastQueue()
    {
        $ordersQuery = OrderModel::find()
            ->where(['status' => array_keys(OrderModel::getFinalStatusesCollection())])
            ->andWhere(['is not', 'last_queue_id', null])
            ->andWhere(['>=', 'updated_at', time()-70])
            ->andWhere(['<=', 'updated_at', time()])
            ->orderBy(['id' => SORT_DESC]);

        if($ordersQuery->exists()){
            foreach ($ordersQuery->batch(10) as $orders){
                foreach ($orders as $order){
                    /** @var OrderModel $order */
                    $order->prevent_queue_id = $order->last_queue_id;
                    $order->last_queue_id = null;

                    $save = true;
                    //$save = $order->saveWithoutLogs(false);

                    $log =  Yii::$app->formatter->asDateFullTime(time()) . ' ' . $order->id;

                    if($save){
                        Console::output($log);
                    }else{
                        Console::output($log . ' ' . Json::encode($order->getErrors()));
                    }
                }
            }
        }
    }

    /**
     * не используется на бою
     * отправка логов из RabbitMQ (logs) на сервер логов
     */
    public function actionSentCrmLogs()
    {
        sleep(3);

        $amqp = yii::$app->amqp;
        $amqp->getLog('crm-auto-call', true);
    }

    public function actionSentAsteriskLogs()
    {
        sleep(3);

        $amqp = yii::$app->amqp;
        $amqp->getLog('asterisk-auto-call', true);
    }

    public function actionSentAsteriskInnerLogs()
    {
        sleep(3);

        $amqp = yii::$app->amqp;
        $amqp->getLog('asterisk-inner', true);
    }

}
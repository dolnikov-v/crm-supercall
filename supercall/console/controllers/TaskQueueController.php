<?php

namespace console\controllers;

use backend\components\Asterisk2;
use backend\models\TaskQueue;
use backend\modules\administration\models\BaseLogger;
use backend\modules\stats\models\export\OperatorExport;
use backend\modules\stats\models\export\OperatorOnlineExport;
use backend\modules\stats\models\export\OrderExport;
use backend\modules\stats\models\report\OperatorReport;
use common\components\Asterisk;
use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;

/**
 * Class TaskQueueController
 * @package console\controllers
 */
class TaskQueueController extends Controller
{
    const BATCH_SIZE = 50;

    /**
     *  Переобор очереди заданий
     */
    public function actionJobTaskQueue()
    {
        /** @var BaseLogger $logger */
        $logger = Yii::$app->get('logger');
        $logger->action = Yii::$app->controller->action->id;
        $logger->route = Yii::$app->controller->route;

        $tasksQuery = TaskQueue::find()->where(['status' => TaskQueue::STATUS_READY]);

        foreach ($tasksQuery->batch(self::BATCH_SIZE) as $tasks) {

            foreach ($tasks as $task) {
                Console::output('Type: ' . $task->type);
                /** @var TaskQueue $task */
                switch ($task->type) {
                    case TaskQueue::TYPE_CALL_RECORD_IMPORT:
                        $this->getCallRecords($task, $logger);
                        break;
                    case TaskQueue::TYPE_STATS_OPERATOR_IMPORT:
                        $task->status = TaskQueue::STATUS_IN_PROGRESS;
                        $task->save(false, ['status', 'path']);

                        $result = OperatorExport::exportExcel($task);

                        if (!$result['success']) {
                            TaskQueue::changeStatus($task->id, TaskQueue::STATUS_FAIL, $result['message']);
                        } else {
                            $task->path = $result['message'];
                            $task->status = TaskQueue::STATUS_COMPLETED;
                            $task->save(false, ['status', 'path']);
                        }
                        break;
                    case TaskQueue::TYPE_STATS_OPERATOR_ONLINE_IMPORT:
                        $task->status = TaskQueue::STATUS_IN_PROGRESS;
                        $task->save(false, ['status', 'path']);

                        $result = OperatorOnlineExport::exportExcel($task);
                        var_dump($result);
                        if (!$result['success']) {
                            TaskQueue::changeStatus($task->id, TaskQueue::STATUS_FAIL, $result['message']);
                        } else {
                            $task->path = $result['message'];
                            $task->status = TaskQueue::STATUS_COMPLETED;
                            $task->save(false, ['status', 'path']);
                        }
                        break;
                    case TaskQueue::TYPE_STATS_ORDER_IMPORT :
                        $task->status = TaskQueue::STATUS_IN_PROGRESS;
                        $task->save(false, ['status', 'path']);
                        $result = OrderExport::export($task);

                        if (!$result['success']) {
                            TaskQueue::changeStatus($task->id, TaskQueue::STATUS_FAIL, $result['message']);
                        } else {
                            $task->path = $result['message'];
                            $task->status = TaskQueue::STATUS_COMPLETED;
                            $task->save(false, ['status', 'path']);
                        }
                        break;
                }
            }
        }
    }

    /**
     * @param TaskQueue $task
     * @param  BaseLogger $logger
     */
    public function getCallRecords($task, $logger)
    {
        /** @var Asterisk2 $asterisk */
        $asterisk = yii::$app->get('Asterisk2');

        $logger->type = TaskQueue::TYPE_CALL_RECORD_IMPORT;

        $logger->tags = [
            'task_id' => $task->id,
            'order_ids' => implode(ArrayHelper::map($task->taskQueueOrders, 'order_id', 'uniqueid')),
        ];

        $task->status = TaskQueue::STATUS_IN_PROGRESS;

        if (!$task->save(false, ['status'])) {
            $logger->error = Json::encode($task->getErrors());
        } else {
            $result = $asterisk->getCallRecords($task);

            if (isset($result['success']) && isset($result['task_queue_id'])) {
                $logger->tags['aster_task_id'] = $result['task_queue_id'];

                if ($result['success']) {
                    $status = TaskQueue::STATUS_IN_PROGRESS;
                    $error = null;
                } else {
                    $status = $result['success'] = TaskQueue::STATUS_FAIL;
                    $error = $result['message'];
                    $logger->tags['aster_error'] = $error;
                }

                $changeStatus = TaskQueue::changeStatus($result['task_queue_id'], $status, $error);

                if (!$changeStatus['success']) {
                    $logger->error = 'Error update to status "in_progress": ' . $changeStatus['message'];
                }

            } else {
                $logger->error = Json::encode($result);
            }

        }

        $logger->save();
    }
}
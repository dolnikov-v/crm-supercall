<?php

namespace console\controllers;

use backend\components\AwsS3;
use backend\components\AwsTranscribe;
use backend\modules\audit\models\TranscribeSetting;
use backend\modules\audit\models\TranscribeStatusSet;
use backend\modules\order\models\TranscribeQueue;
use common\models\AmazonSQS;
use common\models\Country;
use common\modules\call\models\CallHistory;
use common\modules\call\models\query\CallHistoryQuery;
use common\modules\order\models\genesys\Call;
use common\modules\order\models\Order;
use Yii;
use yii\console\Controller;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Class AwsController
 * @package console\controllers
 */
class AwsController extends Controller
{
    const BATCH_SIZE = 10;
    const CHUNK_SIZE = 100;

    /**
     * @return null|AwsS3
     */
    public function getAwsS3()
    {
        return yii::$app->get('AwsS3');
    }

    /**
     * @return null|AwsTranscribe
     */
    public function getAwsTranscribe()
    {
        return yii::$app->get('AwsTranscribe');
    }

    /**
     * @return string
     */
    public function getTempPath()
    {
        return yii::getAlias('@backend') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR;
    }

    /**
     * @param $recordingfile
     * @return string
     */
    public function getName($recordingfile)
    {
        return strtr(basename($recordingfile), [':' => '-']);
    }

    /**
     * Только Латы
     * @return array
     */
    public function getCountriesBySetting()
    {
        return ArrayHelper::getColumn(
            TranscribeSetting::findAll(['active' => TranscribeSetting::ACTIVE])
            , 'country_id'
        );
    }

    /**
     * @return CallHistoryQuery
     */
    public function getRecordsQuery()
    {
        $country = Country::findOne(['char_code' => 'PE']);
        $start_time = 1554832800; //2019-04-10 18 00 00

        if (!$country) {
            Console::output('Country PE not found');
            return new Query();
        }

        Console::output('Country PE found: ' . $country->id);

        return CallHistory::find()
            ->distinct()
            ->leftJoin(Order::tableName(), Order::tableName() . '.id=' . CallHistory::tableName() . '.order_id')
            //->leftJoin(TranscribeSetting::tableName(), TranscribeSetting::tableName() . '.country_id=' . Order::tableName() . '.country_id')
            //->leftJoin(TranscribeStatusSet::tableName(), TranscribeStatusSet::tableName() . '.transcribe_setting_id=' . TranscribeSetting::tableName() . '.id')
            ->where(['is', 'transcribe', null])
            //->andWhere([Order::tableName() . '.status' => new Expression(TranscribeStatusSet::tableName() . '.status')])
            ->andWhere(['disposition' => CallHistory::DISPOSITION_ANSWERED])
            ->andWhere(['is not', 'recordingfile', null])
            ->andWhere(['>=', 'billsec', 5])
            //для Перу только
            ->andWhere([Order::tableName() . '.country_id' => $country->id])
            ->andWhere(['>=', CallHistory::tableName() . '.created_at', $start_time]);
            //->andWhere([TranscribeSetting::tableName() . '.active' => TranscribeSetting::ACTIVE])
            //->orderBy(['call_history.id' => SORT_DESC]);
    }

    /**
     * @param string $recordingfile
     * @return bool|string
     */
    public function getRecordingFileFromAsterisk($recordingfile)
    {
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);

        $response = $client->createRequest()
            ->setUrl($recordingfile)
            ->send();

        if ($response->isOk) {
            return $response->content;
        } else {
            return false;
        }

    }

    /**
     * @param $recordingfile
     * @param $content
     * @return bool|string
     */
    public function saveRecordingFileFromTemp($id, $recordingfile, $content)
    {
        $filePath = $this->getTempPath() . $id . '_' . $this->getName($recordingfile);

        if (!file_exists($filePath)) {
            if (file_put_contents($filePath, $content)) {
                return $filePath;
            } else {
                return false;
            }
        } else {
            return $filePath;
        }
    }


    /**
     * Отправка запроса на сервер астериска для получения файла записи
     * @param TranscribeQueue $transcribeQueue
     * @return array
     */
    public function handlerSentQueryToAsterisk(TranscribeQueue $transcribeQueue)
    {
        $result = [
            'success' => false,
            'message' => 'empty error'
        ];

        $recordingfile = CallHistory::checkRecordFilePath($transcribeQueue->callHistory->recordingfile);

        if (!$content = $this->getRecordingFileFromAsterisk($recordingfile)) {
            $transcribeQueue->status = TranscribeQueue::STATUS_ASTERISK_QUERY_ERROR;
            $transcribeQueue->save();
        } else {
            if ($saveResult = $this->saveRecordingFileFromTemp($transcribeQueue->id, $recordingfile, $content)) {
                $transcribeQueue->status = TranscribeQueue::STATUS_SAVE_TEMP_OK;
                $transcribeQueue->media_file_temp = $saveResult;

                $result = [
                    'success' => true,
                    'message' => null
                ];
            } else {
                $transcribeQueue->status = TranscribeQueue::STATUS_SAVE_TEMP_ERROR;
                $transcribeQueue->error = yii::t('common', 'Ошибка сохранения файла записи {file} в папку Temp', ['file' => $this->getTempPath() . $this->getName($recordingfile)]);
            }

            $transcribeQueue->save(false);
        }

        return $result;
    }

    /**
     * Создать папку для файлов транскрибаций
     * @return bool
     */
    public function createTranscribeBucket()
    {
        if (!$this->getAwsS3()->checkTranscribeBucket()) {
            return $this->getAwsS3()->createBucket(AwsS3::TRANSCRIBE_BUCKET);
        }

        return true;
    }

    /**
     * Загружаем файлы записей разговоров на сервис S3
     * @param TranscribeQueue $transcribeQueue
     * @return array
     */
    public function handlerSentToS3(TranscribeQueue $transcribeQueue)
    {
        $result = [
            'success' => false,
            'message' => 'error creating AwsS3 Component'
        ];

        /** @var AwsS3 $awsS3 */
        if ($awsS3 = $this->getAwsS3()) {
            if (!$haveBucket = $this->createTranscribeBucket()) {
                $transcribeQueue->status = TranscribeQueue::STATUS_SENT_TO_S3_ERROR;
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Не возможно создать папку для транскрибации {bucket}', ['bucket' => AwsS3::TRANSCRIBE_BUCKET])
                ];

            } else {
                $putInOperation = $awsS3->putObjectInBucket(AwsS3::TRANSCRIBE_BUCKET, $transcribeQueue->media_file_temp);

                if (isset($putInOperation['@metadata']) && isset($putInOperation['@metadata']['effectiveUri'])) {
                    $transcribeQueue->status = TranscribeQueue::STATUS_SENT_TO_S3_OK;
                    $transcribeQueue->media_file_s3 = $putInOperation['@metadata']['effectiveUri'];

                    $result = [
                        'success' => true,
                        'message' => $putInOperation['@metadata']['effectiveUri']
                    ];
                } else {
                    $transcribeQueue->status = TranscribeQueue::STATUS_SENT_TO_S3_ERROR;
                    $result = [
                        'success' => false,
                        'message' => Json::encode($putInOperation)
                    ];
                }

                $transcribeQueue->save(false);
            }
        }

        return $result;
    }

    /**
     * @param TranscribeQueue $transcribeQueue
     * @return array
     */
    public function generateParams(TranscribeQueue $transcribeQueue)
    {
        $format = AwsTranscribe::getMediaFormat(yii::getAlias('@backend') . '/files/temp/' . basename($transcribeQueue->media_file_temp));

        if (!$format) {
            $result = [
                'success' => false,
                'message' => 'Incorrect mediaFormatFile'
            ];
        } else {

            $params = [
                'LanguageCode' => AwsTranscribe::ES_LANG,
                'Media' => [
                    'MediaFileUri' => $transcribeQueue->media_file_s3
                ],
                'MediaFormat' => $format,
                'MediaSampleRateHertz' => AwsTranscribe::DEFAUL_RATE_HERTZ,
                'OutputBucketName' => AwsS3::TRANSCRIBE_BUCKET,
                'Settings' => AwsTranscribe::getDefaultSettingsParams(),
            ];

            $generateParams = $this->getAwsTranscribe()->checkParams($params);

            if (!$generateParams['success']) {
                $result = $generateParams;
            } else {
                $params = $generateParams['message'];
                /** Индивидуальные параметры */
                $params['OutputBucketName'] = AwsS3::TRANSCRIBE_BUCKET;
                $params['MediaSampleRateHertz'] = AwsTranscribe::DEFAUL_RATE_HERTZ;
                $params['Settings'] = AwsTranscribe::getDefaultSettingsParams();
                $params['TranscriptionJobName'] = basename($transcribeQueue->media_file_temp) . '_' . time() . '_' . AwsTranscribe::MARK_END_TRANSCRIBE_FILE;

                $result = [
                    'success' => true,
                    'message' => $params
                ];
            }
        }

        return $result;

    }

    /**
     * Создание заданий для транскрибации на сервере AWS
     * @param TranscribeQueue $transcribeQueue
     * @return array
     */
    public function handlerCreateJob(TranscribeQueue $transcribeQueue)
    {
        //s3 не адекватно на очередь запросов отрабатывает
        sleep(3);

        $result = [
            'success' => false,
            'message' => 'error creating AwsTrancribe Component'
        ];

        if (!$generateParams = $this->generateParams($transcribeQueue)) {
            $result = [
                'success' => false,
                'message' => $generateParams['message']
            ];
        } else {
            /** @var AwsTranscribe $awsTranscribe */
            if ($awsTranscribe = $this->getAwsTranscribe()) {
                $params = $generateParams['message'];

                $transcribeQueue->job_name = $generateParams['message']['TranscriptionJobName'];
                $transcribeQueue->job_params = Json::encode($params);
                $transcribeQueue->status = TranscribeQueue::STATUS_WAIT_COMPLETE_JOB;

                $createJob = $awsTranscribe->createJob($transcribeQueue->media_file_s3, $params);

                if (isset($createJob['message']['TranscriptionJob'])
                    && isset($createJob['message']['TranscriptionJob']['TranscriptionJobStatus'])
                    && $createJob['message']['TranscriptionJob']['TranscriptionJobStatus'] == TranscribeQueue::AWS_TRANSCRIBE_IN_PROGRESS
                ) {
                    $transcribeQueue->status = TranscribeQueue::STATUS_WAIT_COMPLETE_JOB;
                    $result = [
                        'success' => true,
                        'message' => $createJob['message']['TranscriptionJob']
                    ];
                } else {
                    $transcribeQueue->status = TranscribeQueue::STATUS_CREATE_JOB_ERROR;
                    $result = [
                        'success' => false,
                        'message' => Json::encode($createJob)
                    ];
                }

                $transcribeQueue->save(false);
            }
        }

        return $result;
    }

    /**
     * Узнать состоянии задачи на транскрибацию
     * @param TranscribeQueue $transcribeQueue
     * @return array
     */
    public function handlerCheckJob(TranscribeQueue $transcribeQueue)
    {
        $result = [
            'success' => false,
            'message' => yii::t('common', 'Ошибка получения данных задачи')
        ];

        $getJob = $this->getAwsTranscribe()->getJob($transcribeQueue->job_name);

        if (isset($getJob['TranscriptionJob']) && isset($getJob['TranscriptionJob']['TranscriptionJobStatus'])) {
            if ($getJob['TranscriptionJob']['TranscriptionJobStatus'] == TranscribeQueue::AWS_TRANSCRIBE_COMPLETED) {
                $transcribeQueue->transcribe_file_s3 = $getJob['TranscriptionJob']['Transcript']['TranscriptFileUri'];
                $transcribeQueue->status = TranscribeQueue::STATUS_CREATE_JOB_OK;
                $result = [
                    'success' => true,
                    'message' => ''
                ];
            } elseif ($getJob['TranscriptionJob']['TranscriptionJobStatus'] == TranscribeQueue::AWS_TRANSCRIBE_FAIL) {
                $transcribeQueue->status = TranscribeQueue::STATUS_CREATE_JOB_ERROR;
                $transcribeQueue->error = Json::encode($getJob);
                $result = [
                    'success' => false,
                    'message' => yii::t('common', 'Ошибка выполнения задачи')
                ];
            } else {
                //для статуса IN_PROGRESS пропускаем
                $result = [
                    'success' => true,
                    'message' => ''
                ];
            }
            $transcribeQueue->save(false);
        }

        return $result;
    }

    /**
     * Чтение файла транскрибации
     * @param TranscribeQueue $transcribeQueue
     * @return array
     */
    public function handlerGetTranscribe(TranscribeQueue $transcribeQueue)
    {

        $transcribeQueue->status = TranscribeQueue::STATUS_GET_TRANSCRIBE_ERROR;

        $getObject = $this->getAwsS3()->getObject(AwsS3::TRANSCRIBE_BUCKET, basename($transcribeQueue->transcribe_file_s3));

        if (!$getObject) {
            $transcribeQueue->status = TranscribeQueue::STATUS_GET_TRANSCRIBE_ERROR;
            $transcribeQueue->error = $getObject;

            $result = [
                'success' => false,
                'message' => $getObject
            ];
        } else {
            ob_start();
            echo $getObject;
            $content = ob_get_contents();
            ob_end_clean();

            $data = Json::decode($content);

            $transcribe = [];

            foreach ($data['results']['transcripts'] as $transcript) {
                $transcribe[] = $transcript['transcript'];
            }

            $result = [
                'success' => true,
                'message' => ''
            ];
            $transcribeQueue->transcribe_json = $content;
            $transcribeQueue->callHistory->transcribe = implode(PHP_EOL, $transcribe);
            $transcribeQueue->callHistory->transcribe_price = $transcribeQueue->callHistory->billsec * CallHistory::TRUNSCRIBE_PRICE_ONE_SECOND;

            if ($transcribeQueue->callHistory->save(false)) {
                $transcribeQueue->status = TranscribeQueue::STATUS_GET_TRANSCRIBE_OK;
            }
        }

        $transcribeQueue->save(false);

        return $result;
    }

    /**
     * @param TranscribeQueue $transcribeQueue
     * @return array
     */
    public function getHandlerByStatus(TranscribeQueue $transcribeQueue)
    {
        switch ($transcribeQueue->status) {
            case TranscribeQueue::STATUS_IN_PROGRESS:
                $result = $this->handlerSentQueryToAsterisk($transcribeQueue);
                break;
            case TranscribeQueue::STATUS_SAVE_TEMP_OK:
                $result = $this->handlerSentToS3($transcribeQueue);
                break;
            case TranscribeQueue::STATUS_SENT_TO_S3_OK:
                $result = $this->handlerCreateJob($transcribeQueue);
                break;
            case TranscribeQueue::STATUS_WAIT_COMPLETE_JOB:
                $result = $this->handlerCheckJob($transcribeQueue);
                break;
            case TranscribeQueue::STATUS_CREATE_JOB_OK:
            case TranscribeQueue::STATUS_GET_TRANSCRIBE_OK:
                $result = $this->handlerGetTranscribe($transcribeQueue);
                break;
            //если запись без статуса - поставим первоначальный статус
            default:
                $transcribeQueue->status = TranscribeQueue::STATUS_IN_PROGRESS;
                $transcribeQueue->save(false);

                $result = [
                    'success' => true,
                    'message' => yii::t('common', 'Неизвестный статус очереди: {status}', ['status' => $transcribeQueue->status])
                ];
                break;
        }

        return $result;
    }

    /**
     *  Создание заданий по CallHistory
     *  Самостоятельный воркер
     */
    public function actionCreate()
    {
        $data = [];
        echo $this->getRecordsQuery()->createCommand()->getRawSql();
        Console::output('Records found: ' . $this->getRecordsQuery()->count());

        foreach ($this->getRecordsQuery()->batch(self::BATCH_SIZE) as $records) {
            foreach ($records as $record) {
                /** @var CallHistory $recordData */
                if (!$transcribeQueue = TranscribeQueue::findByCallHistory($record->id)) {
                    /** @var CallHistory $record */
                    /** @var TranscribeSetting $record ->order->country->transcribeSetting */
                    /** @var Order $record ->order */

                    $data[] = [
                        'call_history_id' => $record->id,
                        'media_file_temp' => null,
                        'media_file_s3' => null,
                        'job_name' => null,
                        'job_params' => null,
                        'transcribe_file_s3' => null,
                        'transcribe_json' => null,
                        'status' => TranscribeQueue::STATUS_IN_PROGRESS,
                        'error' => null
                    ];
                }
            }
        }

        $chunks = array_chunk($data, self::CHUNK_SIZE);

        try {
            foreach ($chunks as $chunk) {

                yii::$app->db->createCommand()->batchInsert(TranscribeQueue::tableName(), [
                    'call_history_id',
                    'media_file_temp',
                    'media_file_s3',
                    'job_name',
                    'job_params',
                    'transcribe_file_s3',
                    'transcribe_json',
                    'status',
                    'error'
                ], $chunk)->execute();
            }
        } catch (Exception $e) {
            Console::error($e->getMessage());
        }
    }

    /**
     * Метод получения транскрибации с сервиса Amazon
     * Самостоятельный воркер
     */
    public function actionTranscribe()
    {
        foreach (TranscribeQueue::find()
                     ->where(['<>', 'status', TranscribeQueue::STATUS_GET_TRANSCRIBE_OK])
                     ->andWhere("coalesce(error,'') = ''")
                     ->batch(self::BATCH_SIZE) as $transcribeQueues) {
            /** @var TranscribeQueue $transcribeQueue */
            foreach ($transcribeQueues as $transcribeQueue) {
                /** @var array $result */
                $result = $this->getHandlerByStatus($transcribeQueue);

                if (!$result['success']) {
                    $transcribeQueue->error = $result['message'];

                    if (!$transcribeQueue->save('false', ['error'])) {
                        Console::error('Error save "error" from #' . $transcribeQueue->id . ': ' . Json::encode($transcribeQueue->getErrors()));
                        Console::output('Data: ' . $result['message']);
                    }
                }
            }
        }
    }

    /**
     *  актуаьный метод получения записей с астериска
     */
    public function actionGetMultipleCallHistory()
    {
        sleep(10);
        
        $sqs = new AmazonSQS();

        $messagesBatch = $sqs->getMessages(10, ['All'], yii::$app->params['AsteriskCallDataMultiple']);

        Console::output('Messages count: ' . count($messagesBatch));

        if (count($messagesBatch) > 0) {
            foreach ($messagesBatch as $messages) {
                $records = Json::decode($messages);

                foreach ($records as $record) {

                    $callHistory = CallHistory::find()
                        ->where([
                            'user_sip' => $record['user_sip'],
                            'phone' => $record['phone'],
                            'disposition' => CallHistory::DISPOSITION_WAITING
                        ])->orderBy(['id' => SORT_ASC])
                        ->one();

                    if ($callHistory) {
                        Console::output('#' . $callHistory->id . ' found by phone: ' . $record['phone'] . ' & sip: ' . $record['user_sip']);

                        $callHistory->duration = $record['duration'];
                        $callHistory->billsec = $record['billsec'];
                        $callHistory->disposition = $record['disposition'];
                        $callHistory->recordingfile = $record['recordingfile'];
                        $callHistory->uniqueid = $record['uniqueid'];

                        if (!$callHistory->save(false)) {
                            $this->sentErrorCallHistory($sqs, Json::encode($callHistory->getErrors()));
                            Console::error('#' . $callHistory->id . ' error: ' . Json::encode($callHistory->getErrors()));
                        } else {
                            Console::output('#' . $callHistory->id . ' updated');
                        }
                    } else {
                        Console::output('Not found by phone: ' . $record['phone'] . ' & sip: ' . $record['user_sip']);
                    }
                }
            }
        } else {
            Console::output('Sleep 60 sec.');
            sleep(60);
        }

        if (!Yii::$app->params['is_dev']) {
            if (!empty($sqs->handlers)) {
                foreach ($sqs->handlers as $handlers) {

                    Console::output("Try to delete message");

                    //Удаляем из очереди обработанные сообщения
                    $sqs->deleteMessage($handlers, yii::$app->params['AsteriskCallDataMultiple']);
                }
            }
        }
    }

    /**
     * Обновление call_history CRM по данным call_history Asterisk
     */
    public function actionGetCallHistory($lifetime = 86400)
    {
        Console::output('Get CallHistory');

        $sqs = new AmazonSQS();
        $launchTime = time();
        while (((time() - $launchTime) < $lifetime)) {

            $messages = $sqs->getMessages(10, ['All'], yii::$app->params['AsteriskCallData']);

            Console::output('Messages count: ' . count($messages));

            if (count($messages) > 0) {
                foreach ($messages as $message) {
                    $data = Json::decode($message);

                    if (isset($data['call_history'])) {
                        continue;
                    }

                    $error = false;

                    if (!isset($data[0])) {
                        $callHistory = CallHistory::find()
                            ->where([
                                'user_sip' => $data['user_sip'],
                                'phone' => $data['phone'],
                                'disposition' => CallHistory::DISPOSITION_WAITING
                            ])
                            ->andWhere([
                                'or',
                                ['uniqueid' => $data['uniqueid']],
                                ['is', 'uniqueid', null]
                            ])
                            ->orderBy(['id' => SORT_ASC])
                            ->one();

                        if (!$callHistory) {
                            $error = 'Not found by user_sip: ' . $data['user_sip'] . ' and phone: ' . $data['phone'] . ' with uniqueid is null or ' . $data['uniqueid'];
                        } else {
                            if (is_null($callHistory->uniqueid)) {
                                $callHistory->duration = $data['duration'];
                                $callHistory->billsec = $data['billsec'];
                                $callHistory->disposition = $data['disposition'];
                                $callHistory->recordingfile = $data['recordingfile'];
                                $callHistory->uniqueid = $data['uniqueid'];

                                if (!$callHistory->save(false)) {
                                    $error = 'Error update #:' . $callHistory->id . ' ' . $callHistory->getFirstErrorAsString();
                                }
                            }
                        }

                        if ($error) {
                            //пока не отправляем
                            //$this->sentErrorCallHistory($sqs, $error);
                        } else {
                            Console::output('Update #' . $callHistory->id . ' success');
                        }
                    }
                }
            } else {
                $seconds = 60;

                Console::output('Sleep ' . $seconds . ' sec.');

                sleep($seconds);
            }

            if (!Yii::$app->params['is_dev']) {
                if (!empty($sqs->handlers)) {
                    foreach ($sqs->handlers as $handlers) {

                        Console::output("Try to delete message");

                        //Удаляем из очереди обработанные сообщения
                        $sqs->deleteMessage($handlers, yii::$app->params['AsteriskCallData']);
                    }
                }
            }
        }
    }

    /**
     * Обновление call_history CRM по данным call_history Asterisk из таблицы CDR по call_history_id
     */
    public function actionGetCallHistoryFromCdr($lifetime = 86400)
    {
        Console::output('Get CallHistory');

        $sqs = new AmazonSQS();

        $launchTime = time();
        while (((time() - $launchTime) < $lifetime)) {
            $messages = $sqs->getMessages(10, ['All'], yii::$app->params['AsteriskCallData']);

            Console::output('Messages count: ' . count($messages));

            if (count($messages) > 1) {
                foreach ($messages as $message) {
                    $data = Json::decode($message);
                    $error = false;

                    if (!isset($data[0])) {
                        if (!isset($data['call_history_id'])) {
                            //$this->sentErrorCallHistory($sqs, $error);
                            continue;
                        }

                        $callHistory = CallHistory::find()
                            ->where([
                                'id' => (int)$data['call_history_id'],
                            ])
                            ->one();

                        if (!$callHistory) {
                            $error = 'Not found by call_history_id: ' . $data['call_history_id'];
                        } else {
                            if ($callHistory->disposition == CallHistory::DISPOSITION_WAITING) {
                                $callHistory->duration = $data['duration'];
                                $callHistory->billsec = $data['billsec'];
                                $callHistory->disposition = $data['disposition'];
                                $callHistory->recordingfile = $data['recordingfile'];
                                $callHistory->uniqueid = $data['uniqueid'];

                                if (!$callHistory->save(false)) {
                                    $error = 'Error update #:' . $callHistory->id . ' ' . $callHistory->getFirstErrorAsString();
                                }
                            }
                        }

                        if ($error) {
                            //$this->sentErrorCallHistory($sqs, $error);
                        } else {
                            Console::output('Update #' . $callHistory->id . ' success');
                        }
                    }
                }
            } else {
                $seconds = 60;

                Console::output('Sleep ' . $seconds . ' sec.');

                sleep($seconds);
            }

            if (!Yii::$app->params['is_dev']) {
                if (!empty($sqs->handlers)) {
                    foreach ($sqs->handlers as $handlers) {

                        Console::output("Try to delete message");

                        //Удаляем из очереди обработанные сообщения
                        $sqs->deleteMessage($handlers, yii::$app->params['AsteriskCallData']);
                    }
                }
            }
        }
    }

    /**
     * @param AmazonSQS $sqs
     * @param string $messageError
     */
    public function sentErrorCallHistory(AmazonSQS $sqs, $messageError)
    {
        Console::error($messageError);

        $sqs->sendMessage([
            'MessageBody' => Json::encode($messageError),
            'QueueUrl' => yii::$app->params['AsteriskCallDataError']
        ]);
    }
}
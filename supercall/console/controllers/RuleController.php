<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\rbac\ManagerInterface;

/**
 * Менеджер прав
 * Class RuleController
 * @package console\controllers
 */
class RuleController extends Controller
{
    const APPLY = 'Apply the above permissions? <yes/no>';
    const REMOVE = 'Remove the above permissions? <yes/no>';

    /** @var ManagerInterface $auth */
    private $auth;

    /**
     * @var
     */
    private $path;

    /**
     * @var array
     */
    private $rules = [];

    /**
     * @var array
     */
    private $required = [];

    /**
     * @var array
     */
    private $ignored = [];

    private $namespace = [
        'access' => 'backend\modules\access\controllers',
        'catalog' => 'backend\modules\catalog\controllers',
        'home' => 'backend\modules\home\controllers',
        'i18n' => 'backend\modules\i18n\controllers',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->path = Yii::getAlias('@backend');
        $this->auth = Yii::$app->authManager;
    }

    /**
     * Поиск и установка новых прав
     * @param string $apply
     */
    public function actionInstall($apply = 'no')
    {
        $this->getAll()->removeIgnore()->getNew()->view()->message(self::APPLY, true, $apply);
    }

    /**
     * Удаление не используемых прав
     * @param string $apply
     */
    public function actionClean($apply = 'no')
    {
        $this->getAll()->removeIgnore()->getExists()->view()->message(self::REMOVE, false, $apply);
    }

    /**
     * Добавить
     */
    public function add()
    {
        foreach ($this->rules as $rule) {
            $permission = $this->auth->createPermission($rule);
            $permission->description = $rule;

            $this->auth->add($permission);
            $this->stdout('Create Permission ' . $rule . PHP_EOL, Console::FG_GREEN);
        }
    }

    /**
     * Удалить
     */
    public function remove()
    {
        foreach ($this->rules as $rule) {
            $this->auth->remove($this->auth->getPermission($rule));
            $this->stdout('Remove Permission ' . $rule . PHP_EOL, Console::FG_GREEN);
        }
    }

    /**
     * Оставить новые
     * @return $this
     */
    public function getNew()
    {
        $new = [];
        foreach ($this->rules as $item) {
            if (!$this->auth->getPermission($item)) {
                $new[] = $item;
            }
        }
        $this->rules = $new;

        return $this;
    }

    /**
     * Удалить игнорируемые
     * @return $this
     */
    public function removeIgnore()
    {
        $this->rules = array_diff($this->rules, $this->ignored);

        return $this;
    }

    /**
     * Поиск всех экшенов
     * @return $this
     */
    public function getAll()
    {
        foreach ($this->namespace as $key => $namespace) {
            $namespacePath = str_replace('\\', DIRECTORY_SEPARATOR, $namespace);
            $path = str_replace(DIRECTORY_SEPARATOR . 'backend' . DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, $this->path . DIRECTORY_SEPARATOR . $namespacePath);

            $files = new \DirectoryIterator($path);


            foreach ($files as $file) {
                if (stristr($file, '.php')) {
                    $controller = substr(basename($file->getFilename()), 0, -4);
                    $methods = get_class_methods($namespace . '\\' . $controller);

                    foreach ($methods as $method) {
                        if (substr($method, 0, 6) == 'action' && $method != 'actions') {
                            $action = substr($method, 6);
                            $keyName = ($key) ? strtolower($key) . '.' : '';
                            $this->rules[] = $keyName . strtolower(substr($controller, 0, -10)) . '.' . strtolower($action);
                        }
                    }
                }
            }
        }

        foreach ($this->required as $item) {
            $this->rules[] = $item;
        }

        return $this;
    }

    /**
     * Оставить не найденные
     * @return $this
     */
    public function getExists()
    {
        $rules = ArrayHelper::getColumn($this->auth->getPermissions(), 'name', false);
        $this->rules = array_diff($rules, $this->rules);

        return $this;
    }

    /**
     * Распечатать
     */
    public function view()
    {
        foreach ($this->rules as $rule) {
            $this->stdout($rule . PHP_EOL, Console::FG_YELLOW);
        }

        return $this;
    }

    /**
     * Получить подверждение
     * @param $message
     * @param boolean $add
     * @param string $apply
     */
    public function message($message, $add = true, $apply = 'no')
    {
        if ($this->rules) {
            if (!in_array($apply, ['y', 'yes'])) {
                $command = $this->prompt($message, ['default' => 'no']);
            } else {
                $command = 'yes';
            }

            if (in_array($command, ['y', 'yes'])) {
                ($add) ? $this->add() : $this->remove();
            }
        }
    }
}

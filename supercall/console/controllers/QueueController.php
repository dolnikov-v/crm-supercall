<?php

namespace console\controllers;

use backend\modules\administration\models\BaseLogger;
use common\models\Timezone;
use Yii;
use yii\helpers\Console;
use common\modules\order\models\Order;
use backend\modules\queue\models\QueueMode;
use backend\modules\queue\models\Queue;
use yii\helpers\Json;

/**
 * Формирование очереди через определенный интервал времени
 * запускается по крону, предварительно через каждые 5 минут
 * @package console\controllers
 *
 * yii queue/create 1  - наполнить очередь
 * yii queue/create-all  - наполнить все очереди
 * yii queue/clear-all  - очистить все очереди
 * yii queue/remove-all  - удалить все очереди
 */
class QueueController extends \yii\console\Controller
{
    /**
     * Очистить все очереди
     * принимаем сообщения  у каждой активной очереди
     */
    public function actionClearAll()
    {
        $this->clearAll();
    }

    private function clearAll()
    {
        echo "queue clear start" . PHP_EOL;
        $queuesQuery = Queue::find();
        echo "queue count = " . $queuesQuery->count() . PHP_EOL;

        Yii::$app->amqp->declareExchange(Queue::EXCHANGE, $type = 'direct', $passive = false, $durable = true);
        foreach ($queuesQuery->all() as $qu) {
            Yii::$app->amqp->declareQueue($qu->name);
            Yii::$app->amqp->bindQueueExchanger($qu->name, Queue::EXCHANGE, $routingKey = $qu->name);
            Yii::$app->amqp->queuePurge($qu->name);
        }

        echo "queue clear finish" . PHP_EOL;
    }

    /**
     * Удалить все очереди
     */
    public function actionRemoveAll()
    {
        $queuesQuery = Queue::find();
        Console::output("queue count = " . $queuesQuery->count());

        foreach ($queuesQuery->all() as $qu) {
            self::actionRemove($qu->id);
        }

        echo "queue clear finish" . PHP_EOL;
    }

    /**
     * Удалить очередь по ID
     * @param $id
     */
    public function actionRemove($id)
    {
        $queueQuery = Queue::find()->where(['id' => $id]);
        if ($queueQuery->exists()) {
            $queue = $queueQuery->one();

            Yii::$app->amqp->queueDelete($queue->name);
            Console::output("queue $queue->name removed");
        }

    }

    /**
     * Создаем очереди и наполняем их заказами
     * @param bool $is_not_console | true - если метод вызывается не из консоли
     */
    public function actionCreateAll($is_not_console = false)
    {
        // блокировка от двойного запуска
        if (!$is_not_console) {
            if (!is_dir(Yii::getAlias('@runtime') . '/blocks/')) {
                mkdir(Yii::getAlias('@runtime') . '/blocks/');
            }
        }

        // clear queues
        //$this->clearAll();

        $queuesQuery = Queue::find();
        echo "queue count = " . $queuesQuery->count() . PHP_EOL;

        Yii::$app->amqp->declareExchange(Queue::EXCHANGE, $type = 'direct', $passive = false, $durable = true);

        //Очищаем очередь в БД
        $this->truncateQueueOrder();

        foreach ($queuesQuery->all() as $qu) {

            $lockFilename = "/blocks/queue_create_" . $qu->id . ".lock";

            $filePathName = strtr(Yii::getAlias('@runtime') . $lockFilename, [
                'backend' => 'console',
                'frontend' => 'console',
            ]);

            $lockFp = fopen($filePathName, 'w');

            if (!flock($lockFp, LOCK_EX | LOCK_NB)) {
                continue;
            }

            if (!$is_not_console) {
                Console::output('queue ' . $qu->id . ' declare');
            }

            $queue_id = (yii::$app->params['is_dev'] ? 'dev_' : '') . $qu->id;

            Yii::$app->amqp->declareQueue($queue_id);
            Yii::$app->amqp->bindQueueExchanger($queue_id, Queue::EXCHANGE, $routingKey = $qu->id);
            Yii::$app->amqp->queuePurge($queue_id);

            //ищем в БД очерель, и  если он активна то идем дальше
            $activeQueues = Queue::find()->where(['id' => $qu->id])->active();
            $queues = $activeQueues->notMode()->priority();
            if ($queues->exists()) {
                if (!$is_not_console) {
                    Console::output('queue create start');
                }
                foreach ($queues->all() as $queue) {
                    self::actionCreate($queue->id, true);
                }
                if (!$is_not_console) {
                    Console::output('queue create finish');
                }
            } else {
                if (!$is_not_console) {
                    Console::output('queue is not exists');
                }
            }
            fclose($lockFp);

        }

        if (!$is_not_console) {
            echo "queue clear finish" . PHP_EOL;
        }
    }

    /**
     * Функция запускает обычные очереди rabbitMQ
     * @param string $activeQueues
     */
    private function create($activeQueues)
    {
        $queues = $activeQueues->notMode()->priority();

        if ($queues->exists()) {
            Console::output('queue create start');

            foreach ($queues->all() as $queue) {
                self::actionCreate($queue->id);
            }

            Console::output('queue create finish');
        } else {
            Console::output('queue is not exists');
        }

    }

    /**
     * Выполняется сбор заказов и передача в загружчик,
     * который будет выгружать заказы для обзвона в систему обзвона
     * @param $activeQueues
     */
    private function createMode($activeQueues)
    {
        $queues = $activeQueues->mode()->priority();

        if ($queues->exists()) {
            Console::output('queueMode create start');

            foreach ($queues->all() as $queue) {
                // запустим сбор заказов ля обзвона
                if (QueueMode::run($queue)) {
                    $status = 'SUCCESS';
                } else {
                    $status = 'WRONG';
                }
                Console::output("queueMode #$queue->name is $status");
            }

            Console::output('queueMode create finish');

        } else {
            Console::output('queueMode is not exists');
        }

    }


    /**
     * Запустить очередь согласно ID
     * @param $id
     * @param bool $is_not_console | true - если метод вызыватеся не из консоли
     * @throws \ErrorException
     */
    public function actionCreate($id, $is_not_console = false)
    {
        $queueQuery = Queue::find()->where(['id' => $id])->active();

        if ($queueQuery->exists()) {
            Yii::$app->amqp->declareExchange(Queue::EXCHANGE, $type = 'direct', $passive = false, $durable = true);

            $queue = $queueQuery->one();
            // имя очереди
            $queueName = (yii::$app->params['is_dev'] ? 'dev_' : '') . $queue->id;
            $messages = Queue::build($queue);

            Yii::$app->amqp->declareQueue($queueName);
            Yii::$app->amqp->bindQueueExchanger($queueName, Queue::EXCHANGE, $routingKey = $queueName);
            // сначала очиститм данную очередь
            Yii::$app->amqp->queuePurge($queueName);

            $count = count($messages);

            if (!$is_not_console) {
                Console::output("queue #$queueName COUNT = " . $count);
            }

            $loggerData = [];

            //Console::startProgress(0, $count);
            if ($count > 0) {
                // все ID сложим с очередь
                $item = 0;
                foreach ($messages as $orderId) {// опубликовать
                    $item++;
                    Queue::publisher($queue, $orderId);

                    $loggerData[] = [
                        'route' => $this->route,
                        'action' => basename(__METHOD__),
                        'type' => Queue::LOGGER_TYPE,
                        'tags' => Json::encode([
                            'queue_id' => $queue->id,
                            'is_primary' => $queue->is_primary,
                            'order_id' => $orderId
                        ]),
                        'created_at' => yii::$app->formatter->asTimestamp(time()),
                    ];
                    //Console::updateProgress($item, $count);
                }
            }

            /** WCALL-489 Пишем время попадания заказа в Rabbit очередь */
            $this->writeBaseLogs($loggerData);

            if (!$is_not_console) {
                Console::output("queue #$queueName is done" . PHP_EOL);
            }
        } else
            throw new \ErrorException('Нет такой очереди, либо она неактивна');

    }

    public function writeBaseLogs($data)
    {
        if(!empty($data)){
            yii::$app->db->createCommand()->batchInsert(BaseLogger::tableName(), [
                'route' ,
                'action',
                'type',
                'tags',
                'created_at',
            ], $data)->execute();
        }
    }

    public function actionTest($qeId)
    {
        $message = Yii::$app->amqp->basicGet($qeId);
        if ($message) {
            Yii::$app->amqp->basicAck($message->delivery_info['delivery_tag']);
            echo 'message = ' . $message->body . PHP_EOL;
        }
    }

    private function truncateQueueOrder()
    {
        $current_time = (new \DateTime())
            ->setTimezone(new \DateTimeZone(Timezone::DEFAULT_TIMEZONE))
            ->getTimestamp();

        Yii::$app->db->createCommand('DELETE FROM "queue_order" WHERE blocked_to < ' . $current_time)->execute();
        //Yii::$app->db->createCommand()->truncateTable('{{%queue_order}}')->execute();
    }

    private function truncateQueueOrderByName($name)
    {
        Yii::$app->db->createCommand("DELETE FROM \"queue_order\" WHERE queue_id IN (SELECT id FROM queue WHERE name = '" . $name . "')")->execute();
    }

}